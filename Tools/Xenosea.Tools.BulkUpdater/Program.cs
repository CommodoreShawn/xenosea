﻿using System;

namespace Xenosea.Tools.BulkUpdater
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var inputDirectory = @"..\..\xenosea\Game\Resources\Equipment";

            var allResources = FindFiles(inputDirectory, "*.tres");

            Console.WriteLine($"Found {allResources.Count} files.");

            foreach(var file in allResources)
            {
                var lines = File.ReadAllLines(file);

                for(var i = 0; i < lines.Length; i++)
                {
                    var line = lines[i];

                    if(line.StartsWith("Value = "))
                    {
                        var amount = double.Parse(line.Substring(8));

                        lines[i] = $"Value = {amount / 2}";

                        Console.WriteLine("Updated value in " + Path.GetFileName(file));
                    }
                }

                File.WriteAllLines(file, lines);
            }
        }

        private static List<string> FindFiles(string directory, string extension)
        {
            Console.WriteLine("Looking in " + directory);
            var files = Directory.GetFiles(directory, extension)
                .ToList();

            var subsDirs = Directory.GetDirectories(directory);

            foreach(var subDir in subsDirs)
            {
                files.AddRange(FindFiles(subDir, extension));
            }

            return files;
        }
    }
}