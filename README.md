# Xenosea

An open world submarine game.

## Asset Attributions
Portraits by ArtBreeder
Models by OtterAlpha
Music by Kevin MacLeod (see Music Attribution.txt)