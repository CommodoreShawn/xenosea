del /Q /S Game\Export\*

godot Game\project.godot --export "Windows Desktop" Export\Windows\Xenosea.exe

godot -s Game\ReplaceIcon.gd Game\icon.ico \Game\Export\Windows\Xenosea.exe

butler push --userversion-file=GameVersion.txt Game\Export\Windows commodoreshawn/xenosea:windows

godot Game\project.godot --export "Linux/X11" Export\Linux\Xenosea.x86_64

butler push --userversion-file=GameVersion.txt Game\Export\Linux commodoreshawn/xenosea:linux