﻿using System;

public static class Constants
{
    public static readonly double CASH_TO_STANDING = 0.0001;
    public static readonly float DAMAGE_TO_AGGRIVATION = 0.04f;
    public static readonly float WORLD_RADIUS = 200000;
    public static readonly string SEED = "calico";
    public static readonly DateTime START_DATE = new DateTime(685, 12, 10);
    public static readonly float MAX_DEPTH = 20000;
    public static readonly float DATUM_DISTANCE = 5000;
    public static readonly int NOISE_SCALE = 200000;
    public static readonly int TERRAIN_VERTEX_COUNT = 100;
    
    public static readonly float DEPTH_MAX_DIVER = 2500;
    public static readonly float DEPTH_MAX_SUIT = 5000;
    public static readonly float DEPTH_MAX_SUB = 8250;

    public static readonly float SONAR_ENV_NOISE = 5;
    public static readonly float SONAR_ENV_PROPAGATION = 0.001f;

    public static readonly float EXPLORATION_DISTANCE = 20000;
    public static readonly float CRUISE_MODE_TIME_SCALE = 30f;
    public static readonly float TURN_MAX_DELTA = 0.1f;

    public static readonly float MINISUB_RECOVER_DISTANCE = 60f;
    public static readonly float VOLUME_TO_CARGO = 0.4f;
    public static readonly float TRADE_IN_VALUE_MOD = 0.5f;
    public static readonly float BOUNTY_FACTOR = 0.1f;


    public static bool DEBUG_SONAR_CONTACT_WIDGET = false;
    public static bool DEBUG_AGENT_GOALS = false;
    public static bool DEBUG_AGENT_DISPLAY = false;
    public static bool DEBUG_PERF_DISPLAY = true;
}
