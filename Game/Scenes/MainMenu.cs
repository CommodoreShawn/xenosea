using Godot;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

public class MainMenu : Control
{
    [Export]
    public StartingFactionRelation[] FactionRelations;
    [Export]
    public BountyOffering[] BountyOfferings;
    [Export]
    public AbstractQuest[] OpenWorldQuests;
    [Export]
    public Alliance[] Alliances;
    [Export]
    public WorldDefinition WorldDefinition;

    private ProgressPopup _progressPopup;
    private SaveLoadPopup _saveLoadPopup;
    private bool _wasLoadPickerOpen;
    private SettingsPopup _settingsPopup;

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IWorldManager _worldManager;
    [Inject]
    private IWorldSimulator _worldSimulator;
    [Inject]
    private IMissionTracker _missionTracker;
    [Inject]
    private IStandingTracker _standingTracker;
    [Inject]
    private ICrimeTracker _crimeTracker;
    [Inject]
    private IQuestTracker _questTracker;
    [Inject]
    private IWorldGenerator _worldGenerator;
    [Inject]
    private IGameSerializer _gameSerializer;
    [Inject]
    private ILoadSequencer _loadSequencer;
    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    public MainMenu()
    {
    }

    public override void _Ready()
    {
        this.ResolveDependencies();
        _progressPopup = GetNode<ProgressPopup>("ProgressPopup");
        _saveLoadPopup = GetNode<SaveLoadPopup>("SaveLoadPopup");
        _settingsPopup = GetNode<SettingsPopup>("SettingsPopup");
        GetNode<Control>("MainMenuPanel").Visible = true;

        GetNode<Button>("MainMenuPanel/VBoxContainer/LoadCampaignButton").Disabled = !_gameSerializer.ListSaves().Any();
        GetNode<Button>("MainMenuPanel/VBoxContainer/ContinueCampaignButton").Disabled = !_gameSerializer.ListSaves().Any();
        Input.MouseMode = Input.MouseModeEnum.Visible;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_wasLoadPickerOpen && !_saveLoadPopup.Visible)
        {
            _wasLoadPickerOpen = false;
            GetNode<Button>("MainMenuPanel/VBoxContainer/LoadCampaignButton").Disabled = !_gameSerializer.ListSaves().Any();
            GetNode<Button>("MainMenuPanel/VBoxContainer/ContinueCampaignButton").Disabled = !_gameSerializer.ListSaves().Any();
        }
    }

    public void ShowMainMenu()
    {
        _soundEffectPlayer.PlayUiClick();
        GetNode<Control>("MainMenuPanel").Visible = true;
    }

    public void ShowWorldGen()
    {
        _soundEffectPlayer.PlayUiClick();
        SetupFactionsAndServices();

        _questTracker.InitializeQuests(OpenWorldQuests);

        _playerData.PlayerLoadout = WorldDefinition.StarterSub;
        _playerData.Faction = WorldDefinition.PlayerFaction;
        _playerData.Money = 100000;
        _playerData.DetailedSimDistance = 40000;

        GetTree().ChangeScene("res://Scenes/WorldgenTest.tscn");
    }

    public void StartNewCampaign()
    {
        _soundEffectPlayer.PlayUiClick();
        SetupFactionsAndServices();

        _questTracker.InitializeQuests(OpenWorldQuests);

        _progressPopup.ShowFor(
                "Generating World...",
                reportProgress =>
                {
                    _loadSequencer.StartNewWorld(WorldDefinition, reportProgress);
                },
                () =>
                {
                    GetTree().ChangeScene("res://Scenes/Gameplay.tscn");
                });
    }

    public void LoadCampaign()
    {
        _soundEffectPlayer.PlayUiClick();
        SetupFactionsAndServices();

        _questTracker.InitializeQuests(OpenWorldQuests);

        _wasLoadPickerOpen = true;

        _saveLoadPopup.ShowForLoad(
            fileName =>
            {
                _progressPopup.ShowFor(
                    "Loading...",
                    reportProgress =>
                    {
                        _loadSequencer.LoadWorld(WorldDefinition, fileName, reportProgress);
                    },
                    () =>
                    {
                        GetTree().ChangeScene("res://Scenes/Gameplay.tscn");
                    });
            });
    }

    public void ContinueCampaign()
    {
        _soundEffectPlayer.PlayUiClick();
        SetupFactionsAndServices();

        _questTracker.InitializeQuests(OpenWorldQuests);

        var toLoad = _gameSerializer.ListSaves()
            .OrderByDescending(x => x.LastPlayed)
            .FirstOrDefault();

        if(toLoad != null)
        {
            _progressPopup.ShowFor(
                "Loading...",
                reportProgress =>
                {
                    _loadSequencer.LoadWorld(
                        WorldDefinition,
                        toLoad.Name,
                        reportProgress);
                },
                () =>
                {
                    GetTree().ChangeScene("res://Scenes/Gameplay.tscn");
                });
        }
    }

    public void OnQuitButton()
    {
        _soundEffectPlayer.PlayUiClick();
        GetTree().Quit();
    }

    private void SetupFactionsAndServices()
    {
        var allFactions = FactionRelations
            .SelectMany(x => x.Factions)
            .Concat(FactionRelations.SelectMany(fr => fr.Alliances.SelectMany(a => a.Members)))
            .Concat(Alliances.SelectMany(a => a.Members))
            .Distinct()
            .ToArray();

        foreach (var faction in allFactions)
        {
            faction.FactionRelations.Clear();
            faction.OfferedBounties.Clear();
        }

        foreach(var alliance in Alliances)
        {
            foreach(var faction in alliance.Members)
            {
                foreach(var otherFaction in alliance.Members.Where(f => f != faction))
                {
                    faction.FactionRelations[otherFaction.Name] = 100;
                }
            }
        }

        foreach (var relation in FactionRelations)
        {
            foreach(var faction in relation.Factions)
            {
                foreach (var otherFaction in relation.Factions.Where(f => f != faction))
                {
                    faction.FactionRelations[otherFaction.Name] = relation.StartingRelation;
                }
                foreach(var alliance in relation.Alliances)
                {
                    foreach(var otherFaction in alliance.Members.Where(f => f != faction))
                    {
                        faction.FactionRelations[otherFaction.Name] = relation.StartingRelation;
                    }
                }
            }

            foreach(var alliance in relation.Alliances)
            {
                foreach (var faction in alliance.Members)
                {
                    foreach (var otherFaction in relation.Factions.Where(f => f != faction))
                    {
                        faction.FactionRelations[otherFaction.Name] = relation.StartingRelation;
                    }
                    foreach (var otherAlliance in relation.Alliances.Where(a => a != alliance))
                    {
                        foreach (var otherFaction in otherAlliance.Members.Where(f => f != faction))
                        {
                            faction.FactionRelations[otherFaction.Name] = relation.StartingRelation;
                        }
                    }
                }
            }
        }

        foreach (var bountyOffering in BountyOfferings)
        {
            foreach(var faction in bountyOffering.OfferingFactions)
            {
                foreach(var otherFaction in bountyOffering.TargetFactions)
                {
                    faction.OfferedBounties.Add(otherFaction.Name);
                }
            }
        }

        _worldManager.Clear();
        _worldSimulator.Clear();
        _missionTracker.Clear();
        _standingTracker.Initialize(WorldDefinition.PlayerFaction);
        _crimeTracker.Clear();

        foreach(var faction in allFactions)
        {
            _worldManager.AddFaction(faction);
        }

        _playerData.PlayerLoadout = null;
        _playerData.PlayerLoadLoadout = null;
        _playerData.SpawnDatum = null;
        _playerData.PlayerSub = null;
        _playerData.PlayerController = null;
        _playerData.KnownTopics.Clear();
        _playerData.KnownPointsOfInterest.Clear();
        _playerData.SkipIntro = false;
    }

    public void ShowSettings()
    {
        _soundEffectPlayer.PlayUiClick();
        GetNode<Control>("MainMenuPanel").Visible = false;

        _settingsPopup.ShowWithCallback(() => GetNode<Control>("MainMenuPanel").Visible = true);
    }
}
