using Godot;
using Xenosea.Logic.Infrastructure.Data;

public class MapCurrentRepresentation : Spatial
{
    public WorldDatum Datum { get; set; }

    [Export]
    public Texture CurrentIconTexture;
    
    private MeshInstance _meshInstance;
    private SpatialMaterial _meshMaterial;

    public override void _Ready()
    {
        _meshMaterial = new SpatialMaterial
        {
            FlagsUnshaded = true,
            FlagsTransparent = true,
            AlbedoTexture = CurrentIconTexture
        };

        _meshInstance = GetNode<MeshInstance>("MeshInstance");

        _meshInstance.MaterialOverride = _meshMaterial;

        RefreshContent();
    }

    public void RefreshContent()
    {
        var currentStrength = Datum.CurrentFlow.Length();

        _meshMaterial.AlbedoColor = new Color(
            Mathf.Clamp(currentStrength / 5f, 0, 1),
            0,
            0);
    }
}
