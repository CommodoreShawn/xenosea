using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;

public class StormNoisePlayer : AudioStreamPlayer3D
{
    [Inject]
    private IPlayerData _playerData;

    private Gameplay _gameplay;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _gameplay = this.FindParent<Gameplay>();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_playerData.PlayerSub != null)
        {
            var position = _playerData.PlayerSub.RealPosition;

            var currentFactor = Mathf.Clamp(
                    (_playerData.PlayerSub.DistanceToGround - 100) / 300,
                    0, 1);

            var stormStrength = _gameplay.GetStormStrength(position) * currentFactor;

            if (stormStrength > 0)
            {
                if (!Playing)
                {
                    Playing = true;
                }

                MaxDb = (1 - Mathf.Clamp(stormStrength * currentFactor / 15f, 0, 1)) * 6;
            }
            else if (Playing)
            {
                Playing = false;
            }
        }
    }
}
