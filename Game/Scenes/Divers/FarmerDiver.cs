using CSPID;
using Godot;
using System.Diagnostics;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class FarmerDiver : RigidBody
{
    public FarmPlot Field { get; set; }

    [Export]
    public float Thrust = 1;
    [Export]
    public float Torque = 1;

    [Inject]
    private IPerftracker _perfTracker;

    private Particles _engineParticles;
    private PIDController _yawPid;
    private PIDController _pitchPid;
    private Vector3 _targetPoint;
    private bool _useOriginRelativeUp;
    private Vector3 _upVector;
    private float _lingerTimer;
    private bool _travelling;
    private System.Random _random;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _engineParticles = GetNode<Particles>("EngineParticles");

        _yawPid = new PIDController(
            minimumError: -4,
            maximumError: 4,
            minimumControl: -4,
            maximumControl: 4)
        {
            MaximumStep = double.MaxValue,
            ProportionalGain = 3,
            IntegralGain = 0,
            DerivativeGain = 0
        };

        _pitchPid = new PIDController(
            minimumError: -4,
            maximumError: 4,
            minimumControl: -4,
            maximumControl: 4)
        {
            MaximumStep = double.MaxValue,
            ProportionalGain = 3,
            IntegralGain = 0,
            DerivativeGain = 0
        };


        _useOriginRelativeUp = this.FindParent<GameplayRoot>().UseOriginRelativeUp;
        _upVector = Vector3.Up;
        _random = new System.Random();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_useOriginRelativeUp)
        {
            _upVector = GlobalTransform.origin.Normalized();
        }

        if(!IsInstanceValid(Field))
        {
            QueueFree();
        }
        else if(!_travelling)
        {
            _lingerTimer -= delta;

            if(_lingerTimer <= 0 && Field.CropPoints.Length > 0)
            {
                _targetPoint = Field.CropPoints[_random.Next(Field.CropPoints.Length)];
                _travelling = true;
            }
        }
        else
        {
            if(GlobalTransform.origin.DistanceTo(_targetPoint) < 5)
            {
                _travelling = false;
                _lingerTimer = (float)(_random.NextDouble() * 5);
            }
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        var stopwatch = Stopwatch.StartNew();
        base._PhysicsProcess(delta);

        if (delta == 0 || _targetPoint == Vector3.Zero)
        {
            return;
        }
        
        if (_travelling)
        {
            var targetVector = GlobalTransform.origin.DirectionTo(_targetPoint);
            var yaw = Transform.basis.Xform(Vector3.Back).SignedAngleTo(targetVector, Transform.basis.y);

            yaw = (float)_yawPid.Next(error: yaw, elapsed: delta);

            var pitch = Transform.basis.Xform(Vector3.Back).SignedAngleTo(targetVector, Transform.basis.x);

            pitch = (float)_pitchPid.Next(error: pitch, elapsed: delta);

            var currentBasis = Transform.basis;

            var xyPlane = new Plane(currentBasis.z, 0);
            var projectedY = xyPlane.Project(_upVector).Normalized();
            var roll = projectedY.AngleTo(currentBasis.y);
            roll *= Mathf.Sign(projectedY.Dot(currentBasis.x));

            AddTorque(currentBasis.Xform(new Vector3(
                pitch * Torque,
                yaw * Torque,
                -roll * Mass * 5)));

            AddCentralForce(Transform.basis.Xform(new Vector3(
                0,
                0,
                Thrust)));

            _engineParticles.Emitting = true;
        }
        else
        {
            _engineParticles.Emitting = false;
        }
        
        stopwatch.Stop();
        _perfTracker.LogPerf("FarmerDiver._PhysicsProcess", stopwatch.ElapsedMilliseconds);
    }
}
