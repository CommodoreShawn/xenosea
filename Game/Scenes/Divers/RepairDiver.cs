using Autofac;
using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;

public class RepairDiver : Diver
{
    private RepairDiverState _currentState;

    private float _postFixLingerTimer;
    private float _stuckTimer;

    private INotificationSystem _notificationSystem;

    private Vector3 _targetPoint;
    private Spatial _targetLeakBubbles;
    private Spatial _weldingEffect;
    

    public override void _Ready()
    {
        base._Ready();
        _notificationSystem = IocManager.IocContainer.Resolve<INotificationSystem>();

        _weldingEffect = GetNode<Spatial>("WeldArc");
        _weldingEffect.Visible = false;
    }

    protected override void DoProcessLogic(float delta)
    {
        if (_targetLeakBubbles != null)
        {
            _stuckTimer += delta;
        }

        if (_targetLeakBubbles != null && !IsInstanceValid(_targetLeakBubbles))
        {
            _targetLeakBubbles = null;
        }

        if(_targetLeakBubbles == null)
        {
            var targetedLeakBubbles = ParentSubmarine.DiveTeams
                .Select(dt => dt.DeployedDiver)
                .Where(d => d != null)
                .OfType<RepairDiver>()
                .Select(d => d._targetLeakBubbles)
                .Where(t => t != null)
                .ToArray();

            _targetLeakBubbles = ParentSubmarine.LeakBubbles
                .Except(targetedLeakBubbles)
                .OrderBy(t => t.GlobalTransform.origin.DistanceSquaredTo(GlobalTransform.origin))
                .FirstOrDefault();

            if(_targetLeakBubbles == null)
            {
                ReturnToHome();
            }
            else
            {
                _currentState = RepairDiverState.GOING_TO_ORBIT;
                _weldingEffect.Visible = false;
            }
        }
        else if(_currentState == RepairDiverState.GOING_TO_ORBIT)
        {
            var transformedPos = ParentSubmarine.GlobalTransform.XformInv(GlobalTransform.origin);
            var angle = Mathf.Atan2(transformedPos.y, transformedPos.x);
            
            _targetPoint = ParentSubmarine.Transform.Xform(
                new Vector3(
                    0.6f * ParentSubmarine.HintDiverRadius * Mathf.Cos(angle),
                    0.6f * ParentSubmarine.HintDiverRadius * Mathf.Sin(angle),
                    transformedPos.z));

            if (GlobalTransform.origin.DistanceTo(_targetPoint) <= 5)
            {
                _currentState = RepairDiverState.ORBITING;
                _stuckTimer = 0;
            }
        }
        else if (_currentState == RepairDiverState.ORBITING)
        {
            var transformedPos = ParentSubmarine.GlobalTransform.XformInv(GlobalTransform.origin);
            var ownAngle = Mathf.Atan2(transformedPos.y, transformedPos.x);
            var destinationAngle = ownAngle;

            var destination = _targetLeakBubbles.GlobalTransform.origin;
            var targetPos = ParentSubmarine.GlobalTransform.XformInv(destination);
            var targetAngle = Mathf.Atan2(targetPos.y, targetPos.x);

            if(ownAngle > targetAngle)
            {
                destinationAngle -= 0.1f;
            }
            else
            {
                destinationAngle += 0.1f;
            }

            _targetPoint = ParentSubmarine.Transform.Xform(new Vector3(
                0.6f * ParentSubmarine.HintDockingWidth * Mathf.Cos(destinationAngle),
                0.6f * ParentSubmarine.HintDockingWidth * Mathf.Sin(destinationAngle),
                transformedPos.z));

            if (Mathf.Abs(ownAngle - targetAngle) < 0.1)
            {
                _currentState = RepairDiverState.ORBITAL_TRAVEL;
                _stuckTimer = 0;
            }
        }
        else if (_currentState == RepairDiverState.ORBITAL_TRAVEL)
        {
            var destination = _targetLeakBubbles.GlobalTransform.origin;

            var transformedPos = ParentSubmarine.GlobalTransform.XformInv(destination);
            var angle = Mathf.Atan2(transformedPos.y, transformedPos.x);

            _targetPoint = ParentSubmarine.Transform.Xform(new Vector3(
                0.6f * ParentSubmarine.HintDockingWidth * Mathf.Cos(angle),
                0.6f * ParentSubmarine.HintDockingWidth * Mathf.Sin(angle),
                transformedPos.z));

            if (GlobalTransform.origin.DistanceTo(_targetPoint) <= 5)
            {
                _currentState = RepairDiverState.TRAVELLING_FROM_ORBIT;
                _stuckTimer = 0;
            }
        }
        else if (_currentState == RepairDiverState.TRAVELLING_FROM_ORBIT)
        {
            _targetPoint = _targetLeakBubbles.GlobalTransform.origin;

            if (GlobalTransform.origin.DistanceTo(_targetPoint) <= 5 || _stuckTimer > 10)
            {
                if (_targetLeakBubbles != null)
                {
                    _currentState = RepairDiverState.REPAIRING;
                    _stuckTimer = 0;
                }
                else
                {
                    ReturnToHome();
                    _stuckTimer = 0;
                }
            }
        }
        else if (_currentState == RepairDiverState.REPAIRING)
        {
            _weldingEffect.Visible = true;

            ParentSubmarine.Health = Mathf.Min(ParentSubmarine.MaximumHealth, ParentSubmarine.Health + delta * StationRepairDiver.REPAIR_RATE);

            if (ParentSubmarine.LeakBubbles.Count > ParentSubmarine.DetermineTargetLeakCount())
            {
                _currentState = RepairDiverState.LINGERING;
                _postFixLingerTimer = 4;
                _targetLeakBubbles.GetNode<Particles>("Particles").Emitting = false;
            }
        }
        else if (_currentState == RepairDiverState.LINGERING)
        {
            _postFixLingerTimer -= delta;

            if (_postFixLingerTimer <= 0)
            {
                ParentSubmarine.LeakBubbles.Remove(_targetLeakBubbles);
                _targetLeakBubbles.QueueFree();
                _targetLeakBubbles = null;
                _weldingEffect.Visible = false;
            }
        }
    }

    protected override Vector3? GetSteerDirection(float delta)
    {
        if(_targetLeakBubbles == null
            || _targetLeakBubbles.GlobalTransform.origin.DistanceTo(GlobalTransform.origin) <= 1)
        {
            return null;
        }

        return GlobalTransform.origin.DirectionTo(_targetPoint);
    }

    public override void ReturnToHome()
    {
        base.ReturnToHome();
        Visible = true;
        _weldingEffect.Visible = false;
    }

    protected override void ReturnedToParentSub()
    {
    }

    public override void Collided(Node other)
    {
    }

    public override void SetTarget(ITargetableThing target)
    {
    }

    private enum RepairDiverState
    {
        GOING_TO_ORBIT,
        ORBITING,
        ORBITAL_TRAVEL,
        TRAVELLING_FROM_ORBIT,
        REPAIRING,
        LINGERING
    }
}
