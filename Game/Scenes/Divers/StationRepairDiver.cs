using CSPID;
using Godot;
using System.Diagnostics;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class StationRepairDiver : RigidBody
{
    public static readonly float REPAIR_RATE = 50;

    public Submarine Submarine { get; set; }
    public DockingPoint DockPoint { get; internal set; }
    public Spatial TargetLeakBubbles { get; set; }

    [Export]
    public float Thrust = 1;
    [Export]
    public float Torque = 1;

    [Inject]
    private IPerftracker _perfTracker;

    private Spatial _weldingEffect;
    private Particles _engineParticles;
    private bool _goingHome;
    private bool _onApproach;
    private bool _lingering;
    private PIDController _yawPid;
    private PIDController _pitchPid;
    private Vector3 _targetPoint;
    private float _postFixLingerTimer;
    private float _stuckTimer;
    private bool _useOriginRelativeUp;
    private Vector3 _upVector;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _weldingEffect = GetNode<Spatial>("WeldArc");
        _engineParticles = GetNode<Particles>("EngineParticles");
        _weldingEffect.Visible = false;

        _yawPid = new PIDController(
            minimumError: -4,
            maximumError: 4,
            minimumControl: -4,
            maximumControl: 4)
        {
            MaximumStep = double.MaxValue,
            ProportionalGain = 3,
            IntegralGain = 0,
            DerivativeGain = 0
        };

        _pitchPid = new PIDController(
            minimumError: -4,
            maximumError: 4,
            minimumControl: -4,
            maximumControl: 4)
        {
            MaximumStep = double.MaxValue,
            ProportionalGain = 3,
            IntegralGain = 0,
            DerivativeGain = 0
        };


        _useOriginRelativeUp = this.FindParent<GameplayRoot>().UseOriginRelativeUp;
        _upVector = Vector3.Up;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(!IsInstanceValid(DockPoint))
        {
            QueueFree();
            return;
        }

        if (_useOriginRelativeUp)
        {
            _upVector = GlobalTransform.origin.Normalized();
        }
        
        if (!_goingHome)
        {
            if (!IsInstanceValid(Submarine) || Submarine.IsDockedTo == null)
            {
                _weldingEffect.Visible = false;
                _goingHome = true;
            }
        }

        if (_goingHome)
        {
            _targetPoint = DockPoint.GlobalTransform.origin;

            if (GlobalTransform.origin.DistanceTo(_targetPoint) < 1)
            {
                DockPoint.RepairDivers.Remove(this);
                QueueFree();
            }
        }
        else if (TargetLeakBubbles == null)
        {
            TargetLeakBubbles = Submarine.LeakBubbles
                .Where(x => !DockPoint.RepairDivers.Any(d => d.TargetLeakBubbles == x))
                .FirstOrDefault();

            if (TargetLeakBubbles == null)
            {
                _goingHome = true;
                _weldingEffect.Visible = false;
            }
            else
            {
                _stuckTimer = 0;
                _onApproach = true;
                _targetPoint = TargetLeakBubbles.GlobalTransform.origin
                    + Submarine.GlobalTransform.origin.DirectionTo(TargetLeakBubbles.GlobalTransform.origin) * 4;
                _weldingEffect.Visible = false;
            }
        }
        else
        {
            _stuckTimer += delta;
            
            if (_onApproach)
            {
                if (GlobalTransform.origin.DistanceTo(_targetPoint) < 1)
                {
                    _targetPoint = TargetLeakBubbles.GlobalTransform.origin;
                    _onApproach = false;
                }
            }
            else if (GlobalTransform.origin.DistanceTo(_targetPoint) < 1.5 || _stuckTimer > 20)
            {
                _weldingEffect.Visible = true;

                Submarine.Health = Mathf.Min(Submarine.MaximumHealth, Submarine.Health + delta * REPAIR_RATE);

                if (_lingering)
                {
                    _postFixLingerTimer -= delta;

                    if (_postFixLingerTimer <= 0)
                    {
                        Submarine.LeakBubbles.Remove(TargetLeakBubbles);
                        TargetLeakBubbles.QueueFree();
                        TargetLeakBubbles = null;
                        _weldingEffect.Visible = false;
                        _lingering = false;
                    }
                }
                else if (Submarine.LeakBubbles.Count > Submarine.DetermineTargetLeakCount())
                {
                    _lingering = true;
                    _postFixLingerTimer = 4;
                    TargetLeakBubbles.GetNode<Particles>("Particles").Emitting = false;
                }
            }
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        var stopwatch = Stopwatch.StartNew();
        base._PhysicsProcess(delta);

        if (delta == 0 || _targetPoint == Vector3.Zero)
        {
            return;
        }
        
        var targetVector = GlobalTransform.origin.DirectionTo(_targetPoint);

        if ((_onApproach || _goingHome) && Submarine != null && IsInstanceValid(Submarine))
        {
            var collisionInfo = GetWorld().DirectSpaceState
                .IntersectRay(
                    GlobalTransform.origin,
                    Submarine.GlobalTransform.origin,
                    null,
                    0b00000000000000000001);

            if(collisionInfo.Count > 0)
            {
                var hitPoint = (Vector3)collisionInfo["position"];

                if(GlobalTransform.origin.DistanceTo(hitPoint) < 3)
                {
                    targetVector += hitPoint.DirectionTo(GlobalTransform.origin);
                }
            }
        }


        if (_onApproach || GlobalTransform.origin.DistanceTo(_targetPoint) > 1)
        {
            AddCentralForce(Transform.basis.Xform(new Vector3(
                0,
                0,
                Thrust)));

            _engineParticles.Emitting = true;
        }
        else
        {
            _engineParticles.Emitting = false;
        }

        var yaw = Transform.basis.Xform(Vector3.Back).SignedAngleTo(targetVector, Transform.basis.y);

        yaw = (float)_yawPid.Next(error: yaw, elapsed: delta);

        var pitch = Transform.basis.Xform(Vector3.Back).SignedAngleTo(targetVector, Transform.basis.x);

        pitch = (float)_pitchPid.Next(error: pitch, elapsed: delta);

        var currentBasis = Transform.basis;

        var xyPlane = new Plane(currentBasis.z, 0);
        var projectedY = xyPlane.Project(_upVector).Normalized();
        var roll = projectedY.AngleTo(currentBasis.y);
        roll *= Mathf.Sign(projectedY.Dot(currentBasis.x));

        AddTorque(currentBasis.Xform(new Vector3(
            pitch * Torque,
            yaw * Torque,
            -roll * Mass * 5)));

        stopwatch.Stop();
        _perfTracker.LogPerf("RepairDiver._PhysicsProcess", stopwatch.ElapsedMilliseconds);
    }
}
