using Autofac;
using CSPID;
using Godot;
using System.Diagnostics;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;

public abstract class Diver : RigidBody
{
    [Export]
    public float Thrust = 4;
    [Export]
    public float Torque = 2;
    [Export]
    public float MaxDiveTime = 120;
    
    public float DiveTimeLeft { get; set; }

    private IPerftracker _perfTracker;
    private GameplayRoot _gameplayRoot;

    public Submarine ParentSubmarine { get; set; }
    public bool DiveTimeLow => DiveTimeLeft < MaxDiveTime * 0.5f;
    public bool IsReturning => _currentState != DiverState.WORKING;
    private Particles _engineParticles;

    private PIDController _yawPid;
    private PIDController _pitchPid;
    private bool _useOriginRelativeUp;
    private Vector3 _upVector;
    private DiverState _currentState;

    public override void _Ready()
    {
        _perfTracker = IocManager.IocContainer.Resolve<IPerftracker>();
        _currentState = DiverState.WORKING;

        _gameplayRoot = this.FindParent<GameplayRoot>();

        _yawPid = new PIDController(
            minimumError: -4,
            maximumError: 4,
            minimumControl: -4,
            maximumControl: 4)
        {
            MaximumStep = double.MaxValue,
            ProportionalGain = 1,
            IntegralGain = 0,
            DerivativeGain = 0
        };

        _pitchPid = new PIDController(
            minimumError: -4,
            maximumError: 4,
            minimumControl: -4,
            maximumControl: 4)
        {
            MaximumStep = double.MaxValue,
            ProportionalGain = 1,
            IntegralGain = 0,
            DerivativeGain = 0
        };

        _engineParticles = GetNode<Particles>("EngineParticles");
        DiveTimeLeft = MaxDiveTime;
        _useOriginRelativeUp = _gameplayRoot.UseOriginRelativeUp;
        _upVector = Vector3.Up;
    }

    public abstract void SetTarget(ITargetableThing target);

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_useOriginRelativeUp)
        {
            _upVector = GlobalTransform.origin.Normalized();
        }

        DiveTimeLeft -= delta;

        if(!IsReturning && DiveTimeLow)
        {
            ReturnToHome();
        }

        if(IsReturning)
        {
            if(GlobalTransform.origin.DistanceTo(ParentSubmarine.DiveHatch.GlobalTransform.origin) < 1)
            {
                ReturnedToParentSub();
                QueueFree();
            }
        }
        else
        {
            DoProcessLogic(delta);
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        var stopwatch = Stopwatch.StartNew();
        base._PhysicsProcess(delta);

        if (delta == 0 || ! IsInstanceValid(ParentSubmarine))
        {
            return;
        }

        if (_gameplayRoot is Gameplay gameplay)
        {
            var currentFactor = 1f;

            var collisionInfo = GetWorld().DirectSpaceState
                .IntersectRay(
                    GlobalTransform.origin,
                    GlobalTransform.origin + _upVector * -1000,
                    null,
                    0b00000000000000000010,
                    collideWithBodies: true,
                    collideWithAreas: true);

            if (collisionInfo.Count > 0)
            {
                currentFactor = Mathf.Clamp(
                    (GlobalTransform.origin.DistanceTo((Vector3)collisionInfo["position"]) - 100) / 300,
                    0, 1);
            }

            AddCentralForce(gameplay.GetCurrentFlow(GlobalTransform.origin) * Mass * currentFactor);
        }

        Vector3? steerDirection;

        if (_currentState == DiverState.GOING_TO_ORBIT)
        {
            var transformedPos = ParentSubmarine.GlobalTransform.XformInv(GlobalTransform.origin);
            var angle = Mathf.Atan2(transformedPos.y, transformedPos.x);

            var targetPoint = ParentSubmarine.Transform.Xform(
                new Vector3(
                    1.2f * ParentSubmarine.HintDiverRadius * Mathf.Cos(angle),
                    1.2f * ParentSubmarine.HintDiverRadius * Mathf.Sin(angle),
                    transformedPos.z));

            if (GlobalTransform.origin.DistanceTo(targetPoint) <= 5)
            {
                _currentState = DiverState.ORBITING;
            }

            steerDirection = GlobalTransform.origin.DirectionTo(targetPoint);
        }
        else if (_currentState == DiverState.ORBITING)
        {
            var transformedPos = ParentSubmarine.GlobalTransform.XformInv(GlobalTransform.origin);
            var ownAngle = Mathf.Atan2(transformedPos.y, transformedPos.x);
            var destinationAngle = ownAngle;

            var destination = ParentSubmarine.DiveHatch.GlobalTransform.origin;
            var targetPos = ParentSubmarine.GlobalTransform.XformInv(destination);
            var targetAngle = Mathf.Atan2(targetPos.y, targetPos.x);

            if (ownAngle > targetAngle)
            {
                destinationAngle -= 0.1f;
            }
            else
            {
                destinationAngle += 0.1f;
            }

            var targetPoint = ParentSubmarine.Transform.Xform(new Vector3(
                1.2f * ParentSubmarine.HintDiverRadius * Mathf.Cos(destinationAngle),
                1.2f * ParentSubmarine.HintDiverRadius * Mathf.Sin(destinationAngle),
                transformedPos.z));

            if (Mathf.Abs(ownAngle - targetAngle) < 0.1)
            {
                _currentState = DiverState.ORBITAL_TRAVEL;
            }

            steerDirection = GlobalTransform.origin.DirectionTo(targetPoint);
        }
        else if (_currentState == DiverState.ORBITAL_TRAVEL)
        {
            var destination = ParentSubmarine.DiveHatch.GlobalTransform.origin;

            var transformedPos = ParentSubmarine.GlobalTransform.XformInv(destination);
            var angle = Mathf.Atan2(transformedPos.y, transformedPos.x);

            var targetPoint = ParentSubmarine.Transform.Xform(
                new Vector3(
                    1.2f * ParentSubmarine.HintDiverRadius * Mathf.Cos(angle),
                    1.2f * ParentSubmarine.HintDiverRadius * Mathf.Sin(angle),
                    transformedPos.z));

            if (GlobalTransform.origin.DistanceTo(targetPoint) <= 5)
            {
                _currentState = DiverState.TRAVELLING_FROM_ORBIT;
                AddCollisionExceptionWith(ParentSubmarine);
            }

            steerDirection = GlobalTransform.origin.DirectionTo(targetPoint);
        }
        else if (_currentState == DiverState.TRAVELLING_FROM_ORBIT)
        {
            steerDirection = GlobalTransform.origin.DirectionTo(ParentSubmarine.DiveHatch.GlobalTransform.origin);
        }
        else
        {
            steerDirection = GetSteerDirection(delta);
        }

        if (steerDirection != null)
        {
            AddCentralForce(Transform.basis.Xform(new Vector3(
                0,
                0,
                Thrust)));

            _engineParticles.Emitting = true;

            var yaw = Transform.basis.Xform(Vector3.Back).SignedAngleTo(steerDirection.Value, Transform.basis.y);

            yaw = (float)_yawPid.Next(error: yaw, elapsed: delta);

            var pitch = Transform.basis.Xform(Vector3.Back).SignedAngleTo(steerDirection.Value, Transform.basis.x);

            pitch = (float)_pitchPid.Next(error: pitch, elapsed: delta);

            var currentBasis = Transform.basis;

            var xyPlane = new Plane(currentBasis.z, 0);
            var projectedY = xyPlane.Project(_upVector).Normalized();
            var roll = projectedY.AngleTo(currentBasis.y);
            roll *= Mathf.Sign(projectedY.Dot(currentBasis.x));

            AddTorque(currentBasis.Xform(new Vector3(
                pitch * Torque,
                yaw * Torque,
                -roll * Mass * 5)));
        }
        else
        {
            _engineParticles.Emitting = false;
        }

        stopwatch.Stop();
        _perfTracker.LogPerf("Diver._PhysicsProcess", stopwatch.ElapsedMilliseconds);
    }

    public virtual void ReturnToHome()
    {
        _currentState = DiverState.GOING_TO_ORBIT;
    }

    protected Vector3? AdjustSteerForStandoff(Spatial objectToAvoid, Vector3? steerDirection)
    {
        if(steerDirection == null)
        {
            return null;
        }

        var collisionInfo = GetWorld().DirectSpaceState
            .IntersectRay(
                GlobalTransform.origin,
                //objectToAvoid.GlobalTransform.origin,
                GlobalTransform.origin - GlobalTransform.basis.z * 5,
                null,
                0b00000000000000000001);

        if (collisionInfo.Count > 0)
        {
            var hitPoint = (Vector3)collisionInfo["position"];

            if (GlobalTransform.origin.DistanceTo(hitPoint) < 5)
            {
                steerDirection -= hitPoint.DirectionTo(GlobalTransform.origin);
            }
        }

        return steerDirection;
    }

    protected abstract Vector3? GetSteerDirection(float delta);

    protected abstract void ReturnedToParentSub();

    protected abstract void DoProcessLogic(float delta);

    public abstract void Collided(Node other);

    private enum DiverState
    {
        WORKING,
        GOING_TO_ORBIT,
        ORBITING,
        ORBITAL_TRAVEL,
        TRAVELLING_FROM_ORBIT
    }
}
