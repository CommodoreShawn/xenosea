﻿using Autofac;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

public class SalvageDiver : Diver
{
    [Export]
    public int CarryCapacity = 10;
    [Export]
    public float LootTimePerCommodity = 0.2f;

    public List<CommodityType> CarriedLoot { get; private set; }
    
    public ISalvageTarget Target { get; set; }
    private bool _isLooting;
    private float _lootingTimer;
    private Random _random;
    private Spatial _box;
    private ISalvageTarget _carryingLootFrom;

    private INotificationSystem _notificationSystem;
    private ISimulationEventBus _simulationEventBus;
    private bool _useOriginRelativeUp;
    private Vector3 _upVector;

    public override void _Ready()
    {
        base._Ready();
        _notificationSystem = IocManager.IocContainer.Resolve<INotificationSystem>();
        _simulationEventBus = IocManager.IocContainer.Resolve<ISimulationEventBus>();
        CarriedLoot = new List<CommodityType>();
        _random = new Random();
        _box = GetNode<Spatial>("CargoBox");
        _box.Visible = false;
        _useOriginRelativeUp = this.FindParent<GameplayRoot>().UseOriginRelativeUp;
        _upVector = Vector3.Up;
    }

    protected override void DoProcessLogic(float delta)
    {
        if (_useOriginRelativeUp)
        {
            _upVector = GlobalTransform.origin.Normalized();
        }

        if (Target == null || (Target is Godot.Object gobj && !IsInstanceValid(gobj)))
        {
            ReturnToHome();
        }

        if (_isLooting)
        {
            _lootingTimer += delta;

            if (_lootingTimer >= LootTimePerCommodity)
            {
                _lootingTimer = 0;

                var commodityOptions = Target.CommodityStorage.CommoditiesPresent
                    .Where(c => Target.CommodityStorage.GetQty(c) >= 1)
                    .ToArray();

                if(commodityOptions.Any())
                {
                    var commodityToTake = commodityOptions[_random.Next(commodityOptions.Length)];
                    Target.CommodityStorage.Remove(commodityToTake, 1);
                    CarriedLoot.Add(commodityToTake);
                    _box.Visible = true;
                    _carryingLootFrom = Target;
                }
                else
                {
                    RotateObjectLocal(_upVector, Mathf.Pi);
                    ReturnToHome();
                }

                if(CarriedLoot.Count >= CarryCapacity)
                {
                    RotateObjectLocal(_upVector, Mathf.Pi);
                    ReturnToHome();
                }
            }
        }
    }

    protected override Vector3? GetSteerDirection(float delta)
    {
        if(_isLooting || Target == null || (Target is Godot.Object gobj && !IsInstanceValid(gobj)))
        {
            return null;
        }

        var steerDirection = GlobalTransform.origin.DirectionTo(Target.RealPosition);
        steerDirection = AdjustSteerForStandoff(ParentSubmarine, steerDirection).Value;
        return steerDirection;
    }

    public override void ReturnToHome()
    {
        base.ReturnToHome();
        Visible = true;
        _isLooting = false;
        CollisionMask = 0b00000000000000000001;
    }

    protected override void ReturnedToParentSub()
    {
        foreach(var commodity in CarriedLoot)
        {
            if (ParentSubmarine.CommodityStorage.RemainingCapacity(commodity) > 0)
            {
                ParentSubmarine.CommodityStorage.Add(commodity, 1);
            }
        }

        if(CarriedLoot.Any() && ParentSubmarine.IsPlayerSub)
        {
            _notificationSystem.AddNotification(new Notification
            {
                IsGood = true,
                Body = $"Retrieved {string.Join(", ", CarriedLoot.Select(x => x.Name).Distinct())}"
            });
        }

        if(_carryingLootFrom != null && ParentSubmarine.IsPlayerSub)
        {
            _simulationEventBus.SendEvent(new PlayerSalvagedSimulationEvent(_carryingLootFrom, CarriedLoot));
        }

        _carryingLootFrom = null;
    }

    public override void Collided(Node other)
    {
        if(other == Target && !IsReturning)
        {
            _isLooting = true;
            _lootingTimer = 0;
            Visible = false;
            CollisionMask = 0b00000000000000000000;
        }
    }

    public override void SetTarget(ITargetableThing target)
    {
        if (target is ISalvageTarget salvageTarget)
        {
            Target = salvageTarget;
        }
        else if (target is SonarContactRecord contactRecord
            && contactRecord.SonarContact is ISalvageTarget salvageTarget1)
        {
            Target = salvageTarget1;
        }
        else
        {
            ReturnToHome();
        }
    }
}
