using Godot;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

public class MapObjectRepresentation : Spatial
{
    public IMapObject MapObject { get; set; }
    public bool WasTypeKnown { get; set; }

    [Export]
    public Texture StationIconTexture;
    [Export]
    public Texture SubmarineIconTexture;
    [Export]
    public Texture WreckIconTexture;
    [Export]
    public Texture WaypointIconTexture;
    [Export]
    public Texture UnknownIconTexture;
    [Export]
    public Texture PlayersubIconTexture;
    [Export]
    public Texture LandmarkIconTexture;
    [Export]
    public Texture TorpedoIconTexture;

    private MeshInstance _meshInstance;
    private SpatialMaterial _meshMaterial;

    public IPlayerData PlayerData { get; set; }

    public override void _Ready()
    {
        _meshMaterial = new SpatialMaterial
        {
            FlagsUnshaded = true,
            FlagsTransparent = true
        };

        _meshInstance = GetNode<MeshInstance>("MeshInstance");

        _meshInstance.MaterialOverride = _meshMaterial;

        RefreshContent();
    }

    public void RefreshContent()
    {
        if (MapObject == null)
        {
            return;
        }

        if(MapObject.ReferenceId == "playersub")
        {
            _meshMaterial.AlbedoTexture = PlayersubIconTexture;
            _meshMaterial.AlbedoColor = Colors.Blue;
        }
        else if (MapObject.TypeKnown)
        {
            _meshMaterial.AlbedoColor = Colors.White;
            switch (MapObject.ContactType)
            {
                case ContactType.Station:
                    _meshMaterial.AlbedoTexture = StationIconTexture;
                    break;
                case ContactType.Sub:
                case ContactType.Minisub:
                    _meshMaterial.AlbedoTexture = SubmarineIconTexture;
                    break;
                case ContactType.Wreck:
                    _meshMaterial.AlbedoTexture = WreckIconTexture;
                    break;
                case ContactType.Waypoint:
                    _meshMaterial.AlbedoTexture = WaypointIconTexture;
                    _meshMaterial.AlbedoColor = Colors.Purple;
                    break;
                case ContactType.Landmark:
                    _meshMaterial.AlbedoTexture = LandmarkIconTexture;
                    break;
                case ContactType.Torpedo:
                    _meshMaterial.AlbedoTexture = TorpedoIconTexture;
                    break;
            }

            if(MapObject.Identified)
            {
                if (MapObject.IsFriendlyTo(PlayerData.PlayerSub))
                {
                    _meshMaterial.AlbedoColor = Colors.Green;
                }
                else if(MapObject.IsHostileTo(PlayerData.PlayerSub))
                {
                    _meshMaterial.AlbedoColor = Colors.Red;
                }
            }
        }
        else
        {
            _meshMaterial.AlbedoColor = Colors.White;
            _meshMaterial.AlbedoTexture = UnknownIconTexture;
        }
    }
}
