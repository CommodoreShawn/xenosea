﻿using Godot;

public class StationRoom : Spatial
{
    [Export]
    public string FriendlyName;

    [Export]
    public string Description;

    [Export]
    public PackedScene StationRoomWidgetPrefab;

    [Export]
    public Npc[] NpcsPresent = new Npc[0];
}