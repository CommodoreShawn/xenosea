﻿using Godot;
using System.Collections.Generic;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Docking;

public interface IStation: ISonarContact, ICombatant
{
    List<TurretSystem> TurretSystems { get; }
    List<TorpedoTube> TorpedoTubes { get; }
    List<MinisubHangar> MinisubHangars { get; }
    CommodityStorage CommodityStorage { get; }
    List<SonarContactRecord> SonarContactRecords { get; }
    Spatial DockWaitingArea { get; }
    Spatial CameraCenter { get; }
    int CommodityCapacity { get; }
    List<StationRoom> StationRooms { get; }
    IEnumerable<DockingPoint> DockingPoints { get; }

    float MinCameraDistance { get; }
    float MaxCameraDistance { get; }

    DockingSequence RequestDocking(Submarine submarine);
    void CancelDocking(DockingSequence dockingSequence);
    void Broadcast(string message);
    float PriceOf(CommodityType commodity);
}