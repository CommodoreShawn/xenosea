using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.Infrastructure.Missions;
using Xenosea.Logic.SimulationEvents;
using Xenosea.Resources;
using Xenosea.Scenes.Ships;

public class GameplayUi : Control
{
    [Export]
    public PackedScene TurretAimingWidget;
    [Export]
    public PackedScene TurretControlRowPrefab;
    [Export]
    public PackedScene TorpedoControlRowPrefab;
    [Export]
    public PackedScene MinisubHangarWidget;
    [Export]
    public PackedScene ContactListItemPrefab;
    [Export]
    public PackedScene SonarContactWidgetPrefab;
    [Export]
    public PackedScene NotificationItemPrefab;
    [Export]
    public PackedScene StationRoomButtonPrefab;
    [Export]
    public Texture TypeIconUnknown;
    [Export]
    public Texture TypeIconSub;
    [Export]
    public Texture TypeIconStation;
    [Export]
    public Texture TypeIconOutpost;
    [Export]
    public Texture TypeIconWreck;
    [Export]
    public Texture TypeIconWaypoint;
    [Export]
    public Texture TypeIconTorpedo;
    [Export]
    public PackedScene MapscreenPrefab;

    [Inject]
    private ISonarContactTracker _sonarContactTracker;
    [Inject]
    private INotificationSystem _notificationSystem;
    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IPerftracker _perfTracker;
    [Inject]
    private IWorldManager _worldManager;
    [Inject]
    private IMissionTracker _missionTracker;
    [Inject]
    private ISimulationEventBus _simulationEventBus;
    [Inject]
    private IGameSerializer _gameSerializer;
    [Inject]
    private ISettingsService _settingsService;
    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    public SonarContactWidget TrackingContact { get; set; }
    public IWeaponSystem ActiveWeaponSystem { get; set; }
    public ITargetableThing CurrentTarget { get; set; }
    public bool HideForOtherUi;

    // Sensor Panel
    private Control _sensorPanel;
    private Label _selfNoiseLabel;
    private Label _selfDetectLabel;
    private Button _towedArrayDeployButton;
    private Label _towedArrayStatusLabel;
    private TextureButton _filterUnknownButton;
    private TextureButton _filterSubsButton;
    private TextureButton _filterStationsButton;
    private TextureButton _filterBastionsButton;
    private TextureButton _filterWrecksButton;
    private TextureButton _filterTorpedoesButton;
    private VBoxContainer _contactList;

    // Manuver Panel
    private Control _manuverPanel;
    private TextureProgress _healthBar;
    private Button _autopilotGotoButton;
    private Button _autopilotDockButton;
    private Button _autopilotFollowButton;
    private Button _autopilotCancelButton;
    private Button _throttleDownButton;
    private Label _throttleLabel;
    private Button _throttleUpButton;
    private Label _speedLabel;
    private Button _diveDownButton;
    private Label _diveLabel;
    private Button _diveUpButton;
    private Label _depthLabel;
    private Button _decoyDropButton;
    private Label _decoyLoadProgress;
    private Button _weaponSafetiesButton;
    private VBoxContainer _weaponControlBox;

    // Button Panel
    private Label _dateTimeLabel;
    private Button _time1;
    private Button _time2;
    private Button _time4;
    private Label _playerWalletLabel;

    // Docked Status Panel
    private Control _dockedStatusPanel;
    private Label _dockedStatusLabel;
    private Label _dockedStatusNameLabel;

    // Hangar Panel
    private HBoxContainer _hangarPanel;

    // Station Name Panel
    private Control _stationNamePanel;
    private Label _stationNameLabel;
    private Label _stationFactionLabel;

    private Control _stationRoomButtonBox;

    private Control _mapPanel;

    // Hud Indicators
    private Control _aimingMask;
    private Label _freelookIndicator;
    private Label _lockedAimingHint;
    private Label _torpedoLockedStatusLabel;
    private Label _torpedoReloadingStatusLabel;
    private Label _torpedoOnWireStatusLabel;
    private Label _torpedoReadyStatusLabel;
    private Label _torpedoSeekingStatusLabel;
    private Label _ammoTypeLabel;
    private Label _ammoRangeLabel;
    private Label _torpedoAltitudeLabel;
    private List<TurretAimingWidget> _turretAimingWidgets;
    private Dictionary<ITargetableThing, SonarContactWidget> _sonarContactWidgets;
    private TextureRect _steerIndicator;
    private VBoxContainer _notificationBox;

    // Mission Info
    private Label _missionInfoLabel;

    // Popups
    private EscapeMenuPopup _escapeMenuPopup;
    private ProgressPopup _progressPopup;
    private StationRoomPopup _stationRoomPopup;
    private StandingsPopup _standingsPopup;
    private LoadoutPopup _loadoutPopup;
    private ConfirmationDialog _quitConfirmDialog;

    private Control _indicatorContainer;
    private AudioStreamPlayer _notificationPlayer;
    private SubCamera _camera;
    private GameplayRoot _gameplayRoot;
    private bool _towedArrayWasDamaged;
    private Submarine _oldPlayerSub;
    private List<TurretMount> _turretMounts;
    private IStation _wasDockedTo;
    private bool _hideToggle;

    public override void _Ready()
    {
        this.ResolveDependencies();


        _sensorPanel = GetNode<Control>("SensorPanel");
        _selfNoiseLabel = GetNode<Label>("SensorPanel/SelfNoiseLabel");
        _selfDetectLabel = GetNode<Label>("SensorPanel/SelfDetectLabel");
        _towedArrayDeployButton = GetNode<Button>("SensorPanel/HBoxContainer6/TowedArrayDeployButton");
        _towedArrayStatusLabel = GetNode<Label>("SensorPanel/HBoxContainer6/TowedArrayStatusLabel");
        _filterUnknownButton = GetNode<TextureButton>("SensorPanel/HBoxContainer/FilterUnknownButton");
        _filterSubsButton = GetNode<TextureButton>("SensorPanel/HBoxContainer/FilterSubsButton");
        _filterStationsButton = GetNode<TextureButton>("SensorPanel/HBoxContainer/FilterStationsButton");
        _filterBastionsButton = GetNode<TextureButton>("SensorPanel/HBoxContainer/FilterBastionsButton");
        _filterWrecksButton = GetNode<TextureButton>("SensorPanel/HBoxContainer/FilterWrecksButton");
        _filterTorpedoesButton = GetNode<TextureButton>("SensorPanel/HBoxContainer/FilterTorpedoesButton");
        _contactList = GetNode<VBoxContainer>("SensorPanel/ScrollContainer/VBoxContainer");

        _manuverPanel = GetNode<Control>("ManuverPanel");
        _healthBar = GetNode<TextureProgress>("ManuverPanel/HealthBar");
        _autopilotGotoButton = GetNode<Button>("ManuverPanel/HBoxContainer/GotoButton");
        _autopilotDockButton = GetNode<Button>("ManuverPanel/HBoxContainer/DockButton");
        _autopilotFollowButton = GetNode<Button>("ManuverPanel/HBoxContainer/FollowButton");
        _autopilotCancelButton = GetNode<Button>("ManuverPanel/HBoxContainer/CancelButton");
        _throttleDownButton = GetNode<Button>("ManuverPanel/GridContainer/ThrottleDownButton");
        _throttleLabel = GetNode<Label>("ManuverPanel/GridContainer/ThrottleLabel");
        _throttleUpButton = GetNode<Button>("ManuverPanel/GridContainer/ThrottleUpButton");
        _speedLabel = GetNode<Label>("ManuverPanel/GridContainer/CurrentSpeedLabel");
        _diveDownButton = GetNode<Button>("ManuverPanel/GridContainer/DiveDownButton");
        _diveLabel=GetNode<Label>("ManuverPanel/GridContainer/DiveLabel");
        _diveUpButton = GetNode<Button>("ManuverPanel/GridContainer/DiveUpButton");
        _depthLabel= GetNode<Label>("ManuverPanel/GridContainer/DepthLabel");
        _decoyDropButton = GetNode<Button>("ManuverPanel/HBoxContainer2/DecoyButton");
        _decoyLoadProgress = GetNode<Label>("ManuverPanel/HBoxContainer2/DecoyReload");
        _weaponSafetiesButton = GetNode<Button>("ManuverPanel/HBoxContainer3/WeaponSaftiesButton");
        _weaponControlBox = GetNode<VBoxContainer>("ManuverPanel/WeaponControlBox");

        _dateTimeLabel = GetNode<Label>("TimePanel/DateTimeLabel");
        _time1 = GetNode<Button>("TimePanel/HBoxContainer2/Time1");
        _time2 = GetNode<Button>("TimePanel/HBoxContainer2/Time2");
        _time4 = GetNode<Button>("TimePanel/HBoxContainer2/Time4");

        _playerWalletLabel = GetNode<Label>("ButtonPanel/VBoxContainer/PlayerWalletLabel");

        _dockedStatusPanel = GetNode<Control>("DockedStatusPanel");
        _dockedStatusLabel = GetNode<Label>("DockedStatusPanel/VBoxContainer/StatusLabel");
        _dockedStatusNameLabel = GetNode<Label>("DockedStatusPanel/VBoxContainer/SubNameLabel");

        _hangarPanel = GetNode<HBoxContainer>("HangarPanels");

        _stationNamePanel = GetNode<Control>("StationNamePanel");
        _stationNameLabel = GetNode<Label>("StationNamePanel/VBoxContainer/StationNameLabel");
        _stationFactionLabel = GetNode<Label>("StationNamePanel/VBoxContainer/StationFactionLabel");

        _stationRoomButtonBox = GetNode<Control>("StationRoomButtonBox");

        _mapPanel = GetNode<Control>("MapPanel");


        //// Hud Indicators
        _aimingMask = GetNode<Control>("AimingMask");
        _freelookIndicator = GetNode<Label>("FreelookIndicator");
        _lockedAimingHint = GetNode<Label>("LockedAimingHint");
        _torpedoLockedStatusLabel = GetNode<Label>("AimingMask/TorpedoLockedStatusLabel");
        _torpedoReloadingStatusLabel = GetNode<Label>("AimingMask/TorpedoReloadingStatusLabel");
        _torpedoOnWireStatusLabel = GetNode<Label>("AimingMask/TorpedoOnWireStatusLabel");
        _torpedoReadyStatusLabel = GetNode<Label>("AimingMask/TorpedoReadyStatusLabel");
        _torpedoSeekingStatusLabel = GetNode<Label>("AimingMask/TorpedoSeekingStatusLabel");
        _ammoTypeLabel = GetNode<Label>("AimingMask/AmmoTypeLabel");
        _ammoRangeLabel = GetNode<Label>("AimingMask/AmmoRangeLabel");
        _torpedoAltitudeLabel = GetNode<Label>("AimingMask/TorpedoAltitudeLabel");
        _turretAimingWidgets = new List<TurretAimingWidget>();
        _sonarContactWidgets = new Dictionary<ITargetableThing, SonarContactWidget>();
        _steerIndicator = GetNode<TextureRect>("AimingMask/SteerIndicator");
        _notificationBox = GetNode<VBoxContainer>("NotificationContainer/NotificationPanel");

        //// Mission Info
        _missionInfoLabel = GetNode<Label>("MissionInfoLabel");

        //// Popups
        _escapeMenuPopup = GetNode<EscapeMenuPopup>("EscapeMenuPopup");
        _progressPopup = GetNode<ProgressPopup>("ProgressPopup");
        _stationRoomPopup = GetNode<StationRoomPopup>("StationRoomPopup");
        _standingsPopup = GetNode<StandingsPopup>("StandingsPopup");
        _loadoutPopup = GetNode<LoadoutPopup>("LoadoutPopup");
        _quitConfirmDialog = GetNode<ConfirmationDialog>("QuitConfirmDialog");

        _indicatorContainer = GetNode<Control>("IndicatorContainer");
        _notificationPlayer = GetNode<AudioStreamPlayer>("NotificationPlayer");
        _camera = GetParent<Spatial>().GetNode<SubCamera>("Camera");
        _gameplayRoot = this.FindParent<GameplayRoot>();
        _turretMounts = new List<TurretMount>();
        _sonarContactWidgets = new Dictionary<ITargetableThing, SonarContactWidget>();

        _notificationSystem.OnNotification += _notificationSystem_OnNotification;
        _simulationEventBus.OnSimulationEvent += OnSimulationEvent;
    }

    public override void _ExitTree()
    {
        base._ExitTree();
        _simulationEventBus.OnSimulationEvent -= OnSimulationEvent;
        _notificationSystem.OnNotification -= _notificationSystem_OnNotification;
    }

    private void OnSimulationEvent(ISimulationEvent evt)
    {
        if(evt is NewQuestSimulationEvent newQuestSimulationEvent
            && _missionTracker.TrackedMission == null)
        {
            _missionTracker.TrackedMission = newQuestSimulationEvent.NewQuest as IMission;
        }
        else if(evt is CargoScanUltimatumSimulationEvent cargoScanUltimatum
            && cargoScanUltimatum.TargetSubmarine == _playerData.PlayerSub)
        {
            ShowCargoUltimatum(cargoScanUltimatum);
        }
        else if(evt is HailResponseSimulationEvent hailResponseSimulationEvent)
        {
            ShowHailResponse(hailResponseSimulationEvent);
        }
        else if(evt is StartConversationSimulationEvent startConvoEvent)
        {
            StartConversation(startConvoEvent.Npc);
        }
    }

    private void _notificationSystem_OnNotification(Notification notification)
    {
        var newItem = NotificationItemPrefab.Instance() as NotificationItem;
        newItem.Init(notification);
        _notificationBox.AddChild(newItem);

        if (Visible && !_notificationPlayer.Playing
            && notification.IsMessage)
        {
            _notificationPlayer.Playing = true;
        }
    }

    public override void _Process(float delta)
    {
        var stopwatch = System.Diagnostics.Stopwatch.StartNew();
        base._Process(delta);

        _dockedStatusPanel.Visible = _playerData.PlayerSub.IsDockedTo != null;
        _playerWalletLabel.Text = _playerData.Money.ToString("C0");

        if(_playerData.PlayerSub != null && IsInstanceValid(_playerData.PlayerSub))
        {
            float depth;
            if(_gameplayRoot.UseOriginRelativeUp)
            {
                depth = _gameplayRoot.CeilingLevel - _playerData.PlayerSub.RealPosition.Length();
            }
            else
            {
                depth = _gameplayRoot.CeilingLevel - _playerData.PlayerSub.RealPosition.y;
            }

            _depthLabel.Text = Util.DistanceToDisplay(depth, false);
        }

        if (_wasDockedTo != _playerData.PlayerSub?.IsDockedTo)
        {
            _wasDockedTo = _playerData.PlayerSub?.IsDockedTo;

            if (_wasDockedTo != null) // just docked to a station
            {
                _camera.CameraPitch = Mathf.Pi / 4;
                _camera.CameraDistance = 250;

                if(_playerData.PlayerSub.IsDockedTo is WorldStation worldStation
                    && worldStation.IsCaveStation)
                {
                    _camera.Environment = _camera.CaveEnvironment;
                    _camera.GetNode<Particles>("Particles").Emitting = false;
                    _camera.GetParent().GetNode<DirectionalLight>("RoofLight").Visible = false;
                }

                if ((DateTime.Now - _worldManager.SessionStartTime) > TimeSpan.FromSeconds(5))
                {
                    _progressPopup.ShowFor(
                        "Autosaving",
                        reportProgress =>
                        {
                            _gameSerializer.SaveGameState("autosave", reportProgress);
                        },
                        () =>
                        {
                        });
                }

                _playerData.WeaponsSafe = true;

                _stationNameLabel.Text = _wasDockedTo.FriendlyName;
                _stationFactionLabel.Text = _wasDockedTo.Faction.Name;

                _stationRoomButtonBox.ClearChildren();
                BuildButtonsForStationRooms();
            }
            else // just undocked
            {
                _camera.Environment = _camera.OceanEnvironment;
                _camera.GetNode<Particles>("Particles").Emitting = true;
                _camera.GetParent().GetNode<DirectionalLight>("RoofLight").Visible = true;
                _stationRoomButtonBox.ClearChildren();
                RebuildSubUi();
            }
        }

        _manuverPanel.Visible = _wasDockedTo == null;
        _hangarPanel.Visible = _wasDockedTo == null && _playerData.PlayerSub.MinisubHangars.Any(h=>h.CanHoldMinisub && h.MinisubType != null);
        _sensorPanel.Visible = _wasDockedTo == null;
        _indicatorContainer.Visible = _wasDockedTo == null;
        _aimingMask.Visible = _wasDockedTo == null && _camera.InPeriscope;
        _stationNamePanel.Visible = _wasDockedTo != null;
        _dockedStatusPanel.Visible = _wasDockedTo != null;
        _stationRoomButtonBox.Visible = _wasDockedTo != null;
        _mapPanel.Visible = _wasDockedTo == null;
        _freelookIndicator.Visible = _wasDockedTo == null && Input.IsActionPressed("ui_freelook");
        _lockedAimingHint.Visible = _wasDockedTo == null && _camera.AimLock;

        if (Input.IsActionJustPressed("ui_hide"))
        {
            _hideToggle = !_hideToggle;
        }

        Visible = _playerData.PlayerSub != null
            && !_playerData.PlayerSub.IsWrecked
            && !_hideToggle
            && !HideForOtherUi;

        if(_playerData.InBattle && Engine.TimeScale > 1)
        {
            Engine.TimeScale = 1;
        }

        _weaponSafetiesButton.Disabled = _playerData.InBattle;
        _weaponSafetiesButton.Pressed = !_playerData.WeaponsSafe;

        if (_playerData.InBattle)
        {
            _playerData.WeaponsSafe = false;
        }

        if(Engine.TimeScale > 1)
        {
            _playerData.WeaponsSafe = true;
        }

        _time2.Disabled = _playerData.InBattle;
        _time4.Disabled = _playerData.InBattle;

        _time1.Pressed = Engine.TimeScale == 1;
        _time2.Pressed = Engine.TimeScale == 2;
        _time4.Pressed = Engine.TimeScale == 4;

        _dateTimeLabel.Text = _worldManager.CurrentDate.ToString();

        if (_wasDockedTo == null)
        {
            ProcessUiForNotDocked();
        }
        else
        {
            ProcessUiForDocked();
        }

        if(_missionTracker.TrackedMission != null)
        {
            if (!_missionTracker.IsHeldByAgent(_playerData.PlayerSub, _missionTracker.TrackedMission)
                && !(_missionTracker.TrackedMission is IQuest quest && quest.IsActive))
            {
                _missionInfoLabel.Text = string.Empty;
            }
            else
            {
                _missionInfoLabel.Text = string.Join("\n", _missionTracker.TrackedMission.Name, _missionTracker.TrackedMission.Detail0, _missionTracker.TrackedMission.Detail1, _missionTracker.TrackedMission.Detail2).Trim();
            }
        }
        else
        {
            _missionInfoLabel.Text = string.Empty;
        }

        if (_playerData.PlayerSub != null)
        {
            foreach (var mission in _missionTracker.GetCurrentMissions(_playerData.PlayerSub))
            {
                mission.ProcessPlayerLogic(
                    _notificationSystem,
                    _playerData.PlayerSub,
                    _missionTracker.TrackedMission == mission, 
                    delta);
            }

            _decoyLoadProgress.Text = (_playerData.PlayerSub.DecoyLoadProgress * 100).ToString("N0") + "%";
            _decoyDropButton.Disabled = _playerData.PlayerSub.DecoyLoadProgress < 1;
        }

        if ((_playerData.WeaponsSafe && _weaponSafetiesButton.Text != "Weapon Safeties On")
            || (!_playerData.WeaponsSafe && _weaponSafetiesButton.Text != "Weapon Safeties Off"))
        {
            if(_playerData.WeaponsSafe)
            {
                _weaponSafetiesButton.Text = "Weapon Safeties On";
                _weaponSafetiesButton.Modulate = Colors.Green;
            }
            else
            {
                _weaponSafetiesButton.Text = "Weapon Safeties Off";
                _weaponSafetiesButton.Modulate = Colors.Red;
            }
        }


        stopwatch.Stop();
        _perfTracker.LogPerf("GameplayUi._Process", stopwatch.ElapsedMilliseconds);
    }

    private void ProcessUiForDocked()
    {
        _dockedStatusNameLabel.Text = _playerData.PlayerSub.FriendlyName;
        if (_playerData.PlayerSub.Health < _playerData.PlayerSub.MaximumHealth)
        {
            _dockedStatusLabel.Text = $"Under repairs: {_playerData.PlayerSub.HealthPercent.ToString("P0")}";
            _dockedStatusLabel.Modulate = Colors.Yellow;
        }
        else
        {
            _dockedStatusLabel.Text = "Ready to Depart";
            _dockedStatusLabel.Modulate = Colors.Green;
        }

        UpdateSonarWidgets();
    }

    private void BuildButtonsForStationRooms()
    {
        var subButton = StationRoomButtonPrefab.Instance() as StationRoomButton;
        _stationRoomButtonBox.AddChild(subButton);
        subButton.InitializeFor(_playerData.PlayerSub);

        foreach (var room in _wasDockedTo.StationRooms)
        {
            var roomButton = StationRoomButtonPrefab.Instance() as StationRoomButton;
            _stationRoomButtonBox.AddChild(roomButton);
            roomButton.InitializeFor(room);
        }
    }

    private void ProcessUiForNotDocked()
    {
        if (_camera != null)
        {
            _aimingMask.Visible = _camera.InPeriscope;
        }

        if (_playerData.PlayerSub != null && IsInstanceValid(_playerData.PlayerSub))
        {
            _autopilotCancelButton.Visible = _playerData.PlayerController.AutopilotType != AutopilotType.None;
            _autopilotGotoButton.Pressed = _playerData.PlayerController.AutopilotType == AutopilotType.Goto;
            _autopilotDockButton.Pressed = _playerData.PlayerController.AutopilotType == AutopilotType.Dock;
            _autopilotFollowButton.Pressed = _playerData.PlayerController.AutopilotType == AutopilotType.Follow;

            _autopilotGotoButton.Modulate = _autopilotGotoButton.Pressed ? Colors.Yellow : Colors.Green;
            _autopilotDockButton.Modulate = _autopilotDockButton.Pressed ? Colors.Yellow : Colors.Green;
            _autopilotFollowButton.Modulate = _autopilotFollowButton.Pressed ? Colors.Yellow : Colors.Green;

            if (_playerData.PlayerController.DockingSequence != null
                && _playerData.PlayerController.DockingSequence.IsLocked)
            {
                _autopilotCancelButton.Visible = false;
                _autopilotCancelButton.Disabled = true;
                _autopilotGotoButton.Disabled = true;
                _autopilotDockButton.Disabled = true;
                _autopilotFollowButton.Disabled = true;
            }
            else
            {
                _autopilotCancelButton.Disabled = false;
                _autopilotGotoButton.Disabled = CurrentTarget == null;

                if (CurrentTarget is SonarContactRecord targetContact)
                {
                    _autopilotDockButton.Disabled = targetContact == null
                        || !targetContact.Identified
                        || !targetContact.SonarContact.IsDockable
                        || targetContact.SonarContact.Faction.IsHostileTo(_playerData.PlayerSub.Faction);
                    _autopilotFollowButton.Disabled = targetContact == null
                        || !targetContact.Identified
                        || !(targetContact.SonarContact is Submarine)
                        || targetContact.SonarContact.Faction.IsHostileTo(_playerData.PlayerSub.Faction);
                }
                else
                {
                    _autopilotDockButton.Disabled = true;
                    _autopilotFollowButton.Disabled = true;
                }
            }

            _healthBar.Value = (int)(100 * _playerData.PlayerSub.Health / _playerData.PlayerSub.MaximumHealth);
            _healthBar.HintTooltip = $"Health: {(int)_playerData.PlayerSub.Health} / {(int)_playerData.PlayerSub.MaximumHealth}";

            _steerIndicator.SetRotation(
                _playerData.PlayerSub.GlobalTransform.basis.z
                    .SignedAngleTo(_camera.GlobalTransform.basis.z, _playerData.PlayerSub.GlobalTransform.basis.y)
                    + Mathf.Pi / 2);

            if (_oldPlayerSub != _playerData.PlayerSub)
            {
                RebuildSubUi();
            }

            var environmentNoise = 5;
            var detectDist = Mathf.Sqrt(_playerData.PlayerSub.OverallNoise / (4 * Mathf.Pi * environmentNoise)) / Constants.SONAR_ENV_PROPAGATION;

            _throttleDownButton.Disabled = _playerData.PlayerSub.Throttle <= -0.25f;
            _throttleLabel.Text = (int)(_playerData.PlayerSub.Throttle * 100) + "%";
            _throttleUpButton.Disabled = _playerData.PlayerSub.Throttle >= 1f;
            _speedLabel.Text = _playerData.PlayerSub.LinearVelocity.Length().ToString("N0");

            _diveDownButton.Disabled = _playerData.PlayerSub.Dive <= -1;
            _diveLabel.Text = (int)(_playerData.PlayerSub.Dive * 100) + "%";
            _diveUpButton.Disabled = _playerData.PlayerSub.Dive >= 1;
            _selfNoiseLabel.Text = $"  Noise: {_playerData.PlayerSub.OverallNoise.ToString("N0")}";
            _selfDetectLabel.Text =$"  Detection: {Util.DistanceToDisplay(detectDist, false)}";

            _ammoTypeLabel.Visible = false;
            _ammoRangeLabel.Visible = false;

            if (ActiveWeaponSystem is TorpedoTube torpedoTube)
            {
                _torpedoLockedStatusLabel.Visible = torpedoTube.Target != null;

                _torpedoAltitudeLabel.Visible = torpedoTube.Target == null;

                _torpedoReloadingStatusLabel.Visible = torpedoTube.LoadProgress != torpedoTube.LoadTime
                    && torpedoTube.ActiveTorpedo == null;

                _torpedoOnWireStatusLabel.Visible = torpedoTube.ActiveTorpedo != null;

                _torpedoReadyStatusLabel.Visible = torpedoTube.LoadProgress == torpedoTube.LoadTime;

                _torpedoSeekingStatusLabel.Visible = torpedoTube.ActiveTorpedo != null
                    && torpedoTube.ActiveTorpedo.ActiveSeeking;

                if (torpedoTube.ActiveAmmo != null)
                {
                    UpdateAmmoLabelsFor(
                        torpedoTube.ActiveAmmo.Name,
                        torpedoTube.ActiveAmmo.FuelTime * torpedoTube.ActiveAmmo.Speed);
                }

                if(torpedoTube.Target == null)
                {
                    var heightDelta = _playerData.PlayerSub.RealPosition.Length() - _playerData.PlayerSub.AimPoint.Length();
                    _torpedoAltitudeLabel.Text = $"Tgt Depth: {Util.DistanceToDisplay(heightDelta, true)}";
                }
            }
            else
            {
                _torpedoLockedStatusLabel.Visible = false;
                _torpedoAltitudeLabel.Visible = false;
                _torpedoReloadingStatusLabel.Visible = false;
                _torpedoOnWireStatusLabel.Visible = false;
                _torpedoReadyStatusLabel.Visible = false;
                _torpedoSeekingStatusLabel.Visible = false;
            }
            
            if (ActiveWeaponSystem is TurretSystem turretSystem
                && turretSystem.ActiveAmmo != null)
            {
                UpdateAmmoLabelsFor(
                    turretSystem.ActiveAmmo.Name,
                    turretSystem.ActiveAmmo.MaximumRange);
            }

            var contacts = _playerData.PlayerSub.SonarContactRecords.Values
                .Where(c => c.IsDetected && TestFilterMatch(c))
                .OfType<ITargetableThing>();

            if(_playerData.ManualWaypoint!= null)
            {
                contacts = contacts.Concat(_playerData.ManualWaypoint.Yield());
            }

            if (_missionTracker.TrackedMission?.MissionPath != null && _missionTracker.TrackedMission.MissionPath.Waypoints.Any())
            {
                contacts = contacts
                    .Concat(_missionTracker.TrackedMission.MissionPath.Yield());
            }

            contacts = contacts
                .OrderBy(c => (c.EstimatedPosition - _playerData.PlayerSub.GlobalTransform.origin).Length());

            UpdateTurretAimWidgets();
            UpdateContactList(contacts.Where(c => c.ContactType != ContactType.Torpedo).ToArray());
            UpdateSonarWidgets();

            if (Input.IsActionJustPressed("target_nearest"))
            {
                if (_camera.InPeriscope)
                {
                    var screenCenter = new Vector2(
                        _camera.GetViewport().Size.x / 2,
                        _camera.GetViewport().Size.y / 2);

                    var newTarget = _sonarContactWidgets
                        .Where(kvp => !kvp.Value.IsOffScreen)
                        .OrderBy(kvp => (kvp.Value.RectPosition - screenCenter).Length())
                        .Select(kvp => kvp.Key)
                        .FirstOrDefault();

                    if (newTarget != null)
                    {
                        CurrentTarget = newTarget;
                    }
                    else
                    {
                        CurrentTarget = contacts.FirstOrDefault();
                    }
                }
                else
                {
                    CurrentTarget = contacts.FirstOrDefault();
                }
            }

            foreach(var tube in _playerData.PlayerSub.TorpedoTubes)
            {
                if(tube.ActiveTargeting)
                {
                    tube.Target = CurrentTarget;
                }
                else
                {
                    tube.Target = null;
                }
            }
        }

        _towedArrayDeployButton.Disabled = _playerData.PlayerSub.TowHook == null 
            || !_playerData.PlayerSub.HasTowedArray
            || _playerData.PlayerSub.TowedArrayRepairTimer > 0;
        _towedArrayDeployButton.Pressed = _playerData.PlayerSub.DeployTowedArray;

        if (!_playerData.PlayerSub.HasTowedArray)
        {
            _towedArrayStatusLabel.Text = "Not Installed";
            _towedArrayStatusLabel.Modulate = Colors.Yellow;
        }
        if (_playerData.PlayerSub.TowedArrayRepairTimer > 0)
        {
            _towedArrayStatusLabel.Text = $"Damaged: {_playerData.PlayerSub.TowedArrayRepairTimer.ToString("F0")}s";
            _towedArrayStatusLabel.Modulate = Colors.Red;
        }
        else if(_playerData.PlayerSub.TowedArray == null)
        {
            _towedArrayStatusLabel.Text = "Retracted";
            _towedArrayStatusLabel.Modulate = Colors.Yellow;
        }
        else if (_playerData.PlayerSub.DeployTowedArray
            && _playerData.PlayerSub.TowedArray.TooFast)
        {
            _towedArrayStatusLabel.Text = "Too Fast";
            _towedArrayStatusLabel.Modulate = Colors.Red;
        }
        else if (_playerData.PlayerSub.DeployTowedArray 
            && _playerData.PlayerSub.TowedArray.FullyExtended
            && _playerData.PlayerSub.TowedArray.Unstable)
        {
            _towedArrayStatusLabel.Text = "Unstable";
            _towedArrayStatusLabel.Modulate = Colors.Yellow;
        }
        else if (_playerData.PlayerSub.DeployTowedArray
            && _playerData.PlayerSub.TowedArray.FullyExtended)
        {
            _towedArrayStatusLabel.Text = "Active";
            _towedArrayStatusLabel.Modulate = Colors.Green;
        }
        else if (_playerData.PlayerSub.DeployTowedArray)
        {
            _towedArrayStatusLabel.Text = "Deploying";
            _towedArrayStatusLabel.Modulate = Colors.Yellow;
        }
        else
        {
            _towedArrayStatusLabel.Text = "Retracting";
            _towedArrayStatusLabel.Modulate = Colors.Yellow;
        }

        if(_playerData.PlayerSub.TowedArrayRepairTimer > 0 && !_towedArrayWasDamaged)
        {
            _towedArrayWasDamaged = true;
            _notificationSystem.AddNotification(new Notification
            {
                IsBad = true,
                Body = "Towed array damaged!"
            });
        }
        if (_playerData.PlayerSub.TowedArrayRepairTimer <= 0 && _towedArrayWasDamaged)
        {
            _towedArrayWasDamaged = false;
            _notificationSystem.AddNotification(new Notification
            {
                IsBad = false,
                Body = "Towed array repaired!"
            });
        }
    }

    private void UpdateAmmoLabelsFor(string ammoName, double ammoRange)
    {
        _ammoTypeLabel.Visible = true;
        _ammoTypeLabel.Text = ammoName;
        _ammoRangeLabel.Visible = true;
        _ammoRangeLabel.Text = $"{ammoRange.ToString("N0")} m";

        if(CurrentTarget != null)
        {
            var rangeToTarget = _playerData.PlayerSub.RealPosition.DistanceTo(CurrentTarget.EstimatedPosition);

            if (rangeToTarget <= ammoRange * 0.8)
            {
                _ammoRangeLabel.Modulate = Colors.Green;
            }
            else if (rangeToTarget <= ammoRange)
            {
                _ammoRangeLabel.Modulate = Colors.Yellow;
            }
            else
            {
                _ammoRangeLabel.Modulate = Colors.Red;
            }
        }
        else
        {
            _ammoRangeLabel.Modulate = Colors.Green;
        }
    }

    private void RebuildSubUi()
    {
        _oldPlayerSub = _playerData.PlayerSub;
        CurrentTarget = null;

        foreach (var value in _sonarContactWidgets.Values)
        {
            value.QueueFree();
        }
        _sonarContactWidgets.Clear();

        if (_turretAimingWidgets != null)
        {
            foreach (var widget in _turretAimingWidgets)
            {
                widget.QueueFree();
            }
        }

        _turretAimingWidgets = new List<TurretAimingWidget>();

        _turretMounts = _playerData.PlayerSub.TurretMounts
            .ToList();

        _hangarPanel.ClearChildren();
        _weaponControlBox.ClearChildren();
        var index = 1;

        ActiveWeaponSystem = ((IWeaponSystem)_playerData.PlayerSub.TurretSystems.FirstOrDefault()) ?? _playerData.PlayerSub.TorpedoTubes.FirstOrDefault();

        foreach (var system in _playerData.PlayerSub.TurretSystems)
        {
            var widget = TurretControlRowPrefab.Instance() as TurretControlRow;
            widget.Index = index;
            widget.TurretSystem = system;
            _weaponControlBox.AddChild(widget);
            index += 1;
        }
        foreach (var tube in _playerData.PlayerSub.TorpedoTubes)
        {
            var widget = TorpedoControlRowPrefab.Instance() as TorpedoControlRow;
            widget.Index = index;
            widget.TorpedoTube = tube;
            _weaponControlBox.AddChild(widget);
            index += 1;
        }
        foreach (var hangar in _playerData.PlayerSub.MinisubHangars.Where(h => h.CanHoldMinisub && h.MinisubType != null))
        {
            var widget = MinisubHangarWidget.Instance() as MinisubHangarWidget;
            widget.Index = index;
            widget.MinisubHangar = hangar;
            _hangarPanel.AddChild(widget);
            index += 1;
        }
    }

    public void SetThrottle(int button)
    {
        switch(button)
        {
            case 0:
                _playerData.PlayerSub.Throttle = -0.25f;
                break;
            case 1:
                _playerData.PlayerSub.Throttle = 0;
                break;
            case 2:
                _playerData.PlayerSub.Throttle = 0.25f;
                break;
            case 3:
                _playerData.PlayerSub.Throttle = 0.5f;
                break;
            case 4:
                _playerData.PlayerSub.Throttle = 0.75f;
                break;
            case 5:
                _playerData.PlayerSub.Throttle = 1;
                break;
        }
    }

    public void SetTime(int factor)
    {
        _soundEffectPlayer.PlayUiClick();
        Engine.TimeScale = factor;
    }

    private void UpdateTurretAimWidgets()
    {
        var screenWidth = _camera.GetViewport().Size.x;
        var screenHeight = _camera.GetViewport().Size.y;

        var aimDistance = _playerData.PlayerSub.RealPosition.DistanceTo(_playerData.PlayerSub.AimPoint);

        for (var i = 0; i < _turretMounts.Count; i++)
        {
            TurretAimingWidget aimWidget;

            if(i >= _turretAimingWidgets.Count)
            {
                aimWidget = TurretAimingWidget.Instance() as TurretAimingWidget;
                _indicatorContainer.AddChild(aimWidget);
                _turretAimingWidgets.Add(aimWidget);
                aimWidget.Mount = _turretMounts[i];
            }
            else
            {
                aimWidget = _turretAimingWidgets[i];
            }

            if (_turretMounts[i].Turret != null)
            {
                UpdateAimingWidget(
                    _turretMounts[i].Turret,
                    aimWidget, 
                    screenWidth, 
                    screenHeight,
                    aimDistance);
            }
        }
    }

    private void UpdateAimingWidget(
        GunTurret turret,
        TurretAimingWidget widget,
        float screenWidth,
        float screenHeight,
        float aimDistance)
    {
        var aimPoint = turret.Muzzles[0].GlobalTransform.Translated(new Vector3(0, 0, aimDistance)).origin;

        var screenPos = _camera.UnprojectPosition(aimPoint);
        var isOffScreen = _camera.IsPositionBehind(aimPoint)
            || screenPos.x < 0 || screenPos.x > screenWidth
            || screenPos.y < 0 || screenPos.y > screenHeight;

        widget.Visible = !isOffScreen
            && !_playerData.WeaponsSafe
            && ActiveWeaponSystem == turret.TurretSystem;
        
        widget.RectPosition = new Vector2(
            screenPos.x - widget.RectSize.x / 2,
            screenPos.y - widget.RectSize.y / 2);
    }

    private void UpdateContactList(ITargetableThing[] contacts)
    {
        var contactListItems = _contactList.GetChildren().OfType<ContactListItem>().ToArray();
        
        for (var i = 0; i < contacts.Length || i < contactListItems.Length; i++)
        {
            if(i < contacts.Length
                && i < contactListItems.Length)
            {
                contactListItems[i].UpdateContact(contacts[i]);
            }
            else if (i < contacts.Length)
            {
                var newItem = ContactListItemPrefab.Instance() as ContactListItem;
                newItem.UpdateContact(contacts[i]);
                _contactList.AddChild(newItem);
            }
            else if (i < contactListItems.Length)
            {
                contactListItems[i].QueueFree();
            }
        }
    }

    public void UpdateSonarWidgets()
    {
        var contacts = _playerData.PlayerSub.SonarContactRecords.Values
            .Where(c => c.IsDetected && TestFilterMatch(c))
            .OfType<ITargetableThing>();

        if (_playerData.ManualWaypoint != null)
        {
            contacts = contacts
                .Concat(_playerData.ManualWaypoint.Yield());
        }

        if (_missionTracker.TrackedMission?.MissionPath != null && _missionTracker.TrackedMission.MissionPath.Waypoints.Any())
        {
            contacts = contacts
                .Concat(_missionTracker.TrackedMission.MissionPath.Yield());
        }

        if (_playerData.PlayerSub.IsDockedTo == null)
        {
            _sonarContactTracker.SetVisibleContacts(
                _playerData.PlayerSub,
                _playerData.PlayerSub.SonarContactRecords.Values
                    .Where(c => c.TypeKnown && (c.PositionLocked || c.ContactType == ContactType.Station))
                    .Select(c => c.SonarContact));
        }
        else
        {
            _sonarContactTracker.SetVisibleContacts(
                _playerData.PlayerSub,
                _playerData.PlayerSub.IsDockedTo.SonarContactRecords
                    .Where(c => c.TypeKnown && (c.PositionLocked || c.ContactType == ContactType.Station))
                    .Select(c => c.SonarContact));
        }

        var screenWidth = GetViewport().Size.x;
        var screenHeight = GetViewport().Size.y;

        var frontPlane = new Plane(_camera.Transform.basis.Xform(Vector3.Forward), 0);

        var contactsToRemove = _sonarContactWidgets
            .Keys
            .Where(key => !key.IsDetected 
                || !key.IsValidInstance
                || (key is SonarContactRecord scr && !TestFilterMatch(scr))
                || (key is MissionPath mp && mp != _missionTracker.TrackedMission?.MissionPath))
            .ToArray();

        foreach(var contact in contactsToRemove)
        {
            _sonarContactWidgets[contact].QueueFree();
            _sonarContactWidgets.Remove(contact);
        }

        TrackingContact = null;

        foreach (var contactRecord in contacts.ToArray())
        {
            SonarContactWidget targetWidget = null;

            if (_sonarContactWidgets.ContainsKey(contactRecord))
            {
                targetWidget = _sonarContactWidgets[contactRecord];
            }
            else
            {
                targetWidget = SonarContactWidgetPrefab.Instance() as SonarContactWidget;
                _sonarContactWidgets.Add(contactRecord, targetWidget);
                targetWidget.TargetableThing = contactRecord;
                _indicatorContainer.AddChild(targetWidget);
            }

            UpdateHudBracket(contactRecord, targetWidget, screenWidth, screenHeight);
        }
    }

    private void UpdateHudBracket(
        ITargetableThing contact,
        SonarContactWidget targetBracket,
        float screenWidth,
        float screenHeight)
    {
        var halfHeight = screenHeight / 2;
        var halfWidth = screenWidth / 2;

        var arrowMargin = 50;
        var leftX = -halfWidth + arrowMargin;
        var rightX = halfWidth - arrowMargin;
        var topY = -halfHeight + arrowMargin;
        var bottomY = halfHeight - arrowMargin;

        var screenPos = _camera.UnprojectPosition(contact.EstimatedPosition);
        var isOffScreen = _camera.IsPositionBehind(contact.EstimatedPosition)
            || screenPos.x < 0 || screenPos.x > screenWidth
            || screenPos.y < 0 || screenPos.y > screenHeight;

        var offscreenAngle = 0f;
        if (isOffScreen)
        {
            var fromTo = (contact.EstimatedPosition - _camera.GlobalTransform.origin).Normalized();

            fromTo = _camera.GlobalTransform.basis.XformInv(fromTo);

            fromTo.y = -fromTo.y;


            if (fromTo.x == 0)
            {
                screenPos.x = halfWidth + halfWidth;

                if (fromTo.y < 0)
                {
                    screenPos.y = bottomY + halfHeight;
                }
                else
                {
                    screenPos.y = topY + halfHeight;
                }
            }
            else if (fromTo.y == 0)
            {
                screenPos.y = halfHeight + halfHeight;

                if (fromTo.x < 0)
                {
                    screenPos.x = leftX + halfWidth;
                }
                else
                {
                    screenPos.x = rightX + halfWidth;
                }
            }
            else
            {

                var slope = (fromTo.y / fromTo.x);

                if (-halfHeight <= slope * halfWidth && slope * halfWidth <= halfHeight)
                {
                    if (fromTo.x < 0)
                    {
                        // hits left edge
                        screenPos.x = leftX + halfWidth;
                        screenPos.y = slope * leftX + halfHeight;
                    }
                    else
                    {
                        // hits right edge
                        screenPos.x = rightX + halfWidth;
                        screenPos.y = slope * rightX + halfHeight;
                    }
                }
                else
                {
                    if (fromTo.y < 0)
                    {
                        // hits top edge
                        screenPos.x = topY / slope + halfWidth;
                        screenPos.y = topY + halfHeight;
                    }
                    else
                    {
                        // hits bottom edge
                        screenPos.x = bottomY / slope + halfWidth;
                        screenPos.y = bottomY + halfHeight;
                    }
                }
            }

            var angle = Mathf.Atan2(fromTo.y, fromTo.x);

            offscreenAngle = angle;
        }

        targetBracket.UpdateForContact(
            CurrentTarget == contact,
            isOffScreen,
            offscreenAngle,
            (_playerData.PlayerSub.RealPosition - contact.EstimatedPosition).Length());

        targetBracket.RectPosition = new Vector2(
            screenPos.x - targetBracket.RectSize.x / 2,
            screenPos.y - targetBracket.RectSize.y / 2);


        if (contact == CurrentTarget && !targetBracket.IsOffScreen)
        {
            TrackingContact = targetBracket;
        }
    }

    private bool TestFilterMatch(SonarContactRecord contactRecord)
    {
        if (contactRecord.TypeKnown)
        {
            return (_filterUnknownButton.Pressed && contactRecord.SonarContact.ContactType == ContactType.Unknown)
                || (_filterSubsButton.Pressed && contactRecord.SonarContact.ContactType == ContactType.Sub)
                || (_filterSubsButton.Pressed && contactRecord.SonarContact.ContactType == ContactType.Minisub)
                || (_filterBastionsButton.Pressed && contactRecord.SonarContact.ContactType == ContactType.Outpost)
                || (_filterStationsButton.Pressed && contactRecord.SonarContact.ContactType == ContactType.Station)
                || (_filterWrecksButton.Pressed && contactRecord.SonarContact.ContactType == ContactType.Wreck)
                || (_filterTorpedoesButton.Pressed && contactRecord.SonarContact.ContactType == ContactType.Torpedo);
        }
        else
        {
            return _filterUnknownButton.Pressed;
        }
    }

    public void CancelAutopilot()
    {
        _soundEffectPlayer.PlayUiClick();
        if (_playerData.PlayerController != null)
        {
            _playerData.PlayerController.AutopilotType = AutopilotType.None;
        }
    }

    public void AutopilotGoto()
    {
        _soundEffectPlayer.PlayUiClick();
        if (_playerData.PlayerController != null
            && CurrentTarget != null)
        {
            _playerData.PlayerController.AutopilotThrottleLimit = 1;
            _playerData.PlayerController.AutopilotType = AutopilotType.Goto;
            _playerData.PlayerController.AutopilotTarget = CurrentTarget;
        }
    }

    public void AutopilotDock()
    {
        _soundEffectPlayer.PlayUiClick();
        if (_playerData.PlayerController != null)
        {
            _playerData.PlayerController.AutopilotThrottleLimit = 1;
            _playerData.PlayerController.AutopilotType = AutopilotType.Dock;
            _playerData.PlayerController.AutopilotTarget = CurrentTarget;
        }
    }

    public void AutopilotFollow()
    {
        _soundEffectPlayer.PlayUiClick();
        if (_playerData.PlayerController != null
            && CurrentTarget != null)
        {
            _playerData.PlayerController.AutopilotThrottleLimit = 1;
            _playerData.PlayerController.AutopilotType = AutopilotType.Follow;
            _playerData.PlayerController.AutopilotTarget = CurrentTarget;
        }
    }

    public void ShowEscapeMenu()
    {
        _escapeMenuPopup.Open();
    }

    public void ReturnToMenu()
    {
        _soundEffectPlayer.PlayUiClick();
        GetTree().ChangeScene("res://Scenes/MainMenu.tscn");
    }

    public void ShowStationRoom(StationRoom stationRoom)
    {
        _soundEffectPlayer.PlayUiClick();
        _stationRoomPopup.ShowFor(stationRoom);
    }

    public void ShowMissions()
    {
        _soundEffectPlayer.PlayUiClick();
        GetNode<MissionPopup>("MissionPopup").InitAndShow();
    }

    public void ShowCargo()
    {
        _soundEffectPlayer.PlayUiClick();
        GetNode<CargoPopup>("CargoPopup").InitAndShow();
    }

    public Texture IconForType(ContactType contactType)
    {
        switch (contactType)
        {
            case ContactType.Outpost:
                return TypeIconOutpost;
            case ContactType.Station:
                return TypeIconStation;
            case ContactType.Sub:
            case ContactType.Minisub:
                return TypeIconSub;
            case ContactType.Waypoint:
                return TypeIconWaypoint;
            case ContactType.Wreck:
                return TypeIconWreck;
            case ContactType.Torpedo:
                return TypeIconTorpedo;
            default:
                return TypeIconUnknown;
        }
    }

    public void ShowStandings()
    {
        _soundEffectPlayer.PlayUiClick();
        _standingsPopup.InitAndShow();
    }

    public void StartConversation(Npc npc)
    {
        _soundEffectPlayer.PlayUiClick();
        GetNode<CampaignNpcDialogPopup>("CampaignNpcDialogPopup").ShowFor(npc);
    }

    public void ShowLoadout()
    {
        _soundEffectPlayer.PlayUiClick();
        GetNode<LoadoutPopup>("LoadoutPopup").ShowFor(_playerData.PlayerSub);
    }

    public void DropDecoy()
    {
        _soundEffectPlayer.PlayUiClick();
        if (_playerData.PlayerSub != null)
        {
            _playerData.PlayerSub.DropDecoy();
        }
    }

    private void ShowCargoUltimatum(CargoScanUltimatumSimulationEvent cargoScanUltimatum)
    {
        if(Engine.TimeScale > 1)
        {
            Engine.TimeScale = 1;
        }

        GetNode<CargoScanUltimatumPopup>("CargoScanUltimatumPopup").ShowFor(cargoScanUltimatum);
    }

    public void PromptForJettison(CommodityType commodityType)
    {
        _playerData.PlayerSub.JettisonCargo(commodityType, _playerData.PlayerSub.CommodityStorage.GetQty(commodityType));
    }

    private void ShowHailResponse(HailResponseSimulationEvent hailResponseSimulationEvent)
    {
        if (Engine.TimeScale > 1)
        {
            Engine.TimeScale = 1;
        }

        GetNode<SubmarineHailPopup>("SubmarineHailPopup").ShowFor(hailResponseSimulationEvent);
    }

    public void DepartStation()
    {
        _soundEffectPlayer.PlayUiClick();
        if (_playerData.PlayerController.DockingSequence != null)
        {
            _playerData.PlayerController.DockingSequence.Undock();
        }
    }

    public void ToggleWeaponsSafeties()
    {
        _playerData.WeaponsSafe = !_playerData.WeaponsSafe;
        _soundEffectPlayer.PlayUiClick();

        if (!_playerData.WeaponsSafe)
        {
            Engine.TimeScale = 1;
        }
    }

    public void ToggleTowedArray()
    {
        _soundEffectPlayer.PlayUiClick();
        _playerData.PlayerSub.DeployTowedArray = !_playerData.PlayerSub.DeployTowedArray;
    }

    public void DiveUp()
    {
        _soundEffectPlayer.PlayUiClick();
        _playerData.PlayerController.DiveUp();
    }

    public void DiveDown()
    {
        _soundEffectPlayer.PlayUiClick();
        _playerData.PlayerController.DiveDown();
    }

    public void ThrottleUp()
    {
        _soundEffectPlayer.PlayUiClick();
        _playerData.PlayerController.ThrottleUp();
    }

    public void ThrottleDown()
    {
        _soundEffectPlayer.PlayUiClick();
        _playerData.PlayerController.ThrottleDown();
    }

    public void ShowFullMap()
    {
        _soundEffectPlayer.PlayUiClick();
        GetTree().Root.AddChild(MapscreenPrefab.Instance());
    }
}
