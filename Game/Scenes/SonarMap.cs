using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

public class SonarMap : TextureRect
{
    private static readonly float PATH_DOT_TIME = 10;
    [Export]
    public bool Minimap;

    [Export]
    public PackedScene MapIconPrefab;

    [Export]
    public PackedScene MapPathDotPrefab;

    [Export]
    public PackedScene MapSonarDotPrefab;

    [Export]
    public float MapZoom = 100f;

    [Export]
    public float MapZoomMin = 5f;

    [Export]
    public float MapZoomMax = 1000f;

    private TextureRect _selfIcon;
    private TextureRect _cameraLine;
    private TextureRect _sonarLine;
    private GameplayUi _gameplayUi;
    private SubCamera _subCamera;
    private Dictionary<ITargetableThing, MapIcon> _mapIcons;
    private Label _zoomLabel;
    private Button _zoomInButton;
    private Button _zoomOutButton;
    private List<MapPathDot> _pathDots;
    private float _selfDotCounter;
    private GameplayRoot _gameplayRoot;

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IPerftracker _perfTracker;
    [Inject]
    private IMissionTracker _missionTracker;

    private float _sweepRate = 0.5f;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _selfIcon = GetNode<TextureRect>("SelfIcon");
        _cameraLine = GetNode<TextureRect>("CameraLine");
        _sonarLine = GetNode<TextureRect>("SonarLine");
        _gameplayUi = this.FindParent<GameplayUi>();
        _mapIcons = new Dictionary<ITargetableThing, MapIcon>();
        _zoomLabel = GetNode<Label>("MapScaleLabel");
        _zoomInButton = GetNode<Button>("../ButtonRow/MapZoomInButton");
        _zoomOutButton = GetNode<Button>("../ButtonRow/MapZoomOutButton");
        _subCamera = _gameplayUi.GetNode<SubCamera>("../Camera");
        _pathDots = new List<MapPathDot>();
        _gameplayRoot = this.FindParent<GameplayRoot>();

        if (Minimap)
        {
            _cameraLine.Visible = true;
        }
        else
        {
            _cameraLine.Visible = false;
        }
    }

    public override void _Process(float delta)
    {
        var stopwatch = System.Diagnostics.Stopwatch.StartNew();
        base._Process(delta);

        if (_playerData.PlayerSub != null)
        {
            UpdateSonarSweep(delta);
            UpdateMapIcons(delta);
        }

        if(Minimap && _subCamera != null)
        {
            _cameraLine.SetRotation(_subCamera.CameraYaw + Mathf.Pi);
            _sonarLine.SetRotation(_sonarLine.GetRotation() + _sweepRate * delta);
        }

        _zoomLabel.Text = MapZoom.ToString() + "x";
        stopwatch.Stop();
        _perfTracker.LogPerf("SonarMap._Process", stopwatch.ElapsedMilliseconds);
    }

    private void UpdateSonarSweep(float delta)
    {
        var playerSub = _playerData.PlayerSub;

        var beamYaw = _sonarLine.GetRotation();
        
        var beamDist = 8000;

        var beamFacing = new Vector3(beamDist, 0, 0);

        if(_gameplayRoot.UseOriginRelativeUp)
        {
            var beamTransform = Transform.Identity.LookingAt(playerSub.RealPosition, new Vector3(0, -1, 0));
            beamTransform = beamTransform.Rotated(beamTransform.Xform(Vector3.Left), Mathf.Pi / 2);
            beamFacing = beamTransform.Xform(beamFacing);
        }

        var beamDelta = beamFacing.Rotated(playerSub.UpVector, -beamYaw);

        var collisionInfo = playerSub.GetWorld().DirectSpaceState
            .IntersectRay(
                playerSub.GlobalTransform.origin,
                playerSub.GlobalTransform.origin + beamDelta,
                new Godot.Collections.Array { playerSub },
                0b00000000000000000001);

        if (collisionInfo.Count > 0)
        {
            var pathDot = MapSonarDotPrefab.Instance() as MapPathDot;
            pathDot.WorldPosition = (Vector3)collisionInfo["position"];
            AddChild(pathDot);
            _pathDots.Add(pathDot);
        }
    }

    private void UpdateMapIcons(float delta)
    {
        Transform mapTransform;

        if(_gameplayRoot.UseOriginRelativeUp)
        {
            mapTransform =  _playerData.PlayerSub.Transform
                .LookingAt(
                    Vector3.Zero, 
                    new Vector3(0, Constants.WORLD_RADIUS, 0));
        }
        else
        {
            mapTransform = _playerData.PlayerSub.Transform
                .LookingAt(
                    _playerData.PlayerSub.GlobalTransform.origin - new Vector3(0, -10000, 0),
                    new Vector3(0, 0, 10000));
        }

        _selfIcon.SetRotation(GetHeading(_playerData.PlayerSub));
        var centerPosition = _playerData.PlayerSub.GlobalTransform.origin;

        _selfDotCounter += delta;
        if (_selfDotCounter >= PATH_DOT_TIME)
        {
            _selfDotCounter -= PATH_DOT_TIME;
            var selfPathDot = MapPathDotPrefab.Instance() as MapPathDot;
            selfPathDot.RectPosition = _selfIcon.RectPosition;
            selfPathDot.WorldPosition = _playerData.PlayerSub.GlobalTransform.origin;
            AddChild(selfPathDot);
            _pathDots.Add(selfPathDot);
        }

        var contactsToRemove = _mapIcons
            .Keys
            .Where(key => !key.IsDetected 
                || !key.IsValidInstance 
                || (key is SonarContactRecord scr && !_playerData.PlayerSub.SonarContactRecords.ContainsKey(scr.SonarContact.Id))
                || (key is MissionPath mp && _missionTracker.TrackedMission?.MissionPath != mp))
            .ToArray();

        var contacts = _playerData.PlayerSub.SonarContactRecords.Values
            .Where(c => c.IsDetected)
            .Where(c => c.IsValidInstance)
            .OfType<ITargetableThing>();

        if (_missionTracker.TrackedMission?.MissionPath != null && _missionTracker.TrackedMission.MissionPath.Waypoints.Any())
        {
            contacts = contacts
                .Concat(_missionTracker.TrackedMission.MissionPath.Yield());
        }

        foreach (var contact in contactsToRemove)
        {
            _mapIcons[contact].QueueFree();
            _mapIcons.Remove(contact);
        }

        foreach (var contactRecord in contacts)
        {
            MapIcon mapIcon = null;

            if (_mapIcons.ContainsKey(contactRecord))
            {
                mapIcon = _mapIcons[contactRecord];
            }
            else
            {
                mapIcon = MapIconPrefab.Instance() as MapIcon;
                _mapIcons.Add(contactRecord, mapIcon);
                mapIcon.TargetableThing = contactRecord;
                mapIcon.PlayerFaction = _playerData.PlayerSub.Faction;
                mapIcon.GameplayUi = _gameplayUi;
                AddChild(mapIcon);
            }

            if(mapIcon.TargetableThing == _gameplayUi.CurrentTarget)
            {
                mapIcon.Modulate = Colors.Yellow;
            }
            else
            {
                mapIcon.Modulate = Colors.White;
            }

            if(contactRecord.PositionLocked 
                && contactRecord is SonarContactRecord scr
                && scr.SonarContact.ContactType != ContactType.Outpost 
                && scr.SonarContact.ContactType != ContactType.Station)
            {
                mapIcon.SetRotation(GetHeading(scr.SonarContact as Spatial));
            }
            else
            {
                mapIcon.SetRotation(0);
            }

            var mapPos = GetPlanePos(mapTransform, contactRecord.EstimatedPosition) / MapZoom;

            mapIcon.RectPosition = new Vector2(mapPos.x, mapPos.y) + RectSize / 2 - new Vector2(8, 8);

            if(contactRecord.PositionLocked)
            {
                mapIcon.PathDotCounter += delta;

                if(mapIcon.PathDotCounter >= PATH_DOT_TIME)
                {
                    mapIcon.PathDotCounter -= PATH_DOT_TIME;

                    var pathDot = MapPathDotPrefab.Instance() as MapPathDot;
                    pathDot.RectPosition = mapIcon.RectPosition;
                    pathDot.WorldPosition = contactRecord.EstimatedPosition;
                    AddChild(pathDot);
                    _pathDots.Add(pathDot);
                }
            }
        }

        foreach(var pathDot in _pathDots)
        {
            pathDot.TimeToLive -= delta;

            var mapPos = GetPlanePos(mapTransform, pathDot.WorldPosition) / MapZoom;

            pathDot.RectPosition = new Vector2(mapPos.x, mapPos.y) + RectSize / 2 - new Vector2(8, 8);

            if (pathDot.TimeToLive <= 0)
            {
                pathDot.QueueFree();
            }
        }

        _pathDots.RemoveAll(x => x.TimeToLive <= 0);
    }

    private float GetHeading(Spatial submarine)
    {
        var facing = -submarine.GlobalTransform.basis.z;
        var toNorth = new Vector3(0, 1, 0);

        var heading = facing.SignedAngleTo(toNorth, _playerData.PlayerSub.UpVector);

        return heading + Mathf.Pi / 2;
    }

    private Vector2 GetPlanePos(Transform mapTransform, Vector3 position)
    {
        var projectedPos = mapTransform.XformInv(position);
        
        return new Vector2(
            projectedPos.x,
            -projectedPos.y);
    }

    public void ZoomIn()
    {
        if(MapZoom > MapZoomMin)
        {
            MapZoom /= 2;
        }

        _zoomInButton.Disabled = MapZoom <= MapZoomMin;
        _zoomOutButton.Disabled = MapZoom >= MapZoomMax;
    }

    public void ZoomOut()
    {
        if (MapZoom < MapZoomMax)
        {
            MapZoom *= 2;
        }

        _zoomInButton.Disabled = MapZoom <= MapZoomMin;
        _zoomOutButton.Disabled = MapZoom >= MapZoomMax;
    }
}
