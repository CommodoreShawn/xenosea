using Godot;
using System;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

public class SonarContactWidget : Control
{
    public ITargetableThing TargetableThing { get; internal set; }

    private TextureRect _reticule;
    private NinePatchRect _smallBracket;
    private NinePatchRect _largeBracket;
    private Control _arrowPivot;
    private TextureRect _smallPointer;
    private TextureRect _largePointer;
    private TextureRect _typeIcon;
    private Label _contactNameLabel;
    private TextureProgress _healthBar;
    private bool _isTarget;
    private GameplayUi _gameplayUi;
    public bool IsOffScreen { get; private set; }
    private Label _noiseLabel;
    private Label _distanceLabel;
    private Label _ageLabel;
    private Label _factionLabel;
    private Label _depthLabel;
    private Label _speedLabel;
    private Label _debugLabel;
    [Export]
    public Texture SpeakingIcon;

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IWorldManager _worldManager;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _reticule = GetNode<TextureRect>("Reticule");
        _smallBracket = GetNode<NinePatchRect>("SmallBracket");
        _largeBracket = GetNode<NinePatchRect>("LargeBracket");
        _arrowPivot = GetNode<Control>("ArrowPivot");
        _smallPointer = GetNode<TextureRect>("ArrowPivot/Small");
        _largePointer = GetNode<TextureRect>("ArrowPivot/Big");
        _typeIcon = GetNode<TextureRect>("HBoxContainer/TypeIcon");
        _contactNameLabel = GetNode<Label>("HBoxContainer/NameLabel");
        _healthBar = GetNode<TextureProgress>("HealthBar");
        _gameplayUi = this.FindParent<GameplayUi>();
        _noiseLabel = GetNode<Label>("VBoxContainer/NoiseLabel");
        _distanceLabel = GetNode<Label>("VBoxContainer/DistanceLabel");
        _ageLabel = GetNode<Label>("VBoxContainer/AgeLabel");
        _factionLabel = GetNode<Label>("VBoxContainer/FactionLabel");
        _depthLabel = GetNode<Label>("VBoxContainer2/DepthLabel");
        _speedLabel = GetNode<Label>("VBoxContainer2/SpeedLabel");
        _debugLabel = GetNode<Label>("VBoxContainer2/DebugLabel");

        _factionLabel.Text = string.Empty;
    }

    public void OnGuiInput(InputEvent @event)
    {
        if (@event is InputEventMouseButton buttonInput)
        {
            if (buttonInput.Pressed)
            {
                _gameplayUi.CurrentTarget = TargetableThing;
            }
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
    }

    public void UpdateForContact(
        bool isTarget, 
        bool isOffScreen, 
        float offscreenAngle,
        float distance)
    {
        var isCrowded = GetParent().GetChildren()
            .OfType<SonarContactWidget>()
            .Where(x => x != this)
            .Where(x => x.RectPosition.DistanceSquaredTo(RectPosition) < 4000)
            .Any();
        isCrowded = false;

        IsOffScreen = isOffScreen;
        _isTarget = isTarget;

        if (TargetableThing is SonarContactRecord sonarContactRecord)
        {
            _noiseLabel.Text = "Noise: " + sonarContactRecord.SignalLevel.ToString("N1");
        }
        else
        {
            _noiseLabel.Text = string.Empty;
        }
        _distanceLabel.Text = _playerData.PlayerSub.RealPosition.DistanceTo(TargetableThing.EstimatedPosition).ToString("N0") + " m";

        _noiseLabel.Visible = isTarget || !isCrowded;
        _distanceLabel.Visible = isTarget || !isCrowded;

        if(TargetableThing is SonarContactRecord scr)
        {
            var knowlegeAge = _worldManager.CurrentDate - scr.LastUpdateTime;

            if (knowlegeAge.TotalSeconds > 1)
            {
                _ageLabel.Text = $"Age: {knowlegeAge.ToString("mm\\:ss")}";
            }
            else
            {
                _ageLabel.Text = string.Empty;
            }
        }
        else
        {
            _ageLabel.Text = string.Empty;
        }
        _ageLabel.Visible = isTarget || !isCrowded;

        if (TargetableThing.IsSpeaking)
        {
            _typeIcon.Visible = isTarget || !isCrowded;
            _typeIcon.Texture = SpeakingIcon;
        }
        else
        {
            _typeIcon.Visible = TargetableThing.PositionLocked && TargetableThing.TypeKnown && (isTarget || !isCrowded);
        }

        _depthLabel.Text = $"Depth: {Util.DistanceToDisplay(Constants.WORLD_RADIUS - TargetableThing.EstimatedPosition.Length(), false)}";
        _depthLabel.Visible = isTarget || !isCrowded;

        if (TargetableThing is SonarContactRecord scr2)
        {
            _speedLabel.Text = $"Speed: {scr2.LastKnownVelocity.Length().ToString("N0")}";

            if (Constants.DEBUG_SONAR_CONTACT_WIDGET)
            {
                _debugLabel.Text = scr2.SonarContact.DebugText;
            }
            else
            {
                _debugLabel.Text = string.Empty;
            }
        }
        else
        {
            _speedLabel.Text = string.Empty;
            _debugLabel.Text = string.Empty;
        }
        _speedLabel.Visible = isTarget || !isCrowded;

        if (TargetableThing.PositionLocked)
        {
            _reticule.Visible = false;
            _smallBracket.Visible = !_isTarget && !isOffScreen;
            _largeBracket.Visible = _isTarget && !isOffScreen;
            _smallPointer.Visible = !_isTarget && isOffScreen;
            _largePointer.Visible = _isTarget && isOffScreen;
            //_typeIcon.Visible = TargetableThing.TypeKnown || TargetableThing.IsSpeaking ;
            _contactNameLabel.Visible = TargetableThing.Identified && (isTarget || !isCrowded);
            _factionLabel.Visible = TargetableThing.Identified && (isTarget || !isCrowded);

            if (TargetableThing.TypeKnown && !TargetableThing.IsSpeaking)
            {
                _typeIcon.Texture = _gameplayUi.IconForType(TargetableThing.ContactType);
            }

            if (TargetableThing.Identified)
            {
                _contactNameLabel.Text = TargetableThing.FriendlyName;
                
                if(TargetableThing is SonarContactRecord sonarContactRecord2)
                {
                    _factionLabel.Text = sonarContactRecord2.SonarContact?.Faction?.Name ?? string.Empty;
                }
                else
                {
                    _factionLabel.Text = string.Empty;
                }
            }
            
            if (isOffScreen)
            {
                _arrowPivot.SetRotation(offscreenAngle);
            }

            if(TargetableThing is SonarContactRecord sonarContactRecord1
                && sonarContactRecord1.SonarContact is ICombatant combatant
                && TargetableThing.Identified)
            {
                _healthBar.Visible = isTarget || !isCrowded;
                _healthBar.Value = 100 * combatant.HealthPercent;
            }
            else
            {
                _healthBar.Visible = false;
            }
        }
        else
        {
            _reticule.Visible = !isOffScreen;
            _smallBracket.Visible = false;
            _largeBracket.Visible = false;
            _smallPointer.Visible = false;
            _largePointer.Visible = false;
            _typeIcon.Visible = TargetableThing.IsSpeaking;
            _contactNameLabel.Visible = false;
            _healthBar.Visible = false;
        }

        if(TargetableThing is MissionPath || TargetableThing is SimpleWaypoint)
        {
            if (TargetableThing == _gameplayUi.CurrentTarget)
            {
                Modulate = Colors.Pink;
            }
            else
            {
                Modulate = Colors.Purple;
            }
        }
        else if (TargetableThing.Identified
            && TargetableThing is SonarContactRecord sonarContactRecord2
            && sonarContactRecord2.SonarContact.IsHostileTo(_playerData.PlayerSub)
            && sonarContactRecord2.ContactType != ContactType.Wreck)
        {
            if (TargetableThing == _gameplayUi.CurrentTarget)
            {
                Modulate = Colors.Orange;
            }
            else
            {
                Modulate = Colors.Red;
            }
        }
        else if (TargetableThing.Identified
            && TargetableThing is SonarContactRecord sonarContactRecord3
            && sonarContactRecord3.SonarContact.IsFriendlyTo(_playerData.PlayerSub)
            && sonarContactRecord3.ContactType != ContactType.Wreck)
        {
            if (TargetableThing == _gameplayUi.CurrentTarget)
            {
                Modulate = Colors.Cyan;
            }
            else
            {
                Modulate = Colors.Green;
            }
        }
        else
        {
            if (TargetableThing == _gameplayUi.CurrentTarget)
            {
                Modulate = Colors.Yellow;
            }
            else
            {
                Modulate = Colors.White;
            }
        }

        if(TargetableThing.IsSpeaking)
        {
            Modulate = Colors.Yellow;
        }
    }
}
