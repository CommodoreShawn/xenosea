using Godot;
using Xenosea.Logic;

public class DiverVendorOptionButton : Button
{
    public bool IsForVendor { get; set; }
    public DiveTeamType DiveTeamType { get; set; }

    public override void _Ready()
    {
        if (DiveTeamType != null)
        {
            Text = DiveTeamType.Name;
        }
    }

    public void OnPressed()
    {
        this.FindParent<DiverVendor>().SetActiveDiveTeamType(DiveTeamType, IsForVendor);
    }
}
