using Godot;

public class MapscreenCamera : Camera
{
    public float CameraDistance { get; set; } = 600;

    private bool _isAiming;

    private bool _isPanning;
    private Vector3 _panningToPos;
    public float PanningSpeed { get; set; } = 200f;

    public float Sensitivity { get; set; } = 1f;

    public override void _Ready()
    {
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if (_isAiming)
        {
            if (@event is InputEventMouseMotion mouseMotionEvt)
            {
                var relativeDelta = new Vector3(
                    -mouseMotionEvt.Relative.x * Sensitivity,
                    mouseMotionEvt.Relative.y * Sensitivity,
                    0);

                var position = GlobalTransform.Xform(relativeDelta);

                LookAtFromPosition(
                    position.Normalized() * CameraDistance,
                    Vector3.Zero,
                    Vector3.Up);
            }
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        var wasAiming = _isAiming;

        var realDelta = 0f;
        if(Engine.TimeScale > 0)
        {
            realDelta = delta / Engine.TimeScale;
        }

        _isAiming = Input.IsMouseButtonPressed((int)ButtonList.Right);

        if (_isAiming && !wasAiming)
        {
            Input.MouseMode = Input.MouseModeEnum.Captured;
        }
        else if (!_isAiming && wasAiming)
        {
            Input.MouseMode = Input.MouseModeEnum.Visible;
        }

        if(_isAiming && _isPanning)
        {
            _isPanning = false;
        }

        if (_isPanning)
        {
            var deltaX = 0f;
            var deltaY = 0f;

            if (Transform.origin.Normalized().SignedAngleTo(_panningToPos.Normalized(), Vector3.Up) > 0)
            {
                var maxArc = Transform.origin.Normalized().SignedAngleTo(_panningToPos.Normalized(), Vector3.Up)
                    * 400;

                //GD.Print(maxArc);
                //deltaX = Mathf.Min(maxArc, _panningSpeed * realDelta);
                deltaX = PanningSpeed * realDelta;
                //deltaX = maxArc * realDelta;
            }
            else
            {
                var maxArc = Transform.origin.Normalized().SignedAngleTo(_panningToPos.Normalized(), Vector3.Up)
                    * 400;

                //GD.Print(maxArc);

                //deltaX = -Mathf.Min(-maxArc, _panningSpeed * realDelta);
                deltaX = -PanningSpeed * realDelta;
                //deltaX = maxArc * realDelta;
            }

            if (Transform.origin.y < _panningToPos.y)
            {
                deltaY = Mathf.Min(PanningSpeed * realDelta, _panningToPos.y - Transform.origin.y);
            }
            else
            {
                deltaY = -Mathf.Min(PanningSpeed * realDelta, Transform.origin.y - _panningToPos.y);
            }

            var relativeDelta = new Vector3(
                    deltaX,
                    deltaY,
                    0);

            var position = GlobalTransform.Xform(relativeDelta);

            if (Transform.origin.DistanceTo(position) < Transform.origin.DistanceTo(_panningToPos))
            {
                LookAtFromPosition(
                    position.Normalized() * CameraDistance,
                    Vector3.Zero,
                    Vector3.Up);
            }
            else
            {
                _isPanning = false;
            }
        }
    }

    public void PanToLookAt(Vector3 position)
    {
        _isPanning = true;
        _panningToPos = position;
    }

    public void SetLookAt(Vector3 position)
    {
        _isPanning = false;

        LookAtFromPosition(
            position.Normalized() * CameraDistance,
            Vector3.Zero,
            Vector3.Up);
    }
}
