using Autofac;
using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;

public class MinisubHangarWidget : PanelContainer
{
    [Export]
    public Texture AmmoIconEmpty;
    [Export]
    public Texture AmmoIconFull;

    public MinisubHangar MinisubHangar { get; set; }
    
    private TextureProgress _healthBar;
    private TextureProgress _chargeBar;
    private Label _wreckedIcon;
    private HBoxContainer _ammoIconBox;
    private TextureRect[] _ammoIcons;
    private Label _statusLabel;
    private Button _targetButton;
    private Button _gotoButton;
    private Button _escortButton;
    private Button _returnButton;
    private Button _contextButton1;
    private Button _contextButton2;
    

    private GameplayUi _gameplayUi;
    private TextureRect _subIcon;

    public int Index { get; set; }

    private bool _inTargetMode;

    public override void _Ready()
    {
        _gameplayUi = this.FindParent<GameplayUi>();

        _subIcon = GetNode<TextureRect>("VBoxContainer/VBoxContainer/HBoxContainer2/TextureRect");
        _healthBar = GetNode<TextureProgress>("VBoxContainer/VBoxContainer/HBoxContainer2/HealthBar");
        _chargeBar = GetNode<TextureProgress>("VBoxContainer/VBoxContainer/HBoxContainer2/ChargeBar");
        _wreckedIcon = GetNode<Label>("VBoxContainer/VBoxContainer/HBoxContainer2/VBoxContainer/Control/WreckedLabel");
        _ammoIconBox = GetNode<HBoxContainer>("VBoxContainer/VBoxContainer/HBoxContainer2/VBoxContainer/Control/AmmoIconBox");
        _ammoIcons = new TextureRect[]
        {
            GetNode<TextureRect>("VBoxContainer/VBoxContainer/HBoxContainer2/VBoxContainer/Control/AmmoIconBox/AmmoSlot0"),
            GetNode<TextureRect>("VBoxContainer/VBoxContainer/HBoxContainer2/VBoxContainer/Control/AmmoIconBox/AmmoSlot1"),
            GetNode<TextureRect>("VBoxContainer/VBoxContainer/HBoxContainer2/VBoxContainer/Control/AmmoIconBox/AmmoSlot2"),
            GetNode<TextureRect>("VBoxContainer/VBoxContainer/HBoxContainer2/VBoxContainer/Control/AmmoIconBox/AmmoSlot3"),
        };
        _statusLabel = GetNode<Label>("VBoxContainer/VBoxContainer/HBoxContainer2/VBoxContainer/StatusLabel");
        _targetButton = GetNode<Button>("VBoxContainer/VBoxContainer/HBoxContainer/TargetButton");
        _gotoButton = GetNode<Button>("VBoxContainer/VBoxContainer/HBoxContainer/GotoButton");
        _escortButton = GetNode<Button>("VBoxContainer/VBoxContainer/HBoxContainer/EscortButton");
        _returnButton= GetNode<Button>("VBoxContainer/VBoxContainer/HBoxContainer/ReturnButton");

        _contextButton1 = GetNode<Button>("VBoxContainer/HBoxContainer/ContextButton1");
        _contextButton2 = GetNode<Button>("VBoxContainer/HBoxContainer/ContextButton2");

        _contextButton1.Visible = false;
        _contextButton2.Visible = false;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (MinisubHangar.MinisubType != null)
        {
            _subIcon.Texture = MinisubHangar.MinisubType.Icon;
        }
        else
        {
            _subIcon.Texture = null;
        }

        var minisubWrecked = IsInstanceValid(MinisubHangar.ActiveMinisub) && MinisubHangar.ActiveMinisub.IsWrecked;


        _targetButton.Disabled = MinisubHangar.MinisubType == null
            || _gameplayUi.CurrentTarget == null
            || minisubWrecked
            || MinisubHangar.IsRebuilding;

        _targetButton.Pressed = MinisubHangar.ActiveMinisub != null
            && MinisubHangar.Target == _gameplayUi.CurrentTarget
            && !minisubWrecked;

        _returnButton.Disabled = MinisubHangar.MinisubType == null
            || minisubWrecked
            || MinisubHangar.IsRebuilding;

        _returnButton.Pressed = MinisubHangar.ActiveMinisub != null
            && MinisubHangar.ShouldRecall
            && !minisubWrecked;

        _escortButton.Disabled = MinisubHangar.MinisubType == null
            || minisubWrecked
            || MinisubHangar.IsRebuilding;

        _escortButton.Pressed = MinisubHangar.ActiveMinisub != null
            && MinisubHangar.ShouldEscort
            && !minisubWrecked;

        _gotoButton.Disabled = MinisubHangar.MinisubType == null
            || minisubWrecked
            || MinisubHangar.IsRebuilding;

        _gotoButton.Pressed = MinisubHangar.ActiveMinisub != null
            && MinisubHangar.GotoPoint != null
            && !minisubWrecked;

        if(minisubWrecked)
        {
            var wreckDistance = MinisubHangar.ApproachPoint.GlobalTransform.origin.DistanceTo(MinisubHangar.ActiveMinisub.GlobalTransform.origin);

            _contextButton1.Visible = true;
            _contextButton1.Text = "Retrieve";
            _contextButton2.Visible = false;

            if(_contextButton1.Pressed && wreckDistance > Constants.MINISUB_RECOVER_DISTANCE)
            {
                _contextButton1.Pressed = false;
            }

            _contextButton1.Disabled = wreckDistance > Constants.MINISUB_RECOVER_DISTANCE;

            _statusLabel.Text = Util.DistanceToDisplay(wreckDistance, false);

            if(wreckDistance > 5 * Constants.MINISUB_RECOVER_DISTANCE)
            {
                _statusLabel.Modulate = Colors.Red;
            }
            else if (wreckDistance > Constants.MINISUB_RECOVER_DISTANCE)
            {
                _statusLabel.Modulate = Colors.Yellow;
            }
            else
            {
                _statusLabel.Modulate = Colors.Green;
            }

            if(wreckDistance < 5)
            {
                MinisubHangar.ActiveMinisub.UnWreck();
                SetRecall();
                MinisubHangar.IsRebuilding = true;
            }
        }
        else  if (_targetButton.Pressed && !_inTargetMode)
        {
            _contextButton1.Visible = true;
            _contextButton1.Text = "Force Attack";
            _contextButton2.Visible = false;
        }
        else if(!_targetButton.Pressed && _inTargetMode)
        {
            _contextButton1.Visible = false;
            _contextButton2.Visible = false;
        }

        if(MinisubHangar.ActiveMinisub == null)
        {
            _contextButton1.Visible = false;
            _contextButton1.Pressed = false;
            _contextButton2.Visible = false;
            _contextButton2.Pressed = false;
        }

        _inTargetMode = _targetButton.Pressed;

        if(_inTargetMode)
        {
            _contextButton1.Pressed = MinisubHangar.ForceShootTarget;
        }

        if (MinisubHangar.MinisubType != null)
        {
            _healthBar.Value = 100 * (MinisubHangar.CurrentHealth / MinisubHangar.MinisubType.HullType.Health);
            _chargeBar.Value = 100 * (MinisubHangar.CurrentCharge / MinisubHangar.MinisubType.MaxCharge);

            for(var i = 0; i < _ammoIcons.Length; i++)
            {
                if(i < MinisubHangar.MaxAmmo)
                {
                    _ammoIcons[i].Visible = true;
                    if (i < MinisubHangar.CurrentAmmo)
                    {
                        _ammoIcons[i].Texture = AmmoIconFull;
                    }
                    else
                    {
                        _ammoIcons[i].Texture = AmmoIconEmpty;
                    }
                }
                else
                {
                    _ammoIcons[i].Visible = false;
                }
            }
        }

        _ammoIconBox.Visible = !minisubWrecked;
        _wreckedIcon.Visible = minisubWrecked;
        
        if (minisubWrecked)
        {
            // handled elsewhere
        }
        else if(MinisubHangar.ActiveMinisub != null && !minisubWrecked)
        {
            if (MinisubHangar.ActiveMinisub.Charge <= 0)
            {
                _statusLabel.Text = "Low Power";
                _statusLabel.Modulate = Colors.Red;
            }
            else if (MinisubHangar.ShouldRecall)
            {
                _statusLabel.Text = "Returning";
                _statusLabel.Modulate = Colors.Yellow;
            }
            else
            {
                _statusLabel.Text = "Active";
                _statusLabel.Modulate = Colors.Green;
            }

            //_statusLabel.Text = MinisubHangar.ActiveMinisub.DebugText;
        }
        else if(MinisubHangar.MinisubType != null)
        {
            if(MinisubHangar.CurrentAmmo < MinisubHangar.MaxAmmo
                || MinisubHangar.CurrentHealth < MinisubHangar.MinisubType.HullType.Health
                || MinisubHangar.CurrentCharge < MinisubHangar.MinisubType.MaxCharge)
            {
                if(MinisubHangar.IsRebuilding)
                {
                    _statusLabel.Text = "Rebuilding";
                }
                else
                {
                    _statusLabel.Text = "Recovering";
                }
                
                _statusLabel.Modulate = Colors.Yellow;
            }
            else
            {
                _statusLabel.Text = "Ready";
                _statusLabel.Modulate = Colors.Green;
            }
        }
        else
        {
            _statusLabel.Text = string.Empty;
            _healthBar.Value = 0;
            _chargeBar.Value = 0;

            for (var i = 0; i < _ammoIcons.Length; i++)
            {
                _ammoIcons[i].Visible = false;
            }
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        if(IsInstanceValid(MinisubHangar.ActiveMinisub) 
            && MinisubHangar.ActiveMinisub.IsWrecked
            && _contextButton1.Pressed)
        {
            var fromTo = MinisubHangar.ApproachPoint.GlobalTransform.origin - MinisubHangar.ActiveMinisub.GlobalTransform.origin;
            var strength = MinisubHangar.ActiveMinisub.Mass * 5f;
            MinisubHangar.ActiveMinisub.AddCentralForce(fromTo.Normalized() * strength);
        }
    }

    public void SetWeapon()
    {
        _gameplayUi.ActiveWeaponSystem = MinisubHangar;
    }

    public void SetTarget()
    {
        MinisubHangar.Launch();
        MinisubHangar.Target = _gameplayUi.CurrentTarget;
        MinisubHangar.ForceShootTarget = false;
        MinisubHangar.GotoPoint = null;
        MinisubHangar.ShouldEscort = false;
        MinisubHangar.ShouldRecall = false;
        MinisubHangar.PromptGoalCheck();
    }

    public void SetGoto()
    {
        MinisubHangar.Launch();
        MinisubHangar.Target = null;
        MinisubHangar.ForceShootTarget = false;
        MinisubHangar.GotoPoint = GetViewport().GetCamera().ProjectPosition(GetViewport().Size / 2, 10000);
        MinisubHangar.ShouldEscort = false;
        MinisubHangar.ShouldRecall = false;
        MinisubHangar.PromptGoalCheck();
    }

    public void SetRecall()
    {
        MinisubHangar.Target = null;
        MinisubHangar.ForceShootTarget = false;
        MinisubHangar.GotoPoint = null;
        MinisubHangar.ShouldEscort = false;
        MinisubHangar.ShouldRecall = true;
        MinisubHangar.PromptGoalCheck();
    }

    public void SetEscort()
    {
        MinisubHangar.Launch();
        MinisubHangar.Target = null;
        MinisubHangar.ForceShootTarget = false;
        MinisubHangar.GotoPoint = null;
        MinisubHangar.ShouldEscort = true;
        MinisubHangar.ShouldRecall = false;
        MinisubHangar.PromptGoalCheck();
    }

    public void ContextAction1()
    {
        if (_inTargetMode)
        {
            MinisubHangar.ForceShootTarget = !MinisubHangar.ForceShootTarget;
            MinisubHangar.PromptGoalCheck();
        }
    }

    public void ContextAction2()
    {
    }
}
