using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class StandingsFactionOptionButton : Button
{
    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    public Faction Faction { get; set; }
    public double Relation { get; set; }
    
    public override void _Ready()
    {
        this.ResolveDependencies();

        Text = $"{Faction.Name} {Relation.ToString("F1")}";

        if(Relation >= 20)
        {
            Modulate = Colors.Green;
        }
        else if (Relation <= -20)
        {
            Modulate = Colors.Red;
        }
        else
        {
            Modulate = Colors.White;
        }
    }

    public void OnPressed()
    {
        _soundEffectPlayer.PlayUiClick();
        this.FindParent<StandingsPopup>().SetActiveFaction(Faction);
    }
}
