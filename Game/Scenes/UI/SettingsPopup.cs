using Godot;
using Godot.Collections;
using System;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class SettingsPopup : Popup
{
    private Action _closeCallback;

    [Inject]
    private ISettingsService _settingsService;
    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    private HSlider _musicSlider;
    private HSlider _effectsSlider;
    private GridContainer _keybindContainer;

    private Popup _newKeyPopup;
    private Button _newKeyOkButton;
    private Label _newKeyConfirmLabel;
    private Label _newKeyErrorLabel;
    private AudioStreamPlayer _effectsPreviewPlayer;
    private Button _fullscreenOnButton;
    private Button _fullscreenOffButton;

    private string _rebindingAction;
    private uint _rebindingScancode;
    private string _rebindActionReplace;
    private bool _inSetup;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _musicSlider = GetNode<HSlider>("PanelContainer/VBoxContainer/HBoxContainer/VBoxContainer/MusicSlider");
        _effectsSlider = GetNode<HSlider>("PanelContainer/VBoxContainer/HBoxContainer/VBoxContainer/EffectsSlider");
        _keybindContainer = GetNode<GridContainer>("PanelContainer/VBoxContainer/HBoxContainer/VBoxContainer2/ScrollContainer/GridContainer");
        _keybindContainer.ClearChildren();
        _newKeyPopup = GetNode<Popup>("NewKeyPopup");
        _newKeyOkButton = GetNode<Button>("NewKeyPopup/PanelContainer/VBoxContainer/HBoxContainer/NewKeyOkButton");
        _newKeyConfirmLabel = GetNode<Label>("NewKeyPopup/PanelContainer/VBoxContainer/NewKeyConfirmLabel");
        _newKeyErrorLabel = GetNode<Label>("NewKeyPopup/PanelContainer/VBoxContainer/NewKeyErrorLabel");
        _effectsPreviewPlayer = GetNode<AudioStreamPlayer>("EffectsPreviewPlayer");
        _fullscreenOnButton = GetNode<Button>("PanelContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer/FullscreenOnButton");
        _fullscreenOffButton = GetNode<Button>("PanelContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer/FullscreenOffButton");
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        _inSetup = false;

        if(Visible)
        {
            _settingsService.MusicVolume = (float)_musicSlider.Value / 100f;
            _settingsService.EffectsVolume = (float)_effectsSlider.Value / 100f;
            _fullscreenOnButton.Pressed = _settingsService.IsFullscreen;
            _fullscreenOffButton.Pressed = !_settingsService.IsFullscreen;
        }
    }

    public void ShowWithCallback(Action closeCallback)
    {
        _inSetup = true;
        _closeCallback = closeCallback;
        PopupCentered();

        _musicSlider.Value = 100 * _settingsService.MusicVolume;
        _effectsSlider.Value = 100 * _settingsService.EffectsVolume;

        foreach(var action in _settingsService.RebindableActions.Keys)
        {
            var label = new Label
            {
                Text = _settingsService.RebindableActions[action],
                RectMinSize = new Vector2(140, 0)
            };

            var button = new Button
            {
                RectMinSize = new Vector2(56, 0)
            };
            button.Connect("pressed", this, "RebindAction", new Godot.Collections.Array { action });

            var currentAction = InputMap.GetActionList(action).OfType<InputEventKey>().FirstOrDefault();

            if(currentAction != null)
            {
                button.Text = OS.GetScancodeString(currentAction.Scancode);
            }
            else
            {
                button.Text = "<not bound>";
            }

            _keybindContainer.AddChild(label);
            _keybindContainer.AddChild(button);
        }
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if(_newKeyPopup.Visible && @event is InputEventKey keyEvent)
        {
            _rebindActionReplace = null;
            _rebindingScancode = keyEvent.Scancode;
            _newKeyConfirmLabel.Text = OS.GetScancodeString(_rebindingScancode);
            _newKeyOkButton.Disabled = false;
            _newKeyErrorLabel.Text = String.Empty;

            foreach (var action in _settingsService.RebindableActions.Keys)
            {
                var currentAction = InputMap.GetActionList(action).OfType<InputEventKey>().FirstOrDefault();
                if (currentAction != null && currentAction.Scancode == _rebindingScancode)
                {
                    if(action == _rebindingAction)
                    {
                        _newKeyOkButton.Disabled = true;
                        _newKeyErrorLabel.Text = "Already assigned.";
                        _newKeyErrorLabel.Modulate = Colors.Red;
                    }
                    else
                    {
                        _newKeyErrorLabel.Text = $"Assigned to {_settingsService.RebindableActions[action]}.";
                        _newKeyErrorLabel.Modulate = Colors.Yellow;
                        _rebindActionReplace = action;
                    }
                }
            }
        }
    }

    public void RebindAction(string action)
    {
        _newKeyOkButton.Disabled = true;
        _rebindingAction = action;
        _rebindActionReplace = null;
        _newKeyPopup.ShowModal(true);
    }

    public void OnClose()
    {
        _soundEffectPlayer.PlayUiClick();
        _settingsService.SaveSettings();
        Hide();
        _closeCallback?.Invoke();
    }

    public void OkNewKeybind()
    {
        _soundEffectPlayer.PlayUiClick();
        if (_rebindActionReplace != null)
        {
            InputMap.ActionEraseEvents(_rebindActionReplace);

            UpdateKeybindButton(_rebindActionReplace);
        }

        InputMap.ActionEraseEvents(_rebindingAction);
        InputMap.ActionAddEvent(_rebindingAction, new InputEventKey { Scancode = _rebindingScancode });
        UpdateKeybindButton(_rebindingAction);

        _rebindingAction = null;
        _rebindActionReplace = null;
        _newKeyPopup.Hide();
    }

    public void CancelNewKeybind()
    {
        _soundEffectPlayer.PlayUiClick();
        _rebindingAction = null;
        _rebindActionReplace = null;
        _newKeyPopup.Hide();
    }

    private void UpdateKeybindButton(string action)
    {
        var friendlyName = _settingsService.RebindableActions[action];

        var rebindGridChildren = _keybindContainer.GetChildren().OfType<Control>().ToArray();

        for (var i = 0; i < rebindGridChildren.Length; i+= 2)
        {
            if (rebindGridChildren[i] is Label label && label.Text == friendlyName)
            {
                var currentAction = InputMap.GetActionList(action).OfType<InputEventKey>().FirstOrDefault();

                if (currentAction != null)
                {
                    (rebindGridChildren[i + 1] as Button).Text = OS.GetScancodeString(currentAction.Scancode);
                }
                else
                {
                    (rebindGridChildren[i + 1] as Button).Text = "<not bound>";
                }                
            }
        }
    }

    public void MusicVolumeChanged(float value)
    {
        _settingsService.MusicVolume = (float)_musicSlider.Value / 100f;
        _settingsService.SaveSettings();
    }

    public void EffectsVolumeChanged(float value)
    {
        _settingsService.EffectsVolume = (float)_effectsSlider.Value / 100f;
        _settingsService.SaveSettings();
        if(!_effectsPreviewPlayer.Playing && !_inSetup)
        {
            _effectsPreviewPlayer.Play();
        }
    }

    public void SetFullscreen(bool isFullscreen)
    {
        _soundEffectPlayer.PlayUiClick();
        _settingsService.IsFullscreen = isFullscreen;
        _settingsService.SaveSettings();
        _settingsService.ApplyDisplaySettings();
    }
}
