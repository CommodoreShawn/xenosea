using Godot;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class TorpedoVendor : VBoxContainer
{
    [Export]
    public PackedScene TorpedoDealerOptionButtonPrefab;

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IMissionTracker _missionTracker;
    [Inject]
    private IWorldManager _worldManager;
    [Inject]
    private IResourceLocator _resourceLocator;

    private VBoxContainer _vendorOptionList;
    private OptionButton _torpedoTubePicker;
    private VBoxContainer _torpedoTubeContentsList;
    private Label _torpedoTubeCapacityLabel;
    private TextureRect _pickedTorpIcon;
    private Label _pickedTorpNameLabel;
    private Label _damageLabel;
    private Label _penetrationLabel;
    private Label _speedLabel;
    private Label _fuelTimeLabel;
    private Label _costLabel;
    private Label _sizeLabel;
    private Label _descriptionLabel;
    private Button _buyButton;
    private Button _sellButton;
    private Label _errorLabel;

    private GameplayUi _gameplayUi;
    private TorpedoTube _activeTorpedoTube;
    private TorpedoType _activeTorpedoType;
    private bool _isForVendor;

    private Submarine _oldPlayerSub;
    private StationRoom _stationRoom;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _gameplayUi = this.FindParent<GameplayUi>();

        _vendorOptionList = GetNode<VBoxContainer>("HBoxContainer/VendorVBoxContainer/ScrollContainer/TorpedoOptionContainer");
        _torpedoTubePicker = GetNode<OptionButton>("HBoxContainer/TorpedoTubeVBoxContainer/OptionButton");
        _torpedoTubeContentsList = GetNode<VBoxContainer>("HBoxContainer/TorpedoTubeVBoxContainer/ScrollContainer/TorpedoOptionContainer");
        _torpedoTubeCapacityLabel = GetNode<Label>("HBoxContainer/TorpedoTubeVBoxContainer/MagazineCapacityLabel");
        _pickedTorpIcon = GetNode<TextureRect>("HBoxContainer/VBoxContainer2/HBoxContainer/PickedTorpIcon");
        _pickedTorpNameLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer/PickedTorpNameLabel");
        _damageLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer2/DamageLabel");
        _penetrationLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer2/PenetrationLabel");
        _speedLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer4/SpeedLabel");
        _fuelTimeLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer4/FuelTimeLabel");
        _costLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer5/CostLabel");
        _sizeLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer5/SizeLabel");
        _descriptionLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/DescriptionLabel");
        _buyButton = GetNode<Button>("HBoxContainer/VBoxContainer2/HBoxContainer3/BuyButton");
        _sellButton = GetNode<Button>("HBoxContainer/VBoxContainer2/HBoxContainer3/SellButton");
        _errorLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/ErrorLabel");
    }

    public void InitializeFor(StationRoom stationRoom)
    {
        _oldPlayerSub = _playerData.PlayerSub;
        _stationRoom = stationRoom;

        _vendorOptionList.ClearChildren();
        _torpedoTubeContentsList.ClearChildren();

        foreach (var tube in _playerData.PlayerSub.TorpedoTubes)
        {
            _torpedoTubePicker.AddItem(tube.Name);
        }
        _torpedoTubePicker.Selected = 0;
        SetActiveTorpedoTube(0);

        _vendorOptionList.ClearChildren();

        var setDefault = false;

        var station = (_playerData.PlayerSub.IsDockedTo as WorldStation).BackingStation;
        var industrialSupport = station.EconomicProcesses.Sum(x => x.IndustrialSupport);
        var militarySupport = station.EconomicProcesses.Sum(x => x.MilitarySupport);

        var torpedoTypes = _resourceLocator.AllTorpedoTypes
            .Where(x => industrialSupport >= x.IndustrialNeed)
            .Where(x => militarySupport >= x.MilitaryNeed)
            .Where(x => x.Operators.Contains(station.Faction))
            .ToArray();

        if (stationRoom is ArmsDealerStationRoom armsDealerStationRoom)
        {
            foreach (var torpedoType in torpedoTypes)
            {
                var button = TorpedoDealerOptionButtonPrefab.Instance() as TorpedoDealerOptionButton;
                button.TorpedoType = torpedoType;
                button.IsForVendor = true;
                _vendorOptionList.AddChild(button);

                if (!setDefault)
                {
                    setDefault = true;
                    SetActiveTorpedoType(torpedoType, true);
                }
            }
        }
    }

    public void SetActiveTorpedoType(TorpedoType torpedoType, bool isForVendor)
    {
        if (torpedoType != null)
        {
            _activeTorpedoType = torpedoType;
            _isForVendor = isForVendor;

            _pickedTorpIcon.Texture = _activeTorpedoType.Icon;
            _pickedTorpNameLabel.Text = _activeTorpedoType.Name;
            _descriptionLabel.Text = _activeTorpedoType.Description;
            _damageLabel.Text = $"Damage: {_activeTorpedoType.Damage.ToString("N0")}";
            _penetrationLabel.Text = $"Armor Penetration: {_activeTorpedoType.Penetration.ToString("N0")}";
            _speedLabel.Text = $"Speed: {_activeTorpedoType.Speed.ToString("N0")}";
            _fuelTimeLabel.Text = $"Fuel Time: {_activeTorpedoType.FuelTime.ToString("N0")}";
            _costLabel.Text = $"Cost: {_activeTorpedoType.Value.ToString("C0")}";
            _sizeLabel.Text = $"Size: {_activeTorpedoType.SizeCategory.ToString()}";
            
            if (_playerData.Money >= torpedoType.Value)
            {
                _costLabel.Modulate = Colors.White;
            }
            else
            {
                _costLabel.Modulate = Colors.Red;
            }

            if (_activeTorpedoType.SizeCategory <= _activeTorpedoTube.MaximumTorpedoSize)
            {
                _sizeLabel.Modulate = Colors.White;
            }
            else
            {
                _sizeLabel.Modulate = Colors.Red;
            }

            if (_activeTorpedoTube.TorpedoesAvailable.Count < _activeTorpedoTube.MagazineCapacity)
            {
                _torpedoTubeCapacityLabel.Modulate = Colors.White;
            }
            else
            {
                _torpedoTubeCapacityLabel.Modulate = Colors.Red;
            }

            var reputationMet = _playerData.Faction
                .GetStandingTo(_playerData.PlayerSub.IsDockedTo.Faction) >= _activeTorpedoType.ReputationRequired;

            if(!reputationMet)
            {
                _errorLabel.Text = "Your standing isn't high enough.";
            }
            else
            {
                _errorLabel.Text = string.Empty;
            }

            _buyButton.Disabled = !isForVendor
                || _playerData.Money < torpedoType.Value
                || _activeTorpedoTube.TorpedoesAvailable.Count >= _activeTorpedoTube.MagazineCapacity
                || _activeTorpedoType.SizeCategory > _activeTorpedoTube.MaximumTorpedoSize
                || !reputationMet;

            _sellButton.Disabled = isForVendor;
        }
        else
        {
            _activeTorpedoType = null;
            _pickedTorpIcon.Texture = null;
            _pickedTorpNameLabel.Text = string.Empty;
            _descriptionLabel.Text = string.Empty;
            _damageLabel.Text = string.Empty;
            _penetrationLabel.Text = string.Empty;
            _speedLabel.Text = string.Empty;
            _fuelTimeLabel.Text = string.Empty;
            _costLabel.Text = string.Empty;
            _costLabel.Modulate = Colors.White;
            _sizeLabel.Text = string.Empty;
            _sizeLabel.Modulate = Colors.White;
            _torpedoTubeCapacityLabel.Modulate = Colors.White;
            _errorLabel.Text = string.Empty;

            _buyButton.Disabled = true;
            _sellButton.Disabled = true;
        }
    }

    public void SetActiveTorpedoTube(int index)
    {
        _activeTorpedoTube = _playerData.PlayerSub.TorpedoTubes[index];

        _torpedoTubeContentsList.ClearChildren();

        foreach (var torpedo in _activeTorpedoTube.TorpedoesAvailable)
        {
            var button = TorpedoDealerOptionButtonPrefab.Instance() as TorpedoDealerOptionButton;
            button.TorpedoType = torpedo;
            button.IsForVendor = false;
            _torpedoTubeContentsList.AddChild(button);
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_playerData.PlayerSub != _oldPlayerSub)
        {
            InitializeFor(_stationRoom);
        }

        _torpedoTubeCapacityLabel.Text = $"{_activeTorpedoTube.TorpedoesAvailable.Count} / {_activeTorpedoTube.MagazineCapacity}";

        foreach(var button in _torpedoTubeContentsList.GetChildren().OfType<TorpedoDealerOptionButton>())
        {
            button.Pressed = button.TorpedoType == _activeTorpedoType && !_isForVendor;
        }
        foreach (var button in _vendorOptionList.GetChildren().OfType<TorpedoDealerOptionButton>())
        {
            button.Pressed = button.TorpedoType == _activeTorpedoType && _isForVendor;
        }
    }

    public void BuyTorpedo()
    {
        _activeTorpedoTube.TorpedoesAvailable.Add(_activeTorpedoType);
        _playerData.Money -= _activeTorpedoType.Value;

        SetActiveTorpedoType(_activeTorpedoType, _isForVendor);
        SetActiveTorpedoTube(_torpedoTubePicker.Selected);
    }

    public void SellTorpedo()
    {
        _activeTorpedoTube.TorpedoesAvailable.Remove(_activeTorpedoType);
        _playerData.Money += _activeTorpedoType.Value;

        SetActiveTorpedoType(_activeTorpedoTube.TorpedoesAvailable.FirstOrDefault(), false);

        SetActiveTorpedoTube(_torpedoTubePicker.Selected);
    }
}
