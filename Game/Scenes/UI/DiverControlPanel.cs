using Godot;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class DiverControlPanel : Control
{
    [Inject]
    private IPlayerData _playerData;

    [Export]
    public PackedScene DiveTeamStatusButtonPrefab;

    private Button _deployButton;
    private Button _returnButton;
    private VBoxContainer _diveTeamStatusButtonContainer;
    private Label _diveTeamTotalLabel;
    private GameplayUi _gameplayUi;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _deployButton = GetNode<Button>("ButtonRow/DeployButton");
        _returnButton = GetNode<Button>("ButtonRow/ReturnButton");
        _diveTeamStatusButtonContainer = GetNode<VBoxContainer>("ScrollContainer/DiveTeamButtonContainer");
        _diveTeamTotalLabel = GetNode<Label>("DiveTeamTotalLabel");
        _diveTeamStatusButtonContainer.ClearChildren();
        _gameplayUi = this.FindParent<GameplayUi>();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        var diversSelected = false;
        var validTarget = false;

        if (_playerData.PlayerSub != null)
        {
            for(var i = 0; i < _diveTeamStatusButtonContainer.GetChildCount() || i < _playerData.PlayerSub.DiveTeams.Count; i++)
            {
                if(i < _diveTeamStatusButtonContainer.GetChildCount()
                    && i < _playerData.PlayerSub.DiveTeams.Count)
                {
                    var button = _diveTeamStatusButtonContainer.GetChild<DiveTeamStatusButton>(i);
                    if (button.DiveTeam != _playerData.PlayerSub.DiveTeams[i])
                    {
                        button.DiveTeam = _playerData.PlayerSub.DiveTeams[i];
                        button.Pressed = false;
                    }
                    else
                    {
                        diversSelected = diversSelected || button.Pressed;

                        validTarget = validTarget
                            || (button.Pressed && button.DiveTeam.IsValidTarget(_gameplayUi.CurrentTarget));
                    }
                }
                else if (i < _playerData.PlayerSub.DiveTeams.Count)
                {
                    var button = DiveTeamStatusButtonPrefab.Instance() as DiveTeamStatusButton;
                    button.DiveTeam = _playerData.PlayerSub.DiveTeams[i];
                    _diveTeamStatusButtonContainer.AddChild(button);
                }
                else if (i < _diveTeamStatusButtonContainer.GetChildCount())
                {
                    _diveTeamStatusButtonContainer.GetChild<DiveTeamStatusButton>(i).QueueFree();
                }
            }

            _diveTeamTotalLabel.Text = $"Dive Teams: {_playerData.PlayerSub.DiveTeams.Count} / {_playerData.PlayerSub.MaximumDiveTeams}";
        }

        _deployButton.Disabled = !validTarget;
        _returnButton.Disabled = !diversSelected;
    }

    public void DeploySelectedDivers()
    {
        foreach(var button in _diveTeamStatusButtonContainer.GetChildren().OfType<DiveTeamStatusButton>())
        {
            if(button.Pressed && button.DiveTeam.IsValidTarget(_gameplayUi.CurrentTarget))
            {
                button.DiveTeam.Deploy(_gameplayUi.CurrentTarget);
            }
        }
    }

    public void RecallSelectedDivers()
    {
        foreach (var button in _diveTeamStatusButtonContainer.GetChildren().OfType<DiveTeamStatusButton>())
        {
            if (button.Pressed)
            {
                button.DiveTeam.ReturnHome();
            }
        }
    }
}
