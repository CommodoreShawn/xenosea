using Godot;
using Xenosea.Logic;

public class TorpedoOptionButton : Button
{
    public int Index { get; set; }
    public TorpedoTube TorpedoTube { get; set; }

    public override void _Process(float delta)
    {
        base._PhysicsProcess(delta);

        if(Index < TorpedoTube.TorpedoesAvailable.Count)
        {
            Icon = TorpedoTube.TorpedoesAvailable[Index].Icon;
            HintTooltip = TorpedoTube.TorpedoesAvailable[Index].Name;
        }
        else
        {
            QueueFree();
        }
    }

    public void OnPressed()
    {
        this.FindParent<TorpedoTubeWidget>().SetAmmo(Index);
    }
}
