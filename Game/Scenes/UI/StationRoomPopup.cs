using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class StationRoomPopup : Popup
{
    [Export]
    public PackedScene NpcTalkButtonPrefab;
    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    private PanelContainer _panelContainer;
    private GridContainer _npcChatRow;
    
    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();

        _panelContainer = GetNode<PanelContainer>("PanelContainer");
        _npcChatRow = GetNode<GridContainer>("PanelContainer/VBoxContainer/NpcChatRow");
    }

    public void ShowFor(StationRoom stationRoom)
    {
        GetNode<Label>("PanelContainer/VBoxContainer/TitleLabel").Text = stationRoom.FriendlyName.ToUpper();

        if (stationRoom.StationRoomWidgetPrefab != null)
        {
            GetNode<Control>("PanelContainer/VBoxContainer/ContentContainer").ClearChildren();

            var contents = stationRoom.StationRoomWidgetPrefab.Instance() as Control;

            GetNode<Control>("PanelContainer/VBoxContainer/ContentContainer").AddChild(contents);

            if(contents is IStationRoomWidgetContents stationRoomWidgetContents)
            {
                stationRoomWidgetContents.InitializeFor(stationRoom);
            }
        }
        else
        {
            GetNode<Control>("PanelContainer/VBoxContainer/ContentContainer").ClearChildren();

            var body = string.Empty;
            var station = stationRoom.FindParent<IStation>();
            foreach (var key in station.CommodityStorage.CommoditiesPresent)
            {
                body += $"{key.Name}: {station.CommodityStorage.GetQty(key)} at {station.PriceOf(key)}\n";
            }

            var contents = new Label()
            {
                Autowrap = true,
                Text = body,
                RectMinSize = new Vector2(200, 0)
            };

            GetNode<Control>("PanelContainer/VBoxContainer/ContentContainer").AddChild(contents);
        }

        _npcChatRow.ClearChildren();
        foreach(var npc in stationRoom.NpcsPresent)
        {
            var button = NpcTalkButtonPrefab.Instance() as NpcTalkButton;
            button.Npc = npc;
            _npcChatRow.AddChild(button);
        }

        _panelContainer.RectSize = Vector2.Zero;

        PopupCentered();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _panelContainer.RectPosition = (GetViewport().Size - _panelContainer.RectSize) / 2;

    }

    public void ClosePopup()
    {
        _soundEffectPlayer.PlayUiClick();
        GetNode<Control>("PanelContainer/VBoxContainer/ContentContainer").ClearChildren();
        Hide();
    }
}
