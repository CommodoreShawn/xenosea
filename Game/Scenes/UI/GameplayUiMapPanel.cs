using Godot;
using System.Linq;

public class GameplayUiMapPanel : PanelContainer
{
    [Export]
    public PackedScene MapscreenPrefab;

    private Button[] _modeButtons;
    private Control[] _contentContainers;
    private int _mapPanelMode;

    public override void _Ready()
    {
        _modeButtons = GetNode<Control>("VBoxContainer/HBoxContainer/ModeSelectButtons")
            .GetChildren()
            .OfType<Button>()
            .ToArray();

        _contentContainers = GetNode<Control>("VBoxContainer/HBoxContainer/ContentContainer")
            .GetChildren()
            .OfType<Control>()
            .ToArray();
    }

    public override void _Process(float delta)
    {
        for(var i = 0; i < _modeButtons.Length; i++)
        {
            _modeButtons[i].Pressed = i == _mapPanelMode;
            _contentContainers[i].Visible = i == _mapPanelMode;
        }
    }

    public void SetMode(int newMode)
    {
        _mapPanelMode = newMode;
    }

    public void ShowFullMap()
    {
        GetTree().Root.AddChild(MapscreenPrefab.Instance());
    }
}
