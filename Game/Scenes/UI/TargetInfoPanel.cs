using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;
using Xenosea.Resources;

public class TargetInfoPanel : VBoxContainer
{
    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private ISimulationEventBus _simulationEventBus;

    private GameplayUi _gameplayUi;
    private LoadoutPopup _loadoutPopup;
    private Label _targetInfoNameLabel;
    private Label _targetInfoTypeLabel;
    private Label _targetInfoDetectionStatusLabel;
    private Label _targetInfoOwnerLabel;
    private Button _targetInfoCargoScanButton;
    private Button _targetInfoHailButton;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _gameplayUi = this.FindParent<GameplayUi>();

        _loadoutPopup = _gameplayUi.GetNode<LoadoutPopup>("LoadoutPopup");
        _targetInfoNameLabel = GetNode<Label>("VBoxContainer/TargetNameLabel");
        _targetInfoTypeLabel = GetNode<Label>("VBoxContainer/GridContainer/TargetTypeLabel");
        _targetInfoDetectionStatusLabel = GetNode<Label>("VBoxContainer/GridContainer/TargetInfoStatusLabel");
        _targetInfoOwnerLabel = GetNode<Label>("VBoxContainer/GridContainer/TargetOwnerLabel");
        _targetInfoHailButton = GetNode<Button>("VBoxContainer/ButtonRow/HailButton");
        _targetInfoCargoScanButton = GetNode<Button>("VBoxContainer/ButtonRow/ScanButton");
    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        if (_gameplayUi.CurrentTarget != null)
        {
            _targetInfoNameLabel.Text = _gameplayUi.CurrentTarget.Identified ? _gameplayUi.CurrentTarget.FriendlyName : "Unknown Contact";
            _targetInfoTypeLabel.Text = _gameplayUi.CurrentTarget.TypeKnown ? _gameplayUi.CurrentTarget.ContactType.ToString() : string.Empty;

            if (_gameplayUi.CurrentTarget.Identified)
            {
                _targetInfoDetectionStatusLabel.Text = "Identified";
            }
            else if (_gameplayUi.CurrentTarget.PositionLocked)
            {
                _targetInfoDetectionStatusLabel.Text = "Position Locked";
            }
            else if (_gameplayUi.CurrentTarget.TypeKnown)
            {
                _targetInfoDetectionStatusLabel.Text = "Type Known";
            }
            else if (_gameplayUi.CurrentTarget.IsDetected)
            {
                _targetInfoDetectionStatusLabel.Text = "Detected";
            }
            else
            {
                _targetInfoDetectionStatusLabel.Text = string.Empty;
            }

            if (_gameplayUi.CurrentTarget.Identified 
                && _gameplayUi.CurrentTarget is SonarContactRecord scr
                && scr.SonarContact is Submarine targetSub)
            {
                _targetInfoOwnerLabel.Text = targetSub.Faction.Name;

                if (targetSub.Faction.IsHostileTo(_playerData.Faction))
                {
                    _targetInfoOwnerLabel.Modulate = Colors.Red;
                }
                else if (targetSub.Faction.IsFriendlyTo(_playerData.Faction))
                {
                    _targetInfoOwnerLabel.Modulate = Colors.Green;
                }
                else
                {
                    _targetInfoOwnerLabel.Modulate = Colors.White;
                }

                _targetInfoCargoScanButton.Disabled = !scr.CargoScan;
                _targetInfoHailButton.Disabled = targetSub.ContactType == ContactType.Wreck || targetSub.ContactType == ContactType.Torpedo;

                if (scr.CargoScan)
                {
                    _targetInfoDetectionStatusLabel.Text = "Cargo Scan";
                }
                else if (_loadoutPopup.Visible && _loadoutPopup.Submarine != _playerData.PlayerSub)
                {
                    _loadoutPopup.Close();
                }
            }
            else if (_gameplayUi.CurrentTarget.Identified 
                && _gameplayUi.CurrentTarget is SonarContactRecord scr2
                && scr2.SonarContact is ISalvageTarget salvageTarget)
            {
                _targetInfoOwnerLabel.Text = string.Empty;

                _targetInfoCargoScanButton.Disabled = !scr2.CargoScan;
                _targetInfoHailButton.Disabled = true;

                if (scr2.CargoScan)
                {
                    _targetInfoDetectionStatusLabel.Text = "Cargo Scan";
                }
                else if (_loadoutPopup.Visible && _loadoutPopup.Submarine != _playerData.PlayerSub)
                {
                    _loadoutPopup.Close();
                }
            }
            else
            {
                _targetInfoOwnerLabel.Text = string.Empty;
                _targetInfoCargoScanButton.Disabled = true;
                _targetInfoHailButton.Disabled = true;

                if (_loadoutPopup.Visible && _loadoutPopup.Submarine != _playerData.PlayerSub)
                {
                    _loadoutPopup.Close();
                }
            }
        }
        else
        {
            _targetInfoNameLabel.Text = string.Empty;
            _targetInfoTypeLabel.Text = string.Empty;
            _targetInfoDetectionStatusLabel.Text = string.Empty;
            _targetInfoOwnerLabel.Text = string.Empty;
            _targetInfoHailButton.Disabled = true;
            _targetInfoCargoScanButton.Disabled = true;

            if (_loadoutPopup.Visible && _loadoutPopup.Submarine != _playerData.PlayerSub)
            {
                _loadoutPopup.Close();
            }
        }
    }

    public void CargoScanTarget()
    {
        var targetSub = (_gameplayUi.CurrentTarget as SonarContactRecord)?.SonarContact as Submarine;
        var salvageTarget = (_gameplayUi.CurrentTarget as SonarContactRecord)?.SonarContact as ISalvageTarget;

        if (targetSub != null)
        {
            _loadoutPopup.ShowFor(targetSub);
        }
        else if (salvageTarget != null)
        {
            _loadoutPopup.ShowFor(salvageTarget);
        }
    }

    public void HailTarget()
    {
        var targetSub = (_gameplayUi.CurrentTarget as SonarContactRecord)?.SonarContact as Submarine;

        if (targetSub != null)
        {
            _simulationEventBus.SendEvent(new PlayerHailSimulationEvent(targetSub));
        }
    }
}
