using Godot;
using Xenosea.Resources.Cinematics;
using Xenosea.Logic;

public class VoiceoverDisplay : Control
{
    private AudioStreamPlayer _audioStreamPlayer;
    private Label _label;
    
    public override void _Ready()
    {
        _audioStreamPlayer = GetNode<AudioStreamPlayer>("AudioStreamPlayer");
        _label = GetNode<Label>("Label");
    }

    public void Play(VoiceoverPart voiceoverPart)
    {
        if (voiceoverPart.VoiceoverStream != null)
        {
            _audioStreamPlayer.Stream = voiceoverPart.VoiceoverStream;
            _audioStreamPlayer.Play();
        }
        else if (_audioStreamPlayer.Playing)
        {
            _audioStreamPlayer.Stop();
        }

        _label.Text = voiceoverPart.Subtitle;
    }

    public void Skip()
    {
        this.FindParent<Gameplay>().SkipIntro();
    }
}
