using Godot;
using System;

public class SubmarineHailOptionButton : Button
{
    public Action Callback { get; set; }
    
    public void OnPressed()
    {
        Callback?.Invoke();
    }
}
