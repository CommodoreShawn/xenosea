using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

public class Mapscreen : Spatial
{
    private static readonly float CAMERA_DISTANCE = 600;
    private static readonly float CURRENT_DISTANCE = 401;
    private static readonly float OBJECT_DISTANCE = 402;
    private static readonly float GLOBE_RADIUS = 400;

    [Export]
    public PackedScene MapObjectButtonPrefab;
    [Export]
    public Material Material;
    [Export]
    public PackedScene MapObjectRepresentationPrefab;
    [Export]
    public PackedScene MapObjectCurrentPrefab;
    [Export]
    public PackedScene StormIndicatorPrefab;
    [Export]
    public PackedScene NotificationItemPrefab;

    private TextureButton _filterStationsButton;
    private TextureButton _filterSubsButton;
    private TextureButton _filterWrecksButton;
    private TextureButton _filterLandmarksButton;
    private TextureButton _filterTorpedoesButton;

    private Button _showCurrentsButton;

    private VBoxContainer _mapObjectButtonContainer;
    private MeshInstance _globeMesh;
    private MapscreenCamera _camera;

    private Button _time1;
    private Button _time2;
    private Button _time4;
    private Label _dateTimeLabel;
    private PanelContainer _tooltipContainer;
    private Label _tooltipLabel;

    private List<MapObjectRepresentation> _objectRepresentations;
    private List<MapCurrentRepresentation> _currentRepresentations;
    private Dictionary<WeatherSystem, StormIndicator> _stormIndicators;

    private Label _readyForCruiseLabel;
    private Label _hostilesNearbyLabel;
    private Label _dockedToStationLabel;
    private Label _diversDeployedLabel;
    private Label _minisubDeployedLabel;
    private Label _torpedoesNearbyLabel;
    private Label _noWaypointLabel;
    private Label _hazarousAreaLabel;
    private Label _cruisingLabel;
    private Button _cruiseModeButton;
    private Button _cruiseFastButton;
    private Button _cruiseStealthButton;

    private Label _missionInfoLabel;
    private VBoxContainer _notificationBox;

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IWorldManager _worldManager;
    [Inject]
    private IMissionTracker _missionTracker;
    [Inject]
    private INotificationSystem _notificationSystem;

    private IMapObject _hoveredMapObject;

    private Gameplay _gameplay;

    private float _rebuildTimer;

    private bool _inCruiseMode;

    public override void _Ready()
    {
        this.ResolveDependencies();
        Input.MouseMode = Input.MouseModeEnum.Visible;
        _objectRepresentations = new List<MapObjectRepresentation>();

        _globeMesh = GetNode<MeshInstance>("GlobeMesh");
        _mapObjectButtonContainer = GetNode<VBoxContainer>("VBoxContainer/ScrollContainer/MapObjectButtonContainer");
        _mapObjectButtonContainer.ClearChildren();

        _filterStationsButton = GetNode<TextureButton>("VBoxContainer/HBoxContainer3/FilterStationsButton");
        _filterSubsButton = GetNode<TextureButton>("VBoxContainer/HBoxContainer3/FilterSubsButton");
        _filterWrecksButton = GetNode<TextureButton>("VBoxContainer/HBoxContainer3/FilterWrecksButton");
        _filterLandmarksButton = GetNode<TextureButton>("VBoxContainer/HBoxContainer3/FilterLandmarksButton");
        _filterTorpedoesButton = GetNode<TextureButton>("VBoxContainer/HBoxContainer3/FilterTorpedoesButton");

        _readyForCruiseLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/CruiseReadyLabel");
        _hostilesNearbyLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/HostilesNearbyLabel");
        _dockedToStationLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/DockedToStationLabel");
        _diversDeployedLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/DiversDeployedLabel");
        _minisubDeployedLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/MinisubsDeployedLabel");
        _torpedoesNearbyLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/TorpedoesNearbyLabel");
        _noWaypointLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/NoWaypointLabel");
        _hazarousAreaLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/HazardousAreaLabel");
        _cruisingLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/CruisingLabel");
        _cruiseModeButton = GetNode<Button>("RightStack/PanelContainer/VBoxContainer/HBoxContainer/CruiseButton");
        _cruiseFastButton = GetNode<Button>("RightStack/PanelContainer/VBoxContainer/HBoxContainer2/CruiseSpeedButton");
        _cruiseStealthButton = GetNode<Button>("RightStack/PanelContainer/VBoxContainer/HBoxContainer2/CruiseStealthButton");

        _playerData.PlayerController.ControlLockout = true;
        _camera = GetNode<MapscreenCamera>("MapscreenCamera");
        _camera.Current = true;
        GetTree().Root.GetNode<GameplayUi>("Gameplay/UI").HideForOtherUi = true;
        _gameplay = GetTree().Root.GetNode<Gameplay>("Gameplay");

        RebuildWorld();

        _camera.CameraDistance = CAMERA_DISTANCE;
        _camera.SetLookAt(_playerData.PlayerSub.RealPosition.Normalized() * CAMERA_DISTANCE);

        _time1 = GetNode<Button>("RightStack/PanelContainer/VBoxContainer/HBoxContainer/Time1");
        _time2 = GetNode<Button>("RightStack/PanelContainer/VBoxContainer/HBoxContainer/Time2");
        _time4 = GetNode<Button>("RightStack/PanelContainer/VBoxContainer/HBoxContainer/Time4");
        _dateTimeLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/DateTimeLabel");

        _tooltipContainer = GetNode<PanelContainer>("TooltipContainer");
        _tooltipLabel = GetNode<Label>("TooltipContainer/Label");
        _currentRepresentations = new List<MapCurrentRepresentation>();
        _showCurrentsButton = GetNode<Button>("VBoxContainer/ShowCurrentsButton");

        _missionInfoLabel = GetNode<Label>("RightStack/MissionLabel");
        _notificationBox = GetNode<VBoxContainer>("NotificationContainer/NotificationPanel");

        _stormIndicators = new Dictionary<WeatherSystem, StormIndicator>();

        _rebuildTimer = 3;

        _inCruiseMode = false;

        _notificationSystem.OnNotification += _notificationSystem_OnNotification;
    }

    public override void _ExitTree()
    {
        base._ExitTree();
        _notificationSystem.OnNotification -= _notificationSystem_OnNotification;
    }

    private void _notificationSystem_OnNotification(Notification notification)
    {
        var newItem = NotificationItemPrefab.Instance() as NotificationItem;
        newItem.Init(notification);
        _notificationBox.AddChild(newItem);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_playerData.PlayerSub == null || !IsInstanceValid(_playerData.PlayerSub))
        {
            return;
        }

        _cruiseStealthButton.Pressed = _playerData.CruiseStealthMode;
        _cruiseFastButton.Pressed = !_playerData.CruiseStealthMode;

        if (Engine.TimeScale > 1)
        {
            delta /= Engine.TimeScale;
        }
        if (delta == 0)
        {
            delta = 0.008f;
        }

        _rebuildTimer -= delta;
        if(_rebuildTimer <= 0)
        {
            _rebuildTimer = 3;
            RebuildWorld();
        }

        var mapObjects = _playerData.KnownPointsOfInterest
            .OfType<IMapObject>()
            .Concat(_playerData.PlayerSub.SonarContactRecords
                    .Where(x => x.Value.IsDetected)
                    .Select(x => x.Value))
            .ToList();

        if(Constants.DEBUG_AGENT_DISPLAY)
        {
            mapObjects.AddRange(_worldManager.SimulationAgents
                .Select(x => new KnownPointOfInterest
                {
                    ContactType = Xenosea.Resources.ContactType.Sub,
                    Location = x.Location,
                    ReferenceId = x.Id
                }));
        }

        if (_missionTracker.TrackedMission?.MissionPath != null && _missionTracker.TrackedMission.MissionPath.Waypoints.Any())
        {
            mapObjects.Add(_missionTracker.TrackedMission.MissionPath);
        }

        mapObjects = mapObjects
            .GroupBy(x => x.ReferenceId).Select(x => x.First())
            .Where(x => (x.ContactType == ContactType.Station && _filterStationsButton.Pressed)
                    || (x.ContactType == ContactType.Sub && _filterSubsButton.Pressed)
                    || (x.ContactType == ContactType.Minisub && _filterSubsButton.Pressed)
                    || (x.ContactType == ContactType.Wreck && _filterWrecksButton.Pressed)
                    || (x.ContactType == ContactType.Landmark && _filterLandmarksButton.Pressed)
                    || x.ContactType == ContactType.Waypoint
                    || (x.ContactType == ContactType.Torpedo && _filterTorpedoesButton.Pressed)
                    || !x.TypeKnown
                    || x.ReferenceId == "playersub")
            .OrderBy(x => x.FriendlyName)
            .ToList();

        if(_playerData.ManualWaypoint != null)
        {
            mapObjects.Insert(0, _playerData.ManualWaypoint);
        }

        var buttons = _mapObjectButtonContainer
            .GetChildren()
            .OfType<MapObjectButton>()
            .ToArray();

        for (var i = 0; i < mapObjects.Count || i < buttons.Length || i < _objectRepresentations.Count; i++)
        {
            if (i < mapObjects.Count && i < buttons.Length)
            {
                if (buttons[i].MapObject != mapObjects[i])
                {
                    buttons[i].MapObject = mapObjects[i];
                    buttons[i].RefreshContent();
                    buttons[i].Visible = mapObjects[i].ContactType != ContactType.Torpedo;
                }

                if(mapObjects[i] == _hoveredMapObject)
                {
                    buttons[i].Modulate = Colors.Yellow;
                }
                else
                {
                    buttons[i].Modulate = Colors.White;
                }

            }
            else if (i < mapObjects.Count)
            {
                var newButton = MapObjectButtonPrefab.Instance() as MapObjectButton;
                newButton.MapObject = mapObjects[i];
                newButton.Visible = mapObjects[i].ContactType != ContactType.Torpedo;
                _mapObjectButtonContainer.AddChild(newButton);
            }
            else if (i < buttons.Length)
            {
                buttons[i].QueueFree();
            }

            if (i < mapObjects.Count && i < _objectRepresentations.Count)
            {
                if (_objectRepresentations[i].MapObject != mapObjects[i])
                {
                    _objectRepresentations[i].MapObject = mapObjects[i];
                    _objectRepresentations[i].RefreshContent();
                }
                else if(_objectRepresentations[i].WasTypeKnown != mapObjects[i].TypeKnown)
                {
                    _objectRepresentations[i].RefreshContent();
                }

                var depthAdj = (Constants.WORLD_RADIUS - mapObjects[i].EstimatedPosition.Length()) / Constants.MAX_DEPTH;
                
                var position = mapObjects[i].EstimatedPosition.Normalized() * (OBJECT_DISTANCE - depthAdj);

                var iconFacingVector = Vector3.Up;

                if(mapObjects[i].ReferenceId == "playersub")
                {
                    iconFacingVector = _playerData.PlayerSub.GlobalTransform.basis.z;
                }
                else if (mapObjects[i].TypeKnown 
                    && (
                        mapObjects[i].ContactType == ContactType.Sub 
                        || mapObjects[i].ContactType == ContactType.Torpedo
                        || mapObjects[i].ContactType == ContactType.Minisub)
                    && mapObjects[i] is SonarContactRecord scr 
                    && scr.SonarContact is Spatial spat)
                {
                    iconFacingVector = spat.GlobalTransform.basis.z;
                }

                _objectRepresentations[i].LookAtFromPosition(
                    position,
                    position.Normalized() * 1000,
                    iconFacingVector);
            }
            else if (i < mapObjects.Count)
            {
                var newRepresentation = MapObjectRepresentationPrefab.Instance() as MapObjectRepresentation;
                newRepresentation.MapObject = mapObjects[i];
                newRepresentation.PlayerData = _playerData;
                AddChild(newRepresentation);
                _objectRepresentations.Add(newRepresentation);
            }
            else if (i < _objectRepresentations.Count)
            {
                _objectRepresentations[i].QueueFree();
                _objectRepresentations[i] = null;
            }
        }

        _objectRepresentations.RemoveAll(x => x == null);

        if (_playerData.InBattle && Engine.TimeScale > 1)
        {
            if(_inCruiseMode)
            {
                ToggleCruiseMode();
            }

            Engine.TimeScale = 1;
        }

        _time1.Disabled = false;
        _time2.Disabled = _playerData.InBattle;
        _time4.Disabled = _playerData.InBattle;

        _time1.Pressed = Engine.TimeScale == 1;
        _time2.Pressed = Engine.TimeScale == 2;
        _time4.Pressed = Engine.TimeScale == 4;
        _dateTimeLabel.Text = _worldManager.CurrentDate.ToString();

        _hoveredMapObject = null;
        if (!Input.IsMouseButtonPressed((int)ButtonList.Left)
            && !Input.IsMouseButtonPressed((int)ButtonList.Right))
        {
            var pickFrom = _camera.ProjectRayOrigin(GetViewport().GetMousePosition());
            var pickTo = pickFrom + _camera.ProjectRayNormal(GetViewport().GetMousePosition()) * 5000;
            var collisions = GetWorld().DirectSpaceState.IntersectRay(pickFrom, pickTo, collideWithAreas: true);

            if (collisions.Count > 0 && collisions["collider"] is MapObjectRepresentation objRep)
            {
                _hoveredMapObject = objRep.MapObject;
            }
        }

        _tooltipContainer.Visible = _hoveredMapObject != null;
        if(_hoveredMapObject != null)
        {
            if(_hoveredMapObject.Identified)
            {
                _tooltipLabel.Text = _hoveredMapObject.FriendlyName;
            }
            else
            {
                _tooltipLabel.Text = "Unknown Contact";
            }

            var depth = Constants.WORLD_RADIUS - _hoveredMapObject.EstimatedPosition.Length();
            _tooltipLabel.Text += $"\nDepth: {Util.DistanceToDisplay(depth, false)}";

            if (_hoveredMapObject is SonarContactRecord scr)
            {
                _tooltipLabel.Text += $"\nSpeed: {scr.LastKnownVelocity.Length().ToString("N0")}";
            }
            else if(_hoveredMapObject.ReferenceId == "playersub")
            {
                _tooltipLabel.Text += $"\nSpeed: {_playerData.PlayerSub.Velocity.Length().ToString("N0")}";
            }

            _tooltipContainer.RectPosition = _camera.UnprojectPosition(
                _hoveredMapObject.EstimatedPosition.Normalized() * OBJECT_DISTANCE)
                + new Vector2(10, -10);
        }


        var nearbyDatums = _gameplay.ActiveDatums
            .OrderBy(d => d.Position.DistanceTo(_playerData.PlayerSub.RealPosition))
            .Take(20)
            .ToArray();

        //for(var i = 0; i < nearbyDatums.Length || i < _currentRepresentations.Count; i++)
        //{
        //    if(i < _currentRepresentations.Count && i < nearbyDatums.Length)
        //    {
        //        if(nearbyDatums[i] != _currentRepresentations[i].Datum)
        //        {
        //            _currentRepresentations[i].Datum = nearbyDatums[i];
        //            _currentRepresentations[i].RefreshContent();
                    
        //            var position = nearbyDatums[i].Position.Normalized() * CURRENT_DISTANCE;
        //            _currentRepresentations[i].LookAtFromPosition(
        //                position,
        //                position.Normalized() * 1000,
        //                nearbyDatums[i].CurrentFlow.Normalized());
        //        }

        //        _currentRepresentations[i].Visible = _showCurrentsButton.Pressed;
        //    }
        //    else if(i < _currentRepresentations.Count)
        //    {
        //        _currentRepresentations[i].QueueFree();
        //        _currentRepresentations[i] = null;
        //    }
        //    else if(i < nearbyDatums.Length)
        //    {
        //        var representation = MapObjectCurrentPrefab.Instance() as MapCurrentRepresentation;
        //        representation.Datum = nearbyDatums[i];
        //        _currentRepresentations.Add(representation);
        //        AddChild(representation);

        //        var position = nearbyDatums[i].Position.Normalized() * OBJECT_DISTANCE;
        //        representation.LookAtFromPosition(
        //            position,
        //            position.Normalized() * 1000,
        //            nearbyDatums[i].CurrentFlow.Normalized());

        //        representation.Visible = _showCurrentsButton.Pressed;
        //    }
        //}

        _currentRepresentations.RemoveAll(x => x == null);

        var nearbyWeatherSystems = _worldManager.WeatherSystems
            .Where(x => x.Position.DistanceTo(_playerData.PlayerSub.RealPosition) < Constants.EXPLORATION_DISTANCE + x.Radius)
            .ToArray();

        foreach (var weatherSystem in nearbyWeatherSystems)
        {
            if (!_stormIndicators.ContainsKey(weatherSystem))
            {
                var indicator = StormIndicatorPrefab.Instance() as StormIndicator;
                indicator.WeatherSystem = weatherSystem;
                indicator.SizeScale = 1 / 490f;
                AddChild(indicator);
                _stormIndicators.Add(weatherSystem, indicator);
            }
        }

        var weatherSystemsToRemove = _stormIndicators.Keys
            .Where(x => x.Energy < 1 || x.Position.DistanceTo(_playerData.PlayerSub.RealPosition) > Constants.EXPLORATION_DISTANCE + x.Radius)
            .ToArray();

        foreach(var weatherSystem in weatherSystemsToRemove)
        {
            _stormIndicators[weatherSystem].QueueFree();
            _stormIndicators.Remove(weatherSystem);
        }

        _hostilesNearbyLabel.Visible = _playerData.PlayerSub.SonarContactRecords.Values
            .Where(x => x.IsValidInstance)
            .Any(x => x.Identified && x.SonarContact.IsHostileTo(_playerData.PlayerSub) && x.ContactType != ContactType.Wreck && x.ContactType != ContactType.Station);

        _dockedToStationLabel.Visible = _playerData.PlayerSub.IsDockedTo != null;

        _diversDeployedLabel.Visible = _playerData.PlayerSub.DiveTeams
            .Any(x => x.DeployedDiver != null);

        _minisubDeployedLabel.Visible = _playerData.PlayerSub.MinisubHangars
            .Any(x => x.ActiveMinisub != null && !x.ActiveMinisub.IsWrecked);

        _torpedoesNearbyLabel.Visible = _playerData.PlayerSub.SonarContactRecords.Values
            .Where(x => x.ContactType == ContactType.Torpedo)
            .Where(x => x.IsValidInstance)
            .Any(x => x.SonarContact.RealPosition.DistanceTo(_playerData.PlayerSub.RealPosition) < 5000);

        _noWaypointLabel.Visible = _playerData.ManualWaypoint == null
            && _missionTracker.TrackedMission?.MissionPath == null;


        _hazarousAreaLabel.Visible = false; // TODO the thing

        _cruisingLabel.Visible = _inCruiseMode;

        var canCruise = !_hostilesNearbyLabel.Visible
            && !_dockedToStationLabel.Visible
            && !_diversDeployedLabel.Visible
            && !_minisubDeployedLabel.Visible
            && !_torpedoesNearbyLabel.Visible
            && !_noWaypointLabel.Visible
            && !_hazarousAreaLabel.Visible;

        _readyForCruiseLabel.Visible = canCruise
            && !_cruisingLabel.Visible;

        _cruiseModeButton.Disabled = !canCruise;
        _cruiseModeButton.Pressed = _inCruiseMode;

        if(_inCruiseMode && !canCruise)
        {
            SetCruiseMode(false);
        }

        if(_inCruiseMode)
        {
            _playerData.PlayerController.AutopilotThrottleLimit = _playerData.CruiseStealthMode ? 0.5f : 1.0f;
            _playerData.PlayerSub.DeployTowedArray = _playerData.CruiseStealthMode && _playerData.PlayerSub.Velocity.Length() <= 20;
            _playerData.PlayerController.AutopilotType = AutopilotType.Goto;
            _playerData.PlayerController.AutopilotTarget = (ITargetableThing)_playerData.ManualWaypoint ?? _missionTracker.TrackedMission.MissionPath;
        }

        if (_missionTracker.TrackedMission != null)
        {
            if (!_missionTracker.IsHeldByAgent(_playerData.PlayerSub, _missionTracker.TrackedMission)
                && !(_missionTracker.TrackedMission is IQuest quest && quest.IsActive))
            {
                _missionInfoLabel.Text = string.Empty;
            }
            else
            {
                _missionInfoLabel.Text = string.Join("\n", _missionTracker.TrackedMission.Name, _missionTracker.TrackedMission.Detail0, _missionTracker.TrackedMission.Detail1, _missionTracker.TrackedMission.Detail2).Trim();
            }
        }
        else
        {
            _missionInfoLabel.Text = string.Empty;
        }
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if (@event is InputEventMouseButton mouseButtonEvent)
        {
            if(mouseButtonEvent.ButtonIndex == (int)ButtonList.Left
                && mouseButtonEvent.IsPressed()
                && !Input.IsMouseButtonPressed((int)ButtonList.Right))
            {
                var pickFrom = _camera.ProjectRayOrigin(GetViewport().GetMousePosition());
                var pickTo = pickFrom + _camera.ProjectRayNormal(GetViewport().GetMousePosition()) * 5000;
                var collisions = GetWorld().DirectSpaceState.IntersectRay(pickFrom, pickTo, collideWithAreas: true);

                if (collisions.Count > 0)
                {
                    var hitPoint = ((Vector3)collisions["position"]).Normalized() * Constants.WORLD_RADIUS;
                    var waypointPos = hitPoint;

                    var nearDatum = _worldManager.WorldDatums
                        .OrderBy(d => (d.Position - hitPoint).LengthSquared())
                        .FirstOrDefault();

                    if(nearDatum.IceDepth < nearDatum.Depth)
                    {
                        var depth = (nearDatum.Depth - nearDatum.IceDepth) / 2 + nearDatum.IceDepth;

                        waypointPos = waypointPos - depth * waypointPos.Normalized();
                    }

                    if(_playerData.ManualWaypoint == null)
                    {
                        _playerData.ManualWaypoint = new SimpleWaypoint("Waypoint", "manualwaypoint", waypointPos);
                    }
                    else if(_playerData.ManualWaypoint.EstimatedPosition.DistanceTo(waypointPos) < 1000)
                    {
                        _playerData.ManualWaypoint.IsDetected = false;
                        _playerData.ManualWaypoint = null;
                    }
                    else
                    {
                        _playerData.ManualWaypoint.UpdatePosition(waypointPos);
                    }
                }
            }
        }
    }

    public void Close()
    {
        if(_inCruiseMode)
        {
            SetCruiseMode(false);
        }
        _playerData.PlayerController.AutopilotThrottleLimit = 1.0f;

        Input.MouseMode = Input.MouseModeEnum.Visible;
        _playerData.PlayerController.ControlLockout = false;
        GetTree().Root.GetNode<GameplayUi>("Gameplay/UI").HideForOtherUi = false;
        GetTree().Root.GetNode<Camera>("Gameplay/Camera").Current = true;
        QueueFree();
    }

    public void CenterView(IMapObject mapObject)
    {
        var position = mapObject.EstimatedPosition.Normalized() * CAMERA_DISTANCE;
        
        _camera.PanToLookAt(position);
    }

    private void RebuildWorld()
    {
        var surfaceTool = new SurfaceTool();
        surfaceTool.Begin(Mesh.PrimitiveType.Triangles);

        var maxDepth = _worldManager.WorldDatums
            .Max(d => d.Depth);

        var datumRows = _worldManager.WorldDatums
            .GroupBy(x => x.Latitude)
            .OrderBy(g => g.Key)
            .Select(g => g.ToArray())
            .ToArray();

        for (var rowIndex = 0; rowIndex < datumRows.Length - 1; rowIndex++)
        {
            for (var colIndex = 0; colIndex < datumRows[rowIndex].Length; colIndex++)
            {
                var nextRow = rowIndex + 1;
                var percInRow = colIndex / (float)datumRows[rowIndex].Length;
                var nextPercInRow = ((colIndex + 1) % datumRows[rowIndex].Length) / (float)datumRows[rowIndex].Length;
                var nextRowIndex = Mathf.RoundToInt(datumRows[rowIndex + 1].Length * percInRow) % datumRows[rowIndex + 1].Length;
                var nextRowNextIndex = (nextRowIndex + 1) % datumRows[rowIndex + 1].Length;

                var datum = datumRows[rowIndex][colIndex];
                var datumUp = datumRows[nextRow][nextRowIndex];
                var datumUpRight = datumRows[nextRow][nextRowNextIndex];
                var datumRight = datumRows[rowIndex][(colIndex + 1) % datumRows[rowIndex].Length];

                surfaceTool.AddColor(ToColor(datum, maxDepth));
                surfaceTool.AddVertex(ToPos(datum));

                surfaceTool.AddColor(ToColor(datumUpRight, maxDepth));
                surfaceTool.AddVertex(ToPos(datumUpRight));

                surfaceTool.AddColor(ToColor(datumUp, maxDepth));
                surfaceTool.AddVertex(ToPos(datumUp));

                surfaceTool.AddColor(ToColor(datum, maxDepth));
                surfaceTool.AddVertex(ToPos(datum));

                surfaceTool.AddColor(ToColor(datumRight, maxDepth));
                surfaceTool.AddVertex(ToPos(datumRight));

                surfaceTool.AddColor(ToColor(datumUpRight, maxDepth));
                surfaceTool.AddVertex(ToPos(datumUpRight));
            }
        }

        surfaceTool.GenerateNormals();
        _globeMesh.Mesh = surfaceTool.Commit();
        _globeMesh.MaterialOverride = Material;
    }

    private Vector3 ToPos(WorldDatum datum)
    {
        return datum.Position.Normalized() * GLOBE_RADIUS;
    }

    private Color ToColor(WorldDatum datum, float maxDepth)
    {
        if (datum.IsExplored)
        {
            // r - seafloor depth
            // g - ice depth
            // b - light amount

            return new Color(
                datum.Depth / maxDepth,
                datum.IceDepth / maxDepth,
                datum.Light);
        }
        else
        {
            return new Color(
                1,
                0,
                0);
        }
    }

    public void SetTime(int factor)
    {
        if (_inCruiseMode)
        {
            ToggleCruiseMode();
        }

        Engine.TimeScale = factor;
    }

    public void ToggleCruiseMode()
    {
        SetCruiseMode(!_inCruiseMode);
    }

    private void SetCruiseMode(bool newCruiseMode)
    {
        _inCruiseMode = newCruiseMode;

        if(newCruiseMode)
        {
            Engine.TimeScale = Constants.CRUISE_MODE_TIME_SCALE;

            _playerData.PlayerController.AutopilotTarget = (ITargetableThing)_playerData.ManualWaypoint ?? _missionTracker.TrackedMission.MissionPath;
            _playerData.PlayerController.AutopilotType = AutopilotType.Goto;
        }
        else
        {
            _playerData.PlayerController.AutopilotType = AutopilotType.None;
            Engine.TimeScale = 1;
        }
    }

    public void SetCruiseStealth(bool stealthMode)
    {
        _playerData.CruiseStealthMode = stealthMode;
    }
}
