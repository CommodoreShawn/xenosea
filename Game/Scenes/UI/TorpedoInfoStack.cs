using Godot;
using System;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;

public class TorpedoInfoStack : VBoxContainer
{
    [Inject]
    private IPlayerData _playerData;

    private OptionButton _tubeOptionButton;
    private TextureProgress _torpedoLoadProgress;
    private Label _torpedoNameLabel;
    private Button _targetButton;
    private Button _wireCutButton;
    private Label _torpedoStatusLabel;
    private OptionButton _torpedoOptionButton;
    private Label _targetRangeLabel;
    private Label _relativeSpeedLabel;
    private Label _weaponSpeedLabel;
    private Label _fuelTimeLabel;
    private Label _travelTimeLabel;
    private Label _attackTimeLabel;

    private Submarine _currentSub;
    private TorpedoTube _currentTube;
    private GameplayUi _gameplayUi;
    private int _lastAmmoCount;


    public override void _Ready()
    {
        this.ResolveDependencies();

        _gameplayUi = this.FindParent<GameplayUi>();
        _tubeOptionButton = GetNode<OptionButton>("VBoxContainer/ButtonRow/TubeOptionButton");
        _torpedoLoadProgress = GetNode<TextureProgress>("VBoxContainer/VBoxContainer/HBoxContainer/TorpedoLoadProgress");
        _torpedoNameLabel = GetNode<Label>("VBoxContainer/VBoxContainer/HBoxContainer/TorpedoNameLabel");
        _targetButton = GetNode<Button>("VBoxContainer/VBoxContainer/HBoxContainer2/VBoxContainer/TargetButton");
        _wireCutButton = GetNode<Button>("VBoxContainer/VBoxContainer/HBoxContainer2/VBoxContainer/WireCutButton");
        _torpedoStatusLabel = GetNode<Label>("VBoxContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/TorpedoStatusLabel");
        _torpedoOptionButton = GetNode<OptionButton>("VBoxContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/TorpedoOptionButton");
        _targetRangeLabel = GetNode<Label>("VBoxContainer/VBoxContainer/TargetRangeLabel");
        _relativeSpeedLabel = GetNode<Label>("VBoxContainer/VBoxContainer/RelativeSpeedLabel");
        _weaponSpeedLabel = GetNode<Label>("VBoxContainer/VBoxContainer/WeaponSpeedLabel");
        _fuelTimeLabel = GetNode<Label>("VBoxContainer/VBoxContainer/FuelTimeLabel");
        _travelTimeLabel = GetNode<Label>("VBoxContainer/VBoxContainer/TravelTimeLabel");
        _attackTimeLabel = GetNode<Label>("VBoxContainer/VBoxContainer/AttackTimeLabel");

        _currentSub = null;
    }

    public void ToggleTargetMode()
    {
        if(_currentTube != null)
        {
            if (_currentTube.ActiveTorpedo == null)
            {
                _currentTube.ActiveTargeting = !_currentTube.ActiveTargeting;
            }
            else
            {
                if(_currentTube.ActiveTorpedo != _gameplayUi.CurrentTarget)
                {
                    _currentTube.ActiveTorpedo.Target = _gameplayUi.CurrentTarget;
                }
                else
                {
                    _currentTube.ActiveTorpedo.Target = null;
                }
            }
        }
    }

    public void CutTorpedoWire()
    {
        if (_currentTube?.ActiveTorpedo != null)
        {
            _currentTube.ActiveTorpedo.ActiveSeeking = true;
            _currentTube.ActiveTorpedo.OnWire = false;
            _currentTube.ActiveTorpedo = null;
        }
    }

    public void SetTorpedoOption(int index)
    {
        if (_currentTube != null && _currentTube.ActiveTorpedo == null)
        {
            _currentTube.ChangeAmmo(index);
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_playerData.PlayerSub != _currentSub)
        {
            Rebuild();
        }

        if (_currentTube != null)
        {
            _torpedoLoadProgress.TextureProgress_ = _currentTube.ActiveAmmo?.Icon;
            _torpedoLoadProgress.Value = (100 * _currentTube.LoadProgress / _currentTube.LoadTime);
            _torpedoNameLabel.Text = _currentTube.ActiveAmmo?.Name ?? string.Empty;
            _wireCutButton.Disabled = _currentTube.ActiveTorpedo == null;

            _torpedoOptionButton.Disabled = _currentTube.ActiveTorpedo != null;

            if (_currentTube.ActiveTorpedo == null || !IsInstanceValid(_currentTube.ActiveTorpedo))
            {
                UpdateUiLabelsForBeforeLaunch();

                _targetButton.Pressed = _currentTube.ActiveTargeting;

                if(_lastAmmoCount != _currentTube.TorpedoesAvailable.Count)
                {
                    RebuildAmmoOptions();
                }
            }
            else
            {
                UpdateUiLabelsForActiveTorpedo();

                _targetButton.Pressed = _currentTube.ActiveTorpedo.Target == _gameplayUi.CurrentTarget;
            }
        }
    }

    private void Rebuild()
    {
        _currentSub = _playerData.PlayerSub;

        _tubeOptionButton.Clear();

        if (_currentSub != null)
        {
            foreach (var tube in _currentSub.TorpedoTubes)
            {
                _tubeOptionButton.AddItem(tube.Name);
            }


            if (_currentSub.TorpedoTubes.Any())
            {
                SetCurrentTube(0);
            }
            else
            {
                SetCurrentTube(-1);
            }
        }
        else
        {
            SetCurrentTube(-1);
        }
    }

    private void SetCurrentTube(int index)
    {
        if (index >= 0)
        {
            _currentTube = _currentSub.TorpedoTubes[index];
            _targetButton.Disabled = false;
            RebuildAmmoOptions();
        }
        else
        {
            _currentTube = null;
            _torpedoLoadProgress.Value = 0;
            _torpedoNameLabel.Text = string.Empty;
            _targetButton.Disabled = true;
            _wireCutButton.Disabled = true;
            _torpedoStatusLabel.Text = string.Empty;
            _torpedoOptionButton.Clear();
            _targetRangeLabel.Text = string.Empty;
            _relativeSpeedLabel.Text = string.Empty;
            _weaponSpeedLabel.Text = string.Empty;
            _fuelTimeLabel.Text = string.Empty;
            _travelTimeLabel.Text = string.Empty;
            _attackTimeLabel.Text = string.Empty;
        }
    }

    private void RebuildAmmoOptions()
    {
        _torpedoOptionButton.Clear();
        foreach (var option in _currentTube.TorpedoesAvailable)
        {
            _torpedoOptionButton.AddIconItem(option.Icon, string.Empty);
        }
        _torpedoOptionButton.Selected = _currentTube.LoadedAmmoIndex;
        _lastAmmoCount = _currentTube.TorpedoesAvailable.Count;
    }

    private void UpdateUiLabelsForBeforeLaunch()
    {
        var targetDistance = 1f;
        var closingSpeed = 1f;

        if (_gameplayUi.CurrentTarget != null)
        {
            var toTarget = _gameplayUi.CurrentTarget.EstimatedPosition - _playerData.PlayerSub.RealPosition;
            targetDistance = toTarget.Length();

            var relativeVel = Vector3.Zero;
            if (_gameplayUi.CurrentTarget is SonarContactRecord scr)
            {
                relativeVel = scr.LastKnownVelocity;
            }

            closingSpeed = relativeVel.Dot(toTarget);

            _targetRangeLabel.Text = $"Target Range: {targetDistance.ToString("N0")} m";
        }
        else
        {
            _targetRangeLabel.Text = string.Empty;
            _relativeSpeedLabel.Text = string.Empty;
        }

        if (_currentTube.ActiveAmmo == null)
        {
            _torpedoStatusLabel.Text = string.Empty;
            _weaponSpeedLabel.Text = string.Empty;
            _fuelTimeLabel.Text = string.Empty;
            _travelTimeLabel.Text = string.Empty;
            _attackTimeLabel.Text = string.Empty;
            _torpedoOptionButton.Icon = null;
        }
        else
        {
            _torpedoOptionButton.Icon = _currentTube.ActiveAmmo.Icon;

            if (_currentTube.LoadProgress < _currentTube.LoadTime)
            {
                _torpedoStatusLabel.Text = $"Loading: {(_currentTube.LoadProgress / _currentTube.LoadTime * 100).ToString("N0")}% ";
                _torpedoStatusLabel.SelfModulate = Colors.Yellow;
            }
            else
            {
                _torpedoStatusLabel.Text = "Ready";
                _torpedoStatusLabel.SelfModulate = Colors.Green;
            }

            _weaponSpeedLabel.Text = $"Weapon Speed: {_currentTube.ActiveAmmo.Speed.ToString("N0")}";
            _fuelTimeLabel.Text = $"Fuel Time: {_currentTube.ActiveAmmo.FuelTime.ToString("N0")}s";

            if (_currentTube.Target != null)
            {
                var travelTime = targetDistance / (_currentTube.ActiveAmmo.Speed);

                _travelTimeLabel.Text = $"Travel Time: {travelTime.ToString("N0")}s";
                _attackTimeLabel.Text = $"Attack Time: {(_currentTube.ActiveAmmo.FuelTime - travelTime).ToString("N0")}s";

                if (travelTime >= _currentTube.ActiveAmmo.FuelTime)
                {
                    _travelTimeLabel.SelfModulate = Colors.Red;
                    _attackTimeLabel.SelfModulate = Colors.Red;
                }
                else if (travelTime >= 0.8f * _currentTube.ActiveAmmo.FuelTime)
                {
                    _travelTimeLabel.SelfModulate = Colors.Yellow;
                    _attackTimeLabel.SelfModulate = Colors.Yellow;
                }
                else
                {
                    _travelTimeLabel.SelfModulate = Colors.White;
                    _attackTimeLabel.SelfModulate = Colors.White;
                }
            }
            else
            {
                _travelTimeLabel.Text = string.Empty;
                _attackTimeLabel.Text = string.Empty;
            }
        }
    }

    private void UpdateUiLabelsForActiveTorpedo()
    {
        var targetDistance = 1f;
        var closingSpeed = 1f;

        if (_currentTube.ActiveTorpedo.Target != null)
        {
            var toTarget = _currentTube.ActiveTorpedo.Target.EstimatedPosition - _currentTube.ActiveTorpedo.RealPosition;
            targetDistance = toTarget.Length();

            var relativeVel = Vector3.Zero;
            if (_currentTube.ActiveTorpedo.Target is SonarContactRecord scr)
            {
                relativeVel = scr.LastKnownVelocity;
            }

            closingSpeed = relativeVel.Dot(toTarget);

            _targetRangeLabel.Text = $"Target Range: {targetDistance.ToString("N0")} m";
        }
        else
        {
            _targetRangeLabel.Text = string.Empty;
            _relativeSpeedLabel.Text = string.Empty;
        }

        _torpedoStatusLabel.Text = $"Running";
        _torpedoStatusLabel.SelfModulate = Colors.Green;
        _weaponSpeedLabel.Text = $"Weapon Speed: {_currentTube.ActiveTorpedo.Velocity.Length().ToString("N0")}";
        _fuelTimeLabel.Text = $"Fuel Time: {_currentTube.ActiveTorpedo.BurnTime.ToString("N0")}s";

        if(_currentTube.ActiveTorpedo.BurnTime < 10)
        {
            _fuelTimeLabel.SelfModulate = Colors.Red;
        }
        else if (_currentTube.ActiveTorpedo.BurnTime < 30)
        {
            _fuelTimeLabel.SelfModulate = Colors.Yellow;
        }
        else
        {
            _fuelTimeLabel.SelfModulate = Colors.White;
        }

        if (_currentTube.ActiveTorpedo.Target != null)
        {
            var travelTime = targetDistance / (Mathf.Max(1, _currentTube.ActiveTorpedo.Velocity.Length()));

            _travelTimeLabel.Text = $"Travel Time: {travelTime.ToString("N0")}s";
            _attackTimeLabel.Text = $"Attack Time: {(_currentTube.ActiveTorpedo.BurnTime - travelTime).ToString("N0")}s";

            if (travelTime >= _currentTube.ActiveTorpedo.BurnTime)
            {
                _travelTimeLabel.SelfModulate = Colors.Red;
                _attackTimeLabel.SelfModulate = Colors.Red;
            }
            else if (travelTime >= 0.8f * _currentTube.ActiveTorpedo.BurnTime)
            {
                _travelTimeLabel.SelfModulate = Colors.Yellow;
                _attackTimeLabel.SelfModulate = Colors.Yellow;
            }
            else
            {
                _travelTimeLabel.SelfModulate = Colors.White;
                _attackTimeLabel.SelfModulate = Colors.White;
            }
        }
        else
        {
            _travelTimeLabel.Text = string.Empty;
            _attackTimeLabel.Text = string.Empty;
        }
    }
}
