using Godot;
using System;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class HintPanel : PanelContainer
{
    [Inject]
    private IPlayerData _playerData;

    private Label _shootHint;
    private Label _aimHint;
    private Label _aimLockHint;
    private Label _periscopeHint;
    private Label _targetHintNormal;
    private Label _targetHintPeriscope;
    
    private SubCamera _camera;
    public override void _Ready()
    {
        this.ResolveDependencies();

        _shootHint = GetNode<Label>("VBoxContainer/ShootHint");
        _aimHint = GetNode<Label>("VBoxContainer/AimHint");
        _aimLockHint = GetNode<Label>("VBoxContainer/AimLockHint");
        _periscopeHint = GetNode<Label>("VBoxContainer/PeriscopeHint");
        _targetHintNormal = GetNode<Label>("VBoxContainer/TargetHintNormal");
        _targetHintPeriscope = GetNode<Label>("VBoxContainer/TargetHintPeriscope");
        _camera = this.FindParent<GameplayUi>().GetParent<Spatial>().GetNode<SubCamera>("Camera");
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        Visible = _playerData.PlayerSub?.IsDockedTo == null && _playerData.InTutorial;
        _shootHint.Visible = _camera.IsAiming;
        _aimHint.Visible = !_camera.IsAiming;
        _aimLockHint.Visible = !_camera.IsAiming;
        _periscopeHint.Visible = !_camera.InPeriscope;
        _targetHintNormal.Visible = !_camera.InPeriscope;
        _targetHintPeriscope.Visible = _camera.InPeriscope;
    }
}
