using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Scenes.Ships;

public class SubmarineVendor : VBoxContainer
{
    [Export]
    public PackedScene ModuleVendorScenePrefab;

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IResourceLocator _resourceLocator;

    private VBoxContainer _currentLoadoutContainer;
    private VBoxContainer _vendorOptionsContainer;
    private VBoxContainer _hullOptionsContainer;

    private ArmsDealerStationRoom _room;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _currentLoadoutContainer = GetNode<VBoxContainer>("HBoxContainer/CurrentLoadoutList/ScrollContainer/CurrentLoadoutContainer");
        _vendorOptionsContainer = GetNode<VBoxContainer>("HBoxContainer/ModuleOptionList/ScrollContainer/ModuleOptionContainer");
        _hullOptionsContainer = GetNode<VBoxContainer>("HBoxContainer/VendorVBoxContainer/ScrollContainer/HullOptionsContainer");
    }

    public void InitializeFor(StationRoom stationRoom)
    {
        _currentLoadoutContainer.ClearChildren();
        _vendorOptionsContainer.ClearChildren();
        _hullOptionsContainer.ClearChildren();

        if (stationRoom is ArmsDealerStationRoom dealer)
        {
            _room = dealer;

            var playerLoadout = _playerData.PlayerSub.GetLoadout();

            AddSection(_currentLoadoutContainer, "Hull Type", playerLoadout.HullType.Yield(), x => x.Name);
            AddSection(_currentLoadoutContainer, "Armor Type", playerLoadout.ArmorType.Yield(), x => x.Name);
            AddSection(_currentLoadoutContainer, "Engine", playerLoadout.EngineModule.Yield(), x => x.Name);
            AddSection(_currentLoadoutContainer, "Sonar", playerLoadout.SonarModule.Yield(), x => x.Name);
            AddSection(_currentLoadoutContainer, "Towed Array", new[] { playerLoadout.TowedArrayModule }, x => x?.Name ?? "none");
            AddSection(_currentLoadoutContainer, "Turrets", playerLoadout.TurretModules, x => x?.Name ?? "none");
            AddSection(_currentLoadoutContainer, "Launchers", playerLoadout.LauncherModules, x => x?.Name ?? "none");
            AddSection(_currentLoadoutContainer, "Hangars", playerLoadout.HangarModules, x => x?.Name ?? "none");
            AddSection(_currentLoadoutContainer, "Other Modules", playerLoadout.OtherModules, x => x?.Name ?? "none");

            var station = (_playerData.PlayerSub.IsDockedTo as WorldStation).BackingStation;
            var industrialSupport = station.EconomicProcesses.Sum(x => x.IndustrialSupport);
            var militarySupport = station.EconomicProcesses.Sum(x => x.MilitarySupport);

            var armorTypes = _resourceLocator.AllArmorTypes
                .Where(x => industrialSupport >= x.IndustrialNeed)
                .Where(x => militarySupport >= x.MilitaryNeed)
                .Where(x => x.Operators.Contains(station.Faction))
                .ToArray();

            var moduleTypes = _resourceLocator.AllModuleTypes
                .Where(x => industrialSupport >= x.IndustrialNeed)
                .Where(x => militarySupport >= x.MilitaryNeed)
                .Where(x => x.Operators.Contains(station.Faction))
                .ToArray();

            var hullTypes = _resourceLocator.AllSubHullTypes
                .Where(x => industrialSupport >= x.IndustrialNeed)
                .Where(x => militarySupport >= x.MilitaryNeed)
                .Where(x => x.Operators.Contains(station.Faction))
                .ToArray();

            AddSection(_vendorOptionsContainer, "Armor Types", armorTypes, x => x.Name);
            AddSection(_vendorOptionsContainer, "Engines", moduleTypes.OfType<EngineModuleType>(), x => x.Name);
            AddSection(_vendorOptionsContainer, "Sonars", moduleTypes.OfType<SonarModuleType>(), x => x.Name);
            AddSection(_vendorOptionsContainer, "Towed Arrays", moduleTypes.OfType<TowedArrayModuleType>(), x => x.Name);
            AddSection(_vendorOptionsContainer, "Turrets", moduleTypes.OfType<TurretModuleType>(), x => x.Name);
            AddSection(_vendorOptionsContainer, "Launchers", moduleTypes.OfType<TorpedoLauncherModuleType>(), x => x.Name);
            AddSection(_vendorOptionsContainer, "Hangars", moduleTypes.OfType<MinisubHangarModuleType>(), x => x.Name);
            AddSection(_vendorOptionsContainer, "Other Modules",
                moduleTypes
                    .Where(x=> !(
                        x is EngineModuleType
                        || x is SonarModuleType
                        || x is TowedArrayModuleType
                        || x is TurretModuleType
                        || x is TorpedoLauncherModuleType
                        || x is MinisubHangarModuleType)),
                x => x.Name);

            foreach (var hullType in hullTypes)
            {
                _hullOptionsContainer.AddChild(new Label
                {
                    Align = Label.AlignEnum.Left,
                    Text = hullType.Name
                });
            }
        }
    }

    private void AddSection<T>(
        VBoxContainer container, 
        string title, 
        IEnumerable<T> contents, 
        Func<T,string> itemToName)
    {
        if (contents.Any())
        {
            container.AddChild(new Label
            {
                Align = Label.AlignEnum.Center,
                Text = title
            });
            foreach (var item in contents)
            {
                container.AddChild(new Label
                {
                    Align = Label.AlignEnum.Left,
                    Text = itemToName(item)
                });
            }
        }
    }

    public void BuyNewSub()
    {
        var newScreen = ModuleVendorScenePrefab.Instance() as ModuleVendorScreen;
        GetTree().Root.AddChild(newScreen);
        newScreen.SetupForPurchase(_room, _playerData.PlayerSub);
        this.FindParent<StationRoomPopup>().ClosePopup();
    }

    public void RefitSub()
    {
        var newScreen = ModuleVendorScenePrefab.Instance() as ModuleVendorScreen;
        GetTree().Root.AddChild(newScreen);
        newScreen.SetupForRefit(_room, _playerData.PlayerSub);
        this.FindParent<StationRoomPopup>().ClosePopup();
    }
}
