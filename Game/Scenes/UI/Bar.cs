using Godot;

public class Bar : TabContainer, IStationRoomWidgetContents
{
    public void InitializeFor(StationRoom stationRoom)
    {
        GetNode<DiverVendor>("Divers").InitializeFor(stationRoom);
        GetNode<NewsAndRumors>("Rumor and News").InitializeFor(stationRoom);
        GetNode<BarMissionsTab>("Missions").InitializeFor(stationRoom);
    }
}
