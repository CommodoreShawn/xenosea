using Godot;
using System;
using Xenosea.Logic;

public class StationRoomButton : Button
{
    private StationRoom _stationRoom;
    private Submarine _playerSub;

    private Label _nameLabel;
    private Label _descriptionLabel;
    private VBoxContainer _contentBox;
    private GameplayUi _gameplayUi;
    private Line2D _pointerLine;
    private int _sizeSet;
    private Camera _camera;

    public override void _Ready()
    {
        _nameLabel = GetNode<Label>("VBoxContainer/NameLabel");
        _descriptionLabel = GetNode<Label>("VBoxContainer/DescriptionLabel");
        _contentBox = GetNode<VBoxContainer>("VBoxContainer");
        _gameplayUi = this.FindParent<GameplayUi>();
        _pointerLine = GetNode<Line2D>("PointerLine");
        _camera = _gameplayUi.GetParent<Spatial>().GetNode<SubCamera>("Camera");
    }

    public void InitializeFor(StationRoom stationRoom)
    {
        _stationRoom = stationRoom;
        _nameLabel.Text = stationRoom.FriendlyName;
        _descriptionLabel.Text = stationRoom.Description;
        _sizeSet = 0;
    }

    public void InitializeFor(Submarine playerSub)
    {
        _playerSub = playerSub;
        _nameLabel.Text = playerSub.FriendlyName;
        _descriptionLabel.Text = "";
        _sizeSet = 0;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if((_stationRoom == null || !IsInstanceValid(_stationRoom)) && _playerSub == null)
        {
            return;
        }

        if(_playerSub != null)
        {
            _nameLabel.Text = _playerSub.FriendlyName;

            if (_playerSub.Health < _playerSub.MaximumHealth)
            {
                _descriptionLabel.Text = $"Under repairs: {_playerSub.HealthPercent.ToString("P0")}";
                _descriptionLabel.Modulate = Colors.Yellow;
            }
            else
            {
                _descriptionLabel.Text = "Ready to Depart";
                _descriptionLabel.Modulate = Colors.Green;
            }

            _pointerLine.SetPointPosition(1,
                _camera.UnprojectPosition(_playerSub.GlobalTransform.origin)
                - RectGlobalPosition);
        }
        else
        {
            _pointerLine.SetPointPosition(1,
                _camera.UnprojectPosition(_stationRoom.GlobalTransform.origin)
                - RectGlobalPosition);
        }

        if(_sizeSet == 1)
        {
            RectMinSize = _contentBox.RectSize + new Vector2(10, 10);
        }

        if (_sizeSet < 2)
        {
            _sizeSet += 1;
        }

        _pointerLine.SetPointPosition(0, new Vector2(
            0,
            RectSize.y / 2));

        

        _pointerLine.Visible = IsHovered();
    }

    public void ButtonPressed()
    {
        if (_stationRoom != null)
        {
            _gameplayUi.ShowStationRoom(_stationRoom);
        }
        else
        {
            _gameplayUi.ShowLoadout();
        }
    }
}
