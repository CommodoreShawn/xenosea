using Godot;
using System.Linq;
using Xenosea.Logic;

public class TurretControlRow : HBoxContainer
{
    public int Index { get; set; }
    public TurretSystem TurretSystem { get; set; }
    
    private GameplayUi _gameplayUi;
    private string _action;
    private Button _setWeaponButton;
    private Button[] _buttons;
    private Label _reloadProgress;

    private TurretModuleType _oldTurretType;

    public override void _Ready()
    {
        _setWeaponButton = GetNode<Button>("SelectButton");
        _setWeaponButton.Text = Index.ToString();
        _reloadProgress = GetNode<Label>("ReloadProgress");
        _buttons = new[]
        {
            GetNode<Button>("Ammo0"),
            GetNode<Button>("Ammo1"),
            GetNode<Button>("Ammo2")
        };

        _gameplayUi = this.FindParent<GameplayUi>();
        

        _oldTurretType = null;

        _action = "switch_ammo_" + Index;

        SetAmmo(0);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_oldTurretType != TurretSystem.TurretModuleType)
        {
            _oldTurretType = TurretSystem.TurretModuleType;

            for (var i = 0; i < _buttons.Length; i++)
            {
                if (i < TurretSystem.TurretModuleType.AmmunitionOptions.Length)
                {
                    var ammoType = TurretSystem.TurretModuleType.AmmunitionOptions[i];

                    _buttons[i].Visible = true;
                    _buttons[i].Pressed = TurretSystem.ActiveAmmo == ammoType;
                    _buttons[i].Icon = ammoType.SmallIcon;
                    _buttons[i].HintTooltip = ammoType.Name +
                        $"\nDamage: {ammoType.Damage}" +
                        $"\nPenetration: {ammoType.ArmorPenetration}" +
                        $"\nRange: {ammoType.MaximumRange.ToString("N0")} m";
                }
                else
                {
                    _buttons[i].Visible = false;
                }
            }
        }

        if (Input.IsActionJustPressed(_action))
        {
            if (_gameplayUi.ActiveWeaponSystem == TurretSystem)
            {
                int currentButton = 0;
                for (var i = 0; i < _buttons.Length; i++)
                {
                    if (_buttons[i].Pressed)
                    {
                        currentButton = i;
                    }
                }

                SetAmmo((currentButton + 1) % TurretSystem.TurretModuleType.AmmunitionOptions.Length);
            }
            else
            {
                _gameplayUi.ActiveWeaponSystem = TurretSystem;
            }
        }

        _setWeaponButton.Pressed = _gameplayUi.ActiveWeaponSystem == TurretSystem;

        var mounts = TurretSystem.Mounts
            .Select(x => x.Turret)
            .Where(x => x!= null)
            .ToArray();

        if(mounts.Any())
        {
            var loadProgress = mounts.Min(x => x.ReloadCounter / x.ReloadTime);

            _reloadProgress.Text = $"{(100 * loadProgress).ToString("N0")}%";
        }
        else
        {
            _reloadProgress.Text = string.Empty;
        }
    }

    public void SetAmmo(int index)
    {
        if (TurretSystem.ActiveAmmo != TurretSystem.TurretModuleType.AmmunitionOptions[index])
        {
            for (var i = 0; i < _buttons.Length; i++)
            {
                _buttons[i].Pressed = i == index;
            }

            TurretSystem.ChangeAmmo(TurretSystem.TurretModuleType.AmmunitionOptions[index]);
        }
    }

    public void SetWeapon()
    {
        _gameplayUi.ActiveWeaponSystem = TurretSystem;
    }
}
