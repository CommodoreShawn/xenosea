using Godot;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class CommodityTraderRow : HBoxContainer
{
    public CommodityType Commodity { get; internal set; }
    public Submarine PlayerSub { get; internal set; }
    public WorldStation Station { get; internal set; }
    
    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private ISimulationTradeMonitor _tradeMonitor;

    private TextureProgress _stockpileProgress;
    private TextureProgress _cargoProgress;
    private Label _priceLabel;
    private HSlider _buySlider;
    private Label _buyQtyLabel;
    private Button _buyButton;
    private float _playerSubCargo;

    public override void _Ready()
    {
        this.ResolveDependencies();
        GetNode<Label>("NameLabel").Text = Commodity.Name;
        _stockpileProgress = GetNode<TextureProgress>("StockpileCenterer/StockpileProgress");
        _cargoProgress = GetNode<TextureProgress>("CargoCenterer/CargoProgress");
        _priceLabel = GetNode<Label>("PriceLabel");
        GetNode<Label>("ExportsLabel").Text = string.Join(", ", _tradeMonitor.GetExports(Station.BackingStation, Commodity).Select(x => x.Name));
        GetNode<Label>("ExportsLabel").HintTooltip = string.Join(", ", _tradeMonitor.GetExports(Station.BackingStation, Commodity).Select(x => x.Name));
        GetNode<Label>("ImportsLabel").Text = string.Join(", ", _tradeMonitor.GetImports(Station.BackingStation, Commodity).Select(x => x.Name));
        GetNode<Label>("ImportsLabel").HintTooltip = string.Join(", ", _tradeMonitor.GetImports(Station.BackingStation, Commodity).Select(x => x.Name));
        _buySlider = GetNode<HSlider>("VBoxContainer/HBoxContainer/HSlider");
        _buyQtyLabel = GetNode<Label>("VBoxContainer/HBoxContainer/QtyLabel");
        _buyButton = GetNode<Button>("VBoxContainer/BuySellButton");
        _playerSubCargo = -1;

        GetNode<TextureRect>("IconCenterer/ContrabandWarningTextureRect").Visible = Station.Faction.Contraband.Contains(Commodity);
    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        _stockpileProgress.Value = 100 * (Station.CommodityStorage.GetQty(Commodity) / Station.CommodityCapacity);
        _stockpileProgress.HintTooltip = $"{Station.CommodityStorage.GetQty(Commodity).ToString("N0")} / {Station.CommodityCapacity.ToString("N0")}";
        _cargoProgress.Value = 100 * (PlayerSub.CommodityStorage.TotalVolume / PlayerSub.CargoVolume);
        _cargoProgress.HintTooltip = $"{PlayerSub.CommodityStorage.TotalVolume.ToString("N0")} / {PlayerSub.CargoVolume.ToString("N0")}";
        _priceLabel.Text = Station.PriceOf(Commodity).ToString("C0");

        var currentQty = PlayerSub.CommodityStorage.GetQty(Commodity);

        if (PlayerSub.CommodityStorage.TotalVolume != _playerSubCargo)
        {
            _playerSubCargo = PlayerSub.CommodityStorage.TotalVolume;
    
            _buySlider.MinValue = 0;
            _buySlider.MaxValue = Mathf.Min(
                PlayerSub.CargoVolume - PlayerSub.CommodityStorage.TotalVolume + currentQty,
                Station.CommodityStorage.GetQty(Commodity) + currentQty);
            _buySlider.Value = currentQty;
        }

        _buyQtyLabel.Text = _buySlider.Value.ToString("N0");

        if (_buySlider.Value == currentQty)
        {
            _buyButton.Disabled = true;
            _buyButton.Text = "Buy / Sell";
        }
        else if(_buySlider.Value < currentQty)
        {
            _buyButton.Disabled = false;
            _buyButton.Text = $"Sell for {((currentQty - _buySlider.Value) * Station.PriceOf(Commodity)).ToString("C0")}";
        }
        else if (_buySlider.Value > currentQty)
        {
            _buyButton.Disabled = _playerData.Money < (_buySlider.Value - currentQty) * Station.PriceOf(Commodity);
            _buyButton.Text = $"Buy for {((_buySlider.Value - currentQty) * Station.PriceOf(Commodity)).ToString("C0")}";
        }
    }

    public void TradeCommodity()
    {
        var currentQty = PlayerSub.CommodityStorage.GetQty(Commodity);

        if (_buySlider.Value < currentQty)
        {
            var moneyToGive = (currentQty - _buySlider.Value) * Station.PriceOf(Commodity);
            var qtyToTake = currentQty - (float)_buySlider.Value;
            PlayerSub.CommodityStorage.Remove(Commodity, qtyToTake);
            Station.CommodityStorage.Add(Commodity, qtyToTake);
            _playerData.Money += moneyToGive;
        }
        else if (_buySlider.Value > currentQty)
        {
            _buyButton.Disabled = false;
            _buyButton.Text = $"Buy for {((_buySlider.Value - currentQty) * Station.PriceOf(Commodity)).ToString("C0")}";

            var moneyToTake = (_buySlider.Value - currentQty) * Station.PriceOf(Commodity);
            var qtyToGive = (float)_buySlider.Value - currentQty;
            Station.CommodityStorage.Remove(Commodity, qtyToGive);
            PlayerSub.CommodityStorage.Add(Commodity, qtyToGive);
            _playerData.Money -= moneyToTake;
        }
    }
}
