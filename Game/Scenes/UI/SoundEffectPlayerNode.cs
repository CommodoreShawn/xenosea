using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class SoundEffectPlayerNode : AudioStreamPlayer
{
    [Export]
    public AudioStream UiClickSound;

    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    public override void _Ready()
    {
        base._Ready();

        this.ResolveDependencies();

        _soundEffectPlayer.PlayerNode = this;
    }

    public void PlayUiClick()
    {
        if(!Playing)
        {
            Stream = UiClickSound;
            Play();
        }
    }
}
