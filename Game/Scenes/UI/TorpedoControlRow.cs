using Godot;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class TorpedoControlRow : HBoxContainer
{
    [Inject]
    private IPlayerData _playerData;

    public int Index { get; set; }
    public TorpedoTube TorpedoTube { get; set; }

    private GameplayUi _gameplayUi;

    private Button _selectButton;
    private TextureRect _ammoIcon;
    private Button _actionButton;

    private string _action;

    private TorpedoType _lastLoadedTorpedoType;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _selectButton = GetNode<Button>("SelectButton");
        _selectButton.Text = Index.ToString();
        _ammoIcon = GetNode<TextureRect>("AmmoIcon");
        _actionButton = GetNode<Button>("ActionButton");

        _gameplayUi = this.FindParent<GameplayUi>();
        _action = "switch_ammo_" + Index;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (Input.IsActionJustPressed(_action))
        {
            if (_gameplayUi.ActiveWeaponSystem == TorpedoTube)
            {
                var nextAmmo = TorpedoTube.NextAmmoIndex + 1;
                if (nextAmmo >= TorpedoTube.TorpedoesAvailable.Count)
                {
                    nextAmmo = 0;
                }
                TorpedoTube.ChangeAmmo(nextAmmo);
            }
            else
            {
                _gameplayUi.ActiveWeaponSystem = TorpedoTube;
            }
        }

        if (TorpedoTube.ActiveTorpedo != null)
        {
            _actionButton.Text = "Cut";
        }
        if (TorpedoTube.LoadProgress < TorpedoTube.LoadTime)
        {
            _actionButton.Text = $"{(TorpedoTube.LoadProgress / TorpedoTube.LoadTime * 100).ToString("N0")}%";
        }
        else if(TorpedoTube.ActiveAmmo != null)
        {
            _actionButton.Text = "Launch";
        }

        _actionButton.Disabled = TorpedoTube.ActiveTorpedo == null
            && (TorpedoTube.ActiveAmmo == null 
                || TorpedoTube.LoadProgress < TorpedoTube.LoadTime
                || _playerData.WeaponsSafe);

        _selectButton.Pressed = _gameplayUi.ActiveWeaponSystem == TorpedoTube;

        if(TorpedoTube.ActiveAmmo != _lastLoadedTorpedoType)
        {
            _lastLoadedTorpedoType = TorpedoTube.ActiveAmmo;

            if(TorpedoTube.ActiveAmmo != null)
            {
                _ammoIcon.Texture = TorpedoTube.ActiveAmmo.Icon;

                var range = TorpedoTube.ActiveAmmo.FuelTime * TorpedoTube.ActiveAmmo.Speed;

                _ammoIcon.HintTooltip = TorpedoTube.ActiveAmmo.Name
                    + $"\nRange: {range.ToString("N0") } m";

            }
            else
            {
                _ammoIcon.Texture = null;
                _ammoIcon.HintTooltip = string.Empty;
            }
        }
    }

    public void SetWeapon()
    {
        _gameplayUi.ActiveWeaponSystem = TorpedoTube;
    }

    public void LaunchOrCut()
    {
        if(TorpedoTube.ActiveTorpedo == null)
        {
            TorpedoTube.Shoot(TorpedoTube.Target);
        }
        else if (TorpedoTube.ActiveTorpedo != null)
        {
            TorpedoTube.ActiveTorpedo.ActiveSeeking = true;
            TorpedoTube.ActiveTorpedo.OnWire = false;
            TorpedoTube.ActiveTorpedo = null;
        }
    }
}
