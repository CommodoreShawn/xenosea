using Godot;
using System;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class EscapeMenuPopup : PopupPanel
{
    private float _oldTimeScale;
    private ProgressPopup _progressPopup;
    private SaveLoadPopup _saveLoadPopup;
    private ConfirmationDialog _confirmationDialog;
    private SettingsPopup _settingsPopup;
    private System.Action _closeAction;

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IGameSerializer _gameSerializer;
    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _progressPopup = this.FindParent<GameplayUi>().GetNode<ProgressPopup>("ProgressPopup");
        _saveLoadPopup = this.FindParent<GameplayUi>().GetNode<SaveLoadPopup>("SaveLoadPopup");
        _confirmationDialog = this.FindParent<GameplayUi>().GetNode<ConfirmationDialog>("ConfirmationDialog");
        _settingsPopup = this.FindParent<GameplayUi>().GetNode<SettingsPopup>("SettingsPopup");
    }

    public void Open()
    {
        _oldTimeScale = Engine.TimeScale;
        Engine.TimeScale = 0;

        GetNode<Button>("VBoxContainer/SaveButton").Disabled = _playerData?.PlayerSub?.IsDockedTo == null;

        PopupCentered();
    }

    public void Close()
    {
        _soundEffectPlayer.PlayUiClick();

        if(_oldTimeScale != 0)
        {
            Engine.TimeScale = _oldTimeScale;
        }
        else
        {
            Engine.TimeScale = 1;
        }

        Hide();
    }

    public void QuitToMenu()
    {
        _soundEffectPlayer.PlayUiClick();
        _closeAction = () => GetTree().ChangeScene("res://Scenes/MainMenu.tscn");
        
        var timeSinceSave = DateTime.Now - _playerData.LastSaveTime;
        if (timeSinceSave > TimeSpan.FromMinutes(1))
        {
            _confirmationDialog.WindowTitle = "Quit without Saving?";
            _confirmationDialog.DialogText = $"You haven't saved for {(int)timeSinceSave.TotalMinutes} minutes.";
            _confirmationDialog.ShowModal(true);
        }
        else    
        {
            _closeAction();
        }
        
    }

    public void QuitToDesktop()
    {
        _soundEffectPlayer.PlayUiClick();
        _closeAction = () => GetTree().Quit();

        var timeSinceSave = DateTime.Now - _playerData.LastSaveTime;
        if (timeSinceSave > TimeSpan.FromMinutes(1))
        {
            _confirmationDialog.WindowTitle = "Quit without Saving?";
            _confirmationDialog.DialogText = $"You haven't saved for {(int)timeSinceSave.TotalMinutes} minutes.";
            _confirmationDialog.ShowModal(true);
        }
        else
        {
            _closeAction();
        }
    }

    public void SaveGame()
    {
        _soundEffectPlayer.PlayUiClick();
        _saveLoadPopup.ShowForSave(
            fileName =>
            {
                _progressPopup.ShowFor(
                    "Saving",
                    reportProgress =>
                    {
                        _gameSerializer.SaveGameState(fileName, reportProgress);
                    },
                    () =>
                    {
                    });
            });
    }

    public void CloseConfirmed()
    {
        _soundEffectPlayer.PlayUiClick();
        _closeAction();
    }

    public void ShowSettings()
    {
        _soundEffectPlayer.PlayUiClick();
        _settingsPopup.ShowWithCallback(() => { });
    }
}
