using Godot;

public class TurretVendorAmmoTypeIcon : TextureRect
{
    public AmmunitionType AmmunitionType { get; set; }

    public override void _Ready()
    {
        if(AmmunitionType != null)
        {
            Texture = AmmunitionType.Icon;
            HintTooltip = $"{AmmunitionType.Name}\n" +
                $"Damage: {AmmunitionType.Damage.ToString("N0")}\n" +
                $"Armor Penetration: {AmmunitionType.ArmorPenetration.ToString("N0")}\n" +
                $"Range: {AmmunitionType.MaximumRange.ToString("N0")}";
        }
    }
}
