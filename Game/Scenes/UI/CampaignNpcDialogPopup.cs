using Godot;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Dialog;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

public class CampaignNpcDialogPopup : Popup
{
    private TextureRect _faceTextureRect;
    private Label _nameLabel;
    private Label _titleLabel;
    private VBoxContainer _textLogBox;
    private Control _speakOptionBox;
    private ScrollContainer _textLogScroll;
    private VBoxContainer _topicListBox;
    private Button _closeButton;
    private Npc _npc;

    private NpcConversation _conversation;

    private bool _doAutoscroll;

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IQuestTracker _questTracker;
    [Inject]
    private ISimulationEventBus _simulationEventBus;
    [Inject]
    private IConversationManager _conversationManager;
    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _faceTextureRect = GetNode<TextureRect>("PanelContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/FaceTextureRect");
        _nameLabel = GetNode<Label>("PanelContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/NameLabel");
        _titleLabel = GetNode<Label>("PanelContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/TitleLabel");
        _textLogBox = GetNode<VBoxContainer>("PanelContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer2/ScrollContainer/TextLogBox");
        _speakOptionBox = GetNode<Control>("PanelContainer/HBoxContainer/VBoxContainer/SpeakOptionBox");
        _textLogScroll = GetNode<ScrollContainer>("PanelContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer2/ScrollContainer");
        _topicListBox = GetNode<VBoxContainer>("PanelContainer/HBoxContainer/VBoxContainer2/ScrollContainer/VBoxContainer");
        _closeButton = GetNode<Button>("PanelContainer/HBoxContainer/VBoxContainer2/CloseButton");
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_doAutoscroll)
        {
            _doAutoscroll = false;
            _textLogScroll.ScrollVertical = (int)_textLogScroll.GetVScrollbar().MaxValue;
        }
    }

    public void ShowFor(Npc npc)
    {
        Input.MouseMode = Input.MouseModeEnum.Visible;
        this.FindParent<Gameplay>().GetNode<SubCamera>("Camera").AimLock = false;

        _npc = npc;
        _conversation = _conversationManager.StartConversationWith(npc);
        _textLogBox.ClearChildren();
        _speakOptionBox.ClearChildren();

        PopupCentered();
        _faceTextureRect.Texture = npc.ImageLarge;
        _nameLabel.Text = npc.Name;
        _titleLabel.Text = npc.Title;

        AddResponse(_conversation.Start());
    }

    private void AddResponse(ConversationResponse conversationResponse)
    {
        foreach(var line in conversationResponse.Lines)
        {
            var label = new Label()
            {
                Text = line.Text.Trim(),
                Autowrap = true
            };

            if(line.Speaker == "Player")
            {
                label.Modulate = Colors.Yellow;
                label.Align = Label.AlignEnum.Left;
            }
            else
            {
                label.Modulate = Colors.White;
                label.Align = Label.AlignEnum.Left;
            }

            _textLogBox.AddChild(label);
        }

        _doAutoscroll = true;

        foreach (var action in conversationResponse.Actions)
        {
            var value = action.Invoke();

            if(!string.IsNullOrWhiteSpace(value))
            {
                if(value.StartsWith("~"))
                {
                    _textLogBox.AddChild(new Label()
                    {
                        Text = value.Substring(1),
                        Autowrap = true,
                        Modulate = Colors.Red,
                        Align = Label.AlignEnum.Left
                    });
                }
                else
                {
                    _textLogBox.AddChild(new Label()
                    {
                        Text = value,
                        Autowrap = true,
                        Modulate = Colors.Green,
                        Align = Label.AlignEnum.Left
                    });
                }
            }
        }

        _topicListBox.ClearChildren();
        foreach(var topic in _conversation.GetAvailableTopics())
        {
            var button = new Button()
            {
                Text = topic,
                SizeFlagsHorizontal = (int)SizeFlags.Fill | (int)SizeFlags.Expand,
                Disabled = conversationResponse.Options.Any()
            };
            button.Connect("pressed", this, "TopicPicked", new Godot.Collections.Array { topic });

            _topicListBox.AddChild(button);
        }
        _closeButton.Disabled = conversationResponse.Options.Any();

        _speakOptionBox.ClearChildren();
        foreach(var option in conversationResponse.Options)
        { 
            var button = new Button()
            {
                Text = option,
                SizeFlagsHorizontal = (int)SizeFlags.Fill | (int)SizeFlags.Expand
            };
            button.Connect("pressed", this, "OptionPicked", new Godot.Collections.Array { option });

            _speakOptionBox.AddChild(button);
        }
    }

    public void OptionPicked(string option)
    {
        _soundEffectPlayer.PlayUiClick();
        AddResponse(_conversation.ChooseOption(option));
    }

    public void TopicPicked(string topic)
    {
        _soundEffectPlayer.PlayUiClick();
        AddResponse(_conversation.ChooseTopic(topic));
    }

    public void EndDialog()
    {
        _soundEffectPlayer.PlayUiClick();
        Hide();
    }
}
