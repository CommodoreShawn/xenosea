using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Resources;

public class MapObjectButton : Button
{
    public IMapObject MapObject { get; set; }

    [Export]
    public Texture StationIconTexture;
    [Export]
    public Texture SubmarineIconTexture;
    [Export]
    public Texture WreckIconTexture;
    [Export]
    public Texture WaypointIconTexture;
    [Export]
    public Texture UnknownIconTexture;
    [Export]
    public Texture LandmarkIconTexture;

    public override void _Ready()
    {
        RefreshContent();
    }

    public void RefreshContent()
    {
        if(MapObject == null)
        {
            return;
        }

        if (MapObject.TypeKnown)
        {
            switch (MapObject.ContactType)
            {
                case ContactType.Station:
                    Icon = StationIconTexture;
                    break;
                case ContactType.Sub:
                case ContactType.Minisub:
                    Icon = SubmarineIconTexture;
                    break;
                case ContactType.Wreck:
                    Icon = WreckIconTexture;
                    break;
                case ContactType.Waypoint:
                    Icon = WaypointIconTexture;
                    break;
                case ContactType.Landmark:
                    Icon = LandmarkIconTexture;
                    break;
                default:
                    Icon = null;
                    break;
            }
        }
        else
        {
            Icon = UnknownIconTexture;
        }

        if (MapObject.Identified)
        {
            Text = MapObject.FriendlyName;
        }
        else
        {
            Text = "Unknown Contact";
        }
    }

    public void CenterViewOnObject()
    {
        if (MapObject == null)
        {
            return;
        }

        this.FindParent<Mapscreen>().CenterView(MapObject);
    }
}
