using Godot;

public class MapPathDot : TextureRect
{
    [Export]
    public float TimeToLive = 60f;

    public float _maxTime;

    public Vector3 WorldPosition { get; set; }

    public float StrengthModifier { get; set; } = 1;

    public override void _Ready()
    {
        base._Ready();
        _maxTime = TimeToLive;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        Modulate = new Color(0, 1, 0, StrengthModifier * TimeToLive / _maxTime);
    }

}
