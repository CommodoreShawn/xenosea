using Godot;
using System;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Hails;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

public class SubmarineHailPopup : Popup
{
    [Export]
    public PackedScene SubmarineHailOptionButtonPrefab;

    private Label _titleLabel;
    private Label _bodyLabel;
    private VBoxContainer _optionButtonContainer;
    
    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    private Submarine _targetSubmarine;
    private IHailResponseLogic _hailResponse;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _titleLabel = GetNode<Label>("PanelContainer/VBoxContainer/TitleLabel");
        _bodyLabel = GetNode<Label>("PanelContainer/VBoxContainer/BodyLabel");
        _optionButtonContainer = GetNode<VBoxContainer>("PanelContainer/VBoxContainer/VBoxContainer");
    }

    public void ShowFor(HailResponseSimulationEvent hailResponseSimulationEvent)
    {
        _hailResponse = hailResponseSimulationEvent.HailResponse;
        _targetSubmarine = hailResponseSimulationEvent.TargetSubmarine;

        RefreshContent();

        PopupCentered();
    }

    private void RefreshContent()
    {
        _titleLabel.Text = _hailResponse.ConversationTitle;

        _bodyLabel.Text = _hailResponse.ConversationBody;

        _optionButtonContainer.ClearChildren();

        foreach (var option in _hailResponse.ResponseOptions)
        {
            var button = SubmarineHailOptionButtonPrefab.Instance() as SubmarineHailOptionButton;
            button.Text = option.Text;
            button.Callback = () =>
            {
                option.OnPicked();
                RefreshContent();
                _soundEffectPlayer.PlayUiClick();
            };
            _optionButtonContainer.AddChild(button);
        }

        var endButton = SubmarineHailOptionButtonPrefab.Instance() as SubmarineHailOptionButton;
        endButton.Text = "End Transmission";
        endButton.Callback = Close;
        endButton.ShortcutInTooltip = false;
        endButton.Shortcut = new ShortCut
        {
            Shortcut = new InputEventAction
            {
                Action = "ui_cancel"
            }
        };
        _optionButtonContainer.AddChild(endButton);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_targetSubmarine == null)
        {
            return;
        }

        if(!IsInstanceValid(_targetSubmarine)
            || _targetSubmarine.IsWrecked
            || !_targetSubmarine.SonarContactRecords.Values.Any(x => x.SonarContact == _playerData.PlayerSub && x.IsDetected)
            || !_playerData.PlayerSub.SonarContactRecords.Values.Any(x => x.SonarContact == _targetSubmarine && x.IsDetected))
        {
            Close();
        }
    }

    public void Close()
    {
        _soundEffectPlayer.PlayUiClick();
        _targetSubmarine = null;
        _hailResponse = null;
        Hide();
    }
}
