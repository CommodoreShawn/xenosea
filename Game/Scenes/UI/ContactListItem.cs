using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

public class ContactListItem : Button
{
    [Export]
    public Texture DefaultIcon;
    [Export]
    public Texture WaypointIcon;
    [Export]
    public Texture SpeakingIcon;

    private GameplayUi _gameplayUi;
    private ITargetableThing _contact;

    private bool _wasTypeKnown;
    private bool _wasIdentified;
    private bool _wasSpeaking;

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private INotificationSystem _notificationSystem;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _gameplayUi = this.FindParent<GameplayUi>();
    }

    public void UpdateContact(ITargetableThing contact)
    {
        if(_contact != contact)
        {
            _contact = contact;

            _wasTypeKnown = false;
            _wasIdentified = false;
            Icon = DefaultIcon;
            Text = "Unknown Contact";
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_contact != null && _playerData.PlayerSub != null)
        {
            if(!_wasSpeaking && _contact.IsSpeaking)
            {
                _wasSpeaking = true;
                Icon = SpeakingIcon;
            }
            else if(_wasSpeaking && !_contact.IsSpeaking)
            {
                _wasSpeaking = false;
                _wasTypeKnown = false;
                _wasIdentified = false;
                Icon = DefaultIcon;
            }

            if (!_wasTypeKnown && _contact.TypeKnown)
            {
                Icon = _gameplayUi.IconForType(_contact.ContactType);
                _wasTypeKnown = true;

                if (_contact is MissionPath)
                {
                    Icon = WaypointIcon;
                }
            }
            
            if (!_wasIdentified && _contact.Identified)
            {
                Text = _contact.FriendlyName;
                _wasIdentified = true;
            }

            Pressed = _gameplayUi.CurrentTarget == _contact;

            if (_contact is MissionPath || _contact is SimpleWaypoint)
            {
                if (_contact == _gameplayUi.CurrentTarget)
                {
                    Modulate = Colors.Pink;
                }
                else
                {
                    Modulate = Colors.Purple;
                }
            }
            else if (_contact.Identified
                && _contact is SonarContactRecord scr1
                && scr1.SonarContact.IsHostileTo(_playerData.PlayerSub)
                && scr1.ContactType != ContactType.Wreck)
            {
                if (_contact == _gameplayUi.CurrentTarget)
                {
                    Modulate = Colors.Orange;
                }
                else
                {
                    Modulate = Colors.Red;
                }
            }
            else if (_contact.Identified
                && _contact is SonarContactRecord scr2
                && scr2.SonarContact.IsFriendlyTo(_playerData.PlayerSub)
                && scr2.ContactType != ContactType.Wreck)
            {
                if (_contact == _gameplayUi.CurrentTarget)
                {
                    Modulate = Colors.Cyan;
                }
                else
                {
                    Modulate = Colors.Green;
                }
            }
            else
            {
                if (_contact == _gameplayUi.CurrentTarget)
                {
                    Modulate = Colors.Yellow;
                }
                else
                {
                    Modulate = Colors.White;
                }
            }

            if (_contact.IsSpeaking)
            {
                Modulate = Colors.Yellow;
            }
        }
    }

    public void OnPressed()
    {
        _gameplayUi.CurrentTarget = _contact;
    }
}
