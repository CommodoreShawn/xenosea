using Autofac;
using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

public class CargoScanUltimatumPopup : Popup
{
    private Label _titleLabel;
    private Label _bodyLabel;
    private CargoScanUltimatumSimulationEvent _ultimatum;
    private ISimulationEventBus _simulationEventBus;

    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _simulationEventBus = IocManager.IocContainer.Resolve<ISimulationEventBus>();

        _titleLabel = GetNode<Label>("PanelContainer/VBoxContainer/TitleLabel");
        _bodyLabel = GetNode<Label>("PanelContainer/VBoxContainer/BodyLabel");
    }

    public void ShowFor(CargoScanUltimatumSimulationEvent cargoScanUltimatum)
    {
        _ultimatum = cargoScanUltimatum;

        _titleLabel.Text = "Contraband Detected";

        if (cargoScanUltimatum.IsLawful)
        {
            _bodyLabel.Text = $"I've detected contraband in your hold. That's prohibited here. Drop it or there's going to be trouble.";
        }
        else
        {
            _bodyLabel.Text = $"That's some nice cargo you've got. I'll take it.";
        }

        foreach(var commdodity in cargoScanUltimatum.ContrabandFound)
        {
            _bodyLabel.Text += $"\n{cargoScanUltimatum.TargetSubmarine.CommodityStorage.GetQty(commdodity)} {commdodity.Name}";
        }

        PopupCentered();
    }

    public void Accept()
    {
        _soundEffectPlayer.PlayUiClick();

        foreach (var commdodity in _ultimatum.ContrabandFound)
        {
            _ultimatum.TargetSubmarine.JettisonCargo(commdodity, _ultimatum.TargetSubmarine.CommodityStorage.GetQty(commdodity));
        }

        _simulationEventBus.SendEvent(
            new CargoScanUltimatumResponseSimulationEvent(
                _ultimatum.DemandingSubmarine,
                _ultimatum.TargetSubmarine,
                wasAgreedTo: true));

        Hide();
    }

    public void Reject()
    {
        _soundEffectPlayer.PlayUiClick();

        _simulationEventBus.SendEvent(
            new CargoScanUltimatumResponseSimulationEvent(
                _ultimatum.DemandingSubmarine,
                _ultimatum.TargetSubmarine,
                wasAgreedTo: false));

        Hide();
    }
}
