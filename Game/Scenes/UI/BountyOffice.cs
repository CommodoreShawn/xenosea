using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class BountyOffice : HBoxContainer
{
    [Inject]
    private IBountyTracker _bountyTracker;
    [Inject]
    private IPlayerData _playerData;

    private VBoxContainer _bountyListContainer;
    private Button _redeemButton;
    private List<RegisteredBounty> _localBounties;
    
    public override void _Ready()
    {
        this.ResolveDependencies();

        _bountyListContainer = GetNode<VBoxContainer>("VendorVBoxContainer/ScrollContainer/BountyListContainer");
        _redeemButton = GetNode<Button>("VendorVBoxContainer/RedeemButton");
        _localBounties = new List<RegisteredBounty>();

        RebuildBountyList();
    }

    private void RebuildBountyList()
    {
        _bountyListContainer.ClearChildren();

        var localFactions = new List<Faction>();
        if(_playerData.PlayerSub.IsDockedTo != null)
        {
            localFactions.Add(_playerData.PlayerSub.IsDockedTo.Faction);
        }
        if(_playerData.PlayerSub.IsDockedTo is WorldStation worldStation)
        {
            localFactions.AddRange(worldStation.BackingStation.FactionsPresent);
        }

        _localBounties = _bountyTracker.AvailableBounties
            .Where(b => localFactions.Contains(b.OfferingFaction))
            .ToList();

        foreach (var bounty in _localBounties)
        {
            var label = new Label
            {
                Text = $"{bounty.BountyValue.ToString("C0")} for destruction of {bounty.TargetFaction.Name} {bounty.TargetName}.",
                Autowrap = true
            };
            _bountyListContainer.AddChild(label);
        }

        _redeemButton.Disabled = !_localBounties.Any();
    }

    public void RedeemBounties()
    {
        foreach(var bounty in _localBounties)
        {
            _bountyTracker.RedeemBounty(bounty);
        }

        RebuildBountyList();
    }
}
