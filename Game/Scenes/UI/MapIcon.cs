using Godot;
using Xenosea.Logic.Detection;
using Xenosea.Resources;

public class MapIcon : TextureRect
{
    [Export]
    public Texture IconDetected;

    [Export]
    public Texture IconPositionLocked;

    [Export]
    public Texture IconSub;

    [Export]
    public Texture IconStation;

    [Export]
    public Texture IconOutpost;

    [Export]
    public Texture IconWaypoint;

    [Export]
    public Texture IconWreck;

    [Export]
    public Texture IconTorpedo;

    public ITargetableThing TargetableThing { get; set; }

    public Faction PlayerFaction{ get; set; }

    public GameplayUi GameplayUi { get; set; }

    public float PathDotCounter { get; set; }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(TargetableThing.TypeKnown && TargetableThing.PositionLocked)
        {
            if (TargetableThing is SonarContactRecord scr)
            {
                switch (scr.SonarContact.ContactType)
                {
                    case ContactType.Outpost:
                        Texture = IconOutpost;
                        break;
                    case ContactType.Station:
                        Texture = IconStation;
                        break;
                    case ContactType.Sub:
                    case ContactType.Minisub:
                        Texture = IconSub;
                        break;
                    case ContactType.Wreck:
                        Texture = IconWreck;
                        break;
                    case ContactType.Torpedo:
                        Texture = IconTorpedo;
                        break;
                }
            }
            else if (TargetableThing is MissionPath)
            {
                Texture = IconWaypoint;
            }
        }
        else if(TargetableThing.PositionLocked)
        {
            Texture = IconPositionLocked;
        }
        else
        {
            Texture = IconDetected;
        }

        if (TargetableThing is MissionPath || TargetableThing is SimpleWaypoint)
        {
            if (TargetableThing == GameplayUi.CurrentTarget)
            {
                Modulate = Colors.Pink;
            }
            else
            {
                Modulate = Colors.Purple;
            }
        }
        else if (TargetableThing.Identified
            && TargetableThing is SonarContactRecord scr1
            && scr1.SonarContact.Faction.IsHostileTo(PlayerFaction)
            && scr1.ContactType != ContactType.Wreck)
        {
            if (TargetableThing == GameplayUi.CurrentTarget)
            {
                Modulate = Colors.Orange;
            }
            else
            {
                Modulate = Colors.Red;
            }
        }
        else if (TargetableThing.Identified
            && TargetableThing is SonarContactRecord scr2
            && scr2.SonarContact.Faction.IsFriendlyTo(PlayerFaction)
            && scr2.ContactType != ContactType.Wreck)
        {
            if (TargetableThing == GameplayUi.CurrentTarget)
            {
                Modulate = Colors.Cyan;
            }
            else
            {
                Modulate = Colors.Green;
            }
        }
        else
        {
            if (TargetableThing == GameplayUi.CurrentTarget)
            {
                Modulate = Colors.Yellow;
            }
            else
            {
                Modulate = Colors.White;
            }
        }
    }
}
