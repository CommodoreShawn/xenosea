using Godot;
using System.Linq;
using Xenosea.Logic;

public class TorpedoTubeWidget : PanelContainer
{
    [Export]
    public Texture StatusIconReady;
    [Export]
    public Texture StatusIconWait;
    [Export]
    public Texture StatusIconActive;
    [Export]
    public PackedScene TorpedoOptionButtonPrefab;

    public int Index { get; set; }
    public TorpedoTube TorpedoTube { get; set; }
    private TextureProgress _loadTextureProgress;
    private TextureRect _statusIcon;
    private Button _aimModeButton;
    private Button _activeModeButton;
    private Button _wireCutButton;
    private GameplayUi _gameplayUi;
    private Button _setWeaponButton;
    private VBoxContainer _optionBox;

    private string _action;

    private int _previousTorpsAvailable;

    public override void _Ready()
    {
        GetNode<Button>("VBoxContainer/SelectButton").Text = Index.ToString();
        _loadTextureProgress = GetNode<TextureProgress>("VBoxContainer/HBoxContainer/LeftStack/TorpedoTubeProgress");
        _optionBox = GetNode<VBoxContainer>("VBoxContainer/HBoxContainer/ScrollContainer/OptionBox");
        _statusIcon = GetNode<TextureRect>("VBoxContainer/HBoxContainer/LeftStack/TorpModeButtons/TorpStatusIcon");
        _aimModeButton = GetNode<Button>("VBoxContainer/HBoxContainer/LeftStack/TorpModeButtons/TorpAimModeButton");
        _activeModeButton = GetNode<Button>("VBoxContainer/HBoxContainer/LeftStack/TorpModeButtons/TorpActiveModeButton");
        _wireCutButton = GetNode<Button>("VBoxContainer/HBoxContainer/LeftStack/TorpModeButtons/TorpWireCutButton");
        _gameplayUi = this.FindParent<GameplayUi>();
        _setWeaponButton = GetNode<Button>("VBoxContainer/SelectButton");
        _optionBox.ClearChildren();

        _action = "switch_ammo_" + Index;

        SetAmmo(0);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        var optionButtons = _optionBox.GetChildren().OfType<TorpedoOptionButton>().ToArray();
        for (var i = 0; i < TorpedoTube.TorpedoesAvailable.Count; i++)
        {
            if(i < optionButtons.Count())
            {
                optionButtons[i].Pressed = i == TorpedoTube.NextAmmoIndex;
            }
            else
            {
                var newButton = TorpedoOptionButtonPrefab.Instance() as TorpedoOptionButton;
                newButton.Index = i;
                newButton.TorpedoTube = TorpedoTube;
                _optionBox.AddChild(newButton);
            }
        }

        if (Input.IsActionJustPressed(_action))
        {
            if (_gameplayUi.ActiveWeaponSystem == TorpedoTube)
            {
                var nextAmmo = TorpedoTube.NextAmmoIndex + 1;
                if(nextAmmo >= TorpedoTube.TorpedoesAvailable.Count)
                {
                    nextAmmo = 0;
                }
                TorpedoTube.ChangeAmmo(nextAmmo);
            }
            else
            {
                _gameplayUi.ActiveWeaponSystem = TorpedoTube;
            }
        }

        _loadTextureProgress.Value = (TorpedoTube.LoadProgress / TorpedoTube.LoadTime) * 100;

        if (TorpedoTube.ActiveTorpedo != null)
        {
            _statusIcon.Texture = StatusIconActive;
        }
        else if (TorpedoTube.LoadProgress == TorpedoTube.LoadTime)
        {
            _statusIcon.Texture = StatusIconReady;
        }
        else
        {
            _statusIcon.Texture = StatusIconWait;
        }

        _setWeaponButton.Pressed = _gameplayUi.ActiveWeaponSystem == TorpedoTube;
        _loadTextureProgress.TextureProgress_ = TorpedoTube.ActiveAmmo?.Icon;

        _wireCutButton.Disabled = TorpedoTube.ActiveTorpedo == null;
        _activeModeButton.Disabled = TorpedoTube.ActiveTorpedo == null;

        _activeModeButton.Pressed = TorpedoTube.ActiveTorpedo?.ActiveSeeking ?? false;

        _aimModeButton.Disabled = _gameplayUi.CurrentTarget == null;

        if (TorpedoTube.ActiveTorpedo == null)
        {
            if (_aimModeButton.Pressed && !_aimModeButton.Disabled)
            {
                TorpedoTube.Target = _gameplayUi.CurrentTarget;
            }
            else
            {
                TorpedoTube.Target = null;
            }
        }
        else
        {
            _aimModeButton.Pressed = _gameplayUi.CurrentTarget == TorpedoTube.ActiveTorpedo.Target;
        }
    }

    public void SetAmmo(int index)
    {
        TorpedoTube.ChangeAmmo(index);
    }

    public void SetWeapon()
    {
        _gameplayUi.ActiveWeaponSystem = TorpedoTube;
    }

    public void CutWire()
    {
        if (TorpedoTube.ActiveTorpedo != null)
        {
            TorpedoTube.ActiveTorpedo.ActiveSeeking = true;
            TorpedoTube.ActiveTorpedo.OnWire = false;
            TorpedoTube.ActiveTorpedo = null;
        }
    }

    public void ActivateTorpedo()
    {
        if (TorpedoTube.ActiveTorpedo != null)
        {
            TorpedoTube.ActiveTorpedo.ActiveSeeking = true;
        }
    }

    public void RetargetTorpedo()
    {
        if (TorpedoTube.ActiveTorpedo != null)
        {
            TorpedoTube.ActiveTorpedo.Target = _gameplayUi.CurrentTarget;
        }
    }
}