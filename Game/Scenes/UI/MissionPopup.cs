using Godot;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.Infrastructure.Missions;

public class MissionPopup : Popup
{
    [Export]
    public PackedScene MissionOptionButtonPrefab;

    private VBoxContainer _missionButtonList;
    private Label _missionNameLabel;
    private Label _missionForFactionNameLabel;
    private Label _missionDescriptionLabel;
    private Label _missionDetailLabel0;
    private Label _missionDetailLabel1;
    private Label _missionDetailLabel2;
    private Button _abandonButton;
    private Button _deliverButton;
    private Button _trackButton;
    private GameplayUi _gameplayUi;

    private IMission _activeMission;

    [Inject]
    public IPlayerData _playerData;
    [Inject]
    public IMissionTracker _missionTracker;
    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _gameplayUi = this.FindParent<GameplayUi>();
        _missionButtonList = GetNode<VBoxContainer>("PanelContainer/VBoxContainer/HBoxContainer2/ScrollContainer/MissionButtonList");
        _missionNameLabel = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/MissionNameLabel");
        _missionDescriptionLabel = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/MissionDescriptionLabel");
        _missionDetailLabel0 = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/MissionDetailLabel0");
        _missionDetailLabel1 = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/MissionDetailLabel1");
        _missionDetailLabel2 = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/MissionDetailLabel2");
        _abandonButton = GetNode<Button>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/HBoxContainer/AbandonButton");
        _deliverButton = GetNode<Button>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/HBoxContainer/DeliverButton");
        _trackButton = GetNode<Button>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/HBoxContainer/TrackButton");
        _missionForFactionNameLabel = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/MissionForFactionLabel");
    }

    public void InitAndShow()
    {
        SetActiveMission(null);

        _missionButtonList.ClearChildren();
        foreach (var mission in _missionTracker.GetCurrentMissions(_playerData.PlayerSub))
        {
            if (_activeMission == null)
            {
                SetActiveMission(mission);
            }

            var button = MissionOptionButtonPrefab.Instance() as MissionOptionButton;
            button.Initialize(mission);
            _missionButtonList.AddChild(button);
        }

        PopupCentered();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (Visible)
        {
            foreach (var button in _missionButtonList.GetChildren().OfType<MissionOptionButton>())
            {
                button.Pressed = button.Mission == _activeMission;

                if(button.Mission.IsExpired)
                {
                    button.QueueFree();
                }
            }

            if(_activeMission != null && _activeMission.IsExpired)
            {
                InitAndShow();
            }

            _deliverButton.Disabled = _activeMission == null || !_activeMission.CanBeDelivered(_playerData.PlayerSub);
            _trackButton.Disabled = _activeMission == null;
            _trackButton.Pressed = _activeMission != null && _activeMission == _missionTracker.TrackedMission;
        }
    }

    public void SetActiveMission(IMission mission)
    {
        _activeMission = mission;

        if (_activeMission != null)
        {
            _missionNameLabel.Text = _activeMission.Name;
            _missionDescriptionLabel.Text = _activeMission.Description;
            _missionDetailLabel0.Text = _activeMission.Detail0;
            _missionDetailLabel1.Text = _activeMission.Detail1;
            _missionDetailLabel2.Text = _activeMission.Detail2;
            _abandonButton.Visible = true;
            _deliverButton.Visible = true;

            if(_activeMission.ForFaction != null)
            {
                _missionForFactionNameLabel.Text = "for " + _activeMission.ForFaction.Name;
            }
            else
            {
                _missionForFactionNameLabel.Text = string.Empty;
            }
        }
        else
        {
            _missionNameLabel.Text = string.Empty;
            _missionForFactionNameLabel.Text = string.Empty;
            _missionDescriptionLabel.Text = string.Empty;
            _missionDetailLabel0.Text = string.Empty;
            _missionDetailLabel1.Text = string.Empty;
            _missionDetailLabel2.Text = string.Empty;
            _abandonButton.Visible = false;
            _deliverButton.Visible = false;
        }
    }

    public void Close()
    {
        _soundEffectPlayer.PlayUiClick();
        Hide();
    }

    public void AbandonMission()
    {
        _soundEffectPlayer.PlayUiClick();
        if (_activeMission is IQuest quest)
        {
            quest.Reset();
        }

        _missionTracker.DestroyMission(_activeMission, _playerData.PlayerSub);
        InitAndShow();
    }

    public void DeliverMission()
    {
        _soundEffectPlayer.PlayUiClick();
        _playerData.Money += _activeMission.Deposit + _activeMission.Reward;
        _missionTracker.DeliverMission(_activeMission, _playerData.PlayerSub.IsDockedTo, _playerData.PlayerSub);
        InitAndShow();
    }

    public void TrackMission()
    {
        _soundEffectPlayer.PlayUiClick();
        if (_missionTracker.TrackedMission == _activeMission)
        {
            _missionTracker.TrackedMission = null;
        }
        else
        {
            _missionTracker.TrackedMission = _activeMission;
        }
    }
}
