﻿public interface IStationRoomWidgetContents
{
    void InitializeFor(StationRoom stationRoom);
}