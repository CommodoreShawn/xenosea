using Godot;
using Xenosea.Logic;

public class TorpedoDealerOptionButton : Button
{
    public bool IsForVendor { get; set; }
    public TorpedoType TorpedoType { get; set; }

    public override void _Ready()
    {
        if (TorpedoType != null)
        {
            Icon = TorpedoType.Icon;
            Text = TorpedoType.Name;
        }
    }

    public void OnPressed()
    {
        this.FindParent<TorpedoVendor>().SetActiveTorpedoType(TorpedoType, IsForVendor);
    }
}
