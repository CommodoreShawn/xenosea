using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;

public class LoadoutPopup : Popup
{
    [Export]
    public PackedScene LoadoutItemPrefab;

    private Label _titleLabel;
    private TextureProgress _cargoProgress;
    private VBoxContainer _equipmentList;
    private VBoxContainer _cargoList;
    private Label _subTypeNameLabel;
    private Label _owningFactionNameLabel;
    private Label _subTypeDescriptionLabel;

    public Submarine Submarine { get; private set; }
    public ISalvageTarget SalvageTarget { get; private set; }

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _titleLabel = GetNode<Label>("PanelContainer/VBoxContainer/Label");
        _cargoProgress = GetNode<TextureProgress>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer1/HBoxContainer/VBoxContainer/CargoProgress");
        _equipmentList = GetNode<VBoxContainer>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer0/ScrollContainer/EquipmentList");
        _cargoList = GetNode<VBoxContainer>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer1/ScrollContainer/CargoList");
        _subTypeNameLabel = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer4/SubTypeNameLabel");
        _owningFactionNameLabel = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer4/OwningFactionNameLabel");
        _subTypeDescriptionLabel = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer4/SubTypeDescriptionLabel");
    }

    public void ShowFor(Submarine submarine)
    {
        if(submarine == _playerData.PlayerSub)
        {
            _titleLabel.Text = "Your Loadout";
        }
        else
        {
            _titleLabel.Text = $"{submarine.Faction.Name} {submarine.FriendlyName}"; 
        }
        
        SalvageTarget = null;
        Submarine = submarine;

        UpdateContents();

        PopupCentered();
    }

    public void ShowFor(ISalvageTarget salvageTarget)
    {
        _titleLabel.Text = salvageTarget.FriendlyName;

        SalvageTarget = salvageTarget;
        Submarine = null;

        UpdateContents();

        PopupCentered();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        UpdateContents();
    }

    private void UpdateContents()
    {
        if(Submarine != null)
        {
            UpdateContentsSubmarine();
            
        }
        else if(SalvageTarget != null)
        {
            UpdateContentsSalvageTarget();
        }
    }

    private void UpdateContentsSubmarine()
    {
        _subTypeNameLabel.Text = Submarine.HullType.Name;
        _subTypeDescriptionLabel.Text = Submarine.HullType.Description;

        if (Submarine != _playerData.PlayerSub)
        {
            _owningFactionNameLabel.Text = Submarine.Faction.Name;

            if (Submarine.Faction.IsHostileTo(_playerData.Faction))
            {
                _owningFactionNameLabel.Modulate = Colors.Red;
            }
            else if (Submarine.Faction.IsFriendlyTo(_playerData.Faction))
            {
                _owningFactionNameLabel.Modulate = Colors.Green;
            }
            else
            {
                _owningFactionNameLabel.Modulate = Colors.White;
            }
        }
        else
        {
            _owningFactionNameLabel.Text = string.Empty;
        }

        var loadout = Submarine.GetLoadout();

        var index = 0;
        var item = GetOrMakeLoadoutItem(index, _equipmentList);
        item.SetContentFor(loadout.ArmorType);
        index += 1;

        foreach(var module in loadout.GetAllModules())
        {
            item = GetOrMakeLoadoutItem(index, _equipmentList);
            item.SetContentFor(module);
            index += 1;
        }

        for(;index < _equipmentList.GetChildCount(); index++)
        {
            _equipmentList.GetChild(index).QueueFree();
        }

        index = 0;
        foreach (var commodity in Submarine.CommodityStorage.CommoditiesPresent)
        {
            item = GetOrMakeLoadoutItem(index, _cargoList);
            item.SetContentFor(commodity, Submarine.CommodityStorage.GetQty(commodity), Submarine.IsPlayerSub);
            index += 1;
        }
        for (; index < _cargoList.GetChildCount(); index++)
        {
            _cargoList.GetChild(index).QueueFree();
        }
        _cargoProgress.Visible = true;
        _cargoProgress.Value = 100 * (Submarine.CommodityStorage.TotalVolume / Submarine.CargoVolume);
        _cargoProgress.HintTooltip = $"{Submarine.CommodityStorage.TotalVolume.ToString("N0")} / {Submarine.CargoVolume.ToString("N0")}";
    }

    private void UpdateContentsSalvageTarget()
    {
        _subTypeNameLabel.Text = string.Empty;
        _subTypeDescriptionLabel.Text = string.Empty;
        _owningFactionNameLabel.Text = string.Empty;

        _equipmentList.ClearChildren();

        var index = 0;
        foreach (var commodity in SalvageTarget.CommodityStorage.CommoditiesPresent)
        {
            var item = GetOrMakeLoadoutItem(index, _cargoList);
            item.SetContentFor(commodity, SalvageTarget.CommodityStorage.GetQty(commodity), false);
            index += 1;
        }
        for (; index < _cargoList.GetChildCount(); index++)
        {
            _cargoList.GetChild(index).QueueFree();
        }
        _cargoProgress.Visible = false;
    }

    private LoadoutItem GetOrMakeLoadoutItem(int index, VBoxContainer list)
    {
        LoadoutItem item;

        if(list.GetChildCount() <= index)
        {
            item = LoadoutItemPrefab.Instance() as LoadoutItem;
            list.AddChild(item);
        }
        else
        {
            item = list.GetChildOrNull<LoadoutItem>(index);
        }

        return item;
    }

    public void Close()
    {
        _soundEffectPlayer.PlayUiClick();
        Submarine = null;
        SalvageTarget = null;
        Hide();
    }
}
