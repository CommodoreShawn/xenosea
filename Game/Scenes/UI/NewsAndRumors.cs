using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Dialog;
using Xenosea.Logic.Infrastructure.Interfaces;

public class NewsAndRumors : Control
{
    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IConversationManager _conversationManager;
    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    private VBoxContainer _rumorsList;
    private VBoxContainer _newsList;
    
    public override void _Ready()
    {
        this.ResolveDependencies();

        _rumorsList = GetNode<VBoxContainer>("VBoxContainer/VBoxContainer3");
        _newsList = GetNode<VBoxContainer>("VBoxContainer2/ScrollContainer/VBoxContainer");
    }

    public void InitializeFor(StationRoom stationRoom)
    {
        _rumorsList.ClearChildren();
        _newsList.ClearChildren();

        var station = _playerData.PlayerSub.IsDockedTo as WorldStation;
        if(station != null)
        {
            var rumors = _conversationManager.GetRumorsFor(station, 1);

            foreach(var response in rumors)
            {
                AddRumorResponse(response);
            }
        }
    }

    public void RefreshRumor()
    {
        _soundEffectPlayer.PlayUiClick();

        _rumorsList.ClearChildren();

        var station = _playerData.PlayerSub.IsDockedTo as WorldStation;
        if (station != null)
        {
            var rumors = _conversationManager.GetRumorsFor(station, 1);

            foreach (var response in rumors)
            {
                AddRumorResponse(response);
            }
        }
    }

    private void AddRumorResponse(ConversationResponse response)
    {
        foreach (var line in response.Lines)
        {
            if (line.Speaker != "Player")
            {
                _rumorsList.AddChild(new Label
                {
                    Text = line.Text.Trim(),
                    Autowrap = true,
                    Modulate = Colors.White,
                    Align = Label.AlignEnum.Left
                });
            }
        }

        foreach (var action in response.Actions)
        {
            var value = action.Invoke();

            if (!string.IsNullOrWhiteSpace(value))
            {
                if (value.StartsWith("~"))
                {
                    _rumorsList.AddChild(new Label()
                    {
                        Text = value.Substring(1),
                        Autowrap = true,
                        Modulate = Colors.Red,
                        Align = Label.AlignEnum.Left
                    });
                }
                else
                {
                    _rumorsList.AddChild(new Label()
                    {
                        Text = value,
                        Autowrap = true,
                        Modulate = Colors.Green,
                        Align = Label.AlignEnum.Left
                    });
                }
            }
        }
    }
}
