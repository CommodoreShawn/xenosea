using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

public class SaveLoadPopup : PopupPanel
{
    private Label _titleLabel;
    private ItemList _fileList;
    private LineEdit _nameEdit;
    private Label _lastPlayedLabel;
    private Label _subLabel;
    private Label _locationLabel;
    private Label _moneyLabel;
    private Label _gameDateLabel;
    private Button _saveLoadButton;
    private Button _deleteButton;

    private bool _inSaveMode;
    private SaveListItem _selectedFile;

    private List<SaveListItem> _saveListItems;
    private Action<string> _filePickedCallback;

    [Inject]
    private IGameSerializer _gameSerializer;
    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _titleLabel = GetNode<Label>("VBoxContainer/TitleLabel");
        _fileList = GetNode<ItemList>("VBoxContainer/HBoxContainer/VBoxContainer2/ItemList");
        _nameEdit = GetNode<LineEdit>("VBoxContainer/HBoxContainer/VBoxContainer/NameEdit");
        _lastPlayedLabel = GetNode<Label>("VBoxContainer/HBoxContainer/VBoxContainer/LastPlayedLabel");
        _subLabel = GetNode<Label>("VBoxContainer/HBoxContainer/VBoxContainer/SubLabel");
        _locationLabel = GetNode<Label>("VBoxContainer/HBoxContainer/VBoxContainer/LocationLabel");
        _moneyLabel = GetNode<Label>("VBoxContainer/HBoxContainer/VBoxContainer/MoneyLabel");
        _gameDateLabel = GetNode<Label>("VBoxContainer/HBoxContainer/VBoxContainer/GameDateLabel");
        _saveLoadButton = GetNode<Button>("VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer/SaveLoadButton");
        _deleteButton = GetNode<Button>("VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer/DeleteButton");
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_inSaveMode && (_selectedFile?.IsNewSave ?? false))
        {
            _saveLoadButton.Disabled = !_gameSerializer.IsValidFileName(_nameEdit.Text);
        }
    }

    public void ShowForSave(Action<string> filePickedCallback)
    {
        _inSaveMode = true;

        _titleLabel.Text = "Save Game";
        _saveLoadButton.Text = "Save";
        _filePickedCallback = filePickedCallback;

        RefreshSaveList();
        PopupCentered();
    }

    public void ShowForLoad(Action<string> filePickedCallback)
    {
        _inSaveMode = false;

        _titleLabel.Text = "Load Game";
        _saveLoadButton.Text = "Load";
        _filePickedCallback = filePickedCallback;
        
        RefreshSaveList();
        PopupCentered();
    }

    private void RefreshSaveList()
    {
        _saveListItems = _gameSerializer.ListSaves()
            .OrderByDescending(x => x.LastPlayed)
            .ToList();

        if (_inSaveMode)
        {
            _saveListItems.Insert(0, new SaveListItem
            {
                Name = "New file...",
                IsNewSave = true
            });
        }

        _fileList.Clear();
        foreach(var saveListItem in _saveListItems)
        {
            _fileList.AddItem(saveListItem.Name, null, true);
        }

        OnFileSelected(0);
    }

    public void OnFileSelected(int itemSelected)
    {
        _soundEffectPlayer.PlayUiClick();
        if (itemSelected < _saveListItems.Count)
        {
            _selectedFile = _saveListItems[itemSelected];

            _nameEdit.Text = _selectedFile.Name;
            _nameEdit.Editable = _selectedFile.IsNewSave;
            _lastPlayedLabel.Text = $"Last Played: {_selectedFile.LastPlayed.ToString()}";
            _saveLoadButton.Disabled = false;
            _deleteButton.Disabled = _selectedFile.IsNewSave;

            if (_selectedFile.IsNewSave)
            {
                _nameEdit.Text = string.Empty;
                _nameEdit.PlaceholderText = "New file...";
                _lastPlayedLabel.Text = string.Empty;
                _subLabel.Text = string.Empty;
                _locationLabel.Text = string.Empty;
                _moneyLabel.Text = string.Empty;
                _gameDateLabel.Text = string.Empty;
            }
            else
            {
                var saveSummary = _gameSerializer.GetSaveSummary(_selectedFile.Name);
                _nameEdit.PlaceholderText = string.Empty;
                _subLabel.Text = $"Submarine: {saveSummary.SubClass}";
                _locationLabel.Text = $"Location: {saveSummary.LocationName}";
                _moneyLabel.Text = $"Money: {saveSummary.Money.ToString("C0")}";
                _gameDateLabel.Text = $"Date: {saveSummary.InGameDate.ToString()}";
            }
        }
        else
        {
            _selectedFile = null;

            _nameEdit.Text = string.Empty;
            _nameEdit.Editable = false;
            _lastPlayedLabel.Text = string.Empty;
            _subLabel.Text = string.Empty;
            _locationLabel.Text = string.Empty;
            _moneyLabel.Text = string.Empty;
            _gameDateLabel.Text = string.Empty;

            _saveLoadButton.Disabled = true;
            _deleteButton.Disabled = true;
        }
    }

    public void SaveLoad()
    {
        _soundEffectPlayer.PlayUiClick();
        if (_inSaveMode && (_selectedFile?.IsNewSave ?? true))
        {
            _filePickedCallback.Invoke(_nameEdit.Text);
        }
        else
        {
            _filePickedCallback.Invoke(_selectedFile.Name);
        }

        Hide();
    }

    public void Delete()
    {
        _soundEffectPlayer.PlayUiClick();
        _gameSerializer.DeleteSave(_selectedFile.Name);

        RefreshSaveList();
    }

    public void Cancel()
    {
        _soundEffectPlayer.PlayUiClick();
        Hide();
    }
}
