using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class ModulePickerPopup : Popup
{
    [Inject]
    private IPlayerData _playerData;

    private Label _titleLabel;
    private VBoxContainer _optionList;
    private Label _selectedOptionNameLabel;
    private GridContainer _selectedOptionStatGrid;
    private Label _selectedOptionDescriptionLabel;
    private Button _acceptButton;
    private Label _errorLabel;

    private SubHullType _subHullType;
    private object[] _options;
    private Button[] _optionButtons;
    private object _selectedOption;
    private object _originalOption;
    private Action<object> _acceptCallback;
    private Action _cancelCallback;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _titleLabel = GetNode<Label>("PanelContainer/VBoxContainer/TitleLabel");
        _optionList = GetNode<VBoxContainer>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer0/ScrollContainer/OptionList");
        _selectedOptionNameLabel = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer4/SelectedOptionNameLabel");
        _selectedOptionStatGrid = GetNode<GridContainer>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer4/StatsBox");
        _selectedOptionDescriptionLabel = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer4/SubTypeDescriptionLabel");
        _acceptButton = GetNode<Button>("PanelContainer/VBoxContainer/HBoxContainer/AcceptButton");
        _errorLabel = GetNode<Label>("PanelContainer/VBoxContainer/ErrorLabel");
    }

    public void ShowFor(
        string title, 
        object selectedOption, 
        IEnumerable<object> options, 
        Action<object> acceptCallback,
        SubHullType subHullType,
        Action cancelCallback)
    {
        _subHullType = subHullType;
        _originalOption = selectedOption;
        _titleLabel.Text = title.ToUpper();
        _options = options.ToArray();
        _acceptCallback = acceptCallback;
        _cancelCallback = cancelCallback;

        _optionList.ClearChildren();
        _optionButtons = new Button[_options.Length];

        for (var i = 0; i < _options.Length; i++)
        {
            var button = new Button
            {
                Text = GetNameOf(_options[i]),
                ToggleMode = true
            };

            button.Connect("pressed", this, "OptionPicked", new Godot.Collections.Array { _options[i] });

            _optionButtons[i] = button;
            _optionList.AddChild(button);
        }

        OptionPicked(selectedOption);
        ShowModal();
    }

    public override void _Process(float delta)
    {
        if (Visible)
        {
            var reputationMet = true;

            if(_selectedOption is BasicModuleType moduleType)
            {
                reputationMet = _playerData.Faction
                    .GetStandingTo(_playerData.PlayerSub.IsDockedTo.Faction) >= moduleType.ReputationRequired;
            }
            else if (_selectedOption is SubHullType hullType)
            {
                reputationMet = _playerData.Faction
                    .GetStandingTo(_playerData.PlayerSub.IsDockedTo.Faction) >= hullType.ReputationRequired;
            }

            if (!reputationMet)
            {
                _errorLabel.Text = "Your standing isn't high enough.";
            }
            else
            {
                _errorLabel.Text = string.Empty;
            }

            _acceptButton.Disabled = _selectedOption == null
                || _selectedOption == _originalOption
                || !reputationMet;

            for (var i = 0; i < _options.Length; i++)
            {
                _optionButtons[i].Pressed = _selectedOption == _options[i];
            }
        }
    }

    public void OptionPicked(object option)
    {
        _selectedOption = option;

        if(option != null)
        {
            _selectedOptionNameLabel.Text = GetNameOf(option);
            _selectedOptionDescriptionLabel.Text = GetDescriptionOf(option);
            _selectedOptionStatGrid.ClearChildren();
            BuildStatsGrid(option);
        }
        else
        {
            _selectedOptionNameLabel.Text = string.Empty;
            _selectedOptionDescriptionLabel.Text = string.Empty;
            _selectedOptionStatGrid.ClearChildren();
        }
    }

    public void Accept()
    {
        _acceptCallback(_selectedOption);
        Hide();
    }

    public void Close()
    {
        if(_cancelCallback != null)
        {
            _cancelCallback();
        }

        Hide();
    }

    private string GetNameOf(object option)
    {
        if(option is SubHullType hullType)
        {
            return hullType.Name;
        }
        else if (option is ArmorType armorType)
        {
            return armorType.Name;
        }
        else if(option is BasicModuleType basicModuleType)
        {
            return basicModuleType.Name;
        }
        else
        {
            return option.ToString();
        }
    }

    private string GetDescriptionOf(object option)
    {
        if (option is SubHullType hullType)
        {
            return hullType.Description;
        }
        else if (option is ArmorType armorType)
        {
            return armorType.Description;
        }
        else if (option is BasicModuleType basicModuleType)
        {
            return basicModuleType.Description;
        }
        else
        {
            return option.ToString();
        }
    }

    private void BuildStatsGrid(object option)
    {
        if (option is SubHullType hullType)
        {
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Health: {hullType.Health.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Turrets: {hullType.Turrets.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Launchers: {hullType.TorpedoTubes.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Hangars: {hullType.MinisubHangars.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Drag: {hullType.FrontDragFactor.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Value: {hullType.Value.ToString("C0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
        }
        else if (option is ArmorType armorType)
        {
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Armor: {armorType.Armor.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Drag: {armorType.DragFactor.ToString("P0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Volume: {(armorType.VolumePerSurfaceArea * _subHullType.HullSurfaceArea).ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Value: {(armorType.ValuePerSurfaceArea * _subHullType.HullSurfaceArea).ToString("C0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
        }
        else if (option is EngineModuleType engineModuleType)
        {
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Thrust: {engineModuleType.MaximumThrust.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Noise: {engineModuleType.MaximumNoise.ToString("P0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Turn Factor: {engineModuleType.TurningFactor.ToString("N1")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Control());
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Volume: {engineModuleType.Volume.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Value: {engineModuleType.Value.ToString("C0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
        }
        else if (option is SonarModuleType sonarModuleType)
        {
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Submarines: {sonarModuleType.SubFactor.ToString("P0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Minerals: {sonarModuleType.OreFactor.ToString("P0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Sensitivity: {sonarModuleType.Sensitivity.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Control());
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Volume: {sonarModuleType.Volume.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Value: {sonarModuleType.Value.ToString("C0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
        }
        else if (option is TowedArrayModuleType towedArrayModuleType)
        {
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Minerals: {towedArrayModuleType.OreFactor.ToString("P0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Submarines: {towedArrayModuleType.SubFactor.ToString("P0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Sensitivity: {towedArrayModuleType.Sensitivity.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Control());
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Volume: {towedArrayModuleType.Volume.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Value: {towedArrayModuleType.Value.ToString("C0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
        }
        else if (option is DiveChamberModuleType diveChamberModuleType)
        {
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Divers: {diveChamberModuleType.DiverCapacity.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Control());
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Volume: {diveChamberModuleType.Volume.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Value: {diveChamberModuleType.Value.ToString("C0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
        }
        else if (option is TurretModuleType turretModuleType)
        {
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Shots: {turretModuleType.ShotCount.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Reload: {turretModuleType.ReloadTime.ToString("N1")} s",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            var ammoGroup = new HBoxContainer();
            foreach (var ammoType in turretModuleType.AmmunitionOptions)
            {
                ammoGroup.AddChild(new TextureRect
                {
                    Texture = ammoType.SmallIcon,
                    HintTooltip = $"{ammoType.Name}\nDamage: {ammoType.Damage}\nPenetration: {ammoType.ArmorPenetration}\nRange{Util.DistanceToDisplay(ammoType.MaximumRange, false)}"
                });
            }
            _selectedOptionStatGrid.AddChild(ammoGroup);
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Turn Rate: {turretModuleType.TurnRate.ToString("N2")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Volume: {turretModuleType.Volume.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Value: {turretModuleType.Value.ToString("C0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
        }
        else if (option is TorpedoLauncherModuleType launcherModuleType)
        {
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Capacity: {launcherModuleType.MagazineCapacity.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Reload Time: {launcherModuleType.ReloadTime.ToString("N0")} s",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Size Limit: {launcherModuleType.MaximumTorpedoSize}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Control());
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Volume: {launcherModuleType.Volume.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Value: {launcherModuleType.Value.ToString("C0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
        }
        else if (option is MinisubHangarModuleType hangarModuleType)
        {
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Volume: {hangarModuleType.Volume.ToString("N0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
            _selectedOptionStatGrid.AddChild(new Label
            {
                Text = $"Value: {hangarModuleType.Value.ToString("C0")}",
                SizeFlagsHorizontal = (int)SizeFlags.ExpandFill
            });
        }
    }
}
