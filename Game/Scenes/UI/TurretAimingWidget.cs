using Godot;
using Xenosea.Logic;

public class TurretAimingWidget : Control
{
    [Export]
    public Texture AimedUnloaded;

    [Export]
    public Texture UnaimedUnloaded;

    [Export]
    public Texture AimedLoaded;

    [Export]
    public Texture UnaimedLoaded;

    public TurretMount Mount { get; set; }

    private TextureProgress _loadProgress;
    private TextureRect _reticule;
    private bool _loadedTexture;
    private bool _aimedTexture;
    private GameplayUi _gameplayUi;

    public override void _Ready()
    {
        _loadProgress = GetNode<TextureProgress>("LoadProgress");
        _reticule = GetNode<TextureRect>("Reticule");
        _gameplayUi = this.FindParent<GameplayUi>();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(Mount.Turret != null)
        {
            _loadProgress.Value = (Mount.Turret.ReloadCounter / Mount.Turret.ReloadTime) * 100;

            _loadProgress.Visible = _loadProgress.Value < 100;

            if ((Mount.Turret.ReloadCounter >= Mount.Turret.ReloadTime && !_loadedTexture)
                || (Mount.Turret.ReloadCounter < Mount.Turret.ReloadTime && _loadedTexture)
                || Mount.Turret.IsAimed != _aimedTexture)
            {
                _aimedTexture = Mount.Turret.IsAimed;

                if(Mount.Turret.ReloadCounter >= Mount.Turret.ReloadTime)
                {
                    _loadedTexture = true;
                    _reticule.Texture = Mount.Turret.IsAimed ? AimedLoaded : UnaimedLoaded;
                }
                else
                {
                    _loadedTexture = false;
                    _reticule.Texture = Mount.Turret.IsAimed ? AimedUnloaded : UnaimedUnloaded;
                }
            }
        }
    }
}
