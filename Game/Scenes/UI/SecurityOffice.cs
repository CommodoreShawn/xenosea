using Godot;
using System;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.Infrastructure.Missions;

public class SecurityOffice : Control
{
    [Export]
    public PackedScene MissionOptionButtonPrefab;

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IMissionTracker _missionTracker;
    [Inject]
    private IWorldManager _worldManager;

    private IPatrolMission _activeMission;
    private VBoxContainer _missionButtonList;
    private Label _missionTitleLabel;
    private Label _missionRewardLabel;
    private Label _missionDepositLabel;
    private Label _missionDurationLabel;
    private Label _missionDescriptionLabel;
    private Button _acceptMissionButton;
    private Button _deliverMissionButton;
    private Control _missionInfoContents;
    private GameplayUi _gameplayUi;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _gameplayUi = this.FindParent<GameplayUi>();

        _missionButtonList = GetNode<VBoxContainer>("HBoxContainer/VBoxContainer/ScrollContainer/MissionButtonContainer");
        _missionTitleLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/MissionTitleLabel");
        _missionDescriptionLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/RulesLabel");
        _missionRewardLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer2/RewardLabel");
        _missionDepositLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer2/DepositLabel");
        _missionDurationLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/DurationLabel");
        _acceptMissionButton = GetNode<Button>("HBoxContainer/VBoxContainer2/HBoxContainer3/AcceptButton");
        _deliverMissionButton = GetNode<Button>("HBoxContainer/VBoxContainer2/HBoxContainer3/Deliver Button");
        _missionInfoContents = GetNode<Control>("HBoxContainer/VBoxContainer2");

        SetActiveMission(null);

        RefreshMissionList();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        foreach (var missionButton in _missionButtonList.GetChildren().OfType<MissionOptionButton>().ToArray())
        {
            missionButton.Pressed = missionButton.Mission == _activeMission;

            if(!_missionTracker.IsMissionAvailableAt(_playerData.PlayerSub.IsDockedTo, missionButton.Mission)
                && !_missionTracker.IsHeldByAgent(_playerData.PlayerSub, missionButton.Mission))
            {
                if(_activeMission == missionButton.Mission)
                {
                    SetActiveMission(_missionButtonList
                        .GetChildren()
                        .OfType<MissionOptionButton>()
                        .Select(x => x.Mission)
                        .Where(m => m != _activeMission)
                        .FirstOrDefault());
                }

                missionButton.QueueFree();
            }
        }

        if(_activeMission != null)
        {
            if (_missionTracker.IsHeldByAgent(_playerData.PlayerSub, _activeMission))
            {
                _missionDurationLabel.Text = $"Deliver By: {_activeMission.DueDate.Value.ToShortTimeString()} {_activeMission.DueDate.Value.ToShortDateString()}";
            }
            else
            {
                var deliverTime = _worldManager.CurrentDate + _activeMission.AllowedTime;
                _missionDurationLabel.Text = $"Deliver By: {deliverTime.Value.ToShortTimeString()} {deliverTime.Value.ToShortDateString()}";
            }
        }
        else
        {
            SetActiveMission(_missionButtonList.GetChildren().OfType<MissionOptionButton>().Select(x => x.Mission).FirstOrDefault());
        }
    }

    public void SetActiveMission(IMission mission)
    {
        if (mission != null && mission is IPatrolMission patrolMission)
        {
            _activeMission = patrolMission;

            _deliverMissionButton.Disabled = !_missionTracker.IsHeldByAgent(_playerData.PlayerSub, _activeMission)
                || !_activeMission.CanBeDelivered(_playerData.PlayerSub);

            _missionInfoContents.Visible = true;
            _missionTitleLabel.Text = patrolMission.Name;
            _missionDescriptionLabel.Text = patrolMission.Description;
            _missionRewardLabel.Text = $"Reward: {patrolMission.Reward.ToString("C0")}";
            if (patrolMission.Deposit > 0)
            {
                _missionDepositLabel.Text = $"Deposit: {patrolMission.Deposit.ToString("C0")}";
            }
            else
            {
                _missionDepositLabel.Text = string.Empty;
            }

            _acceptMissionButton.Disabled = ! _missionTracker.IsMissionAvailableAt(_playerData.PlayerSub.IsDockedTo, _activeMission);

            if (_playerData.Money < patrolMission.Deposit)
            {
                _missionDepositLabel.Modulate = Colors.Red;
                _acceptMissionButton.Disabled = true;
            }
            else
            {
                _missionDepositLabel.Modulate = Colors.White;
            }
        }
        else
        {
            _activeMission = null;
            _missionInfoContents.Visible = false;
        }
    }

    public void AcceptCurrentMission()
    {
        _playerData.Money -= _activeMission.Deposit;

        _missionTracker.PickupMission(
            _activeMission, 
            _playerData.PlayerSub.IsDockedTo, 
            _playerData.PlayerSub);

        if(_missionTracker.TrackedMission == null)
        {
            _missionTracker.TrackedMission = _activeMission;
        }

        _activeMission = null;
        RefreshMissionList();
    }

    private void RefreshMissionList()
    {
        _missionButtonList.ClearChildren();

        var availableMissions = _missionTracker
            .GetAvailableMissions(_playerData.PlayerSub.IsDockedTo)
            .OfType<IPatrolMission>()
            .OrderByDescending(x => x.Reward)
            .ToArray();

        var playerMissions = _missionTracker.GetCurrentMissions(_playerData.PlayerSub);

        foreach (var mission in playerMissions.Concat(availableMissions).OfType<IPatrolMission>())
        {
            if (_activeMission == null)
            {
                if(availableMissions.Any())
                {
                    if(availableMissions.Contains(mission))
                    {
                        SetActiveMission(mission);
                    }
                }
                else
                {
                    SetActiveMission(mission);
                }
            }

            var missionOptionButton = MissionOptionButtonPrefab.Instance() as MissionOptionButton;
            missionOptionButton.Initialize(mission);
            _missionButtonList.AddChild(missionOptionButton);
        }

        SetActiveMission(_activeMission);
    }

    public void DeliverCurrentMission()
    {
        _playerData.Money += _activeMission.Deposit + _activeMission.Reward;

        _missionTracker.DeliverMission(
            _activeMission, 
            _playerData.PlayerSub.IsDockedTo, 
            _playerData.PlayerSub);

        SetActiveMission(null);

        RefreshMissionList();
    }
}
