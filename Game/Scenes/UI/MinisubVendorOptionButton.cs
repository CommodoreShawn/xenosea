using Godot;
using Xenosea.Logic;

public class MinisubVendorOptionButton : Button
{
    public MinisubType MinisubType { get; set; }
    public MinisubHangar Hangar { get; set; }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (Hangar == null)
        {
            Text = MinisubType.Name;
            Icon = MinisubType.Icon;
        }
        else
        {
            Text = Hangar.MinisubType?.Name ?? "Empty";
            Icon = Hangar.MinisubType?.Icon;
        }
    }

    public void OnPressed()
    {
        if (Hangar == null)
        {
            this.FindParent<MinisubVendor>().SetActiveMinisubOption(MinisubType);
        }
        else
        {
            this.FindParent<MinisubVendor>().SetActiveHangar(Hangar);
        }
    }
}
