using Godot;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class StandingsPopup : Popup
{
    [Export]
    public PackedScene StandingsFactionOptionButtonPrefab;

    private VBoxContainer _factionButtonList;
    private Label _factionNameLabel;
    private Label _factionDescriptionLabel;
    private VBoxContainer _otherFactionStandingList;
    private Faction _activeFaction;

    [Inject]
    private IWorldManager _worldManager;
    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();

        _factionButtonList = GetNode<VBoxContainer>("PanelContainer/VBoxContainer/HBoxContainer2/ScrollContainer/FactionButtonList");
        _factionNameLabel = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/FactionNameLabel");
        _factionDescriptionLabel = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/FactionDescriptionLabel");
        _otherFactionStandingList = GetNode<VBoxContainer>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/ScrollContainer/OtherFactionStandingList");
    }

    public void InitAndShow()
    {
        _activeFaction = null;
        _factionButtonList.ClearChildren();
        _otherFactionStandingList.ClearChildren();

        var knownFactions = _playerData.PlayerSub.Faction
            .FactionRelations.Keys
            .Select(n => _worldManager.Factions.FirstOrDefault(f => f.Name == n))
            .Where(f => f != null)
            .OrderByDescending(f => _playerData.PlayerSub.Faction.FactionRelations[f.Name])
            .ToArray();

        foreach (var faction in knownFactions)
        {
            var button = StandingsFactionOptionButtonPrefab.Instance() as StandingsFactionOptionButton;
            button.Faction = faction;
            button.Relation = _playerData.PlayerSub.Faction.FactionRelations[faction.Name];
            _factionButtonList.AddChild(button);

            if (_activeFaction == null)
            {
                SetActiveFaction(faction);
            }
        }

        PopupCentered();
    }

    public void SetActiveFaction(Faction faction)
    {
        _activeFaction = faction;
        _factionNameLabel.Text = faction.Name;
        _factionDescriptionLabel.Text = faction.Description;
        _otherFactionStandingList.ClearChildren();

        var knownFactions = _playerData.PlayerSub.Faction
            .FactionRelations.Keys
            .Intersect(faction.FactionRelations.Keys)
            .Select(n => _worldManager.Factions.FirstOrDefault(f => f.Name == n))
            .Where(f => f != null)
            .OrderByDescending(f => faction.FactionRelations[f.Name])
            .ToArray();

        foreach(var otherFaction in knownFactions)
        {
            var relation = faction.FactionRelations[otherFaction.Name];
            var label = new Label
            {
                Text = $"{otherFaction.Name} {relation.ToString("F1")}"
            };

            if (relation >= 20)
            {
                label.Modulate = Colors.Green;
            }
            else if (relation <= -20)
            {
                label.Modulate = Colors.Red;
            }
            else
            {
                label.Modulate = Colors.White;
            }

            _otherFactionStandingList.AddChild(label);
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        foreach(var button in _factionButtonList.GetChildren().OfType<StandingsFactionOptionButton>())
        {
            button.Pressed = button.Faction == _activeFaction;
        }
    }

    public void Close()
    {
        _soundEffectPlayer.PlayUiClick();
        Hide();
    }
}
