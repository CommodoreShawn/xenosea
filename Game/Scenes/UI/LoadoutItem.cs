using Godot;
using Xenosea.Logic;

public class LoadoutItem : HBoxContainer
{
    private CommodityType _commodityType;

    public void SetContentFor(CommodityType commodityType, float quantity, bool forOwnSub)
    {
        _commodityType = commodityType;
        GetNode<Label>("Label").Text = $"{quantity.ToString("N0")} {commodityType.Name}";
        GetNode<TextureRect>("TextureRect").Texture = commodityType.Icon;
        HintTooltip = commodityType.Description;
        GetNode<TextureButton>("VBoxContainer/JettisonButton").Visible = forOwnSub;
    }

    public void SetContentFor(ArmorType armorType)
    {
        GetNode<Label>("Label").Text = armorType.Name;
        GetNode<TextureRect>("TextureRect").Texture = null;
        HintTooltip = armorType.Description;
        GetNode<TextureButton>("VBoxContainer/JettisonButton").Visible = false;
    }

    public void SetContentFor(BasicModuleType moduleType)
    {
        GetNode<Label>("Label").Text = moduleType.Name;
        GetNode<TextureRect>("TextureRect").Texture = null;
        HintTooltip = moduleType.Description;
        GetNode<TextureButton>("VBoxContainer/JettisonButton").Visible = false;
    }

    public void SetContentFor(MinisubType minisubType)
    {
        GetNode<Label>("Label").Text = minisubType.Name;
        GetNode<TextureRect>("TextureRect").Texture = minisubType.Icon;
        HintTooltip = minisubType.Description;
        GetNode<TextureButton>("VBoxContainer/JettisonButton").Visible = false;
    }

    public void SetContentFor(TorpedoType torpedoType)
    {
        GetNode<Label>("Label").Text = torpedoType.Name;
        GetNode<TextureRect>("TextureRect").Texture = torpedoType.Icon;
        HintTooltip = torpedoType.Description;
        GetNode<TextureButton>("VBoxContainer/JettisonButton").Visible = false;
    }

    public void OnJettison()
    {
        this.FindParent<GameplayUi>().PromptForJettison(_commodityType);
    }
}
