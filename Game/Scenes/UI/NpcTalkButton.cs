using Godot;
using Xenosea.Logic;

public class NpcTalkButton : Button
{
    public Npc Npc { get; internal set; }

    public override void _Ready()
    {
        if(Npc != null)
        {
            Icon = Npc.ImageSmall;
            Text = Npc.Name;
            HintTooltip = Npc.Name + "\n" + Npc.Title;
        }
    }

    public void StartConversation()
    {
        this.FindParent<GameplayUi>().StartConversation(Npc);
    }
}
