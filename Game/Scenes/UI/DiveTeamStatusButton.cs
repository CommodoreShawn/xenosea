using Godot;
using Xenosea.Logic;
using Xenosea.Scenes.Ships;

public class DiveTeamStatusButton : Button
{

    private DiveTeam _diveTeam;
    private Label _statusLabel;

    public DiveTeam DiveTeam
    {
        get => _diveTeam;
        set
        {
            _diveTeam = value;
            Text = _diveTeam.Name;
        }
    }

    public override void _Ready()
    {
        _statusLabel = GetNode<Label>("StatusLabel");
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(DiveTeam.DeployedDiver == null)
        {
            if(DiveTeam.DiverIsRecovering)
            {
                _statusLabel.Text = "Recovering";
                _statusLabel.Modulate = Colors.Yellow;
            }
            else
            {
                _statusLabel.Text = "Ready";
                _statusLabel.Modulate = Colors.Green;
            }
        }
        else if (DiveTeam.DeployedDiver.DiveTimeLow)
        {
            _statusLabel.Text = "Low Oxygen";
            _statusLabel.Modulate = Colors.Red;
        }
        else if (DiveTeam.DeployedDiver.IsReturning)
        {
            _statusLabel.Text = "Returning";
            _statusLabel.Modulate = Colors.Yellow;
        }
        else
        {
            _statusLabel.Text = "Working";
            _statusLabel.Modulate = Colors.Yellow;
        }
    }
}
