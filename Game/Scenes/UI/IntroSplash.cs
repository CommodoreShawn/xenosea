using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;

public class IntroSplash : Control
{
    private float _delay0 = 1;
    private float _delay1 = 3;
    private float _delay2 = 2;

    private float _timer;
    private int _state;
    private List<Label> _labels1;
    private List<Label> _labels2;
    private AudioStreamPlayer _audioStreamPlayer;
    public override void _Ready()
    {
        _audioStreamPlayer = GetNode<AudioStreamPlayer>("AudioStreamPlayer");

        _labels1 = new List<Label>
        {
            GetNode<Label>("VBoxContainer/Label0"),
            GetNode<Label>("VBoxContainer/Label1"),
            GetNode<Label>("VBoxContainer/Label2")
        };

        _labels2 = new List<Label>
        {
            GetNode<Label>("VBoxContainer/Label3")
        };

        foreach(var label in _labels1.Concat(_labels2))
        {
            label.Visible = false;
        }
    }

    public override void _Process(float delta)
    {
        _timer += delta;

        if(_timer >= _delay0 && _state == 0)
        {
            _state += 1;
            _timer = 0;
            _audioStreamPlayer.Play();

            foreach (var label in _labels1)
            {
                label.Visible = true;
            }
        }
        else if (_timer >= _delay1 && _state == 1)
        {
            _state += 1;
            _timer = 0;
            _audioStreamPlayer.Play();

            foreach (var label in _labels2)
            {
                label.Visible = true;
            }
        }
        else if (_timer >= _delay2 && _state == 2)
        {
            _state += 1;
            QueueFree();
        }
    }

    public void Skip()
    {
        this.FindParent<Gameplay>().SkipIntro();
        QueueFree();
    }
}
