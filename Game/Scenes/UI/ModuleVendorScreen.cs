using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class ModuleVendorScreen : Spatial
{
    private bool _refitting;
    private SubLoadout _currentLoadout;
    private SubLoadout _newLoadout;

    private Label _roomTitleLabel;
    private GridContainer _statsGrid;
    private Label _costLabel;
    private Label _costValueLabel;
    private Button _buyButton;

    private Button _hullOptionButtion;
    private Button _armorOptionButton;
    private Button _engineOptionButton;
    private Button _sonarOptionButton;
    private Button _arrayOptionButton;
    private Button _arrayClearButton;
    private VBoxContainer _turretModuleList;
    private VBoxContainer _launcherModuleList;
    private VBoxContainer _hangarModuleList;
    private VBoxContainer _otherModuleList;
    private VBoxContainer _hangarGroup;

    private Label _totalVolumeLabel;
    private Label _remainingVolumeLabel;
    private Label _errorLabel;
    private Label _warningLabel;
    private Label _budgetLabel;

    private ModulePickerPopup _modulePickerPopup;
    private ArmsDealerStationRoom _stationRoom;

    private Spatial _hullMount;

    private MapscreenCamera _camera;

    private float _buyPrice;

    private string _oldName;
    private string _newName;
    private bool _customName;

    private LineEdit _subNameEdit;
    private Button _subNameResetButton;

    private SubHullType[] _hullTypes;
    private ArmorType[] _armorTypes;
    private BasicModuleType[] _moduleTypes;

    [Export]
    public PackedScene ModuleVendorSlotRowPrefab;

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IMissionTracker _missionTracker;
    [Inject]
    private INotificationSystem _notificationSystem;
    [Inject]
    private IResourceLocator _resourceLocator;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _modulePickerPopup = GetNode<ModulePickerPopup>("ModulePickerPopup");
        _camera = GetNode<MapscreenCamera>("MapscreenCamera");

        _roomTitleLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/TitleLabel");
        _statsGrid = GetNode<GridContainer>("RightStack/PanelContainer/VBoxContainer/GridContainer");
        _costLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/CostGrid/CostLabel");
        _costValueLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/CostGrid/CostValLabel");
        _buyButton = GetNode<Button>("RightStack/PanelContainer/VBoxContainer/HBoxContainer3/BuyButton");

        _hullOptionButtion = GetNode<Button>("LeftStack/PanelContainer/VBoxContainer/HBoxContainer/HullOptionButton");
        _armorOptionButton = GetNode<Button>("LeftStack/PanelContainer/VBoxContainer/ScrollContainer/VBoxContainer/GridContainer/ArmorOptionButton");
        _engineOptionButton = GetNode<Button>("LeftStack/PanelContainer/VBoxContainer/ScrollContainer/VBoxContainer/GridContainer/EngineOptionButton");
        _sonarOptionButton = GetNode<Button>("LeftStack/PanelContainer/VBoxContainer/ScrollContainer/VBoxContainer/GridContainer/SonarOptionButton");
        _arrayOptionButton = GetNode<Button>("LeftStack/PanelContainer/VBoxContainer/ScrollContainer/VBoxContainer/GridContainer2/TowedArrayOptionButton");
        _arrayClearButton = GetNode<Button>("LeftStack/PanelContainer/VBoxContainer/ScrollContainer/VBoxContainer/GridContainer2/ClearTowedArrayButton");
        _turretModuleList = GetNode<VBoxContainer>("LeftStack/PanelContainer/VBoxContainer/ScrollContainer/VBoxContainer/TurretGroup/List");
        _launcherModuleList = GetNode<VBoxContainer>("LeftStack/PanelContainer/VBoxContainer/ScrollContainer/VBoxContainer/LauncherGroup/List");
        _hangarModuleList = GetNode<VBoxContainer>("LeftStack/PanelContainer/VBoxContainer/ScrollContainer/VBoxContainer/HangarGroup/List");
        _otherModuleList = GetNode<VBoxContainer>("LeftStack/PanelContainer/VBoxContainer/ScrollContainer/VBoxContainer/OtherGroup/List");
        _hangarGroup = GetNode<VBoxContainer>("LeftStack/PanelContainer/VBoxContainer/ScrollContainer/VBoxContainer/HangarGroup");

        _totalVolumeLabel = GetNode<Label>("LeftStack/PanelContainer/VBoxContainer/HBoxContainer2/TotalVolume");
        _remainingVolumeLabel = GetNode<Label>("LeftStack/PanelContainer/VBoxContainer/HBoxContainer2/RemainingVolume");
        _errorLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/ErrorLabel");
        _warningLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/WarningLabel");
        _budgetLabel = GetNode<Label>("RightStack/PanelContainer/VBoxContainer/Balance");

        _hullMount = GetNode<Spatial>("HullMount");
        _hullMount.ClearChildren();

        _subNameEdit = GetNode<LineEdit>("LeftStack/PanelContainer/VBoxContainer/HBoxContainer3/SubNameEdit");
        _subNameResetButton = GetNode<Button>("LeftStack/PanelContainer/VBoxContainer/HBoxContainer3/ResetNameButton");

        GetTree().Root.GetNode<GameplayUi>("Gameplay/UI").HideForOtherUi = true;
        _camera = GetNode<MapscreenCamera>("MapscreenCamera");
        _camera.Current = true;
        _camera.CameraDistance = 50;
        _camera.Sensitivity = 0.3f;
        LookAtModulePosition(new Vector3(0, 1, 1));
    }

    public void SetupForRefit(ArmsDealerStationRoom room, Submarine currentSub)
    {
        _roomTitleLabel.Text = room.FriendlyName.ToUpper();
        _refitting = true;
        _currentLoadout = currentSub.GetLoadout();
        _newLoadout = currentSub.GetLoadout();
        _hullOptionButtion.Disabled = true;

        _hullOptionButtion.Disabled = true;

        _oldName = currentSub.FriendlyName;
        _newName = currentSub.FriendlyName;
        _subNameEdit.Text = _oldName;
        _subNameResetButton.Visible = true;

        BaseSetup(room);
    }

    public void SetupForPurchase(ArmsDealerStationRoom room, Submarine currentSub)
    {
        _roomTitleLabel.Text = room.FriendlyName.ToUpper();
        _refitting = false;
        _currentLoadout = currentSub.GetLoadout();
        _newLoadout = new SubLoadout
        {
            TurretModules= new TurretModuleType[0],
            LauncherModules = new TorpedoLauncherModuleType[0],
            HangarModules = new MinisubHangarModuleType[0],
            OtherModules = new BasicModuleType[0],
            DiveTeams = new DiveTeamType[0],
            TorpedoesPerTube = new TorpedoType[0],
            Minisubs = new MinisubType[0]
        };

        _hullOptionButtion.Disabled = false;

        _oldName = string.Empty;
        _newName = string.Empty;
        _subNameEdit.Text = _oldName;
        _subNameResetButton.Visible = false;

        BaseSetup(room);
    }

    private void BaseSetup(ArmsDealerStationRoom room)
    {
        _stationRoom = room;

        var station = (_playerData.PlayerSub.IsDockedTo as WorldStation).BackingStation;
        var industrialSupport = station.EconomicProcesses.Sum(x => x.IndustrialSupport);
        var militarySupport = station.EconomicProcesses.Sum(x => x.MilitarySupport);

        _hullTypes = _resourceLocator.AllSubHullTypes
            .Where(x => industrialSupport >= x.IndustrialNeed)
            .Where(x => militarySupport >= x.MilitaryNeed)
            .Where(x => x.Operators.Contains(station.Faction))
            .ToArray();

        _armorTypes = _resourceLocator.AllArmorTypes
            .Where(x => industrialSupport >= x.IndustrialNeed)
            .Where(x => militarySupport >= x.MilitaryNeed)
            .Where(x => x.Operators.Contains(station.Faction))
            .ToArray();

        _moduleTypes = _resourceLocator.AllModuleTypes
            .Where(x => industrialSupport >= x.IndustrialNeed)
            .Where(x => militarySupport >= x.MilitaryNeed)
            .Where(x => x.Operators.Contains(station.Faction))
            .ToArray();

        RebuildModuleSlots();
        UpdateStats();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _newName = _subNameEdit.Text;

        if (_oldName != string.Empty)
        {
            if(_oldName != _newName && !_customName)
            {
                _customName = true;
                UpdateStats();
            }
            else if(_oldName == _newName && _customName)
            {
                _customName = false;
                UpdateStats();
            }
        }

        _subNameResetButton.Disabled = !_customName;
    }

    public void ChangeHullOption()
    {
        _modulePickerPopup.ShowFor(
            "Hull Type",
            _newLoadout.HullType,
            _hullTypes,
            (pickedOption) =>
            {
                _newLoadout.HullType = (SubHullType)pickedOption;
                _newLoadout.TurretModules = new TurretModuleType[_newLoadout.HullType.Turrets];
                _newLoadout.LauncherModules = new TorpedoLauncherModuleType[_newLoadout.HullType.TorpedoTubes];
                _newLoadout.HangarModules = new MinisubHangarModuleType[_newLoadout.HullType.MinisubHangars];
                _newLoadout.OtherModules = new BasicModuleType[0];

                RebuildModuleSlots();
                UpdateStats();
            },
            null,
            null);
    }

    public void ChangeArmorOption()
    {
        _modulePickerPopup.ShowFor(
            "Armor Type",
            _newLoadout.ArmorType,
            _armorTypes,
            (pickedOption) =>
            {
                _newLoadout.ArmorType = (ArmorType)pickedOption;
                UpdateStats();
            },
            _newLoadout.HullType,
            null);
    }

    public void ChangeEngineOption()
    {
        _modulePickerPopup.ShowFor(
            "Engine",
            _newLoadout.EngineModule,
            _moduleTypes.OfType<EngineModuleType>().ToArray(),
            (pickedOption) =>
            {
                _newLoadout.EngineModule = (EngineModuleType)pickedOption;
                UpdateStats();
            },
            _newLoadout.HullType,
            null);
    }

    public void ChangeSonarOption()
    {
        _modulePickerPopup.ShowFor(
            "Sonar System",
            _newLoadout.SonarModule,
            _moduleTypes.OfType<SonarModuleType>().ToArray(),
            (pickedOption) =>
            {
                _newLoadout.SonarModule = (SonarModuleType)pickedOption;
                UpdateStats();
            },
            _newLoadout.HullType,
            null);
    }

    public void ChangeTowedArrayOption()
    {
        _modulePickerPopup.ShowFor(
            "Towed Sonar Array",
            _newLoadout.TowedArrayModule,
            _moduleTypes.OfType<TowedArrayModuleType>().ToArray(),
            (pickedOption) =>
            {
                _newLoadout.TowedArrayModule = (TowedArrayModuleType)pickedOption;
                UpdateStats();
            },
            _newLoadout.HullType,
            null);
    }

    public void ClearTowedArray()
    {
        _newLoadout.TowedArrayModule = null;
        UpdateStats();
    }

    public void LookAtModulePosition(Vector3 position)
    {
        _camera.SetLookAt(position);
    }

    private void UpdateStats()
    {
        _hullOptionButtion.Text = _newLoadout.HullType?.Name ?? string.Empty;
        _armorOptionButton.Text = _newLoadout.ArmorType?.Name ?? string.Empty;
        _armorOptionButton.Disabled = _newLoadout.HullType == null;
        _engineOptionButton.Text = _newLoadout.EngineModule?.Name ?? string.Empty;
        _engineOptionButton.Disabled = _newLoadout.HullType == null;
        _sonarOptionButton.Text = _newLoadout.SonarModule?.Name ?? string.Empty;
        _sonarOptionButton.Disabled = _newLoadout.HullType == null;
        _arrayOptionButton.Text = _newLoadout.TowedArrayModule?.Name ?? string.Empty;
        _arrayOptionButton.Disabled = _newLoadout.HullType == null;
        _arrayClearButton.Disabled = _newLoadout.TowedArrayModule == null;

        var currentValue = 0f;
        var newValue = 0f;
        var statsLabels = _statsGrid
            .GetChildren()
            .OfType<Label>()
            .ToArray();

        var totalVolume = _newLoadout.HullType?.Volume ?? 0;
        var usedVolume = (_newLoadout.HullType?.HullSurfaceArea ?? 0) * (_newLoadout.ArmorType?.VolumePerSurfaceArea ?? 0)
            + _newLoadout.GetAllModules().Sum(x => x.Volume);

        var newCargoLimit = (totalVolume - usedVolume) * Constants.VOLUME_TO_CARGO;

        for (var i = 0; i < statsLabels.Length; i+= 3)
        {
            switch(i)
            {
                case 0: // cargo
                    currentValue = (_currentLoadout.HullType.Volume
                        - _currentLoadout.HullType.HullSurfaceArea * _currentLoadout.ArmorType.VolumePerSurfaceArea
                        - _currentLoadout.GetAllModules().Sum(x => x.Volume))
                    * Constants.VOLUME_TO_CARGO;

                    newValue = newCargoLimit;
                    break;
                case 3: // top speed
                    currentValue = _currentLoadout.EngineModule.MaximumThrust / (_currentLoadout.HullType.FrontDragFactor * _currentLoadout.ArmorType.DragFactor);

                    if (_newLoadout.EngineModule != null)
                    {
                        newValue = _newLoadout.EngineModule.MaximumThrust / (_newLoadout.HullType.FrontDragFactor * _newLoadout.ArmorType.DragFactor);
                    }
                    else
                    {
                        newValue = 0;
                    }
                    break;
                case 6: // health
                    currentValue = _currentLoadout.HullType.Health;
                    newValue = _newLoadout?.HullType?.Health ?? 0;
                    break;
                case 9: // armor
                    currentValue = _currentLoadout.ArmorType.Armor;
                    newValue = _newLoadout?.ArmorType?.Armor ?? 0;
                    break;
                case 12: // max_noise
                    currentValue = _currentLoadout.EngineModule.MaximumNoise + _currentLoadout.HullType.GetBaseNoise();
                    if (_newLoadout.EngineModule != null)
                    {
                        newValue = _newLoadout.EngineModule.MaximumNoise + _newLoadout.HullType.GetBaseNoise();
                    }
                    else
                    {
                        newValue = 0;
                    }
                    break;
                case 15: // detected_at
                    var environmentNoise = 5;
                    var currentNoise = _currentLoadout.EngineModule.MaximumNoise + _currentLoadout.HullType.GetBaseNoise();
                    currentValue = Mathf.Sqrt(currentNoise / (4 * Mathf.Pi * environmentNoise)) / Constants.SONAR_ENV_PROPAGATION;

                    if (_newLoadout.EngineModule != null)
                    {
                        var newNoise = _newLoadout.EngineModule.MaximumNoise + _newLoadout.HullType.GetBaseNoise();
                        newValue = Mathf.Sqrt(newNoise / (4 * Mathf.Pi * environmentNoise)) / Constants.SONAR_ENV_PROPAGATION;
                    }
                    else
                    {
                        newValue = 0;
                    }
                    break;
                case 18: // turrets
                    currentValue = _currentLoadout.TurretModules
                        .Count(x => x != null);
                    newValue = _newLoadout.TurretModules
                        .Count(x => x != null);
                    break;
                case 21: // torpedoes
                    currentValue = _currentLoadout.LauncherModules
                        .Sum(x => x?.MagazineCapacity ?? 0);
                    newValue = _newLoadout.LauncherModules
                        .Sum(x => x?.MagazineCapacity ?? 0);
                    break;
                case 24: // minisubs
                    currentValue = _currentLoadout.HangarModules
                        .Count(x => x != null);
                    newValue = _newLoadout.HangarModules
                        .Count(x => x != null);
                    break;
                case 27: // dive teams
                    currentValue = _currentLoadout.OtherModules
                        .OfType<DiveChamberModuleType>()
                        .Sum(x => x.DiverCapacity);
                    newValue = _newLoadout.OtherModules
                        .OfType<DiveChamberModuleType>()
                        .Sum(x => x.DiverCapacity);
                    break;
            }

            if (i == 15) // detect distance
            {
                statsLabels[i + 1].Text = Util.DistanceToDisplay(newValue, false);
                
                if (newValue - currentValue != 0)
                {
                    statsLabels[i + 2].Text = Util.DistanceToDisplay(newValue - currentValue, true);
                }
                else
                {
                    statsLabels[i + 2].Text = string.Empty;
                }

                statsLabels[i + 2].Modulate = ModulateDiff(currentValue - newValue);
            }
            else
            {
                statsLabels[i + 1].Text = newValue.ToString("N0");
                statsLabels[i + 2].Text = FormatDiff((newValue - currentValue).ToString("N0"));

                if (i == 12)// max noise
                {
                    statsLabels[i + 2].Modulate = ModulateDiff(currentValue - newValue);
                }
                else
                {
                    statsLabels[i + 2].Modulate = ModulateDiff(newValue - currentValue);
                }
            }
        }

        _totalVolumeLabel.Text = $"Volume: {totalVolume.ToString("N0")}";
        _remainingVolumeLabel.Text = $"Empty: {(totalVolume - usedVolume).ToString("N0")}";

        _buyPrice = 0f;

        if(_refitting)
        {
            _costLabel.Text = "Refit Cost:";

            if(_newLoadout.ArmorType != _currentLoadout.ArmorType)
            {
                _buyPrice += (_newLoadout.ArmorType?.ValuePerSurfaceArea ?? 0) * (_newLoadout.HullType?.HullSurfaceArea ?? 0);
                _buyPrice -= _currentLoadout.ArmorType.ValuePerSurfaceArea * _currentLoadout.HullType.HullSurfaceArea * Constants.TRADE_IN_VALUE_MOD;
            }

            var newModules = _newLoadout.GetAllModules();
            var currentModules = _currentLoadout.GetAllModules();

            var distinctModuleTypes = currentModules.Concat(newModules).Distinct();

            foreach (var moduleType in distinctModuleTypes)
            {
                var newCount = newModules
                    .Where(x => x == moduleType)
                    .Count();
                var currentCount = currentModules
                    .Where(x => x == moduleType)
                    .Count();

                var delta = newCount - currentCount;

                if(delta > 0)
                {
                    _buyPrice += delta * moduleType.Value;
                }
                else if (delta < 0)
                {
                    _buyPrice += delta * moduleType.Value * Constants.TRADE_IN_VALUE_MOD;
                }
            }
        }
        else
        {
            _costLabel.Text = "Buy Cost:";

            var newCost = (_newLoadout.HullType?.Value ?? 0)
                + (_newLoadout.HullType?.HullSurfaceArea  ?? 0 * _newLoadout.ArmorType?.ValuePerSurfaceArea ?? 0)
                + _newLoadout.GetAllModules().Sum(x => x.Value);

            var tradeInCost = _currentLoadout.HullType.Value
                + _currentLoadout.HullType.HullSurfaceArea * _currentLoadout.ArmorType.ValuePerSurfaceArea
                + _currentLoadout.GetAllModules().Sum(x => x.Value);

            tradeInCost = tradeInCost * Constants.TRADE_IN_VALUE_MOD;

            _buyPrice = newCost - tradeInCost;
        }

        if(_oldName != string.Empty && _customName)
        {
            _buyPrice += 10000;
        }

        _costValueLabel.Text = _buyPrice.ToString("C0");
        
        var canBuy = true;
        var errors = new List<string>();
        var warnings = new List<string>();

        _errorLabel.Text = string.Empty;
        if(_newLoadout.HullType == null)
        {
            canBuy = false;
            errors.Add("Hull type required");
        }
        if (_newLoadout.ArmorType == null)
        {
            canBuy = false;
            errors.Add("Armor type required");
        }
        if (_newLoadout.EngineModule == null)
        {
            canBuy = false;
            errors.Add("Engine required");
        }
        if (_newLoadout.SonarModule == null)
        {
            canBuy = false;
            errors.Add("Sonar required");
        }
        if (totalVolume < usedVolume)
        {
            canBuy = false;
            errors.Add("Not enough internal volume");
        }
        if (_playerData.Money < _buyPrice)
        {
            canBuy = false;
            errors.Add("Not enough money");
        }
        if(newCargoLimit < _playerData.PlayerSub.CommodityStorage.TotalVolume)
        {
            canBuy = false;
            errors.Add("Not enough room to transfer cargo.");
        }
        if(string.IsNullOrWhiteSpace(_newName))
        {
            canBuy = false;
            errors.Add("Name required");
        }

        if (_newLoadout.HangarModules.Where(h => h != null).Count() < _playerData.PlayerSub.MinisubHangars.Where(h=>h?.MinisubType != null).Count())
        {
            warnings.Add("Excess minisubs may be sold.");
        }
        if (_newLoadout.LauncherModules.Where(h => h != null).Sum(x => x.MagazineCapacity) < _playerData.PlayerSub.TorpedoTubes.Sum(t => t.TorpedoesAvailable.Count))
        {
            warnings.Add("Excess torpedoes will be sold.");
        }

        if (_newLoadout.OtherModules.OfType<DiveChamberModuleType>().Sum(x => x.DiverCapacity) < _playerData.PlayerSub.DiveTeams.Count())
        {
            warnings.Add("Excess dive teams will be sold.");
        }

        if (_oldName != string.Empty && _customName)
        {
            warnings.Add("Name registration will be updated.");
        }

        if (errors.Any())
        {
            _errorLabel.Text = string.Join("\n", errors);
        }
        else
        {
            _errorLabel.Text = string.Empty;
        }
        
        if (warnings.Any())
        {
            _warningLabel.Text = string.Join("\n", warnings);
        }
        else
        {
            _warningLabel.Text = string.Empty;
        }

        var turretMounts = _hullMount
            .GetChildren()
            .OfType<TurretMount>()
            .ToArray();

        for(var i = 0; i < turretMounts.Length; i++)
        {
            var mount = turretMounts[i];
            mount.ClearChildren();

            if(_newLoadout.TurretModules.Length > i
                && _newLoadout.TurretModules[i] != null)
            {
                var turret = _newLoadout.TurretModules[i].Prefab.Instance() as Node;
                mount.AddChild(turret);

                Util.TreeTraverse(
                    turret,
                    x =>
                    {
                        if (x is VisualInstance vi)
                        {
                            vi.Layers = _camera.CullMask;
                        }
                    });
            }
        }

        _budgetLabel.Text = _playerData.Money.ToString("C0");
        _warningLabel.Visible = _warningLabel.Text.Count() > 0;
        _errorLabel.Visible = _errorLabel.Text.Count() > 0;

        _buyButton.Disabled = !canBuy;
    }

    private string FormatDiff(string baseValue)
    {
        if(baseValue == "0")
        {
            return string.Empty;
        }

        if(!baseValue.StartsWith("-")
            && !baseValue.StartsWith("0"))
        {
            return "+ " + baseValue;
        }

        return baseValue;
    }

    private Color ModulateDiff(float diff)
    {
        if(diff > 0)
        {
            return Colors.Green;
        }
        else if (diff < 0)
        {
            return Colors.Red;
        }
        else
        {
            return Colors.White;
        }
    }

    private void RebuildModuleSlots()
    {
        _turretModuleList.ClearChildren();
        _launcherModuleList.ClearChildren();
        _hangarModuleList.ClearChildren();
        _otherModuleList.ClearChildren();
        _hullMount.ClearChildren();

        _newLoadout.OtherModules = _newLoadout.OtherModules
            .Where(x => x != null)
            .ToArray();

        if (_newLoadout.HullType != null)
        {
            var submarineHull = _newLoadout.HullType.SubPrefab.Instance();
            var turretMounts = new List<TurretMount>();
            var torpedoTubes = new List<TorpedoTube>();
            var minisubHangars = new List<MinisubHangar>();

            foreach (var child in submarineHull.GetChildren().OfType<Node>())
            {
                if(child is VisualInstance visualInstance)
                {
                    visualInstance.Layers = _camera.CullMask;
                }

                if (child is TurretMount turretMount)
                {
                    turretMounts.Add(turretMount);
                }
                else if (child is TorpedoTube torpedoTube)
                {
                    torpedoTubes.Add(torpedoTube);
                }
                else if (child is MinisubHangar minisubHangar)
                {
                    minisubHangars.Add(minisubHangar);
                }

                submarineHull.RemoveChild(child);
                _hullMount.AddChild(child);
            }
            submarineHull.QueueFree();

            for(var i = 0; i < turretMounts.Count && i < _newLoadout.TurretModules.Length; i++)
            {
                var index = i;
                var row = ModuleVendorSlotRowPrefab.Instance() as ModuleVendorModuleSlotRow;
                row.Initialize(
                    "TURRET",
                    index,
                    turretMounts[index].GlobalTransform.origin,
                    _newLoadout.TurretModules[index],
                    _moduleTypes.OfType<TurretModuleType>().ToArray(),
                    newModule =>
                        {
                            _newLoadout.TurretModules[index] = newModule as TurretModuleType;
                            UpdateStats();
                        });
                _turretModuleList.AddChild(row);
            }

            for (var i = 0; i < torpedoTubes.Count && i < _newLoadout.LauncherModules.Length; i++)
            {
                var index = i;
                var row = ModuleVendorSlotRowPrefab.Instance() as ModuleVendorModuleSlotRow;
                row.Initialize(
                    "LAUNCHER",
                    index,
                    torpedoTubes[index].GlobalTransform.origin,
                    _newLoadout.LauncherModules[index],
                    _moduleTypes.OfType<TorpedoLauncherModuleType>().ToArray(),
                    newModule =>
                    {
                        _newLoadout.LauncherModules[index] = newModule as TorpedoLauncherModuleType;
                        UpdateStats();
                    });
                _launcherModuleList.AddChild(row);
            }

            _hangarGroup.Visible = minisubHangars.Count > 0;
            for (var i = 0; i < minisubHangars.Count && i < _newLoadout.HangarModules.Length; i++)
            {
                var index = i;
                var row = ModuleVendorSlotRowPrefab.Instance() as ModuleVendorModuleSlotRow;
                row.Initialize(
                    "HANGAR",
                    index,
                    minisubHangars[index].GlobalTransform.origin,
                    _newLoadout.HangarModules[index],
                    _moduleTypes.OfType<MinisubHangarModuleType>().ToArray(),
                    newModule =>
                    {
                        _newLoadout.HangarModules[index] = newModule as MinisubHangarModuleType;
                        UpdateStats();
                    });
                _hangarModuleList.AddChild(row);
            }

            var otherModules = _moduleTypes
                .Where(x => !(
                    x is EngineModuleType
                    || x is SonarModuleType
                    || x is TowedArrayModuleType
                    || x is TurretModuleType
                    || x is TorpedoLauncherModuleType
                    || x is MinisubHangarModuleType))
                .ToArray();

            for (var i = 0; i < _newLoadout.OtherModules.Length; i++)
            {
                var index = i;
                var row = ModuleVendorSlotRowPrefab.Instance() as ModuleVendorModuleSlotRow;
                row.Initialize(
                    "MODULE",
                    index,
                    null,
                    _newLoadout.OtherModules[index],
                    otherModules,
                    newModule =>
                    {
                        _newLoadout.OtherModules[index] = newModule as BasicModuleType;
                        
                        if (newModule == null)
                        {
                            RebuildModuleSlots();
                        }
                        UpdateStats();
                    });
                _otherModuleList.AddChild(row);
            }
        }
    }

    public void AddOtherModule()
    {
        var otherModules = _moduleTypes
            .Where(x => !(
                x is EngineModuleType
                || x is SonarModuleType
                || x is TowedArrayModuleType
                || x is TurretModuleType
                || x is TorpedoLauncherModuleType
                || x is MinisubHangarModuleType))
            .ToArray();

        var index = _newLoadout.OtherModules.Length;

        _newLoadout.OtherModules = _newLoadout.OtherModules
            .Concat(new BasicModuleType[] { null })
            .ToArray();

        var row = ModuleVendorSlotRowPrefab.Instance() as ModuleVendorModuleSlotRow;
        row.Initialize(
            "MODULE",
            index,
            null,
            _newLoadout.OtherModules[index],
            otherModules,
            newModule =>
            {
                _newLoadout.OtherModules[index] = newModule as BasicModuleType;

                RebuildModuleSlots();
                UpdateStats();
            });

        _otherModuleList.AddChild(row);
        row.PickModule(() =>
            {

                _newLoadout.OtherModules = _newLoadout.OtherModules
                    .Where(m => m != null)
                    .ToArray();
                _otherModuleList.RemoveChild(row);
                row.QueueFree();
            });

    }

    public void BuyLoadout()
    {
        _playerData.Money -= _buyPrice;

        var orphanTorpedos = _playerData.PlayerSub.TorpedoTubes
            .SelectMany(x => x.TorpedoesAvailable)
            .ToList();
        foreach(var torpedoTube in _playerData.PlayerSub.TorpedoTubes)
        {
            torpedoTube.TorpedoesAvailable.Clear();
        }
        var minisubs = _playerData.PlayerSub.MinisubHangars
            .Select(x => x.MinisubType)
            .ToArray();
        var divers = _playerData.PlayerSub.DiveTeams
            .Select(x => x.DiveTeamType)
            .ToArray();
        var cargo = _playerData.PlayerSub.CommodityStorage;

        if (!_refitting) // Switch submarine hulls
        {
            var oldSub = _playerData.PlayerSub;
            var newSub = _newLoadout.HullType.SubPrefab.Instance() as Submarine;
            _playerData.PlayerSub.GetParent().AddChild(newSub);
            newSub.IsPlayerSub = true;
            _playerData.PlayerSub = newSub;
            oldSub.RemoveChild(_playerData.PlayerController);
            newSub.AddChild(_playerData.PlayerController);
            _playerData.PlayerController.Submarine = newSub;
            newSub.Faction = oldSub.Faction;
            newSub.GlobalTransform = oldSub.GlobalTransform;
            newSub.IsDockedTo = oldSub.IsDockedTo;
            _playerData.PlayerController.DockingSequence.Submarine = newSub;

            _missionTracker.TransferMissions(oldSub, newSub);

            foreach(var diveTeam in oldSub.DiveTeams)
            {
                newSub.DiveTeams.Add(new Xenosea.Scenes.Ships.DiveTeam(newSub, diveTeam.DiveTeamType, _notificationSystem));
            }

            oldSub.QueueFree();
        }

        _playerData.PlayerSub.BuildModules(_newLoadout);

        var orphanMinisubs = new List<MinisubType>();
        for (var i = 0; i < Mathf.Max(_playerData.PlayerSub.MinisubHangars.Count, minisubs.Length); i++)
        {
            if(i < _playerData.PlayerSub.MinisubHangars.Count && i < minisubs.Length)
            {
                if (_playerData.PlayerSub.MinisubHangars[i].CanHoldMinisub)
                {
                    _playerData.PlayerSub.MinisubHangars[i].MinisubType = minisubs[i];
                }
                else if (minisubs[i] != null)
                {
                    orphanMinisubs.Add(minisubs[i]);
                }
            }
            else if (i < minisubs.Length)
            {
                orphanMinisubs.Add(minisubs[i]);
            }
        }
        foreach(var hangar in _playerData.PlayerSub.MinisubHangars)
        {
            if(hangar.MinisubType == null && orphanMinisubs.Any())
            {
                hangar.MinisubType = orphanMinisubs[0];
                orphanMinisubs.RemoveAt(0);
            }
        }
        foreach(var minisub in orphanMinisubs)
        {
            _playerData.Money += minisub.Value;
        }

        foreach(var torpedo in orphanTorpedos)
        {
            var torpedoTube = _playerData.PlayerSub.TorpedoTubes
                .Where(t => t.TorpedoesAvailable.Count < t.MagazineCapacity)
                .Where(t => torpedo.SizeCategory <= t.MaximumTorpedoSize)
                .FirstOrDefault();

            if(torpedoTube != null)
            {
                torpedoTube.TorpedoesAvailable.Add(torpedo);
            }
            else
            {
                _playerData.Money += torpedo.Value;
            }
        }
        
        while(_playerData.PlayerSub.DiveTeams.Count > _playerData.PlayerSub.MaxDiveTeams)
        {
            var toSell = _playerData.PlayerSub.DiveTeams.Last();
            _playerData.Money += toSell.DiveTeamType.Value;
            _playerData.PlayerSub.DiveTeams.Remove(toSell);
        }

        foreach (var commodity in cargo.CommoditiesPresent)
        {
            _playerData.PlayerSub.CommodityStorage.Add(commodity, cargo.GetQty(commodity));
        }

        _playerData.PlayerSub.FriendlyName = _newName;

        Close();
    }

    public void Close()
    {
        GetTree().Root.GetNode<GameplayUi>("Gameplay/UI").HideForOtherUi = false;
        GetTree().Root.GetNode<Camera>("Gameplay/Camera").Current = true;
        QueueFree();
    }

    public void ResetSubName()
    {
        _subNameEdit.Text = _oldName;
    }
}
