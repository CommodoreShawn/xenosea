using Godot;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Scenes.Ships;

public class DiverVendor : VBoxContainer
{
    [Export]
    public PackedScene DiverVendorOptionButtonPrefab;

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private INotificationSystem _notificationSystem;
    [Inject]
    private IResourceLocator _resourceLocator;

    private VBoxContainer _vendorOptionList;
    private VBoxContainer _currentDiversList;
    private Label _diveTeamCapacityLabel;
    private Label _activeDiveTeamTypeLabel;
    private Label _costLabel;
    private Label _descriptionLabel;
    private Button _buyButton;
    private Button _sellButton;

    private GameplayUi _gameplayUi;
    private DiveTeamType _activeDiveTeamType;
    private bool _isForVendor;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _gameplayUi = this.FindParent<GameplayUi>();

        _vendorOptionList = GetNode<VBoxContainer>("HBoxContainer/VendorVBoxContainer/ScrollContainer/DiverOptionContainer");
        _currentDiversList = GetNode<VBoxContainer>("HBoxContainer/CurrentVBoxContainer/ScrollContainer/DiverOptionContainer");
        _diveTeamCapacityLabel = GetNode<Label>("HBoxContainer/CurrentVBoxContainer/DiveTeamCapacityLabel");
        _activeDiveTeamTypeLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/PickedDiveTeamTypeLabel");
        _costLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/CostLabel");
        _descriptionLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/DescriptionLabel");
        _buyButton = GetNode<Button>("HBoxContainer/VBoxContainer2/HBoxContainer3/HireButton");
        _sellButton = GetNode<Button>("HBoxContainer/VBoxContainer2/HBoxContainer3/DismissButton");

        _vendorOptionList.ClearChildren();


        RefreshCurrentDiverList();
    }

    private void RefreshCurrentDiverList()
    {
        _currentDiversList.ClearChildren();

        foreach (var diveTeam in _playerData.PlayerSub.DiveTeams)
        {
            var button = DiverVendorOptionButtonPrefab.Instance() as DiverVendorOptionButton;
            button.DiveTeamType = diveTeam.DiveTeamType;
            button.IsForVendor = false;
            _currentDiversList.AddChild(button);
        }
    }

    public void InitializeFor(StationRoom stationRoom)
    {
        _vendorOptionList.ClearChildren();

        var setDefault = false;

        var station = (_playerData.PlayerSub.IsDockedTo as WorldStation).BackingStation;
        var industrialSupport = station.EconomicProcesses.Sum(x => x.IndustrialSupport);
        var militarySupport = station.EconomicProcesses.Sum(x => x.MilitarySupport);

        var diveTeamTypes = _resourceLocator.AllDiveTeamTypes
            .Where(x => industrialSupport >= x.IndustrialNeed)
            .Where(x => militarySupport >= x.MilitaryNeed)
            .Where(x => x.Operators.Contains(station.Faction))
            .ToArray();

        if (stationRoom is BarStationRoom barStationRoom)
        {
            foreach (var diveTeamType in diveTeamTypes)
            {
                var button = DiverVendorOptionButtonPrefab.Instance() as DiverVendorOptionButton;
                button.DiveTeamType = diveTeamType;
                button.IsForVendor = true;
                _vendorOptionList.AddChild(button);

                if (!setDefault)
                {
                    setDefault = true;
                    SetActiveDiveTeamType(diveTeamType, true);
                }
            }
        }
    }

    public void SetActiveDiveTeamType(DiveTeamType diveTeamType, bool isForVendor)
    {
        if (diveTeamType != null)
        {
            _activeDiveTeamType = diveTeamType;
            _isForVendor = isForVendor;

            _activeDiveTeamTypeLabel.Text = diveTeamType.Name;
            _costLabel.Text = $"Cost: {diveTeamType.Value.ToString("C0")}";
            _descriptionLabel.Text = diveTeamType.Description;

            if (_playerData.Money >= diveTeamType.Value)
            {
                _costLabel.Modulate = Colors.White;
            }
            else
            {
                _costLabel.Modulate = Colors.Red;
            }

            if (_playerData.PlayerSub.DiveTeams.Count() < _playerData.PlayerSub.MaximumDiveTeams)
            {
                _diveTeamCapacityLabel.Modulate = Colors.White;
            }
            else
            {
                _diveTeamCapacityLabel.Modulate = Colors.Red;
            }

            _buyButton.Disabled = !isForVendor
                || _playerData.Money < diveTeamType.Value
                || _playerData.PlayerSub.DiveTeams.Count() >= _playerData.PlayerSub.MaximumDiveTeams;

            _sellButton.Disabled = isForVendor;
        }
        else
        {
            _activeDiveTeamTypeLabel.Text = string.Empty;
            _costLabel.Text = string.Empty;
            _descriptionLabel.Text = string.Empty;
            _costLabel.Modulate = Colors.White;
            _diveTeamCapacityLabel.Modulate = Colors.White;

            _buyButton.Disabled = true;
            _sellButton.Disabled = true;
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _diveTeamCapacityLabel.Text = $"Dive Teams: {_playerData.PlayerSub.DiveTeams.Count} / {_playerData.PlayerSub.MaximumDiveTeams}";

        foreach(var button in _currentDiversList.GetChildren().OfType<DiverVendorOptionButton>())
        {
            button.Pressed = button.DiveTeamType == _activeDiveTeamType && !_isForVendor;
        }
        foreach (var button in _vendorOptionList.GetChildren().OfType<DiverVendorOptionButton>())
        {
            button.Pressed = button.DiveTeamType == _activeDiveTeamType && _isForVendor;
        }
    }

    public void HireDiver()
    {
        _playerData.Money -= _activeDiveTeamType.Value;
        _playerData.PlayerSub.DiveTeams.Add(new DiveTeam(_playerData.PlayerSub, _activeDiveTeamType, _notificationSystem));

        SetActiveDiveTeamType(_activeDiveTeamType, _isForVendor);

        RefreshCurrentDiverList();
    }

    public void DismissDiver()
    {
        var toRemove = _playerData.PlayerSub.DiveTeams
            .Where(d => d.DiveTeamType == _activeDiveTeamType)
            .FirstOrDefault();

        if(toRemove != null)
        {
            _playerData.PlayerSub.DiveTeams.Remove(toRemove);
            _playerData.Money += _activeDiveTeamType.Value;

            SetActiveDiveTeamType(_activeDiveTeamType, _isForVendor);
        }

        RefreshCurrentDiverList();
    }
}
