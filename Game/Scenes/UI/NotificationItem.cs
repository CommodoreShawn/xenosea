using Godot;
using Xenosea.Logic;

public class NotificationItem : Label
{
    private float _timeToLive = 15;
    
    public void Init(Notification notification)
    {
        Text = notification.Body;

        if (notification.IsBad)
        {
            Modulate = Colors.Red;
        }
        else
        {
            Modulate = Colors.Green;
        }

        if (notification.IsMessage)
        {
            _timeToLive *= 2;
        }

        if (notification.IsImportant)
        {
            _timeToLive *= 2;
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _timeToLive -= delta;
        
        if(_timeToLive <= 0)
        {
            QueueFree();
        }
    }
}
