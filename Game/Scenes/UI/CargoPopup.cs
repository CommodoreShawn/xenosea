using Godot;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class CargoPopup : Popup
{
    [Export]
    public PackedScene CargoCommodityButtonPrefab;

    private VBoxContainer _cargoCommodityButtonList;
    private TextureRect _commodityIcon;
    private Label _commodityNameLabel;
    private Label _commodityDescriptionLabel;
    private Label _commodityAmountLabel;
    private TextureProgress _cargoProgress;

    private CommodityType _activeCommodity;

    [Inject]
    public IPlayerData _playerData;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _cargoCommodityButtonList = GetNode<VBoxContainer>("PanelContainer/VBoxContainer/HBoxContainer2/ScrollContainer/CargoCommodityButtonList");
        _commodityIcon = GetNode<TextureRect>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/HBoxContainer/CommodityIcon");
        _commodityNameLabel = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/HBoxContainer/CommodityNameLabel");
        _commodityDescriptionLabel = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/CommodityDescriptionLabel");
        _commodityAmountLabel = GetNode<Label>("PanelContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/CommodityAmountLabel");
        _cargoProgress = GetNode<TextureProgress>("PanelContainer/VBoxContainer/HBoxContainer/VBoxContainer/CargoProgress");
    }

    public void InitAndShow()
    {
        SelectCommodity(null);

        _cargoCommodityButtonList.ClearChildren();
        foreach (var commodity in _playerData.PlayerSub.CommodityStorage.CommoditiesPresent.OrderBy(x => x.Name))
        {
            if(_activeCommodity == null)
            {
                SelectCommodity(commodity);
            }

            var button = CargoCommodityButtonPrefab.Instance() as CargoCommodityButton;
            button.Commodity = commodity;
            _cargoCommodityButtonList.AddChild(button);
        }

        Show();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (Visible)
        {
            _cargoProgress.Value = 100 * (_playerData.PlayerSub.CommodityStorage.TotalVolume / _playerData.PlayerSub.CargoVolume);
            _cargoProgress.HintTooltip = $"{_playerData.PlayerSub.CommodityStorage.TotalVolume.ToString("N0")} / {_playerData.PlayerSub.CargoVolume.ToString("N0")}";

            foreach (var button in _cargoCommodityButtonList.GetChildren().OfType<CargoCommodityButton>())
            {
                button.Pressed = button.Commodity == _activeCommodity;
            }
        }
    }

    public void SelectCommodity(CommodityType commodity)
    {
        _activeCommodity = commodity;

        if (_activeCommodity != null)
        {
            //_commodityIcon.Texture = commodity.Icon;
            _commodityNameLabel.Text = commodity.Name;
            _commodityDescriptionLabel.Text = commodity.Description;
            _commodityAmountLabel.Text = $"Amount: {_playerData.PlayerSub.CommodityStorage.GetQty(commodity).ToString("N0")} u";
        }
        else
        {
            _commodityIcon.Texture = null;
            _commodityNameLabel.Text = string.Empty;
            _commodityDescriptionLabel.Text = string.Empty;
            _commodityAmountLabel.Text = string.Empty;
        }
    }

    public void Close()
    {
        Hide();
    }
}
