using Godot;
using System;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.Infrastructure.Missions;

public class CommodityTrader : TabContainer
{
    [Export]
    public PackedScene CommodityTraderRowPrefab;
    [Export]
    public PackedScene MissionOptionButtonPrefab;

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IMissionTracker _missionTracker;
    [Inject]
    private IWorldManager _worldManager;

    private ITradeMission _activeMission;

    private TextureProgress _selfCargoProgress;
    private Button _sortDestinationButton;
    private Button _sortRewardButton;
    private Button _filterSmugglingButton;
    private Button _filterValidButton;
    private VBoxContainer _missionButtonList;
    private Label _missionTitleLabel;
    private Label _missionCommodityLabel;
    private Label _missionForFactionLabel;
    private Label _missionAmountLabel;
    private Label _missionRewardLabel;
    private Label _missionDepositLabel;
    private Label _missionDurationLabel;
    private Button _acceptMissionButton;
    private Button _deliverMissionButton;
    private Control _missionInfoContents;
    private Label _smugglingWarningLabel;
    private Label _hostilityWarningLabel;
    private GameplayUi _gameplayUi;

    private OptionButton _filterDestinationButton;
    private string[] _destinationOptions;
    private OptionButton _filterFactionButton;
    private string[] _factionOptions;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _gameplayUi = this.FindParent<GameplayUi>();

        var commoditiesPresent = _playerData.PlayerSub.CommodityStorage.CommoditiesPresent
            .Concat(_playerData.PlayerSub.IsDockedTo.CommodityStorage.CommoditiesPresent)
            .Distinct()
            .OrderBy(c => c.Name)
            .ToArray();

        var commodityMarketList = GetNode<VBoxContainer>("Commodity Market/ScrollContainer/VBoxContainer");
        foreach(var commodity in commoditiesPresent)
        {
            var row = CommodityTraderRowPrefab.Instance() as CommodityTraderRow;
            row.Commodity = commodity;
            row.PlayerSub = _playerData.PlayerSub;
            row.Station = _playerData.PlayerSub.IsDockedTo as WorldStation;
            commodityMarketList.AddChild(row);
        }

        _selfCargoProgress = GetNode<TextureProgress>("Delivery Jobs/HBoxContainer/VBoxContainer/HBoxContainer2/CargoProgress");
        _sortDestinationButton = GetNode<Button>("Delivery Jobs/HBoxContainer/VBoxContainer/HBoxContainer/SortDestinationButton");
        _sortRewardButton = GetNode<Button>("Delivery Jobs/HBoxContainer/VBoxContainer/HBoxContainer/SortRewardButton");
        _filterSmugglingButton = GetNode<Button>("Delivery Jobs/HBoxContainer/VBoxContainer/HBoxContainer3/FilterSmugglingButton");
        _missionButtonList = GetNode<VBoxContainer>("Delivery Jobs/HBoxContainer/VBoxContainer/ScrollContainer/MissionButtonContainer");
        _missionTitleLabel = GetNode<Label>("Delivery Jobs/HBoxContainer/VBoxContainer2/MissionTitleLabel");
        _missionCommodityLabel = GetNode<Label>("Delivery Jobs/HBoxContainer/VBoxContainer2/HBoxContainer/CommodityLabel");
        _missionAmountLabel = GetNode<Label>("Delivery Jobs/HBoxContainer/VBoxContainer2/HBoxContainer/AmountLabel");
        _missionRewardLabel = GetNode<Label>("Delivery Jobs/HBoxContainer/VBoxContainer2/HBoxContainer2/RewardLabel");
        _missionDepositLabel = GetNode<Label>("Delivery Jobs/HBoxContainer/VBoxContainer2/HBoxContainer2/DepositLabel");
        _missionDurationLabel = GetNode<Label>("Delivery Jobs/HBoxContainer/VBoxContainer2/DurationLabel");
        _acceptMissionButton = GetNode<Button>("Delivery Jobs/HBoxContainer/VBoxContainer2/HBoxContainer3/AcceptButton");
        _deliverMissionButton = GetNode<Button>("Delivery Jobs/HBoxContainer/VBoxContainer2/HBoxContainer3/Deliver Button");
        _missionInfoContents = GetNode<Control>("Delivery Jobs/HBoxContainer/VBoxContainer2");
        _missionForFactionLabel = GetNode<Label>("Delivery Jobs/HBoxContainer/VBoxContainer2/ForFactionLabel");
        _smugglingWarningLabel = GetNode<Label>("Delivery Jobs/HBoxContainer/VBoxContainer2/SmugglingWarningLabel");
        _hostilityWarningLabel = GetNode<Label>("Delivery Jobs/HBoxContainer/VBoxContainer2/HostilityWarningLabel");
        _filterValidButton = GetNode<Button>("Delivery Jobs/HBoxContainer/VBoxContainer/HBoxContainer3/FilterValidButton");
        _filterDestinationButton = GetNode<OptionButton>("Delivery Jobs/HBoxContainer/VBoxContainer/HBoxContainer4/FilterDestinationButton");
        _filterFactionButton = GetNode<OptionButton>("Delivery Jobs/HBoxContainer/VBoxContainer/HBoxContainer4/FilterFactionButton");

        var allMissions = _missionTracker
            .GetAvailableMissions(_playerData.PlayerSub.IsDockedTo)
            .OfType<ITradeMission>();

        _filterDestinationButton.Clear();

        _destinationOptions = "Filter Destination".Yield()
            .Concat(
                allMissions
                    .Select(x => x.DestinationName)
                    .Distinct())
            .ToArray();

        foreach (var destination in _destinationOptions)
        {
            _filterDestinationButton.AddItem(destination);
        }
        _filterDestinationButton.Select(0);


        _filterFactionButton.Clear();

        _factionOptions = "Filter Faction".Yield()
            .Concat(
                allMissions
                    .Select(x => x.ForFaction.Name)
                    .Distinct())
            .ToArray();

        foreach (var faction in _factionOptions)
        {
            _filterFactionButton.AddItem(faction);
        }
        _filterFactionButton.Select(0);

        SetActiveMission(null);

        RefreshMissionList();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _selfCargoProgress.Value = 100 * (_playerData.PlayerSub.CommodityStorage.TotalVolume / _playerData.PlayerSub.CargoVolume);
        _selfCargoProgress.HintTooltip = $"{_playerData.PlayerSub.CommodityStorage.TotalVolume.ToString("N0")} / {_playerData.PlayerSub.CargoVolume.ToString("N0")}";

        foreach (var missionButton in _missionButtonList.GetChildren().OfType<MissionOptionButton>().ToArray())
        {
            missionButton.Pressed = missionButton.Mission == _activeMission;

            if(!_missionTracker.IsMissionAvailableAt(_playerData.PlayerSub.IsDockedTo, missionButton.Mission)
                && !_missionTracker.IsHeldByAgent(_playerData.PlayerSub, missionButton.Mission))
            {
                if(_activeMission == missionButton.Mission)
                {
                    SetActiveMission(_missionButtonList
                        .GetChildren()
                        .OfType<MissionOptionButton>()
                        .Select(x => x.Mission)
                        .Where(m => m != _activeMission)
                        .FirstOrDefault());
                }

                missionButton.QueueFree();
            }
        }

        if(_activeMission != null)
        {
            if (_missionTracker.IsHeldByAgent(_playerData.PlayerSub, _activeMission))
            {
                _missionDurationLabel.Text = $"Deliver By: {_activeMission.DueDate.Value.ToShortTimeString()} {_activeMission.DueDate.Value.ToShortDateString()}";
            }
            else
            {
                var deliverTime = _worldManager.CurrentDate + _activeMission.AllowedTime;
                _missionDurationLabel.Text = $"Deliver By: {deliverTime.Value.ToShortTimeString()} {deliverTime.Value.ToShortDateString()}";
            }
        }
        else
        {
            SetActiveMission(_missionButtonList.GetChildren().OfType<MissionOptionButton>().Select(x => x.Mission).FirstOrDefault());
        }
    }

    public void SetActiveMission(IMission mission)
    {
        if (mission != null && mission is ITradeMission tradeMission)
        {
            _activeMission = tradeMission;

            _deliverMissionButton.Disabled = !_missionTracker.IsHeldByAgent(_playerData.PlayerSub, _activeMission)
                || !_activeMission.CanBeDelivered(_playerData.PlayerSub);

            _missionInfoContents.Visible = true;
            _missionTitleLabel.Text = $"Deliver {tradeMission.Commodity.Name} to {tradeMission.DestinationName}";
            _missionForFactionLabel.Text = $"for {tradeMission.ForFaction.Name}";
            _missionCommodityLabel.Text = $"Commodity: {tradeMission.Commodity.Name}";
            _missionAmountLabel.Text = $"Amount: {tradeMission.Quantity} u";
            _missionRewardLabel.Text = $"Reward: {tradeMission.Reward.ToString("C0")}";
            _missionDepositLabel.Text = $"Deposit: {tradeMission.Deposit.ToString("C0")}";
            _acceptMissionButton.Disabled = !_missionTracker.IsMissionAvailableAt(_playerData.PlayerSub.IsDockedTo, _activeMission);
            
            if(_playerData.PlayerSub.IsDockedTo.Faction.Contraband.Contains(tradeMission.Commodity)
                && tradeMission.DestinationFaction.Contraband.Contains(tradeMission.Commodity))
            {
                _smugglingWarningLabel.Text = $"{tradeMission.Commodity.Name} is prohibited at this station and at {tradeMission.DestinationName}";
            }
            else if(_playerData.PlayerSub.IsDockedTo.Faction.Contraband.Contains(tradeMission.Commodity))
            {
                _smugglingWarningLabel.Text = $"{tradeMission.Commodity.Name} is prohibited at this station.";
            }
            else if(tradeMission.DestinationFaction.Contraband.Contains(tradeMission.Commodity))
            {
                _smugglingWarningLabel.Text = $"{tradeMission.Commodity.Name} is prohibited at {tradeMission.DestinationName}";
            }
            else
            {
                _smugglingWarningLabel.Text = string.Empty;
            }

            if(tradeMission.DestinationFaction.IsHostileTo(_playerData.Faction))
            {
                _hostilityWarningLabel.Text = $"{tradeMission.DestinationName} is hostile to you.";
                _acceptMissionButton.Disabled = true;
            }
            else
            {
                _hostilityWarningLabel.Text = string.Empty;
            }

            if (_playerData.PlayerSub.CommodityStorage.RemainingCapacity(tradeMission.Commodity) < tradeMission.Quantity)
            {
                _missionAmountLabel.Modulate = Colors.Red;
                _acceptMissionButton.Disabled = true;
            }
            else
            {
                _missionAmountLabel.Modulate = Colors.White;
            }

            if (_playerData.Money < tradeMission.Deposit)
            {
                _missionDepositLabel.Modulate = Colors.Red;
                _acceptMissionButton.Disabled = true;
            }
            else
            {
                _missionDepositLabel.Modulate = Colors.White;
            }
        }
        else
        {
            _activeMission = null;
            _missionInfoContents.Visible = false;
        }
    }

    public void SortMissionsByDestination()
    {
        _sortDestinationButton.Pressed = true;
        _sortRewardButton.Pressed = false;

        RefreshMissionList();
    }

    public void SortMissionsByReward()
    {
        _sortDestinationButton.Pressed = false;
        _sortRewardButton.Pressed = true;

        RefreshMissionList();
    }

    public void RefreshMissionFilters()
    {
        RefreshMissionList();
    }

    public void AcceptCurrentMission()
    {
        _playerData.Money -= _activeMission.Deposit;

        _missionTracker.PickupMission(
            _activeMission, 
            _playerData.PlayerSub.IsDockedTo, 
            _playerData.PlayerSub);

        if(_missionTracker.TrackedMission == null)
        {
            _missionTracker.TrackedMission = _activeMission;
        }

        _activeMission = null;
        RefreshMissionList();
    }

    private void RefreshMissionList()
    {
        _missionButtonList.ClearChildren();

        var availableMissions = _missionTracker
            .GetAvailableMissions(_playerData.PlayerSub.IsDockedTo)
            .OfType<ITradeMission>()
            .Where(m => !m.IsSmuggling || _filterSmugglingButton.Pressed)
            .Where(m => CanBeAccepted(m) || !_filterValidButton.Pressed);

        if(_filterDestinationButton.Selected > 0)
        {
            availableMissions = availableMissions
                .Where(m => m.DestinationName == _destinationOptions[_filterDestinationButton.Selected]);
        }
        if (_filterFactionButton.Selected > 0)
        {
            availableMissions = availableMissions
                .Where(m => m.ForFaction.Name == _factionOptions[_filterFactionButton.Selected]);
        }

        if (_sortDestinationButton.Pressed)
        {
            availableMissions = availableMissions
                .OrderBy(x => x.DestinationName);
        }
        if(_sortRewardButton.Pressed)
        {
            availableMissions = availableMissions
                .OrderByDescending(x => x.Reward);
        }

        var playerMissions = _missionTracker.GetCurrentMissions(_playerData.PlayerSub);

        if(_activeMission != null && !availableMissions.Contains(_activeMission))
        {
            _activeMission = null;
        }

        foreach (var mission in playerMissions.Concat(availableMissions).OfType<ITradeMission>())
        {
            if (_activeMission == null)
            {
                if(availableMissions.Any())
                {
                    if(availableMissions.Contains(mission))
                    {
                        SetActiveMission(mission);
                    }
                }
                else
                {
                    SetActiveMission(mission);
                }
            }

            var missionOptionButton = MissionOptionButtonPrefab.Instance() as MissionOptionButton;
            missionOptionButton.Initialize(mission);
            _missionButtonList.AddChild(missionOptionButton);
        }

        SetActiveMission(_activeMission);
    }

    private bool CanBeAccepted(ITradeMission tradeMission)
    {
        return !tradeMission.DestinationFaction.IsHostileTo(_playerData.Faction)
            && _playerData.PlayerSub.CommodityStorage.RemainingCapacity(tradeMission.Commodity) >= tradeMission.Quantity
            && _playerData.Money >= tradeMission.Deposit;
    }

    public void DeliverCurrentMission()
    {
        _playerData.Money += _activeMission.Deposit + _activeMission.Reward;

        _missionTracker.DeliverMission(
            _activeMission, 
            _playerData.PlayerSub.IsDockedTo, 
            _playerData.PlayerSub);

        SetActiveMission(null);

        RefreshMissionList();
    }

    public void SetDestinationFilter(int destinationIndex)
    {
        RefreshMissionList();
    }

    public void SetFactionFilter(int factionIndex)
    {
        RefreshMissionList();
    }
}
