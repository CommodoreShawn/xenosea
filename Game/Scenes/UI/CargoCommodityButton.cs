using Godot;
using Xenosea.Logic;

public class CargoCommodityButton : Button
{
    public CommodityType Commodity { get; internal set; }

    public override void _Ready()
    {
        Text = Commodity.Name;
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
    public void SelectCommodity()
    {
        this.FindParent<CargoPopup>().SelectCommodity(Commodity);
    }
}
