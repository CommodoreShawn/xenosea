using Godot;

public class ArmsDealer : TabContainer, IStationRoomWidgetContents
{
    public void InitializeFor(StationRoom stationRoom)
    {
        GetNode<TorpedoVendor>("Torpedos").InitializeFor(stationRoom);
        GetNode<SubmarineVendor>("Submarines").InitializeFor(stationRoom);
        GetNode<MinisubVendor>("Minisubs").InitializeFor(stationRoom);
    }
}
