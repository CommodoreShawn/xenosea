using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.Infrastructure.Missions;

public class MissionOptionButton : Button
{
    public IMission Mission { get; private set; }

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IMissionTracker _missionTracker;
    [Inject]
    private ISoundEffectPlayer _soundEffectPlayer;

    private Label _amountLabel;
    private TextureRect _missionReadyIcon;
    private TextureRect _missionPendingIcon;
    private TextureRect _missionSmugglingIcon;

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();
    }

    public void Initialize(IMission mission)
    {
        Mission = mission;
        GetNode<Label>("VBoxContainer/MissionNameLabel").Text = mission.Name;
        _amountLabel = GetNode<Label>("VBoxContainer/HBoxContainer/AmountLabel");
        _missionReadyIcon = GetNode<TextureRect>("ReadyIcon");
        _missionPendingIcon = GetNode<TextureRect>("PendingIcon");
        _missionSmugglingIcon = GetNode<TextureRect>("SmugglingIcon");

        if(mission.Reward > 0)
        {
            GetNode<Label>("VBoxContainer/HBoxContainer/RewardLabel").Text = "for " + mission.Reward.ToString("C0");
        }
        else
        {
            GetNode<Label>("VBoxContainer/HBoxContainer/RewardLabel").Text = string.Empty;
        }

        if(mission is ITradeMission tradeMission)
        {
            GetNode<Label>("VBoxContainer/HBoxContainer/AmountLabel").Text = $"{tradeMission.Quantity.ToString("N0")} u";
        }
        else
        {
            GetNode<Label>("VBoxContainer/HBoxContainer/AmountLabel").Text = string.Empty;
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_missionTracker.IsHeldByAgent(_playerData.PlayerSub, Mission))
        {
            _missionSmugglingIcon.Visible = false;

            if (Mission.CanBeDelivered(_playerData.PlayerSub))
            {
                _missionReadyIcon.Visible = true;
                _missionPendingIcon.Visible = false;
            }
            else
            {
                _missionReadyIcon.Visible = false;
                _missionPendingIcon.Visible = true;
            }
        }
        else
        {
            _missionReadyIcon.Visible = false;
            _missionPendingIcon.Visible = false;
            
            if (Mission is ITradeMission tradeMission)
            {
                _missionSmugglingIcon.Visible = tradeMission.IsSmuggling;

                if (_playerData.PlayerSub.CommodityStorage.RemainingCapacity(tradeMission.Commodity) < tradeMission.Quantity)
                {
                    _amountLabel.Modulate = Colors.Red;
                }
                else
                {
                    _amountLabel.Modulate = Colors.White;
                }
            }
            else
            {
                _missionSmugglingIcon.Visible = false;
            }
        }
    }

    public void SelectMission()
    {
        _soundEffectPlayer.PlayUiClick();
        this.FindParent<CommodityTrader>()?.SetActiveMission(Mission);
        this.FindParent<MissionPopup>()?.SetActiveMission(Mission);
        this.FindParent<SecurityOffice>()?.SetActiveMission(Mission);
        this.FindParent<BarMissionsTab>()?.SetActiveMission(Mission);
    }
}
