using Godot;
using System;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class MinisubVendor : VBoxContainer
{
    [Export]
    public PackedScene MinisubVendorOptionButtonPrefab;

    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IMissionTracker _missionTracker;
    [Inject]
    private IWorldManager _worldManager;
    [Inject]
    private IResourceLocator _resourceLocator;

    private VBoxContainer _minisubOptionContainer;
    private VBoxContainer _hangarOptionContainer;
    private TextureRect _minisubIconRect;
    private Label _minisubNameLabel;
    private Label _turretsLabel;
    private Label _speedLabel;
    private Label _healthLabel;
    private Label _armorLabel;
    private TextureRect _torpedoTypeIconRect;
    private Label _torpedoNameLabel;
    private Label _costLabel;
    private Label _descriptionLabel;
    private Button _buyButton;
    private Button _sellButton;
    private Label _batteryLabel;
    private Label _errorLabel;

    private MinisubType _activeMinisubType;
    private MinisubHangar _activeHangar;

    private Submarine _oldPlayerSub;
    private StationRoom _stationRoom;

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();

        _minisubOptionContainer = GetNode<VBoxContainer>("HBoxContainer/VendorVBoxContainer/ScrollContainer/MinisubOptionContainer");
        _hangarOptionContainer = GetNode<VBoxContainer>("HBoxContainer/HangarVBoxContainer/ScrollContainer/HangarOptionContainer");
        _minisubIconRect = GetNode<TextureRect>("HBoxContainer/VBoxContainer2/HBoxContainer/MinisubIconRect");
        _minisubNameLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer/MinisubNameLabel");
        _turretsLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer0/TurretsLabel");
        _speedLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer0/SpeedLabel");
        _healthLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer1/HealthLabel");
        _armorLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer1/ArmorLabel");
        _torpedoTypeIconRect = GetNode<TextureRect>("HBoxContainer/VBoxContainer2/HBoxContainer4/TorpedoIconRect");
        _torpedoNameLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer4/TorpedoNameLabel");
        _costLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer5/CostLabel");
        _descriptionLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/DescriptionLabel");
        _batteryLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/HBoxContainer5/BatteryLabel");
        _buyButton = GetNode<Button>("HBoxContainer/VBoxContainer2/HBoxContainer3/BuyButton");
        _sellButton = GetNode<Button>("HBoxContainer/VBoxContainer2/HBoxContainer3/SellButton");
        _errorLabel = GetNode<Label>("HBoxContainer/VBoxContainer2/ErrorLabel");
    }

    public void InitializeFor(StationRoom stationRoom)
    {
        _oldPlayerSub = _playerData.PlayerSub;
        _stationRoom = stationRoom;

        _hangarOptionContainer.ClearChildren();

        foreach (var hangar in _playerData.PlayerSub.MinisubHangars.Where(h => h.CanHoldMinisub))
        {
            var button = MinisubVendorOptionButtonPrefab.Instance() as MinisubVendorOptionButton;
            button.Hangar = hangar;
            _hangarOptionContainer.AddChild(button);

            if (_activeHangar == null)
            {
                SetActiveHangar(hangar);
            }
        }

        _minisubOptionContainer.ClearChildren();

        var station = (_playerData.PlayerSub.IsDockedTo as WorldStation).BackingStation;
        var industrialSupport = station.EconomicProcesses.Sum(x => x.IndustrialSupport);
        var militarySupport = station.EconomicProcesses.Sum(x => x.MilitarySupport);

        var minisubTypes = _resourceLocator.AllMinisubTypes
            .Where(x => industrialSupport >= x.IndustrialNeed)
            .Where(x => militarySupport >= x.MilitaryNeed)
            .Where(x => x.Operators.Contains(station.Faction))
            .ToArray();

        if (stationRoom is ArmsDealerStationRoom armsDealerStationRoom)
        {
            foreach (var minisubType in minisubTypes)
            {
                var button = MinisubVendorOptionButtonPrefab.Instance() as MinisubVendorOptionButton;
                button.MinisubType = minisubType;
                _minisubOptionContainer.AddChild(button);

                if (_activeMinisubType == null)
                {
                    SetActiveMinisubOption(minisubType);
                }
            }
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_playerData.PlayerSub != _oldPlayerSub)
        {
            InitializeFor(_stationRoom);
        }

        foreach (var button in _minisubOptionContainer.GetChildren().OfType<MinisubVendorOptionButton>())
        {
            button.Pressed = button.MinisubType == _activeMinisubType;
        }
        foreach (var button in _hangarOptionContainer.GetChildren().OfType<MinisubVendorOptionButton>())
        {
            button.Pressed = button.Hangar == _activeHangar;
        }

        var reputationMet = _playerData.Faction
                .GetStandingTo(_playerData.PlayerSub.IsDockedTo.Faction) >= _activeMinisubType.ReputationRequired;

        if (!reputationMet)
        {
            _errorLabel.Text = "Your standing isn't high enough.";
        }
        else
        {
            _errorLabel.Text = string.Empty;
        }

        _buyButton.Disabled = _activeHangar == null 
            || _activeMinisubType == null 
            || _activeHangar.MinisubType != null
            || _activeMinisubType.Value > _playerData.Money
            || !reputationMet;

        _sellButton.Disabled = _activeHangar == null || _activeHangar.MinisubType == null;
    }

    public void SetActiveHangar(MinisubHangar hangar)
    {
        _activeHangar = hangar;

        if (hangar.MinisubType != null)
        {
            SetActiveMinisubOption(hangar.MinisubType);
        }
    }

    public void SetActiveMinisubOption(MinisubType minisubType)
    {
        _activeMinisubType = minisubType;

        if(_activeMinisubType != null)
        {
            _minisubIconRect.Texture = minisubType.Icon;
            _minisubNameLabel.Text = minisubType.Name;
            _turretsLabel.Text = $"Guns: {minisubType.TurretModules.Length}";
            _speedLabel.Text = $"Top Speed: {(minisubType.EngineModule.MaximumThrust / (minisubType.HullType.FrontDragFactor * minisubType.ArmorType.DragFactor)).ToString("N0")}";
            _healthLabel.Text = $"Health: {minisubType.HullType.Health.ToString("N0")}";
            _armorLabel.Text = $"Armor: {minisubType.ArmorType.Armor}";

            if (_activeMinisubType.TorpedoesPerTube.Any())
            {
                _torpedoNameLabel.Text = $"{_activeMinisubType.LauncherModules.Sum(x=>x.MagazineCapacity)} {_activeMinisubType.TorpedoesPerTube[0].Name} torpedoes";
                _torpedoTypeIconRect.Texture = _activeMinisubType.TorpedoesPerTube[0].Icon;
            }
            else
            {
                _torpedoNameLabel.Text = string.Empty;
                _torpedoTypeIconRect.Texture = null;
            }

            _costLabel.Text = $"Value: {minisubType.Value.ToString("C0")}";
            _descriptionLabel.Text = minisubType.Description;
            _batteryLabel.Text = $"Battery Time: {minisubType.MaxCharge} s";

            if(_activeMinisubType != _activeHangar?.MinisubType
                && _activeMinisubType.Value > _playerData.Money)
            {
                _costLabel.Modulate = Colors.Red;
            }
        }
        else
        {
            _minisubIconRect.Texture = null;
            _minisubNameLabel.Text = string.Empty;
            _turretsLabel.Text = string.Empty;
            _speedLabel.Text = string.Empty;
            _healthLabel.Text = string.Empty;
            _armorLabel.Text = string.Empty;
            _torpedoNameLabel.Text = string.Empty;
            _torpedoTypeIconRect.Texture = null;
            _costLabel.Text = string.Empty;
            _descriptionLabel.Text = string.Empty;
            _batteryLabel.Text = string.Empty;
            _costLabel.Modulate = Colors.White;
        }
    }

    public void BuyMinisub()
    {
        _activeHangar.MinisubType = _activeMinisubType;
        _activeHangar.RebuildDockedModel();
        _playerData.Money -= _activeMinisubType.Value;
    }

    public void SellMinisub()
    {
        _playerData.Money += _activeHangar.MinisubType.Value;
        _activeHangar.MinisubType = null;
        _activeHangar.RebuildDockedModel();
    }
}
