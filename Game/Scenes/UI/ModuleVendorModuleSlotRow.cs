using Godot;
using Xenosea.Logic;
using System;

public class ModuleVendorModuleSlotRow : HBoxContainer
{
    private Button _lookButton;
    private Button _optionButton;
    private Button _clearButton;

    private string _pickerTitle;
    private int? _slotNumber;
    private Vector3? _lookAtPosition;
    private BasicModuleType _mountedModule;
    private BasicModuleType[] _options;
    private Action<BasicModuleType> _setModule;

    private ModuleVendorScreen _moduleVendorScreen;
    private ModulePickerPopup _modulePickerPopup;

    public override void _Ready()
    {
        _moduleVendorScreen = this.FindParent<ModuleVendorScreen>();
        _modulePickerPopup = _moduleVendorScreen.GetNode<ModulePickerPopup>("ModulePickerPopup");
    }

    public void Initialize(
        string pickerTitle,
        int? slotNumber,
        Vector3? lookAtPosition,
        BasicModuleType mountedModule,
        BasicModuleType[] options,
        Action<BasicModuleType> setModule)
    {
        _lookButton = GetNode<Button>("LookButton");
        _optionButton = GetNode<Button>("OptionButton");
        _clearButton = GetNode<Button>("ClearButton");

        _pickerTitle = pickerTitle;
        _slotNumber = slotNumber;
        _lookAtPosition = lookAtPosition;
        _setModule = setModule;
        _options = options;

        _lookButton.Text = slotNumber?.ToString() ?? string.Empty;
        _lookButton.Visible = _lookAtPosition != null;

        UpdateModule(mountedModule);
    }

    private void UpdateModule(BasicModuleType module)
    {
        _mountedModule = module;
        _optionButton.Text = _mountedModule?.Name ?? string.Empty;
        _clearButton.Disabled = _mountedModule == null;
    }

    public void LookAtModule()
    {
        if (_lookAtPosition.HasValue)
        {
            _moduleVendorScreen.LookAtModulePosition(_lookAtPosition.Value);
        }
    }

    public void PickModule()
    {
        _modulePickerPopup.ShowFor(
            _pickerTitle,
            _mountedModule,
            _options,
            (pickedOption) =>
            {
                UpdateModule((BasicModuleType)pickedOption);
                _setModule((BasicModuleType)pickedOption);
            },
            null,
            null);
    }

    public void PickModule(Action cancelCallback)
    {
        _modulePickerPopup.ShowFor(
            _pickerTitle,
            _mountedModule,
            _options,
            (pickedOption) =>
            {
                UpdateModule((BasicModuleType)pickedOption);
                _setModule((BasicModuleType)pickedOption);
            },
            null,
            cancelCallback);
    }

    public void ClearModule()
    {
        UpdateModule(null);
        _setModule(null);
    }
}
