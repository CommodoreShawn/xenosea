using Godot;
using System;
using Xenosea.Logic;
using Xenosea.Logic.Detection;

public class GunTurret : Spatial
{
    [Export]
    public float MinElevation = 60;

    [Export]
    public float MaxElevation = 100;

    [Export]
    public float ShotVariance = 0.1f;

    [Export]
    public int BarrelCount = 1;

    [Export]
    public float ShotDelay = 0.15f;

    public float ReloadTime => TurretModuleType.ReloadTime;

    public float ReloadCounter { get; set; }
    public Spatial[] Muzzles { get; private set; }
    public bool IsAimed { get; private set; }
    public bool IsTurning { get; private set; }
    public TurretModuleType TurretModuleType { get; set; }

    protected ICombatant _combatant;
    protected ITargetableThing _volleyTarget;
    protected TurretMount _mount;
    public TurretSystem TurretSystem { get; set; }
    protected Spatial _barrelBase;
    protected System.Random _random;

    protected bool _isShooting;
    protected float _shotDelay;
    protected int _nextMuzzle;
    protected int _shotsLeft;
    protected GameplayRoot _gameplayRoot;

    public override void _Ready()
    {
        _gameplayRoot = this.FindParent<GameplayRoot>();
        _combatant = this.FindParent<ICombatant>();
        _mount = this.FindParent<TurretMount>();
        _barrelBase = GetNode<Spatial>("BarrelBase");
        _random = new System.Random();

        Muzzles = new Spatial[BarrelCount];

        for(var i = 0; i < BarrelCount; i++)
        {
            Muzzles[i] = GetNode<Spatial>($"BarrelBase/Muzzle{i}");
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        IsTurning = false;

        if(_combatant != null && !_combatant.IsWrecked)
        {
            DoAimingLogic(delta);
        }

        if (TurretModuleType != null)
        {
            DoReloadLogic(delta);
        }

        if(_isShooting)
        {
            DoShootingLogic(delta);
        }
    }

    protected virtual void DoShootingLogic(float delta)
    {
        _shotDelay -= delta;

        if (_shotDelay <= 0 && TurretSystem.ActiveAmmo != null)
        {
            var projectile = TurretSystem.ActiveAmmo.BulletPrefab.Instance() as RigidBody;

            if (projectile is JetBullet shell)
            {
                shell.Shooter = _combatant;

                if(shell is SemiSeekingMissile missile
                    && _volleyTarget is SonarContactRecord scr)
                {
                    missile.Target = scr;
                }
            }
            else if (projectile is Torpedo torp)
            {
                torp.Shooter = _combatant;
            }

            projectile.GlobalTransform = Muzzles[_nextMuzzle].GlobalTransform;

            if (TurretSystem.ActiveAmmo.ShotEffectPrefab != null)
            {
                var shotEffect = TurretSystem.ActiveAmmo.ShotEffectPrefab.Instance() as Spatial;
                shotEffect.GlobalTransform = Muzzles[_nextMuzzle].GlobalTransform;
                _gameplayRoot.AddChild(shotEffect);
            }

            _gameplayRoot.AddChild(projectile);

            projectile.RotateX((float)_random.NextDouble() * ShotVariance);
            projectile.RotateY((float)_random.NextDouble() * ShotVariance);
            projectile.RotateZ((float)_random.NextDouble() * ShotVariance);
            projectile.LinearVelocity = projectile.GlobalTransform.basis.Xform(new Vector3(0, 0, TurretSystem.ActiveAmmo.MuzzleVelocity));

            projectile.AddCollisionExceptionWith(_combatant as Spatial);

            _combatant.TemporaryNoises.Add(new TemporaryNoise
            {
                NoiseAmount = TurretSystem.ActiveAmmo.FiringNoise,
                TimeToLive = 30
            });

            _nextMuzzle = (_nextMuzzle + 1) % BarrelCount;
            _shotDelay = (float)_random.NextDouble() * ShotDelay;
            _shotsLeft -= 1;
            _isShooting = _shotsLeft > 0;

            if (_shotsLeft <= 0)
            {
                ReloadCounter = 0;
                _volleyTarget = null;
            }
        }
    }

    protected virtual void DoReloadLogic(float delta)
    {
        if (ReloadCounter < TurretModuleType.ReloadTime && !_isShooting)
        {
            ReloadCounter = Mathf.Min(ReloadCounter + delta, TurretModuleType.ReloadTime);
        }
    }

    protected virtual void DoAimingLogic(float delta)
    {
        var aimVector = _combatant.AimPoint - Muzzles[_nextMuzzle % BarrelCount].GlobalTransform.origin;

        var horizontalPlane = new Plane(GlobalTransform.basis.y, 0);
        var projectedAim = horizontalPlane.Project(aimVector).Normalized();
        var yaw = projectedAim.AngleTo(GlobalTransform.basis.z);
        yaw *= Mathf.Sign(projectedAim.Dot(GlobalTransform.basis.x));

        var verticalPlane = new Plane(_barrelBase.GlobalTransform.basis.x, 0);
        projectedAim = verticalPlane.Project(aimVector).Normalized();
        var pitch = projectedAim.AngleTo(_barrelBase.GlobalTransform.basis.z);
        pitch *= -Mathf.Sign(projectedAim.Dot(_barrelBase.GlobalTransform.basis.y));

        IsAimed = Mathf.Abs(yaw) < TurretModuleType.TurnRate * delta
            && Mathf.Abs(pitch) < TurretModuleType.TurnRate * delta;

        var yawTurn = Mathf.Clamp(yaw, -TurretModuleType.TurnRate * delta, TurretModuleType.TurnRate * delta);
        var pitchTurn = Mathf.Clamp(pitch, -TurretModuleType.TurnRate * delta, TurretModuleType.TurnRate * delta);

        if (yawTurn != 0)
        {
            RotateY(yawTurn);

            if (GlobalTransform.basis.z.AngleTo(_mount.GlobalTransform.basis.z) > Mathf.Deg2Rad(_mount.YawRange))
            {
                RotateY(-yawTurn);
            }
            else if (Mathf.Abs(yaw) > 2 * TurretModuleType.TurnRate * delta)
            {
                IsTurning = true;
            }
        }

        var maxElevation = Mathf.Min(MaxElevation, _mount.MaxElevation);
        var minElevation = Mathf.Max(MinElevation, _mount.MinElevation);

        if (pitchTurn > 0 && _barrelBase.Transform.basis.GetEuler().x + pitchTurn < Mathf.Deg2Rad(maxElevation))
        {
            _barrelBase.RotateX(pitchTurn);

            IsTurning = IsTurning || pitch > TurretModuleType.TurnRate * delta;
        }
        else if (pitchTurn < 0 && _barrelBase.Transform.basis.GetEuler().x + pitchTurn > Mathf.Deg2Rad(minElevation))
        {
            _barrelBase.RotateX(pitchTurn);

            IsTurning = IsTurning || pitch > TurretModuleType.TurnRate * delta;
        }
    }

    public virtual void Shoot(ITargetableThing target)
    {
        if (_combatant != null
            && ReloadCounter >= TurretModuleType.ReloadTime
            && IsAimed
            && !_combatant.IsWrecked
            && !_isShooting)
        {
            _volleyTarget = target;
            _isShooting = true;
            _nextMuzzle = 0;
            _shotDelay = (float)_random.NextDouble() * Mathf.Min(ShotDelay, 0.5f);
            _shotsLeft = TurretModuleType.ShotCount;
        }
    }
}
