using Godot;
using Xenosea.Logic.Detection;

public class RailgunTurret : GunTurret
{
    [Export]
    public PackedScene ChargeEffect;

    [Export]
    public float ChargeTime;

    private bool _startedCharge;
    private float _chargeTimer;

    protected override void DoShootingLogic(float delta)
    {
        _shotDelay -= delta;

        if (_shotDelay <= 0 && TurretSystem.ActiveAmmo != null)
        {
            if(!_startedCharge)
            {
                _startedCharge = true;
                _chargeTimer = ChargeTime;

                if(ChargeEffect != null)
                {
                    var chargeEffect = ChargeEffect.Instance() as Spatial;
                    Muzzles[_nextMuzzle].AddChild(chargeEffect);
                }
            }
            else if(_startedCharge && _chargeTimer > 0)
            {
                _chargeTimer -= delta;
            }
            else if(_startedCharge && _chargeTimer <= 0)
            {
                var projectile = TurretSystem.ActiveAmmo.BulletPrefab.Instance() as Spatial;

                if (projectile is RailgunShot railShot)
                {
                    railShot.Shooter = _combatant;
                }

                _gameplayRoot.AddChild(projectile);
                projectile.GlobalTransform = Muzzles[_nextMuzzle].GlobalTransform;

                if (TurretSystem.ActiveAmmo.ShotEffectPrefab != null)
                {
                    var shotEffect = TurretSystem.ActiveAmmo.ShotEffectPrefab.Instance() as Spatial;
                    Muzzles[_nextMuzzle].AddChild(shotEffect);
                }

                projectile.RotateX((float)_random.NextDouble() * ShotVariance);
                projectile.RotateY((float)_random.NextDouble() * ShotVariance);
                projectile.RotateZ((float)_random.NextDouble() * ShotVariance);

                

                _combatant.TemporaryNoises.Add(new TemporaryNoise
                {
                    NoiseAmount = TurretSystem.ActiveAmmo.FiringNoise,
                    TimeToLive = 30
                });

                _nextMuzzle = (_nextMuzzle + 1) % BarrelCount;
                _shotDelay = (float)_random.NextDouble() * ShotDelay;
                _shotsLeft -= 1;
                _isShooting = _shotsLeft > 0;
                _startedCharge = false;

                if (_shotsLeft <= 0)
                {
                    ReloadCounter = 0;
                }
            }
        }
    }

    public override void Shoot(ITargetableThing target)
    {
        if (_combatant != null
            && ReloadCounter >= TurretModuleType.ReloadTime
            && IsAimed
            && !_combatant.IsWrecked
            && !_isShooting)
        {
            _isShooting = true;
            _startedCharge = false;
            _nextMuzzle = 0;
            _shotDelay = (float)_random.NextDouble() * Mathf.Min(ShotDelay, 0.5f);
            _shotsLeft = TurretModuleType.ShotCount;
        }
    }
}
