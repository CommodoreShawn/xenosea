using Godot;
using Xenosea.Logic;

public class Searchlight : Spatial
{
    [Export]
    public float TurnRate = 1;

    [Export]
    public float MinElevation = 30;

    [Export]
    public float MaxElevation = -30;

    private ICombatant _combatant;
    private Spatial _mount;
    private Spatial _barrelBase;
    private System.Random _random;

    public override void _Ready()
    {
        _combatant = this.FindParent<ICombatant>();
        _barrelBase = GetNode<Spatial>("BarrelBase");
        _mount = GetParent<Spatial>();
        _random = new System.Random();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_combatant != null)
        {
            var aimVector = _combatant.AimPoint - GlobalTransform.origin;
            var horizontalPlane = new Plane(GlobalTransform.basis.y, 0);
            var projectedAim = horizontalPlane.Project(aimVector).Normalized();
            var yaw = projectedAim.AngleTo(GlobalTransform.basis.z);
            yaw *= Mathf.Sign(projectedAim.Dot(GlobalTransform.basis.x));

            var verticalPlane = new Plane(_barrelBase.GlobalTransform.basis.x, 0);
            projectedAim = verticalPlane.Project(aimVector).Normalized();
            var pitch = projectedAim.AngleTo(_barrelBase.GlobalTransform.basis.z);
            pitch *= -Mathf.Sign(projectedAim.Dot(_barrelBase.GlobalTransform.basis.y));
            
            var yawTurn = Mathf.Clamp(yaw, -TurnRate * delta, TurnRate * delta);
            var pitchTurn = Mathf.Clamp(pitch, -TurnRate * delta, TurnRate * delta);

            if (yawTurn != 0)
            {
                RotateY(yawTurn);
            }

            var maxElevation = MaxElevation;
            var minElevation = MinElevation;

            if (pitchTurn > 0 && _barrelBase.Transform.basis.GetEuler().x + pitchTurn < Mathf.Deg2Rad(maxElevation))
            {
                _barrelBase.RotateX(pitchTurn);
            }
            else if (pitchTurn < 0 && _barrelBase.Transform.basis.GetEuler().x + pitchTurn > Mathf.Deg2Rad(minElevation))
            {
                _barrelBase.RotateX(pitchTurn);
            }
        }
    }
}
