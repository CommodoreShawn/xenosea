using Godot;
using Xenosea.Logic;

public class RailgunShot : Spatial
{
    [Export]
    public PackedScene HitEffect;
    [Export]
    public float PersistTime;
    [Export]
    public float Damage;
    [Export]
    public float Penetration;
    [Export]
    public float MaximumRange;

    private bool _shotDone;
    private float _timeToLive;
    private RayCast _rayCast;
    private CPUParticles _bubbles;

    public ICombatant Shooter { get; set; }

    public override void _Ready()
    {
        this.ResolveDependencies();
        _rayCast = GetNode<RayCast>("RayCast");
        _bubbles = GetNode<CPUParticles>("BubbleLine");

        if (Shooter != null)
        {
            _rayCast.AddException(Shooter as Godot.Object);
        }
        _rayCast.CastTo = GlobalTransform.basis.z * MaximumRange;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (!_shotDone)
        {
            _shotDone = true;
            _timeToLive = PersistTime;

            if (_rayCast.IsColliding())
            {
                var hitPoint = _rayCast.GetCollisionPoint();

                var fromTo = hitPoint - GlobalTransform.origin;
                _bubbles.LookAtFromPosition(
                    GlobalTransform.origin + fromTo / 2,
                    hitPoint,
                    Vector3.Up);
                _bubbles.EmissionBoxExtents = new Vector3(
                    0,
                    0,
                    fromTo.Length() / 2);

                if (HitEffect != null)
                {
                    var hitEffect = HitEffect.Instance() as Spatial;
                    GetParent().AddChild(hitEffect);
                    hitEffect.LookAtFromPosition(
                        hitPoint,
                        Vector3.Zero,
                        Vector3.Up);
                }

                if (_rayCast.GetCollider() is ICombatant combatant)
                {
                    var damageFactor =
                        Mathf.Clamp(
                            1 - Mathf.Pow(fromTo.Length() / MaximumRange, 2),
                            0,
                            1);

                    combatant.HitRecieved(
                        Shooter,
                        damageFactor * Damage,
                        Penetration,
                        hitPoint,
                        fromTo.Normalized());
                }
            }

            _bubbles.Emitting = true;
        }

        if (_shotDone)
        {
            _timeToLive -= delta;

            if (_timeToLive <= 0)
            {
                QueueFree();
            }
        }
    }
}
