using Godot;
using Xenosea.Logic;

public class SonicBeamShot : Spatial
{
    [Export]
    public float BeamTime;
    [Export]
    public float PersistTime;
    [Export]
    public float Damage;
    [Export]
    public float Penetration;
    [Export]
    public float MaximumRange;

    private bool _shotDone;
    private float _timer;
    private RayCast _rayCast;
    private CPUParticles _beamParticles;
    private CPUParticles _shootingParticles;
    private CPUParticles _hitParticles;
    private float _damagePerSecond;

    public ICombatant Shooter { get; set; }

    public Spatial Muzzle { get; set; }

    public override void _Ready()
    {
        this.ResolveDependencies();
        _rayCast = GetNode<RayCast>("RayCast");
        _beamParticles = GetNode<CPUParticles>("BeamParticles");
        _shootingParticles = GetNode<CPUParticles>("ShootingParticles");
        _hitParticles = GetNode<CPUParticles>("HitParticles");

        if (Shooter != null)
        {
            _rayCast.AddException(Shooter as Godot.Object);
        }
        _rayCast.CastTo = GlobalTransform.basis.z * MaximumRange;

        _damagePerSecond = Damage / BeamTime;

        _shootingParticles.Emitting = false;
        _hitParticles.Emitting = false;
        _beamParticles.Emitting = false;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _timer += delta;

        if(Muzzle != null && IsInstanceValid(Muzzle))
        {
            GlobalTransform = Muzzle.GlobalTransform;
        }

        if (!_shotDone)
        {
            if(_timer < BeamTime)
            {
                if (_rayCast.IsColliding())
                {
                    var hitPoint = _rayCast.GetCollisionPoint();

                    var fromTo = hitPoint - GlobalTransform.origin;
                    //_beamParticles.LookAtFromPosition(
                    //    GlobalTransform.origin + fromTo / 2,
                    //    hitPoint,
                    //    Vector3.Up);

                    //_beamParticles.EmissionBoxExtents = new Vector3(
                    //    0,
                    //    0,
                    //    fromTo.Length() / 2);

                    _hitParticles.LookAtFromPosition(hitPoint, GlobalTransform.origin, Vector3.Up);

                    if(!_hitParticles.Emitting)
                    {
                        _hitParticles.Emitting = true;
                    }

                    if (_rayCast.GetCollider() is ICombatant combatant)
                    {
                        combatant.HitRecieved(
                            Shooter,
                            delta * _damagePerSecond,
                            Penetration,
                            hitPoint,
                            fromTo.Normalized());
                    }
                }
                else
                {
                    if (_hitParticles.Emitting)
                    {
                        _hitParticles.Emitting = false;
                    }
                }

                if (!_beamParticles.Emitting)
                {
                    _shootingParticles.Emitting = true;
                    _beamParticles.Emitting = true;
                }
            }
            else
            {
                _shotDone = true;
                _shootingParticles.Emitting = false;
                _hitParticles.Emitting = false;
                _beamParticles.Emitting = false;
            }
        }

        if (_shotDone && _timer > BeamTime + PersistTime)
        {
            QueueFree();
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);
    }
}
