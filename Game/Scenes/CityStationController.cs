using System.Linq;
using Xenosea.Logic;
using Xenosea.Resources;

public class CityStationController : AbstractStationController
{
    private float _launchCheckTimer;

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _launchCheckTimer -= delta;

        if(_launchCheckTimer <= 0 && !Station.IsWrecked)
        {
            _launchCheckTimer = 7;

            var shouldLaunch = Station.SonarContactRecords
                .Where(scr => scr.IsDetected && scr.ContactType != ContactType.Wreck)
                .Where(scr => scr.ContactType != ContactType.Torpedo)
                .Where(scr => scr.IsDetected && Station.IsHostileTo(scr.SonarContact))
                .Any();

            if(shouldLaunch)
            {
                foreach(var hangar in Station.MinisubHangars)
                {
                    hangar.Launch();
                }
            }
            else
            {
                foreach (var hangar in Station.MinisubHangars)
                {
                    hangar.ShouldRecall = true;
                }
            }
        }
    }
}
