using Xenosea.Logic;

public class CircleBotController : AbstractSubController
{
    private System.Random _random;
    private float _turnTimer;

    public override void _Ready()
    {
        base._Ready();

        _random = new System.Random();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (Submarine != null)
        {
            _turnTimer -= delta;

            if(_turnTimer <= 0)
            {
                _turnTimer = _random.Next(5, 15);

                Submarine.Rudder = (float)_random.NextDouble() - 0.25f;
            }

            Submarine.Throttle = 1;
        }
    }
}
