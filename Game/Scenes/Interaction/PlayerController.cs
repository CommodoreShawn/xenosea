using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Docking;
using Xenosea.Logic.Infrastructure.Interfaces;

public class PlayerController : AbstractSubController
{
    private SubCamera _camera;
    private GameplayUi _gameplayUi;

    public AutopilotType AutopilotType { get; set; }
    public ITargetableThing AutopilotTarget { get; set; }
    public DockingSequence DockingSequence { get; set; }

    public bool ControlLockout { get; set; }

    public float AutopilotThrottleLimit { get; set; } = 1;

    private bool _wasAutopilot;

    protected override double TurningPidGain => 4;


    [Inject]
    private IPlayerData _playerData;

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();

        _camera = this.FindParent<Submarine>().GetParent().GetNode<SubCamera>("Camera");
        _gameplayUi = this.FindParent<Submarine>().GetParent().GetNode<GameplayUi>("UI");
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (Submarine != null)
        {
            if(AutopilotType == AutopilotType.None && _wasAutopilot)
            {
                Submarine.Throttle = Mathf.Clamp(Mathf.RoundToInt(Submarine.Throttle * 4) * 0.25f, -0.25f, 1f);
            }

            _wasAutopilot = AutopilotType != AutopilotType.None;
            
            if (AutopilotType != AutopilotType.None)
            {
                if (AutopilotTarget == null || !AutopilotTarget.IsValidInstance || !AutopilotTarget.IsDetected)
                {
                    if (DockingSequence == null)
                    {
                        AutopilotType = AutopilotType.None;
                        AutopilotTarget = null;
                        Submarine.Throttle = 0;
                        Submarine.Rudder = 0;
                        Submarine.Dive = 0;
                    }
                }
            }

            if(DockingSequence != null 
                && !DockingSequence.IsLocked
                && (AutopilotType != AutopilotType.Dock 
                    || (AutopilotTarget is SonarContactRecord scr && scr.SonarContact != DockingSequence.Station) 
                    || Submarine.IsWrecked))
            {
                DockingSequence.Cancel();
                DockingSequence = null;
                Submarine.Throttle = 0;
                Submarine.Rudder = 0;
                Submarine.Dive = 0;
            }

            if (AutopilotType == AutopilotType.Dock && DockingSequence != null && DockingSequence.IsFinished)
            {
                AutopilotType = AutopilotType.None;
                AutopilotTarget = null;
                DockingSequence = null;
                Submarine.Throttle = 0;
                Submarine.Rudder = 0;
                Submarine.Dive = 0;
            }

            if (AutopilotType == AutopilotType.None)
            {
                if (!ControlLockout)
                {
                    if (Input.IsActionJustPressed("control_throttle_up"))
                    {
                        ThrottleUp();
                        
                    }
                    if (Input.IsActionJustPressed("control_throttle_down"))
                    {
                        ThrottleDown();
                    }
                    if (Input.IsActionJustPressed("control_dive_up"))
                    {
                        DiveUp();
                    }

                    if (Input.IsActionJustPressed("control_dive_down"))
                    {
                        DiveDown();
                    }

                    Submarine.Rudder = Input.GetActionStrength("control_rudder_left") - Input.GetActionStrength("control_rudder_right");
                }
                else
                {
                    Submarine.Rudder = 0;
                }
            }
            else if (AutopilotType == AutopilotType.Goto)
            {
                AutopilotTo(AutopilotTarget.EstimatedPosition, delta, AutopilotThrottleLimit);

                if(Submarine.RealPosition.DistanceTo(AutopilotTarget.EstimatedPosition) < 500
                    && !(AutopilotTarget is MissionPath))
                {
                    AutopilotType = AutopilotType.None;
                    Submarine.Throttle = 0;
                    Submarine.Rudder = 0;
                    Submarine.Dive = 0;
                }
            }
            else if (AutopilotType == AutopilotType.Follow)
            {
                AutopilotFollow(AutopilotTarget.EstimatedPosition, delta);
            }
            else if (AutopilotType == AutopilotType.Dock)
            {
                if(DockingSequence != null)
                {
                    DockingSequence.Update(this, delta);
                }
                else if (Submarine.RealPosition.DistanceTo(((AutopilotTarget as SonarContactRecord).SonarContact as IStation).DockWaitingArea.GlobalTransform.origin) > 1000)
                {
                    AutopilotTo(((AutopilotTarget as SonarContactRecord).SonarContact as IStation).DockWaitingArea.GlobalTransform.origin, delta, 1);
                }
                else
                {
                    DockingSequence = ((AutopilotTarget as SonarContactRecord).SonarContact as IStation).RequestDocking(Submarine);

                    Submarine.Throttle = 0;
                    Submarine.Rudder = 0;
                    Submarine.Dive = 0;

                    if (DockingSequence == null)
                    {
                        AutopilotType = AutopilotType.None;
                    }
                }
            }

            if (DockingSequence == null || !DockingSequence.IsLocked)
            {
                if (Input.IsMouseButtonPressed((int)ButtonList.Left) 
                    && _camera.IsAiming
                    && !ControlLockout
                    && !_playerData.WeaponsSafe)
                {
                    _gameplayUi.ActiveWeaponSystem?.Shoot(_gameplayUi.CurrentTarget);
                }
            }
        }
    }

    public void DiveUp()
    {
        Submarine.Dive = Mathf.Clamp(Util.Debucket(Util.Bucketize(Submarine.Dive) + 1), -1, 1);
    }

    public void DiveDown()
    {
        Submarine.Dive = Mathf.Clamp(Util.Debucket(Util.Bucketize(Submarine.Dive) - 1), -1, 1);
    }

    public void ThrottleUp()
    {
        Submarine.Throttle = Mathf.Clamp(Util.Debucket(Util.Bucketize(Submarine.Throttle) + 1), -0.25f, 1);
    }

    public void ThrottleDown()
    {
        Submarine.Throttle = Mathf.Clamp(Util.Debucket(Util.Bucketize(Submarine.Throttle) - 1), -0.25f, 1);
    }
}
