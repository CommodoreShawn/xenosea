﻿using Godot;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.AI;
using Xenosea.Logic.Infrastructure.Interfaces;

public class GoalStationController : AbstractStationController
{
    [Export]
    public BotGoalType[] GoalTypes;

    private IBotGoal _botGoal;
    private float _goalRecheckTimer;

    [Inject]
    private INotificationSystem _notificationSystem;
    
    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (Station != null && !Station.IsWrecked)
        {
            _goalRecheckTimer -= delta;

            if (_goalRecheckTimer <= 0)
            {

                _goalRecheckTimer = 7;

                if(_botGoal != null && _botGoal.IsValid(Station))
                {
                    var newGoalType = GoalTypes
                        .Where(g => g.Priority > _botGoal.Priority)
                        .Where(g => g.IsValid(Station))
                        .OrderByDescending(g => g.Priority)
                        .FirstOrDefault();

                    if(newGoalType != null)
                    {
                        _botGoal = newGoalType.CreateGoal(Station);
                        _notificationSystem.Broadcast(Station, _botGoal.MakeBroadcast());
                    }
                }
                else
                {
                    var newGoalType = GoalTypes
                        .Where(g => g.IsValid(Station))
                        .OrderByDescending(g => g.Priority)
                        .FirstOrDefault();

                    if (newGoalType != null)
                    {
                        _botGoal = newGoalType.CreateGoal(Station);
                        _notificationSystem.Broadcast(Station, _botGoal.MakeBroadcast());
                    }
                    else
                    {
                        _botGoal = null;
                    }
                }
            }

            if(_botGoal != null)
            {
                _botGoal.Update(Station, delta);
            }
        }
    }
}
