﻿using Autofac;
using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.AI;
using Xenosea.Logic.Docking;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

public class GoalBotController : AbstractSubController
{
    [Export]
    public BotGoalType[] GoalTypes;

    [Export]
    public AbstractHailResponseLogicSource HailResponseLogicSource;

    [Export]
    public bool IsSecurityResponder;

    private Stack<IBotGoal> _activeGoals;
    private float _goalRecheckTimer;

    public DockingSequence DockingSequence { get; set; }

    private INotificationSystem _notificationSystem;
    private ISimulationEventBus _simulationEventBus;
    private IPlayerData _playerData;

    public IEnumerable<IBotGoal> ActiveGoals => _activeGoals;

    public override void _Ready()
    {
        base._Ready();
        _activeGoals = new Stack<IBotGoal>();
        _notificationSystem = IocManager.IocContainer.Resolve<INotificationSystem>();
        _simulationEventBus = IocManager.IocContainer.Resolve<ISimulationEventBus>();
        _playerData = IocManager.IocContainer.Resolve<IPlayerData>();
        _simulationEventBus.OnSimulationEvent += OnSimulationEvent;
    }

    public override void _ExitTree()
    {
        base._ExitTree();

        _simulationEventBus.OnSimulationEvent -= OnSimulationEvent;
    }

    private void OnSimulationEvent(ISimulationEvent evt)
    {
        var goalToHandle = _activeGoals
            .Where(gt => gt.CanHandle(this, evt))
            .FirstOrDefault();

        if(goalToHandle != null)
        {
            goalToHandle.Handle(this, evt);
        }
        else if(evt is PlayerHailSimulationEvent playerHailSimulationEvent
            && playerHailSimulationEvent.TargetSubmarine == Submarine)
        {
            if (Submarine.IsHostileTo(_playerData.PlayerSub))
            {
                _notificationSystem.Broadcast(
                    Submarine,
                    "I have nothing to say to you.");
            }
            else if(HailResponseLogicSource != null)
            {
                _simulationEventBus.SendEvent(new HailResponseSimulationEvent(Submarine, HailResponseLogicSource.GetResponseLogic(this)));
            }
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (Submarine != null && !Submarine.IsWrecked)
        {
            _goalRecheckTimer -= delta;

            if (_goalRecheckTimer <= 0)
            {
                _goalRecheckTimer = 7;
                
                while (_activeGoals.Any() && !_activeGoals.Peek().IsValid(this))
                {
                    _activeGoals.Pop();
                }

                if(_activeGoals.Any())
                {
                    var newGoalType = GoalTypes
                        .Where(g => g.Priority > _activeGoals.Peek().Priority)
                        .Where(g => g.IsValid(Submarine))
                        .OrderByDescending(g => g.Priority)
                        .FirstOrDefault();

                    if (newGoalType != null)
                    {
                        
                        var newGoal = newGoalType.CreateGoal(Submarine);

                        if (newGoal != null)
                        {
                            _activeGoals.Push(newGoal);
                            _notificationSystem.Broadcast(Submarine, newGoal.MakeBroadcast());
                        }
                    }
                }
                else
                {
                    var newGoalType = GoalTypes
                        .Where(g => g.IsValid(Submarine))
                        .OrderByDescending(g => g.Priority)
                        .FirstOrDefault();

                    if (newGoalType != null)
                    {
                        var newGoal = newGoalType.CreateGoal(Submarine);

                        if (newGoal != null)
                        {
                            _notificationSystem.Broadcast(Submarine, newGoal.MakeBroadcast());
                            _activeGoals.Push(newGoal);
                        }
                    }
                }
            }

            Submarine.Throttle = 0;
            Submarine.Dive = 0;
            Submarine.Rudder = 0;
            Submarine.DebugText = string.Empty;

            if (_activeGoals.Any())
            {
                Submarine.DebugText = _activeGoals.Peek().ToString();
                _activeGoals.Peek().Update(this, delta);
            }
        }
    }

    public void PromptGoalCheck()
    {
        _goalRecheckTimer = 0;
    }
}
