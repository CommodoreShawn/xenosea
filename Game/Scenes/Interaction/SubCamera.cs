using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class SubCamera : Camera
{
    public bool AimLock { get; set; } = false;
    public bool IsAiming { get; private set; }

    public float CameraYaw { get; set; }
    public float CameraPitch { get; set; } = Mathf.Pi / 2;
    public float CameraDistance { get; set; } = 50;

    private float _zoomDistSpeed = 500;
    private float _maxDist = 200;

    private float _fov = 70;
    private float _minFov = 05;
    private float _maxFov = 70;
    private float _zoomFovSpeed = 200;

    public bool InPeriscope { get; private set; }

    [Export]
    public float Sensitivity = 0.01f;

    [Export]
    public Environment OceanEnvironment;
    [Export]
    public Environment CaveEnvironment;

    private GameplayUi _gameplayUi;

    [Inject]
    private IPlayerData _playerData;

    private bool _useOriginRelativeUp;
    private Vector3 _upVector;

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();
        
        _gameplayUi = GetParent().GetNode<GameplayUi>("UI");

        _useOriginRelativeUp = this.FindParent<GameplayRoot>().UseOriginRelativeUp;
        _upVector = Vector3.Up;
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if (IsAiming && !_playerData.PlayerController.ControlLockout)
        {
            if (@event is InputEventMouseMotion mouseMotionEvt)
            {
                var zoomSlowFactor = 1f;
                if(InPeriscope)
                {
                    zoomSlowFactor = Fov / _maxFov;
                }

                CameraYaw += mouseMotionEvt.Relative.x * Sensitivity * zoomSlowFactor;

                CameraPitch = Mathf.Clamp(
                    CameraPitch - mouseMotionEvt.Relative.y * Sensitivity * zoomSlowFactor, 
                    0.2f * Mathf.Pi, 
                    0.8f * Mathf.Pi);
            }
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_useOriginRelativeUp)
        {
            _upVector = GlobalTransform.origin.Normalized();
        }

        if (_playerData.PlayerSub != null && !_playerData.PlayerController.ControlLockout)
        {
            if (Input.IsActionJustPressed("aim_toggle"))
            {
                AimLock = !AimLock;
            }

            if(_playerData.PlayerSub.IsDockedTo != null)
            {
                AimLock = false;
            }

            var wasAiming = IsAiming;

            IsAiming = AimLock || Input.IsMouseButtonPressed((int)ButtonList.Right);

            if (IsAiming && !wasAiming)
            {
                Input.MouseMode = Input.MouseModeEnum.Captured;
            }
            else if (!IsAiming && wasAiming)
            {
                Input.MouseMode = Input.MouseModeEnum.Visible;
            }

            if (_playerData.PlayerSub.IsDockedTo != null)
            {
                DoStationCamera(delta);
            }
            else
            {
                DoSubCamera(delta);
            }
        }
    }

    private void DoStationCamera(float delta)
    {
        var cityCenter = _playerData.PlayerSub.IsDockedTo.CameraCenter ?? (_playerData.PlayerSub.IsDockedTo as Spatial);

        if (Input.IsActionJustReleased("zoom_out"))
        {
            CameraDistance += delta * _zoomDistSpeed;
        }

        if (Input.IsActionJustReleased("zoom_in"))
        {
            CameraDistance -= delta * _zoomDistSpeed;
        }

        CameraDistance = Mathf.Clamp(
            CameraDistance, 
            _playerData.PlayerSub.IsDockedTo.MinCameraDistance, 
            _playerData.PlayerSub.IsDockedTo.MaxCameraDistance);

        var relativePos = new Vector3(
            CameraDistance * Mathf.Cos(CameraYaw) * Mathf.Sin(CameraPitch),
            CameraDistance * Mathf.Cos(CameraPitch),
            CameraDistance * Mathf.Sin(CameraYaw) * Mathf.Sin(CameraPitch));

        var worldTransform = Transform.Identity;
        if (_useOriginRelativeUp)
        {
            worldTransform = worldTransform.LookingAt(new Vector3(0, 1, 0), _upVector);
        }

        var newPos = cityCenter.GlobalTransform.origin
            + worldTransform.Xform(relativePos);

        LookAtFromPosition(newPos, cityCenter.GlobalTransform.origin, _upVector);
    }

    private void DoSubCamera(float delta)
    {
        if (_playerData.PlayerSub != null)
        {
            if (Input.IsActionJustPressed("zoom_togglebinocs"))
            {
                InPeriscope = !InPeriscope;

                if (!InPeriscope)
                {
                    Fov = _maxFov;
                    CameraDistance = Mathf.Max(CameraDistance, _playerData.PlayerSub.MinCameraDist + 1);
                }
            }

            if (_playerData.PlayerSub.IsWrecked)
            {
                InPeriscope = false;
                Fov = _maxFov;
                CameraDistance = _maxDist;
            }

            if (!InPeriscope)
            {
                if (Input.IsActionJustReleased("zoom_out"))
                {
                    CameraDistance += delta * _zoomDistSpeed;
                }

                if (Input.IsActionJustReleased("zoom_in"))
                {
                    CameraDistance -= delta * _zoomDistSpeed;
                }

                if (CameraDistance < _playerData.PlayerSub.MinCameraDist)
                {
                    InPeriscope = true;
                }

                CameraDistance = Mathf.Clamp(CameraDistance, _playerData.PlayerSub.MinCameraDist, _maxDist);

                var relativePos = new Vector3(
                        CameraDistance * Mathf.Cos(CameraYaw) * Mathf.Sin(CameraPitch),
                        CameraDistance * Mathf.Cos(CameraPitch),
                        CameraDistance * Mathf.Sin(CameraYaw) * Mathf.Sin(CameraPitch));

                var worldTransform = Transform.Identity;
                if (_useOriginRelativeUp)
                {
                    worldTransform = worldTransform.LookingAt(new Vector3(0, 1, 0), _upVector);
                }

                var newPos = _playerData.PlayerSub.CameraCenter.GlobalTransform.origin
                    + worldTransform.Xform(relativePos);

                LookAtFromPosition(newPos, _playerData.PlayerSub.CameraCenter.GlobalTransform.origin, _upVector);
            }
            else
            {
                if (Input.IsActionJustReleased("zoom_out"))
                {
                    _fov += delta * _zoomFovSpeed;
                }

                if (Input.IsActionJustReleased("zoom_in"))
                {
                    _fov -= delta * _zoomFovSpeed;
                }

                if (_fov > _maxFov)
                {
                    InPeriscope = false;
                }

                _fov = Mathf.Clamp(_fov, _minFov, _maxFov);

                Fov = _fov;

                GlobalTransform = _playerData.PlayerSub.CameraCenter.GlobalTransform;

                var relativePos = new Vector3(
                    1000 * Mathf.Cos(CameraYaw) * Mathf.Sin(CameraPitch),
                    1000 * Mathf.Cos(CameraPitch),
                    1000 * Mathf.Sin(CameraYaw) * Mathf.Sin(CameraPitch));

                var worldTransform = Transform.Identity;
                if (_useOriginRelativeUp)
                {
                    worldTransform = worldTransform.LookingAt(new Vector3(0, 1, 0), _upVector);
                }

                var lookPos = _playerData.PlayerSub.CameraCenter.GlobalTransform.origin
                    - worldTransform.Xform(relativePos);

                LookAtFromPosition(_playerData.PlayerSub.CameraCenter.GlobalTransform.origin, lookPos, _upVector);
            }

            if (!Input.IsActionPressed("ui_freelook"))
            {
                var aimDist = 3000f;

                if (_playerData.WeaponsSafe)
                {
                    _playerData.PlayerSub.AimPoint = 
                        _playerData.PlayerSub.CameraCenter.GlobalTransform.origin
                        + _playerData.PlayerSub.CameraCenter.GlobalTransform.basis.z * aimDist;
                }
                else
                {
                    if (_gameplayUi.CurrentTarget != null)
                    {
                        aimDist = _gameplayUi.CurrentTarget.EstimatedPosition.DistanceTo(_playerData.PlayerSub.GlobalTransform.origin);
                    }

                    _playerData.PlayerSub.AimPoint =
                        _playerData.PlayerSub.CameraCenter.GlobalTransform.origin
                        - GlobalTransform.basis.z * aimDist;
                }
            }
        }
    }
}
