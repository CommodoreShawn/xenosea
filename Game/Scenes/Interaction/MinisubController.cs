﻿using Xenosea.Logic.Detection;

public class MinisubController : GoalBotController
{
    public MinisubHangar Home { get; set; }
    public bool IsUndocking { get; set; }

    protected override double TurningPidGain => 1;
}
