using Godot;

public class SpawnParticles : Spatial
{
    [Export]
    public float BaseTimeToLive;

    private float _timeToLive;

    public override void _Ready()
    {
        _timeToLive = BaseTimeToLive;

        foreach(var child in GetChildren())
        {
            if(child is Particles particles)
            {
                particles.Emitting = true;

                _timeToLive = Mathf.Max(_timeToLive, particles.Lifetime);
            }
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _timeToLive -= delta;

        if(_timeToLive <= 0)
        {
            QueueFree();
        }
    }
}
