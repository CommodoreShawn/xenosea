using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;

public class JetBullet : RigidBody
{
    [Export]
    public PackedScene HitEffect;

    [Export]
    public float Thrust;

    [Export]
    public float BurnTime;

    [Export]
    public float Damage;

    [Export]
    public float Penetration;

    [Export]
    public float DragFactor;
    [Export]
    public float SideDragFactor;

    private bool _isDying;
    private float _timeToLive;
    public bool IsBurning { get; private set; }
    private RayCast _rayCast;
    public ICombatant Shooter { get; set; }

    [Inject]
    private IPerftracker _perfTracker;

    public override void _Ready()
    {
        _perfTracker = IocManager.Perftracker;
        IsBurning = true;
        _rayCast = GetNode<RayCast>("RayCast");
        if(Shooter is Submarine sub)
        {
            AddCollisionExceptionWith(sub);
        }
    }

    public override void _Process(float delta)
    {
        var stopwatch = System.Diagnostics.Stopwatch.StartNew();
        base._Process(delta);

        if(IsBurning)
        {
            BurnTime -= delta;

            if(BurnTime <= 0)
            {
                IsBurning = false;
                GetNode<Particles>("JetTrail").Emitting = false;

                GetNode<AudioStreamPlayer3D>("RocketSoundPlayer").Playing = false;
            }
        }

        if(_isDying)
        {
            _timeToLive -= delta;

            if(_timeToLive <= 0)
            {
                QueueFree();
            }
        }

        if (!_isDying && LinearVelocity.Length() < 6 && !IsBurning)
        {
            _isDying = true;

            var bubbles = GetNodeOrNull<Particles>("BubbleTrail");
            if (bubbles != null)
            {
                bubbles.Emitting = false;
                _timeToLive = bubbles.Lifetime;
            }
        }

        stopwatch.Stop();
        _perfTracker.LogPerf("JetBullet._Process", stopwatch.ElapsedMilliseconds);
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        if(IsBurning)
        {
            AddCentralForce(Transform.basis.Xform(new Vector3(0, 0, Thrust)));
        }

        // Custom Drag Logic
        var tansformedVel = Transform.basis.XformInv(LinearVelocity);
        AddCentralForce(Transform.basis.Xform(
            new Vector3(
                -tansformedVel.x * SideDragFactor,
                -tansformedVel.y * SideDragFactor,
                -tansformedVel.z * DragFactor)));
    }

    public void Collided(Node other)
    {
        if (HitEffect != null)
        {
            var hitEffect = HitEffect.Instance() as Spatial;
            hitEffect.GlobalTransform = GlobalTransform;
            GetParent().AddChild(hitEffect);
        }

        if (other is ICombatant combatant && combatant != Shooter)
        {
            var hitPoint = GlobalTransform.origin;

            if (_rayCast.IsColliding())
            {
                hitPoint = _rayCast.GetCollisionPoint();
            }

            combatant.HitRecieved(
                Shooter,
                Damage,
                Penetration,
                hitPoint,
                LinearVelocity);
        }

        var bubbles = GetNodeOrNull<Particles>("BubbleTrail");
        if (bubbles != null)
        {
            bubbles.Emitting = false;
            var timedEffect = new TimedEffect()
            {
                TimeToLive = bubbles.Lifetime
            };
            timedEffect.GlobalTransform = GlobalTransform;
            RemoveChild(bubbles);
            timedEffect.AddChild(bubbles);
            GetParent().AddChild(timedEffect);
        }

        QueueFree();
    }
}
