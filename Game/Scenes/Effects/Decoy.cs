using Godot;
using System;
using System.Collections.Generic;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

public class Decoy : RigidBody, ISonarContact
{
    [Export]
    public float ActivationDelay;
    [Export]
    public float DecoyTime;
    [Export]
    public float Noise;
    [Export]
    public Faction Faction { get; set; }

    public string ReferenceId => Id.ToString();

    private Particles _bubbles;
    private AudioStreamPlayer3D _staticNoise;
    private CollisionShape _sonarBlockerShape;
    private MeshInstance _meshInstance;
    private Particles _bubbleTrail;

    private bool _isWaiting;
    private bool _isActive;
    private bool _isCooldown;

    private float _counter;
    private GameplayRoot _gameplayRoot;
    private Vector3 _upVector;
    [Inject]
    private readonly ISonarContactTracker _sonarContactTracker;

    public ulong Id { get; private set; }
    public string FriendlyName => "Decoy";
    public Vector3 RealPosition => GlobalTransform.origin;
    public float OverallNoise => _isActive ? Noise : 0;
    public float BracketSizeFactor => 1;
    public bool IsPlayerVisible { get; set; }
    public ContactType ContactType => ContactType.Unknown;
    
    public List<TemporaryNoise> TemporaryNoises { get; private set; }
    public bool IsDockable => false;
    public Vector3 Velocity => LinearVelocity;

    public float SpeakingTimer { get; set; }
    public string DebugText { get; set; }

    public override void _Ready()
    {
        Id = GetInstanceId();
        this.ResolveDependencies();
        TemporaryNoises = new List<TemporaryNoise>();

        _bubbles = GetNode<Particles>("Bubbles");
        _staticNoise = GetNode<AudioStreamPlayer3D>("StaticNoise");
        _sonarBlockerShape = GetNode<CollisionShape>("Area/SonarBlockerShape");
        _meshInstance = GetNode<MeshInstance>("MeshInstance");
        _bubbleTrail= GetNode<Particles>("BubbleTrail");

        _isWaiting = true;

        _bubbles.Emitting = false;
        _staticNoise.Playing = false;
        _sonarBlockerShape.Disabled = true;
        _sonarContactTracker.AddSonarContact(this);

        _gameplayRoot = this.FindParent<GameplayRoot>();
        _upVector = Vector3.Up;
    }

    public override void _ExitTree()
    {
        base._ExitTree();
        _sonarContactTracker.RemoveSonarContact(this);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        _counter += delta;

        if(_isWaiting && _counter > ActivationDelay)
        {
            _counter = 0;
            _isWaiting = false;
            _isActive = true;
            _bubbles.Emitting = true;
            _staticNoise.Playing = true;
            _sonarBlockerShape.Disabled = false;
            _bubbleTrail.Emitting = false;
            _meshInstance.Visible = false;
        }
        else if(_isActive && _counter > DecoyTime)
        {
            _counter = 0;
            _isActive = false;
            _isCooldown = true;
            _bubbles.Emitting = false;
            _staticNoise.Playing = false;
            _sonarBlockerShape.Disabled = true;
        }
        else if(_isCooldown && _counter > _bubbles.Lifetime)
        {
            QueueFree();
        }

        if (_gameplayRoot.UseOriginRelativeUp)
        {
            _upVector = GlobalTransform.origin.Normalized();
        }
    }

    public bool IsHostileTo(ISonarContact other)
    {
        return false;
    }

    public bool IsFriendlyTo(ISonarContact other)
    {
        return false;
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        if (_gameplayRoot is Gameplay gameplay)
        {
            var currentFactor = 1f;

            var collisionInfo = GetWorld().DirectSpaceState
                .IntersectRay(
                    GlobalTransform.origin,
                    GlobalTransform.origin + _upVector * -1000,
                    null,
                    0b00000000000000000010,
                    collideWithBodies: true,
                    collideWithAreas: true);

            if (collisionInfo.Count > 0)
            {
                currentFactor = Mathf.Clamp(
                    (GlobalTransform.origin.DistanceTo((Vector3)collisionInfo["position"]) - 100) / 300,
                    0, 1);
            }

            AddCentralForce(gameplay.GetCurrentFlow(GlobalTransform.origin) * Mass * currentFactor);
        }
    }
}
