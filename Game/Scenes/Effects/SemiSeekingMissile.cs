using CSPID;
using Godot;
using System.Diagnostics;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;

public class SemiSeekingMissile : JetBullet
{
    [Export]
    public float Torque;
    [Export]
    public float SeekerAngle = 3.14f;

    public SonarContactRecord Target { get; set; }

    private IPerftracker _perfTracker;

    private PIDController _yawPid;
    private PIDController _pitchPid;

    private Vector3 _upVector;
    private GameplayRoot _gameplayRoot;

    public override void _Ready()
    {
        base._Ready();
        _perfTracker = IocManager.Perftracker;
        _yawPid = new PIDController(
            minimumError: -4,
            maximumError: 4,
            minimumControl: -3,
            maximumControl: 3)
        {
            MaximumStep = double.MaxValue,
            ProportionalGain = 1,
            IntegralGain = 0,
            DerivativeGain = 0
        };
        _pitchPid = new PIDController(
            minimumError: -4,
            maximumError: 4,
            minimumControl: -3,
            maximumControl: 3)
        {
            MaximumStep = double.MaxValue,
            ProportionalGain = 1,
            IntegralGain = 0,
            DerivativeGain = 0
        };

        _gameplayRoot = this.FindParent<GameplayRoot>();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        var stopwatch = Stopwatch.StartNew();

        _upVector = GlobalTransform.origin.Normalized();

        if(Target != null && Target.IsValidInstance)
        {
            var angle = GlobalTransform.basis.z.AngleTo(Target.EstimatedPosition - GlobalTransform.origin);
            if(angle > SeekerAngle)
            {
                Target = null;
            }
        }

        if(float.IsNaN(GlobalTransform.origin.x))
        {
            Free();
        }

        stopwatch.Stop();
        _perfTracker.LogPerf("SemiSeekingMissile._Process", stopwatch.ElapsedMilliseconds);
    }

    public override void _PhysicsProcess(float delta)
    {
        if (delta > 0)
        {
            var stopwatch = Stopwatch.StartNew();

            if (Target != null && Target.IsValidInstance && IsBurning)
            {
                var speed = Mathf.Max(20, LinearVelocity.Length());
                var timeToReach = GlobalTransform.origin.DistanceTo(Target.EstimatedPosition) / speed;
                var leadPoint = Target.EstimatedPosition + Target.LastKnownVelocity * timeToReach;
                var targetVector = leadPoint - GlobalTransform.origin;
                //var targetVector = Target.EstimatedPosition - GlobalTransform.origin;

                var yawError = GlobalTransform.basis.Xform(Vector3.Back).SignedAngleTo(targetVector, Transform.basis.y);
                var yaw = (float)_yawPid.Next(error: yawError, elapsed: delta);

                var pitchError = GlobalTransform.basis.Xform(Vector3.Back).SignedAngleTo(targetVector, Transform.basis.x);
                var pitch = (float)_pitchPid.Next(error: pitchError, elapsed: delta);

                var currentBasis = GlobalTransform.basis;

                var xyPlane = new Plane(currentBasis.z, 0);
                var projectedY = xyPlane.Project(_upVector).Normalized();
                var roll = projectedY.AngleTo(currentBasis.y);
                roll *= Mathf.Sign(projectedY.Dot(currentBasis.x));

                //var yzPlane = new Plane(currentBasis.x, 0);
                //projectedY = yzPlane.Project(_upVector).Normalized();
                //var correctivePitch = projectedY.AngleTo(currentBasis.y);
                //correctivePitch *= Mathf.Sign(projectedY.Dot(currentBasis.z));

                //GD.Print($"PITCH: Corr: {correctivePitch} Steer {pitch} Result: {correctivePitch - pitch} ");

                //pitch = 0;
                //yaw = 0.1f;
                roll = 0;

                AddTorque(currentBasis.Xform(new Vector3(
                    pitch * Torque,
                    yaw * Torque,
                    -roll * Torque)));
            }

            stopwatch.Stop();
            _perfTracker.LogPerf("SemiSeekingMissile._PhysicsProcess", stopwatch.ElapsedMilliseconds);
        }

        base._PhysicsProcess(delta);
    }
}
