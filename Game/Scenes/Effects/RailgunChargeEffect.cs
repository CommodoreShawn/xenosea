using Godot;

public class RailgunChargeEffect : Spatial
{
    private Particles _bubbles;

    [Export]
    public float ChargeTime;

    private float _chargeTimer;
    private float _dieTimer;
    
    public override void _Ready()
    {
        _bubbles = GetNode<Particles>("Bubbles");
        _dieTimer = _bubbles.Lifetime;
        _chargeTimer = ChargeTime;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_chargeTimer > 0)
        {
            _chargeTimer -= delta;

            if(_chargeTimer <= 0)
            {
                _bubbles.Emitting = false;
            }
        }
        else
        {
            _dieTimer -= delta;

            if(_dieTimer <= 0)
            {
                QueueFree();
            }
        }
    }
}
