using Godot;
using System;
using System.Linq;
using Xenosea.Logic.Detection;

public class HydraTorpedo : Torpedo
{
    [Export]
    public float LaunchDistance;

    [Export]
    public float LaunchDelay;

    [Export]
    public int LaunchCount;

    [Export]
    public PackedScene SubtorpPrefab;

    private Spatial[] _muzzles;
    private bool _isLaunching;
    private float _delayCounter;
    private int _nextMuzzle;
    private Random _random;
    private int _subtorpsLaunched;

    public override void _Ready()
    {
        base._Ready();

        _random = new Random();

        _muzzles = GetNode<Spatial>("Muzzles")
            .GetChildren()
            .OfType<Spatial>()
            .ToArray();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        
        if(_isLaunching)
        {
            _delayCounter -= delta;

            if(_delayCounter <= 0)
            {
                _delayCounter = LaunchDelay;

                var subtorp = SubtorpPrefab.Instance() as Torpedo;
                subtorp.GlobalTransform = _muzzles[_nextMuzzle].GlobalTransform;

                GetParent().AddChild(subtorp);

                subtorp.LinearVelocity = LinearVelocity;
                subtorp.Shooter = Shooter;
                subtorp.AddCollisionExceptionWith(this);
                subtorp.Target = Target;

                _nextMuzzle = (_nextMuzzle + 1) % _muzzles.Length;
                _subtorpsLaunched += 1;

                if (_subtorpsLaunched >= LaunchCount)
                {
                    QueueFree();
                }
            }
        }
    }

    protected override void OnTracking(ISonarContact target)
    {
        base.OnTracking(target);

        if(!_isLaunching
            && !InSteeringDelay
            && GlobalTransform.origin.DistanceTo(target.RealPosition) < LaunchDistance)
        {
            _isLaunching = true;
        }
    }

    protected override void OnTracking(ITargetableThing target)
    {
        base.OnTracking(target);

        if (!_isLaunching
            && !InSteeringDelay
            && GlobalTransform.origin.DistanceTo(target.EstimatedPosition) < LaunchDistance)
        {
            _isLaunching = true;
        }
    }
}
