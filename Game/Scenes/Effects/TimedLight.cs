using Godot;
using System;

public class TimedLight : OmniLight
{
    [Export]
    public float TimeToLight;

    private float _timeRemaining;

    public override void _Ready()
    {
        base._Ready();

        _timeRemaining = TimeToLight;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        _timeRemaining -= delta;

        if(_timeRemaining > 0 && TimeToLight > 0)
        {
            LightEnergy = _timeRemaining / TimeToLight;
        }


        Visible = _timeRemaining > 0;
    }
}
