using Godot;
using System.Collections.Generic;
using Xenosea.Logic;

public class TowedArray : RigidBody
{
    private const int _lineSegmentCountMax = 15;
    private const float _lineSegmentLengthMax = 7;
    private const float _extensionRate = 5;

    [Export]
    public PackedScene CableSegmentPrefab;

    private Spatial _towHook;

    private List<Vector3> _lineSegmentPoints;
    private List<float> _lineSegmentLengths;
    private GameplayRoot _gameplayRoot;
    private List<Spatial> _cableSegments;

    public Submarine Submarine { get; set; }

    public bool FullyExtended { get; private set; }
    public bool Unstable { get; private set; }
    public bool TooFast { get; private set; }

    private float _damageGraceTime;

    public override void _Ready()
    {
        _towHook = GetNode<Spatial>("TowHook");
        _lineSegmentLengths = new List<float>();
        _lineSegmentPoints = new List<Vector3>();
        _cableSegments = new List<Spatial>();
        _gameplayRoot = this.FindParent<GameplayRoot>();
        _damageGraceTime = 15;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        FullyExtended = false;

        if (Submarine.DeployTowedArray)
        {
            if(Submarine.Velocity.Length() > 20)
            {
                TooFast = true;
                _damageGraceTime -= delta;

                if(_damageGraceTime<= 0)
                {
                    Submarine.TowedArrayRepairTimer = Mathf.Max(Submarine.TowedArrayRepairTimer, 90);
                }
            }
            else
            {
                TooFast = false;
                _damageGraceTime = Mathf.Min(_damageGraceTime + delta, 15);
            }

            if (_lineSegmentLengths.Count > 0 && _lineSegmentLengths[0] < _lineSegmentLengthMax)
            {
                _lineSegmentLengths[0] = Mathf.Min(_lineSegmentLengths[0] + delta * _extensionRate, _lineSegmentLengthMax);
            }
            else if(_lineSegmentPoints.Count < _lineSegmentCountMax)
            {
                _lineSegmentLengths.Insert(0, 0);
                _lineSegmentPoints.Insert(0, Submarine.TowHook.GlobalTransform.origin);

                var newCableSegment = CableSegmentPrefab.Instance() as Spatial;
                _cableSegments.Add(newCableSegment);
                AddChild(newCableSegment);
            }
            else
            {
                FullyExtended = true;
            }
        }
        else
        {
            if(_lineSegmentPoints.Count > 0)
            {
                _lineSegmentLengths[0] = _lineSegmentLengths[0] - delta * _extensionRate;

                if (_lineSegmentLengths[0] <= 0)
                {
                    _cableSegments[0].QueueFree();
                    _cableSegments.RemoveAt(0);
                    _lineSegmentLengths.RemoveAt(0);
                    _lineSegmentPoints.RemoveAt(0);
                }
            }
            else
            {
                Submarine.TowedArray = null;
                QueueFree();
            }
        }

        for(var i = 0; i < _lineSegmentLengths.Count; i++)
        {
            Vector3 previousPoint;
            if(i == 0)
            {
                previousPoint = Submarine.TowHook.GlobalTransform.origin;
            }
            else
            {
                previousPoint = _lineSegmentPoints[i - 1];
            }

            var fromTo = _lineSegmentPoints[i] - previousPoint;

            if(fromTo.Length() > _lineSegmentLengths[i])
            {
                _lineSegmentPoints[i] = fromTo.Normalized() * _lineSegmentLengths[i] + previousPoint;

                var midpoint = (_lineSegmentPoints[i] - previousPoint) / 2 + previousPoint;
                _cableSegments[i].LookAtFromPosition(midpoint, previousPoint, Submarine.UpVector);
                _cableSegments[i].Scale = new Vector3(1, 1, (_lineSegmentPoints[i] - previousPoint).Length());
            }
        }

        Unstable = GlobalTransform.basis.z.AngleTo(Submarine.GlobalTransform.basis.z) < 3.0f;

        if (_lineSegmentPoints.Count > 1)
        {
            var lastIndex = _lineSegmentPoints.Count - 1;

            LookAtFromPosition(
                _lineSegmentPoints[lastIndex],
                _lineSegmentPoints[lastIndex - 1],
                Submarine.UpVector);
        }
        else if (_lineSegmentPoints.Count > 0)
        {
            LookAtFromPosition(
                _lineSegmentPoints[_lineSegmentPoints.Count - 1],
                Submarine.TowHook.GlobalTransform.origin,
                Submarine.UpVector);
        }
    }
}
