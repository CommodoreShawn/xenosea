using Godot;
using System;
using System.Linq;
using Xenosea.Logic.Detection;

public class BarrageTorpedo : Torpedo
{
    [Export]
    public float ShootDistance;

    [Export]
    public float RocketShootDelay;

    [Export]
    public int RocketCount;

    [Export]
    public float RocketVariance;

    [Export]
    public PackedScene RocketPrefab;

    private Spatial[] _muzzles;
    private bool _isShooting;
    private float _delayCounter;
    private int _nextMuzzle;
    private Random _random;
    private int _shotsFired;
    private Vector3 _targetPos;

    public override void _Ready()
    {
        base._Ready();

        _random = new Random();

        _muzzles = GetNode<Spatial>("Muzzles")
            .GetChildren()
            .OfType<Spatial>()
            .ToArray();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        
        if(_isShooting)
        {
            _delayCounter -= delta;

            if(_delayCounter <= 0)
            {
                _delayCounter = RocketShootDelay;

                var rocket = RocketPrefab.Instance() as JetBullet;
                
                GetParent().AddChild(rocket);

                rocket.LookAtFromPosition(_muzzles[_nextMuzzle].GlobalTransform.origin, _targetPos, UpVector);
                rocket.RotateObjectLocal(UpVector, Mathf.Pi);

                rocket.RotateX((float)_random.NextDouble() * RocketVariance);
                rocket.RotateY((float)_random.NextDouble() * RocketVariance);
                rocket.RotateZ((float)_random.NextDouble() * RocketVariance);
                rocket.LinearVelocity = LinearVelocity;
                rocket.Shooter = Shooter;
                rocket.AddCollisionExceptionWith(this);

                _nextMuzzle = (_nextMuzzle + 1) % _muzzles.Length;
                _shotsFired += 1;

                if (_shotsFired >= RocketCount)
                {
                    QueueFree();
                }
            }
        }
    }

    protected override void OnTracking(ISonarContact target)
    {
        base.OnTracking(target);

        if(target != null
            && GlobalTransform.origin.DistanceTo(target.RealPosition) < ShootDistance)
        {
            _isShooting = true;
            _targetPos = target.RealPosition;
        }
    }

    protected override void OnTracking(ITargetableThing target)
    {
        base.OnTracking(target);

        if (target != null
            && GlobalTransform.origin.DistanceTo(target.EstimatedPosition) < ShootDistance)
        {
            _isShooting = true;
            _targetPos = target.EstimatedPosition;
        }
    }
}
