﻿using Godot;

public class SpotlightController
{
    public bool LightOn { get; private set; }

    private bool _turningOn;
    private bool _inFlicker;
    private float _sequenceTimer;
    private float _flickerTime;
    private System.Random _rnd;

    public SpotlightController()
    {
        _rnd = new System.Random();
    }

    public void SetTargetState(bool lightOn)
    {
        if(lightOn != _turningOn)
        {
            _turningOn = lightOn;
            _inFlicker = true;
            _sequenceTimer = (float)_rnd.NextDouble() * 1.5f;
            _flickerTime = (float)_rnd.NextDouble() * 0.1f;
        }
    }

    public void Update(float delta)
    {
        if(_inFlicker)
        {
            _sequenceTimer -= delta;
            _flickerTime -= delta;

            if(_sequenceTimer <= 0)
            {
                _inFlicker = false;
                LightOn = _turningOn;
            }
            else if(_flickerTime <= 0)
            {
                _flickerTime = (float)_rnd.NextDouble() * 0.1f;
                LightOn = !LightOn;
            }
        }
    }
}
