using Godot;
using System;

public class TimedEffect : Spatial
{
    [Export]
    public float TimeToLive { get; set; }

    public override void _Process(float delta)
    {
        base._Process(delta);

        TimeToLive -= delta;
        
        if(TimeToLive <= 0)
        {
            QueueFree();
        }
    }
}
