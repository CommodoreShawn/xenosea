using Godot;
using Xenosea.Logic;

public class HangarSpotlight : SpotLight
{
    private Submarine _submarine;
    private SpotlightController _spotlightController;

    public override void _Ready()
    {
        _submarine = this.FindParent<Submarine>();
        _spotlightController = new SpotlightController();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_submarine != null)
        {
            if (_submarine.IsWrecked
                || _submarine.IsDockedTo != null)
            {
                _spotlightController.SetTargetState(false);
            }
            else if (_submarine.HangarDoorsOpen)
            {
                _spotlightController.SetTargetState(true);
            }
            else
            {
                _spotlightController.SetTargetState(false);
            }
        }

        _spotlightController.Update(delta);
        Visible = _spotlightController.LightOn;
    }
}
