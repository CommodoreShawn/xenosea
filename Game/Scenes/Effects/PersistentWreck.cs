﻿using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

public class PersistentWreck : StaticBody, ISalvageTarget, ISonarContact
{
    [Export]
    public Faction Faction { get; set; }
    [Export]
    public string FriendlyName { get; set; }
    [Export]
    public float BracketSizeFactor { get; set; }
    [Export]
    public Texture TypeIcon { get; set; }
    [Export]
    public float OverallNoise { get; set; }
    [Export]
    public CommodityQuantity[] WreckCommodities;

    public Vector3 RealPosition => GlobalTransform.origin;
    public Vector3 Velocity => Vector3.Zero;
    public bool CanBeSalvaged => CommodityStorage.CommoditiesPresent.Where(c => CommodityStorage.GetQty(c) >= 1).Any();
    public CommodityStorage CommodityStorage { get; set; }
    public ulong Id { get; private set; }
    public bool IsPlayerVisible { get; set; }
    public ContactType ContactType => ContactType.Wreck;
    public List<TemporaryNoise> TemporaryNoises { get; private set; }
    public bool IsDockable => false;

    public string ReferenceId => Id.ToString();

    public float SpeakingTimer { get; set; }
    public string DebugText { get; set; }

    [Inject]
    private ISonarContactTracker _sonarContactTracker;

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();

        Id = GetInstanceId();
        CommodityStorage = new CommodityStorage(float.MaxValue, false);

        foreach (var commodityQuantity in WreckCommodities)
        {
            CommodityStorage.Add(commodityQuantity.Commodity, commodityQuantity.Quantity);
        }

        _sonarContactTracker.AddSonarContact(this);
    }

    public override void _ExitTree()
    {
        base._ExitTree();
        _sonarContactTracker.RemoveSonarContact(this);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        Visible = IsPlayerVisible;
    }

    public bool IsHostileTo(ISonarContact other)
    {
        return false;
    }

    public bool IsFriendlyTo(ISonarContact other)
    {
        return false;
    }
}