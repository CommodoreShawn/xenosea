using Autofac;
using CSPID;
using Godot;
using System.Diagnostics;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

public class Torpedo : RigidBody, ISonarContact
{
    [Export]
    public PackedScene HitEffect;

    [Export]
    public float Thrust;

    [Export]
    public float Torque;

    [Export]
    public float BurnTime;

    [Export]
    public float Damage;

    [Export]
    public float Penetration;

    [Export]
    public float TurningThrottleMin;

    [Export]
    public float FrontDragFactor;
    [Export]
    public float SideDragFactor;
    [Export]
    public float EngineNoise = 0.7f;

    private float SeekerAngle = Mathf.Pi / 4;

    private RayCast _rayCast;
    public ICombatant Shooter { get; set; }

    public Vector3 TargetPoint { get; set; }
    public ITargetableThing Target { get; set; }
    public bool OnWire { get; set; }
    public bool ActiveSeeking { get; set; }

    private ISonarContactTracker _sonarContactTracker;
    private IPerftracker _perfTracker;

    private PIDController _yawPid;
    private PIDController _altitudePid;
    
    [Export]
    public float MaxSteeringDelay = 5;
    private float _steeringDelay;
    private bool _useOriginRelativeUp;
    public Vector3 UpVector { get; private set; }
    private GameplayRoot _gameplayRoot;

    private AudioStreamPlayer3D _sonarPingPlayer;
    private float _sonarPingTimer;
    private bool _hadTarget;

    public bool InSteeringDelay => _steeringDelay > 0;


    public string ReferenceId { get; private set; }
    public ulong Id { get; private set; }
    [Export]
    public string FriendlyName { get; set; }
    public Vector3 RealPosition => GlobalTransform.origin;
    public float OverallNoise { get; set; }
    public float BracketSizeFactor { get; } = 50;
    public bool IsPlayerVisible { get; set; }
    public ContactType ContactType => ContactType.Torpedo;
    public Faction Faction => Shooter?.Faction;
    public bool IsDockable => false;
    public Vector3 Velocity => LinearVelocity;
    public float SpeakingTimer { get; set; }
    public string DebugText { get; set; }

    public override void _Ready()
    {
        Id = GetInstanceId();
        ReferenceId = GetInstanceId().ToString();

        _steeringDelay = MaxSteeringDelay;
        var stopwatch = System.Diagnostics.Stopwatch.StartNew();
        _perfTracker = IocManager.IocContainer.Resolve<IPerftracker>();
        _sonarContactTracker = IocManager.IocContainer.Resolve<ISonarContactTracker>();

        _sonarContactTracker.AddSonarContact(this);

        _sonarPingPlayer = GetNodeOrNull<AudioStreamPlayer3D>("SonarPingPlayer");

        _rayCast = GetNode<RayCast>("RayCast");
        _yawPid = new PIDController(
            minimumError: -4,
            maximumError: 4,
            minimumControl: -3,
            maximumControl: 3)
        {
            MaximumStep = double.MaxValue,
            ProportionalGain = 3,
            IntegralGain = 0,
            DerivativeGain = 0
        };

        _altitudePid = new PIDController(
            minimumError: -300,
            maximumError: 300,
            minimumControl: -0.5f,
            maximumControl: 0.5f)
        {
            MaximumStep = double.MaxValue,
            ProportionalGain = 3,
            IntegralGain = 0,
            DerivativeGain = 0
        };

        _gameplayRoot = this.FindParent<GameplayRoot>();

        _useOriginRelativeUp = this.FindParent<GameplayRoot>().UseOriginRelativeUp;
        UpVector = Vector3.Up;

        stopwatch.Stop();
        _perfTracker.LogPerf("Torpeo._Ready", stopwatch.ElapsedMilliseconds);
    }

    public override void _ExitTree()
    {
        base._ExitTree();
        _sonarContactTracker.RemoveSonarContact(this);
    }

    public override void _Process(float delta)
    {
        var stopwatch = Stopwatch.StartNew();
        base._Process(delta);

        if (_useOriginRelativeUp)
        {
            UpVector = GlobalTransform.origin.Normalized();
        }

        Visible = IsPlayerVisible;

        _steeringDelay -= delta;

        BurnTime -= delta;

        if (BurnTime <= 0)
        {
            var hitEffect = HitEffect.Instance() as Spatial;
            hitEffect.GlobalTransform = GlobalTransform;
            GetParent().AddChild(hitEffect);
            ReparentBubbles();
            QueueFree();
            return;
        }

        var isHoming = false;

        if (ActiveSeeking || !OnWire || !IsInstanceValid(Shooter as Object) || Shooter.IsWrecked)
        {
            var loudestContactNoise = 5f;
            ISonarContact loudestContact = null;
            foreach (var contact in _sonarContactTracker.GetSonarContacts())
            {
                var detectorGain = 1f;

                var distance = (GlobalTransform.origin - contact.RealPosition).Length();

                var detectedNoise = contact.OverallNoise / (4 * Mathf.Pi * Mathf.Pow(Constants.SONAR_ENV_PROPAGATION * distance, 2)) * detectorGain;

                var angle = GlobalTransform.basis.z.AngleTo(contact.RealPosition - GlobalTransform.origin);

                if (detectedNoise > loudestContactNoise 
                    && angle < SeekerAngle
                    && contact.ContactType != ContactType.Wreck
                    && contact.ContactType != ContactType.Torpedo)
                {
                    loudestContactNoise = detectedNoise;
                    loudestContact = contact;
                }
            }

            if (loudestContact != null)
            {
                TargetPoint = loudestContact.RealPosition;

                OnTracking(loudestContact);
                isHoming = true;
                _hadTarget = true;
            }
        }
        else if (Target != null)
        {
            if (!OnWire || !Target.IsDetected)
            {
                Target = null;
            }
            else
            {
                TargetPoint = Target.EstimatedPosition;
                isHoming = true;
            }

            OnTracking(Target);
        }

        if(isHoming && _sonarPingPlayer != null)
        {
            if(!_sonarPingPlayer.Playing)
            {
                _sonarPingTimer += delta;

                var delay = RealPosition.DistanceTo(TargetPoint) / 500;

                if(_sonarPingTimer >= delay)
                {
                    _sonarPingTimer = 0;
                    _sonarPingPlayer.Play();
                }
            }
        }

        if(float.IsNaN(GlobalTransform.origin.x))
        {
            Free();
        }

        stopwatch.Stop();
        _perfTracker.LogPerf("Torpedo._Process", stopwatch.ElapsedMilliseconds);
    }

    protected virtual void OnTracking(ITargetableThing target)
    {
    }

    protected virtual void OnTracking(ISonarContact target)
    {
    }

    public override void _PhysicsProcess(float delta)
    {
        var stopwatch = Stopwatch.StartNew();
        base._PhysicsProcess(delta);

        if(delta == 0)
        {
            return;
        }

        var waterCurrentVelocity = Vector3.Zero;
        if (_gameplayRoot is Gameplay gameplay)
        {
            var currentFactor = 1f;

            var collisionInfo = GetWorld().DirectSpaceState
                .IntersectRay(
                    GlobalTransform.origin,
                    GlobalTransform.origin + UpVector * -1000,
                    null,
                    0b00000000000000000010,
                    collideWithBodies: true,
                    collideWithAreas: true);

            if (collisionInfo.Count > 0)
            {
                currentFactor = Mathf.Clamp(
                    (GlobalTransform.origin.DistanceTo((Vector3)collisionInfo["position"]) - 100) / 300,
                    0, 1);
            }

            waterCurrentVelocity = gameplay.GetCurrentFlow(GlobalTransform.origin) * currentFactor;
        }

        var targetVector = TargetPoint - RealPosition;


        var yawError = Transform.basis.Xform(Vector3.Back).SignedAngleTo(targetVector, Transform.basis.y);

        var yaw = (float)_yawPid.Next(error: yawError, elapsed: delta);
        var altitudeError = TargetPoint.Length() - RealPosition.Length();
        var pitch = (float)_altitudePid.Next(error: altitudeError, elapsed: delta);

        if(_steeringDelay > 0)
        {
            var steeringDamp = 1 - _steeringDelay / MaxSteeringDelay;

            yaw *= steeringDamp;
            pitch *= steeringDamp;
            yawError *= steeringDamp;
        }

        var currentBasis = Transform.basis;

        var xyPlane = new Plane(currentBasis.z, 0);
        var projectedY = xyPlane.Project(UpVector).Normalized();
        var roll = projectedY.AngleTo(currentBasis.y);
        roll *= Mathf.Sign(projectedY.Dot(currentBasis.x));

        var yzPlane = new Plane(currentBasis.x, 0);
        projectedY = yzPlane.Project(UpVector).Normalized();
        var correctivePitch = projectedY.AngleTo(currentBasis.y);
        correctivePitch *= Mathf.Sign(projectedY.Dot(currentBasis.z));

        //GD.Print($"PITCH: Corr: {correctivePitch} Steer {pitch} Result: {correctivePitch - pitch} ");

        AddTorque(currentBasis.Xform(new Vector3(
            (correctivePitch - pitch) * Torque,
            yaw * Torque,
            -roll * Mass * 5))); ;

        var throttle = Mathf.Clamp(0.5f - Mathf.Abs(yawError) / 1.5f + 0.5f, TurningThrottleMin, 1f);

        OverallNoise = throttle * EngineNoise * Thrust;

        if (FrontDragFactor != 0 && SideDragFactor != 0)
        {
            // Custom Drag Logic
            var tansformedVel = Transform.basis.XformInv(LinearVelocity - waterCurrentVelocity);

            AddCentralForce(Transform.basis.Xform(
                new Vector3(
                    -tansformedVel.x * SideDragFactor / Engine.TimeScale,
                    -tansformedVel.y * SideDragFactor / Engine.TimeScale,
                    -tansformedVel.z * FrontDragFactor + throttle * Thrust)));
        }

        stopwatch.Stop();
        _perfTracker.LogPerf("Torpedo._PhysicsProcess", stopwatch.ElapsedMilliseconds);
    }

    public void Collided(Node other)
    {
        var hitEffect = HitEffect.Instance() as Spatial;
        hitEffect.GlobalTransform = GlobalTransform;
        GetParent().AddChild(hitEffect);

        if (other is ICombatant combatant && combatant != Shooter)
        {
            var hitPoint = GlobalTransform.origin;

            if (_rayCast.IsColliding())
            {
                hitPoint = _rayCast.GetCollisionPoint();
            }

            combatant.HitRecieved(
                Shooter,
                Damage,
                Penetration,
                hitPoint,
                LinearVelocity);
        }

        ReparentBubbles();

        QueueFree();
    }

    private void ReparentBubbles()
    {
        try
        {
            var bubbles = GetNodeOrNull<Particles>("BubbleTrail");

            if (bubbles != null)
            {
                bubbles.Emitting = false;
                var timedEffect = new TimedEffect()
                {
                    TimeToLive = bubbles.Lifetime
                };
                timedEffect.GlobalTransform = GlobalTransform;
                RemoveChild(bubbles);
                timedEffect.AddChild(bubbles);
                GetParent().AddChild(timedEffect);
            }
        }
        catch (System.Exception)
        {
        }
    }

    public bool IsHostileTo(ISonarContact other)
    {
        return !OnWire || Faction.IsHostileTo(other.Faction);
    }

    public bool IsFriendlyTo(ISonarContact other)
    {
        return OnWire && Faction.IsFriendlyTo(other.Faction);
    }
}
