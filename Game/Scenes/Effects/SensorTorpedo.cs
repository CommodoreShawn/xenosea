﻿using Godot;
using Xenosea.Logic.Infrastructure.Interfaces;

public class SensorTorpedo : Torpedo
{
    [Export]
    public float SonarSensitivity = 1;
}
