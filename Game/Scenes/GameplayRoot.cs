using Autofac;
using Godot;
using System;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

public class GameplayRoot : Spatial
{
    private IWorldSimulator _worldSimulator;
    private IPlayerData _playerData;

    [Export]
    public float StandardWreckLifespan = 120;
    [Export]
    public bool UseOriginRelativeUp;
    [Export]
    public float CeilingLevel;

    public override void _Ready()
    {
        _worldSimulator = IocManager.IocContainer.Resolve<IWorldSimulator>();
        _playerData = IocManager.IocContainer.Resolve<IPlayerData>();

        Engine.TimeScale = 1;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _worldSimulator.Process(delta);

        _playerData.InBattle = _playerData.PlayerSub != null
            && (_playerData.PlayerSub.IsWrecked
                || _playerData.PlayerSub.SonarContactRecords.Values
                    .Any(x => x.IsDetected
                        && x.Identified
                        && x.SonarContact.ContactType != ContactType.Station
                        && x.SonarContact.ContactType != ContactType.Wreck
                        && x.SonarContact.ContactType != ContactType.Torpedo
                        && x.SonarContact.IsHostileTo(_playerData.PlayerSub)));
    }

    public float GetAltitude(Vector3 position)
    {
        if (UseOriginRelativeUp)
        {
            return position.Length();
        }
        else
        {
            return position.y;
        }
    }
}
