using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

public class CargoPod : RigidBody, ISonarContact, ICombatant, ISalvageTarget
{
    public ulong Id { get; private set; }

    public string FriendlyName => "Cargo Pod";
    public Vector3 RealPosition => GlobalTransform.origin;
    
    [Export]
    public float OverallNoise { get; set; }
    [Export]
    public float BracketSizeFactor { get; set; }
    [Export]
    public float CargoVolume { get; set; }
    [Export]
    public float MaximumHealth;
    [Export]
    public float DecayRate;

    public bool IsPlayerVisible { get; set; }
    public ContactType ContactType => ContactType.Wreck;
    public Faction Faction { get; set; }
    public List<TemporaryNoise> TemporaryNoises { get; } = new List<TemporaryNoise>();
    public bool IsDockable => false;
    public Vector3 Velocity => LinearVelocity;
    public Vector3 AimPoint { get; set; }
    public bool IsWrecked => false;
    public bool CanBeSalvaged => CommodityStorage.CommoditiesPresent.Where(c => CommodityStorage.GetQty(c) >= 1).Any();
    public CommodityStorage CommodityStorage { get; private set; }
    public float Health { get; set; }

    private GameplayRoot _gameplayRoot;
    private Vector3 _upVector;

    public float HealthPercent => Health / MaximumHealth;

    public string ReferenceId => Id.ToString();

    public float SpeakingTimer { get; set; }
    public string DebugText { get; set; }

    [Inject]
    private ISonarContactTracker _sonarContactTracker;
    [Inject]
    private INotificationSystem _notificationSystem;
    [Inject]
    private IPlayerData _playerData;

    public void Initialize()
    {
        CommodityStorage = new CommodityStorage(CargoVolume, false);
        Health = MaximumHealth;

        _gameplayRoot = this.FindParent<GameplayRoot>();

        _upVector = Vector3.Up;
    }

    public override void _Ready()
    {
        Id = GetInstanceId();
        this.ResolveDependencies();
        _sonarContactTracker.AddSonarContact(this);
    }

    public override void _ExitTree()
    {
        base._ExitTree();
        _sonarContactTracker.RemoveSonarContact(this);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        Health -= delta * DecayRate;

        if (Health <= 0 || CommodityStorage.TotalVolume <= 0)
        {
            QueueFree();
        }

        if (_gameplayRoot.UseOriginRelativeUp)
        {
            _upVector = GlobalTransform.origin.Normalized();
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        if (_gameplayRoot is Gameplay gameplay)
        {
            var currentFactor = 1f;

            var collisionInfo = GetWorld().DirectSpaceState
                .IntersectRay(
                    GlobalTransform.origin,
                    GlobalTransform.origin +_upVector * -1000,
                    null,
                    0b00000000000000000010,
                    collideWithBodies: true,
                    collideWithAreas: true);

            if (collisionInfo.Count > 0)
            {
                currentFactor = Mathf.Clamp(
                    (GlobalTransform.origin.DistanceTo((Vector3)collisionInfo["position"]) - 100) / 300,
                    0, 1);
            }

            AddCentralForce(gameplay.GetCurrentFlow(GlobalTransform.origin) * Mass * currentFactor);
        }
    }

    public void HitRecieved(ICombatant shooter, float damage, float penetration, Vector3 hitLocation, Vector3 velocity)
    {
        var penetrationFactor = 1f;

        var actualDamage = damage * penetrationFactor;

        Health -= actualDamage;

        if (shooter is Submarine submarine && submarine.Faction == _playerData.Faction)
        {
            _notificationSystem.AddNotification(new Notification
            {
                IsGood = true,
                Body = $"Hit for {actualDamage.ToString("N0")} damage ({penetrationFactor.ToString("P0")} penetration)"
            });
        }

        if (Health <= 0)
        {
            QueueFree();
        }
    }

    public bool IsFriendlyTo(ISonarContact other)
    {
        return false;
    }

    public bool IsHostileTo(ISonarContact other)
    {
        return false;
    }
}
