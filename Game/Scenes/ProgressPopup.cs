using Autofac;
using Godot;
using System;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;

public class ProgressPopup : PopupPanel
{
    private TextureProgress _overallProgress;
    private TextureProgress _stepProgress;
    private Label _stepLabel;
    private Label _titleLabel;
    private ILogger _logger;
    private PanelContainer _errorPanel;
    private Label _errorTitleLabel;
    private Label _errorBodyLabel;
    
    public override void _Ready()
    {
        _logger = IocManager.IocContainer.Resolve<ILogger>();
        _overallProgress = GetNode<TextureProgress>("VBoxContainer/OverallProgress");
        _stepProgress = GetNode<TextureProgress>("VBoxContainer/StepProgress");
        _stepLabel = GetNode<Label>("VBoxContainer/StepLabel");
        _titleLabel = GetNode<Label>("VBoxContainer/TitleLabel");
        _errorPanel = GetNode<PanelContainer>("ErrorPanel");
        _errorTitleLabel = GetNode<Label>("ErrorPanel/VBoxContainer/ErrorTitleLabel");
        _errorBodyLabel = GetNode<Label>("ErrorPanel/VBoxContainer/ErrorBodyLabel");
    }

    public void ShowFor(
        string title,
        Action<ReportProgress> workAction,
        Action finishAction)
    {
        _errorPanel.Visible = false;
        _titleLabel.Text = title;

        System.Threading.Tasks.Task.Run(() =>
        {
            try
            {
                workAction.Invoke(UpdateProgress);
                finishAction();
                Visible = false;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error in ProgressPopup");
                _errorTitleLabel.Text = "Failed";
                _errorBodyLabel.Text = $"{ex.GetType().Name} {ex.Message}\nCheck log file for details.";
                _errorPanel.Visible = true;
            }
        });

        ShowModal(true);
    }

    private void UpdateProgress(double overallProgress, string stepName, double stepProgress)
    {
        _overallProgress.Value = 100 * overallProgress;
        _stepProgress.Value = 100 * stepProgress;
        _stepLabel.Text = stepName;
    }

    public void ErrorClose()
    {
        Hide();
    }
}
