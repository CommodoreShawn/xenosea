using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.Infrastructure.Missions;

public class WorldgenTest : Spatial
{
    private bool _worldGenerated;
    private ProgressPopup _progressPopup;
    private MeshInstance _globeMesh;
    private Label _datumCoordsLabel;
    private Label _datumDepthLabel;
    private Label _datumIceLabel;
    private Label _datumCurrentLabel;
    private WorldgenCamera _camera;
    private CollisionShape _collisionSphere;
    private ConfirmationDialog _spawnConfirm;
    private Label _datumIronLabel;
    private Label _datumCopperLabel;
    private Label _datumLeadLabel;
    private Label _datumNickelLabel;
    private Label _datumThoriumLabel;
    private Label _datumPhosphorusLabel;
    private Label _datumMagnesiumLabel;
    private Label _datumSulfurLabel;
    private Label _datumFertilityLabel;
    private Label _datumStationLabel;
    private Label _datumInfluenceList;

    private Label _dateLabel;
    private Label _timeLabel;
    private Control _timeButtonContainer;

    private Control _stationInfoPanel;
    private Label _stationInfoNameLabel;
    private GridContainer _stationInfoEconomyBox;
    private GridContainer _globalEconomyBox;
    private GridContainer _stationAgentGrid;
    private VBoxContainer _factionButtonList; 

    private Vector3 _lastHitPoint;
    private WorldDatum _selectedDatum;
    private SimulationStation _currentlyShownStation;

    private bool _generationFinished;

    private int _colorMode;
    private int _timeFactor;
    private Dictionary<SimulationAgent, AgentIndicator> _agentIndicators;
    private Dictionary<WeatherSystem, StormIndicator> _stormIndicators;

    private Control _agentInfoPanel;
    private Label _agentNameLabel;
    private Label _agentFactionLabel;
    private Label _agentOrdersLabel;

    private List<Faction> _factionsForEconomy;

    [Inject]
    private IWorldGenerator _worldGenerator;
    [Inject]
    private IWorldManager _worldManager;
    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IWorldSimulator _worldSimulator;
    [Inject]
    private IMissionTracker _missionTracker;
    [Inject]
    private IStationGenerator _stationGenerator;

    [Export]
    public SpatialMaterial Material;
    [Export]
    public PackedScene StationIndicatorPrefab;
    [Export]
    public WorldDefinition WorldDefinition;
    [Export]
    public PackedScene StormIndicatorPrefab;

    private Spatial _indicatorContainer;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _progressPopup = GetNode<ProgressPopup>("ProgressPopup");
        _globeMesh = GetNode<MeshInstance>("GlobeMesh");
        _datumCoordsLabel = GetNode<Label>("PanelContainer/VBoxContainer/DatumCoordsLabel");
        _datumDepthLabel = GetNode<Label>("PanelContainer/VBoxContainer/DatumDepthLabel");
        _datumIceLabel = GetNode<Label>("PanelContainer/VBoxContainer/DatumIceLabel");
        _datumCurrentLabel = GetNode<Label>("PanelContainer/VBoxContainer/DatumCurrentLabel");
        _camera = GetNode<WorldgenCamera>("WorldgenCamera");
        _collisionSphere = GetNode<CollisionShape>("Area/CollisionShape");
        (_collisionSphere.Shape as SphereShape).Radius = Constants.WORLD_RADIUS / 500;
        _spawnConfirm = GetNode<ConfirmationDialog>("SpawnDialog");

        _datumIronLabel = GetNode<Label>("PanelContainer/VBoxContainer/DatumIronLabel");
        _datumCopperLabel = GetNode<Label>("PanelContainer/VBoxContainer/DatumCopperLabel");
        _datumLeadLabel = GetNode<Label>("PanelContainer/VBoxContainer/DatumLeadLabel");
        _datumNickelLabel = GetNode<Label>("PanelContainer/VBoxContainer/DatumNickelLabel");
        _datumThoriumLabel = GetNode<Label>("PanelContainer/VBoxContainer/DatumThoriumLabel");
        _datumPhosphorusLabel = GetNode<Label>("PanelContainer/VBoxContainer/DatumPhosphorusLabel");
        _datumMagnesiumLabel = GetNode<Label>("PanelContainer/VBoxContainer/DatumMagnesiumLabel");
        _datumSulfurLabel = GetNode<Label>("PanelContainer/VBoxContainer/DatumSulfurLabel");
        _datumFertilityLabel = GetNode<Label>("PanelContainer/VBoxContainer/DatumFertilityLabel");
        _datumStationLabel = GetNode<Label>("PanelContainer/VBoxContainer/DatumStationLabel");
        _indicatorContainer = GetNode<Spatial>("Indicators");

        _dateLabel = GetNode<Label>("TimePanel/VBoxContainer/DateLabel");
        _timeLabel = GetNode<Label>("TimePanel/VBoxContainer/TimeLabel");
        _timeButtonContainer = GetNode<Control>("TimePanel/VBoxContainer/HBoxContainer");

        _stationInfoPanel = GetNode<Control>("StationInfoPanel");
        _stationInfoNameLabel = GetNode<Label>("StationInfoPanel/VBoxContainer/NameLabel");
        _stationInfoEconomyBox = GetNode<GridContainer>("StationInfoPanel/VBoxContainer/EconomyGrid");
        _stationAgentGrid = GetNode<GridContainer>("StationInfoPanel/VBoxContainer/AgentGrid");

        _globalEconomyBox = GetNode<GridContainer>("GlobalInfoPanel/VBoxContainer/HBoxContainer/HBoxContainer/EconomyGrid");
        _factionButtonList = GetNode<VBoxContainer>("GlobalInfoPanel/VBoxContainer/HBoxContainer/HBoxContainer/FactionButtonList");

        _agentInfoPanel = GetNode<Control>("AgentlInfoPanel");
        _agentNameLabel = GetNode<Label>("AgentlInfoPanel/VBoxContainer/NameLabel");
        _agentFactionLabel = GetNode<Label>("AgentlInfoPanel/VBoxContainer/FactionLabel");
        _agentOrdersLabel = GetNode<Label>("AgentlInfoPanel/VBoxContainer/OrdersLabel");
        _datumInfluenceList = GetNode<Label>("PanelContainer/VBoxContainer/DatumInfluenceList");

        _datumCoordsLabel.Text = string.Empty;
        _datumDepthLabel.Text = string.Empty;
        _datumIceLabel.Text = string.Empty;
        _datumCurrentLabel.Text = string.Empty;
        _datumIronLabel.Text = string.Empty;
        _datumCopperLabel.Text = string.Empty;
        _datumLeadLabel.Text = string.Empty;
        _datumNickelLabel.Text = string.Empty;
        _datumThoriumLabel.Text = string.Empty;
        _datumPhosphorusLabel.Text = string.Empty;
        _datumMagnesiumLabel.Text = string.Empty;
        _datumSulfurLabel.Text = string.Empty;
        _datumFertilityLabel.Text = string.Empty;
        _datumStationLabel.Text = string.Empty;
        _datumInfluenceList.Text = string.Empty;
        _timeFactor = 0;
        _agentIndicators = new Dictionary<SimulationAgent, AgentIndicator>();
        _stormIndicators = new Dictionary<WeatherSystem, StormIndicator>();
        _factionsForEconomy = new List<Faction>();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(!_worldGenerated)
        {
            _worldGenerated = true;

            _progressPopup.ShowFor(
                "Generating World...",
                reportProgress =>
                {
                    _worldGenerator.GenerateWorld(
                        WorldDefinition,
                        (overall, step, prog) => reportProgress(overall * 0.7, step, prog));

                    _stationGenerator.PlaceStations(
                        WorldDefinition,
                        prog => reportProgress(0.8, "Placing stations.", prog));

                    _worldGenerator.PlaceAgents(
                        prog => reportProgress(0.9, "Placing agents.", prog));
                },
                () => RebuildWorld());
        }

        if (_generationFinished && !Input.IsMouseButtonPressed((int)ButtonList.Right) && !_spawnConfirm.Visible)
        {
            var pickFrom = _camera.ProjectRayOrigin(GetViewport().GetMousePosition());
            var pickTo = pickFrom + _camera.ProjectRayNormal(GetViewport().GetMousePosition()) * 5000;
            var collisions = GetWorld().DirectSpaceState.IntersectRay(pickFrom, pickTo, collideWithAreas: true);

            if (collisions.Count > 0)
            {
                var hitPoint = (Vector3)collisions["position"] * 500;

                if (hitPoint != _lastHitPoint)
                {
                    _lastHitPoint = hitPoint;

                    var nearDatum = _worldManager.WorldDatums
                        .OrderBy(d => (d.Position - hitPoint).LengthSquared())
                        .FirstOrDefault();

                    if (nearDatum != null)
                    {
                        _datumCoordsLabel.Text = $"{nearDatum.Latitude}, {nearDatum.Longitude}";
                        _datumDepthLabel.Text = $"Depth: {nearDatum.Depth.ToString("N0")}m";
                        _datumIceLabel.Text = $"Ice: {nearDatum.IceDepth.ToString("N0")}m";
                        _datumCurrentLabel.Text = $"Current: {nearDatum.CurrentFlow.Length().ToString("F1")}m/s";
                        _selectedDatum = nearDatum;
                        _datumIronLabel.Text = $"Iron: {nearDatum.IronOutput.ToString("N1")}";
                        _datumCopperLabel.Text = $"Copper: {nearDatum.CopperOutput.ToString("N1")}";
                        _datumLeadLabel.Text = $"Lead: {nearDatum.LeadOutput.ToString("N1")}";
                        _datumNickelLabel.Text = $"Nickel: {nearDatum.NickelOutput.ToString("N1")}";
                        _datumThoriumLabel.Text = $"Thorium: {nearDatum.ThoriumOutput.ToString("N1")}";
                        _datumPhosphorusLabel.Text = $"Phosphorus: {nearDatum.PhosphorusOutput.ToString("N1")}";
                        _datumMagnesiumLabel.Text = $"Magnesium: {nearDatum.MagnesiumOutput.ToString("N1")}";
                        _datumSulfurLabel.Text = $"Sulfur: {nearDatum.SulfurOutput.ToString("N1")}";
                        _datumFertilityLabel.Text = $"Fertility: {nearDatum.Fertility.ToString("P0")}";

                        _datumInfluenceList.Text = string.Empty; 

                        foreach(var faction in nearDatum.FactionInfluence.OrderByDescending(kvp => kvp.Value).Select(kvp=>kvp.Key))
                        {
                            var line = $"{faction.Name}: {nearDatum.FactionInfluence[faction]}: ";

                            line += string.Join(", ", nearDatum.InfluenceSources.Where(s => s.Faction == faction).Select(x => x.Name));

                            if(_datumInfluenceList.Text.Length < 1)
                            {
                                _datumInfluenceList.Text = line;
                            }
                            else
                            {
                                _datumInfluenceList.Text += "\n" + line;
                            }
                        }

                        if (nearDatum.SimulationStation != null)
                        {
                            _datumStationLabel.Text = $"{nearDatum.SimulationStation.Name} ({nearDatum.SimulationStation.Faction.Name})";
                        }
                        else
                        {
                            _datumStationLabel.Text = string.Empty;
                        }
                    }
                }

                if (_selectedDatum != null && Input.IsMouseButtonPressed((int)ButtonList.Left))
                {
                    _spawnConfirm.DialogText = "Spawn at this location?";
                    _spawnConfirm.GetOk().Disabled = _selectedDatum.Biome == Biome.Ice;
                    _spawnConfirm.ShowModal(true);
                }

                var agent = _worldManager.SimulationAgents
                    .Where(a => a.Location == _selectedDatum)
                    .FirstOrDefault();

                _agentInfoPanel.Visible = agent != null;

                if(agent != null)
                {
                    _agentNameLabel.Text = $"{agent.Name} ({agent.Health} / {agent.MaxHealth})";
                    _agentFactionLabel.Text = agent.Faction.Name;

                    if (agent.CurrentBattle != null)
                    {
                        _agentOrdersLabel.Text = "In Battle";
                    }
                    else
                    {
                        _agentOrdersLabel.Text = agent.Goal?.Description ?? "idle";
                    }
                }
            }
            else
            {
                _datumCoordsLabel.Text = string.Empty;
                _datumDepthLabel.Text = string.Empty;
                _datumIceLabel.Text = string.Empty;
                _datumCurrentLabel.Text = string.Empty;
                _datumIronLabel.Text = string.Empty;
                _datumCopperLabel.Text = string.Empty;
                _datumLeadLabel.Text = string.Empty;
                _datumNickelLabel.Text = string.Empty;
                _datumThoriumLabel.Text = string.Empty;
                _datumPhosphorusLabel.Text = string.Empty;
                _datumMagnesiumLabel.Text = string.Empty;
                _datumSulfurLabel.Text = string.Empty;
                _datumFertilityLabel.Text = string.Empty;
                _datumStationLabel.Text = string.Empty;
                _datumInfluenceList.Text = string.Empty;
                _agentInfoPanel.Visible = false;
            }
        }

        if (_generationFinished)
        {
            _dateLabel.Text = _worldManager.CurrentDate.ToShortDateString();
            _timeLabel.Text = _worldManager.CurrentDate.ToShortTimeString();

            foreach (var child in _timeButtonContainer.GetChildren().OfType<Button>())
            {
                child.Pressed = child.Text == "x" + _timeFactor;
            }

            _worldSimulator.Process(delta * _timeFactor);

            if(_currentlyShownStation != _selectedDatum?.SimulationStation)
            {
                _currentlyShownStation = _selectedDatum?.SimulationStation;
                _stationInfoNameLabel.Text = _currentlyShownStation?.Name;
            }

            CommodityType[] commoditiesPresent;
            Label[] commodityLabels;
            int labelRows;

            if (_currentlyShownStation != null)
            {
                commoditiesPresent = _currentlyShownStation.CommodityStorage.CommoditiesPresent.ToArray();
                commodityLabels = _stationInfoEconomyBox.GetChildren().OfType<Label>().ToArray();
                labelRows = commodityLabels.Length / _stationInfoEconomyBox.Columns;

                for(var i = 0; i < (commoditiesPresent.Length + 1)|| i < labelRows; i++)
                {
                    var indexName = i * _stationInfoEconomyBox.Columns;
                    var indexQty = i * _stationInfoEconomyBox.Columns + 1;
                    var indexRate = i * _stationInfoEconomyBox.Columns + 2;
                    var indexImport = i * _stationInfoEconomyBox.Columns + 3;
                    var indexExport = i * _stationInfoEconomyBox.Columns + 4;

                    if(i == 0)
                    {

                    }
                    else if (i < (commoditiesPresent.Length + 1) && i < labelRows)
                    {
                        var commodity = commoditiesPresent[i - 1];

                        var net = _currentlyShownStation.EconomicProcesses.Sum(ep => ep.GetNetPerSecond(commodity, _currentlyShownStation)) * 60;

                        var tradeMissions = _missionTracker.GetTradeMissionsByCommodity(commodity).OfType<SimulationTradeMission>();
                        var imports = tradeMissions.Where(tm => tm.Destination == _currentlyShownStation).Sum(tm => tm.Quantity);
                        var exports = tradeMissions.Where(tm => tm.Source == _currentlyShownStation).Sum(tm => tm.Quantity);

                        commodityLabels[indexName].Text = commodity.Name;
                        commodityLabels[indexQty].Text = _currentlyShownStation.CommodityStorage.GetQty(commodity).ToString("N0");
                        commodityLabels[indexRate].Text = net.ToString("N0");
                        commodityLabels[indexImport].Text = imports.ToString("N0");
                        commodityLabels[indexExport].Text = exports.ToString("N0");
                    }
                    else if(i < commoditiesPresent.Length)
                    {
                        _stationInfoEconomyBox.AddChild(new Label());
                        _stationInfoEconomyBox.AddChild(new Label
                        {
                            Align = Label.AlignEnum.Right
                        });
                        _stationInfoEconomyBox.AddChild(new Label
                        {
                            Align = Label.AlignEnum.Right
                        });
                        _stationInfoEconomyBox.AddChild(new Label
                        {
                            Align = Label.AlignEnum.Right
                        });
                        _stationInfoEconomyBox.AddChild(new Label
                        {
                            Align = Label.AlignEnum.Right
                        });
                    }
                    else if(i < labelRows)
                    {
                        commodityLabels[indexName].QueueFree();
                        commodityLabels[indexQty].QueueFree();
                        commodityLabels[indexRate].QueueFree();
                        commodityLabels[indexImport].QueueFree();
                        commodityLabels[indexExport].QueueFree();
                    }
                }

                var supportedAgents = _currentlyShownStation.SupportedAgentDefinitions.Length;
                var agentLabels = _stationAgentGrid.GetChildren().OfType<Label>().ToArray();
                labelRows = agentLabels.Length / _stationAgentGrid.Columns;
                

                for (var i = 0; i < supportedAgents || i < labelRows; i++)
                {
                    var indexName = i * _stationAgentGrid.Columns;
                    var indexState = i * _stationAgentGrid.Columns + 1;

                    if (i < supportedAgents && i < labelRows)
                    {
                        agentLabels[indexName].Text = _currentlyShownStation.SupportedAgentDefinitions[i].Name;

                        if(_currentlyShownStation.SupportedAgents[i] != null)
                        {
                            agentLabels[indexState].Text = "active";
                        }
                        else
                        {
                            var rebuildPerc = _currentlyShownStation.AgentRebuildCounters[i] / _currentlyShownStation.SupportedAgentDefinitions[i].RebuildCost;
                            agentLabels[indexState].Text = $"Rebuilding: {rebuildPerc.ToString("P0")}";
                        }
                    }
                    else if (i < supportedAgents)
                    {
                        _stationAgentGrid.AddChild(new Label());
                        _stationAgentGrid.AddChild(new Label
                        {
                            Align = Label.AlignEnum.Right
                        });
                    }
                    else if (i < labelRows)
                    {
                        agentLabels[indexName].QueueFree();
                        agentLabels[indexState].QueueFree();
                    }
                }
            }

            _stationInfoPanel.Visible = _currentlyShownStation != null;

            commoditiesPresent = _worldManager.SimulationStations
                .SelectMany(x => x.CommodityStorage.CommoditiesPresent)
                .Distinct()
                .ToArray();

            commodityLabels = _globalEconomyBox.GetChildren().OfType<Label>().ToArray();

            labelRows = commodityLabels.Length / _globalEconomyBox.Columns;

            for (var i = 0; i < (commoditiesPresent.Length + 1) || i < labelRows; i++)
            {
                var indexName = i * _globalEconomyBox.Columns;
                var indexQty = i * _globalEconomyBox.Columns + 1;
                var indexRate = i * _globalEconomyBox.Columns + 2;
                var indexTrade = i * _globalEconomyBox.Columns + 3;

                if(i == 0)
                {
                    if(i < labelRows)
                    {
                        commodityLabels[indexName].Text = "Commodity";
                        commodityLabels[indexQty].Text = "Stored";
                        commodityLabels[indexRate].Text = "Net";
                        commodityLabels[indexTrade].Text = "Trade";
                    }
                    else
                    {
                        _globalEconomyBox.AddChild(new Label());
                        _globalEconomyBox.AddChild(new Label
                        {
                            Align = Label.AlignEnum.Right
                        });
                        _globalEconomyBox.AddChild(new Label
                        {
                            Align = Label.AlignEnum.Right
                        });
                        _globalEconomyBox.AddChild(new Label
                        {
                            Align = Label.AlignEnum.Right
                        });
                    }
                }
                else if (i > 0 && i < (commoditiesPresent.Length + 1) && i < labelRows)
                {
                    var commodity = commoditiesPresent[i - 1];

                    var net = _worldManager.SimulationStations
                        .Where(s => _factionsForEconomy.Contains(s.Faction))
                        .Sum(x => x.EconomicProcesses.Sum(ep => ep.GetNetPerSecond(commodity, x)) * 60);

                    var stored = _worldManager.SimulationStations
                        .Where(s => _factionsForEconomy.Contains(s.Faction))
                        .Sum(x => x.CommodityStorage.GetQty(commodity));

                    var trade = _missionTracker.GetTradeMissionsByCommodity(commodity)
                        .Where(m => _factionsForEconomy.Contains(m.ForFaction))
                        .Sum(x => x.Quantity);

                    commodityLabels[indexName].Text = commodity.Name;
                    commodityLabels[indexQty].Text = stored.ToString("N0");
                    commodityLabels[indexRate].Text = net.ToString("N0");
                    commodityLabels[indexTrade].Text = trade.ToString("N0");
                }
                else if (i < (commoditiesPresent.Length + 1))
                {
                    _globalEconomyBox.AddChild(new Label());
                    _globalEconomyBox.AddChild(new Label
                    {
                        Align = Label.AlignEnum.Right
                    });
                    _globalEconomyBox.AddChild(new Label
                    {
                        Align = Label.AlignEnum.Right
                    });
                    _globalEconomyBox.AddChild(new Label
                    {
                        Align = Label.AlignEnum.Right
                    });
                }
                else if (i < labelRows)
                {
                    commodityLabels[indexName].QueueFree();
                    commodityLabels[indexQty].QueueFree();
                    commodityLabels[indexRate].QueueFree();
                    commodityLabels[indexTrade].QueueFree();
                }
            }

            foreach(var agent in _worldManager.SimulationAgents)
            {
                if(!_agentIndicators.ContainsKey(agent))
                {
                    var indicator = agent.IndicatorPrefab.Instance() as AgentIndicator;
                    indicator.Agent = agent;
                    AddChild(indicator);
                    _agentIndicators.Add(agent, indicator);
                }
            }

            foreach(var weatherSystem in _worldManager.WeatherSystems)
            {
                if (!_stormIndicators.ContainsKey(weatherSystem))
                {
                    var indicator = StormIndicatorPrefab.Instance() as StormIndicator;
                    indicator.WeatherSystem = weatherSystem;
                    indicator.SizeScale = 1 / 500f;
                    AddChild(indicator);
                    _stormIndicators.Add(weatherSystem, indicator);
                }
            }
        }
    }

    public void RebuildWorld()
    {
        _indicatorContainer.ClearChildren();
        var surfaceTool = new SurfaceTool();
        surfaceTool.Begin(Mesh.PrimitiveType.Triangles);

        var maxDepth = _worldManager.WorldDatums
            .Max(d => d.Depth);

        var datumRows = _worldManager.WorldDatums
            .GroupBy(x => x.Latitude)
            .OrderBy(g => g.Key)
            .Select(g => g.ToArray())
            .ToArray();

        for (var rowIndex = 0; rowIndex < datumRows.Length - 1; rowIndex++)
        {
            for (var colIndex = 0; colIndex < datumRows[rowIndex].Length; colIndex++)
            {
                var nextRow = rowIndex + 1;
                var percInRow = colIndex / (float)datumRows[rowIndex].Length;
                var nextPercInRow = ((colIndex + 1) % datumRows[rowIndex].Length) / (float)datumRows[rowIndex].Length;
                var nextRowIndex = Mathf.RoundToInt(datumRows[rowIndex + 1].Length * percInRow) % datumRows[rowIndex + 1].Length;
                var nextRowNextIndex = (nextRowIndex + 1) % datumRows[rowIndex + 1].Length;

                var datum = datumRows[rowIndex][colIndex];
                var datumUp = datumRows[nextRow][nextRowIndex];
                var datumUpRight = datumRows[nextRow][nextRowNextIndex];
                var datumRight = datumRows[rowIndex][(colIndex + 1) % datumRows[rowIndex].Length];

                surfaceTool.AddColor(ToColor(datum, maxDepth));
                surfaceTool.AddVertex(ToPos(datum));

                surfaceTool.AddColor(ToColor(datumUpRight, maxDepth));
                surfaceTool.AddVertex(ToPos(datumUpRight));

                surfaceTool.AddColor(ToColor(datumUp, maxDepth));
                surfaceTool.AddVertex(ToPos(datumUp));

                surfaceTool.AddColor(ToColor(datum, maxDepth));
                surfaceTool.AddVertex(ToPos(datum));

                surfaceTool.AddColor(ToColor(datumRight, maxDepth));
                surfaceTool.AddVertex(ToPos(datumRight));

                surfaceTool.AddColor(ToColor(datumUpRight, maxDepth));
                surfaceTool.AddVertex(ToPos(datumUpRight));
            }
        }

        surfaceTool.GenerateNormals();
        _globeMesh.Mesh = surfaceTool.Commit();
        _globeMesh.MaterialOverride = Material;

        foreach(var simStation in _worldManager.SimulationStations)
        {
            var toCenter = (-simStation.Location.Position).Normalized();

            var adjustedPos = simStation.Location.Position / 500
                + (simStation.Location.Depth * toCenter) / 200;

            var indicator = StationIndicatorPrefab.Instance() as Spatial;
            indicator.Transform = Transform.Identity
                .Translated(adjustedPos)
                .LookingAt(simStation.Location.Position, Vector3.Up);
            _indicatorContainer.AddChild(indicator);
        }

        _factionButtonList.ClearChildren();
        foreach(var faction in _worldManager.Factions)
        {
            var button = new Button
            {
                Text = faction.Name,
                ToggleMode = true,
                Pressed = true
            };
            
            _factionsForEconomy.Add(faction);

            button.Connect("pressed", this, nameof(ToggleFactionEconomy), new Godot.Collections.Array { faction });
            _factionButtonList.AddChild(button);
        }

        _generationFinished = true;
    }

    private Vector3 ToPos(WorldDatum datum)
    {
        var basePosition = datum.Position / 500;

        if (datum.Biome != Biome.Ice)
        {
            var toCenter = (-basePosition).Normalized();

            return basePosition + (datum.Depth * toCenter) / 200;
        }
        else
        {
            return basePosition;
        }
    }

    private Vector3 ToPos(Vector3 rawPosition, float depth)
    {
        var basePosition = rawPosition / 500;

        var toCenter = (-basePosition).Normalized();

        return basePosition + (depth * toCenter) / 200;
    }

    private Color ToColor(WorldDatum datum, float maxDepth)
    {
        if (_colorMode == 0)
        {
            if (datum.Depth <= datum.IceDepth)
            {
                return new Color(1, 1, 1);
            }

            var d = 1 - (datum.Depth / maxDepth);

            if (datum.Depth < Constants.DEPTH_MAX_DIVER)
            {
                return new Color(0, d, 0);
            }
            else if (datum.Depth < Constants.DEPTH_MAX_SUIT)
            {
                return new Color(0, 0, d);
            }
            else if (datum.Depth < Constants.DEPTH_MAX_SUB)
            {
                return new Color(d, d, 0);
            }

            return new Color(d, 0, 0);
        }
        else if (_colorMode == 1)
        {
            return new Color(datum.Volcanism, 0, 0);
        }
        else if (_colorMode == 2)
        {
            return new Color(0, datum.Fertility, 0);
        }
        else if (_colorMode == 3)
        {
            var d = 1 - (datum.Depth / maxDepth);

            var r = 0f;
            var g = 0f;
            var b = 0f;
            switch (datum.Biome)
            {
                case Biome.Ice:
                    r = 0.5f;
                    g = 0.8f;
                    b = 1.0f;
                    d = 1f;
                    break;
                case Biome.Dunes:
                    r = 1.0f;
                    g = 1.0f;
                    b = 0.4f;
                    break;
                case Biome.Forest:
                    r = 0.0f;
                    g = 0.5f;
                    b = 0.0f;
                    break;
                case Biome.Plains:
                    r = 0.0f;
                    g = 1.0f;
                    b = 0.5f;
                    break;
                case Biome.Reef:
                    r = 1.0f;
                    g = 0.0f;
                    b = 0.6f;
                    break;
                case Biome.Smokers:
                    r = 1.0f;
                    g = 0.0f;
                    b = 0.0f;
                    break;
            }

            return new Color(r * d, g * d, b * d);
        }
        else if (_colorMode == 4)
        {
            return new Color(
                datum.IronOutput,
                datum.IronOutput,
                datum.IronOutput);
        }
        else if (_colorMode == 5)
        {
            return new Color(
                datum.CopperOutput,
                datum.CopperOutput,
                datum.CopperOutput);
        }
        else if (_colorMode == 6)
        {
            return new Color(
                datum.LeadOutput,
                datum.LeadOutput,
                datum.LeadOutput);
        }
        else if (_colorMode == 7)
        {
            return new Color(
                datum.NickelOutput,
                datum.NickelOutput,
                datum.NickelOutput);
        }
        else if (_colorMode == 8)
        {
            return new Color(
                datum.ThoriumOutput,
                datum.ThoriumOutput,
                datum.ThoriumOutput);
        }
        else if (_colorMode == 9)
        {
            return new Color(
                datum.PhosphorusOutput,
                datum.PhosphorusOutput,
                datum.PhosphorusOutput);
        }
        else if (_colorMode == 10)
        {
            return new Color(
                datum.MagnesiumOutput,
                datum.MagnesiumOutput,
                datum.MagnesiumOutput);
        }
        else if (_colorMode == 11)
        {
            return new Color(
                datum.SulfurOutput,
                datum.SulfurOutput,
                datum.SulfurOutput);
        }
        else if (_colorMode == 12)
        {
            var primaryFaction = datum.FactionInfluence
                .OrderByDescending(kvp => kvp.Value)
                .Where(kvp => kvp.Value > 1)
                .Select(kvp => kvp.Key)
                .FirstOrDefault();

            if (datum.Depth <= datum.IceDepth)
            {
                return Colors.White;
            }
            else if (primaryFaction != null)
            {
                return primaryFaction.Color;
            }
            else
            {
                return Colors.Black;
            }
        }
        else if (_colorMode == 13)
        {
            var toEast = datum.Position
                .DirectionTo(
                    Util.LatLonToVector(
                        datum.Latitude, 
                        datum.Longitude - 1, 
                        Constants.WORLD_RADIUS));

            var strengthFactor = Mathf.Clamp((datum.CurrentFlow.Length() / 10), 0, 1);

            var dirFactor = Mathf.Clamp(datum.CurrentFlow.AngleTo(toEast) / Mathf.Pi, 0, 1);

            return new Color(
                strengthFactor * dirFactor,
                strengthFactor * (1 - dirFactor),
                0);
        }

        return new Color();
    }

    public void SetColorMode(int colorMode)
    {
        _colorMode = colorMode;
        RebuildWorld();
    }

    public void OnQuit()
    {
        GetTree().ChangeScene("res://Scenes/MainMenu.tscn");
    }

    public void SetTime(int timeFactor)
    {
        _timeFactor = timeFactor;
    }

    public void SaveAsImage()
    {
        var width = 720;
        var height = 360;

        _progressPopup.ShowFor(
            "Saving Image",
            (reportProgres) =>
            {
                var image = new System.Drawing.Bitmap(width, height);

                var maxDepth = _worldManager.WorldDatums
                    .Max(d => d.Depth);

                var datumRows = _worldManager.WorldDatums
                    .GroupBy(d => d.Latitude)
                    .ToArray();

                for (var y = 0; y < height; y++)
                {
                    var projectedY = -(((y / (float)height) * 2) - 1) * Constants.WORLD_RADIUS;

                    var row = datumRows
                        .OrderBy(g => Mathf.Abs(g.First().Position.y - projectedY))
                        .First();
                    
                    var lat = -(y / 2f - 90);
                    for (var x = 0; x < width; x++)
                    {
                        var lon = 360 - (x / 2f);
                     
                        var pos = Util.LatLonToVector(lat, lon, Constants.WORLD_RADIUS);
                        var datum = row
                            .OrderBy(d => d.Position.DistanceSquaredTo(pos))
                            .FirstOrDefault();
                        var color = ToColor(datum, maxDepth);
                        image.SetPixel(x, y, ToSystemColor(color));
                    }
                    reportProgres(0, "Generating Image", y / (double)height);
                }

                reportProgres(0.7, "Saving", 0);

                string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);

                image.Save(System.IO.Path.Combine(path, $"map_{_colorMode}_{DateTime.Now.Millisecond}.png"));
            },
            () => { });
    }

    private System.Drawing.Color ToSystemColor(Color color)
    {
        return System.Drawing.Color.FromArgb(
            (int)(255 * color.a),
            (int)(255 * color.r),
            (int)(255 * color.g),
            (int)(255 * color.b));
    }

    public void ToggleFactionEconomy(Faction faction)
    {
        if(_factionsForEconomy.Contains(faction))
        {
            _factionsForEconomy.Remove(faction);
        }
        else
        {
            _factionsForEconomy.Add(faction);
        }
    }

    public void SpawnConfirmed()
    {
        foreach (var poi in WorldDefinition.StartingKnownPoi)
        {
            var poiPos = Util.LatLonToVector(poi.Latitude, poi.Longitude, Constants.WORLD_RADIUS);

            var location = _worldManager.WorldDatums
                .OrderBy(d => d.Position.DistanceTo(poiPos))
                .Take(1)
                .FirstOrDefault();

            if (location != null)
            {
                _playerData.KnownPointsOfInterest
                    .Add(new KnownPointOfInterest
                    {
                        ContactType = poi.ContactType,
                        Location = location,
                        FriendlyName = poi.Name,
                        ReferenceId = poi.Name
                    });
            }
        }

        foreach (var topic in WorldDefinition.StartingTopics)
        {
            _playerData.KnownTopics.Add(topic);
        }

        _playerData.Faction = WorldDefinition.PlayerFaction;
        _playerData.Money = WorldDefinition.StartingMoney;
        _playerData.DetailedSimDistance = 40000;
        _playerData.SpawnDatum = _selectedDatum;
        _playerData.PlayerLoadout = WorldDefinition.StarterSub;
        _playerData.SkipIntro = true;

        GetTree().ChangeScene("res://Scenes/Gameplay.tscn");
    }
}
