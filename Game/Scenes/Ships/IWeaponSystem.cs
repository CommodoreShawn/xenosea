﻿using Xenosea.Logic.Detection;

namespace Xenosea.Scenes.Ships
{
    public interface IWeaponSystem
    {
        void Shoot(ITargetableThing target);
    }
}
