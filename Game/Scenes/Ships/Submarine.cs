using Godot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;
using Xenosea.Resources;
using Xenosea.Scenes.Ships;

public class Submarine : RigidBody, ISonarContact, ICombatant, ISalvageTarget
{
    public SubHullType HullType { get; private set; }
    private ArmorType _armorType;
    private EngineModuleType _engineModuleType;
    private SonarModuleType _sonarModuleType;
    private TowedArrayModuleType _towedArrayModuleType;
    private TurretModuleType[] _turretModuleTypes;
    private TorpedoLauncherModuleType[] _launcherModuleTypes;
    private MinisubHangarModuleType[] _hangarModuleTypes;
    private BasicModuleType[] _otherModuleTypes;

    [Export]
    public float ThrottleRate = 0.1f;
    [Export]
    public float MinCameraDist;
    [Export]
    public PackedScene CargoPodPrefab;
    [Export]
    public Texture TypeIcon { get; set; }
    [Export]
    public float BracketSizeFactor { get; set; }
    [Export]
    public PackedScene LeakBubblesPrefab { get; set; }
    [Export]
    public bool UsesCharge { get; set; }
    [Export]
    public float MaximumCharge { get; set; }

    public int MaximumLeakBubbles => (int)Mathf.Max(HullType.Health / 1000, 1);
    
    public bool IsPlayerSub { get; set; }

    [Export]
    public bool HintBroadsideShooter;
    [Export]
    public float HintDockingWidth;
    [Export]
    public float HintDiverRadius;
    [Export]
    public bool HintStrafingShooter;
    [Export]
    public bool HintFighterShooter;
    [Export]
    public float DivePitchFactor = 1;
    [Export]
    public float BallastFactor = 1;
    [Export]
    public bool IsMinisub = false;
    [Export]
    public float TurningRollFactor = 0.5f;

    public string FriendlyName { get; set; }
    public float CargoVolume { get; private set; }

    public int MaximumDiveTeams => _otherModuleTypes.OfType<DiveChamberModuleType>().Sum(x => x.DiverCapacity);
    [Export]
    public CommodityQuantity[] WreckCommodities;
    [Export]
    public PackedScene CollisionSoundPrefab;

    [Inject]
    private ISonarContactTracker _sonarContactTracker;
    [Inject]
    private INotificationSystem _notificationSystem;
    [Inject]
    private IPerftracker _perfTracker;
    [Inject]
    private IBountyTracker _bountyTracker;
    [Inject]
    private IWorldManager _worldManager;
    [Inject]
    private ICrimeTracker _crimeTracker;
    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private ISimulationEventBus _simulationEventBus;

    public string ReferenceId => Id.ToString();
    public HubPoint LocalHub { get; set; }
    public ulong Id { get; private set; }
    public float ActualThrottle { get; set; } = 0;
    public float Throttle { get; set; } = 0;
    public float Rudder { get; set; } = 0;
    public float Dive { get; set; } = 0;
    public Spatial CameraCenter { get; set; }
    public Vector3 AimPoint { get; set; }
    public Vector3? SlideForce { get; set; } = null;
    public List<TurretMount> TurretMounts { get; private set; }
    public List<TurretSystem> TurretSystems { get; private set; }
    public List<TorpedoTube> TorpedoTubes { get; private set; }
    public List<MinisubHangar> MinisubHangars { get; private set; }
    public Vector3 RealPosition => Transform.origin;
    public Vector3 Velocity => LinearVelocity;
    public float OverallNoise { get; private set; }
    public List<TemporaryNoise> TemporaryNoises { get; private set; }
    public Dictionary<ulong, SonarContactRecord> SonarContactRecords { get; private set; }
    public bool IsPlayerVisible { get; set; }
    public ContactType ContactType => IsWrecked ? ContactType.Wreck : (IsMinisub ? ContactType.Minisub : ContactType.Sub);
    public float Health { get; set; }
    public float Charge { get; set; }
    public bool IsWrecked { get; set; }
    public bool IsMissionWreck { get; set; }
    public bool IsDockable => false;
    public IStation IsDockedTo { get; set; }
    public CommodityStorage CommodityStorage { get; private set; }
    private float _bountyTimer;
    public List<DiveTeam> DiveTeams { get; private set; }
    public Spatial DiveHatch { get; private set; }
    public Spatial DiveHatchApproach { get; private set; }
    public SimulationAgent AgentRepresentation { get; set; }
    public Faction Faction { get; set; }

    public float HealthPercent => Health / HullType.Health;

    private Vector3 _wreckTorque;
    private float _wreckSpinTime;

    private System.Random _random;
    public List<Spatial> LeakBubbles { get; private set; }
    public bool CanBeSalvaged => IsWrecked && CommodityStorage.CommoditiesPresent.Where(c => CommodityStorage.GetQty(c) >= 1).Any();

    private float _wreckTimer;

    public bool HangarDoorsOpen { get; private set; }
    public float AuxilaryLaunchDelay { get; set; }

    [Export]
    public float DecoyReloadTime = 30f;
    public float DecoyLoadProgress => _decoyLoadTimer / DecoyReloadTime;
    private float _decoyLoadTimer;
    [Export]
    public PackedScene DecoyPrefab;
    private Spatial _decoyDropPoint;

    public bool IsMissionEnemy { get; set; }

    private float _cargoJettisonTimer;
    private List<CargoPod> _cargoJettisonQueue;
    private bool _useOriginRelativeUp;
    public Vector3 UpVector { get; private set; }
    private GameplayRoot _gameplayRoot;
    public float Altitude => _gameplayRoot.GetAltitude(GlobalTransform.origin);

    public float DistanceToGround { get; private set; }
    public float SpeakingTimer { get; set; }
    public Spatial TowHook { get; set; }
    public TowedArray TowedArray { get; set; }
    public bool DeployTowedArray { get; set; }
    public float TowedArrayRepairTimer { get; set; }

    public string DebugText { get; set; }
    private Vector3 _lastUpdateVelocity;

    public int MaxDiveTeams { get; private set; }
    public float MaximumHealth => HullType.Health;
    public bool HasTowedArray => _towedArrayModuleType != null;

    public override void _Ready()
    {
        Id = GetInstanceId();
        this.ResolveDependencies();

        _random = new System.Random();

        CameraCenter = GetNode<Spatial>("CameraCenter");

        TurretMounts = new List<TurretMount>();
        TorpedoTubes = new List<TorpedoTube>();
        MinisubHangars = new List<MinisubHangar>();
        foreach (var child in GetChildren())
        {
            if (child is TurretMount turretMount)
            {
                TurretMounts.Add(turretMount);
            }
            else if (child is TorpedoTube torpedoTube)
            {
                TorpedoTubes.Add(torpedoTube);
            }
            else if (child is MinisubHangar minisubHangar)
            {
                MinisubHangars.Add(minisubHangar);
            }
        }
        
        TemporaryNoises = new List<TemporaryNoise>();
        _sonarContactTracker.AddSonarContact(this);
        SonarContactRecords = new Dictionary<ulong, SonarContactRecord>();
        LeakBubbles = new List<Spatial>();
        DiveTeams = new List<DiveTeam>();
        DiveHatch = GetNode<Spatial>("DiveHatch");
        DiveHatchApproach = DiveHatch.GetNodeOrNull<Spatial>("ApproachPoint") ?? DiveHatch;
        _decoyDropPoint = GetNode<Spatial>("DecoyDropper");
        _decoyLoadTimer = DecoyReloadTime;
        Charge = MaximumCharge;
        _cargoJettisonQueue = new List<CargoPod>();

        _gameplayRoot = this.FindParent<GameplayRoot>();
        _useOriginRelativeUp = _gameplayRoot.UseOriginRelativeUp;
        UpVector = Vector3.Up;
        TowHook = GetNodeOrNull<Spatial>("TowHook");
    }

    public override void _ExitTree()
    {
        base._ExitTree();
        _sonarContactTracker.RemoveSonarContact(this);
    }

    public override void _Process(float delta)
    {
        var stopwatch = Stopwatch.StartNew();
        base._Process(delta);

        if(Health > MaximumHealth)
        {
            GD.Print($"Sub with {HealthPercent.ToString("P0")} hp!");
            Health = MaximumHealth;
        }

        _lastUpdateVelocity = Velocity;

        if(SpeakingTimer > 0)
        {
            SpeakingTimer -= delta;
        }

        if(_useOriginRelativeUp)
        {
            UpVector = GlobalTransform.origin.Normalized();
        }

        if(UsesCharge)
        {
            if(IsDockedTo != null)
            {
                Charge = Mathf.Clamp(Charge + delta * 10, 0, MaximumCharge);
            }
            else
            {
                Charge -= delta;
            }
        }

        if(AuxilaryLaunchDelay > 0)
        {
            AuxilaryLaunchDelay -= delta;
        }

        UpdateNoises(delta);

        Visible = IsPlayerVisible;

        if(IsWrecked)
        {
            _wreckTimer -= delta;

            if(_wreckTimer <= 0 
                && !IsPlayerVisible
                && !IsMissionWreck)
            {
                QueueFree();
            }
        }
        else
        {
            var throttleDelta = ThrottleRate * delta;
            ActualThrottle += Mathf.Clamp(Throttle - ActualThrottle, -throttleDelta, throttleDelta);

            UpdateSonarContactRecords(delta);

            _decoyLoadTimer = Mathf.Clamp(_decoyLoadTimer + delta, 0, DecoyReloadTime);
        }

        if(_bountyTimer > 0)
        {
            _bountyTimer -= delta;
        }

        foreach(var diveTeam in DiveTeams)
        {
            diveTeam.Process(delta);
        }
        DiveTeams.RemoveAll(d => d.DiverIsDead);

        var shouldHangarBeOpen = DiveTeams.Any(dt => dt.HasTarget || dt.DeployedDiver != null)
            || MinisubHangars.Any(x => x.ShouldLaunch || x.SubNearDock);

        if (shouldHangarBeOpen != HangarDoorsOpen)
        {
            HangarDoorsOpen = shouldHangarBeOpen;
            AuxilaryLaunchDelay = 3;
        }

        if(_cargoJettisonTimer > 0)
        {
            _cargoJettisonTimer -= delta;
        }

        if (_cargoJettisonQueue.Any() && _cargoJettisonTimer <= 0)
        {
            _cargoJettisonTimer = 3;

            var pod = _cargoJettisonQueue.First();
            _cargoJettisonQueue.Remove(pod);

            pod.GlobalTransform = _decoyDropPoint.GlobalTransform;
            pod.AddCollisionExceptionWith(this);
            GetParent().AddChild(pod);
            pod.LinearVelocity = LinearVelocity
                + pod.GlobalTransform.basis.Xform(new Vector3(0, 0, 10));
        }

        if(TowedArrayRepairTimer > 0)
        {
            DeployTowedArray = false;
            if (TowedArray == null)
            {
                TowedArrayRepairTimer -= delta;
            }
        }

        if (DeployTowedArray && TowedArray == null && _towedArrayModuleType != null)
        {
            TowedArray = _towedArrayModuleType.Prefab.Instance() as TowedArray;
            TowedArray.Submarine = this;
            TowedArray.AddCollisionExceptionWith(this);
            GetParent().AddChild(TowedArray);
            TowedArray.GlobalTransform = TowHook.GlobalTransform;
            TowedArray.LinearVelocity = LinearVelocity;
        }

        stopwatch.Stop();
        _perfTracker.LogPerf("Submarine._Process", stopwatch.ElapsedMilliseconds);
    }

    private void UpdateNoises(float delta)
    {
        if (IsWrecked)
        {
            OverallNoise = HullType.GetBaseNoise() * 2;

            if(IsMissionWreck)
            {
                OverallNoise += HullType.GetBaseNoise();
            }
        }
        else
        {
            OverallNoise = 0;

            foreach (var noiseSource in TemporaryNoises)
            {
                noiseSource.Elapsed += delta;

                OverallNoise += (1 - noiseSource.Elapsed / noiseSource.TimeToLive) * noiseSource.NoiseAmount;
            }

            OverallNoise += HullType.GetBaseNoise();
            OverallNoise += Mathf.Abs(ActualThrottle) * _engineModuleType.MaximumNoise;

            TemporaryNoises.RemoveAll(x => x.Elapsed > x.TimeToLive);
        }
    }

    private void UpdateSonarContactRecords(float delta)
    {
        var stopwatch = Stopwatch.StartNew();

        var toRemove = SonarContactRecords
            .Where(kvp => !kvp.Value.IsValidInstance)
            .Select(kvp => kvp.Key)
            .ToArray();

        foreach (var key in toRemove)
        {
            SonarContactRecords.Remove(key);
        }

        var towedArrayActive = TowedArray != null
            && TowedArray.FullyExtended
            && !TowedArray.Unstable
            && !TowedArray.TooFast
            && TowedArrayRepairTimer <= 0;

        var environmentNoise = 5;

        foreach (var contact in _sonarContactTracker.GetSonarContacts())
        {
            if (contact != this)
            {
                SonarContactRecord record;

                if (SonarContactRecords.ContainsKey(contact.Id))
                {
                    record = SonarContactRecords[contact.Id];
                }
                else
                {
                    record = new SonarContactRecord
                    {
                        SonarContact = contact,
                        PositionEstimateCounter = -1,
                        LastUpdateTime = System.DateTime.MinValue
                    };
                    SonarContactRecords.Add(contact.Id, record);
                }

                if (Engine.TimeScale > 0)
                {
                    // Make detection check counter independent from engine time scale
                    record.DetectionCheckCounter -= delta / Engine.TimeScale;
                }

                if(record.DetectionCheckCounter <= 0)
                {
                    if(Faction != _playerData.Faction)
                    {
                        record.DetectionCheckCounter = _random.Next(5, 7) + (float)_random.NextDouble();
                    }

                    var detection = _sonarContactTracker
                        .RunSonarDetectionLogic(
                            GetWorld(),
                            GlobalTransform,
                            1.0f,
                            towedArrayActive,
                            contact);

                    var sensorTorpedoes = TorpedoTubes
                        .Where(t => t.ActiveTorpedo != null && IsInstanceValid(t.ActiveTorpedo))
                        .Select(t => t.ActiveTorpedo)
                        .OfType<SensorTorpedo>()
                        .ToArray();

                    foreach (var sensorTorpedo in sensorTorpedoes)
                    {
                        var torpedoDetection = _sonarContactTracker
                            .RunSonarDetectionLogic(
                                GetWorld(),
                                sensorTorpedo.GlobalTransform,
                                sensorTorpedo.SonarSensitivity,
                                false,
                                contact);

                        if (!torpedoDetection.IsTerrainBlocked
                            && (detection.IsTerrainBlocked || torpedoDetection.Noise > detection.Noise))
                        {
                            detection = torpedoDetection;
                        }
                    }

                    record.WasTerrainBlocked = detection.IsTerrainBlocked;
                    record.SignalLevel = detection.IsTerrainBlocked ? 0 : detection.Noise;
                }
                
                if (record.SignalLevel >= environmentNoise)
                {
                    record.LastUpdateTime = _worldManager.CurrentDate;
                }

                record.IsDetected = record.SignalLevel >= environmentNoise;
                record.PositionLocked = record.SignalLevel >= environmentNoise * 2;
                record.CargoScan = record.SignalLevel >= environmentNoise * 16;

                var knowledgeAge = _worldManager.CurrentDate - record.LastUpdateTime;

                if (record.IsDetected)
                {
                    record.TimeDetected += delta;

                    record.TypeKnown = record.TypeKnown || (record.SignalLevel >= environmentNoise * 4);
                    record.Identified = record.Identified || (record.SignalLevel >= environmentNoise * 8);

                    if (record.PositionLocked)
                    {
                        record.PositionEstimateCounter = -1;
                        record.EstimatedPosition = contact.RealPosition;
                        record.LastKnownVelocity = contact.Velocity;
                        record.Altitude = _gameplayRoot.GetAltitude(record.EstimatedPosition);
                    }
                    else
                    {
                        record.PositionEstimateCounter -= delta;

                        if (record.PositionEstimateCounter <= 0)
                        {
                            record.PositionEstimateCounter = (float)(_random.NextDouble() + 0.5f) * 15f;

                            var errorDist = contact.RealPosition.DistanceTo(RealPosition) * 0.1f;

                            record.EstimatedPosition = contact.RealPosition
                                + new Vector3(
                                    (float)(_random.NextDouble() - 0.5f) * errorDist,
                                    (float)(_random.NextDouble() - 0.5f) * errorDist,
                                    (float)(_random.NextDouble() - 0.5f) * errorDist);
                            record.Altitude = _gameplayRoot.GetAltitude(record.EstimatedPosition);
                        }
                    }
                }
                else if (knowledgeAge.TotalSeconds < 120)
                {
                    var estimateDelta = Mathf.Max(delta, record.TimeSinceUpdate);
                    record.EstimatedPosition += record.LastKnownVelocity * estimateDelta;
                    record.IsDetected = true;
                    record.Altitude = _gameplayRoot.GetAltitude(record.EstimatedPosition);
                }

                if (!record.IsDetected)
                {
                    record.AggrivationScore = 0;
                    record.TypeKnown = false;
                    record.PositionLocked = false;
                    record.Identified = false;
                    record.AiData.Clear();
                    record.TimeDetected = 0;
                }

                record.TimeSinceUpdate = 0;
            }
        }

        stopwatch.Stop();
        _perfTracker.LogPerf("Submarine._Process.UpdateSonarContactRecords", stopwatch.ElapsedMilliseconds);
    }

    public override void _PhysicsProcess(float delta)
    {
        var stopwatch = Stopwatch.StartNew();
        base._PhysicsProcess(delta);

        var waterCurrentVelocity = Vector3.Zero;

        if (_gameplayRoot is Gameplay gameplay)
        {
            var currentFactor = 1f;

            var collisionInfo = GetWorld().DirectSpaceState
                .IntersectRay(
                    GlobalTransform.origin,
                    GlobalTransform.origin + UpVector * -1000,
                    null,
                    0b00000000000000000010,
                    collideWithBodies: true,
                    collideWithAreas: true);

            if (collisionInfo.Count > 0)
            {
                DistanceToGround = (GlobalTransform.origin.DistanceTo((Vector3)collisionInfo["position"]));

                currentFactor = Mathf.Clamp(
                    (DistanceToGround - 100) / 300,
                    0, 1);
            }
            else
            {
                DistanceToGround = 1000;
            }

            waterCurrentVelocity = gameplay.GetCurrentFlow(GlobalTransform.origin) * currentFactor;
        }

        if (IsWrecked)
        {
            if (!IsMissionWreck)
            {
                AddCentralForce(UpVector * -Mass * 2);

                ActualThrottle = 0;
                LinearDamp = 0.1f;

                if (_wreckSpinTime > 0)
                {
                    _wreckSpinTime -= delta;
                    AddTorque(_wreckTorque);
                }
            }
        }
        else
        {
            var chargeFactor = 1f;

            if(UsesCharge && Charge <= 0)
            {
                chargeFactor = 0.5f;
            }

            AddCentralForce(Transform.basis.Xform(new Vector3(
                0, 
                _engineModuleType.MaximumThrust * Dive * BallastFactor, 
                ActualThrottle * _engineModuleType.MaximumThrust * chargeFactor)));

            if(SlideForce != null)
            {
                AddCentralForce(SlideForce.Value * _engineModuleType.MaximumThrust * 0.5f);
            }

            // Some subs at high time speed can build up a roll occilation
            // To prevent that, tone down roll strength at high time speed
            var deltaDampner = 0.03f / delta;

            //var rudderThrust = (1 - Mathf.Pow(Mathf.Abs(Throttle) - 0.4f, 2) * 2) * _engineModuleType.RudderThrust;
            var rudderThrust = _engineModuleType.RudderThrust;

            var currentBasis = Transform.basis;

            var xyPlane = new Plane(currentBasis.z, 0);
            var projectedY = xyPlane.Project(UpVector).Normalized();
            var roll = projectedY.AngleTo(currentBasis.y);
            roll *= Mathf.Sign(projectedY.Dot(currentBasis.x));

            var yzPlane = new Plane(currentBasis.x, 0);
            projectedY = yzPlane.Project(UpVector).Normalized();
            var pitch = projectedY.AngleTo(currentBasis.y);
            pitch *= Mathf.Sign(projectedY.Dot(currentBasis.z));

            AddTorque(currentBasis.Xform(new Vector3(
                pitch * Mass * 100 * deltaDampner
                    - LinearVelocity.Length() / 20 * rudderThrust * Dive / 1f * deltaDampner * DivePitchFactor,
                rudderThrust * Rudder * chargeFactor * deltaDampner,
                -roll * Mass * 50 * deltaDampner
                    - Rudder * LinearVelocity.Length() * Mass * deltaDampner * TurningRollFactor)));
        }


        // Custom Drag Logic
        var tansformedVel = Transform.basis.XformInv(LinearVelocity - waterCurrentVelocity);
        AddCentralForce(Transform.basis.Xform(
            new Vector3(
                -tansformedVel.x * HullType.SideDragFactor * _armorType.DragFactor,
                -tansformedVel.y * HullType.SideDragFactor * _armorType.DragFactor,
                -tansformedVel.z * HullType.FrontDragFactor * _armorType.DragFactor)));

        stopwatch.Stop();
        _perfTracker.LogPerf("Submarine._PhysicsProcess", stopwatch.ElapsedMilliseconds);
    }

    public void HitRecieved(ICombatant shooter, float damage, float penetration, Vector3 hitLocation, Vector3 velocity)
    {
        var totalPenetration = velocity.Length() / 100f + penetration;
        var effectiveArmor = Mathf.Max(_armorType.Armor - totalPenetration, 0);
        var penetrationFactor = Mathf.Clamp(1 - effectiveArmor / 25f, 0.1f, 1f);

        var actualDamage = damage * penetrationFactor;

        if(shooter is Submarine submarine && submarine.Faction == _playerData.Faction)
        {
            _bountyTimer = 30f;

            _notificationSystem.AddNotification(new Notification
            {
                IsGood = true,
                Body = $"Hit for {actualDamage.ToString("N0")} damage ({penetrationFactor.ToString("P0")} penetration)"
            });

            if(Faction.TracksCrimes)
            {
                _crimeTracker.ReportCrime(
                    shooter,
                    this,
                    actualDamage);
            }

            if(!Faction.IsHostileTo(shooter.Faction))
            {
                var contact = SonarContactRecords.Values
                    .Where(scr => scr.SonarContact == shooter && scr.IsDetected && scr.Identified)
                    .FirstOrDefault();

                if(contact != null)
                {
                    var newAggrivation = contact.AggrivationScore + actualDamage * Constants.DAMAGE_TO_AGGRIVATION;

                    if(contact.AggrivationScore < 50 
                        && contact.AggrivationScore + newAggrivation >= 50
                        && !IsMissionEnemy)
                    {
                        _notificationSystem.Broadcast(
                            this,
                            "Watch your fire, I'm a friendly!");
                    }

                    contact.AggrivationScore += newAggrivation;

                    if(contact.AggrivationScore >= 100 && !IsMissionEnemy)
                    {
                        BroadcastAggrivation(contact);
                    }
                }
            }

            _simulationEventBus.SendEvent(new PlayerHitSubmarineSimulationEvent(this));
        }
        else if(IsPlayerSub)
        {
            _notificationSystem.AddNotification(new Notification
            {
                IsBad = true,
                Body = $"Recieved {actualDamage.ToString("N0")} damage ({penetrationFactor.ToString("P0")} penetration)"
            });
        }

        ApplyDamage(actualDamage);

        if (LeakBubbles.Count < DetermineTargetLeakCount())
        {
            var newLeak = LeakBubblesPrefab.Instance() as Spatial;
            AddChild(newLeak);
            newLeak.LookAtFromPosition(hitLocation, hitLocation - velocity, UpVector);
            LeakBubbles.Add(newLeak);
        }
    }

    public void BroadcastAggrivation(SonarContactRecord contact)
    {
        var contactObject = contact.SonarContact;

        var nearbyStationContactRecords = _sonarContactTracker.GetSonarContacts()
            .OfType<IStation>()
            .Where(s => s.IsFriendlyTo(this))
            .SelectMany(s => s.SonarContactRecords.Where(scr => scr.SonarContact == contactObject && scr.IsDetected && scr.Identified));

        var nearbySubmarineContactRecords = _sonarContactTracker.GetSonarContacts()
            .OfType<Submarine>()
            .Where(s => s.IsFriendlyTo(this))
            .SelectMany(s => s.SonarContactRecords.Values.Where(scr => scr.SonarContact == contactObject && scr.IsDetected && scr.Identified));

        foreach (var contactRecord in nearbyStationContactRecords.Concat(nearbySubmarineContactRecords))
        {
            contactRecord.AggrivationScore = Mathf.Max(contactRecord.AggrivationScore, contact.AggrivationScore);
        }
    }

    public int DetermineTargetLeakCount()
    {
        return Mathf.FloorToInt((1 -Health / HullType.Health) * MaximumLeakBubbles);
    }

    public void DropDecoy()
    {
        if (_decoyLoadTimer >= DecoyReloadTime)
        {
            _decoyLoadTimer = 0;

            var decoy = DecoyPrefab.Instance() as RigidBody;
            decoy.GlobalTransform = _decoyDropPoint.GlobalTransform;
            decoy.AddCollisionExceptionWith(this);
            GetParent().AddChild(decoy);
            decoy.LinearVelocity = LinearVelocity
                + decoy.GlobalTransform.basis.Xform(new Vector3(0, 0, 10));
        }
    }

    public bool IsHostileTo(ISonarContact other)
    {
        if(Faction.IsHostileTo(other.Faction))
        {
            return true;
        }

        if(SonarContactRecords.Values
            .Any(scr => scr.SonarContact == other && scr.AggrivationScore >= 100))
        {
            return true;
        }

        if (other is Submarine submarine && submarine.IsPlayerSub && IsMissionEnemy)
        {
            return true;
        }

        if (IsPlayerSub && other is Submarine submarine1 && submarine1.IsMissionEnemy)
        {
            return true;
        }

        return false;
    }

    public bool IsFriendlyTo(ISonarContact other)
    {
        if (SonarContactRecords.Values
            .Any(scr => scr.SonarContact == other && scr.AggrivationScore > 100))
        {
            return false;
        }

        if(other is Submarine submarine && submarine.IsPlayerSub && IsMissionEnemy)
        {
            return false;
        }

        if(IsPlayerSub && other is Submarine submarine1 && submarine1.IsMissionEnemy)
        {
            return false;
        }

        return Faction.IsFriendlyTo(other.Faction);
    }

    public void JettisonCargo(CommodityType commodity, float quantity)
    {
        while(quantity > 0)
        {
            var pod = CargoPodPrefab.Instance() as CargoPod;
            var toTake = Mathf.Min(quantity, pod.CargoVolume);
            quantity -= toTake;
            pod.Initialize();
            pod.Faction = Faction;
            pod.CommodityStorage.Add(commodity, toTake);
            _cargoJettisonQueue.Add(pod);
            CommodityStorage.Remove(commodity, toTake);
        }
    }

    public float GetTotalCombatThreat()
    {
        var threat = (HullType.Health * _armorType.Armor) / 100
            + _engineModuleType.CombatThreat
            + _sonarModuleType.CombatThreat
            + (_towedArrayModuleType?.CombatThreat ?? 0)
            + _turretModuleTypes.Sum(x => x?.CombatThreat ?? 0)
            + _launcherModuleTypes.Sum(x => x?.CombatThreat ?? 0)
            + _hangarModuleTypes.Sum(x => x?.CombatThreat ?? 0)
            + _otherModuleTypes.Sum(x => x?.CombatThreat ?? 0)
            + MinisubHangars.Sum(x => x.MinisubType?.GetCombatThreat() ?? 0);

        return threat;
    }

    public SubState BuildSubState()
    {
        var subState = new SubState
        {
            FriendlyName = FriendlyName,
            Health = Health,
            HullType = HullType,
            ArmorType = _armorType,
            EngineModule = _engineModuleType,
            SonarModule = _sonarModuleType,
            TowedArrayModule = _towedArrayModuleType,
            LauncherModules = _launcherModuleTypes,
            TurretModules = _turretModuleTypes,
            HangarModules = _hangarModuleTypes,
            OtherModules = _otherModuleTypes,
            DiveTeams = DiveTeams
                .Select(d => d.DiveTeamType)
                .ToArray(),
            Torpedoes = TorpedoTubes
                .Select(x => x?.TorpedoesAvailable?.ToArray() ?? new TorpedoType[0])
                .ToArray(),
            Minisubs = MinisubHangars
                .Select(h => h?.MinisubType)
                .ToArray(),
            Cargo = CommodityStorage.CommoditiesPresent
                .Select(c => Tuple.Create(c, CommodityStorage.GetQty(c)))
                .ToArray(),
            DockedTo = (IsDockedTo as WorldStation)?.BackingStation,
            ActiveAmmo = TurretSystems
                .Select(t => t?.ActiveAmmo)
                .ToArray(),
            LoadedTorpedo = TorpedoTubes
                .Select(t => t?.ActiveAmmo)
                .ToArray(),
        };

        if(IsDockedTo != null)
        {
            var dockingPoints = IsDockedTo.DockingPoints.ToArray();

            for (var i = 0; i < dockingPoints.Length; i++)
            {
                if(dockingPoints[i].DockingSequence?.Submarine == this)
                {
                    subState.DockNumber = i;
                }
            }
        }

        return subState;
    }

    public SubLoadout GetLoadout()
    {
        return new SubLoadout
        {
            HullType = HullType,
            ArmorType = _armorType,
            EngineModule = _engineModuleType,
            SonarModule = _sonarModuleType,
            TowedArrayModule = _towedArrayModuleType,
            LauncherModules = _launcherModuleTypes.ToArray(),
            TurretModules = _turretModuleTypes.ToArray(),
            HangarModules = _hangarModuleTypes.ToArray(),
            OtherModules = _otherModuleTypes.ToArray(),
            DiveTeams = new DiveTeamType[0],
            Minisubs = new MinisubType[0],
            TorpedoesPerTube = new TorpedoType[0]
        };
    }

    public void BuildModules(SubLoadout subLoadout)
    {
        HullType = subLoadout.HullType;
        _armorType = subLoadout.ArmorType;
        _engineModuleType = subLoadout.EngineModule;
        _sonarModuleType = subLoadout.SonarModule;
        _towedArrayModuleType = subLoadout.TowedArrayModule;
        _turretModuleTypes = subLoadout.TurretModules;
        _launcherModuleTypes = subLoadout.LauncherModules;
        _hangarModuleTypes = subLoadout.HangarModules;
        _otherModuleTypes = subLoadout.OtherModules;
        Health = HullType.Health;

        var usedVolume = _armorType.VolumePerSurfaceArea * HullType.HullSurfaceArea
            + _engineModuleType.Volume
            + (_sonarModuleType?.Volume ?? 0)
            + (_towedArrayModuleType?.Volume ?? 0)
            + _turretModuleTypes.Sum(x => x?.Volume ?? 0)
            + _launcherModuleTypes.Sum(x => x?.Volume ?? 0)
            + _hangarModuleTypes.Sum(x => x?.Volume ?? 0)
            + _otherModuleTypes.Sum(x => x?.Volume ?? 0);

        CargoVolume = (HullType.Volume - usedVolume) * Constants.VOLUME_TO_CARGO;

        if(AgentRepresentation != null)
        {
            CommodityStorage = AgentRepresentation.CommodityStorage;
        }
        else
        {
            CommodityStorage = new CommodityStorage(CargoVolume, false);
        }

        MaxDiveTeams = _otherModuleTypes.OfType<DiveChamberModuleType>().Sum(x => x.DiverCapacity);

        for (var i = 0; i < TurretMounts.Count; i++)
        {
            if (i < _turretModuleTypes.Length)
            {
                TurretMounts[i].Mount(_turretModuleTypes[i]);
            }
            else
            {
                TurretMounts[i].Mount(null);
            }
        }

        TurretSystems = TurretMounts
            .Where(t => t.TurretModuleType != null)
            .GroupBy(t => t.TurretModuleType)
            .Select(g => new TurretSystem(g.Key, g.ToList()))
            .ToList();

        for (var i = 0; i < TorpedoTubes.Count; i++)
        {
            if (i < _launcherModuleTypes.Length)
            {
                TorpedoTubes[i].Mount(_launcherModuleTypes[i]);
            }
            else
            {
                TorpedoTubes[i].Mount(null);
            }
        }

        for (var i = 0; i < MinisubHangars.Count; i++)
        {
            if (i < _hangarModuleTypes.Length)
            {
                MinisubHangars[i].Mount(_hangarModuleTypes[i]);
            }
            else
            {
                MinisubHangars[i].Mount(null);
            }
        }

        foreach (var diveTeamType in subLoadout.DiveTeams)
        {
            if (DiveTeams.Count() < MaxDiveTeams)
            {
                DiveTeams.Add(new DiveTeam(this, diveTeamType, _notificationSystem));
            }
        }

        for (var i = 0; i < subLoadout.TorpedoesPerTube.Length; i++)
        {
            if (TorpedoTubes[i] != null)
            {
                TorpedoTubes[i].TorpedoesAvailable.Clear();

                for (var j = 0; j < TorpedoTubes[i].MagazineCapacity; j++)
                {
                    TorpedoTubes[i].TorpedoesAvailable.Add(subLoadout.TorpedoesPerTube[i]);
                }
            }
        }

        for (var i = 0; i < MinisubHangars.Count; i++)
        {
            if (MinisubHangars[i] != null)
            {
                if (i < subLoadout.Minisubs.Length)
                {
                    MinisubHangars[i].MinisubType = subLoadout.Minisubs[i];
                }
                else
                {
                    MinisubHangars[i].MinisubType = null;
                }

                MinisubHangars[i].RebuildDockedModel();
            }
        }
    }

    public void ApplySubState(SubState subState)
    {
        BuildModules(new SubLoadout
        {
            HullType = subState.HullType,
            ArmorType = subState.ArmorType,
            EngineModule = subState.EngineModule,
            SonarModule = subState.SonarModule,
            TowedArrayModule = subState.TowedArrayModule,
            TurretModules = subState.TurretModules,
            LauncherModules = subState.LauncherModules,
            HangarModules = subState.HangarModules,
            OtherModules = subState.OtherModules,
            DiveTeams = subState.DiveTeams,
            TorpedoesPerTube = new TorpedoType[0],
            Minisubs = new MinisubType[0]
        });

        FriendlyName = subState.FriendlyName;
        Health = subState.Health;

        for(var i = 0; i < TurretSystems.Count && i < subState.ActiveAmmo.Length; i++)
        {
            TurretSystems[i].ChangeAmmo(subState.ActiveAmmo[i]);
        }

        for (var i = 0; i < TorpedoTubes.Count; i++)
        {
            if(i < subState.Torpedoes.Length)
            {
                TorpedoTubes[i].TorpedoesAvailable.Clear();
                TorpedoTubes[i].TorpedoesAvailable.AddRange(subState.Torpedoes[i]);
            }
            else if (i < subState.LoadedTorpedo.Length && subState.LoadedTorpedo[i] != null)
            {
                TorpedoTubes[i].LoadedAmmoIndex = TorpedoTubes[i].TorpedoesAvailable.IndexOf(subState.LoadedTorpedo[i]);
            }
        }

        for (var i = 0; i < MinisubHangars.Count && i < subState.Minisubs.Length; i++)
        {
            MinisubHangars[i].MinisubType = subState.Minisubs[i];
            MinisubHangars[i].RebuildDockedModel();
        }


        foreach (var kvp in subState.Cargo)
        {
            CommodityStorage.Add(kvp.Item1, kvp.Item2);
        }
    }

    public void Collided(Node other)
    {
        if (other is DatumCell
            || other is WorldStation
            || other is Submarine)
        {
            var speed = _lastUpdateVelocity.Length();

            if(speed > 2)
            {
                var damage = Mass / 500 * speed;

                if (IsPlayerSub)
                {
                    _notificationSystem.AddNotification(new Notification
                    {
                        IsBad = true,
                        Body = $"{damage.ToString("N0")} collision damage."
                    });
                }

                if(CollisionSoundPrefab != null)
                {
                    AddChild(CollisionSoundPrefab.Instance());
                }
                
                ApplyDamage(damage);
            }
        }
    }

    private void ApplyDamage(float damage)
    {
        Health -= damage;

        if (Health <= 0 && !IsWrecked)
        {
            TemporaryNoises.Add(new TemporaryNoise
            {
                NoiseAmount = Mathf.Abs(ActualThrottle) * _engineModuleType.MaximumNoise,
                TimeToLive = 60
            });

            IsWrecked = true;
            _wreckTorque = new Vector3(
                (float)(_random.NextDouble() - 0.5f) * Mass * 60,
                (float)(_random.NextDouble() - 0.5f) * Mass * 60,
                (float)(_random.NextDouble() - 0.5f) * Mass * 60);
            _wreckSpinTime = 10;

            _wreckTimer = this.FindParent<GameplayRoot>().StandardWreckLifespan;

            var commoditiesPresent = CommodityStorage.CommoditiesPresent.ToArray();
            foreach (var commodity in commoditiesPresent)
            {
                var toRemove = _random.Next((int)CommodityStorage.GetQty(commodity));
                CommodityStorage.Remove(commodity, toRemove);
            }

            foreach (var commodityQuantity in WreckCommodities)
            {
                CommodityStorage.Add(commodityQuantity.Commodity, commodityQuantity.Quantity);
            }

            if (_bountyTimer > 0)
            {
                var value = HullType.Value
                    + _armorType.ValuePerSurfaceArea * HullType.HullSurfaceArea
                    + _engineModuleType.Value
                    + _sonarModuleType.Value
                    + (_towedArrayModuleType?.Value ?? 0)
                    + _turretModuleTypes.Sum(x => x?.Value ?? 0)
                    + _launcherModuleTypes.Sum(x => x?.Value ?? 0)
                    + _hangarModuleTypes.Sum(x => x?.Value ?? 0)
                    + _otherModuleTypes.Sum(x => x?.Value ?? 0);

                _bountyTracker.AddBounty(Faction, FriendlyName, value);
            }
        }
    }

    public void UnWreck()
    {
        IsWrecked = false;
    }
}
