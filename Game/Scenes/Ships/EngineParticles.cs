using Godot;
using Xenosea.Logic;

public class EngineParticles : Particles
{
    private Submarine _submarine;

    public override void _Ready()
    {
        _submarine = this.FindParent<Submarine>();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_submarine == null)
        {
            if (Emitting)
            {
                Emitting = false;
            }
        }
        else if(_submarine.ActualThrottle > 0 && !_submarine.IsWrecked)
        {
            if(!Emitting)
            {
                Emitting = true;
            }
        }
        else
        {
            if (Emitting)
            {
                Emitting = false;
            }
        }
    }
}
