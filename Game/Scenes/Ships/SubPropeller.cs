using Godot;
using Xenosea.Logic;

public class SubPropeller : Spatial
{
    [Export]
    public float MaxRotationRate = 10f;

    private Submarine _submarine;
    
    public override void _Ready()
    {
        _submarine = this.FindParent<Submarine>();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_submarine != null)
        {
            RotateObjectLocal(Vector3.Forward, delta * MaxRotationRate * _submarine.ActualThrottle);
        }
    }
}
