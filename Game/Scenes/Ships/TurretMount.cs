using Godot;
using Xenosea.Logic;

public class TurretMount : Spatial
{
    [Export]
    public float YawRange;

    [Export]
    public float MinElevation = -30;

    [Export]
    public float MaxElevation = 16;

    public TurretModuleType TurretModuleType { get; private set; }

    public GunTurret Turret { get; private set; }

    public void Mount(TurretModuleType turretType)
    {
        this.ClearChildren();
        TurretModuleType = turretType;

        if (turretType != null)
        {
            //GD.Print("Mounting: " + turretType?.Name);
            Turret = turretType.Prefab.Instance() as GunTurret;
            Turret.TurretModuleType = turretType;
            AddChild(Turret);
            Turret.Transform = Transform.Identity;
            //GD.Print("Mount Successful");
        }
    }
}
