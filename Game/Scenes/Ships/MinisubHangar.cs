using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Scenes.Ships;

public class MinisubHangar : Spatial, IWeaponSystem
{
    public bool CanHoldMinisub => _minisubHangarModuleType != null;

    private MinisubHangarModuleType _minisubHangarModuleType;
    public MinisubType MinisubType { get; set; }

    [Inject]
    private INotificationSystem _notificationSystem;

    public Submarine ActiveMinisub { get; private set; }
    public AbstractSubController ActiveController { get; private set; }
    public float CurrentAmmo { get; private set; }
    public float CurrentHealth { get; private set; }
    public float CurrentCharge { get; private set; }
    public Spatial ApproachPoint { get; private set; }
    public ICombatant Combatant { get; private set; }
    public bool ShouldLaunch { get; private set; }
    public bool SubNearDock { get; private set; }
    public Spatial DockedModel { get; private set; }

    public ITargetableThing Target { get; set; }
    
    public bool ForceShootTarget { get; set; }
    public bool ShouldEscort { get; set; }
    public bool ShouldRecall { get; set; }
    public Vector3? GotoPoint { get; set; }
    public bool IsRebuilding { get; set; }

    public float MaxAmmo { get; private set; }
    private GameplayRoot _gameplayRoot;

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();
        _gameplayRoot = this.FindParent<GameplayRoot>();
        Combatant = this.FindParent<ICombatant>();

        ApproachPoint = GetNode<Spatial>("ApproachPoint");

        RebuildDockedModel();
        CurrentAmmo = MinisubType?.LauncherModules?.Sum(x => x.MagazineCapacity) ?? 0;
        CurrentHealth = MinisubType?.HullType?.Health ?? 0;
        CurrentCharge = MinisubType?.MaxCharge ?? 0;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (ActiveMinisub != null)
        {
            if (!IsInstanceValid(ActiveMinisub))
            {
                ActiveMinisub = null;
                MinisubType = null;
            }
            else
            {
                CurrentHealth = ActiveMinisub.Health;
                CurrentCharge = ActiveMinisub.Charge;
                CurrentAmmo = ActiveMinisub.TorpedoTubes.Sum(tt => tt.TorpedoesAvailable.Count);

                if (!ActiveMinisub.IsWrecked)
                {
                    RelayContactInfoToParent();
                }
            }
        }
        else if (MinisubType != null)
        {
            if (Combatant is Submarine sub && sub.IsDockedTo != null)
            {
                CurrentHealth = MinisubType.HullType.Health;
                CurrentCharge = MinisubType.MaxCharge;
                CurrentAmmo = MaxAmmo;
            }

            var repairRateMod = 1f;
            if(IsRebuilding)
            {
                repairRateMod = 0.5f;
            }

            CurrentHealth = Mathf.Clamp(CurrentHealth + delta * StationRepairDiver.REPAIR_RATE * repairRateMod, 0, MinisubType.HullType.Health);
            CurrentCharge = Mathf.Clamp(CurrentCharge + delta * 10 * repairRateMod, 0, MinisubType.MaxCharge);
            CurrentAmmo = Mathf.Clamp(CurrentAmmo + delta * 0.1f * repairRateMod, 0, MaxAmmo);

            if (CurrentHealth >= MinisubType.HullType.Health)
            {
                IsRebuilding = false;
            }
        }
        else
        {
            CurrentHealth = 0;
            CurrentCharge = 0;
            CurrentAmmo = 0;
        }

        if (Combatant is Submarine submarine)
        {
            if (ShouldLaunch && submarine.AuxilaryLaunchDelay <= 0)
            {
                submarine.AuxilaryLaunchDelay = 5;
                SpawnMinisub();
            }
        }
        else
        {
            if (ShouldLaunch)
            {
                SpawnMinisub();
            }
        }

        if (ActiveMinisub != null)
        {
            SubNearDock = ActiveMinisub.GlobalTransform.origin.DistanceTo(ApproachPoint.GlobalTransform.origin) < 40;
        }
        else
        {
            SubNearDock = false;
        }
    }

    public void MinisubDocked()
    {
        ShouldRecall = true;
        if(ActiveMinisub != null)
        {
            ActiveMinisub.QueueFree();
            ActiveMinisub = null;
        }

        RebuildDockedModel();
    }

    public void RebuildDockedModel()
    {
        if (DockedModel != null)
        {
            DockedModel.QueueFree();
            DockedModel = null;
        }

        if (MinisubType != null)
        {
            DockedModel = MinisubType.DockedModelPrefab.Instance() as Spatial;
            AddChild(DockedModel);
        }

        MaxAmmo = MinisubType?.LauncherModules?.Sum(x => x.MagazineCapacity) ?? 0;
    }

    public void Shoot(ITargetableThing target)
    {
        if (Combatant.IsWrecked)
        {
            return;
        }

        if (ActiveMinisub == null && MinisubType != null && !IsRebuilding)
        {
            ShouldLaunch = true;
        }
    }

    public void Launch()
    {
        if (ActiveMinisub == null && MinisubType != null && !IsRebuilding)
        {
            ShouldLaunch = true;
        }
    }

    private void SpawnMinisub()
    {
        ShouldRecall = false;
        ShouldLaunch = false;

        if (MinisubType == null || ActiveMinisub != null)
        {
            return;
        }

        if (DockedModel != null)
        {
            DockedModel.QueueFree();
            DockedModel = null;
        }

        ActiveMinisub = MinisubType.HullType.SubPrefab.Instance() as Submarine;
        ActiveMinisub.FriendlyName = MinisubType.HullType.Name;
        ActiveMinisub.GlobalTransform = GlobalTransform;
        ActiveMinisub.Faction = Combatant.Faction;

        if (Combatant is Submarine submarine)
        {
            ActiveMinisub.LocalHub = submarine.LocalHub;
            ActiveMinisub.IsMissionEnemy = submarine.IsMissionEnemy;
        }

        ActiveController = MinisubType.ControllerPrefab.Instance() as AbstractSubController;

        ActiveMinisub.AddChild(ActiveController);
        _gameplayRoot.AddChild(ActiveMinisub);
        ActiveMinisub.BuildModules(MinisubType);
        
        if (ActiveController is MinisubController minisubController)
        {
            minisubController.Home = this;
            minisubController.IsUndocking = true;
        }

        if (Combatant is Submarine)
        {
            ActiveMinisub.AddCollisionExceptionWith(Combatant as Node);
        }

        ActiveMinisub.CollisionLayer = 0b00000000000000000000;
        ActiveMinisub.Mode = RigidBody.ModeEnum.Static;
        ActiveMinisub.Health = CurrentHealth;
        ActiveMinisub.Charge = CurrentCharge;

        while (ActiveMinisub.TorpedoTubes.Sum(x => x.TorpedoesAvailable.Count) > CurrentAmmo)
        {
            foreach (var torpedoTube in ActiveMinisub.TorpedoTubes)
            {
                if(ActiveMinisub.TorpedoTubes.Sum(x => x.TorpedoesAvailable.Count) > CurrentAmmo
                    && torpedoTube.TorpedoesAvailable.Any())
                {
                    torpedoTube.TorpedoesAvailable.RemoveAt(torpedoTube.TorpedoesAvailable.Count - 1);
                }
            }
        }
    }

    public void PromptGoalCheck()
    {
        if (ActiveController is MinisubController minisubController)
        {
            minisubController.PromptGoalCheck();
        }
    }

    private void RelayContactInfoToParent()
    {
        SonarContactRecord parentRecordOfSelf = null;
        if (Combatant is IStation station1)
        {
            parentRecordOfSelf = station1.SonarContactRecords
                .Where(x => x.SonarContact == ActiveMinisub)
                .FirstOrDefault();
        }
        else if (Combatant is Submarine submarine1)
        {
            parentRecordOfSelf = submarine1.SonarContactRecords.Values
                .Where(x => x.SonarContact == ActiveMinisub)
                .FirstOrDefault();
        }

        if(parentRecordOfSelf != null)
        {
            parentRecordOfSelf.IsDetected = true;
            parentRecordOfSelf.Identified = true;
            parentRecordOfSelf.TypeKnown = true;
            parentRecordOfSelf.PositionLocked = true;
            parentRecordOfSelf.LastKnownVelocity = ActiveMinisub.LinearVelocity;
            parentRecordOfSelf.EstimatedPosition = ActiveMinisub.RealPosition;
            parentRecordOfSelf.Altitude = ActiveMinisub.Altitude;
        }

        foreach (var contactRecord in ActiveMinisub.SonarContactRecords.Values)
        {
            SonarContactRecord parentRecord = null;
            if(Combatant is IStation station)
            {
                parentRecord = station.SonarContactRecords
                    .Where(x => x.SonarContact == contactRecord.SonarContact)
                    .FirstOrDefault();
            }
            else if(Combatant is Submarine submarine)
            {
                parentRecord = submarine.SonarContactRecords.Values
                    .Where(x => x.SonarContact == contactRecord.SonarContact)
                    .FirstOrDefault();
            }

            if(parentRecord != null)
            {
                parentRecord.IsDetected = parentRecord.IsDetected || contactRecord.IsDetected;
                parentRecord.Identified = parentRecord.Identified || contactRecord.Identified;
                parentRecord.TypeKnown = parentRecord.TypeKnown || contactRecord.TypeKnown;
                
                if(contactRecord.PositionLocked && !parentRecord.PositionLocked)
                {
                    parentRecord.PositionLocked = true;
                    parentRecord.LastKnownVelocity = contactRecord.LastKnownVelocity;
                    parentRecord.EstimatedPosition = contactRecord.EstimatedPosition;
                    parentRecord.Altitude = contactRecord.Altitude;
                }
            }
        }

        IEnumerable<SonarContactRecord> parentRecords;

        if (Combatant is IStation station2)
        {
            parentRecords = station2.SonarContactRecords
                .ToArray();
        }
        else if (Combatant is Submarine submarine)
        {
            parentRecords = submarine.SonarContactRecords.Values
                .ToArray();
        }
        else
        {
            parentRecords = Enumerable.Empty<SonarContactRecord>();
        }

        foreach (var parentRecord in parentRecords)
        {
            var ownRecord = ActiveMinisub.SonarContactRecords.Values
                    .Where(x => x.SonarContact == parentRecord.SonarContact)
                    .FirstOrDefault();

            if (ownRecord != null)
            {
                parentRecord.IsDetected = parentRecord.IsDetected || parentRecord.IsDetected;
                parentRecord.Identified = parentRecord.Identified || parentRecord.Identified;
                parentRecord.TypeKnown = parentRecord.TypeKnown || parentRecord.TypeKnown;

                if (parentRecord.PositionLocked && !parentRecord.PositionLocked)
                {
                    parentRecord.PositionLocked = true;
                    parentRecord.LastKnownVelocity = parentRecord.LastKnownVelocity;
                    parentRecord.EstimatedPosition = parentRecord.EstimatedPosition;
                    parentRecord.Altitude = parentRecord.Altitude;
                }
            }
        }
    }

    public void Mount(MinisubHangarModuleType minisubHangarModuleType)
    {
        _minisubHangarModuleType = minisubHangarModuleType;

        if(_minisubHangarModuleType == null)
        {
            MinisubType = null;
        }
    }
}
