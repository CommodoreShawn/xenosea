using Godot;
using Xenosea.Logic;

public class HangarDoor : Spatial
{
    [Export]
    public float OpenRadians;

    [Export]
    public float OpenTime;

    private Submarine _submarine;
    private float _closedRadians;
    private float _openProgress;

    public override void _Ready()
    {
        _submarine = this.FindParent<Submarine>();
        _closedRadians = 0;
        _openProgress = 0;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_submarine != null)
        {
            if(_submarine.HangarDoorsOpen)
            {
                _openProgress = Mathf.Clamp(_openProgress + 1 / OpenTime * delta, 0, 1);
            }
            else
            {
                _openProgress = Mathf.Clamp(_openProgress - 1 / OpenTime * delta, 0, 1);
            }

            Rotation = new Vector3(
                Rotation.x,
                Rotation.y,
                _closedRadians + _openProgress * OpenRadians);
        }
    }
}
