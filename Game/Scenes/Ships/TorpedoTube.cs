using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Scenes.Ships;

public class TorpedoTube : Spatial, IWeaponSystem
{
    private TorpedoLauncherModuleType _torpedoLauncherModuleType;

    public int MagazineCapacity => _torpedoLauncherModuleType?.MagazineCapacity ?? 0;
    public float LoadTime => _torpedoLauncherModuleType?.ReloadTime ?? 1;
    public bool IsActive => _torpedoLauncherModuleType != null;
    public TorpedoSizeCategory MaximumTorpedoSize => _torpedoLauncherModuleType?.MaximumTorpedoSize ?? TorpedoSizeCategory.Large;

    [Inject]
    private INotificationSystem _notificationSystem;

    [Export]
    public float WireRadians = 1.57f;

    private readonly float _wireDistance = 5000;

    public float LoadProgress { get; set; }

    private bool _loading;
    private bool _unloading;

    public Torpedo ActiveTorpedo { get; set; }
    public ITargetableThing Target { get; set; }
    private ICombatant _combatant;
    private float _wireGracePeriod;

    public List<TorpedoType> TorpedoesAvailable { get; set; }

    public int LoadedAmmoIndex { get; set; }
    public int NextAmmoIndex { get; set; }

    public bool ActiveTargeting { get; set; }
    private GameplayRoot _gameplayRoot;

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();

        _gameplayRoot = this.FindParent<GameplayRoot>();
        LoadedAmmoIndex = 0;
        NextAmmoIndex = 0;
        LoadProgress = 0;
        _combatant = this.FindParent<ICombatant>();
        _loading = true;
        TorpedoesAvailable = new List<TorpedoType>();
        ActiveTargeting = true;
    }

    public void Mount(TorpedoLauncherModuleType moduleType)
    {
        _torpedoLauncherModuleType = moduleType;
    }

    public TorpedoType ActiveAmmo
    {
        get => LoadedAmmoIndex < TorpedoesAvailable.Count ? TorpedoesAvailable[LoadedAmmoIndex] : null;
    }
    
    public override void _Process(float delta)
    {
        base._Process(delta);
        
        if (_combatant != null
            && !_combatant.IsWrecked 
            && _torpedoLauncherModuleType != null)
        {
            if (ActiveTorpedo == null)
            {
                if (_loading && TorpedoesAvailable.Any())
                {
                    LoadProgress += delta;

                    if (LoadProgress >= _torpedoLauncherModuleType.ReloadTime)
                    {
                        LoadProgress = _torpedoLauncherModuleType.ReloadTime;
                        _loading = false;

                    }
                }

                if (_unloading)
                {
                    LoadProgress -= delta;

                    if (LoadProgress <= 0)
                    {
                        LoadProgress = 0;
                        _unloading = false;
                        _loading = true;
                        LoadedAmmoIndex = NextAmmoIndex;
                    }
                }
            }
            else if (!IsInstanceValid(ActiveTorpedo))
            {
                ActiveTorpedo = null;
            }
            else
            {
                var toTorpeo = GlobalTransform.origin.DirectionTo(ActiveTorpedo.GlobalTransform.origin);

                _wireGracePeriod -= delta;

                if (_wireGracePeriod <= 0
                    && (
                        GlobalTransform.basis.z.AngleTo(toTorpeo) > WireRadians 
                        || GlobalTransform.origin.DistanceTo(ActiveTorpedo.GlobalTransform.origin) > _wireDistance))
                {
                    ActiveTorpedo.ActiveSeeking = true;
                    ActiveTorpedo.OnWire = false;
                    ActiveTorpedo = null;

                    if (_combatant is Submarine sub && sub.IsPlayerSub)
                    {
                        _notificationSystem.AddNotification(
                            new Notification
                            {
                                IsBad = true,
                                Body = $"Torpeo wire broken!"
                            });
                    }
                }
            }
        }
    }

    public void ChangeAmmo(int index)
    {
        if (index >= 0 && index < TorpedoesAvailable.Count)
        {
            if (index == LoadedAmmoIndex)
            {
                if (_unloading)
                {
                    _unloading = false;
                    _loading = true;
                }

                NextAmmoIndex = index;
            }
            else if (index != LoadedAmmoIndex)
            {
                NextAmmoIndex = index;
                _unloading = true;
                _loading = false;
            }
        }
    }

    public void Shoot(ITargetableThing target)
    {
        if (_torpedoLauncherModuleType != null
            && LoadProgress == _torpedoLauncherModuleType.ReloadTime
            && ActiveTorpedo == null
            && !_combatant.IsWrecked)
        {
            _wireGracePeriod = 4;
            _loading = true;
            LoadProgress = 0;

            var launchEffect = ActiveAmmo.LaunchEffectPrefab.Instance() as Spatial;
            launchEffect.GlobalTransform = GlobalTransform;

            var torpedo = ActiveAmmo.TorpedoPrefab.Instance() as Torpedo;
            torpedo.GlobalTransform = GlobalTransform;
            ActiveTorpedo = torpedo;

            torpedo.TargetPoint = _combatant.AimPoint;

            _gameplayRoot.AddChild(torpedo);
            _gameplayRoot.AddChild(launchEffect);

            torpedo.LinearVelocity = (_combatant as RigidBody)?.LinearVelocity ?? Vector3.Zero;
            torpedo.Shooter = _combatant;

            torpedo.Target = Target;
            torpedo.OnWire = true;
            torpedo.AddCollisionExceptionWith(_combatant as Spatial);

            _combatant.TemporaryNoises.Add(new TemporaryNoise
            {
                NoiseAmount = ActiveAmmo.LaunchingNoise,
                TimeToLive = 30
            });

            TorpedoesAvailable.RemoveAt(LoadedAmmoIndex);

            if (LoadedAmmoIndex >= TorpedoesAvailable.Count)
            {
                LoadedAmmoIndex = 0;
            }
        }
    }
}
