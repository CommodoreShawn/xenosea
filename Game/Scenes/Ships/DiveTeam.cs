﻿using Godot;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Scenes.Ships
{
    public class DiveTeam
    {
        private Submarine _parentSubmarine;
        public DiveTeamType DiveTeamType { get; set; }
        public Diver DeployedDiver { get; set; }
        public bool DiverIsDead { get; private set; }
        public bool DiverIsRecovering => DeployedDiver == null &&_diverRecoverTimer > 0;
        public string Name => DiveTeamType.Name;
        private ITargetableThing _currentTarget;
        private float _diverRecoverTimer;
        public bool HasTarget => _shouldDeploy;
        private bool _shouldDeploy;

        private readonly INotificationSystem _notificationSystem;

        public DiveTeam(
            Submarine parentSubmarine, 
            DiveTeamType diveTeamType,
            INotificationSystem notificationSystem)
        {
            _notificationSystem = notificationSystem;
            _parentSubmarine = parentSubmarine;
            DiveTeamType = diveTeamType;
        }

        public void ReturnHome()
        {
            _currentTarget = null;
            _shouldDeploy = false;

            if(DeployedDiver != null)
            {
                DeployedDiver.ReturnToHome();
            }
        }

        public bool IsValidTarget(ITargetableThing target)
        {
            return DiveTeamType.IsValidTarget(_parentSubmarine, target);
        }

        public void Deploy(ITargetableThing target)
        {
            if(IsValidTarget(target))
            {
                _shouldDeploy = true;

                _currentTarget = target;

                if (DeployedDiver != null)
                {
                    DeployedDiver.SetTarget(target);
                }
            }
        }

        public void Process(float delta)
        {
            if(DeployedDiver != null && !Object.IsInstanceValid(DeployedDiver))
            {
                DeployedDiver = null;
                _diverRecoverTimer = DiveTeamType.RecoverTime;
            }

            if(DeployedDiver != null)
            {
                if(DeployedDiver.DiveTimeLeft <= 0)
                {
                    DiverIsDead = true;
                    DeployedDiver.QueueFree();
                    DeployedDiver = null;

                    if(_parentSubmarine.IsPlayerSub)
                    {
                        _notificationSystem.AddNotification(new Logic.Notification
                        {
                            IsMessage = true,
                            IsBad = true,
                            Body = $"{DiveTeamType.Name} ran out of oxygen and died."
                        });
                    }
                }
            }
            else
            {
                _diverRecoverTimer -= delta;

                if(_diverRecoverTimer <= 0
                    && _shouldDeploy)
                { 
                    if(!IsValidTarget(_currentTarget))
                    {
                        _shouldDeploy = false;
                        _currentTarget = null;
                    }
                    else if(_parentSubmarine.HangarDoorsOpen && _parentSubmarine.AuxilaryLaunchDelay <= 0)
                    {
                        DeployDiver();
                    }
                }
            }
        }

        private void DeployDiver()
        {
            _parentSubmarine.AuxilaryLaunchDelay = 3f;
            DeployedDiver = DiveTeamType.DiverPrefab.Instance() as Diver;
            DeployedDiver.ParentSubmarine = _parentSubmarine;
            _parentSubmarine.GetParent().AddChild(DeployedDiver);
            DeployedDiver.GlobalTransform = _parentSubmarine.DiveHatch.GlobalTransform;
            DeployedDiver.SetTarget(_currentTarget);
        }
    }
}
