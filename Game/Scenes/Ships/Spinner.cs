using Godot;
using System;

public class Spinner : Spatial
{
    [Export]
    public float RotationRate;

    public override void _Process(float delta)
    {
        base._Process(delta);

        RotateY(RotationRate * delta);
    }
}
