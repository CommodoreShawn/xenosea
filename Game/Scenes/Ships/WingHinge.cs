using Godot;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.AI;

public class WingHinge : Spatial
{
    [Export]
    public float ClosedAngle;

    [Export]
    public float ClosedTime;

    private MinisubController _controller;

    private float _transitionTimer;
    private bool _isClosed;

    public override void _Ready()
    {
        _controller = this.FindParent<Submarine>().GetNode<MinisubController>("MinisubController");
        _transitionTimer = ClosedTime;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _isClosed = false;

        if (_controller != null)
        {
            if (_controller.ActiveGoals.Any()
                && (_controller.ActiveGoals.First() is UndockMinisubGoal
                    || _controller.ActiveGoals.First() is ReturnHomeMinisubGoal rhg && rhg.InDockSlide))
            {
                _isClosed = true;
            }

            if (_isClosed)
            {
                _transitionTimer = Mathf.Min(_transitionTimer + delta, ClosedTime);
            }
            else
            {
                _transitionTimer = Mathf.Max(_transitionTimer - delta, 0);
            }

            RotationDegrees = new Vector3(
                0,
                ClosedAngle * (_transitionTimer / ClosedTime),
                0);
        }

    }
}
