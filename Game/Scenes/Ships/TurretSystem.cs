using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Detection;
using Xenosea.Scenes.Ships;

public class TurretSystem : IWeaponSystem
{
    public IEnumerable<TurretMount> Mounts { get; private set; }
    public AmmunitionType ActiveAmmo { get; private set; }
    public TurretModuleType TurretModuleType { get; private set; }

    public TurretSystem(TurretModuleType turretModuleType, List<TurretMount> mounts)
    {
        TurretModuleType = turretModuleType;
        Mounts = mounts;
        ChangeAmmo(TurretModuleType.AmmunitionOptions.First());

        foreach (var turret in mounts.Select(m => m.Turret))
        {
            if(turret != null)
            {
                turret.TurretSystem = this;
            }
        }
    }

    public void ChangeAmmo(AmmunitionType ammoType)
    {
        if (ActiveAmmo != ammoType)
        {
            ActiveAmmo = ammoType;
            foreach (var mount in Mounts)
            {
                if (mount.Turret != null)
                {
                    mount.Turret.ReloadCounter = 0;
                }
            }
        }
    }

    public void Shoot(ITargetableThing target)
    {
        foreach(var mount in Mounts)
        {
            if(mount.Turret != null)
            {
                mount.Turret.Shoot(target);
            }
        }
    }
}
