using Godot;
using Xenosea.Logic;

public class EngineSoundPlayer : AudioStreamPlayer3D
{
    private Submarine _submarine;

    [Export]
    public float PitchAdjust = 1;

    public override void _Ready()
    {
        base._Ready();
        _submarine = this.FindParent<Submarine>();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_submarine == null || _submarine.IsWrecked)
        {
            Playing = false;
        }
        else
        {
            var throttlePerc = Mathf.Abs(_submarine.ActualThrottle);

            MaxDb = GD.Linear2Db(Mathf.Clamp(throttlePerc * 2, 0, 1));
            PitchScale = PitchAdjust + throttlePerc / 2;
        }
    }
}
