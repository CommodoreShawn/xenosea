﻿namespace Xenosea.Scenes.Ships
{
    public class ItemInventory
    {
        public float VolumeLimit { get; private set; }

        public ItemInventory(float volumeLimit)
        {
            VolumeLimit = volumeLimit;
        }

    }
}
