using Godot;
using Xenosea.Logic.Infrastructure.Data;

public class StormIndicator : Spatial
{
    private CPUParticles _particles;

    public float SizeScale { get; set; }
    public WeatherSystem WeatherSystem { get; set; }
    
    public override void _Ready()
    {
        _particles = GetNode<CPUParticles>("CPUParticles");
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _particles.EmissionRingRadius = WeatherSystem.Radius * SizeScale;
        LookAtFromPosition(WeatherSystem.Position * SizeScale, Vector3.Zero, Vector3.Up);

        if (WeatherSystem.Energy <= 0)
        {
            QueueFree();
        }
    }
}
