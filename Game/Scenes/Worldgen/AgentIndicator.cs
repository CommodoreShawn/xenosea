using Godot;
using Xenosea.Logic.Infrastructure.Data;

public class AgentIndicator : Spatial
{
    public SimulationAgent Agent { get; set; }

    private Particles _battleParticles;

    public override void _Ready()
    {
        base._Ready();

        _battleParticles = GetNode<Particles>("BattleParticles");

        if(Agent != null)
        {
            GetNode<GeometryInstance>("MeshInstance").MaterialOverride = new SpatialMaterial
            {
                AlbedoColor = Agent.Faction.Color
            };
        }
    }

    public override void _Process(float delta)
    {
        _battleParticles.Emitting = Agent?.CurrentBattle != null;

        if(Agent != null)
        {
            Vector3 position;
            Vector3 lookAt;
            float depth;

            if(Agent.MovingTo == null || Agent.MovementProgress == 0)
            {
                position = Agent.Location.Position / 500;
                lookAt = position + GlobalTransform.basis.z;
                depth = Mathf.Max(0, Agent.Location.Depth - 100);
            }
            else
            {

                position = Agent.Location.Position * (1 - Agent.MovementProgress)
                    + Agent.MovingTo.Position * Agent.MovementProgress;
                position /= 500;
                lookAt = Agent.MovingTo.Position / 500;
                depth = Agent.Location.Depth * (1 - Agent.MovementProgress)
                    + Agent.MovingTo.Depth * Agent.MovementProgress;
                depth = Mathf.Max(0, depth - 100);
            }

            depth = Mathf.Min(Constants.DEPTH_MAX_SUIT, depth);

            position -= position.Normalized() * depth / 200;
            lookAt -= position.Normalized() * depth / 200;

            LookAtFromPosition(position, lookAt, position.Normalized());

            if(Agent.Health <= 0)
            {
                QueueFree();
            }
        }
    }
}
