using Godot;
using System;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Data;

public class FarmPlot : Spatial
{
    [Export]
    public PackedScene[] CropPrefabs;
    [Export]
    public PackedScene FarmerDiverPrefab;

    public Vector3[] CropPoints { get; private set; }

    private bool _spawnsFarmer;
    private bool _farmerSpawned;

    public override void _Ready()
    {
        CropPoints = new Vector3[0];
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_spawnsFarmer && !_farmerSpawned)
        {
            _farmerSpawned = true;

            var farmer = FarmerDiverPrefab.Instance() as FarmerDiver;
            farmer.Field = this;
            this.FindParent<GameplayRoot>().AddChild(farmer);
            
            farmer.LookAtFromPosition(
                GlobalTransform.origin + GlobalTransform.origin.Normalized() * 10,
                Vector3.Zero,
                Vector3.Up);
        }
    }

    public void InitializePlants(
        Random random,
        FarmPlotBuildout farmBuildout)
    {
        _spawnsFarmer = random.NextDouble() < 0.5;

        CropPoints = farmBuildout.PlantPositions
            .Concat(farmBuildout.PlantPositions.Select(x => x + x.Normalized() * 3))
            .Concat(farmBuildout.PlantPositions.Select(x => x + x.Normalized() * 6))
            .ToArray();

        foreach (var plantPosition in farmBuildout.PlantPositions)
        {
            var plant = CropPrefabs[random.Next(CropPrefabs.Length)].Instance() as Spatial;
            AddChild(plant);

            plant.LookAtFromPosition(
                plantPosition,
                Vector3.Zero,
                Vector3.Up);
        }
    }
}
