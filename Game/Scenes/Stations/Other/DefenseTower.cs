using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Docking;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

public class DefenseTower : Spatial, ISonarContact, ICombatant, IStation
{
    [Export]
    public string FriendlyName { get; set; }
    [Export]
    public float ConstantNoise = 2000;
    [Export]
    public Faction Faction { get; set; }
    [Export]
    public float MaximumHealth { get; set; }
    [Export]
    public float Armor { get; set; }
    [Export]
    public PackedScene LeakBubblesPrefab { get; set; }
    [Export]
    public int MaximumLeakBubbles { get; set; }
    [Export]
    public TorpedoType TorpedoReloadType;
    [Export]
    public TurretModuleType[] TurretTypes;
    [Export]
    public TorpedoLauncherModuleType TorpedoTubeType;

    public string ReferenceId => Name;
    public ulong Id { get; private set; }
    public Vector3 RealPosition => GlobalTransform.origin;
    public float BracketSizeFactor => 0;
    public float OverallNoise { get; set; }
    public bool IsPlayerVisible { get; set; }
    public bool IsDockable => false;
    public ContactType ContactType => IsWrecked ? ContactType.Wreck : ContactType.Outpost;
    public bool IsWrecked { get; private set; }
    public Vector3 Velocity => Vector3.Zero;
    public float SpeakingTimer { get; set; }
    public string DebugText { get; set; }

    public float Health { get; private set; }
    public Vector3 AimPoint { get; set; }
    public float HealthPercent => Health / MaximumHealth;
    public List<TemporaryNoise> TemporaryNoises { get; private set; }
    public List<TurretSystem> TurretSystems { get; private set; }
    public List<TorpedoTube> TorpedoTubes { get; private set; }
    public List<MinisubHangar> MinisubHangars { get; private set; }
    public List<SonarContactRecord> SonarContactRecords { get; private set; }
    public CommodityStorage CommodityStorage { get; }
    public Spatial DockWaitingArea { get; }
    public Spatial CameraCenter { get; }
    public int CommodityCapacity { get; }
    public List<StationRoom> StationRooms { get; }
    public IEnumerable<DockingPoint> DockingPoints { get; }
    public float MinCameraDistance { get; }
    public float MaxCameraDistance { get; }

    private System.Random _random;
    private List<Spatial> _leakBubbles;

    private bool _useOriginRelativeUp;
    private Vector3 _upVector;
    private GameplayRoot _gameplayRoot;
    private double _sonarDelay;

    [Inject]
    private ISonarContactTracker _sonarContactTracker;
    [Inject]
    private INotificationSystem _notificationSystem;
    [Inject]
    private ICrimeTracker _crimeTracker;

    public override void _Ready()
    {
        this.ResolveDependencies();
        Id = GetInstanceId();
        _random = new System.Random();

        TurretSystems = new List<TurretSystem>();
        TorpedoTubes = new List<TorpedoTube>();
        MinisubHangars = new List<MinisubHangar>();
        TemporaryNoises = new List<TemporaryNoise>();
        _sonarContactTracker.AddSonarContact(this);
        SonarContactRecords = new List<SonarContactRecord>();
        Health = MaximumHealth;
        _leakBubbles = new List<Spatial>();

        var turretMounts = new List<TurretMount>();

        foreach (var child in GetChildren())
        {
            if (child is TurretMount turretMount)
            {
                turretMounts.Add(turretMount);
            }
            else if (child is TorpedoTube torpedoTube)
            {
                TorpedoTubes.Add(torpedoTube);
                torpedoTube.Mount(TorpedoTubeType);
            }
            else if (child is MinisubHangar minisubHangar)
            {
                MinisubHangars.Add(minisubHangar);
            }
        }

        for(var i = 0; i < turretMounts.Count; i++)
        {
            if (i < TurretTypes.Length)
            {
                turretMounts[i].Mount(TurretTypes[i]);
            }
        }

        if (turretMounts.Any())
        {
            TurretSystems.AddRange(
                turretMounts
                    .Where(t => t.TurretModuleType != null)
                    .GroupBy(t => t.TurretModuleType)
                    .Select(g => new TurretSystem(g.Key, g.ToList())));
        }

        _gameplayRoot = this.FindParent<GameplayRoot>();
        _useOriginRelativeUp = _gameplayRoot.UseOriginRelativeUp;
        _upVector = Vector3.Up;
    }

    public override void _ExitTree()
    {
        base._ExitTree();

        _sonarContactTracker.RemoveSonarContact(this);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(SpeakingTimer > 0)
        {
            SpeakingTimer -= delta;
        }

        if (_useOriginRelativeUp)
        {
            _upVector = GlobalTransform.origin.Normalized();
        }

        UpdateNoises(delta);


        if (!IsWrecked)
        {
            UpdateSonarContactRecords(delta);

            foreach(var torpedoTube in TorpedoTubes)
            {
                if(torpedoTube.TorpedoesAvailable.Count < torpedoTube.MagazineCapacity)
                {
                    torpedoTube.TorpedoesAvailable.Add(TorpedoReloadType);
                }
            }
        }

        Visible = IsPlayerVisible || IsWrecked;
    }

    private void UpdateNoises(float delta)
    {
        if (IsWrecked)
        {
            OverallNoise = 0;
        }
        else
        {
            OverallNoise = 0;
            foreach (var noiseSource in TemporaryNoises)
            {
                noiseSource.Elapsed += delta;

                OverallNoise += (1 - noiseSource.Elapsed / noiseSource.TimeToLive) * noiseSource.NoiseAmount;
            }

            OverallNoise += ConstantNoise;

            TemporaryNoises.RemoveAll(x => x.Elapsed > x.TimeToLive);
        }
    }

    private void UpdateSonarContactRecords(float delta)
    {
        var newRecords = new List<SonarContactRecord>();


        if (Engine.TimeScale > 0)
        {
            // Make sonar delay independent from engine time scale
            _sonarDelay -= delta / Engine.TimeScale;
        }

        if (_sonarDelay <= 0)
        {
            _sonarDelay = _random.NextDouble() * 6;

            foreach (var contact in _sonarContactTracker.GetSonarContacts())
            {
                if (contact != this)
                {
                    var record = SonarContactRecords
                        .Where(r => r.SonarContact == contact)
                        .FirstOrDefault();

                    if (record == null)
                    {
                        record = new SonarContactRecord
                        {
                            SonarContact = contact,
                            PositionEstimateCounter = -1
                        };
                    }

                    var environmentNoise = 5;
                    var detectorGain = 1f;
                    var distance = (RealPosition - contact.RealPosition).Length();

                    var detectedNoise = contact.OverallNoise / (4 * Mathf.Pi * Mathf.Pow(Constants.SONAR_ENV_PROPAGATION * distance, 2)) * detectorGain;

                    record.IsDetected = detectedNoise >= environmentNoise;
                    record.PositionLocked = detectedNoise >= environmentNoise * 2;

                    if (record.IsDetected)
                    {
                        record.TypeKnown = record.TypeKnown || (detectedNoise >= environmentNoise * 4);
                        record.Identified = record.Identified || (detectedNoise >= environmentNoise * 8);

                        if (record.PositionLocked)
                        {
                            record.PositionEstimateCounter = -1;
                            record.EstimatedPosition = contact.RealPosition;
                            record.Altitude = _gameplayRoot.GetAltitude(record.EstimatedPosition);
                        }
                        else
                        {
                            record.PositionEstimateCounter -= delta;

                            if (record.PositionEstimateCounter <= 0)
                            {
                                record.PositionEstimateCounter = (float)(_random.NextDouble() + 0.5f) * 15f;

                                var errorDist = distance * 0.5f;

                                record.EstimatedPosition = contact.RealPosition
                                    + new Vector3(
                                        (float)(_random.NextDouble() - 0.5f) * errorDist,
                                        (float)(_random.NextDouble() - 0.5f) * errorDist,
                                        (float)(_random.NextDouble() - 0.5f) * errorDist);
                                record.Altitude = _gameplayRoot.GetAltitude(record.EstimatedPosition);
                            }
                        }

                        newRecords.Add(record);
                    }
                }
            }
        }

        SonarContactRecords = newRecords;
    }

    public void HitRecieved(ICombatant shooter, float damage, float penetration, Vector3 hitLocation, Vector3 velocity)
    {
        var penetrationFactor = 1f;

        if (Armor > 0)
        {
            var totalPenetration = velocity.Length() / 100f + penetration;
            penetrationFactor = Mathf.Clamp(totalPenetration / Armor, 0.1f, 1f);
        }

        var actualDamage = damage * penetrationFactor;

        Health -= actualDamage;

        if (shooter is Submarine submarine && submarine.IsPlayerSub)
        {
            _notificationSystem.AddNotification(new Notification
            {
                IsGood = true,
                Body = $"Hit for {actualDamage.ToString("N0")} damage ({penetrationFactor.ToString("P0")} penetration)"
            });

            if (Faction.TracksCrimes)
            {
                _crimeTracker.ReportCrime(
                    shooter,
                    this,
                    actualDamage);
            }
        }

        if (_leakBubbles.Count < DetermineTargetLeakCount())
        {
            var newLeak = LeakBubblesPrefab.Instance() as Spatial;
            AddChild(newLeak);
            newLeak.LookAtFromPosition(hitLocation, hitLocation - velocity, _upVector);
            _leakBubbles.Add(newLeak);
        }

        if (Health <= 0 && !IsWrecked)
        {
            IsWrecked = true;
        }
    }

    private int DetermineTargetLeakCount()
    {
        return (int)Mathf.Round((Health / MaximumHealth) * MaximumLeakBubbles);
    }

    public void Broadcast(string message)
    {
        _notificationSystem.Broadcast(this, message);
    }

    public bool IsHostileTo(ISonarContact other)
    {
        if (Faction.IsHostileTo(other.Faction))
        {
            return true;
        }

        if (SonarContactRecords
            .Any(scr => scr.SonarContact == other && scr.AggrivationScore > 100))
        {
            return true;
        }

        return false;
    }

    public bool IsFriendlyTo(ISonarContact other)
    {
        if (SonarContactRecords
            .Any(scr => scr.SonarContact == other && scr.AggrivationScore > 100))
        {
            return false;
        }

        return Faction.IsFriendlyTo(other.Faction);
    }

    public DockingSequence RequestDocking(Submarine submarine)
    {
        return null;
    }

    public void CancelDocking(DockingSequence dockingSequence)
    {
    }

    public float PriceOf(CommodityType commodity)
    {
        return 0;
    }
}
