using Godot;
using System;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;

public class WorldStationContainer : Spatial
{
    [Export]
    public PackedScene BarRoomPrefab;
    [Export]
    public PackedScene EquipmentRoomPrefab;
    [Export]
    public PackedScene WarehouseRoomPrefab;
    [Export]
    public PackedScene SecurityRoomPrefab;
    [Export]
    public PackedScene BountyRoomPrefab;

    public override void _Ready()
    {
    }

    public void BuildStation(SimulationStation simulationStation)
    {
        var random = new Random(simulationStation.Location.Id.GetHashCode());

        var worldStation = GetNode<WorldStation>("Station");

        var upPoint = simulationStation.StationBuildout.CenterPosition * 2;

        worldStation.LookAtFromPosition(
            simulationStation.StationBuildout.CenterPosition,
            upPoint,
            Vector3.Up);

        foreach (var tower in simulationStation.StationBuildout.Towers)
        {
            var foundation = new StaticBody()
            {
                Name = "Tower",
            };
            worldStation.AddChild(foundation);
            foundation.LookAtFromPosition(
                tower.FoundationPosition,
                upPoint,
                Vector3.Up);

            foundation.AddChild(new CollisionShape
            {
                Shape = new CylinderShape
                {
                    Radius = simulationStation.StationBuildout.StationModuleSet.SectionRadius,
                    Height = simulationStation.StationBuildout.StationModuleSet.SectionHeight
                    * tower.Sections.Length * 2
                    + tower.Sections.Length * 2,
                },
                Transform = Transform.Identity.Rotated(Vector3.Right, Mathf.Pi / 2)
            });

            for (var i = -2; i < tower.Sections.Length + 1; i++)
            {
                PackedScene prefab;
                if (i < 0)
                {
                    prefab = simulationStation.StationBuildout.StationModuleSet.BaseSectionPrefab;
                }
                else if (i < tower.Sections.Length)
                {
                    var process = tower.Sections[i];

                    if (process.IsIndustrial)
                    {
                        prefab = simulationStation.StationBuildout.StationModuleSet.IndustrialSectionPrefab;
                    }
                    else if (process.IsCivil)
                    {
                        prefab = simulationStation.StationBuildout.StationModuleSet.CivilianSectionPrefab;
                    }
                    else if (process.IsDefense)
                    {
                        prefab = simulationStation.StationBuildout.StationModuleSet.DefenseSectionPrefab;
                    }
                    else
                    {
                        prefab = simulationStation.StationBuildout.StationModuleSet.BaseSectionPrefab;
                    }
                }
                else
                {
                    prefab = simulationStation.StationBuildout.StationModuleSet.CapPrefab;
                }

                var section = prefab.Instance() as Spatial;

                foundation.AddChild(section);
                section.Transform = Transform.Identity
                    .LookingAt(Vector3.Up, Vector3.Forward)
                    .Translated(Vector3.Up * i * simulationStation.StationBuildout.StationModuleSet.SectionHeight);
            }

            foreach (var bridge in tower.Bridges)
            {
                var bridgeSegmentCount = Mathf.Max(bridge.From.DistanceTo(bridge.To) / simulationStation.StationBuildout.StationModuleSet.BridgeLength, 1);

                var bridgeNode = new Spatial
                {
                    Name = "Bridge"
                };
                foundation.AddChild(bridgeNode);
                bridgeNode.LookAtFromPosition(
                    bridge.From,
                    bridge.To,
                    upPoint.Normalized());

                bridgeNode.Owner = GetTree().EditedSceneRoot;

                for (var i = 0; i < bridgeSegmentCount; i++)
                {
                    var bridgeSegment = simulationStation.StationBuildout.StationModuleSet.BridgePrefab.Instance() as Spatial;

                    bridgeNode.AddChild(bridgeSegment);
                    bridgeSegment.Transform = Transform.Identity
                        .Translated(Vector3.Forward * i * simulationStation.StationBuildout.StationModuleSet.BridgeLength);
                }
            }
        }

        foreach (var dockingTower in simulationStation.StationBuildout.DockingTowers)
        {
            var tower = simulationStation.StationBuildout.StationModuleSet.DockingTowerPrefab.Instance() as Spatial;

            var lookingAt = dockingTower.AttachedTowerPosition.Normalized() * dockingTower.FoundationPosition.Length();

            worldStation.AddChild(tower);
            tower.LookAtFromPosition(
                dockingTower.FoundationPosition,
                lookingAt, 
                upPoint.Normalized());
        }

        foreach (var defenseTowerBuildout in simulationStation.StationBuildout.DefenseTowers)
        {
            var tower = simulationStation.StationBuildout.StationModuleSet.DefenseTowerPrefab.Instance() as Spatial;

            var lookingAt = simulationStation.StationBuildout.CenterPosition.Normalized() * defenseTowerBuildout.FoundationPosition.Length();

            AddChild(tower);

            tower.LookAtFromPosition(
                defenseTowerBuildout.FoundationPosition,
                lookingAt,
                upPoint.Normalized());

            if (tower is DefenseTower defenseTower)
            {
                defenseTower.Faction = simulationStation.Faction;
            }
        }

        foreach (var farmBuildout in simulationStation.StationBuildout.FarmPlots)
        {
            var farm = simulationStation.StationBuildout.StationModuleSet.FarmPlotPrefab.Instance() as FarmPlot;

            AddChild(farm);

            farm.LookAtFromPosition(
                farmBuildout.CenterPosition,
                upPoint,
                Vector3.Up);

            farm.InitializePlants(random, farmBuildout);
        }

        if(simulationStation.StationBuildout.Bar != null)
        {
            AddStationRoom(
                worldStation,
                BarRoomPrefab,
                simulationStation.StationBuildout.Bar);
        }
        if (simulationStation.StationBuildout.EquipmentVendor != null)
        {
            AddStationRoom(
                worldStation,
                EquipmentRoomPrefab,
                simulationStation.StationBuildout.EquipmentVendor);
        }
        if (simulationStation.StationBuildout.Warehouse != null)
        {
            AddStationRoom(
                worldStation,
                WarehouseRoomPrefab,
                simulationStation.StationBuildout.Warehouse);
        }
        if (simulationStation.StationBuildout.SecurityOffice != null)
        {
            AddStationRoom(
                worldStation,
                SecurityRoomPrefab,
                simulationStation.StationBuildout.SecurityOffice);
        }
        if (simulationStation.StationBuildout.BountyOffice != null)
        {
            AddStationRoom(
                worldStation,
                BountyRoomPrefab,
                simulationStation.StationBuildout.BountyOffice);
        }

        if(simulationStation.StationBuildout.StationModuleSet.CaveLoadout != null)
        {
            var cave = simulationStation.StationBuildout.StationModuleSet.CaveLoadout.CavePrefab.Instance() as Spatial;

            var stationCenter = simulationStation.StationBuildout.CenterPosition;
            worldStation.AddChild(cave);
            cave.LookAtFromPosition(
                stationCenter.Normalized() * (stationCenter.Length() - 10000),
                stationCenter,
                Vector3.Up);
            foreach (var child in cave.GetChildren().OfType<Spatial>().Where(x => x.Name.StartsWith("Tower")))
            {
                child.Visible = false;
            }
        }

        worldStation.Initialize(simulationStation);
    }

    private void AddStationRoom(
        WorldStation worldStation, 
        PackedScene roomPrefab, 
        StationRoomBuildout roomBuildout)
    {
        var room = roomPrefab.Instance() as StationRoom;
        room.FriendlyName = roomBuildout.Name;
        worldStation.AddChild(room);
        room.LookAtFromPosition(
            roomBuildout.Position,
            Vector3.Zero,
            Vector3.Up);
    }
}
