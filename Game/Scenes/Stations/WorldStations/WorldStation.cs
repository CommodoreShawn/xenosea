using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Docking;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

public class WorldStation : Spatial, IStation, ISonarContact, ICombatant
{
    [Export]
    public float ConstantNoise = 100000;
    [Export]
    public float BracketSizeFactor { get; set; }
    [Export]
    public Texture TypeIcon { get; set; }
    [Export]
    public PackedScene LeakBubblesPrefab { get; set; }
    [Export]
    public float MaximumHealth { get; set; }
    [Export]
    public float Armor { get; set; }
    [Export]
    public int MaximumLeakBubbles { get; set; }
    [Export]
    public PackedScene RepairDiverPrefab;
    [Export]
    public float MinCameraDistance { get; set; } = 600;
    [Export]
    public float MaxCameraDistance { get; set; } = 600;
    

    public Vector3 RealPosition => GlobalTransform.origin;
    public Vector3 AimPoint { get; set; }
    public List<TurretSystem> TurretSystems { get; private set; }
    public List<TorpedoTube> TorpedoTubes { get; private set; }
    public List<MinisubHangar> MinisubHangars { get; private set; }
    public CommodityStorage CommodityStorage => BackingStation.CommodityStorage;
    public List<SonarContactRecord> SonarContactRecords { get; private set; }
    public bool IsWrecked { get; private set; }
    public Vector3 Velocity => Vector3.Zero;
    public float Health { get; private set; }
    public bool IsPlayerVisible { get; set; }
    public ContactType ContactType => IsWrecked ? ContactType.Wreck : ContactType.Station;
    public float HealthPercent => Health / MaximumHealth;
    public bool IsDockable => _dockingPoints.Any();
    public Spatial CameraCenter { get; private set; }
    public Spatial DockWaitingArea { get; private set; }
    public ulong Id { get; private set; }
    public float OverallNoise { get; set; }
    public List<TemporaryNoise> TemporaryNoises { get; private set; }
    private System.Random _random;
    private List<Spatial> _leakBubbles;
    public Faction Faction => BackingStation.Faction;
    public IEnumerable<Faction> FactionsPresent => BackingStation.FactionsPresent;

    public IEnumerable<DockingPoint> DockingPoints => _dockingPoints;
    private List<DockingPoint> _dockingPoints;
    private List<DockingSequence> _dockingQueue;
    public List<StationRoom> StationRooms { get; private set; }
    public string FriendlyName => BackingStation.Name;

    public SimulationStation BackingStation {get; set;}
    public int CommodityCapacity => BackingStation.CommodityCapacity;

    public string ReferenceId => BackingStation.ReferenceId;

    public float SpeakingTimer { get; set; }
    public string DebugText { get; set; }

    public bool IsCaveStation => BackingStation.StationBuildout.StationModuleSet.CaveLoadout != null;

    private bool _useOriginRelativeUp;
    private Vector3 _upVector;
    private GameplayRoot _gameplayRoot;

    [Inject]
    private ISonarContactTracker _sonarContactTracker;
    [Inject]
    private INotificationSystem _notificationSystem;
    [Inject]
    private IPerftracker _perfTracker;
    [Inject]
    private ICrimeTracker _crimeTracker;

    public override void _Ready()
    {
        this.ResolveDependencies();
        Id = GetInstanceId();
        _random = new System.Random();

        TurretSystems = new List<TurretSystem>();
        TorpedoTubes = new List<TorpedoTube>();
        MinisubHangars = new List<MinisubHangar>();
        TemporaryNoises = new List<TemporaryNoise>();
        _sonarContactTracker.AddSonarContact(this);
        SonarContactRecords = new List<SonarContactRecord>();
        Health = MaximumHealth;
        _leakBubbles = new List<Spatial>();
        _dockingPoints = new List<DockingPoint>();
        _dockingQueue = new List<DockingSequence>();
        StationRooms = new List<StationRoom>();
    }

    public void Initialize(SimulationStation simulationStation)
    {
        BackingStation = simulationStation;
        BackingStation.DetailedRepresentation = this;

        foreach (var child in GetChildren())
        {
            if (child is TurretSystem turretSystem)
            {
                TurretSystems.Add(turretSystem);
            }
            else if (child is TorpedoTube torpedoTube)
            {
                TorpedoTubes.Add(torpedoTube);
            }
            else if (child is DockingPoint dockingPoint)
            {
                _dockingPoints.Add(dockingPoint);
            }
            else if (child is StationRoom stationRoom)
            {
                StationRooms.Add(stationRoom);
            }
            else if (child is MinisubHangar minisubHangar)
            {
                MinisubHangars.Add(minisubHangar);
            }

            if (child is Node childNode)
            {
                foreach (var grandChild in childNode.GetChildren())
                {
                    if (grandChild is DockingPoint dockingPoint)
                    {
                        _dockingPoints.Add(dockingPoint);
                    }
                }
            }
        }

        for(var i = 0; i < _dockingPoints.Count; i++)
        {
            _dockingPoints[i].Name = $"Dock {i}";
        }

        _gameplayRoot = this.FindParent<GameplayRoot>();
        _useOriginRelativeUp = _gameplayRoot.UseOriginRelativeUp;
        if (_useOriginRelativeUp)
        {
            _upVector = GlobalTransform.origin.Normalized();
        }
        else
        {
            _upVector = Vector3.Up;
        }

        if (simulationStation.StationBuildout.StationModuleSet.MinisubHangarType != null)
        {
            foreach (var hangar in MinisubHangars)
            {
                hangar.Mount(simulationStation.StationBuildout.StationModuleSet.MinisubHangarType);
                hangar.MinisubType = simulationStation.StationBuildout.StationModuleSet.MinisubType;
                hangar.RebuildDockedModel();
            }
        }

        DockWaitingArea = new Spatial
        {
            Name = "DockWaitingArea"
        };
        AddChild(DockWaitingArea);
        DockWaitingArea.LookAtFromPosition(
            simulationStation.StationBuildout.DockWaitingArea,
            Vector3.Forward,
            Vector3.Up);

        CameraCenter = new Spatial
        {
            Name = "CameraCenter"
        };
        AddChild(CameraCenter);

        CameraCenter.LookAtFromPosition(
            simulationStation.StationBuildout.CameraCenter,
            Vector3.Forward,
            Vector3.Up);

        MaxCameraDistance = simulationStation.StationBuildout.CameraDistance;
        MinCameraDistance = simulationStation.StationBuildout.CameraDistance;
    }

    public override void _ExitTree()
    {
        base._ExitTree();

        BackingStation.DetailedRepresentation = null;
        _sonarContactTracker.RemoveSonarContact(this);
    }

    public override void _Process(float delta)
    {
        var stopwatch = System.Diagnostics.Stopwatch.StartNew();
        base._Process(delta);

        if(SpeakingTimer > 0)
        {
            SpeakingTimer -= delta;
        }

        UpdateNoises(delta);


        if (!IsWrecked)
        {
            UpdateSonarContactRecords(delta);

            if (_dockingQueue.Any())
            {
                if (_dockingPoints.Any(x => x.DockingSequence == null))
                {
                    var dockingSequence = _dockingQueue.First();
                    _dockingQueue.Remove(dockingSequence);

                    dockingSequence.DockingPoint = _dockingPoints
                        .Where(x => x.DockingSequence == null)
                        .OrderBy(x => x.ApproachPoint.GlobalTransform.origin.DistanceTo(dockingSequence.Submarine.RealPosition))
                        .First();
                    dockingSequence.DockingPoint.DockingSequence = dockingSequence;
                    _notificationSystem.Broadcast(this, $"{dockingSequence.Submarine.FriendlyName}, docking request accepted. Proceed to {dockingSequence.DockingPoint.Name}.");
                }
            }

            UpdateSubRepairs(delta);
        }

        Visible = IsPlayerVisible || IsWrecked;

        stopwatch.Stop();
        _perfTracker.LogPerf("Station._Process", stopwatch.ElapsedMilliseconds);
    }

    private void UpdateSubRepairs(float delta)
    {
        foreach (var dockPoint in _dockingPoints)
        {
            dockPoint.DiverLaunchCooldown -= delta;

            if (dockPoint.DockingSequence != null
                && dockPoint.DockingSequence.IsDocked)
            {
                var submarine = dockPoint.DockingSequence.Submarine;

                var maxDivers = Mathf.RoundToInt(Mathf.Max(1, submarine.MaximumHealth / 2000f));

                var targetDivers = Mathf.Min(maxDivers, submarine.LeakBubbles.Count);

                if (targetDivers > 0)
                {
                    var actualDivers = dockPoint.RepairDivers
                        .Where(rd => rd.Submarine == submarine)
                        .Count();

                    if (actualDivers < targetDivers && dockPoint.DiverLaunchCooldown <= 0)
                    {
                        dockPoint.DiverLaunchCooldown = 2;

                        var diver = RepairDiverPrefab.Instance() as StationRepairDiver;
                        diver.DockPoint = dockPoint;
                        diver.Submarine = submarine;
                        _gameplayRoot.AddChild(diver);
                        dockPoint.RepairDivers.Add(diver);

                        diver.GlobalTransform = dockPoint.GlobalTransform;
                        if (dockPoint.LeftSideDocking)
                        {
                            diver.RotateObjectLocal(_upVector, Mathf.Pi / 2);
                            diver.TranslateObjectLocal(Vector3.Forward);
                        }
                        else
                        {
                            diver.RotateObjectLocal(_upVector, -Mathf.Pi / 2);
                            diver.TranslateObjectLocal(Vector3.Forward);
                        }
                    }
                }
                else
                {
                    submarine.Health = Mathf.Min(submarine.MaximumHealth, submarine.Health + delta * StationRepairDiver.REPAIR_RATE);
                }
            }
        }
    }

    private void UpdateNoises(float delta)
    {
        if (IsWrecked)
        {
            OverallNoise = 0;
        }
        else
        {
            OverallNoise = 0;
            foreach (var noiseSource in TemporaryNoises)
            {
                noiseSource.Elapsed += delta;

                OverallNoise += (1 - noiseSource.Elapsed / noiseSource.TimeToLive) * noiseSource.NoiseAmount;
            }

            OverallNoise += ConstantNoise;

            TemporaryNoises.RemoveAll(x => x.Elapsed > x.TimeToLive);
        }
    }

    private void UpdateSonarContactRecords(float delta)
    {
        var newRecords = new List<SonarContactRecord>();

        foreach (var contact in _sonarContactTracker.GetSonarContacts())
        {
            if (contact != this)
            {
                var record = SonarContactRecords
                    .Where(r => r.SonarContact == contact)
                    .FirstOrDefault();

                if (record == null)
                {
                    record = new SonarContactRecord
                    {
                        SonarContact = contact,
                        PositionEstimateCounter = -1
                    };
                }

                var environmentNoise = 5;
                var detectorGain = 1f;
                var distance = (RealPosition - contact.RealPosition).Length();

                var detectedNoise = contact.OverallNoise / (4 * Mathf.Pi * Mathf.Pow(Constants.SONAR_ENV_PROPAGATION * distance, 2)) * detectorGain;

                record.IsDetected = detectedNoise >= environmentNoise;
                record.PositionLocked = detectedNoise >= environmentNoise * 2;

                if (record.IsDetected)
                {
                    record.TypeKnown = record.TypeKnown || (detectedNoise >= environmentNoise * 4);
                    record.Identified = record.Identified || (detectedNoise >= environmentNoise * 8);

                    if (record.PositionLocked)
                    {
                        record.PositionEstimateCounter = -1;
                        record.EstimatedPosition = contact.RealPosition;
                        record.Altitude = _gameplayRoot.GetAltitude(record.EstimatedPosition);
                    }
                    else
                    {
                        record.PositionEstimateCounter -= delta;

                        if (record.PositionEstimateCounter <= 0)
                        {
                            record.PositionEstimateCounter = (float)(_random.NextDouble() + 0.5f) * 15f;

                            var errorDist = distance * 0.5f;

                            record.EstimatedPosition = contact.RealPosition
                                + new Vector3(
                                    (float)(_random.NextDouble() - 0.5f) * errorDist,
                                    (float)(_random.NextDouble() - 0.5f) * errorDist,
                                    (float)(_random.NextDouble() - 0.5f) * errorDist);
                            record.Altitude = _gameplayRoot.GetAltitude(record.EstimatedPosition);
                        }
                    }

                    newRecords.Add(record);
                }
            }
        }

        SonarContactRecords = newRecords;
    }

    public void HitRecieved(ICombatant shooter, float damage, float penetration, Vector3 hitLocation, Vector3 velocity)
    {
        var penetrationFactor = 1f;

        if (Armor > 0)
        {
            var totalPenetration = velocity.Length() / 100f + penetration;
            penetrationFactor = Mathf.Clamp(totalPenetration / Armor, 0.1f, 1f);
        }

        var actualDamage = damage * penetrationFactor;

        Health -= actualDamage;

        if (shooter is Submarine submarine && submarine.IsPlayerSub)
        {
            _notificationSystem.AddNotification(new Notification
            {
                IsGood = true,
                Body = $"Hit for {actualDamage.ToString("N0")} damage ({penetrationFactor.ToString("P0")} penetration)"
            });

            if (Faction.TracksCrimes)
            {
                _crimeTracker.ReportCrime(
                    shooter,
                    this,
                    actualDamage);
            }
        }

        if (_leakBubbles.Count < DetermineTargetLeakCount())
        {
            var newLeak = LeakBubblesPrefab.Instance() as Spatial;
            AddChild(newLeak);
            newLeak.LookAtFromPosition(hitLocation, hitLocation - velocity, _upVector);
            _leakBubbles.Add(newLeak);
        }

        if (Health <= 0 && !IsWrecked)
        {
            IsWrecked = true;
        }
    }

    private int DetermineTargetLeakCount()
    {
        return (int)Mathf.Round((Health / MaximumHealth) * MaximumLeakBubbles);
    }

    public DockingSequence RequestDocking(Submarine submarine)
    {
        _notificationSystem.Broadcast(submarine, $"{FriendlyName}, requesting permission to dock.");
        GD.Print("request recieved");

        var dockingSequence = new DockingSequence(submarine, this);

        if (_dockingPoints.Any(x => x.DockingSequence == null))
        {
            GD.Print("dock accepted");
            dockingSequence.DockingPoint = _dockingPoints
                .Where(x => x.DockingSequence == null)
                .OrderBy(x => x.ApproachPoint.GlobalTransform.origin.DistanceTo(submarine.RealPosition))
                .First();
            dockingSequence.DockingPoint.DockingSequence = dockingSequence;
            _notificationSystem.Broadcast(this, $"{submarine.FriendlyName}, docking request accepted. Proceed to {dockingSequence.DockingPoint.Name}.");
        }
        else
        {
            GD.Print("no dock");
            _dockingQueue.Add(dockingSequence);
            _notificationSystem.Broadcast(this, $"{submarine.FriendlyName}, please hold. All docks are currently occupied.");
        }

        return dockingSequence;
    }

    public void CancelDocking(DockingSequence dockingSequence)
    {
        _dockingQueue.Remove(dockingSequence);
    }

    public void Broadcast(string message)
    {
        _notificationSystem.Broadcast(this, message);
    }

    public float PriceOf(CommodityType commodity)
    {
        var stockpilePerc = CommodityStorage.GetQty(commodity) / BackingStation.CommodityCapacity;

        return commodity.BaseValue * (1 - stockpilePerc + 0.5f);
    }

    public bool IsHostileTo(ISonarContact other)
    {
        if (Faction.IsHostileTo(other.Faction))
        {
            return true;
        }

        if (SonarContactRecords
            .Any(scr => scr.SonarContact == other && scr.AggrivationScore > 100))
        {
            return true;
        }

        return false;
    }

    public bool IsFriendlyTo(ISonarContact other)
    {
        if (SonarContactRecords
            .Any(scr => scr.SonarContact == other && scr.AggrivationScore > 100))
        {
            return false;
        }

        return Faction.IsFriendlyTo(other.Faction);
    }
}
