using Autofac;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

public class DatumCell : Spatial
{
    [Export]
    public Material LandMaterial;
    [Export]
    public Material RoofMaterial;
    [Export]
    public PackedScene WorldStationContainerPrefab;

    [Inject]
    private IWorldGenerator _worldGenerator;
    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private IWorldManager _worldManager;
    [Inject]
    private ISimulationEventBus _simulationEventBus;

    private ISeafloorObjectSpawner[] _seafloorObjectSpawners;

    private MeshInstance _floorMesh;
    private MeshInstance _roofMesh;
    private CollisionShape _floorCollider;
    private CollisionShape _roofCollider;

    private float _checkTimer;
    private Random _random;
    public WorldDatum WorldDatum { get; set; }

    private int _vertexCount;

    public CellBuildData CellBuildData { get; set; }
    private bool _builtStation;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _seafloorObjectSpawners = IocManager.IocContainer.Resolve<IEnumerable<ISeafloorObjectSpawner>>().ToArray();
        _floorMesh = GetNode<MeshInstance>("FloorMesh");
        _roofMesh = GetNode<MeshInstance>("RoofMesh");
        _floorCollider = GetNode<CollisionShape>("FloorCollider");
        _roofCollider = GetNode<CollisionShape>("RoofCollider");
        _random = new Random();
        WorldDatum.InDetailedSim = true;
        _builtStation = false;

        _simulationEventBus.OnSimulationEvent += OnSimulationEvent;

        LookAtFromPosition(WorldDatum.Position, WorldDatum.Position + Vector3.Forward, Vector3.Up);
    }

    public override void _ExitTree()
    {
        base._ExitTree();
        _simulationEventBus.OnSimulationEvent -= OnSimulationEvent;
    }

    private void OnSimulationEvent(ISimulationEvent evt)
    {
        if(evt is NewSeafloorObjectSimulationEvent newSeafloorObjectEvent)
        {
            if(newSeafloorObjectEvent.NewObject.Datum == WorldDatum)
            {
                SpawnSeafloorObject(newSeafloorObjectEvent.NewObject);
            }
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _checkTimer -= delta;

        if (_checkTimer <= 0 )
        {
            _checkTimer = (float)_random.NextDouble() * 5;

            if (_playerData.PlayerSub != null && IsInstanceValid(_playerData.PlayerSub))
            {
                LookAtFromPosition(WorldDatum.Position, WorldDatum.Position + Vector3.Forward, Vector3.Up);

                if (!_builtStation)
                {
                    BuildStation();
                }

                var distanceToPlayer = WorldDatum.Position.DistanceTo(_playerData.PlayerSub.RealPosition);

                var distanceBracket = (int)(distanceToPlayer / 3000f);

                distanceBracket = Mathf.Max(distanceBracket, 1);

                if(Engine.TimeScale > 10)
                {
                    distanceBracket = Mathf.Max(distanceBracket, 4);
                }

                var targetVertexCount = (int)(Constants.TERRAIN_VERTEX_COUNT / Mathf.Pow(distanceBracket, 2)) + 2;

                if (targetVertexCount != _vertexCount)
                {
                    _vertexCount = targetVertexCount;
                    _worldGenerator.QueueGeometryRebuild(this, _vertexCount);
                }
            }
        }

        if(CellBuildData != null)
        {
            var data = CellBuildData;
            CellBuildData = null;

            var floorMesh = new ArrayMesh();
            floorMesh.AddSurfaceFromArrays(Mesh.PrimitiveType.Triangles, data.FloorSurface);
            var roofMesh = new ArrayMesh();
            roofMesh.AddSurfaceFromArrays(Mesh.PrimitiveType.Triangles, data.RoofSurface);

            _floorMesh.Mesh = floorMesh;
            _floorMesh.MaterialOverride = LandMaterial;
            _floorCollider.Shape = floorMesh.CreateTrimeshShape();

            _roofMesh.Mesh = roofMesh;
            _roofMesh.MaterialOverride = RoofMaterial;
            _roofCollider.Shape = roofMesh.CreateTrimeshShape();
        }
    }

    public void BuildStation()
    {
        _builtStation = true;

        if (WorldDatum.SimulationStation != null)
        {
            var stationContainer = WorldStationContainerPrefab.Instance() as WorldStationContainer;

            AddChild(stationContainer);
            stationContainer.LookAtFromPosition(WorldDatum.Position, WorldDatum.Position * 2, Vector3.Up);

            stationContainer.BuildStation(WorldDatum.SimulationStation);
        }

        if (WorldDatum.LocalWreck?.WreckPrefab != null)
        {
            //GD.Print("Buliding wreck");
            var wreckSpatial = WorldDatum.LocalWreck.WreckPrefab.Instance() as Spatial;

            AddChild(wreckSpatial);
            wreckSpatial.LookAtFromPosition(WorldDatum.Position, WorldDatum.Position * 2, Vector3.Up);
        }

        //GD.Print("Building stations for " + WorldDatum.Id);
        //GD.Print("Datums with objs " + _worldManager.WorldDatums.Where(x => x.SeafloorObjects.Any()).Count());

        foreach (var seafloorObject in WorldDatum.SeafloorObjects)
        {
            SpawnSeafloorObject(seafloorObject);
        }
    }

    private void SpawnSeafloorObject(SeafloorObject seafloorObject)
    {
        var spawner = _seafloorObjectSpawners
            .Where(x => x.CanHandle(seafloorObject))
            .FirstOrDefault();

        if (spawner != null)
        {
            spawner.Spawn(this, seafloorObject);
        }
        else
        {
            GD.Print("Cannot find seafloor object spawner for type: " + seafloorObject.Type);
        }
    }
}
