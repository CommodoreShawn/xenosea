using Godot;

public class WorldgenCamera : Camera
{
    public float CameraYaw { get; private set; }
    public float CameraPitch { get; set; } = Mathf.Pi / 2;
    public float CameraDistance { get; set; } = 50;

    private float _zoomDistSpeed = 1000;
    private float _minDist = 800;
    private float _maxDist = 800;

    private bool _isAiming;

    [Export]
    public float Sensitivity = 0.01f;

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if (_isAiming)
        {
            if (@event is InputEventMouseMotion mouseMotionEvt)
            {
                CameraYaw += mouseMotionEvt.Relative.x * Sensitivity;

                CameraPitch = Mathf.Clamp(
                    CameraPitch - mouseMotionEvt.Relative.y * Sensitivity,
                    0.2f * Mathf.Pi,
                    0.8f * Mathf.Pi);
            }
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        var wasAiming = _isAiming;

        _isAiming = Input.IsMouseButtonPressed((int)ButtonList.Right);

        if (_isAiming && !wasAiming)
        {
            Input.MouseMode = Input.MouseModeEnum.Captured;
        }
        else if (!_isAiming && wasAiming)
        {
            Input.MouseMode = Input.MouseModeEnum.Visible;
        }

        if (Input.IsActionJustReleased("zoom_out"))
        {
            CameraDistance += delta * _zoomDistSpeed;
        }

        if (Input.IsActionJustReleased("zoom_in"))
        {
            CameraDistance -= delta * _zoomDistSpeed;
        }

        CameraDistance = Mathf.Clamp(CameraDistance, _minDist, _maxDist);

        var newPos = new Vector3(
                CameraDistance * Mathf.Cos(CameraYaw) * Mathf.Sin(CameraPitch),
                CameraDistance * Mathf.Cos(CameraPitch),
                CameraDistance * Mathf.Sin(CameraYaw) * Mathf.Sin(CameraPitch));

        LookAtFromPosition(newPos, Vector3.Zero, Vector3.Up);
    }
}
