using Autofac;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources.Cinematics;
using Xenosea.Scenes.Ships;

public class Gameplay : GameplayRoot
{
    [Export]
    public PackedScene ControllerPrefab;
    [Export]
    public PackedScene DatumCellPrefab;
    [Export]
    public PackedScene AgentControllerPrefab;
    [Export]
    public Cinescript[] IntroScripts;
    [Export]
    public AudioStream[] StartBattleBarks;
    [Export]
    public AudioStream[] StartCruiseBarks;
    [Export]
    public AudioStream[] ExitCruiseBarks;
    [Export]
    public AudioStream[] UndockBarks;

    private bool _spawnedPlayer;
    private DirectionalLight _roofLight;
    private AudioStreamPlayer _barkPlayer;
    private Random _random;
    private float _datumSpawnTimer;
    private float _barkMute;
    private bool _wasInBattle;
    private bool _wasInCruise;
    private bool _wasUndocking;

    [Inject]
    private IWorldManager _worldManager;
    [Inject]
    private INotificationSystem _notificationSystem;
    [Inject]
    private IResourceLocator _resourceLocator;
    [Inject]
    private ISimToEngineLink _simToEngineLink;
    [Inject]
    private IPlayerData _playerData;
    [Inject]
    private ISettingsService _settingsService;
    [Inject]
    private IPerftracker _perftracker;
    [Inject]
    private IWorldGenerator _worldGenerator;

    private List<DatumCell> _activeCells;

    public IEnumerable<WorldDatum> ActiveDatums => _activeCells.Select(x => x.WorldDatum);

    private float _endGameTimer;
    private float _stormCheckTimer;

    private Vector3 _dominantCurrentFlow;

    private WeatherSystem[] _nearbyStorms;

    private SimplexNoiseGenerator _stormCurrentNoisegenX;
    private SimplexNoiseGenerator _stormCurrentNoisegenY;
    private SimplexNoiseGenerator _stormCurrentNoisegenZ;

    private List<Cinescript> _executingCinescripts;

    public WorldDatum CenterDatum { get; private set; }

    private int _framesSincePerf;

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();

        _roofLight = GetNode<DirectionalLight>("RoofLight");
        _barkPlayer = GetNode<AudioStreamPlayer>("BarkPlayer");
        _activeCells = new List<DatumCell>();
        _random = new Random();
        _simToEngineLink.GameplayInstance = this;

        _endGameTimer = 5;

        _playerData.LastSaveTime = DateTime.Now;
        _dominantCurrentFlow = Vector3.Zero;
        _nearbyStorms = new WeatherSystem[0];

        _stormCurrentNoisegenX = new SimplexNoiseGenerator("storm_x");
        _stormCurrentNoisegenY = new SimplexNoiseGenerator("storm_y");
        _stormCurrentNoisegenZ = new SimplexNoiseGenerator("storm_z");
        _executingCinescripts = new List<Cinescript>();

        _worldManager.SessionStartTime = DateTime.Now;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_barkMute > 0)
        {
            _barkMute -= delta;
        }

        _framesSincePerf += 1;

        if(_framesSincePerf > 60 && Constants.DEBUG_PERF_DISPLAY)
        {
            _perftracker.PrintPerfInfo(_framesSincePerf);
            _framesSincePerf = 0;
        }

        foreach(var script in _executingCinescripts)
        {
            script.Update(delta, this, _playerData);
        }
        _executingCinescripts.RemoveAll(x => x.IsFinished);

        _stormCheckTimer -= delta;
        if(_stormCheckTimer <= 0 && _playerData.PlayerSub != null && IsInstanceValid(_playerData.PlayerSub))
        {
            _stormCheckTimer = 10;

            var playerShellPos = _playerData.PlayerSub.RealPosition.Normalized() * Constants.WORLD_RADIUS;

            _nearbyStorms = _worldManager.WeatherSystems
                .Where(x => x.Position.DistanceTo(playerShellPos) < x.Radius + _playerData.DetailedSimDistance)
                .ToArray();
        }

        if (IsInstanceValid(_playerData.PlayerSub) && _playerData.PlayerSub != null && GetViewport().GetCamera().Environment != null)
        {
            var currentFactor = Mathf.Clamp(
                    (_playerData.PlayerSub.DistanceToGround - 100) / 300,
                    0, 1);

            var stormStrength = GetStormStrength(_playerData.PlayerSub.RealPosition) * currentFactor;

            if(stormStrength > 0)
            {
                GetViewport().GetCamera().Environment.FogDepthEnd = 4000 * (1 - Mathf.Clamp(stormStrength / 20f, 0.01f, 1));
            }
            else
            {
                GetViewport().GetCamera().Environment.FogDepthEnd = 4000;
            }
        }

        if (!_spawnedPlayer)
        {
            _spawnedPlayer = true;

            _playerData.WeaponsSafe = true;

            if (_playerData.PlayerLoadLoadout != null)
            {
                LoadPlayer();
            }
            else
            {
                SpawnPlayer();

                if (!_playerData.SkipIntro)
                {
                    foreach (var cinescript in IntroScripts)
                    {
                        cinescript.InitialExecute(this, _playerData);

                        if (!cinescript.IsFinished)
                        {
                            _executingCinescripts.Add(cinescript);
                        }
                    }
                }
            }
        }

        _datumSpawnTimer -= delta;

        if (_playerData.PlayerSub != null && IsInstanceValid(_playerData.PlayerSub))
        {
            _roofLight.LookAtFromPosition(_playerData.PlayerSub.RealPosition, Vector3.Zero, Vector3.Up);
            var playerDepth = Constants.WORLD_RADIUS - _playerData.PlayerSub.RealPosition.Length();

            var lightIntensity = Mathf.Clamp(1 - playerDepth / 5000f, 0, 1);

            _roofLight.LightColor = new Color(
                0 * lightIntensity,
                0.45f * lightIntensity,
                0.46f * lightIntensity);

            if(_datumSpawnTimer <= 0)
            {
                _datumSpawnTimer = (float)_random.NextDouble() * 3;

                var playerShellPos = _playerData.PlayerSub.RealPosition.Normalized() * Constants.WORLD_RADIUS;

                LoadDatumsAround(playerShellPos);

                if(_activeCells.Any())
                {
                    var centerCells = _activeCells
                        .OrderBy(c => c.WorldDatum.Position.DistanceSquaredTo(playerShellPos))
                        .Take(4)
                        .ToArray();

                    CenterDatum = centerCells.FirstOrDefault()?.WorldDatum;

                    _dominantCurrentFlow = Vector3.Zero;
                    foreach(var cell in centerCells)
                    {
                        _dominantCurrentFlow += cell.WorldDatum.CurrentFlow;
                    }
                    _dominantCurrentFlow /= centerCells.Length;
                }
            }

            if(_playerData.PlayerSub.IsWrecked)
            {
                _endGameTimer -= delta;
                Engine.TimeScale = 1;

                if(_endGameTimer <= 0)
                {
                    GetTree().ChangeScene("res://Scenes/MainMenu.tscn");
                }
            }

            var playerSelfPoi = _playerData.KnownPointsOfInterest
                .Where(x => x.ReferenceId == "playersub")
                .FirstOrDefault();

            if(playerSelfPoi == null)
            {
                playerSelfPoi = new KnownPointOfInterest
                {
                    ContactType = Xenosea.Resources.ContactType.Sub,
                    Location = _worldManager.WorldDatums.First(),
                    ReferenceId = "playersub"
                };
                _playerData.KnownPointsOfInterest.Add(playerSelfPoi);
            }

            playerSelfPoi.PreciseLocation = _playerData.PlayerSub.RealPosition;
            playerSelfPoi.FriendlyName = "Your " + _playerData.PlayerSub.FriendlyName;


            if(_playerData.InBattle 
                && !_wasInBattle
                && _barkMute <= 0)
            {
                PlayBark(StartBattleBarks);
            }
            
            if (Engine.TimeScale == Constants.CRUISE_MODE_TIME_SCALE
                && !_wasInCruise
                && _barkMute <= 0)
            {
                PlayBark(StartCruiseBarks);
            }

            if (Engine.TimeScale != Constants.CRUISE_MODE_TIME_SCALE
                && _wasInCruise
                && _barkMute <= 0)
            {
                PlayBark(ExitCruiseBarks);
            }

            if (_playerData.PlayerController.DockingSequence?.IsUndocking != true
                && _wasUndocking
                && _barkMute <= 0)
            {
                PlayBark(UndockBarks);
            }

            _wasInBattle = _playerData.InBattle;
            _wasInCruise = Engine.TimeScale == Constants.CRUISE_MODE_TIME_SCALE;
            _wasUndocking = _playerData.PlayerController.DockingSequence?.IsUndocking == true;
        }
    }

    public override void _ExitTree()
    {
        base._ExitTree();

        _simToEngineLink.GameplayInstance = null;
    }

    private void LoadDatumsAround(Vector3 loadCenter)
    {
        foreach (var datumCell in _activeCells)
        {
            if (datumCell.WorldDatum.Position.DistanceTo(loadCenter) >= _playerData.DetailedSimDistance)
            {
                if(datumCell.WorldDatum.SimulationStation != null)
                {
                    datumCell.WorldDatum.SimulationStation.DetailedRepresentation = null;
                }

                datumCell.WorldDatum.InDetailedSim = false;
                datumCell.QueueFree();
            }
        }

        _activeCells.RemoveAll(c => !c.WorldDatum.InDetailedSim);

        var datumsToAdd = _worldManager.WorldDatums
            .Where(d => !d.InDetailedSim)
            .Where(d => d.Position.DistanceTo(loadCenter) < _playerData.DetailedSimDistance)
            .OrderBy(d => d.Position.DistanceTo(loadCenter))
            .ToArray();

        foreach (var datum in datumsToAdd)
        {
            datum.InDetailedSim = true;
            var datumCell = DatumCellPrefab.Instance() as DatumCell;
            datumCell.WorldDatum = datum;
            AddChild(datumCell);
            _activeCells.Add(datumCell);

            datumCell.BuildStation();
        }

        foreach (var datum in _activeCells.Select(c => c.WorldDatum))
        {
            datum.IsExplored = datum.IsExplored || datum.Position.DistanceTo(loadCenter) < Constants.EXPLORATION_DISTANCE;
        }
    }

    private void SpawnPlayer()
    {
        if (_playerData.PlayerSub != null)
        {
            _playerData.PlayerSub.IsPlayerSub = false;
        }

        var spawnDatum = _playerData.SpawnDatum;
        var subLoadout = _playerData.PlayerLoadout;
        
        var spawnDepth = (spawnDatum.Depth - spawnDatum.IceDepth) / 2 + spawnDatum.IceDepth;

        var spawnPos = spawnDatum.Position - spawnDepth * spawnDatum.Position.Normalized();

        var playerController = ControllerPrefab.Instance() as PlayerController;
        _playerData.PlayerSub = subLoadout.HullType.SubPrefab.Instance() as Submarine;
        _playerData.PlayerSub.FriendlyName = "Calico";
        _playerData.PlayerSub.IsPlayerSub = true;
        _playerData.PlayerController = playerController;
        _playerData.PlayerSub.AddChild(playerController);
        _playerData.PlayerSub.Faction = _playerData.Faction;
        _playerData.PlayerSub.LookAtFromPosition(spawnPos, spawnPos + Vector3.Forward, spawnPos.Normalized());
        AddChild(_playerData.PlayerSub);

        _playerData.PlayerSub.BuildModules(subLoadout);
    }

    private void LoadPlayer()
    {
        if (_playerData.PlayerSub != null)
        {
            _playerData.PlayerSub.IsPlayerSub = false;
        }

        var spawnPos = Util.GetNavigationCenter(_playerData.PlayerLoadLoadout.DockedTo.Location);

        LoadDatumsAround(_playerData.PlayerLoadLoadout.DockedTo.Location.Position);

        var playerController = ControllerPrefab.Instance() as PlayerController;
        _playerData.PlayerSub = _playerData.PlayerLoadLoadout.HullType.SubPrefab.Instance() as Submarine;
        _playerData.PlayerSub.IsPlayerSub = true;
        _playerData.PlayerController = playerController;
        _playerData.PlayerSub.AddChild(playerController);
        _playerData.PlayerSub.Faction = _playerData.Faction;
        _playerData.PlayerSub.LookAtFromPosition(spawnPos, spawnPos + Vector3.Forward, spawnPos.Normalized());
        AddChild(_playerData.PlayerSub);
        _playerData.PlayerSub.ApplySubState(_playerData.PlayerLoadLoadout);

        var dockTargetStation = GetChildren()
            .OfType<Spatial>()
            .Select(x => x.GetChildren().OfType<Spatial>().SelectMany(y => y.GetChildren().OfType<WorldStation>()).FirstOrDefault())
            .Where(x => x != null)
            .Where(s => s.BackingStation == _playerData.PlayerLoadLoadout.DockedTo)
            .FirstOrDefault();

        if(dockTargetStation != null)
        {
            var dockingPoint = dockTargetStation.DockingPoints.ToArray()[_playerData.PlayerLoadLoadout.DockNumber];

            playerController.AutopilotType = AutopilotType.Dock;
            dockingPoint.DockingSequence = new Xenosea.Logic.Docking.DockingSequence(_playerData.PlayerSub, dockTargetStation);
            playerController.DockingSequence = dockingPoint.DockingSequence;
            dockingPoint.DockingSequence.InstantDock(dockingPoint, playerController);

        }
        else
        {
            GD.Print("Could not find loaded station: " + _playerData.PlayerLoadLoadout.DockedTo?.Name);
        }
    }

    public Submarine SpawnAgent(SimulationAgent agent)
    {
        Vector3 spawnPos;

        if (agent.MovingTo == null)
        {
            spawnPos = Util.GetNavigationCenter(agent.Location);
        }
        else
        {
            spawnPos = (1 - agent.MovementProgress) * Util.GetNavigationCenter(agent.Location)
                + (agent.MovementProgress) * Util.GetNavigationCenter(agent.MovingTo);
        }

        //GD.Print("Spawning: " + agent?.Name);
        //GD.Print("HullType: " + agent?.SubmarineLoadout?.HullType?.Name);
        //GD.Print("--Controller");
        var agentController = agent.ControllerPrefab.Instance() as GoalBotController;
        //GD.Print("--Sub");
        var agentSub = agent.SubmarineLoadout.HullType.SubPrefab.Instance() as Submarine;
        agentSub.FriendlyName = agent.Name;
        agentSub.AgentRepresentation = agent;
        agent.DetailedRepresentation = agentSub;
        agentSub.AddChild(agentController);
        agentSub.Faction = agent.Faction;
        agentSub.LookAtFromPosition(spawnPos, spawnPos + Vector3.Forward, spawnPos.Normalized());
        //GD.Print("--adding sub");
        AddChild(agentSub);
        //GD.Print("--building modules");
        agentSub.BuildModules(agent.SubmarineLoadout);
        agentSub.Health = agent.Health;

        var leakBubblesToAdd = (1 - agentSub.HealthPercent) * agentSub.MaximumLeakBubbles;
        for(var i = 0; i < leakBubblesToAdd; i++)
        {
            // TODO how to do this?
        }

        if(agent.MovingTo == null 
            && agent.Location?.SimulationStation != null
            && agent.Faction.IsFriendlyTo(agent.Location.SimulationStation.Faction)
            && agent.Location.SimulationStation.DetailedRepresentation != null)
        {
            var dockTargetStation = agent.Location.SimulationStation.DetailedRepresentation;
            var dockingPoint = dockTargetStation.DockingPoints
                .Where(x => x.DockingSequence == null)
                .FirstOrDefault();

            if (dockingPoint != null)
            {
                dockingPoint.DockingSequence = new Xenosea.Logic.Docking.DockingSequence(agentSub, dockTargetStation);
                agentController.DockingSequence = dockingPoint.DockingSequence;
                dockingPoint.DockingSequence.InstantDock(dockingPoint, agentController);
            }
        }



        //GD.Print("--return");
        return agentSub;
    }

    public Vector3 GetCurrentFlow(Vector3 position)
    {
        if(_nearbyStorms.Length < 1)
        {
            return _dominantCurrentFlow;
        }

        try
        {
            var stormStrength = _nearbyStorms
                .Where(x => x.Position.DistanceTo(position) < x.Radius)
                .Select(x => x.Energy * (1 - x.Position.DistanceTo(position) / x.Radius))
                .Sum();

            var timeFactor = Mathf.Sin(Mathf.Deg2Rad(_worldManager.CurrentDate.Minute + _worldManager.CurrentDate.Second / 60f)) * 1000;

            var stormFlow = new Vector3(
                _stormCurrentNoisegenX.CoherentNoise(position.x, position.y, position.z + timeFactor, 1, 100),
                _stormCurrentNoisegenY.CoherentNoise(position.x, position.y, position.z + timeFactor, 1, 100),
                _stormCurrentNoisegenZ.CoherentNoise(position.x, position.y, position.z + timeFactor, 1, 100)) * 200;

            return _dominantCurrentFlow + stormFlow;
        }
        catch(Exception)
        {
        }

        return _dominantCurrentFlow;
    }

    public float GetStormStrength(Vector3 position)
    {
        if (_nearbyStorms.Length < 1)
        {
            return 0;
        }

        return _nearbyStorms
            .Where(x => x.Position.DistanceTo(position) < x.Radius)
            .Select(x => x.Energy * (1 - x.Position.DistanceTo(position) / x.Radius))
            .Sum();
    }

    private void PlayBark(AudioStream[] barkOptions)
    {
        if(barkOptions.Length > 0)
        {
            _barkMute = 2;

            var bark = barkOptions[_random.Next(barkOptions.Length)];
            _barkPlayer.Stream = bark;
            _barkPlayer.Play();
        }
    }

    public void SkipIntro()
    {
        foreach(var script in _executingCinescripts)
        {
            script.Cancel();
        }
    }
}
