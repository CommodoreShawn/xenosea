using Autofac;
using Godot;
using System;
using System.Globalization;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;

public class InitializationSplash : Control
{
    public override void _Ready()
    {
        if (!Engine.EditorHint)
        {
            IocManager.Initialize();
        }

        // Change negative currency to show as -$1,234 instead of ($1,234)
        var cultureInfo = (CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
        cultureInfo.NumberFormat.CurrencyNegativePattern = 1;
        System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;

        AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;
    }

    private void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
    {
        try
        {
            var logger = IocManager.IocContainer.Resolve<ILogger>();
            logger.Error(e.ExceptionObject as Exception, "Unhandled Exception");
        }
        catch(Exception)
        {
            GD.PrintErr($"Unhandled  {e.ExceptionObject.GetType().Name}: {(e.ExceptionObject as Exception)?.Message}");
            GD.PrintErr((e.ExceptionObject as Exception)?.StackTrace);
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        IocManager.IocContainer.Resolve<ISettingsService>().LoadSettings();
        IocManager.IocContainer.Resolve<ISettingsService>().ApplyDisplaySettings();

        GetTree().ChangeScene("res://Scenes/MainMenu.tscn");
    }
}
