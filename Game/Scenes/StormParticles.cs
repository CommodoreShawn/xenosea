using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;

public class StormParticles : CPUParticles
{
    [Inject]
    private IPlayerData _playerData;

    private Gameplay _gameplay;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _gameplay = this.FindParent<Gameplay>();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_playerData.PlayerSub != null)
        {
            var position = _playerData.PlayerSub.RealPosition;

            var currentFactor = Mathf.Clamp(
                    (_playerData.PlayerSub.DistanceToGround - 100) / 300,
                    0, 1);

            var stormStrength = _gameplay.GetStormStrength(position) * currentFactor;

            if (stormStrength > 0)
            {
                if (!Emitting)
                {
                    Emitting = true;
                }

                Color = new Color(
                    1, 1, 1,
                    (1 - Mathf.Clamp(stormStrength * currentFactor / 20f, 0, 1)) / 0.1f);
            }
            else if(Emitting)
            {
                Emitting = false;
            }

        }
        
    }
}
