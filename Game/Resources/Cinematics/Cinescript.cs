﻿using Godot;
using System;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Resources.Cinematics
{
    public abstract class Cinescript : Resource
    {
        public abstract bool IsFinished { get; protected set; }
        public abstract void Update(float delta, Gameplay gameplay, IPlayerData playerData);
        public abstract void InitialExecute(Gameplay gameplay, IPlayerData playerData);

        public abstract void Cancel();
    }
}
