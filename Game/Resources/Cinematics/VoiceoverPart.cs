﻿using Godot;

namespace Xenosea.Resources.Cinematics
{
    public class VoiceoverPart : Resource
    {
        [Export]
        public AudioStream VoiceoverStream;
        [Export(PropertyHint.MultilineText)]
        public string Subtitle;
        [Export]
        public float Duration;
    }
}
