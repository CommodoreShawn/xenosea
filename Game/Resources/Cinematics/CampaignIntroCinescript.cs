using Autofac;
using Godot;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Resources.Cinematics
{
    public class CampaignIntroCinescript : Cinescript
    {
        [Export]
        public SubLoadout EnemySub;

        [Export]
        public Faction EnemySubFaction;

        [Export]
        public float EnemyYaw;
        [Export]
        public float EnemyPitch;
        [Export]
        public float EnemyDist;

        [Export]
        public float DamageToPlayer;

        [Export]
        public Vector3 HitPoint;

        [Export]
        public Vector3 HitDirection;

        [Export]
        public float StartHeightAdjust;

        [Export]
        public PackedScene IntroSplash;

        [Export]
        public Npc InitialConversationXo;

        [Export]
        public PackedScene VoiceoverDisplayPrefab;
        [Export]
        public float VoiceoverDelay;
        [Export]
        public VoiceoverPart[] Voiceovers;

        private bool _appliedDamage;
        private bool _enemyPlaced;
        private float _sequenceTimer;
        private GameplayUi _gameplayUi;
        private SubCamera _camera;
        private ISimulationEventBus _simulationEventBus;
        private float _voiceoverTimer;
        private int _voiceoverIndex;
        private VoiceoverDisplay _voiceoverDisplay;

        public override bool IsFinished { get; protected set; }

        public override void InitialExecute(Gameplay gameplay, IPlayerData playerData)
        {
            _simulationEventBus = IocManager.IocContainer.Resolve<ISimulationEventBus>();
            gameplay.AddChild(IntroSplash.Instance());
            _gameplayUi = gameplay.GetNode<GameplayUi>("UI");
            _gameplayUi.HideForOtherUi = true;
            _camera = gameplay.GetNode<SubCamera>("Camera");

            _appliedDamage = false;
            _enemyPlaced = false;
            _sequenceTimer = 0;
            _voiceoverIndex = -1;
            _voiceoverTimer = 0;
            Input.MouseMode = Input.MouseModeEnum.Captured;
            IsFinished = false;
        }

        public override void Update(float delta, Gameplay gameplay, IPlayerData playerData)
        {
            _voiceoverTimer += delta;
            if(_voiceoverIndex <= 0)
            {
                if(_voiceoverTimer >= VoiceoverDelay)
                {
                    PlayNextVoiceover(gameplay);
                }
            }

            if(_voiceoverIndex >= 0
                && _voiceoverIndex < Voiceovers.Length
                && _voiceoverTimer >= Voiceovers[_voiceoverIndex].Duration)
            {
                PlayNextVoiceover(gameplay);
            }

            _sequenceTimer += delta;
            if(!_appliedDamage)
            {
                _appliedDamage = true;

                ApplyDamage(gameplay, playerData);
                playerData.PlayerController.ControlLockout = true;
                playerData.PlayerSub.Throttle = 0.5f;
            }
            else if(!_enemyPlaced)
            {
                _enemyPlaced = true;
                PlaceEnemy(gameplay, playerData);
            }

            if(_sequenceTimer < 30)
            {
                var cameraDist = 1000 - _sequenceTimer * 25;
                var cameraYaw = 1 / (31 - _sequenceTimer) * Mathf.Pi;
                var cameraPitch = Mathf.Pi / 2 - (30 - _sequenceTimer) / 90;

                var relativePos = new Vector3(
                    cameraDist * Mathf.Cos(cameraYaw) * Mathf.Sin(cameraPitch),
                    cameraDist * Mathf.Cos(cameraPitch),
                    cameraDist * Mathf.Sin(cameraYaw) * Mathf.Sin(cameraPitch));

                var upVector = playerData.PlayerSub.RealPosition.Normalized();

                var worldTransform = Transform.Identity.LookingAt(new Vector3(0, 1, 0), upVector);
                
                var newPos = playerData.PlayerSub.CameraCenter.GlobalTransform.origin
                    + worldTransform.Xform(relativePos);

                _camera.LookAtFromPosition(
                    newPos, 
                    playerData.PlayerSub.CameraCenter.GlobalTransform.origin, 
                    upVector);

                _camera.CameraPitch = cameraPitch;
                _camera.CameraDistance = cameraDist;
                _camera.CameraYaw = cameraYaw;
            }

            if(_sequenceTimer >= 30)
            {
                _gameplayUi.HideForOtherUi = false;
                playerData.PlayerSub.Throttle = 0f;
                playerData.PlayerController.ControlLockout = false;
                IsFinished = true;
                Input.MouseMode = Input.MouseModeEnum.Visible;
                _simulationEventBus.SendEvent(new StartConversationSimulationEvent(InitialConversationXo));
                
                if (_voiceoverDisplay != null && IsInstanceValid(_voiceoverDisplay))
                {
                    _voiceoverDisplay.QueueFree();
                    _voiceoverDisplay = null;
                }
            }
        }

        public override void Cancel()
        {
            _sequenceTimer = 29;
        }

        private void ApplyDamage(Gameplay gameplay, IPlayerData playerData)
        {
            playerData.PlayerSub.Translate(playerData.PlayerSub.RealPosition.Normalized() * -StartHeightAdjust);

            playerData.PlayerSub.HitRecieved(
                null,
                DamageToPlayer,
                0,
                playerData.PlayerSub.GlobalTransform.Xform(HitPoint),
                playerData.PlayerSub.GlobalTransform.basis.Xform(HitDirection));
        }

        private void PlaceEnemy(Gameplay gameplay, IPlayerData playerData)
        {
            var relativePos = new Vector3(
                        EnemyDist * Mathf.Cos(EnemyYaw) * Mathf.Sin(EnemyPitch),
                        EnemyDist * Mathf.Cos(EnemyPitch),
                        EnemyDist * Mathf.Sin(EnemyYaw) * Mathf.Sin(EnemyPitch));

            var upVector = playerData.PlayerSub.RealPosition.Normalized();

            var worldTransform = Transform.Identity.LookingAt(new Vector3(0, 1, 0), upVector);

            var newPos = playerData.PlayerSub.CameraCenter.GlobalTransform.origin
                + worldTransform.Xform(relativePos);

            var enemySub = EnemySub.HullType.SubPrefab.Instance() as Submarine;
            enemySub.Faction = EnemySubFaction;
            enemySub.LookAtFromPosition(
                newPos,
                playerData.PlayerSub.GlobalTransform.origin,
                playerData.PlayerSub.GlobalTransform.origin.Normalized());
            gameplay.AddChild(enemySub);
            enemySub.Health = 0;
            enemySub.Name = "Tutorial Target";
            enemySub.BuildModules(EnemySub);
            enemySub.FriendlyName = "Pirate  Wreck";
            enemySub.HitRecieved(
                null,
                enemySub.MaximumHealth * 2,
                1000,
                enemySub.GlobalTransform.origin,
                new Vector3(0, 0, -1));
        }

        private void PlayNextVoiceover(Gameplay gameplay)
        {
            _voiceoverIndex += 1;
            _voiceoverTimer = 0;

            if (_voiceoverIndex < Voiceovers.Length)
            {
                if (_voiceoverDisplay == null)
                {
                    _voiceoverDisplay = VoiceoverDisplayPrefab.Instance() as VoiceoverDisplay;
                    gameplay.AddChild(_voiceoverDisplay);
                }
                _voiceoverDisplay.Play(Voiceovers[_voiceoverIndex]);
            }
            else
            {
                if (_voiceoverDisplay != null)
                {
                    _voiceoverDisplay.QueueFree();
                    _voiceoverDisplay = null;
                }
            }
        }
    }
}