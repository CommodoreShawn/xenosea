﻿using Xenosea.Logic.Hails;

public class DefaultHailResponseLogicSource : AbstractHailResponseLogicSource
{
    public override IHailResponseLogic GetResponseLogic(GoalBotController controller)
    {
        return new DefaultHailResponseLogic(controller);
    }
}
