﻿using Godot;
using Xenosea.Logic.Hails;

public abstract class AbstractHailResponseLogicSource : Resource
{
    public abstract IHailResponseLogic GetResponseLogic(GoalBotController controller);
}
