﻿namespace Xenosea.Resources
{
    public enum ContactType
    {
        Unknown,
        Sub,
        Station,
        Outpost,
        Wreck,
        Waypoint,
        Landmark,
        Torpedo,
        Minisub,
    }
}
