﻿using Godot;
using Godot.Collections;

public class Npc : Resource
{
    [Export]
    public Texture ImageSmall;

    [Export]
    public Texture ImageLarge;

    [Export]
    public string Name;

    [Export]
    public string Title;

    [Export]
    public string DialogueFilename;

    [Export]
    public string[] Traits;

    public Dictionary DialogueState = new Dictionary();
}
