﻿using Godot;

public class MusicOption : Resource
{
    [Export]
    public AudioStream MusicStream;

    [Export]
    public bool IsExploration;

    [Export]
    public bool IsBattle;
}