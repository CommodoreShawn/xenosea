﻿using Godot;

public class CommodityQuantity : Resource
{
    [Export]
    public CommodityType Commodity;

    [Export]
    public int Quantity;
}
