using Godot;
using System;
using System.Collections.Generic;
using Xenosea.Logic.Infrastructure.Data;

public class EconomicProcess : AbstractEconomicProcess
{
    [Export]
    public override string Name { get; set; }
    [Export]
    public override float ProcessTime { get; set; }
    [Export]
    public override bool IsIndustrial { get; set; }
    [Export]
    public override float AgentSupport { get; set; }
    [Export]
    public override float InfluenceStrength { get; set; }
    [Export]
    public override bool IsCivil { get; set; }
    [Export]
    public override bool IsDefense { get; set; }
    [Export]
    public override bool IsFarm { get; set; }
    [Export]
    public CommodityType[] Inputs { get; set; }
    [Export]
    public int[] InputQty { get; set; }
    [Export]
    public CommodityType[] Outputs { get; set; }
    [Export]
    public int[] OutputsQty { get; set; }
    [Export]
    public override float IndustrialSupport { get; set; }
    [Export]
    public override float MilitarySupport { get; set; }

    public override float GetNetPerSecond(CommodityType commodityType, SimulationStation simulationStation)
    {
        var net = 0f;

        for(var i = 0; i < Outputs.Length; i++)
        {
            if(Outputs[i] == commodityType)
            {
                net += OutputsQty[i] / ProcessTime;
            }
        }

        for (var i = 0; i < Inputs.Length; i++)
        {
            if (Inputs[i] == commodityType)
            {
                net -= InputQty[i] / ProcessTime;
            }
        }

        return net;
    }

    public override IEnumerable<Tuple<float, CommodityType>> InputsForCycle(SimulationStation simulationStation)
    {
        var result = new Tuple<float, CommodityType>[Inputs.Length];

        for (var i = 0; i < Inputs.Length; i++)
        {
            result[i] = Tuple.Create((float)InputQty[i], Inputs[i]);
        }

        return result;
    }

    public override IEnumerable<Tuple<float, CommodityType>> OutputsForCycle(SimulationStation simulationStation)
    {
        var result = new Tuple<float, CommodityType>[Outputs.Length];

        for (var i = 0; i < Outputs.Length; i++)
        {
            result[i] = Tuple.Create((float)OutputsQty[i], Outputs[i]);
        }

        return result;
    }
}
