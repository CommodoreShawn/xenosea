﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;

public class FarmingEconomicProcess : AbstractEconomicProcess
{
    [Export]
    public override string Name { get; set; }
    [Export]
    public override float ProcessTime { get; set; }
    [Export]
    public override bool IsIndustrial { get; set; }
    [Export]
    public override float AgentSupport { get; set; }
    [Export]
    public override float InfluenceStrength { get; set; }
    [Export]
    public override bool IsCivil { get; set; }
    [Export]
    public override bool IsDefense { get; set; }
    [Export]
    public override bool IsFarm { get; set; }
    [Export]
    public override float IndustrialSupport { get; set; }
    [Export]
    public override float MilitarySupport { get; set; }
    [Export]
    public CommodityType[] Outputs { get; set; }
    [Export]
    public int[] OutputsQty { get; set; }

    public override float GetNetPerSecond(CommodityType commodityType, SimulationStation simulationStation)
    {
        var net = 0f;

        if (simulationStation?.Location == null)
        {
            return net;
        }

        for (var i = 0; i < Outputs.Length; i++)
        {
            if(Outputs[i] == commodityType)
            {
                net += OutputsQty[i] / ProcessTime * simulationStation.Location.Fertility;
            }
        }

        return net;
    }

    public override IEnumerable<Tuple<float, CommodityType>> InputsForCycle(SimulationStation simulationStation)
    {
        return Enumerable.Empty<Tuple<float, CommodityType>>();
    }

    public override IEnumerable<Tuple<float, CommodityType>> OutputsForCycle(SimulationStation simulationStation)
    {
        var result = new Tuple<float, CommodityType>[Outputs.Length];

        if (simulationStation?.Location == null)
        {
            return result;
        }

        for (var i = 0; i < Outputs.Length; i++)
        {
            result[i] = Tuple.Create(OutputsQty[i] * simulationStation.Location.Fertility, Outputs[i]);
        }

        return result;
    }
}
