﻿using Godot;
using System;
using System.Collections.Generic;
using Xenosea.Logic.Infrastructure.Data;

public abstract class AbstractEconomicProcess : Resource
{
    public abstract string Name { get; set; }
    public abstract float ProcessTime { get; set; }
    public abstract float AgentSupport { get; set; }
    public abstract float InfluenceStrength { get; set; }
    public abstract bool IsIndustrial { get; set; }
    public abstract bool IsCivil { get; set; }
    public abstract bool IsDefense { get; set; }
    public abstract bool IsFarm { get; set; }
    public abstract float IndustrialSupport { get; set; }
    public abstract float MilitarySupport { get; set; }

    public abstract IEnumerable<Tuple<float, CommodityType>> InputsForCycle(SimulationStation simulationStation);

    public abstract IEnumerable<Tuple<float, CommodityType>> OutputsForCycle(SimulationStation simulationStation);

    public abstract float GetNetPerSecond(CommodityType commodityType, SimulationStation simulationStation);
}