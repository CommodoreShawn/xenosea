﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;

public class MiningEconomicProcess : AbstractEconomicProcess
{
    [Export]
    public override string Name { get; set; }
    [Export]
    public override float ProcessTime { get; set; }
    [Export]
    public override bool IsIndustrial { get; set; }
    [Export]
    public int OutputQty { get; set; }
    [Export]
    public override float AgentSupport { get; set; }
    [Export]
    public override float InfluenceStrength { get; set; }
    [Export]
    public override bool IsCivil { get; set; }
    [Export]
    public override bool IsDefense { get; set; }
    [Export]
    public override bool IsFarm { get; set; }
    [Export]
    public override float IndustrialSupport { get; set; }
    [Export]
    public override float MilitarySupport { get; set; }
    [Export]
    public CommodityType IronCommodity;
    [Export]
    public CommodityType CopperCommodity;
    [Export]
    public CommodityType LeadCommodity;
    [Export]
    public CommodityType MagnesiumCommodity;
    [Export]
    public CommodityType NickelCommodity;
    [Export]
    public CommodityType PhosphorusCommodity;
    [Export]
    public CommodityType SulfurCommodity;
    [Export]
    public CommodityType ThoriumCommodity;

    public override float GetNetPerSecond(CommodityType commodityType, SimulationStation simulationStation)
    {
        var net = 0f;

        if (simulationStation?.Location == null)
        {
            return net;
        }

        foreach (var output in OutputsForCycle(simulationStation))
        {
            if(output.Item2 == commodityType)
            {
                net += output.Item1 / ProcessTime;
            }
        }

        return net;
    }

    public override IEnumerable<Tuple<float, CommodityType>> InputsForCycle(SimulationStation simulationStation)
    {
        return Enumerable.Empty<Tuple<float, CommodityType>>();
    }

    public override IEnumerable<Tuple<float, CommodityType>> OutputsForCycle(SimulationStation simulationStation)
    {
        var result = new List<Tuple<float, CommodityType>>();

        if(simulationStation?.Location == null)
        {
            return result;
        }

        if(simulationStation.Location.IronOutput > 0)
        {
            result.Add(Tuple.Create(OutputQty * simulationStation.Location.IronOutput, IronCommodity));
        }
        if (simulationStation.Location.CopperOutput > 0)
        {
            result.Add(Tuple.Create(OutputQty * simulationStation.Location.CopperOutput, CopperCommodity));
        }
        if (simulationStation.Location.LeadOutput > 0)
        {
            result.Add(Tuple.Create(OutputQty * simulationStation.Location.LeadOutput, LeadCommodity));
        }
        if (simulationStation.Location.MagnesiumOutput > 0)
        {
            result.Add(Tuple.Create(OutputQty * simulationStation.Location.MagnesiumOutput, MagnesiumCommodity));
        }
        if (simulationStation.Location.NickelOutput > 0)
        {
            result.Add(Tuple.Create(OutputQty * simulationStation.Location.NickelOutput, NickelCommodity));
        }
        if (simulationStation.Location.PhosphorusOutput > 0)
        {
            result.Add(Tuple.Create(OutputQty * simulationStation.Location.PhosphorusOutput, PhosphorusCommodity));
        }
        if (simulationStation.Location.SulfurOutput > 0)
        {
            result.Add(Tuple.Create(OutputQty * simulationStation.Location.SulfurOutput, SulfurCommodity));
        }
        if (simulationStation.Location.ThoriumOutput > 0)
        {
            result.Add(Tuple.Create(OutputQty * simulationStation.Location.ThoriumOutput, ThoriumCommodity));
        }

        return result;
    }
}
