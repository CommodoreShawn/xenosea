﻿using Godot;

public class CommodityType : Resource
{
    [Export]
    public string Name { get; set; }

    [Export]
    public float BaseValue { get; set; }

    [Export]
    public Texture Icon { get; set; }

    [Export]
    public float Mass { get; set; }

    [Export(PropertyHint.MultilineText)]
    public string Description;
}
