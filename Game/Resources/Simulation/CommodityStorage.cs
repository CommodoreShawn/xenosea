﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

public class CommodityStorage
{
    private Dictionary<CommodityType, float> _dictionary;

    public bool PerCommodityCapped { get; private set; }
    public float Cap { get; private set; }
    public float TotalVolume => _dictionary.Values.Sum();

    public IEnumerable<CommodityType> CommoditiesPresent => _dictionary.Keys;

    public CommodityStorage(float cap, bool perCommodityCapped)
    {
        _dictionary = new Dictionary<CommodityType, float>();
        Cap = cap;
        PerCommodityCapped = perCommodityCapped;
    }

    public float GetQty(CommodityType commodity)
    {
        if(_dictionary.ContainsKey(commodity))
        {
            return _dictionary[commodity];
        }

        return 0;
    }

    public void Remove(CommodityType commodity, float qty)
    {
        if (_dictionary.ContainsKey(commodity))
        {
            _dictionary[commodity] = Mathf.Max(0, _dictionary[commodity] - qty);

            if(_dictionary[commodity] == 0)
            {
                _dictionary.Remove(commodity);
            }
        }
    }

    public void Add(CommodityType commodity, float qty)
    {
        if(!_dictionary.ContainsKey(commodity))
        {
            _dictionary.Add(commodity, 0);
        }

        if(PerCommodityCapped)
        {
            _dictionary[commodity] = Mathf.Min(Cap, _dictionary[commodity] + qty);
        }
        else
        {
            var remainingSpace = Cap - _dictionary.Values.Sum();

            _dictionary[commodity] += Mathf.Min(remainingSpace, qty);
        }
    }

    public float RemainingCapacity(CommodityType commodity)
    {
        if(PerCommodityCapped)
        {
            return Cap - GetQty(commodity);
        }

        return Cap - TotalVolume;
    }

    public void SaveState(SerializationObject storageRoot)
    {
        foreach(var kvp in _dictionary)
        {
            storageRoot.AddChild("item")
                .SetAttribute("key", kvp.Key.Name)
                .SetAttribute("val", kvp.Value.ToString());
        }
    }

    public void LoadState(IResourceLocator resourceLocator, SerializationObject storageRoot)
    {
        _dictionary.Clear();

        foreach(var child in storageRoot.Children)
        {
            if(child.Name == "item")
            {
                var commodityType = resourceLocator.GetCommodityType(child.GetAttribute("key"));

                Add(commodityType, Convert.ToSingle(child.GetAttribute("val")));
            }
        }

    }

    public void CloneFrom(CommodityStorage other)
    {
        _dictionary = other._dictionary;
        PerCommodityCapped = other.PerCommodityCapped;
        Cap = other.Cap;
    }
}
