using Godot;

public class Alliance : Resource
{
    [Export]
    public Faction Leader;

    [Export]
    public Faction[] Members;
}
