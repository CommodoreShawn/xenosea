﻿using Godot;

public class StartingFactionRelation : Resource
{
    [Export]
    public Faction[] Factions = new Faction[0];

    [Export]
    public Alliance[] Alliances = new Alliance[0];

    [Export]
    public float StartingRelation;
}
