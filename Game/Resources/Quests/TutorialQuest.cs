﻿using Autofac;
using Godot;
using System;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

public class TutorialQuest : AbstractQuest
{
    public static readonly string STATE_NOT_STARTED = "not_started";
    public static readonly string STATE_APPROACH_WRECK = "approach_wreck";
    public static readonly string STATE_DIVERS = "divers";
    public static readonly string STATE_BACK_OFF = "back_off";
    public static readonly string STATE_TARGET_PRACTICE = "target_practice";
    public static readonly string STATE_NORTHBURG = "northburg";
    public static readonly string STATE_DOCK = "dock";
    public static readonly string STATE_DONE = "done";

    public override string Id => "TutorialQuest";
    public override string CurrentStateId { get; protected set; }
    public override TimeSpan? AllowedTime => null;
    public override DateTime? DueDate { get; set; }
    public override string Name => "Getting Started";
    public override double Deposit { get; }
    public override double Reward { get; }
    public override string Description { get; protected set; }
    public override string Detail0 { get; protected set; }
    public override string Detail1 { get; protected set; }
    public override string Detail2 { get; protected set; }
    public override bool IsExpired => false;
    public override MissionPath MissionPath { get; set; }
    public override bool IsActive { get; protected set; }
    public override Faction ForFaction => null;

    private INotificationSystem _notificationSystem;
    private ISimulationEventBus _simulationEventBus;
    private IWorldManager _worldManager;
    private IPlayerData _playerData;

    [Export]
    public Npc InitialConversationXo;

    private bool _didSalvage;

    public override void Initialize()
    {
        _notificationSystem = IocManager.IocContainer.Resolve<INotificationSystem>();
        _simulationEventBus = IocManager.IocContainer.Resolve<ISimulationEventBus>();
        _worldManager = IocManager.IocContainer.Resolve<IWorldManager>();
        _playerData = IocManager.IocContainer.Resolve<IPlayerData>();
    }

    public override void Reset()
    {
        base.Reset();
        CurrentStateId = STATE_NOT_STARTED;
    }

    public override bool CanBeDelivered(Submarine playerSub)
    {
        return false;
    }

    public override void Process(float delta)
    {
        _playerData.InTutorial = IsActive;

        if(CurrentStateId != STATE_DONE
            && CurrentStateId != STATE_NOT_STARTED)
        {
            var wreckDistance = float.MaxValue;

            var wreck = _playerData.PlayerSub.SonarContactRecords.Values
                .Where(x => x.IsValidInstance)
                .Where(x => x.SonarContact is Submarine sub && sub.Name == "Tutorial Target")
                .Select(x=>x.SonarContact as Submarine)
                .FirstOrDefault();

            if(wreck != null)
            {
                wreckDistance = _playerData.PlayerSub.RealPosition.DistanceTo(wreck.RealPosition);
            }

            if(wreck == null && CurrentStateId != STATE_NORTHBURG && CurrentStateId != STATE_DOCK)
            {
                _notificationSystem.AddNotification(new Notification
                {
                    IsGood = false,
                    IsMessage = true,
                    IsImportant = true,
                    Body = "We've lost the wreck, so much for using it as a learning opportunity."
                });
                _notificationSystem.AddNotification(new Notification
                {
                    IsGood = false,
                    IsMessage = true,
                    IsImportant = true,
                    Body = "Ended quest: " + Name
                });
                CurrentStateId = STATE_DONE;
                IsActive = false;
            }
            if(CurrentStateId == STATE_APPROACH_WRECK && wreckDistance <= 50)
            {
                _playerData.PlayerSub.Throttle = 0;
                _playerData.PlayerSub.Dive = 0;
                _simulationEventBus.SendEvent(new StartConversationSimulationEvent(InitialConversationXo));
            }
            else if(CurrentStateId == STATE_BACK_OFF && wreckDistance >= 300)
            {
                _playerData.PlayerSub.Throttle = 0;
                _playerData.PlayerSub.Dive = 0;
                _simulationEventBus.SendEvent(new StartConversationSimulationEvent(InitialConversationXo));
            }
        }

        if (CurrentStateId == STATE_DIVERS
            && _didSalvage
            && !_playerData.PlayerSub.LeakBubbles.Any()
            && _playerData.PlayerSub.DiveTeams.All(dt => dt.DeployedDiver == null))
        {
            _playerData.PlayerSub.Throttle = 0;
            _playerData.PlayerSub.Dive = 0;
            _simulationEventBus.SendEvent(new StartConversationSimulationEvent(InitialConversationXo));
        }
        else if (CurrentStateId == STATE_NORTHBURG)
        {
            var northburg = _playerData.PlayerSub.SonarContactRecords.Values
                .Where(x => x.IsValidInstance)
                .Where(x => x.SonarContact is WorldStation station && station.Name == "Northburg")
                .Select(x => x.SonarContact as WorldStation)
                .FirstOrDefault();

            if(northburg != null && _playerData.PlayerSub.RealPosition.DistanceTo(northburg.RealPosition) < 5000)
            {
                _playerData.PlayerSub.Throttle = 0;
                _playerData.PlayerSub.Dive = 0;
                _simulationEventBus.SendEvent(new StartConversationSimulationEvent(InitialConversationXo));
            }
        }
        else if (CurrentStateId == STATE_DOCK
                && _playerData.PlayerSub.IsDockedTo?.FriendlyName == "Northburg")
        {
            _notificationSystem.AddNotification(new Notification
            {
                IsGood = true,
                IsMessage = true,
                IsImportant = true,
                Body = "Ended quest: " + Name
            });
            CurrentStateId = STATE_DONE;
            IsActive = false;
        }
    }

    public override void ProcessPlayerLogic(
        INotificationSystem notificationSystem, 
        Submarine playerSub, 
        bool isTrackedMission, 
        float delta)
    {
    }

    public override void OnSimulationEvent(ISimulationEvent evt)
    {
        if(evt is SetQuestStateSimulationEvent questStateEvt
            && questStateEvt.QuestId == Id)
        {
            if (CurrentStateId == STATE_NOT_STARTED
                && questStateEvt.State == STATE_APPROACH_WRECK)
            {
                _notificationSystem.AddNotification(new Notification
                {
                    IsGood = true,
                    IsMessage = true,
                    IsImportant = true,
                    Body = "Started quest: " + Name
                });
                CurrentStateId = STATE_APPROACH_WRECK;
                IsActive = true;
                Description = "Lt. Scott wants you to approach that nearby wreck.";
                Detail0 = "Approach within 50m of the Wreck";
                Detail1 = "";
                _simulationEventBus.SendEvent(new NewQuestSimulationEvent(this));
            }
            else if (CurrentStateId == STATE_APPROACH_WRECK
                && questStateEvt.State == STATE_DIVERS)
            {
                CurrentStateId = STATE_DIVERS;
                Description = "Dispatch salvage divers to the wreck, and a repair diver.";
                Detail0 = "Dispatch salvage and repair divers to the wreck";
                Detail1 = "Wait for the divers to retun";
            }
            else if (CurrentStateId == STATE_DIVERS
                && questStateEvt.State == STATE_BACK_OFF)
            {
                CurrentStateId = STATE_BACK_OFF;
                Description = "Back off from the wreck for target practice.";
                Detail0 = "Back off to at least 300m";
                Detail1 = "";
            }
            else if (CurrentStateId == STATE_BACK_OFF
                && questStateEvt.State == STATE_TARGET_PRACTICE)
            {
                CurrentStateId = STATE_TARGET_PRACTICE;
                Description = "Hit the wreck with gun turrets or torpedoes.";
                Detail0 = "";
                Detail1 = "";
                _playerData.WeaponsSafe = false;
            }
            else if (CurrentStateId == STATE_TARGET_PRACTICE
                && questStateEvt.State == STATE_NORTHBURG)
            {
                CurrentStateId = STATE_NORTHBURG;
                Description = "Set course for Northburg.";
                Detail0 = "Open the full Map.";
                Detail1 = "Click to set a waypoint.";
            }
            else if (CurrentStateId == STATE_NORTHBURG
                && questStateEvt.State == STATE_DOCK)
            {
                CurrentStateId = STATE_DOCK;
                Description = "Target Northburg and select dock.";
                Detail0 = "";
                Detail1 = "";
            }
        }
        else if(evt is PlayerSalvagedSimulationEvent salvageEvent
            && CurrentStateId == STATE_DIVERS
            && salvageEvent.SalvagedSub is Submarine submarine
            && submarine.Name == "Tutorial Target")
        {
            _didSalvage = true;
        }
        else if (evt is PlayerHitSubmarineSimulationEvent playerHitEvent
            && CurrentStateId == STATE_TARGET_PRACTICE
            && playerHitEvent.Target.Name == "Tutorial Target")
        {
            _playerData.WeaponsSafe = true;
            _playerData.PlayerSub.Throttle = 0;
            _playerData.PlayerSub.Dive = 0;
            _simulationEventBus.SendEvent(new StartConversationSimulationEvent(InitialConversationXo));
        }
    }
}
