﻿using Autofac;
using System;
using Xenosea.Logic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

public class TheDeathOfTheFitzgeraldQuest : AbstractQuest
{
    public const string STATE_NOT_STARTED = "not_started";
    public const string STATE_LOOKING_FOR_WRECK = "looking_for_wreck";
    public const string STATE_ASK_ABOUT_WRECK = "ask_about_wreck";
    public const string STATE_TELL_ABOUT_WRECK= "tell_about_wreck";
    public const string STATE_WHISTLEBLOWER = "whistleblower";
    public const string STATE_WAITING_ON_REPORT_OFFICIAL = "waiting_on_report_official";
    public const string STATE_WAITING_ON_REPORT_TALIN = "waiting_on_report_talin";
    public const string STATE_WAITING_ON_REPORT_OMAR = "waiting_on_report_omar";
    public const string STATE_DONE = "done";

    public override string Id => "TheDeathOfTheFitzgeraldQuest";
    public override string CurrentStateId { get; protected set; }
    public override TimeSpan? AllowedTime => null;
    public override DateTime? DueDate { get; set; }
    public override string Name => "The Death Of The Fitzgerald";
    public override double Deposit { get; }
    public override double Reward { get; }
    public override string Description { get; protected set; }
    public override string Detail0 { get; protected set; }
    public override string Detail1 { get; protected set; }
    public override string Detail2 { get; protected set; }
    public override bool IsExpired => false;
    public override MissionPath MissionPath { get; set; }
    public override bool IsActive { get; protected set; }
    public override Faction ForFaction => null;

    private DateTime _reportDate;

    private INotificationSystem _notificationSystem;
    private ISimulationEventBus _simulationEventBus;
    private IWorldManager _worldManager;
    private IPlayerData _playerData;

    public override void Initialize()
    {
        _notificationSystem = IocManager.IocContainer.Resolve<INotificationSystem>();
        _simulationEventBus = IocManager.IocContainer.Resolve<ISimulationEventBus>();
        _worldManager = IocManager.IocContainer.Resolve<IWorldManager>();
        _playerData = IocManager.IocContainer.Resolve<IPlayerData>();
    }

    public override void Reset()
    {
        base.Reset();

        CurrentStateId = STATE_NOT_STARTED;
        _reportDate = DateTime.MinValue;
    }

    public override bool CanBeDelivered(Submarine playerSub)
    {
        return false;
    }

    public override void Process(float delta)
    {
        if((CurrentStateId == STATE_WAITING_ON_REPORT_OFFICIAL
            || CurrentStateId == STATE_WAITING_ON_REPORT_OMAR
            || CurrentStateId == STATE_WAITING_ON_REPORT_TALIN)
            && _worldManager.CurrentDate >= _reportDate)
        {
            CurrentStateId = STATE_DONE;

            // TODO add news article based on resolution type
        }
    }

    public override void ProcessPlayerLogic(
        INotificationSystem notificationSystem, 
        Submarine playerSub, 
        bool isTrackedMission, 
        float delta)
    {
    }

    public override void OnSimulationEvent(ISimulationEvent evt)
    {
        if(evt is PlayerSalvagedSimulationEvent salvageEvent
            && salvageEvent.SalvagedSub is PersistentWreck persistentWreck
            && persistentWreck.Name == "The Fitzgerald")
        {
            if(CurrentStateId == STATE_NOT_STARTED)
            {
                SetQuestState(STATE_ASK_ABOUT_WRECK);
            }
            else if (CurrentStateId == STATE_LOOKING_FOR_WRECK)
            {
                SetQuestState(STATE_TELL_ABOUT_WRECK);
            }
        }
        else if (evt is SetQuestStateSimulationEvent questStateEvt
                && questStateEvt.QuestId == Id)
        {
            SetQuestState(questStateEvt.State);
        }
    }

    private void SetQuestState(string newState)
    {
        CurrentStateId = newState;

        switch(CurrentStateId)
        {
            case STATE_NOT_STARTED:
                break;
            case STATE_LOOKING_FOR_WRECK:
                Description = "Gillian Mclaren wants you to find her brother's sub, the Fitzgerald, last seen heading from Northburg to Bastion.";
                Detail0 = "Salvage the Fitzgerald";
                Detail1 = "Look east of Northburg";
                _notificationSystem.AddNotification(new Notification
                {
                    IsGood = true,
                    IsMessage = true,
                    IsImportant = true,
                    Body = "Started quest: " + Name
                });
                IsActive = true;
                _simulationEventBus.SendEvent(new NewQuestSimulationEvent(this));
                break;
            case STATE_ASK_ABOUT_WRECK:
                Description = "You've recovered an logbook from a wrecked submarine. It's written in a strange cypher, maybe you can find someone who knows more.";
                Detail0 = "Ask about the Fitzgerald";
                Detail1 = string.Empty;
                _notificationSystem.AddNotification(new Notification
                {
                    IsGood = true,
                    IsMessage = true,
                    IsImportant = true,
                    Body = "Your salvage team has discovered an odd logbook on the submarine."
                });
                _notificationSystem.AddNotification(new Notification
                {
                    IsGood = true,
                    IsMessage = true,
                    IsImportant = true,
                    Body = "Started quest: " + Name
                });
                IsActive = true;

                if (!_playerData.KnownTopics.Contains("Fitzgerald"))
                {
                    _playerData.KnownTopics.Add("Fitzgerald");
                }
                _simulationEventBus.SendEvent(new NewQuestSimulationEvent(this));
                break;
            case STATE_TELL_ABOUT_WRECK:
                _notificationSystem.AddNotification(new Notification
                {
                    IsGood = true,
                    IsMessage = true,
                    IsImportant = true,
                    Body = "Your salvage team has discovered an odd logbook on the Fitzgerald."
                });
                Description = "Gillian Mclaren wants you to find her brother's sub, the Fitzgerald, last seen heading south from Bastion. You found an odd logbook on board.";
                Detail0 = "Bring the logbook to Gillian Mclaren";
                Detail1 = "Find her at the Rusty Torpedo in Northburg";
                break;
            case STATE_WHISTLEBLOWER:
                Description = "Gillian Mclaren wants you to find someone who can hold Interstation Shipping accountable for secretly supplying the pirates.";
                Detail0 = "Ask around the stations.";
                Detail1 = string.Empty;
                break;
            case STATE_WAITING_ON_REPORT_OFFICIAL:
            case STATE_WAITING_ON_REPORT_OMAR:
            case STATE_WAITING_ON_REPORT_TALIN:
                // TODO when news is in, schedule the news report
                IsActive = false;
                break;
        }
    }

    public override void SerializeTo(SerializationObject questRoot)
    {
        base.SerializeTo(questRoot);
        questRoot.SetAttribute("report_date", _reportDate.ToString());
    }

    public override void DeserializeFrom(SerializationObject questRoot)
    {
        base.DeserializeFrom(questRoot);
        _reportDate = Convert.ToDateTime(questRoot.GetAttribute("report_date"));
    }
}
