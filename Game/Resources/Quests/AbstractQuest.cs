﻿using Godot;
using System;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.Infrastructure.Missions;
using Xenosea.Logic.SimulationEvents;

public abstract class AbstractQuest : Resource, IQuest, IMission
{
    public abstract TimeSpan? AllowedTime { get; }
    public abstract DateTime? DueDate { get; set; }
    public abstract string Id { get; }
    public abstract string Name { get; }
    public abstract double Deposit { get; }
    public abstract double Reward { get; }
    public abstract string Description { get; protected set; }
    public abstract string Detail0 { get; protected set; }
    public abstract string Detail1 { get; protected set; }
    public abstract string Detail2 { get; protected set; }
    public abstract bool IsExpired { get; }
    public abstract MissionPath MissionPath { get; set; }
    public abstract string CurrentStateId { get; protected set; }
    public abstract bool IsActive { get; protected set; }
    public abstract Faction ForFaction { get; }
    public bool CanBeSerialized => false;

    public abstract bool CanBeDelivered(Submarine playerSub);
    public abstract void Initialize();

    public abstract void OnSimulationEvent(ISimulationEvent evt);

    public abstract void Process(float delta);
    public abstract void ProcessPlayerLogic(INotificationSystem notificationSystem, Submarine playerSub, bool isTrackedMission, float delta);

    public virtual void Reset()
    {
        IsActive = false;
        Description = string.Empty;
        Detail0 = string.Empty;
        Detail1 = string.Empty;
        Detail2 = string.Empty;
    }

    public virtual void SerializeTo(SerializationObject questRoot)
    {
        questRoot.SetAttribute("is_active", IsActive.ToString());
        questRoot.SetAttribute("current_state", CurrentStateId);
        questRoot.SetAttribute("description", Description);
        questRoot.SetAttribute("detail0", Detail0);
        questRoot.SetAttribute("detail1", Detail1);
        questRoot.SetAttribute("detail2", Detail2);
    }

    public virtual void DeserializeFrom(SerializationObject questRoot)
    {
        IsActive = Convert.ToBoolean(questRoot.GetAttribute("is_active"));
        CurrentStateId = questRoot.GetAttribute("current_state");
        Description = questRoot.GetAttribute("description");
        Detail0 = questRoot.GetAttribute("detail0");
        Detail1 = questRoot.GetAttribute("detail1");
        Detail2 = questRoot.GetAttribute("detail2");
    }
}
