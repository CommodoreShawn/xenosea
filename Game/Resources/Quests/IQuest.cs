﻿using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.SimulationEvents;

public interface IQuest
{
    string Id { get; }
    string CurrentStateId { get; }
    bool IsActive { get; }
    void Process(float delta);
    void Reset();
    void Initialize();
    void OnSimulationEvent(ISimulationEvent evt);
    void SerializeTo(SerializationObject questRoot);
    void DeserializeFrom(SerializationObject questRoot);
}
