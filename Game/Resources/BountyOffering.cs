﻿using Godot;

public class BountyOffering : Resource
{
    [Export]
    public Faction[] OfferingFactions;

    [Export]
    public Faction[] TargetFactions;
}
