﻿namespace Xenosea.Resources
{
    public enum StationShapeProfile
    {
        Peak,
        Flat
    }
}
