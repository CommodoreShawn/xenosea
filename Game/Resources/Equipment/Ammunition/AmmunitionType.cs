using Godot;

public class AmmunitionType : Resource
{
    [Export]
    public PackedScene BulletPrefab;

    [Export]
    public PackedScene ShotEffectPrefab;

    [Export]
    public Texture Icon;

    [Export]
    public Texture SmallIcon;

    [Export]
    public string Name;

    [Export]
    public float FiringNoise;

    [Export]
    public float MaximumRange;

    [Export]
    public float Damage;

    [Export]
    public float ArmorPenetration;

    [Export]
    public float MuzzleVelocity;

    [Export]
    public float LeadVelocity;
}
