using Godot;

public class TorpedoType : Resource
{
    [Export]
    public PackedScene TorpedoPrefab;

    [Export]
    public PackedScene LaunchEffectPrefab;

    [Export]
    public Texture Icon;

    [Export]
    public string Name;

    [Export]
    public float LaunchingNoise;

    [Export]
    public double Value;

    [Export]
    public TorpedoSizeCategory SizeCategory;

    [Export(PropertyHint.MultilineText)]
    public string Description;

    [Export]
    public double Damage;

    [Export]
    public double Penetration;

    [Export]
    public double Speed;

    [Export]
    public double FuelTime;
    [Export]
    public float IndustrialNeed;
    [Export]
    public float MilitaryNeed;
    [Export]
    public Faction[] Operators;
    [Export]
    public float ReputationRequired;
}