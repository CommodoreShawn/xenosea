﻿using Godot;

public class SubHullType : Resource
{
    [Export]
    public string SubHullTypeId;
    [Export]

    public string Name;
    [Export(PropertyHint.MultilineText)]
    public string Description;

    [Export]
    public PackedScene SubPrefab;

    [Export]
    public float Volume;
    [Export]
    public float Health;
    [Export]
    public float Value;
    [Export]
    public int Turrets;
    [Export]
    public int TorpedoTubes;
    [Export]
    public int MinisubHangars;
    [Export]
    public float FrontDragFactor;
    [Export]
    public float SideDragFactor;
    [Export]
    public float HullSurfaceArea;
    [Export]
    public float IndustrialNeed;
    [Export]
    public float MilitaryNeed;
    [Export]
    public Faction[] Operators;
    [Export]
    public float ReputationRequired;

    public float GetBaseNoise()
    {
         return Volume / 10f;
    }
}
