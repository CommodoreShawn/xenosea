﻿using Godot;
using Xenosea.Logic.Detection;

public abstract class DiveTeamType : Resource
{
    [Export]
    public string Name;

    [Export(PropertyHint.MultilineText)]
    public string Description;

    [Export]
    public float Value;

    [Export]
    public float RecoverTime;

    [Export]
    public PackedScene DiverPrefab;

    [Export]
    public float IndustrialNeed;
    [Export]
    public float MilitaryNeed;
    [Export]
    public Faction[] Operators;

    public abstract bool SelfTargets { get; }

    public abstract bool IsValidTarget(Submarine parentSubmarine, ITargetableThing target);
}
