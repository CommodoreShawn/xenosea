﻿using Xenosea.Logic.Detection;

public class RepairDiveTeamType : DiveTeamType
{
    public override bool SelfTargets => true;

    public override bool IsValidTarget(Submarine parentSubmarine, ITargetableThing target)
    {
        return parentSubmarine.LeakBubbles.Count > 0;
    }
}
