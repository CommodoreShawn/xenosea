﻿using Godot;
using Xenosea.Logic.Detection;

public class SalvageDiveTeamType : DiveTeamType
{
    public override bool SelfTargets => false;

    public override bool IsValidTarget(Submarine parentSubmarine, ITargetableThing target)
    {
        if(target == null)
        {
            return false;
        }

        if(parentSubmarine.CommodityStorage.TotalVolume  >= parentSubmarine.CargoVolume)
        {
            return false;
        }

        if (target is ISalvageTarget salvageTarget)
        {
            return salvageTarget.CanBeSalvaged;
        }
        else if (target is SonarContactRecord contactRecord
            && contactRecord.SonarContact is ISalvageTarget salvageTarget1)
        {
            return salvageTarget1.CanBeSalvaged;
        }

        return false;
    }
}
