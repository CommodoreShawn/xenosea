using Godot;

public class EngineModuleType : BasicModuleType
{
    [Export]
    public float MaximumThrust;

    [Export]
    public float TurningFactor;

    [Export]
    public float MaximumNoise;

    public float RudderThrust => MaximumThrust * TurningFactor * 10;
}