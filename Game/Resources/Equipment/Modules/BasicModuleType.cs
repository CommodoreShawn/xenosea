﻿using Godot;

public class BasicModuleType : Resource
{
    [Export]
    public string ModuleTypeId;

    [Export]
    public ModuleCategory Category;

    [Export]
    public string Name;

    [Export(PropertyHint.MultilineText)]
    public string Description;

    [Export]
    public float Volume;

    [Export]
    public float Value;
    [Export]
    public float IndustrialNeed;
    [Export]
    public float MilitaryNeed;
    [Export]
    public Faction[] Operators;

    [Export]
    public float CombatThreat;
    [Export]
    public float ReputationRequired;
}
