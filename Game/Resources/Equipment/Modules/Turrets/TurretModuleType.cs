﻿using Godot;

public class TurretModuleType : BasicModuleType
{
    [Export]
    public PackedScene Prefab;

    [Export]
    public float TurnRate;

    [Export]
    public float ReloadTime;

    [Export]
    public int ShotCount;

    [Export]
    public AmmunitionType[] AmmunitionOptions;
}