﻿using Godot;

public class SonarModuleType : BasicModuleType
{
    [Export]
    public float Sensitivity;

    [Export]
    public float OreFactor;

    [Export]
    public float SubFactor;
}