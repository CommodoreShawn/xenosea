﻿public enum ModuleCategory
{
    Normal,
    Torpedo,
    Engine,
    Turret,
    Hangar,
    Sonar,
    TowedArray
}