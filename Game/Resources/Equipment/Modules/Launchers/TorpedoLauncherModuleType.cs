﻿using Godot;

public class TorpedoLauncherModuleType : BasicModuleType
{
    [Export]
    public int MagazineCapacity;

    [Export]
    public TorpedoSizeCategory MaximumTorpedoSize;

    [Export]
    public float ReloadTime;
}