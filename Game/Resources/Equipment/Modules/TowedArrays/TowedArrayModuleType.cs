﻿using Godot;

public class TowedArrayModuleType : BasicModuleType
{
    [Export]
    public PackedScene Prefab;

    [Export]
    public float Sensitivity;

    [Export]
    public float OreFactor;

    [Export]
    public float SubFactor;
}