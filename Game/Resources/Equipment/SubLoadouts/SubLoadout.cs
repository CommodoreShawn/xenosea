using Godot;
using System.Collections.Generic;
using System.Linq;

public class SubLoadout : Resource
{
    [Export]
    public SubHullType HullType;

    [Export]
    public ArmorType ArmorType;

    [Export]
    public EngineModuleType EngineModule;

    [Export]
    public SonarModuleType SonarModule;

    [Export]
    public TowedArrayModuleType TowedArrayModule;

    [Export]
    public TorpedoLauncherModuleType[] LauncherModules;

    [Export]
    public TurretModuleType[] TurretModules;

    [Export]
    public MinisubHangarModuleType[] HangarModules;

    [Export]
    public BasicModuleType[] OtherModules;

    [Export]
    public DiveTeamType[] DiveTeams;

    [Export]
    public TorpedoType[] TorpedoesPerTube;

    [Export]
    public MinisubType[] Minisubs;


    public IEnumerable<BasicModuleType> GetAllModules()
    {
        return new BasicModuleType[]
            {
                EngineModule,
                SonarModule,
                TowedArrayModule
            }
            .Concat(TurretModules)
            .Concat(LauncherModules)
            .Concat(HangarModules)
            .Concat(OtherModules)
            .Where(m => m != null)
            .ToArray();
    }

    public float GetCombatThreat()
    {
        return (HullType.Health * ArmorType.Armor) / 100
            + EngineModule.CombatThreat
            + GetAllModules().Sum(x => x.CombatThreat);
    }
}