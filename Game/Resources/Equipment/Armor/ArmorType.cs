﻿using Godot;

public class ArmorType : Resource
{
    [Export]
    public string ArmorTypeId;

    [Export]
    public string Name;

    [Export(PropertyHint.MultilineText)]
    public string Description;

    [Export]
    public float VolumePerSurfaceArea;

    [Export]
    public float DragFactor;

    [Export]
    public float Armor;

    [Export]
    public float ValuePerSurfaceArea;
    [Export]
    public float IndustrialNeed;
    [Export]
    public float MilitaryNeed;
    [Export]
    public Faction[] Operators;
}
