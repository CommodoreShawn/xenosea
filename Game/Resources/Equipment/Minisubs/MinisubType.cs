using Godot;

public class MinisubType : SubLoadout
{
    [Export]
    public Texture Icon;

    [Export]
    public string Name;

    [Export(PropertyHint.MultilineText)]
    public string Description;

    [Export]
    public float Value;

    [Export]
    public PackedScene DockedModelPrefab;

    [Export]
    public float MaxCharge;

    [Export]
    public PackedScene ControllerPrefab;
    [Export]
    public float IndustrialNeed;
    [Export]
    public float MilitaryNeed;
    [Export]
    public Faction[] Operators;
    [Export]
    public float ReputationRequired;
}
