﻿using Godot;
using System;
using System.Collections.Generic;
using Xenosea.Logic.Infrastructure.Data;

public class Faction : Resource
{
    [Export]
    public string Name;

    [Export(PropertyHint.MultilineText)]
    public string Description;

    [Export]
    public Color Color;

    [Export]
    public bool TracksCrimes;

    [Export]
    public CommodityType[] Contraband;

    [Export]
    public CommodityType[] SmugglingMissionCommodities;

    [Export]
    public float MinimumFactionStandingForTrade;

    [Export]
    public Faction TradeSubfaction;

    [Export]
    public Faction SecuritySubfaction;

    public Dictionary<string, double> FactionRelations { get; }

    public List<string> OfferedBounties { get; }

    public Faction()
    {
        FactionRelations = new Dictionary<string, double>();
        OfferedBounties = new List<string>();
    }

    public bool IsHostileTo(Faction faction)
    {
        if(FactionRelations.ContainsKey(faction.Name))
        {
            return FactionRelations[faction.Name] <= -20;
        }

        return false;
    }

    public bool IsFriendlyTo(Faction faction)
    {
        if(faction == this)
        {
            return true;
        }

        if (FactionRelations.ContainsKey(faction.Name))
        {
            return FactionRelations[faction.Name] >= 20;
        }

        return false;
    }

    public double GetStandingTo(Faction faction)
    {
        if (faction == this)
        {
            return 100;
        }

        if (FactionRelations.ContainsKey(faction.Name))
        {
            return FactionRelations[faction.Name];
        }

        return 0;
    }

    public bool OffersBountyAgainst(Faction faction)
    {
        return OfferedBounties.Contains(faction.Name);
    }

    public void SaveState(SerializationObject factionRoot)
    {
        foreach(var factionName in FactionRelations.Keys)
        {
            factionRoot.AddChild("relation")
                .SetAttribute("name", factionName)
                .SetAttribute("relation", FactionRelations[factionName].ToString());
        }

        foreach (var factionName in OfferedBounties)
        {
            factionRoot.AddChild("bounty_on")
                .SetAttribute("name", factionName);
        }
    }

    public void LoadState(SerializationObject factionRoot)
    {
        FactionRelations.Clear();
        OfferedBounties.Clear();

        foreach(var child in factionRoot.Children)
        {
            if(child.Name == "relation")
            {
                FactionRelations.Add(child.GetAttribute("name"), Convert.ToDouble(child.GetAttribute("relation")));
            }
            else if(child.Name == "bounty_on")
            {
                OfferedBounties.Add(child.GetAttribute("name"));
            }
        }
    }
}
