﻿using Godot;
using Xenosea.Resources;

public class FixedPointOfInterest : Resource
{
    [Export]
    public float Latitude;
    [Export]
    public float Longitude;
    [Export]
    public string Name;
    [Export]
    public ContactType ContactType;
}
