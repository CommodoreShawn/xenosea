using Godot;

public class AgentDefinition : Resource
{
    [Export]
    public string Name;
    [Export]
    public bool IsTrader;
    [Export]
    public bool IsSecurity;
    [Export]
    public float MovementSpeed;
    [Export]
    public float SupportCost;
    [Export]
    public SimulationAgentBehavior AgentBehavior;
    [Export]
    public Texture MapIcon;
    [Export]
    public SubLoadout SubLoadout;
    [Export]
    public PackedScene IndicatorPrefab;
    [Export]
    public PackedScene ControllerPrefab;
    [Export]
    public PackedScene MissionEnemyControllerPrefab;
    [Export]
    public bool CanBePatrolEnemy;
    [Export]
    public float Agility;
    [Export]
    public float Evasion;
    [Export]
    public float FocusedDamage;
    [Export]
    public float WideDamage;
    [Export]
    public float Armor;
    [Export]
    public float Stealth;
    [Export]
    public string AgentNameRole;

    public float RebuildCost => SupportCost * 50;
}
