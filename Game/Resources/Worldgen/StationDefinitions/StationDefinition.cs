using Godot;
using Xenosea.Resources;

public class StationDefinition : Resource
{
    public string ReferenceId => Name;
    [Export]
    public string Name;
    [Export]
    public Faction Faction;
    [Export]
    public float Latitude;
    [Export]
    public float Longitude;
    [Export]
    public int CommodityCapacity = 50000;
    [Export]
    public Faction[] FactionsPresent;
    [Export]
    public float TradeDistance = 125000;
    [Export]
    public AbstractEconomicProcess[] EconomicProcesses = new AbstractEconomicProcess[0];
    [Export]
    public AgentDefinition[] SupportedAgents;
    [Export]
    public StationModuleSet ModuleSet;
    [Export]
    public StationShapeProfile StationShape;
    [Export]
    public float TradeSourceLatitude;
    [Export]
    public float TradeSourceLongitude;
    [Export]
    public float AttackSourceLatitude;
    [Export]
    public float AttackSourceLongitude;
    [Export]
    public int DefenseTowerCount;
    [Export]
    public string BarName;
    [Export]
    public string EquipmentShopName;
    [Export]
    public string WarehouseName;
    [Export]
    public string SecurityOfficeName;
    [Export]
    public string BountyOfficeName;
}
