﻿using Godot;

public class WorldDefinition : Resource
{
    [Export]
    public StationDefinition[] Stations;
    [Export]
    public float PlayerStartLatitude;
    [Export]
    public float PlayerStartLongitude;
    [Export]
    public Faction PlayerFaction;
    [Export]
    public SubLoadout StarterSub;
    [Export]
    public double StartingMoney;
    [Export]
    public string[] StartingTopics;
    [Export]
    public FixedPointOfInterest[] StartingKnownPoi;
    [Export]
    public WreckDefinition[] PersistentWrecks;
}
