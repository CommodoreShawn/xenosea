using Godot;

public class CaveSpecification : Resource
{
    [Export]
    public PackedScene CavePrefab;
    [Export]
    public Vector3[] TowerPositions;
}
