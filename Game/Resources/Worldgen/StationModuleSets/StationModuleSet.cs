using Godot;

public class StationModuleSet : Resource
{
    [Export]
    public string Name;
    [Export]
    public PackedScene CapPrefab;
    [Export]
    public PackedScene BaseSectionPrefab;
    [Export]
    public PackedScene IndustrialSectionPrefab;
    [Export]
    public PackedScene CivilianSectionPrefab;
    [Export]
    public PackedScene DefenseSectionPrefab;
    [Export]
    public float SectionHeight;
    [Export]
    public float SectionRadius;
    [Export]
    public float GroundSlopeTolerance;
    [Export]
    public float GridSpacing;
    [Export]
    public StationLayoutType LayoutType;
    [Export]
    public PackedScene BridgePrefab;
    [Export]
    public float BridgeLength;
    [Export]
    public PackedScene DockingTowerPrefab;
    [Export]
    public PackedScene DefenseTowerPrefab;
    [Export]
    public float DefenseTowerDistance;
    [Export]
    public float FarmPlotSize;
    [Export]
    public float PlantSpacing;
    [Export]
    public PackedScene FarmPlotPrefab;
    [Export]
    public MinisubHangarModuleType MinisubHangarType;
    [Export]
    public MinisubType MinisubType;
    [Export]
    public CaveSpecification CaveLoadout;
}
