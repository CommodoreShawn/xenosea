using Godot;

public class WreckDefinition : Resource
{
    [Export]
    public float Latitude;
    [Export]
    public float Longitude;
    [Export]
    public string Name;
    [Export]
    public PackedScene WreckPrefab;
}
