﻿using Godot;
using System;
using System.Linq;
using Xenosea.Logic.AI;

public class DepartOnMissionGoalType : BotGoalType
{
    [Export]
    public bool Lawful;

    public static readonly int PRIORITY = 3;

    public override int Priority => PRIORITY;

    private Random _random = new Random();

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to DepartOnMissionGoalType");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        var paths = submarine.LocalHub.AiPaths
            .Where(p => p.Lawful == Lawful)
            .ToArray();

        if (paths.Any())
        {
            var path = paths[_random.Next(paths.Length)];

            return new FollowPathGoal(path, PRIORITY);
        }

        return null;
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.IsDockedTo != null
            && submarine.LocalHub.AiPaths
                .Where(p => p.Lawful == Lawful)
                .Any();
    }
}
