﻿using Godot;
using Xenosea.Logic.AI;

public abstract class BotGoalType : Resource
{
    public abstract int Priority { get; }

    public abstract bool IsValid(IStation station);

    public abstract bool IsValid(Submarine submarine);

    public abstract IBotGoal CreateGoal(IStation station);

    public abstract IBotGoal CreateGoal(Submarine submarine);
}
