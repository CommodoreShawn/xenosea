﻿using System;
using System.Linq;
using Xenosea.Logic.AI;
using Xenosea.Logic.SimulationEvents;
using Xenosea.Resources;

public class ScanForContrabandGoalType : BotGoalType
{
    public static readonly int PRIORITY = 6;

    public override int Priority => PRIORITY;

    private readonly Random _random = new Random();

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException();
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        var nearContact = submarine.SonarContactRecords.Values
            .Where(cr => cr.IsDetected && cr.Identified && cr.SonarContact is Submarine)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
            .Where(cr => cr.SonarContact.Faction != submarine.Faction)
            .Where(cr => !cr.AiData.ContainsKey("cargo_scanned"))
            .OrderBy(cr => (cr.EstimatedPosition - submarine.RealPosition).LengthSquared())
            .FirstOrDefault();

        if (nearContact != null)
        {
            if (_random.NextDouble() < 0.5)
            {
                return new ScanForContrabandGoal(nearContact);
            }
            else
            {
                nearContact.AiData.Add("cargo_scanned", string.Empty);
            }
        }

        return null;
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.SonarContactRecords.Values
            .Where(cr => cr.IsDetected && cr.Identified && cr.SonarContact is Submarine)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
            .Where(cr => cr.SonarContact.Faction != submarine.Faction)
            .Where(cr => !cr.AiData.ContainsKey("cargo_scanned"))
            .OrderBy(cr => (cr.EstimatedPosition - submarine.RealPosition).LengthSquared())
            .Any();
    }
}
