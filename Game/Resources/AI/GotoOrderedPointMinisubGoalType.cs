﻿using Godot;
using System;
using System.Linq;
using Xenosea.Logic.AI;
using Xenosea.Logic.Detection;
using Xenosea.Resources;

public class GotoOrderedPointMinisubGoalType : BotGoalType
{
    public static readonly int PRIORITY = 11;

    public override int Priority => PRIORITY;

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to GotoOrderedPointMinisubGoalType");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        var controller = submarine.GetNodeOrNull<MinisubController>("MinisubController");

        if (controller != null && controller.Home.GotoPoint != null)
        {
            return new GotoPointMinisubGoal(controller.Home.GotoPoint.Value, Priority);
        }

        return null;
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        var controller = submarine.GetNodeOrNull<MinisubController>("MinisubController");

        return controller?.Home?.GotoPoint != null;
    }
}
