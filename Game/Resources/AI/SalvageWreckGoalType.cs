﻿using System;
using System.Linq;
using Xenosea.Logic.AI;

public class SalvageWreckGoalType : BotGoalType
{
    public static readonly int PRIORITY = 7;

    public override int Priority => PRIORITY;

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException();
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        var target = submarine.SonarContactRecords.Values
            .Where(scr => scr.IsDetected && scr.ContactType == Xenosea.Resources.ContactType.Wreck)
            .Where(scr => scr.SonarContact is Submarine submarine1 && submarine1.CommodityStorage.TotalVolume > 0)
            .Where(scr => submarine.DiveTeams.Any(dt => dt.IsValidTarget(scr)))
            .OrderBy(cr => (cr.EstimatedPosition - submarine.RealPosition).LengthSquared())
            .FirstOrDefault();

        return new SalvageWreckGoal(target);
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.SonarContactRecords.Values
            .Where(scr => scr.IsDetected && scr.ContactType == Xenosea.Resources.ContactType.Wreck)
            .Where(scr => scr.SonarContact is Submarine submarine1 && submarine1.CommodityStorage.TotalVolume > 0)
            .Where(scr => submarine.DiveTeams.Any(dt => dt.IsValidTarget(scr)))
            .Any();
    }
}
