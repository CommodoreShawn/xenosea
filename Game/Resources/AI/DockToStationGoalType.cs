﻿using Godot;
using System;
using System.Linq;
using Xenosea.Logic.AI;

public class DockToStationGoalType : BotGoalType
{
    public static readonly int PRIORITY = 2;

    public override int Priority => PRIORITY;

    private Random _random = new Random();

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to DockToStationGoalType");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        var stations = submarine.LocalHub.LocalStations
            .Where(s => !s.Faction.IsHostileTo(submarine.Faction) && s.IsDockable)
            .Distinct()
            .ToArray();

        if(stations.Any())
        {
            var station = stations[_random.Next(stations.Length)];

            return new DockToStationGoal(station);
        }

        return null;
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.LocalHub.LocalStations
            .Where(s => !s.Faction.IsHostileTo(submarine.Faction) && s.IsDockable)
            .Any();
    }
}
