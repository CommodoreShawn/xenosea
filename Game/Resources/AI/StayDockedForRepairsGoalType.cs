﻿using System;
using Xenosea.Logic.AI;

public class StayDockedForRepairsGoalType : BotGoalType
{
    public static readonly int PRIORITY = 10;

    public override int Priority => PRIORITY;

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to StayDockedForRepairsGoalType");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        return new StayDockedForRepairsGoal();
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.IsDockedTo != null && submarine.Health < submarine.MaximumHealth;
    }
}
