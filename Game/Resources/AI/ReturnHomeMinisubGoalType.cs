﻿using System;
using Xenosea.Logic.AI;


public class ReturnHomeMinisubGoalType : BotGoalType
{
    public static readonly int PRIORITY = 15;

    public override int Priority => PRIORITY;

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to ReturnHomeMinisubGoalType");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        return new ReturnHomeMinisubGoal();
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        var controller = submarine.GetNodeOrNull<MinisubController>("MinisubController");

        if(!IsInstanceValid(controller.Home))
        {
            return false;
        }

        return (controller != null && controller.Home.ShouldRecall) 
            || (submarine.UsesCharge && submarine.Charge <= 0.25 * submarine.MaximumCharge);
    }
}
