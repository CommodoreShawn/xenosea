﻿using System;
using Xenosea.Logic.AI;

public class OrbitBaseGoalType : BotGoalType
{
    public static readonly int PRIORITY = 0;

    public override int Priority => PRIORITY;

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to OrbitBaseGoalType");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        return new OrbitBaseGoal();
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        return true;
    }
}
