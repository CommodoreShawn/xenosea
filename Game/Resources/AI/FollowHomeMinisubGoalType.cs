﻿using System;
using Xenosea.Logic.AI;


public class FollowHomeMinisubGoalType : BotGoalType
{
    public static readonly int PRIORITY = 0;

    public override int Priority => PRIORITY;

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to FollowHomeMinisubGoalType");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        return new FollowHomeMinisubGoal();
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        var controller = submarine.GetNodeOrNull<MinisubController>("MinisubController");

        return controller != null && IsInstanceValid(controller.Home);
    }
}
