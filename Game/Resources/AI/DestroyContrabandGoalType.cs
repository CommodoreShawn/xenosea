﻿using System;
using System.Linq;
using Xenosea.Logic.AI;

public class DestroyContrabandGoalType : BotGoalType
{
    public static readonly int PRIORITY = 7;

    public override int Priority => PRIORITY;

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException();
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        var target = submarine.SonarContactRecords.Values
            .Where(scr => scr.IsDetected && scr.ContactType == Xenosea.Resources.ContactType.Wreck)
            .Where(scr => scr.SonarContact is CargoPod lootPod && submarine.Faction.Contraband.Any(x => lootPod.CommodityStorage.GetQty(x) > 0))
            .OrderBy(cr => (cr.EstimatedPosition - submarine.RealPosition).LengthSquared())
            .FirstOrDefault();

        return new DestroyContrabandGoal(target);
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.SonarContactRecords.Values
            .Where(scr => scr.IsDetected && scr.ContactType == Xenosea.Resources.ContactType.Wreck)
            .Where(scr => scr.SonarContact is CargoPod lootPod && submarine.Faction.Contraband.Any(x => lootPod.CommodityStorage.GetQty(x) > 0))
            .OrderBy(cr => (cr.EstimatedPosition - submarine.RealPosition).LengthSquared())
            .Any();
    }
}
