﻿using System;
using System.Linq;
using Xenosea.Logic.AI;
using Xenosea.Resources;
using Godot;


public class UndockMinisubGoalType : BotGoalType
{
    public static readonly int PRIORITY = 100;

    public override int Priority => PRIORITY;

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to UndockMinisubGoalType");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        return new UndockMinisubGoal();
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        var controller = submarine.GetNodeOrNull<MinisubController>("MinisubController");

        return controller != null && controller.IsUndocking;
    }
}
