﻿using Godot;
using System;
using System.Linq;
using Xenosea.Logic.AI;
using Xenosea.Logic.Detection;
using Xenosea.Resources;

public class ShootOrderedTargetMinisubGoalType : BotGoalType
{
    public static readonly int PRIORITY = 12;

    public override int Priority => PRIORITY;

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to ShootOrderedTargetMinisubGoalType");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        var controller = submarine.GetNodeOrNull<MinisubController>("MinisubController");

        if (controller != null && controller.Home.Target is SonarContactRecord targetRecord)
        {
            var nearContact = submarine.SonarContactRecords.Values
                .Where(cr => cr.SonarContact == targetRecord.SonarContact)
                .Where(cr => cr.Identified && cr.IsDetected)
                .Where(cr => submarine.IsHostileTo(cr.SonarContact) 
                    || cr.SonarContact.IsHostileTo(controller.Home.Combatant) 
                    || controller.Home.ForceShootTarget)
                .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
                .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
                .FirstOrDefault();

            if (nearContact != null)
            {
                return new ShootHostileContactGoal(nearContact, Priority, true);
            }
        }

        return null;
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        var controller = submarine.GetNodeOrNull<MinisubController>("MinisubController");

        if (controller != null && controller.Home.Target is SonarContactRecord targetRecord)
        {
            return submarine.SonarContactRecords.Values
                .Where(cr => cr.SonarContact == targetRecord.SonarContact)
                .Where(cr => cr.Identified && cr.IsDetected)
                .Where(cr => submarine.IsHostileTo(cr.SonarContact) || cr.SonarContact.IsHostileTo(controller.Home.Combatant) || controller.Home.ForceShootTarget)
                .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
                .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
                .Any();
        }

        return false;
    }
}
