﻿using System;
using System.Linq;
using Xenosea.Logic.AI;
using Xenosea.Resources;

public class ScanForPiracyGoalType : BotGoalType
{
    public static readonly int PRIORITY = 6;

    public override int Priority => PRIORITY;

    private readonly Random _random = new Random();

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException();
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        var nearContact = submarine.SonarContactRecords.Values
            .Where(cr => cr.IsDetected && cr.Identified && cr.SonarContact is Submarine)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
            .Where(cr => cr.SonarContact.Faction != submarine.Faction)
            .Where(cr => !cr.AiData.ContainsKey("piracy_checked"))
            .OrderBy(cr => (cr.EstimatedPosition - submarine.RealPosition).LengthSquared())
            .FirstOrDefault();

        if (nearContact != null)
        {
            var relation = 0.0;
            if(submarine.Faction.FactionRelations.ContainsKey(nearContact.SonarContact.Faction.Name))
            {
                relation = submarine.Faction.FactionRelations[nearContact.SonarContact.Faction.Name];
            }

            if (Math.Max(relation, 0.4) < _random.NextDouble())
            {
                return new ScanForPiracyGoal(nearContact);
            }
            else
            {
                nearContact.AiData.Add("piracy_checked", string.Empty);
            }
        }

        return null;
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.SonarContactRecords.Values
            .Where(cr => cr.IsDetected && cr.Identified && cr.SonarContact is Submarine)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
            .Where(cr => cr.SonarContact.Faction != submarine.Faction)
            .Where(cr => !cr.AiData.ContainsKey("piracy_checked"))
            .OrderBy(cr => (cr.EstimatedPosition - submarine.RealPosition).LengthSquared())
            .Any();
    }
}
