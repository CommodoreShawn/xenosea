﻿using Godot;
using System;
using System.Linq;
using Xenosea.Logic.AI;

public class TravelAndDockGoalType : BotGoalType
{
    public static readonly int PRIORITY = 1;

    public override int Priority => PRIORITY;

    private Random _random = new Random();

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to TravelAndDockGoalType");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        var stations = submarine.LocalHub.AiPaths
            .Where(p => p.TradeRoute)
            .SelectMany(p => p.DestinationHub.LocalStations)
            .Where(s => !s.Faction.IsHostileTo(submarine.Faction) && s.IsDockable)
            .Distinct()
            .ToArray();

        if(stations.Any())
        {
            var station = stations[_random.Next(stations.Length)];

            return new TravelAndDockGoal(station);
        }

        return null;
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.LocalHub.AiPaths
            .Where(p => p.TradeRoute)
            .SelectMany(p => p.DestinationHub.LocalStations)
            .Where(s => !s.Faction.IsHostileTo(submarine.Faction) && s.IsDockable)
            .Any();
    }
}
