﻿using System.Linq;
using Xenosea.Logic.AI;
using Xenosea.Resources;

public class InvestigateContactGoalType : BotGoalType
{
    public static readonly int PRIORITY = 5;

    public override int Priority => PRIORITY;

    public override IBotGoal CreateGoal(IStation station)
    {
        var nearContact = station.SonarContactRecords
            .Where(cr => cr.IsDetected && !cr.Identified && cr.SonarContact is Submarine)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
            .OrderBy(cr => (cr.EstimatedPosition - station.RealPosition).LengthSquared())
            .FirstOrDefault();

        return new InvestigateContactGoal(nearContact, Priority, false);
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        var nearContact = submarine.SonarContactRecords.Values
            .Where(cr => cr.IsDetected && !cr.Identified && cr.SonarContact is Submarine)
            .Where(cr => cr.TimeDetected > 10)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
            .Where(cr => cr.SonarContact.Faction != submarine.Faction)
            .OrderBy(cr => (cr.EstimatedPosition - submarine.RealPosition).LengthSquared())
            .FirstOrDefault();

        return new InvestigateContactGoal(nearContact, Priority, false);
    }

    public override bool IsValid(IStation station)
    {
        return station.SonarContactRecords
            .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
            .Where(cr => cr.IsDetected && !cr.Identified && cr.SonarContact is Submarine)
            .Any();
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.SonarContactRecords.Values
            .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
            .Where(cr => cr.SonarContact.Faction != submarine.Faction)
            .Where(cr => cr.IsDetected && !cr.Identified && cr.SonarContact is Submarine)
            .Where(cr => cr.TimeDetected > 10)
            .Any();
    }
}
