﻿using Godot;
using System;
using System.Linq;
using Xenosea.Logic.AI;
using Xenosea.Logic.Detection;
using Xenosea.Resources;

public class InvestigateOrderedTargetMinisubGoalType : BotGoalType
{
    public static readonly int PRIORITY = 13;

    public override int Priority => PRIORITY;

    [Export]
    public bool IgnoreStructures;

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to InvestigateOrderedTargetMinisubGoalType");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        var controller = submarine.GetNodeOrNull<MinisubController>("MinisubController");

        if (controller != null && controller.Home.Target is SonarContactRecord targetRecord)
        {
            var nearContact = submarine.SonarContactRecords.Values
                .Where(cr => cr.SonarContact == targetRecord.SonarContact)
                .Where(cr => cr.IsDetected && !cr.Identified)
                .FirstOrDefault();

            if (nearContact != null)
            {
                return new InvestigateContactGoal(nearContact, Priority, true);
            }
        }

        return null;
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        var controller = submarine.GetNodeOrNull<MinisubController>("MinisubController");

        if (controller != null && controller.Home.Target is SonarContactRecord targetRecord)
        {
            return submarine.SonarContactRecords.Values
                .Where(cr => cr.SonarContact == targetRecord.SonarContact)
                .Where(cr => cr.IsDetected && !cr.Identified)
                .Any();
        }

        return false;
    }
}
