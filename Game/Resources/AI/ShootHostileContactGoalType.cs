﻿using Godot;
using System.Linq;
using Xenosea.Logic.AI;
using Xenosea.Resources;

public class ShootHostileContactGoalType : BotGoalType
{
    public override int Priority => GoalPriority;

    [Export]
    public int GoalPriority;
    [Export]
    public bool IgnoreStructures;
    [Export]
    public bool IgnoreMinisubs;


    public override IBotGoal CreateGoal(IStation station)
    {
        var nearContact = station.SonarContactRecords
            .Where(cr => cr.Identified && station.IsHostileTo(cr.SonarContact) && cr.IsDetected)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
            .Where(cr => !IgnoreStructures || (cr.SonarContact.ContactType != ContactType.Station && cr.SonarContact.ContactType != ContactType.Outpost))
            .Where(cr => !IgnoreMinisubs || (cr.SonarContact.ContactType != ContactType.Minisub))
            .OrderBy(cr => (cr.EstimatedPosition - station.RealPosition).LengthSquared())
            .FirstOrDefault();

        return new ShootHostileContactGoal(nearContact, Priority, false);
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        var nearContact = submarine.SonarContactRecords.Values
            .Where(cr => cr.Identified && submarine.IsHostileTo(cr.SonarContact) && cr.IsDetected)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
            .Where(cr => !IgnoreStructures || (cr.SonarContact.ContactType != ContactType.Station && cr.SonarContact.ContactType != ContactType.Outpost))
            .Where(cr => !IgnoreMinisubs || (cr.SonarContact.ContactType != ContactType.Minisub))
            .OrderBy(cr => (cr.EstimatedPosition - submarine.RealPosition).LengthSquared())
            .FirstOrDefault();

        return new ShootHostileContactGoal(nearContact, Priority, false);
    }

    public override bool IsValid(IStation station)
    {
        return station.SonarContactRecords
            .Where(cr => cr.Identified && station.IsHostileTo(cr.SonarContact) && cr.IsDetected)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
            .Where(cr => !IgnoreStructures || (cr.SonarContact.ContactType != ContactType.Station && cr.SonarContact.ContactType != ContactType.Outpost))
            .Where(cr => !IgnoreMinisubs || (cr.SonarContact.ContactType != ContactType.Minisub))
            .Any();
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.SonarContactRecords.Values
            .Where(cr => cr.Identified && submarine.IsHostileTo(cr.SonarContact) && cr.IsDetected)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
            .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
            .Where(cr => !IgnoreStructures || (cr.SonarContact.ContactType != ContactType.Station && cr.SonarContact.ContactType != ContactType.Outpost))
            .Where(cr => !IgnoreMinisubs || (cr.SonarContact.ContactType != ContactType.Minisub))
            .Any();
    }
}
