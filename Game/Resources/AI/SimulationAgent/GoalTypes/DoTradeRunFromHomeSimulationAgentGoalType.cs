﻿using Autofac;
using Godot;
using Xenosea.Logic.AI.SimulationGoals;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

public class DoTradeRunFromHomeSimulationAgentGoalType : SimulationAgentGoalType
{
    public override string Name => "DoTradeRunFromHomeSimulationAgentGoalType";
    public override int Priority => 15;

    private IMissionTracker _missionTracker;
    private IWorldManager _worldManager;

    public override ISimulationBotGoal CreateGoal(SimulationAgent agent)
    {
        if(_missionTracker == null)
        {
            _missionTracker = IocManager.IocContainer.Resolve<IMissionTracker>();
            _worldManager = IocManager.IocContainer.Resolve<IWorldManager>();
        }

        return new DoTradeRunSimulationAgentGoal(Priority, _missionTracker, _worldManager, agent, false);
    }

    public override bool IsValid(SimulationAgent agent)
    {
        return agent.Location.SimulationStation == agent.HomeStation
            && !agent.Location.SimulationStation.Faction.IsHostileTo(agent.Faction)
            && !agent.Faction.IsHostileTo(agent.Location.SimulationStation.Faction);
    }

    public override bool CanLoad(SerializationObject goalRoot)
    {
        return goalRoot.GetAttribute("goal_type") == "DoTradeRunSimulationAgentGoal";
    }

    public override ISimulationBotGoal LoadGoal(
        IWorldManager worldManager,
        IResourceLocator resouceLocator,
        SerializationObject goalRoot,
        SimulationAgent agent)
    {
        if (_missionTracker == null)
        {
            _missionTracker = IocManager.IocContainer.Resolve<IMissionTracker>();
            _worldManager = IocManager.IocContainer.Resolve<IWorldManager>();
        }

        return DoTradeRunSimulationAgentGoal.LoadState(worldManager, _missionTracker, goalRoot, agent);
    }
}
