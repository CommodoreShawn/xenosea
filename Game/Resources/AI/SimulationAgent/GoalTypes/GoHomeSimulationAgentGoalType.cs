﻿using Xenosea.Logic.AI.SimulationGoals;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

public class GoHomeSimulationAgentGoalType : SimulationAgentGoalType
{
    public override string Name => "GoHomeSimulationAgentGoalType";
    public override int Priority => 5;

    public override ISimulationBotGoal CreateGoal(SimulationAgent agent)
    {
        return new GoHomeSimulationAgentGoal(Priority, agent);
    }

    public override bool IsValid(SimulationAgent agent)
    {
        return agent.Location.SimulationStation != agent.HomeStation;
    }

    public override bool CanLoad(SerializationObject goalRoot)
    {
        return goalRoot.GetAttribute("goal_type") == "GoHomeSimulationAgentGoal";
    }

    public override ISimulationBotGoal LoadGoal(
        IWorldManager worldManager,
        IResourceLocator resouceLocator,
        SerializationObject goalRoot,
        SimulationAgent agent)
    {
        return GoHomeSimulationAgentGoal.LoadState(worldManager, goalRoot, agent);
    }
}
