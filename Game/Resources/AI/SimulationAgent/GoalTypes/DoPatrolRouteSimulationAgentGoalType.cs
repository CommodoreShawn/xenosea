﻿using Autofac;
using Xenosea.Logic.AI.SimulationGoals;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

public class DoPatrolRouteSimulationAgentGoalType : SimulationAgentGoalType
{
    public override string Name => "DoPatrolRouteSimulationAgentGoalType";
    public override int Priority => 10;

    public override ISimulationBotGoal CreateGoal(SimulationAgent agent)
    {
        var simulationBattleHandler = IocManager.IocContainer.Resolve<ISimulationBattleHandler>();
        var worldManager = IocManager.IocContainer.Resolve<IWorldManager>();

        return new DoPatrolRouteSimulationAgentGoal(Priority, agent, agent.Location.SimulationStation, simulationBattleHandler, worldManager);
    }

    public override bool IsValid(SimulationAgent agent)
    {
        return agent.Location.SimulationStation == agent.HomeStation;
    }

    public override bool CanLoad(SerializationObject goalRoot)
    {
        return goalRoot.GetAttribute("goal_type") == "DoPatrolRouteSimulationAgentGoal";
    }

    public override ISimulationBotGoal LoadGoal(
        IWorldManager worldManager, 
        IResourceLocator  resouceLocator, 
        SerializationObject goalRoot, 
        SimulationAgent agent)
    {
        var simulationBattleHandler = IocManager.IocContainer.Resolve<ISimulationBattleHandler>();

        return DoPatrolRouteSimulationAgentGoal.LoadState(worldManager, simulationBattleHandler, goalRoot, agent);
    }
}
