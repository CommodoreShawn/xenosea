﻿using Godot;
using Xenosea.Logic.AI.SimulationGoals;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

public abstract class SimulationAgentGoalType : Resource
{
    public abstract string Name { get; }

    public abstract int Priority { get; }

    public abstract bool IsValid(SimulationAgent agent);

    public abstract ISimulationBotGoal CreateGoal(SimulationAgent agent);

    public abstract bool CanLoad(SerializationObject goalRoot);

    public abstract ISimulationBotGoal LoadGoal(
        IWorldManager worldManager,
        IResourceLocator resouceLocator, 
        SerializationObject goalRoot, 
        SimulationAgent agent);
}