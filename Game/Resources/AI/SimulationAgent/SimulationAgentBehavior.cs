﻿using Godot;
using System.Linq;
using Xenosea.Logic.AI.SimulationGoals;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

public class SimulationAgentBehavior : Resource
{
    [Export]
    public SimulationAgentGoalType[] GoalTypes;

    public ISimulationBotGoal LoadGoal(
        IWorldManager worldManager, 
        IResourceLocator resourceLocator,
        SerializationObject goalRoot, 
        SimulationAgent agent)
    {
        var validGoalType = GoalTypes
            .Where(x => x.CanLoad(goalRoot))
            .FirstOrDefault();

        if(validGoalType != null)
        {
            return validGoalType.LoadGoal(worldManager, resourceLocator, goalRoot, agent);
        }

        return null;
    }
}
