﻿using Godot;
using System.Linq;
using Xenosea.Logic.AI;

public class GatherMinisubsGoalType : BotGoalType
{
    public static readonly int PRIORITY = 8;

    public override int Priority => PRIORITY;

    public override IBotGoal CreateGoal(IStation station)
    {
        return new GatherMinisubsGoal();
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        return new GatherMinisubsGoal();
    }

    public override bool IsValid(IStation station)
    {
        return station.MinisubHangars
            .Where(h => h.ActiveMinisub != null)
            .Any();
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.MinisubHangars
            .Where(h => h.ActiveMinisub != null)
            .Any();
    }
}
