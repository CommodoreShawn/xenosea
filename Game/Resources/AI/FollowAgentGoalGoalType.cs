﻿using Autofac;
using Godot;
using System;
using System.Linq;
using Xenosea.Logic.AI;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.Infrastructure.Missions;

public class FollowAgentGoalGoalType : BotGoalType
{
    public static readonly int PRIORITY = 3;

    public override int Priority => PRIORITY;

    public FollowAgentGoalGoalType()
    {
    }

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to FollowAgentGoalGoal");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        return new FollowAgentGoalGoal(Priority, submarine.AgentRepresentation.Goal);
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.AgentRepresentation != null;
    }
}
