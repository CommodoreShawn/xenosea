﻿using Godot;
using System;
using System.Linq;
using Xenosea.Logic.AI;

public class ReloadTorpedoesGoalType : BotGoalType
{
    public static readonly int PRIORITY = 9;

    public override int Priority => PRIORITY;

    [Export]
    public TorpedoType TypeToLoad;

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to ReloadTorpedoesGoalType");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        return new ReloadTorpedoesGoal(TypeToLoad);
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.IsDockedTo != null
            && submarine.TorpedoTubes.Any(t => t.TorpedoesAvailable.Count < t.MagazineCapacity);
    }
}
