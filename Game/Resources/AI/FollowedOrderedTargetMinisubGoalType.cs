﻿using Godot;
using System;
using System.Linq;
using Xenosea.Logic.AI;
using Xenosea.Logic.Detection;
using Xenosea.Resources;

public class FollowedOrderedTargetMinisubGoalType : BotGoalType
{
    public static readonly int PRIORITY = 5;

    public override int Priority => PRIORITY;

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to FollowedOrderedTargetMinisubGoalType");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        var controller = submarine.GetNodeOrNull<MinisubController>("MinisubController");

        if (controller != null && controller.Home.Target is SonarContactRecord targetRecord)
        {
            var nearContact = submarine.SonarContactRecords.Values
                .Where(cr => cr.SonarContact == targetRecord.SonarContact)
                .Where(cr => cr.Identified && cr.IsDetected)
                .Where(cr => !cr.IsHostileTo(submarine) && !cr.IsHostileTo(controller.Home.Combatant))
                .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
                .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
                .FirstOrDefault();

            if (nearContact != null)
            {
                return new FollowTargetMinisubGoal(nearContact, Priority);
            }
        }

        return null;
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        var controller = submarine.GetNodeOrNull<MinisubController>("MinisubController");

        if (controller != null && controller.Home.Target is SonarContactRecord targetRecord)
        {
            return submarine.SonarContactRecords.Values
                .Where(cr => cr.SonarContact == targetRecord.SonarContact)
                .Where(cr => cr.Identified && cr.IsDetected)
                .Where(cr => !cr.IsHostileTo(submarine) && !cr.IsHostileTo(controller.Home.Combatant))
                .Where(cr => cr.SonarContact.ContactType != ContactType.Wreck)
                .Where(cr => cr.SonarContact.ContactType != ContactType.Torpedo)
                .Any();
        }

        return false;
    }
}
