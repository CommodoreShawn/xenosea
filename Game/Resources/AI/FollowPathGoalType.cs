﻿using System;
using System.Linq;
using Xenosea.Logic.AI;

public class FollowPathGoalType : BotGoalType
{
    public static readonly int PRIORITY = 0;

    public override int Priority => PRIORITY;

    private Random _random = new Random();

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to FollowPath");

    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        var paths = submarine.LocalHub.AiPaths
            .Where(p => p.DestinationHub.Faction != submarine.Faction)
            .ToArray();

        if(paths.Length < 1)
        {
            paths = submarine.LocalHub.AiPaths.ToArray();
        }

        if(paths.Any())
        {
            var path = paths[_random.Next(paths.Length)];

            return new FollowPathGoal(path, PRIORITY);
        }

        return null;
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.LocalHub != null
            && submarine.LocalHub.AiPaths.Any();
    }
}
