﻿using Godot;
using System;
using System.Linq;
using Xenosea.Logic.AI;

public class SellLootGoalType : BotGoalType
{
    public static readonly int PRIORITY = 9;

    public override int Priority => PRIORITY;

    public override IBotGoal CreateGoal(IStation station)
    {
        throw new NotImplementedException("Cannot make a station goal to SellLootGoalType");
    }

    public override IBotGoal CreateGoal(Submarine submarine)
    {
        return new SellLootGoal();
    }

    public override bool IsValid(IStation station)
    {
        return false;
    }

    public override bool IsValid(Submarine submarine)
    {
        return submarine.IsDockedTo != null
            && submarine.CommodityStorage.TotalVolume > 0;
    }
}
