﻿namespace Xenosea.Logic
{
    public delegate void ReportProgress(double overallProgress, string stepName, double stepProgress);

    public delegate void ReportStepProgress(double stepProgress);
}
