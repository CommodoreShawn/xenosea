﻿using Godot;
using System.Collections.Generic;
using Xenosea.Logic.Detection;

namespace Xenosea.Logic
{
    public interface ICombatant : ISonarContact
    {
        Vector3 AimPoint { get; set; }

        bool IsWrecked { get; }

        float HealthPercent { get; }

        List<TemporaryNoise> TemporaryNoises { get; }

        void HitRecieved(ICombatant shooter, float damage, float penetration, Vector3 hitLocation, Vector3 velocity);
    }
}
