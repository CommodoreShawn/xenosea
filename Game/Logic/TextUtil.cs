﻿using System.Linq;

namespace Xenosea.Logic
{
    public static class TextUtil
    {
        public static string JoinWithAnd(string[] elements)
        {
            if(elements.Length == 0)
            {
                return string.Empty;
            }
            else if(elements.Length == 1)
            {
                return elements[0];
            }
            else if(elements.Length == 2)
            {
                return $"{elements[0]} and {elements[1]}";
            }
            else
            {
                return $"{string.Join(", ", elements.Take(elements.Length - 1))} and {elements[elements.Length - 1]}";
            }
        }
    }
}
