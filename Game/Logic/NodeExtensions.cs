﻿using Autofac;
using Godot;
using System;
using System.Linq;
using System.Reflection;
using Xenosea.Logic.Infrastructure;

namespace Xenosea.Logic
{
    public static class NodeExtensions
    {
        public static void ResolveDependencies(this Node node)
        {
            var at = typeof(InjectAttribute);
            var fields = node.GetType()
                .GetRuntimeFields()
                .Where(f => f.GetCustomAttributes(at, true).Any());

            foreach (var field in fields)
            {
                var obj = IocManager.IocContainer.Resolve(field.FieldType);

                try
                {
                    field.SetValue(node, obj);
                }
                catch (InvalidCastException)
                {
                    GD.PrintErr($"Error converting value {obj} ({obj.GetType()}) to {field.FieldType}");
                    throw;
                }
            }
        }

        public static T FindParent<T>(this Node child) where T : class
        {
            if(child == null)
            {
                return null;
            }

            if (child.GetParent() == null)
            {
                return null;
            }

            if (child.GetParent() is T)
            {
                return child.GetParent() as T;
            }

            return FindParent<T>(child.GetParent());
        }

        public static void ClearChildren(this Node node)
        {
            var children = node.GetChildren();
            foreach (var child in children)
            {
                (child as Node).QueueFree();
            }
        }
    }
}