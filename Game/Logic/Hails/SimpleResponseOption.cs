﻿using System;

namespace Xenosea.Logic.Hails
{
    public class SimpleResponseOption : IHailResponseOption
    {
        public string Text { get; set; }
        public Action PickedAction { get; set; }

        public void OnPicked()
        {
            PickedAction?.Invoke();
        }
    }
}
