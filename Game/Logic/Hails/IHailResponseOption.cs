﻿namespace Xenosea.Logic.Hails
{
    public interface IHailResponseOption
    {
        string Text { get; }
        void OnPicked();
    }
}