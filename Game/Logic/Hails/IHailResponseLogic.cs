﻿using System.Collections.Generic;

namespace Xenosea.Logic.Hails
{
    public interface IHailResponseLogic
    {
        string ConversationTitle { get; }
        string ConversationBody { get; }
        IEnumerable<IHailResponseOption> ResponseOptions { get; }
    }
}
