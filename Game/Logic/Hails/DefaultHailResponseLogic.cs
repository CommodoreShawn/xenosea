﻿using Autofac;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Hails
{
    public class DefaultHailResponseLogic : IHailResponseLogic
    {
        private GoalBotController _controller;
        private readonly IPlayerData _playerData;
        private readonly ICrimeTracker _crimeTracker;

        public string ConversationTitle => $"{_controller.Submarine.Faction.Name} {_controller.Submarine.FriendlyName}";
        public string ConversationBody { get; private set; }
        public IEnumerable<IHailResponseOption> ResponseOptions => _responseOptions;
        public List<IHailResponseOption> _responseOptions;

        public DefaultHailResponseLogic(GoalBotController controller)
        {
            _controller = controller;
            _playerData = IocManager.IocContainer.Resolve<IPlayerData>();
            _crimeTracker = IocManager.IocContainer.Resolve<ICrimeTracker>();

            if (_controller.Submarine.IsFriendlyTo(_playerData.PlayerSub))
            {
                ConversationBody = "Hello there, what can I do for you?";
            }
            else
            {
                ConversationBody = "Yes?";
            }
            _responseOptions = new List<IHailResponseOption>();

            BuildOptionsList();
        }

        private void BuildOptionsList()
        {
            _responseOptions.Clear();

            _responseOptions.Add(new SimpleResponseOption
            {
                Text = "What are you doing?",
                PickedAction = DoStatus
            });

            if(_controller.Submarine.CommodityStorage.TotalVolume > 0)
            {
                _responseOptions.Add(new SimpleResponseOption
                {
                    Text = "Drop your cargo, or else!",
                    PickedAction = DoPiracy
                });
            }
        }

        private void DoStatus()
        {
            ConversationBody = _controller.ActiveGoals
                .Select(g => g.DescribeGoalForHail(_controller))
                .Where(t => t != null)
                .FirstOrDefault() ?? "We're waiting for orders.";

            BuildOptionsList();
        }

        private void DoPiracy()
        {
            if(_controller.Submarine.GetTotalCombatThreat() < _playerData.PlayerSub.GetTotalCombatThreat())
            {
                ConversationBody = "Fine, but you wont get away with this!";
                _responseOptions.Clear();

                _crimeTracker.ReportCrime(
                    _playerData.PlayerSub,
                    _controller.Submarine,
                    _controller.Submarine.MaximumHealth);

                _crimeTracker.ReportPiracy(_playerData.PlayerSub, _controller.Submarine);

                foreach (var commodity in _controller.Submarine.CommodityStorage.CommoditiesPresent.ToArray())
                {
                    _controller.Submarine.JettisonCargo(commodity, _controller.Submarine.CommodityStorage.GetQty(commodity));
                }
            }
            else
            {
                ConversationBody = "Hah! You don't scare me.";
                _responseOptions.Clear();

                _crimeTracker.ReportCrime(
                    _playerData.PlayerSub,
                    _controller.Submarine,
                    _controller.Submarine.MaximumHealth / 4);
            }

            _responseOptions.Clear();
        }
    }
}
