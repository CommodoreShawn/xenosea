﻿namespace Xenosea.Logic
{
    public enum AutopilotType
    {
        None,
        Goto,
        Dock,
        Follow
    }
}
