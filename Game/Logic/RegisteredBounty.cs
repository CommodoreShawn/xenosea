﻿namespace Xenosea.Logic
{
    public class RegisteredBounty
    {
        public Faction OfferingFaction { get; set; }
        public Faction TargetFaction { get; set; }
        public string TargetName { get; set; }
        public double BountyValue { get; set; }
    }
}
