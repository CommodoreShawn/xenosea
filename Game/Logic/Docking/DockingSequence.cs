﻿using Godot;

namespace Xenosea.Logic.Docking
{
    public class DockingSequence
    {
        private const int STATE_INITIAL = 0;
        private const int STATE_TO_HIGH_APPROACH = 1;
        private const int STATE_HIGH_APPROACH_SLIDE = 2;
        private const int STATE_APPROACH_SLIDE = 3;
        private const int STATE_DOCK_SLIDE = 4;
        private const int STATE_FINAL_SLIDE = 5;
        private const int STATE_LOCKED = 6;
        private const int STATE_UNDOCKING_SLIDE = 7;
        private const int STATE_UNDOCKING_REVERSE = 8;
        private const int STATE_UNDOCKING_UPBACK = 9;
        private const int STATE_FINISHED = 10;

        public Submarine Submarine { get; set; }
        public IStation Station { get; private set; }
        public bool IsLocked => STATE_DOCK_SLIDE <= _state && _state <= STATE_UNDOCKING_REVERSE;
        public DockingPoint DockingPoint { get; set; }

        private int _state;

        private Vector3 _dockFacing;
        private Vector3 _dockPoint;
        private Vector3 _dockPointRest;
        private Vector3 _approachPoint;
        private Vector3 _highApproachPoint;

        public bool IsUndocking => _state > STATE_LOCKED;
        public bool IsFinished => _state == STATE_FINISHED;
        public bool IsDocked => _state == STATE_LOCKED;

        public DockingSequence(Submarine submarine, IStation station)
        {
            Submarine = submarine;
            Station = station;
            _state = STATE_INITIAL;
        }

        public void Cancel()
        {
            _state = STATE_FINISHED;
            Submarine.SlideForce = null;
            if (DockingPoint != null && DockingPoint.DockingSequence == this)
            {
                DockingPoint.DockingSequence = null;
            }

            Station.CancelDocking(this);
        }

        public void Update(AbstractSubController subController, float delta)
        {
            if(DockingPoint == null || !Object.IsInstanceValid(DockingPoint))
            {
                return;
            }

            //if (subController.Submarine.IsPlayerSub)
            //{
            //    GD.Print("Docking State: " + _state);

            //    GD.Print(">" + subController.Name);
            //}

            switch (_state)
            {
                case STATE_INITIAL:
                    CalculatePoints();
                    _state = STATE_TO_HIGH_APPROACH;
                    break;
                
                case STATE_TO_HIGH_APPROACH:
                    if (subController.Submarine.RealPosition.DistanceTo(_highApproachPoint) > 1500)
                    {
                        subController.CarefulAutopilotTo(_highApproachPoint, delta, 0.5f);
                    }
                    else
                    {
                        subController.CarefulAutopilotTo(_highApproachPoint, delta, 0.25f);
                    }
                    if (subController.Submarine.RealPosition.DistanceTo(_highApproachPoint) < 100)
                    {
                        _state = STATE_HIGH_APPROACH_SLIDE;
                    }
                    break;

                case STATE_HIGH_APPROACH_SLIDE:
                    
                    subController.SlideTo(
                        _highApproachPoint,
                        _dockFacing,
                        delta,
                        1);

                    var subFacing = Submarine.Transform.basis.Xform(Vector3.Back);

                    //if (Submarine.IsPlayerSub)
                    //{
                    //    Godot.GD.Print("Dist:  " + subController.Submarine.RealPosition.DistanceTo(_highApproachPoint));
                    //    Godot.GD.Print("Vel:   " + subController.Submarine.Velocity.Length());
                    //    Godot.GD.Print("Angle: " + subFacing.AngleTo(_dockFacing));
                    //}
                    
                    if (subController.Submarine.RealPosition.DistanceTo(_highApproachPoint) < 5
                        && subController.Submarine.Velocity.Length() < 5
                        && subFacing.AngleTo(_dockFacing) < 0.1)
                    {
                        _state = STATE_APPROACH_SLIDE;
                    }
                    break;

                case STATE_APPROACH_SLIDE:
                    subController.SlideTo(
                        _approachPoint,
                        _dockFacing,
                        delta,
                        2);

                    if (subController.Submarine.RealPosition.DistanceTo(_approachPoint) < 5)
                    {
                        _state = STATE_DOCK_SLIDE;
                    }
                    break;

                case STATE_DOCK_SLIDE:
                    subController.SlideTo(
                        _dockPoint,
                        _dockFacing,
                        delta,
                        1);

                    if (subController.Submarine.RealPosition.DistanceTo(_dockPoint) < 5)
                    {
                        _state = STATE_FINAL_SLIDE;
                    }
                    break;

                case STATE_FINAL_SLIDE:
                    subController.SlideTo(
                        _dockPointRest,
                        _dockFacing,
                        delta,
                        1);

                    if (subController.Submarine.RealPosition.DistanceTo(_dockPointRest) < 2)
                    {
                        if (Submarine.IsPlayerSub)
                        {
                            Station.Broadcast($"Welcome to {Station.FriendlyName}");
                        }

                        _state = STATE_LOCKED;
                    }
                    break;

                case STATE_LOCKED:
                    subController.Submarine.LookAtFromPosition(_dockPointRest, _dockPointRest - DockingPoint.GlobalTransform.basis.z, DockingPoint.GlobalTransform.basis.y);
                    subController.Submarine.Throttle = 0;
                    subController.Submarine.Dive = 0;
                    subController.Submarine.Rudder = 0;
                    subController.Submarine.IsDockedTo = Station;
                    Submarine.SlideForce = null;
                    break;

                case STATE_UNDOCKING_SLIDE:
                    subController.SlideTo(
                        _dockPoint,
                        DockingPoint.GlobalTransform.basis.Xform(Vector3.Back),
                        delta,
                        1);

                    //if (Submarine.IsPlayerSub)
                    //{
                    //    Godot.GD.Print("Dist:  " + subController.Submarine.RealPosition.DistanceTo(_dockPoint));
                    //    Godot.GD.Print("FromTo:   " + _dockPoint.DistanceTo(_dockPointRest));
                    //}

                    if (subController.Submarine.RealPosition.DistanceTo(_dockPoint) < 1)
                    {
                        _state = STATE_UNDOCKING_REVERSE;
                    }
                    break;

                case STATE_UNDOCKING_REVERSE:
                    subController.Submarine.Throttle = -0.25f;
                    subController.Submarine.Rudder = 0;
                    subController.Submarine.SlideForce = null;
                    subController.Submarine.Dive = 0;

                    if (subController.Submarine.RealPosition.DistanceTo(_dockPoint) > 60)
                    {
                        _state = STATE_UNDOCKING_UPBACK;
                    }
                    break;

                case STATE_UNDOCKING_UPBACK:
                    subController.Submarine.Throttle = -0.25f;
                    subController.Submarine.Rudder = 0;
                    subController.Submarine.SlideForce = null;
                    subController.Submarine.Dive = 1;

                    if (subController.Submarine.RealPosition.DistanceTo(_dockPoint) > 150)
                    {
                        if (Submarine.IsPlayerSub)
                        {
                            Station.Broadcast($"Safe travels, you are free to navigate.");
                        }
                        _state = STATE_FINISHED;
                        Cancel();
                    }
                    break;
            }
        }

        private void CalculatePoints()
        {
            var sideDelta = DockingPoint.LeftSideDocking ? 1 : -1;

            var dockDelta = sideDelta * DockingPoint.GlobalTransform.basis.x * Submarine.HintDockingWidth;

            var approachDelta = dockDelta * 1.5f;


            _highApproachPoint = DockingPoint.HighApproachPoint.GlobalTransform.origin + approachDelta;
            _approachPoint = DockingPoint.ApproachPoint.GlobalTransform.origin + approachDelta;
            _dockPoint = DockingPoint.GlobalTransform.origin + approachDelta;
            _dockPointRest = DockingPoint.GlobalTransform.origin + dockDelta;

            _dockFacing = DockingPoint.GlobalTransform.basis.Xform(Vector3.Back);
        }

        public void Undock()
        {
            _state = STATE_UNDOCKING_SLIDE;
            Submarine.IsDockedTo = null;
        }

        public void InstantDock(DockingPoint dockingPoint, AbstractSubController subController)
        {
            DockingPoint = dockingPoint;
            _state = STATE_LOCKED;
            CalculatePoints();
        }
    }
}
