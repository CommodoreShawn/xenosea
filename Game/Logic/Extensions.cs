﻿using System;
using System.Collections.Generic;

namespace Xenosea.Logic
{
    public static class Extensions
    {
        public static IEnumerable<T> Yield<T>(this T self)
        {
            yield return self;
        }

        // Adapted from http://stackoverflow.com/a/1262619
        public static void Shuffle<T>(this Random rnd, T[] list)
        {
            var n = list.Length;
            while (n > 1)
            {
                n--;
                var k = rnd.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
