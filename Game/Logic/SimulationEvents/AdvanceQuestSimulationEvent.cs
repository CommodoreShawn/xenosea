﻿namespace Xenosea.Logic.SimulationEvents
{
    public class AdvanceQuestSimulationEvent : ISimulationEvent
    {
        public string QuestId { get; private set; }

        public AdvanceQuestSimulationEvent(string questId)
        {
            QuestId = questId;
        }
    }
}
