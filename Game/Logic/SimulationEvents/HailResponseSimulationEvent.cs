﻿using Xenosea.Logic.Hails;

namespace Xenosea.Logic.SimulationEvents
{
    public class HailResponseSimulationEvent : ISimulationEvent
    {
        public Submarine TargetSubmarine { get; }
        public IHailResponseLogic HailResponse{ get; }
        
        public HailResponseSimulationEvent(
            Submarine targetSubmarine,
            IHailResponseLogic hailResponse)
        {
            TargetSubmarine = targetSubmarine;
            HailResponse = hailResponse;
        }
    }
}
