﻿namespace Xenosea.Logic.SimulationEvents
{
    public class CargoScanUltimatumSimulationEvent : ISimulationEvent
    {
        public Submarine DemandingSubmarine { get; }
        public Submarine TargetSubmarine { get; }
        public CommodityType[] ContrabandFound { get; }
        public bool IsLawful { get; }

        public CargoScanUltimatumSimulationEvent(
            Submarine demandingSubmarine, 
            Submarine targetSubmarine, 
            CommodityType[] contrabandFound,
            bool isLawful)
        {
            DemandingSubmarine = demandingSubmarine;
            TargetSubmarine = targetSubmarine;
            ContrabandFound = contrabandFound;
            IsLawful = isLawful;
        }
    }
}
