﻿namespace Xenosea.Logic.SimulationEvents
{
    public class DialogueTriggerSimulationEvent : ISimulationEvent
    {
        public string TriggerName { get; private set; }

        public DialogueTriggerSimulationEvent(string triggerName)
        {
            TriggerName = triggerName;
        }
    }
}
