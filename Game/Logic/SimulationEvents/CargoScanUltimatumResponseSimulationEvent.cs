﻿namespace Xenosea.Logic.SimulationEvents
{
    public class CargoScanUltimatumResponseSimulationEvent : ISimulationEvent
    {
        public Submarine DemandingSubmarine { get; }
        public Submarine TargetSubmarine { get; }
        public bool WasAgreedTo { get; }

        public CargoScanUltimatumResponseSimulationEvent(
            Submarine demandingSubmarine, 
            Submarine targetSubmarine,
            bool wasAgreedTo)
        {
            DemandingSubmarine = demandingSubmarine;
            TargetSubmarine = targetSubmarine;
            WasAgreedTo = wasAgreedTo;
        }
    }
}
