﻿using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Detection;

namespace Xenosea.Logic.SimulationEvents
{
    public class PlayerSalvagedSimulationEvent : ISimulationEvent
    {
        public ISalvageTarget SalvagedSub { get; }
        public List<CommodityType> CarriedLoot { get; }

        public PlayerSalvagedSimulationEvent(
            ISalvageTarget SalvagedSub, 
            List<CommodityType> carriedLoot)
        {
            this.SalvagedSub = SalvagedSub;
            CarriedLoot = carriedLoot.ToList();
        }
    }
}
