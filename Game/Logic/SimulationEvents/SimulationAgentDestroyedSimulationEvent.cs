﻿using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic.SimulationEvents
{
    public class SimulationAgentDestroyedSimulationEvent : ISimulationEvent
    {
        public SimulationAgent SimulationAgent { get; private set; }

        public SimulationAgentDestroyedSimulationEvent(SimulationAgent simulationAgent)
        {
            SimulationAgent = simulationAgent;
        }
    }
}
