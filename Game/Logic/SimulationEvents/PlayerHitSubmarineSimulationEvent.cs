﻿namespace Xenosea.Logic.SimulationEvents
{
    public class PlayerHitSubmarineSimulationEvent : ISimulationEvent
    {
        public Submarine Target { get; private set; }

        public PlayerHitSubmarineSimulationEvent(Submarine target)
        {
            Target = target;
        }
    }
}
