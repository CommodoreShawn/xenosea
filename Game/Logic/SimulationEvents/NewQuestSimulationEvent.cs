﻿namespace Xenosea.Logic.SimulationEvents
{
    public class NewQuestSimulationEvent : ISimulationEvent
    {
        public IQuest NewQuest { get; private set; }

        public NewQuestSimulationEvent(IQuest newQuest)
        {
            NewQuest = newQuest;
        }
    }
}
