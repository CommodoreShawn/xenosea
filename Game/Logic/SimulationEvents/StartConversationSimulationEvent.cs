﻿namespace Xenosea.Logic.SimulationEvents
{
    public class StartConversationSimulationEvent : ISimulationEvent
    {
        public Npc Npc { get; private set; }

        public StartConversationSimulationEvent(Npc npc)
        {
            Npc = npc;
        }
    }
}
