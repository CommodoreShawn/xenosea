﻿using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic.SimulationEvents
{
    public class NewSeafloorObjectSimulationEvent : ISimulationEvent
    {
        public SeafloorObject NewObject { get; private set; }

        public NewSeafloorObjectSimulationEvent(SeafloorObject newObject)
        {
            NewObject = newObject;
        }
    }
}
