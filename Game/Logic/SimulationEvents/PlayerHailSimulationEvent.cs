﻿namespace Xenosea.Logic.SimulationEvents
{
    public class PlayerHailSimulationEvent : ISimulationEvent
    {
        public Submarine TargetSubmarine { get; }

        public PlayerHailSimulationEvent(
            Submarine targetSubmarine)
        {
            TargetSubmarine = targetSubmarine;
        }
    }
}
