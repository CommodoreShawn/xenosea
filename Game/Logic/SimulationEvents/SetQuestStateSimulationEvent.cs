﻿namespace Xenosea.Logic.SimulationEvents
{
    public class SetQuestStateSimulationEvent : ISimulationEvent
    {
        public string QuestId { get; private set; }

        public string State { get; private set; }

        public SetQuestStateSimulationEvent(string questId, string state)
        {
            QuestId = questId;
            State = state;
        }
    }
}
