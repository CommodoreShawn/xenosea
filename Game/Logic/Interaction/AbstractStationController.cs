using Godot;

namespace Xenosea.Logic
{
    public abstract class AbstractStationController : Spatial
    {
        protected IStation Station { get; private set; }

        public override void _Ready()
        {
            Station = this.FindParent<IStation>();
        }
    }
}
