using CSPID;
using Godot;

namespace Xenosea.Logic
{
    public abstract class AbstractSubController : Spatial
    {
        public Submarine Submarine { get; set; }

        private PIDController _rudderPid;
        private  PIDController _divePid;
        private PIDController _throttlePid;
        private GameplayRoot _gameplayRoot;

        protected virtual double TurningPidGain => 3;

        public override void _Ready()
        {
            Submarine = this.FindParent<Submarine>();
            _gameplayRoot = this.FindParent<GameplayRoot>();

            _rudderPid = new PIDController(
                minimumError: -4,
                maximumError: 4,
                minimumControl: -1,
                maximumControl: 1)
            {
                MaximumStep = double.MaxValue,
                ProportionalGain = TurningPidGain,
                IntegralGain = 0,
                DerivativeGain = 0
            };

            _divePid = new PIDController(
                minimumError: -300,
                maximumError: 300,
                minimumControl: -1,
                maximumControl: 1)
            {
                MaximumStep = double.MaxValue,
                ProportionalGain = TurningPidGain,
                IntegralGain = 0,
                DerivativeGain = 0
            };

            _throttlePid = new PIDController(
                minimumError: -600,
                maximumError: 600,
                minimumControl: -0.25,
                maximumControl: 1)
            {
                MaximumStep = double.MaxValue,
                ProportionalGain = 3,
                IntegralGain = 0,
                DerivativeGain = 0
            };
        }

        public void AutopilotTo(Vector3 destination, float delta, float throttleLimit)
        {
            if (delta == 0)
            {
                return;
            }

            var yawError = Submarine.Transform.basis.Xform(Vector3.Back)
                        .SignedAngleTo(Submarine.RealPosition.DirectionTo(destination), Submarine.Transform.basis.y);

            var turnMin = 0f;

            Submarine.Throttle = Mathf.Clamp(0.5f - Mathf.Abs(yawError) / 1.5f + 0.5f, turnMin, 1f) * throttleLimit;

            Submarine.Rudder = (float)_rudderPid.Next(error: yawError, elapsed: delta);

            var groundAlt = Submarine.Altitude - Submarine.DistanceToGround;

            var targetAltitude = Mathf.Max(_gameplayRoot.GetAltitude(destination), groundAlt + 100);

            if (_gameplayRoot is Gameplay gameplay && gameplay.CenterDatum != null
                && Submarine.RealPosition.DistanceTo(destination) > Constants.DATUM_DISTANCE * 2)
            {
                var roofAlt = Constants.WORLD_RADIUS - gameplay.CenterDatum.IceDepth;

                if(roofAlt - 100 < targetAltitude)
                {
                    if(roofAlt - 100 > groundAlt + 100)
                    {
                        targetAltitude = roofAlt - 100;
                    }
                    else
                    {
                        targetAltitude = (roofAlt - 100 + groundAlt + 100) / 2f;
                    }
                }
            }

            var altitudeError = targetAltitude - _gameplayRoot.GetAltitude(Submarine.RealPosition);

            Submarine.Dive = (float)_divePid.Next(error: altitudeError, elapsed: delta);
        }

        public void CarefulAutopilotTo(Vector3 destination, float delta, float speedLimit)
        {
            if (delta == 0)
            {
                return;
            }

            var yawError = Submarine.Transform.basis.Xform(Vector3.Back)
                        .SignedAngleTo(Submarine.RealPosition.DirectionTo(destination), Submarine.Transform.basis.y);
            Submarine.Rudder = (float)_rudderPid.Next(error: yawError, elapsed: delta);

            if (Mathf.Abs(yawError) > 0.1)
            {
                Submarine.Throttle = 0;
            }
            else
            {
                var distError = Submarine.RealPosition.DistanceTo(destination);
                distError = Mathf.Clamp(distError, -600, 600);
                Submarine.Throttle = speedLimit * (float)_throttlePid.Next(error: distError, elapsed: delta);
            }

            var altitudeError = _gameplayRoot.GetAltitude(destination) - _gameplayRoot.GetAltitude(Submarine.RealPosition);
            
            Submarine.Dive = (float)_divePid.Next(error: altitudeError * 10, elapsed: delta);
        }

        public void SlideTo(Vector3 destination, Vector3 facing, float delta, float slideSpeed)
        {
            if (delta == 0)
            {
                return;
            }

            var yawError = Submarine.Transform.basis.Xform(Vector3.Back)
                        .SignedAngleTo(facing, Submarine.Transform.basis.y);
            Submarine.Rudder = (float)_rudderPid.Next(error: yawError, elapsed: delta);

            var fromTo = (destination - Submarine.RealPosition);
            var targetSpeed = fromTo.Length() / 20 + 1;
            
            var targetVelocity = fromTo.Normalized() * targetSpeed * slideSpeed;

            var slideForce = targetVelocity - Submarine.LinearVelocity;

            if (slideForce.Length() > 1)
            {
                slideForce = slideForce.Normalized();
            }

            Submarine.SlideForce = slideForce;
            Submarine.Throttle = 0;
            Submarine.Dive = 0;
        }

        public void AutopilotHeading(Vector3 heading, float delta, float throttle)
        {
            if (delta == 0)
            {
                return;
            }

            var yawError = Submarine.Transform.basis.Xform(Vector3.Back)
                        .SignedAngleTo(heading, Submarine.Transform.basis.y);

            Submarine.Throttle = throttle;

            Submarine.Rudder = (float)_rudderPid.Next(error: yawError, elapsed: delta);
        }

        public void AutopilotAltitude(Vector3 destination, float altitudeAdjustment, float delta)
        {
            var altitudeError = _gameplayRoot.GetAltitude(destination) - _gameplayRoot.GetAltitude(Submarine.RealPosition);

            Submarine.Dive = (float)_divePid.Next(error: altitudeError, elapsed: delta);
        }

        public void AutopilotFollow(Vector3 destination, float delta)
        {
            if (delta == 0)
            {
                return;
            }

            var yawError = Submarine.Transform.basis.Xform(Vector3.Back)
                        .SignedAngleTo(Submarine.RealPosition.DirectionTo(destination), Submarine.Transform.basis.y);

            var distError = Submarine.RealPosition.DistanceTo(destination) - 100;
            distError = Mathf.Clamp(distError, -100, 300);

            Submarine.Throttle = (float)_throttlePid.Next(error: distError, elapsed: delta) 
                * Mathf.Clamp(0.7f - yawError / 3 + 0.5f, 0.4f, 1f);

            Submarine.Rudder = (float)_rudderPid.Next(error: yawError, elapsed: delta);

            var altitudeError = _gameplayRoot.GetAltitude(destination) - _gameplayRoot.GetAltitude(Submarine.RealPosition);

            Submarine.Dive = (float)_divePid.Next(error: altitudeError, elapsed: delta);
        }
    }
}
