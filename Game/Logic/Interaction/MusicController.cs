﻿using Godot;
using System;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

public class MusicController : AudioStreamPlayer
{
    [Export]
    public MusicOption[] Options;

    private bool _playingBattle;
    private Random _random;

    [Inject]
    private IPlayerData _playerData;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _random = new Random();

        PlayNewTrack();
    }

    public override void _ExitTree()
    {
        base._ExitTree();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_playerData.InBattle != _playingBattle)
        {
            _playingBattle = _playerData.InBattle;
            PlayNewTrack();
        }
    }
        
    public void OnTrackFinished()
    {
        PlayNewTrack();
    }

    private void PlayNewTrack()
    {
        var options = Options
            .Where(o => o.IsBattle == _playingBattle)
            .ToArray();

        if(options.Any())
        {
            Stream = options[_random.Next(options.Length)].MusicStream;
            Playing = true;
        }
    }
}
