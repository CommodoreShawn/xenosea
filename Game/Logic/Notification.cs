﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xenosea.Logic
{
    public class Notification
    {
        public bool IsMessage { get; set; }
        public bool IsImportant { get; set; }
        public bool IsGood { get; set; }
        public bool IsBad { get; set; }
        public string Body { get; set; }
    }
}
