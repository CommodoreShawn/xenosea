﻿using Godot;
using Xenosea.Resources;

namespace Xenosea.Logic.Detection
{
    public interface ISonarContact
    {
        string ReferenceId { get; }
        ulong Id { get; }
        string FriendlyName { get; }
        Vector3 RealPosition { get; }
        float OverallNoise { get; }
        float BracketSizeFactor { get; }
        bool IsPlayerVisible { get; set; }
        ContactType ContactType { get; }
        Faction Faction { get; }
        bool IsDockable { get; }
        Vector3 Velocity { get; }
        float SpeakingTimer { get; set; }
        bool IsHostileTo(ISonarContact other);
        bool IsFriendlyTo(ISonarContact other);
        string DebugText { get; set; }
    }
}
