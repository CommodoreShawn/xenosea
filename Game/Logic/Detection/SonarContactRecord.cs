﻿using Godot;
using System.Collections.Generic;
using Xenosea.Resources;

namespace Xenosea.Logic.Detection
{
    public class SonarContactRecord : ITargetableThing
    {
        public ISonarContact SonarContact { get; set; }
        public Vector3 EstimatedPosition { get; set; }
        public bool IsDetected { get; set; }
        public bool PositionLocked { get; set; }
        public bool TypeKnown { get; set; }
        public bool Identified { get; set; }
        public bool CargoScan { get; set; }
        public float PositionEstimateCounter { get; set; }
        public float SignalLevel { get; set; }
        public float DetectionCheckCounter { get; set; }
        public float TimeSinceUpdate { get; set; }
        public float TimeDetected { get; set; }
        public float WasDetected { get; set; }
        public bool WasTerrainBlocked { get; set; }
        public string FriendlyName => SonarContact.FriendlyName;
        public ContactType ContactType => SonarContact.ContactType;
        public bool IsValidInstance => Object.IsInstanceValid(SonarContact as Object);
        public System.DateTime LastUpdateTime { get; set; }
        public Vector3 LastKnownVelocity { get; set; }
        public float AggrivationScore { get; set; }
        public Dictionary<string, string> AiData { get; } = new Dictionary<string, string>();
        public float Altitude { get; set; }
        public string ReferenceId => SonarContact.ReferenceId;
        public bool IsSpeaking => SonarContact.SpeakingTimer > 0;

        public bool IsHostileTo(ISonarContact other)
        {
            return SonarContact.IsHostileTo(other);
        }

        public bool IsFriendlyTo(ISonarContact other)
        {
            return SonarContact.IsFriendlyTo(other);
        }
    }
}
