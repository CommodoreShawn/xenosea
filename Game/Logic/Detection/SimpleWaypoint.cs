﻿using Godot;
using System;
using System.Collections.Generic;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Resources;

namespace Xenosea.Logic.Detection
{
    public class SimpleWaypoint : ITargetableThing
    {
        public Vector3 EstimatedPosition { get; private set; }
        public bool PositionLocked => true;
        public bool TypeKnown => true;
        public bool Identified => true;
        public Texture TypeIcon => null;
        public string FriendlyName { get; private set; }
        public bool IsDetected { get; set; }
        public bool IsValidInstance => true;
        public List<Vector3> Waypoints { get; }
        public ContactType ContactType => ContactType.Waypoint;

        public string Name => FriendlyName;
        public string ReferenceId { get; private set; }
        public bool IsSpeaking => false;

        public SimpleWaypoint(string name, string referenceId, Vector3 pos)
        {
            FriendlyName = name;
            ReferenceId = referenceId;
            EstimatedPosition = pos;
            IsDetected = true;
        }

        public void UpdatePosition(Vector3 pos)
        {
            EstimatedPosition = pos;
        }

        public void SaveState(SerializationObject objectRoot)
        {
            objectRoot.SetAttribute("friendly_name", FriendlyName);
            objectRoot.SetAttribute("reference_id", ReferenceId);
            objectRoot.SetAttribute("pos_x", EstimatedPosition.x.ToString());
            objectRoot.SetAttribute("pos_y", EstimatedPosition.y.ToString());
            objectRoot.SetAttribute("pos_z", EstimatedPosition.z.ToString());
        }

        public static SimpleWaypoint LoadState(SerializationObject objectRoot)
        {
            return new SimpleWaypoint(
                objectRoot.GetAttribute("friendly_name"),
                objectRoot.GetAttribute("reference_id"),
                new Vector3(
                    Convert.ToSingle(objectRoot.GetAttribute("pos_x")),
                    Convert.ToSingle(objectRoot.GetAttribute("pos_y")),
                    Convert.ToSingle(objectRoot.GetAttribute("pos_z"))));
        }

        public bool IsHostileTo(ISonarContact other)
        {
            return false;
        }

        public bool IsFriendlyTo(ISonarContact other)
        {
            return false;
        }
    }
}
