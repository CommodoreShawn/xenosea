﻿using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Resources;

namespace Xenosea.Logic.Detection
{
    public class MissionPath : ITargetableThing
    {
        public Vector3 EstimatedPosition => Waypoints.FirstOrDefault();
        public bool PositionLocked => true;
        public bool TypeKnown => true;
        public bool Identified => true;
        public Texture TypeIcon => null;
        public string FriendlyName => "Mission Waypoint";
        public bool IsDetected => true;
        public bool IsValidInstance => Waypoints.Any();
        public List<Vector3> Waypoints { get; }
        public ContactType ContactType => ContactType.Waypoint;

        public string ReferenceId => "missionpath";

        public bool IsSpeaking => false;

        public MissionPath(IEnumerable<Vector3> waypoints)
        {
            Waypoints = waypoints.ToList();
        }

        public MissionPath(PatrolAiPath patrolPath)
        {
            Waypoints = patrolPath.GetWaypoints()
                .Select(w => w.Position)
                .ToList();
        }

        public bool IsHostileTo(ISonarContact other)
        {
            return false;
        }

        public bool IsFriendlyTo(ISonarContact other)
        {
            return false;
        }
    }
}
