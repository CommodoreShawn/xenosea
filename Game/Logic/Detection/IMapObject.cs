﻿using Godot;
using Xenosea.Resources;

namespace Xenosea.Logic.Detection
{
    public interface IMapObject
    {
        string FriendlyName { get; }
        ContactType ContactType { get; }
        string ReferenceId { get; }
        Vector3 EstimatedPosition { get; }
        bool TypeKnown { get; }
        bool Identified { get; }

        bool IsHostileTo(ISonarContact other);
        bool IsFriendlyTo(ISonarContact other);
    }
}
