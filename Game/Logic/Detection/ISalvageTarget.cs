﻿using Godot;

namespace Xenosea.Logic.Detection
{
    public interface ISalvageTarget
    {
        string FriendlyName { get; }
        Vector3 RealPosition { get; }
        bool CanBeSalvaged { get; }
        CommodityStorage CommodityStorage { get; }
    }
}
