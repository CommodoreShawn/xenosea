﻿namespace Xenosea.Logic.Detection
{
    public interface ITargetableThing: IMapObject
    {
        bool PositionLocked { get; }
        bool IsDetected { get; }
        bool IsValidInstance { get; }
        bool IsSpeaking { get; }
    }
}
