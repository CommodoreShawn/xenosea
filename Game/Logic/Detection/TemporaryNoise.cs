﻿namespace Xenosea.Logic.Detection
{
    public class TemporaryNoise
    {
        public float TimeToLive { get; set; }
        public float Elapsed { get; set; }
        public float NoiseAmount { get; set; }
    }
}
