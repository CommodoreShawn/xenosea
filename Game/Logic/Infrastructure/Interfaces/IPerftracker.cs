﻿namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IPerftracker
    {
        void PrintPerfInfo(int framesSinceLast);

        void LogPerf(string name, long elapsedMilliseconds);
    }
}
