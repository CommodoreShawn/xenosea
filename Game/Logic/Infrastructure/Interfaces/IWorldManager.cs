﻿using System;
using System.Collections.Generic;
using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IWorldManager
    {
        DateTime SessionStartTime { get; set; }
        DateTime CurrentDate { get; set; }
        IEnumerable<Faction> Factions { get; }
        IEnumerable<WorldDatum> WorldDatums { get; }
        IEnumerable<SimulationStation> SimulationStations { get; }
        IEnumerable<SimulationAgent> SimulationAgents { get; }
        IEnumerable<WeatherSystem> WeatherSystems { get; }
        IEnumerable<SeafloorObject> SeafloorObjects { get; }

        void AddFaction(Faction faction);
        void AddSimulationStation(SimulationStation station);
        void AddSimulationAgent(SimulationAgent agent);
        void AddHubPoint(HubPoint hubPoint);
        void Clear();
        void AddDatums(IEnumerable<WorldDatum> worldDatums);
        void RemoveSimulationAgent(SimulationAgent deadAgent);
        void AddWeatherSystem(WeatherSystem weatherSystem);
        void RemoveWeatherSystem(WeatherSystem weatherSystem);
        void AddSeafloorObject(SeafloorObject obj);
        void RemoveSeafloorObject(SeafloorObject obj);
    }
}
