﻿using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface ISimulationBattleHandler
    {
        SimulationBattle GetOrStartBattle(WorldDatum location);
    }
}
