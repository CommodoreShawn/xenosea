﻿using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface ISimToEngineLink
    {
        Gameplay GameplayInstance { get; set; }

        void Spawn(SimulationAgent agent);

        void Despawn(SimulationAgent agent);
    }
}
