﻿namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface ISoundEffectPlayer
    {
        SoundEffectPlayerNode PlayerNode {get; set;}

        void PlayUiClick();
    }
}
