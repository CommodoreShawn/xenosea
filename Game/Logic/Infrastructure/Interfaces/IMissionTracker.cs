﻿using System.Collections.Generic;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Missions;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IMissionTracker
    {
        IMission TrackedMission { get; set; }

        IEnumerable<IMission> GetAvailableMissions(IStation station);

        IEnumerable<IMission> GetAvailableMissions(SimulationStation station);

        IEnumerable<IMission> GetCurrentMissions(Submarine agent);

        IEnumerable<IMission> GetCurrentMissions(SimulationAgent agent);

        void PickupMission(IMission mission, IStation fromStation, Submarine agent);

        void PickupMission(IMission mission, SimulationStation fromStation, SimulationAgent agent);

        void DestroyMission(IMission mission, Submarine agent);

        void DestroyMission(IMission mission, SimulationAgent agent);

        void DestroyMission(IMission mission, SimulationStation station);

        void DeliverMission(IMission mission, IStation toStation, Submarine agent);

        void DeliverMission(IMission mission, SimulationStation toStation, SimulationAgent agent);

        IEnumerable<ITradeMission> GetTradeMissionsByCommodity(CommodityType commodity);

        IEnumerable<SimulationPatrolMission> GetPatrolMissionsBySource(SimulationStation fromStation);

        void AddMission(IStation location, IMission mission);

        void AddMission(SimulationStation location, IMission mission);

        void Clear();

        bool IsMissionAvailableAt(IStation location, IMission mission);

        bool IsHeldByAgent(Submarine agent, IMission mission);
        void TransferMissions(Submarine oldSub, Submarine newSub);
    }
}
