﻿using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IStateTrackingService
    {
        void ClearState();
        void SaveState(SerializationObject root);
        void LoadState(IResourceLocator resourceLocator, SerializationObject root);
    }
}
