﻿using System.Collections.Generic;
using Xenosea.Logic.Dialog;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IConversationManager
    {
        NpcConversation StartConversationWith(Npc npc);
        
        IEnumerable<ConversationResponse> GetRumorsFor(WorldStation station, int numberOfRumors);
    }
}
