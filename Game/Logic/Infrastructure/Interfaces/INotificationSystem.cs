﻿using Xenosea.Logic.Detection;

namespace Xenosea.Logic.Infrastructure.Interfaces
{

    public delegate void NotificationHandler(Notification notification);

    public interface INotificationSystem
    {
        void AddNotification(Notification notification);

        event NotificationHandler OnNotification;

        void Broadcast(ISonarContact sender, string message);
    }
}
