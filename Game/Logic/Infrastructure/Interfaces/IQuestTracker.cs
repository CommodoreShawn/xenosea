﻿using System.Collections.Generic;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IQuestTracker
    {
        IEnumerable<IQuest> AllQuests { get; }

        Dictionary<string, string> GetAllQuestStates();

        string GetQuestState(string questId);

        IQuest GetQuest(string questId);

        void InitializeQuests(IEnumerable<IQuest> quests);
    }
}
