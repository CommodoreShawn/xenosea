﻿using System.Collections.Generic;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public delegate void SettingsChangedHandler();

    public interface ISettingsService
    {
        float EffectsVolume { get; set; }
        float MusicVolume { get; set; }
        Dictionary<string, string> RebindableActions { get; }
        bool IsFullscreen { get; set; }

        event SettingsChangedHandler OnSettingsChanged;

        void LoadSettings();
        void SaveSettings();

        void ApplyDisplaySettings();
    }
}
