﻿namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IWorldSimulator
    {
        void Process(float delta);
        void Clear();
    }
}
