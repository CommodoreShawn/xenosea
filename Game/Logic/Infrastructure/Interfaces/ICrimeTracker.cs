﻿namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface ICrimeTracker
    {
        void Clear();

        void ReportCrime(ICombatant offender, ICombatant victim, float actualDamage);

        void ReportPiracy(ICombatant offender, ICombatant victim);
    }
}
