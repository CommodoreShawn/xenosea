﻿using Godot;
using System;
using System.Collections.Generic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IPlayerData
    {
        PlayerController PlayerController { get; set; }
        Submarine PlayerSub { get; set; }
        Faction Faction { get; set; }
        double Money { get; set; }
        WorldDatum SpawnDatum { get; set; }
        float DetailedSimDistance { get; set; }
        SubLoadout PlayerLoadout { get; set; }
        SubState PlayerLoadLoadout { get; set; }
        DateTime LastSaveTime { get; set; }
        SimpleWaypoint ManualWaypoint { get; set; }
        bool WeaponsSafe { get; set; }
        bool CruiseStealthMode { get; set; }
        bool InBattle { get; set; }
        bool InTutorial { get; set; }
        bool SkipIntro { get; set; }

        List<KnownPointOfInterest> KnownPointsOfInterest { get; }
        List<string> KnownTopics { get; }
    }
}
