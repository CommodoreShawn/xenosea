﻿using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface ISeafloorObjectSpawner
    {
        bool CanHandle(SeafloorObject seafloorObject);

        void Spawn(DatumCell datumCell, SeafloorObject seafloorObject);
    }
}
