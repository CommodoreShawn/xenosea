﻿using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Resources;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IStationGenerator
    {
        void PlaceStations(
            WorldDefinition worldDefinition,
            ReportStepProgress reportStepProgress);

        StationBuildout GenerateStation(WorldDatum location, StationDefinition stationDefinition);

        void CalculateStationInfleunce(
            ReportStepProgress reportStepProgress);

        StationBuildout GenerateTestStation(
            float latitude,
            float longitude,
            int moduleCount,
            StationShapeProfile stationShape,
            StationLayoutType layoutType,
            float gridSpacing,
            float groundSlopeTolerance,
            float moduleHeight,
            float tradeSourceLatitude,
            float tradeSourceLongitude,
            int dockingTowerCount,
            int defenseTowerCount,
            float attackSourceLatitude,
            float attackSourceLongitude,
            float defenseTowerDistance,
            int farmCount,
            float farmPlotSize,
            float plantSpacing);
    }
}
