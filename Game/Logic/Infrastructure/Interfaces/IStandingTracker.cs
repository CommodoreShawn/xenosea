﻿namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IStandingTracker
    {
        void Clear();

        void Initialize(Faction playerFaction);

        void AddStandingChange(Faction toFaction, double standingChange, string description);
    }
}
