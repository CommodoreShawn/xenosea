﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface ISubSimulator
    {
        void Process(float delta);
        void Initialize();
        void Clear();
    }
}
