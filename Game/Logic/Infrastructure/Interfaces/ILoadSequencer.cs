﻿namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface ILoadSequencer
    {
        void StartNewWorld(WorldDefinition worldDefinition, ReportProgress reportProgress);

        void LoadWorld(WorldDefinition worldDefinition, string fileName, ReportProgress reportProgress);
    }
}
