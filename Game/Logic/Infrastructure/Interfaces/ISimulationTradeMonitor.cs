﻿using System;
using System.Collections.Generic;
using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface ISimulationTradeMonitor
    {
        IEnumerable<SimulationStation> GetExports(SimulationStation source, CommodityType commodity);

        IEnumerable<SimulationStation> GetImports(SimulationStation destination, CommodityType commodity);

        IEnumerable<Tuple<SimulationStation, CommodityType>> GetExports(SimulationStation source);

        IEnumerable<Tuple<SimulationStation, CommodityType>> GetImports(SimulationStation destination);
    }
}
