﻿namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface ILogger
    {
        void Error(string message);

        void Error(System.Exception ex, string message);

        void Info(string message);
    }
}
