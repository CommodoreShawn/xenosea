﻿using Godot;
using System.Collections.Generic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface ISonarContactTracker
    {
        void AddSonarContact(ISonarContact sonarContact);

        IEnumerable<ISonarContact> GetSonarContacts();

        void SetVisibleContacts(Submarine playerSub, IEnumerable<ISonarContact> visibleContacts);
        
        void RemoveSonarContact(ISonarContact sonarContact);

        SonarDetectionInfo RunSonarDetectionLogic(
            World world,
            Transform detectorGlobalTransform, 
            float detectorSensitivity, 
            bool towedArrayActive,
            ISonarContact contact);
    }
}
