﻿using System.Collections.Generic;
using Xenosea.addons.XenoTalk.Data;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IResourceLocator
    {
        IEnumerable<SubHullType> AllSubHullTypes { get; }
        IEnumerable<ArmorType> AllArmorTypes { get; }
        IEnumerable<TorpedoType> AllTorpedoTypes { get; }
        IEnumerable<MinisubType> AllMinisubTypes { get; }
        IEnumerable<DiveTeamType> AllDiveTeamTypes { get; }
        IEnumerable<BasicModuleType> AllModuleTypes { get; }


        MinisubType GetMinisubType(string name);

        DiveTeamType GetDiveTeamType(string name);

        TorpedoType GetTorpedoType(string name);

        CommodityType GetCommodityType(string name);

        AgentDefinition GetAgentDefinition(string name);
        
        AmmunitionType GetAmmoType(string name);

        FixedPointOfInterest GetFixedPoi(string name);

        List<ConvoTopic> GetConversationTopics();

        Npc GetNpc(string name);

        TModule GetModuleType<TModule>(string name) where TModule : BasicModuleType;

        SubHullType GetSubHull(string name);

        ArmorType GetArmorType(string name);

        AbstractEconomicProcess GetEconomicProcess(string name);

        Faction GetFaction(string name);
        
        StationModuleSet GetStationModuleSet(string name);
    }
}
