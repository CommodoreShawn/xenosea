﻿using System.Collections.Generic;
using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IGameSerializer
    {
        string ActiveSaveName { get; }

        IEnumerable<SaveListItem> ListSaves();

        void ClearGameState();

        void SaveGameState(string fileName, ReportProgress reportProgress);

        void LoadGameState(string fileName, ReportProgress reportProgress);

        SaveFileSummary GetSaveSummary(string fileName);

        void DeleteSave(string fileName);

        bool IsValidFileName(string fileName);
    }
}
