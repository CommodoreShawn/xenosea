﻿using System.Collections.Generic;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IBountyTracker
    {
        IEnumerable<RegisteredBounty> AvailableBounties { get; }

        void RedeemBounty(RegisteredBounty bounty);

        void AddBounty(Faction targetOwner, string targetName, double targetValue);
    }
}
