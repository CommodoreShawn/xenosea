﻿using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Missions;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IEconomicLogger
    {
        void LogMissionPickup(SimulationAgent agent, ITradeMission tradeMission);

        void LogMissionDeliver(SimulationAgent agent, ITradeMission tradeMission);
    }
}
