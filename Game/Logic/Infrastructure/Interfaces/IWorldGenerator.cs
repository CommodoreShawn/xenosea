﻿using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic.Infrastructure.Interfaces
{
    public interface IWorldGenerator
    {
        void GenerateWorld(
            WorldDefinition worldDefinition, 
            ReportProgress reportProgress);

        void PlaceAgents(ReportStepProgress reportStepProgress);

        float GetDepthAt(
            float latitude,
            float longitude);

        float GetVolcanismAt(
            float latitude,
            float longitude);

        void QueueGeometryRebuild(DatumCell datumCell, int vertexCount);

        CellBuildData ForceGeometryRebuild(float latitude, float longitude, int vertexCount, bool plainRoof);

        float GetDetailedDepthAt(
            float latitude,
            float longitude);
    }
}
