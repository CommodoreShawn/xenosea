﻿namespace Xenosea.Logic.Infrastructure.Data
{
    public class SonarDetectionInfo
    {
        public bool IsTerrainBlocked { get; set; }
        public float Noise { get; set; }
        public float SensorGain { get; set; }
        public float Distance { get; set; }
    }
}
