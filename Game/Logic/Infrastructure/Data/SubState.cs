﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class SubState
    {
        public string FriendlyName { get; set; }
        public float Health { get; set; }
        public SubHullType HullType { get; set; }
        public ArmorType ArmorType { get; set; }
        public EngineModuleType EngineModule { get; set; }
        public SonarModuleType SonarModule { get; set; }
        public TowedArrayModuleType TowedArrayModule { get; set; }
        public TorpedoLauncherModuleType[] LauncherModules { get; set; }
        public TurretModuleType[] TurretModules { get; set; }
        public MinisubHangarModuleType[] HangarModules { get; set; }
        public BasicModuleType[] OtherModules { get; set; }
        public DiveTeamType[] DiveTeams { get; set; }
        public TorpedoType[][] Torpedoes { get; set; }
        public MinisubType[] Minisubs { get; set; }
        public Tuple<CommodityType, float>[] Cargo { get; set; }
        public SimulationStation DockedTo { get; set; }
        public int DockNumber { get; set; }
        public TorpedoType[] LoadedTorpedo { get; set; }
        public AmmunitionType[] ActiveAmmo { get; set; }

        public void SaveState(SerializationObject root)
        {
            root.SetAttribute("health", Health.ToString());
            root.SetAttribute("friendly_name", FriendlyName);
            root.SetAttribute("hull_type", HullType.SubHullTypeId);
            root.SetAttribute("armor_type", ArmorType.ArmorTypeId);
            root.SetAttribute("engine_module", EngineModule?.ModuleTypeId ?? string.Empty);
            root.SetAttribute("sonar_module", SonarModule?.ModuleTypeId ?? string.Empty);
            root.SetAttribute("towed_array_module", TowedArrayModule?.ModuleTypeId ?? string.Empty);
            root.AddList("launcher_modules", LauncherModules.Select(x => x?.ModuleTypeId ?? string.Empty));
            root.AddList("turret_moduels", TurretModules.Select(x => x?.ModuleTypeId ?? string.Empty));
            root.AddList("hangar_modules", HangarModules.Select(x => x?.ModuleTypeId ?? string.Empty));
            root.AddList("other_modules", OtherModules.Select(x => x?.ModuleTypeId ?? string.Empty));
            root.AddList("divers", DiveTeams.Select(x => x.Name));
            for (var i = 0; i < Torpedoes.Length; i++)
            {
                root.AddList("torpedoes" + i, Torpedoes[i].Select(x => x.Name));
            }
            root.AddList("minisubs", Minisubs.Select(x => x?.Name ?? string.Empty));

            root.SetAttribute("docked_to", DockedTo?.Name);
            root.SetAttribute("dock_number", DockNumber.ToString());
            root.AddList("active_ammo", ActiveAmmo.Select(x => x?.Name ?? string.Empty));
            root.AddList("loaded_torpedoes", LoadedTorpedo.Select(x => x?.Name ?? string.Empty));

            var cargoRoot = root.AddChild("cargo");
            foreach (var tuple in Cargo)
            {
                cargoRoot.AddChild("item")
                    .SetAttribute("commodity", tuple.Item1.Name)
                    .SetAttribute("qty", tuple.Item2.ToString());
            }
        }

        public static SubState LoadState(
            IResourceLocator resourceLocator,
            IWorldManager worldManager,
            SerializationObject root)
        {
            var state = new SubState();

            state.FriendlyName = root.GetAttribute("friendly_name");
            state.Health = Convert.ToSingle(root.GetAttribute("health"));
            state.HullType = resourceLocator.GetSubHull(root.GetAttribute("hull_type"));
            state.ArmorType = resourceLocator.GetArmorType(root.GetAttribute("armor_type"));
            state.EngineModule = GetModuleOrNull<EngineModuleType>(resourceLocator, root.GetAttribute("engine_module"));
            state.SonarModule = GetModuleOrNull<SonarModuleType>(resourceLocator, root.GetAttribute("sonar_module"));
            state.TowedArrayModule = GetModuleOrNull<TowedArrayModuleType>(resourceLocator, root.GetAttribute("towed_array_module"));
            state.LauncherModules = root.GetList("launcher_modules")
                .Select(x => GetModuleOrNull<TorpedoLauncherModuleType>(resourceLocator, x))
                .ToArray();
            state.TurretModules = root.GetList("turret_moduels")
                .Select(x => GetModuleOrNull<TurretModuleType>(resourceLocator, x))
                .ToArray();
            state.HangarModules = root.GetList("hangar_modules")
                .Select(x => GetModuleOrNull<MinisubHangarModuleType>(resourceLocator, x))
                .ToArray();
            state.OtherModules = root.GetList("other_modules")
                .Select(x => GetModuleOrNull<BasicModuleType>(resourceLocator, x))
                .ToArray();
            state.DiveTeams = root.GetList("divers")
                .Select(x => resourceLocator.GetDiveTeamType(x))
                .ToArray();

            var torps = new List<TorpedoType[]>();
            for (var i = 0; i < state.LauncherModules.Length; i++)
            {
                torps.Add(root.GetList("torpedoes" + i)
                    .Select(x => resourceLocator.GetTorpedoType(x))
                    .ToArray());
            }
            state.Torpedoes = torps.ToArray();

            state.Minisubs = root.GetList("minisubs")
                .Select(x => string.IsNullOrWhiteSpace(x) ? null : resourceLocator.GetMinisubType(x))
                .ToArray();

            state.DockedTo = root.LookupReference(
                "docked_to",
                worldManager.SimulationStations,
                s => s.Name);

            state.Cargo = root.GetChild("cargo").Children
                .Select(x => Tuple.Create(
                        resourceLocator.GetCommodityType(x.GetAttribute("commodity")),
                        Convert.ToSingle(x.GetAttribute("qty"))))
                .ToArray();

            state.DockNumber = Convert.ToInt32(root.GetAttribute("dock_number"));

            state.ActiveAmmo = root.GetList("active_ammo")
                .Select(x => string.IsNullOrWhiteSpace(x) ? null : resourceLocator.GetAmmoType(x))
                .ToArray();
            state.LoadedTorpedo = root.GetList("loaded_torpedoes")
                .Select(x => string.IsNullOrWhiteSpace(x) ? null : resourceLocator.GetTorpedoType(x))
                .ToArray();

            return state;
        }

        private static TModule GetModuleOrNull<TModule>(
            IResourceLocator resourceLocator,
            string typeName) where TModule : BasicModuleType
        {
            if (string.IsNullOrWhiteSpace(typeName))
            {
                return null;
            }

            return resourceLocator.GetModuleType<TModule>(typeName);

        }
    }
}
