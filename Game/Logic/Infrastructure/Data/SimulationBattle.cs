﻿using System.Collections.Generic;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class SimulationBattle
    {
        public List<SimulationAgent> Participants { get; }
        public WorldDatum Location { get; }

        public SimulationBattle(WorldDatum location)
        {
            Location = location;
            Participants = new List<SimulationAgent>();
        }
    }
}
