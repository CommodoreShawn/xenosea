﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class SerializationObject
    {
        public string Name { get; set; }
        public List<SerializationAttribute> Attributes { get; set; }
        public List<SerializationObject> Children { get; set; }

        public SerializationObject()
        {
            Attributes = new List<SerializationAttribute>();
            Children = new List<SerializationObject>();
        }

        public string GetAttribute(string attributeName)
        {
            return Attributes
                .Where(x => x.Name == attributeName)
                .Select(x => x.Value)
                .FirstOrDefault();
        }

        public SerializationObject SetAttribute(string name, string value)
        {
            var existingAttribute = Attributes
                .Where(x => x.Name == name)
                .FirstOrDefault();

            if(existingAttribute != null)
            {
                if(value == null)
                {
                    Attributes.Remove(existingAttribute);
                }
                else
                {
                    existingAttribute.Value = value;
                }
            }
            else if (value != null)
            {
                Attributes.Add(new SerializationAttribute
                {
                    Name = name,
                    Value = value
                });
            }

            return this;
        }

        public T LookupReference<T>(string attributeName, IEnumerable<T> options, Func<T, string> optionToKey)
        {
            var val = GetAttribute(attributeName);

            if(val != null)
            {
                var match = options
                    .Where(o => optionToKey(o) == val)
                    .FirstOrDefault();

                if(match != null)
                {
                    return match;
                }
                else
                {
                    throw SerializationException.MatchNotFound(this, val, typeof(T), options.Select(o => optionToKey(o)));
                }
            }
            else
            {
                throw SerializationException.AttributeNotFound(this, attributeName);
            }
        }

        public T LookupReferenceOrNull<T>(string attributeName, IEnumerable<T> options, Func<T, string> optionToKey)
        {
            var val = GetAttribute(attributeName);

            if (val != null)
            {
                var match = options
                    .Where(o => optionToKey(o) == val)
                    .FirstOrDefault();

                return match;
            }
            else
            {
                return default(T);
            }
        }

        public void AddList(string listName, IEnumerable<string> values)
        {
            var child = AddChild(listName);
            foreach(var value in values)
            {
                child.AddChild("item").SetAttribute("val", value);
            }
        }

        public void AddDictionary(string dictionaryName, Dictionary<string, string> dictionary)
        {
            var child = AddChild(dictionaryName);
            foreach (var kvp in dictionary)
            {
                child.AddChild("item")
                    .SetAttribute("k", kvp.Key)
                    .SetAttribute("v", kvp.Value);
            }
        }

        public IEnumerable<string> GetList(string listName)
        {
            var child = GetChild(listName);
            if (child != null)
            {
                return child.Children
                    .Select(x => x.GetAttribute("val"));
            }

            return Enumerable.Empty<string>();
        }

        public Dictionary<string, string> GetDictionary(string dictionaryName)
        {
            var dictionary = new Dictionary<string, string>();
            var child = GetChild(dictionaryName);
            foreach (var itemChild in child.Children.Where(x => x.Name == "item"))
            {
                var key = itemChild.GetAttribute("k");
                var value = itemChild.GetAttribute("v");
                dictionary.Add(key, value);
            }

            return dictionary;
        }

        public SerializationObject AddChild(string name)
        {
            var newChild = new SerializationObject()
            {
                Name = name
            };
            Children.Add(newChild);

            return newChild;
        }

        public SerializationObject GetChild(string name)
        {
            var child = Children
                .Where(x => x.Name == name)
                .FirstOrDefault();

            if(child == null)
            {
                throw SerializationException.ChildNotFound(this, name);
            }

            return child;
        }

        public SerializationObject GetChildOrNull(string name)
        {
            var child = Children
                .Where(x => x.Name == name)
                .FirstOrDefault();

            return child;
        }

        public bool HasAttribute(string attributeName)
        {
            return Attributes
                .Where(x => x.Name == attributeName)
                .Any();
        }

        public bool HasChild(string name)
        {
            return Children
                .Where(x => x.Name == name)
                .Any();
        }

        public string GetAttributeOrDefault(string attributeName, string defaultValue)
        {
            return Attributes
                .Where(x => x.Name == attributeName)
                .Select(x => x.Value)
                .FirstOrDefault()
                ?? defaultValue;
        }
    }
}
