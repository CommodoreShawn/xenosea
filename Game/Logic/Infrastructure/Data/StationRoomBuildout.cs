﻿using Godot;
using System;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class StationRoomBuildout
    {
        public Vector3 Position { get; set; }
        public string Name { get; set; }

        public void SerializeTo(SerializationObject buildoutRoot)
        {
            buildoutRoot.SetAttribute("pos_x", Position.x.ToString());
            buildoutRoot.SetAttribute("pos_y", Position.y.ToString());
            buildoutRoot.SetAttribute("pos_z", Position.z.ToString());
            buildoutRoot.SetAttribute("name", Name);
        }

        public static StationRoomBuildout DeserializeFrom(
            SerializationObject buildoutRoot)
        {
            return new StationRoomBuildout
            {
                Position = new Vector3(
                    Convert.ToSingle(buildoutRoot.GetAttribute("pos_x")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("pos_y")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("pos_z"))),
                Name = buildoutRoot.GetAttribute("name")
            };
        }
    }
}
