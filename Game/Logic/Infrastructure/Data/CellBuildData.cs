﻿using Godot.Collections;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class CellBuildData
    {
        public Array FloorSurface { get; set; }
        public Array RoofSurface { get; set; }
    }
}
