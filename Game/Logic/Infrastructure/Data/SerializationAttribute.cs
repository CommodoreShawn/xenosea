﻿namespace Xenosea.Logic.Infrastructure.Data
{
    public class SerializationAttribute
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}