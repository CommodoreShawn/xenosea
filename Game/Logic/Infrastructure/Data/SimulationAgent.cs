﻿using Autofac;
using Godot;
using System;
using System.Linq;
using Xenosea.Logic.AI.SimulationGoals;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class SimulationAgent
    {
        public string Id { get; private set; }
        public string Name { get; private set; }
        private AgentDefinition _agentDefinition;
        public Faction Faction { get; private set; }
        public WorldDatum Location { get; set; }
        public WorldDatum MovingTo { get; set; }
        public float MovementProgress { get; set; }
        public float Speed => _agentDefinition.MovementSpeed;
        public PackedScene IndicatorPrefab => _agentDefinition.IndicatorPrefab;
        public SubLoadout SubmarineLoadout => _agentDefinition.SubLoadout;
        public PackedScene ControllerPrefab => _agentDefinition.ControllerPrefab;
        public ISimulationBotGoal Goal { get; set; }
        public SimulationAgentBehavior Behavior => _agentDefinition.AgentBehavior;
        public SimulationStation HomeStation { get; set; }
        public float CargoCapacity { get; private set; }
        public CommodityStorage CommodityStorage { get; private set; }
        public float RemainingCargoCapacity => CargoCapacity - CommodityStorage.TotalVolume;
        public bool InDetailedSim => DetailedRepresentation != null;
        public float Agility => _agentDefinition.Agility;
        public float Evasion => _agentDefinition.Evasion;
        public float FocusedDamage => _agentDefinition.FocusedDamage;
        public float WideDamage => _agentDefinition.WideDamage;
        public float Armor => _agentDefinition.Armor;
        public float Stealth => _agentDefinition.Stealth;
        public SimulationBattle CurrentBattle { get; set; }
        public float Health { get; set; }
        public float MaxHealth => _agentDefinition.SubLoadout.HullType.Health;
        public float HealthPercent => Health / MaxHealth;
        public float BattleEscapeTimer { get; set; }

        public Submarine DetailedRepresentation { get; set; }

        private double _datumRecheck;

        public SimulationAgent(
            AgentDefinition agentDefinition, 
            SimulationStation homeStation,
            Faction faction, 
            WorldDatum location)
        {
            Id = System.Guid.NewGuid().ToString();
            Name = $"{faction.Name} {agentDefinition.AgentNameRole}";
            _agentDefinition = agentDefinition;
            HomeStation = homeStation;
            Faction = faction;
            Location = location;
            MovingTo = null;
            MovementProgress = 0;
            
            Health = agentDefinition.SubLoadout.HullType.Health;

            var usedVolume = agentDefinition.SubLoadout.ArmorType.VolumePerSurfaceArea * agentDefinition.SubLoadout.HullType.HullSurfaceArea
                + agentDefinition.SubLoadout.GetAllModules().Sum(x => x.Volume);

            CargoCapacity = (agentDefinition.SubLoadout.HullType.Volume - usedVolume) * Constants.VOLUME_TO_CARGO;
            CommodityStorage = new CommodityStorage(CargoCapacity, false);
        }

        public Vector3 RoughPosition
        {
            get
            {
                if(MovingTo == null || MovementProgress == 0)
                {
                    return Location.Position;
                }
                else
                {
                    return Location.Position * (1 - MovementProgress)
                        + MovementProgress * MovingTo.Position;
                }
            }
        }

        public void SimulationUpdate(float delta, Random random)
        {
            if (BattleEscapeTimer > 0)
            {
                BattleEscapeTimer -= delta;
            }

            if(Behavior == null)
            {
                return;
            }

            if(InDetailedSim && DetailedRepresentation != null)
            {
                _datumRecheck -= delta;

                if(_datumRecheck <= 0)
                {
                    _datumRecheck = random.NextDouble() * 6;

                    var nearDatum = Location.Yield()
                        .Concat(Location.Neighbors)
                        .OrderBy(x => (x.Position - DetailedRepresentation.RealPosition).Length())
                        .FirstOrDefault();

                    if(nearDatum != Location)
                    {
                        Location = nearDatum;
                    }
                }
            }

            var newGoalType = Behavior.GoalTypes
                .OrderByDescending(x => x.Priority)
                .Where(x => x.IsValid(this))
                .FirstOrDefault();

            if(Goal != null && !Goal.IsValid)
            {
                AgentGoalDebug($"{Name} Invalid Goal: {Goal?.Description}");
                Goal.OnRemoved();
                Goal = null;
            }

            if (newGoalType != null)
            {
                if (Goal == null
                    || newGoalType.Priority > Goal.Priority)
                {
                    AgentGoalDebug($"{Name} Changing Goal From {Goal?.Description} to {newGoalType.Name}");

                    Goal?.OnRemoved();
                    Goal = newGoalType.CreateGoal(this);
                }
            }

            if(Goal != null)
            {
                Goal.SimulationUpdate(delta);
            }

            if (!InDetailedSim)
            {
                DoSummarySimLogic(delta, random);
            }
            else
            {
                DoDetailedSimLogic(delta, random);
            }
        }

        private void DoSummarySimLogic(float delta, Random random)
        {
            if (MovingTo != null && CurrentBattle == null)
            {
                if (Location != MovingTo)
                {
                    MovementProgress += Speed * delta / Util.EstimateTravelDistance(Location, MovingTo);

                    if (MovementProgress >= 1)
                    {
                        MovementProgress = 0;
                        Location = MovingTo;
                        MovingTo = null;
                    }
                }
                else
                {
                    MovementProgress = 0;
                    MovingTo = null;
                }
            }
        }

        private void DoDetailedSimLogic(float delta, Random random)
        {
            if(DetailedRepresentation != null)
            {
                Health = DetailedRepresentation.Health;
            }
        }

        public void SaveState(SerializationObject agentRoot)
        {
            agentRoot.SetAttribute("id", Id);
            agentRoot.SetAttribute("definition", _agentDefinition.Name);
            agentRoot.SetAttribute("faction_id", Faction.Name);
            agentRoot.SetAttribute("location_id", Location.Id);
            agentRoot.SetAttribute("moving_to_id", MovingTo?.Id);
            agentRoot.SetAttribute("movement_progress", MovementProgress.ToString());
            agentRoot.SetAttribute("home_station", HomeStation?.Name);
            agentRoot.SetAttribute("health", Health.ToString());
            agentRoot.SetAttribute("battle_escape_timer", BattleEscapeTimer.ToString());

            CommodityStorage.SaveState(agentRoot.AddChild("commodity_storage"));

            if(Goal != null)
            {
                Goal.SaveState(agentRoot.AddChild("goal"));
            }
        }

        public static SimulationAgent LoadState(
            IResourceLocator resourceLocator, 
            IWorldManager worldManager, 
            SerializationObject agentRoot)
        {
            SimulationAgent agent = null;

            try
            {
                agent = new SimulationAgent(
                    resourceLocator.GetAgentDefinition(agentRoot.GetAttribute("definition")),
                    agentRoot.LookupReference("home_station", worldManager.SimulationStations, s => s.Name),
                    agentRoot.LookupReference("faction_id", worldManager.Factions, s => s.Name),
                    agentRoot.LookupReference("location_id", worldManager.WorldDatums, s => s.Id))
                {
                    Id = agentRoot.GetAttribute("id"),
                    MovingTo = agentRoot.LookupReferenceOrNull("moving_to_id", worldManager.WorldDatums, s => s.Id),
                    MovementProgress = Convert.ToSingle(agentRoot.GetAttribute("movement_progress")),
                    Health = Convert.ToSingle(agentRoot.GetAttribute("health")),
                    BattleEscapeTimer = Convert.ToSingle(agentRoot.GetAttribute("battle_escape_timer")),
                };

                agent.CommodityStorage.LoadState(resourceLocator, agentRoot.GetChild("commodity_storage"));

                var goalRoot = agentRoot.GetChild("goal");

                if (goalRoot != null)
                {
                    agent.Goal = agent._agentDefinition.AgentBehavior.LoadGoal(worldManager, resourceLocator, goalRoot, agent);
                }
            }
            catch(Exception ex)
            {
                IocManager.IocContainer.Resolve<ILogger>().Error(ex, "SimulationAgent.LoadState");
            }

            return agent;
        }

        private void AgentGoalDebug(string message)
        {
            if(Constants.DEBUG_AGENT_GOALS)
            {
                GD.Print(message);
            }
        }
    }
}
