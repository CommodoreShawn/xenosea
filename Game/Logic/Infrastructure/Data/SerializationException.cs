﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class SerializationException : Exception
    {
        public SerializationException(string message)
            :base(message)
        {
        }

        public static SerializationException AttributeNotFound(SerializationObject serializationObject, string attributeName)
        {
            return new SerializationException(
                $"Expected attribute '{attributeName}' on object '{serializationObject.Name}', but was not found. Attributes on object: {string.Join(", ", serializationObject.Attributes.Select(a => a.Name))}");
        }

        public static SerializationException MatchNotFound(SerializationObject serializationObject, string attributeName, Type type, IEnumerable<string> options)
        {
            return new SerializationException(
                $"Expected {type.Name} matching '{attributeName}', but no match was found in: {string.Join(", ", options)}");
        }

        public static SerializationException ChildNotFound(SerializationObject serializationObject, string childName)
        {
            return new SerializationException(
                $"Expected child '{childName}' on object '{serializationObject.Name}', but was not found. Children on object: {string.Join(", ", serializationObject.Children.Select(a => a.Name))}");
        }
    }
}
