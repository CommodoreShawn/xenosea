﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class SimulationStation
    {
        public WorldDatum Location { get; private set; }
        public CommodityStorage CommodityStorage { get; private set; }
        public StationBuildout StationBuildout { get; private set; }
        public string ReferenceId {get;  private set;}
        public string Name { get; private set; }
        public Faction Faction { get; set; }
        public List<Faction> FactionsPresent { get; set; }
        public int CommodityCapacity { get; private set; }
        public float InfluenceStrength => EconomicProcesses.Sum(x => x.InfluenceStrength);
        public AbstractEconomicProcess[] EconomicProcesses { get; private set; }
        private float[] _economicProcessCounter;
        
        public CommodityType[] Imports { get; }
        public CommodityType[] Exports { get; }

        public AgentDefinition[] SupportedAgentDefinitions { get; private set; }
        public float[] AgentRebuildCounters { get; private set; }
        public SimulationAgent[] SupportedAgents { get; private set; }

        public List<List<WorldDatum>> PerimiterPatrols { get; private set; }

        public WorldStation DetailedRepresentation { get; set; }

        private bool _commoditySeeded;

        private readonly IWorldManager _worldManager;

        public float TradeDistance { get; private set; }

        private SimulationStation(
            IWorldManager worldManager,
            WorldDatum location,
            StationBuildout stationBuildout,
            AbstractEconomicProcess[] economicProcesses,
            int commodityCapacity,
            AgentDefinition[] supportedAgentDefinitions,
            string name,
            Faction faction,
            List<Faction> factionsPresent,
            float tradeDistance)
        {
            StationBuildout = stationBuildout;
            CommodityCapacity = commodityCapacity;
            EconomicProcesses = economicProcesses;
            SupportedAgentDefinitions = supportedAgentDefinitions;
            TradeDistance = tradeDistance;
            FactionsPresent = factionsPresent;

            Name = name;
            ReferenceId = name;
            Faction = faction;

            _worldManager = worldManager;
            Location = location;
            CommodityStorage = new CommodityStorage(CommodityCapacity, true);
            _economicProcessCounter = new float[EconomicProcesses.Length];
            AgentRebuildCounters = new float[SupportedAgentDefinitions.Length];
            SupportedAgents = new SimulationAgent[SupportedAgentDefinitions.Length];
            PerimiterPatrols = new List<List<WorldDatum>>();

            var allInputs = EconomicProcesses
                .SelectMany(x => x.InputsForCycle(this))
                .ToArray();

            var allOutputs = EconomicProcesses
                .SelectMany(x => x.OutputsForCycle(this))
                .ToArray();

            var allCommodities = allInputs.Select(t => t.Item2)
                .Concat(allOutputs.Select(t => t.Item2))
                .Distinct()
                .ToArray();

            Imports = allCommodities
                .Where(c => allInputs.Where(x => x.Item2 == c).Sum(x => x.Item1) > allOutputs.Where(x => x.Item2 == c).Sum(x => x.Item1))
                .ToArray();

            Exports = allCommodities
                .Where(c => allInputs.Where(x => x.Item2 == c).Sum(x => x.Item1) < allOutputs.Where(x => x.Item2 == c).Sum(x => x.Item1))
                .ToArray();
        }

        public void SimulationUpdate(float delta)
        {
            for(var i = 0; i < SupportedAgents.Length; i++)
            {
                if(SupportedAgents[i] != null)
                {
                    AgentRebuildCounters[i] = 0;

                    if (SupportedAgents[i].Health <= 0)
                    {
                        SupportedAgents[i] = null;
                    }
                }
                else
                {
                    if(AgentRebuildCounters[i] >= SupportedAgentDefinitions[i].RebuildCost)
                    {
                        AgentRebuildCounters[i] = 0;

                        //GD.Print($"Rebuilding {SupportedAgentDefinitions[i].Name} at {Name}");

                        var agentOwner = Faction;
                        if (SupportedAgentDefinitions[i].IsSecurity)
                        {
                            agentOwner = Faction.SecuritySubfaction ?? Faction;
                        }
                        else if (SupportedAgentDefinitions[i].IsTrader)
                        {
                            agentOwner = Faction.TradeSubfaction ?? Faction;
                        }

                        var newAgent = new SimulationAgent(
                            SupportedAgentDefinitions[i],
                            this,
                            agentOwner,
                            Location);

                        SupportedAgents[i] = newAgent;
                        _worldManager.AddSimulationAgent(newAgent);
                    }
                }
            }

            if(!_commoditySeeded)
            {
                _commoditySeeded = true;
                foreach (var import in Imports)
                {
                    CommodityStorage.Add(import, CommodityCapacity * 0.4f);
                }
                foreach (var export in Exports.Except(Imports))
                {
                    CommodityStorage.Add(export, CommodityCapacity * 0.6f);
                }
            }

            UpdateEconomicProcesses(delta);
        }

        private void UpdateEconomicProcesses(float delta)
        {
            for (var i = 0; i < EconomicProcesses.Length; i++)
            {
                var process = EconomicProcesses[i];

                _economicProcessCounter[i] += delta;

                if (_economicProcessCounter[i] > process.ProcessTime)
                {
                    _economicProcessCounter[i] = 0;

                    var outputFactor = 1f;
                    var inputFactor = 1f;

                    var processInputs = process.InputsForCycle(this).ToArray();
                    var processOutputs = process.OutputsForCycle(this).ToArray();

                    for (var j = 0; j < processInputs.Length; j++)
                    {
                        var input = processInputs[j].Item2;
                        var stockpilePerc = CommodityStorage.GetQty(input) / (float)CommodityCapacity;

                        var consumptionRateForInput = Mathf.Min(1, stockpilePerc / 0.5f);
                        var productionRateForInput = Mathf.Min(0.7f, stockpilePerc / 0.5f) + 0.3f;

                        outputFactor = Mathf.Min(outputFactor, productionRateForInput);
                        inputFactor = Mathf.Min(inputFactor, consumptionRateForInput);
                    }

                    for (var j = 0; j < processInputs.Length; j++)
                    {
                        var input = processInputs[j].Item2;
                        var toUse = Mathf.Min(CommodityStorage.GetQty(input), processInputs[j].Item1 * inputFactor);
                        CommodityStorage.Remove(input, toUse);
                    }

                    for (var j = 0; j < processOutputs.Length; j++)
                    {
                        var output = processOutputs[j].Item2;
                        var toAdd = Mathf.Min(CommodityCapacity - CommodityStorage.GetQty(output), processOutputs[j].Item1 * outputFactor);
                        CommodityStorage.Add(output, toAdd);
                    }

                    if(process.AgentSupport > 0 && SupportedAgents.Length > 0)
                    {
                        var supportPerSlot = process.AgentSupport * outputFactor / SupportedAgents.Length;

                        for(var j = 0; j < SupportedAgents.Length; j++)
                        {
                            if(SupportedAgents[j] == null)
                            {
                                AgentRebuildCounters[j] += supportPerSlot;
                            }
                        }
                    }
                }
            }
        }

        public float PriceOf(CommodityType commodity)
        {
            var stockpilePerc = CommodityStorage.GetQty(commodity) / (float)CommodityCapacity;

            return commodity.BaseValue * (1 - stockpilePerc + 0.5f);
        }

        public void SaveState(SerializationObject stationRoot)
        {
            stationRoot.SetAttribute("location_id", Location.Id);
            CommodityStorage.SaveState(stationRoot.AddChild("commodity_storage"));
            StationBuildout.SerializeTo(stationRoot.AddChild("buildout"));
            stationRoot.SetAttribute("name", Name);
            stationRoot.SetAttribute("faction", Faction.Name);
            stationRoot.AddList("factions_present", FactionsPresent.Select(f => f.Name));
            stationRoot.AddList("supported_agent_definitions", SupportedAgentDefinitions.Select(x => x.Name));
            stationRoot.SetAttribute("commodity_capacity", CommodityCapacity.ToString());
            for (var i = 0; i < EconomicProcesses.Length; i++)
            {
                var procChild = stationRoot.AddChild("economic_process");
                procChild.SetAttribute("name", EconomicProcesses[i].Name);
                procChild.SetAttribute("counter", _economicProcessCounter[i].ToString());
            }
            stationRoot.SetAttribute("trade_distance", TradeDistance.ToString());
            for (var i = 0; i < SupportedAgents.Length;i++)
            {
                var agentChild = stationRoot.AddChild("agent_support");
                agentChild.SetAttribute("rebuild", AgentRebuildCounters[i].ToString());
                agentChild.SetAttribute("agent_id", SupportedAgents[i]?.Id?.ToString());
            }
        }

        public void LoadSupportedAgentState(IWorldManager worldManager, SerializationObject stationRoot)
        {
            var supportedAgentChildren = stationRoot.Children
                .Where(x => x.Name == "agent_support")
                .ToArray();

            for (var i = 0; i < SupportedAgents.Length; i++)
            {
                if(i < supportedAgentChildren.Length)
                {
                    AgentRebuildCounters[i] = Convert.ToSingle(supportedAgentChildren[i].GetAttribute("rebuild"));
                    SupportedAgents[i] = supportedAgentChildren[i].LookupReferenceOrNull(
                        "agent_id",
                        worldManager.SimulationAgents,
                        a => a.Id);
                }
            }
        }

        public static SimulationStation BuildStation(
            IWorldManager worldManager,
            IStationGenerator stationGenerator,
            WorldDatum location,
            StationDefinition stationDefinition)
        {
            var buildout = stationGenerator.GenerateStation(
                location,
                stationDefinition);

            return new SimulationStation(
                worldManager,
                location,
                buildout,
                stationDefinition.EconomicProcesses,
                stationDefinition.CommodityCapacity,
                stationDefinition.SupportedAgents,
                stationDefinition.Name,
                stationDefinition.Faction,
                stationDefinition.FactionsPresent.ToList(),
                stationDefinition.TradeDistance);
        }


        public static SimulationStation LoadState(
            IResourceLocator resourceLocator, 
            IWorldManager worldManager,
            SerializationObject stationRoot)
        {
            var datumId = stationRoot.GetAttribute("location_id");

            var location = worldManager
                .WorldDatums
                .Where(d => d.Id == datumId)
                .FirstOrDefault();

            var stationBuildout = StationBuildout.Deserialize(
                resourceLocator,
                stationRoot.GetChild("buildout"));

            var economicProcessChildren = stationRoot.Children
                .Where(c => c.Name == "economic_process")
                .ToArray();

            var economicProcesses = economicProcessChildren
                .Select(x => resourceLocator.GetEconomicProcess(x.GetAttribute("name")))
                .ToArray();

            var supportedAgentDefinitions = stationRoot
                .GetList("supported_agent_definitions")
                .Select(x => resourceLocator.GetAgentDefinition(x))
                .ToArray();

            var factionsPresent = stationRoot
                .GetList("factions_present")
                .Select(x => resourceLocator.GetFaction(x))
                .ToList();

            var station = new SimulationStation(
                worldManager,
                location,
                stationBuildout,
                economicProcesses,
                Convert.ToInt32(stationRoot.GetAttribute("commodity_capacity")),
                supportedAgentDefinitions,
                stationRoot.GetAttribute("name"),
                resourceLocator.GetFaction(stationRoot.GetAttribute("faction")),
                factionsPresent,
                Convert.ToSingle(stationRoot.GetAttribute("trade_distance")));

            station.CommodityStorage.LoadState(resourceLocator, stationRoot.GetChild("commodity_storage"));

            for (var i = 0; i < station.EconomicProcesses.Length; i++)
            {
                if (i < economicProcessChildren.Length)
                {
                    station._economicProcessCounter[i] = Convert.ToSingle(economicProcessChildren[i].GetAttribute("counter"));
                }
            }

            return station;
        }
    }
}
