﻿using Godot;
using System;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class DockingTowerBuildout
    {
        public Vector3 FoundationPosition { get; set; }
        public Vector3 AttachedTowerPosition { get; set; }

        public void SerializeTo(SerializationObject buildoutRoot)
        {
            buildoutRoot.SetAttribute("from_x", FoundationPosition.x.ToString());
            buildoutRoot.SetAttribute("from_y", FoundationPosition.y.ToString());
            buildoutRoot.SetAttribute("from_z", FoundationPosition.z.ToString());

            buildoutRoot.SetAttribute("to_x", AttachedTowerPosition.x.ToString());
            buildoutRoot.SetAttribute("to_y", AttachedTowerPosition.y.ToString());
            buildoutRoot.SetAttribute("to_z", AttachedTowerPosition.z.ToString());
        }

        public static DockingTowerBuildout Deserialize(
            SerializationObject buildoutRoot)
        {
            return new DockingTowerBuildout
            {
                FoundationPosition = new Vector3(
                    Convert.ToSingle(buildoutRoot.GetAttribute("from_x")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("from_y")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("from_z"))),
                AttachedTowerPosition = new Vector3(
                    Convert.ToSingle(buildoutRoot.GetAttribute("to_x")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("to_y")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("to_z")))
            };
        }
    }
}
