﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class StationBuildout
    {
        public Vector3 CenterPosition { get; set; }
        public List<TowerBuildout> Towers{ get; set; }
        public List<DockingTowerBuildout> DockingTowers { get; set; }
        public List<DefenseTowerBuildout> DefenseTowers { get; set; }
        public List<FarmPlotBuildout> FarmPlots { get; set; }
        public Vector3 DockWaitingArea { get; set; }
        public StationModuleSet StationModuleSet { get; set; }
        public StationRoomBuildout Bar { get; set; }
        public StationRoomBuildout EquipmentVendor { get; set; }
        public StationRoomBuildout Warehouse { get; set; }
        public StationRoomBuildout SecurityOffice { get; set; }
        public StationRoomBuildout BountyOffice { get; set; }
        public Vector3 CameraCenter { get; set; }
        public float CameraDistance { get; set; }

        public void SerializeTo(SerializationObject buildoutRoot)
        {
            buildoutRoot.SetAttribute("center_x", CenterPosition.x.ToString());
            buildoutRoot.SetAttribute("center_y", CenterPosition.y.ToString());
            buildoutRoot.SetAttribute("center_z", CenterPosition.z.ToString());
            foreach(var tower in Towers)
            {
                tower.SerializeTo(buildoutRoot.AddChild("tower"));
            }
            foreach (var tower in DockingTowers)
            {
                tower.SerializeTo(buildoutRoot.AddChild("docking_tower"));
            }
            foreach (var tower in DefenseTowers)
            {
                tower.SerializeTo(buildoutRoot.AddChild("defense_tower"));
            }
            foreach (var plot in FarmPlots)
            {
                plot.SerializeTo(buildoutRoot.AddChild("farm_plot"));
            }
            buildoutRoot.SetAttribute("dock_x", DockWaitingArea.x.ToString());
            buildoutRoot.SetAttribute("dock_y", DockWaitingArea.y.ToString());
            buildoutRoot.SetAttribute("dock_z", DockWaitingArea.z.ToString());
            buildoutRoot.SetAttribute("module_set", StationModuleSet.Name);
            if(Bar != null)
            {
                Bar.SerializeTo(buildoutRoot.AddChild("room_bar"));
            }
            if (EquipmentVendor != null)
            {
                EquipmentVendor.SerializeTo(buildoutRoot.AddChild("room_equipment"));
            }
            if (Warehouse != null)
            {
                Warehouse.SerializeTo(buildoutRoot.AddChild("room_warehouse"));
            }
            if (SecurityOffice != null)
            {
                SecurityOffice.SerializeTo(buildoutRoot.AddChild("room_security"));
            }
            if (BountyOffice != null)
            {
                BountyOffice.SerializeTo(buildoutRoot.AddChild("room_bounty"));
            }
            buildoutRoot.SetAttribute("camera_x", CameraCenter.x.ToString());
            buildoutRoot.SetAttribute("camera_y", CameraCenter.y.ToString());
            buildoutRoot.SetAttribute("camera_z", CameraCenter.z.ToString());
            buildoutRoot.SetAttribute("camera_dist", CameraDistance.ToString());
        }

        public static StationBuildout Deserialize(
            IResourceLocator resourceLocator, 
            SerializationObject buildoutRoot)
        {
            var stationBuildout = new StationBuildout();

            stationBuildout.CenterPosition = new Vector3(
                Convert.ToSingle(buildoutRoot.GetAttribute("center_x")),
                Convert.ToSingle(buildoutRoot.GetAttribute("center_y")),
                Convert.ToSingle(buildoutRoot.GetAttribute("center_z")));

            stationBuildout.Towers = buildoutRoot.Children
                .Where(c => c.Name == "tower")
                .Select(c => TowerBuildout.Deserialize(
                    resourceLocator,
                    c))
                .ToList();

            stationBuildout.DockingTowers = buildoutRoot.Children
                .Where(c => c.Name == "docking_tower")
                .Select(c => DockingTowerBuildout.Deserialize(c))
                .ToList();

            stationBuildout.DefenseTowers = buildoutRoot.Children
                .Where(c => c.Name == "defense_tower")
                .Select(c => DefenseTowerBuildout.Deserialize(c))
                .ToList();

            stationBuildout.FarmPlots = buildoutRoot.Children
                .Where(c => c.Name == "farm_plot")
                .Select(c => FarmPlotBuildout.Deserialize(c))
                .ToList();

            stationBuildout.DockWaitingArea = new Vector3(
                Convert.ToSingle(buildoutRoot.GetAttribute("dock_x")),
                Convert.ToSingle(buildoutRoot.GetAttribute("dock_y")),
                Convert.ToSingle(buildoutRoot.GetAttribute("dock_z")));

            stationBuildout.StationModuleSet = resourceLocator.GetStationModuleSet(buildoutRoot.GetAttribute("module_set"));

            if(buildoutRoot.HasChild("room_bar"))
            {
                stationBuildout.Bar = StationRoomBuildout.DeserializeFrom(
                    buildoutRoot.GetChild("room_bar"));
            }
            if (buildoutRoot.HasChild("room_equipment"))
            {
                stationBuildout.EquipmentVendor = StationRoomBuildout.DeserializeFrom(
                    buildoutRoot.GetChild("room_equipment"));
            }
            if (buildoutRoot.HasChild("room_bar"))
            {
                stationBuildout.Bar = StationRoomBuildout.DeserializeFrom(
                    buildoutRoot.GetChild("room_bar"));
            }
            if (buildoutRoot.HasChild("room_warehouse"))
            {
                stationBuildout.Warehouse = StationRoomBuildout.DeserializeFrom(
                    buildoutRoot.GetChild("room_warehouse"));
            }
            if (buildoutRoot.HasChild("room_security"))
            {
                stationBuildout.SecurityOffice = StationRoomBuildout.DeserializeFrom(
                    buildoutRoot.GetChild("room_security"));
            }
            if (buildoutRoot.HasChild("room_bounty"))
            {
                stationBuildout.BountyOffice = StationRoomBuildout.DeserializeFrom(
                    buildoutRoot.GetChild("room_bounty"));
            }

            stationBuildout.CameraCenter = new Vector3(
                Convert.ToSingle(buildoutRoot.GetAttribute("camera_x")),
                Convert.ToSingle(buildoutRoot.GetAttribute("camera_y")),
                Convert.ToSingle(buildoutRoot.GetAttribute("camera_z")));

            stationBuildout.CameraDistance = Convert.ToSingle(buildoutRoot.GetAttribute("camera_dist"));

            return stationBuildout;
        }
    }
}
