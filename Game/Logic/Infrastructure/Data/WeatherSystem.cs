﻿using Godot;
using System;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class WeatherSystem
    {
        public Vector3 Position { get; set; }
        public float Energy { get; set; }
        public float MaxEnergy { get; set; }
        public float Age { get; set; }
        public float Radius => Energy > 0 ? Mathf.Sqrt(Energy) * 5000 : 0;
        public WorldDatum NearestDatum { get; set; }

        public WeatherSystem(
            Vector3 position, 
            float maxEnergy)
        {
            Position = position;
            MaxEnergy = maxEnergy;
            Energy = maxEnergy;
            Age = 0;
        }

        public void SaveState(SerializationObject weatherRoot)
        {
            weatherRoot.SetAttribute("pos_x", Position.x.ToString());
            weatherRoot.SetAttribute("pos_y", Position.y.ToString());
            weatherRoot.SetAttribute("pos_z", Position.z.ToString());
            weatherRoot.SetAttribute("energy", Energy.ToString());
            weatherRoot.SetAttribute("max_energy", MaxEnergy.ToString());
            weatherRoot.SetAttribute("age", Age.ToString());
        }

        public static WeatherSystem LoadState(SerializationObject weatherRoot)
        {
            return new WeatherSystem(
                new Vector3(
                    Convert.ToSingle(weatherRoot.GetAttribute("pos_x")),
                    Convert.ToSingle(weatherRoot.GetAttribute("pos_y")),
                    Convert.ToSingle(weatherRoot.GetAttribute("pos_z"))),
                Convert.ToSingle(weatherRoot.GetAttribute("max_energy")))
            {
                Energy = Convert.ToSingle(weatherRoot.GetAttribute("energy")),
                Age = Convert.ToSingle(weatherRoot.GetAttribute("age"))
            };
        }
    }
}
