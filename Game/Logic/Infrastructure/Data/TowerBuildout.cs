﻿using Godot;
using System;
using System.Linq;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class TowerBuildout
    {
        public Vector3 FoundationPosition { get; set; }
        public AbstractEconomicProcess[] Sections { get; set; }
        public BridgeBuildout[] Bridges { get; set; }

        public void SerializeTo(SerializationObject buildoutRoot)
        {
            buildoutRoot.SetAttribute("foundation_x", FoundationPosition.x.ToString());
            buildoutRoot.SetAttribute("foundation_y", FoundationPosition.y.ToString());
            buildoutRoot.SetAttribute("foundation_z", FoundationPosition.z.ToString());
            foreach(var section in Sections)
            {
                buildoutRoot
                    .AddChild("section")
                    .SetAttribute("process", section.Name);
            }
            foreach (var bridge in Bridges)
            {
                bridge.SerializeTo(buildoutRoot.AddChild("bridge"));
            }
        }

        public static TowerBuildout Deserialize(
            IResourceLocator resourceLocator, 
            SerializationObject buildoutRoot)
        {
            return new TowerBuildout
            {
                FoundationPosition = new Vector3(
                    Convert.ToSingle(buildoutRoot.GetAttribute("foundation_x")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("foundation_y")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("foundation_z"))),
                Sections = buildoutRoot.Children
                    .Where(c => c.Name == "section")
                    .Select(c => resourceLocator.GetEconomicProcess(c.GetAttribute("process")))
                    .ToArray(),
                Bridges = buildoutRoot.Children
                    .Where(c => c.Name == "bridge")
                    .Select(c => BridgeBuildout.Deserialize(c))
                    .ToArray()
            };
        }
    }
}
