﻿using Godot;
using System;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class KnownPointOfInterest : IMapObject
    {
        public ContactType ContactType { get; set; }
        public string FriendlyName { get; set; }
        public string ReferenceId { get; set; }
        public WorldDatum Location { get; set; }
        public Vector3? PreciseLocation { get; set; }
        public Vector3 EstimatedPosition => PreciseLocation ?? Util.GetNavigationCenter(Location);
        public bool TypeKnown => true;
        public bool Identified => true;

        public void SaveState(SerializationObject poiRoot)
        {
            poiRoot.SetAttribute("contact_type", ContactType.ToString());
            poiRoot.SetAttribute("name", FriendlyName.ToString());
            poiRoot.SetAttribute("location", Location.Id.ToString());
            poiRoot.SetAttribute("reference_id", ReferenceId.ToString());
            if(PreciseLocation.HasValue)
            {
                poiRoot.SetAttribute("precise_x", PreciseLocation.Value.x.ToString());
                poiRoot.SetAttribute("precise_y", PreciseLocation.Value.y.ToString());
                poiRoot.SetAttribute("precise_z", PreciseLocation.Value.z.ToString());
            }
        }

        public static KnownPointOfInterest LoadState(
            IWorldManager worldManager, 
            SerializationObject poiRoot)
        {
            var poi = new KnownPointOfInterest
            {
                ContactType = (ContactType)Enum.Parse(typeof(ContactType), poiRoot.GetAttribute("contact_type")),
                Location = poiRoot.LookupReference("location", worldManager.WorldDatums, x => x.Id),
                FriendlyName = poiRoot.GetAttribute("name"),
                ReferenceId = poiRoot.GetAttribute("reference_id")
            };

            if(poiRoot.HasAttribute("precise_x"))
            {
                poi.PreciseLocation = new Vector3(
                    Convert.ToSingle(poiRoot.GetAttribute("precise_x")),
                    Convert.ToSingle(poiRoot.GetAttribute("precise_y")),
                    Convert.ToSingle(poiRoot.GetAttribute("precise_z")));
            }    

            return poi;
        }

        public bool IsHostileTo(ISonarContact other)
        {
            return false;
        }

        public bool IsFriendlyTo(ISonarContact other)
        {
            return false;
        }
    }
}
