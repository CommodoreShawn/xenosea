﻿using System;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class SaveListItem
    {
        public string Name { get; set; }
        public DateTime LastPlayed { get; set; }
        public bool IsNewSave { get; set; }
    }
}
