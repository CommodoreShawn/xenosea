﻿using Godot;
using System.Collections.Generic;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class WorldDatum
    {
        public string Id { get; }
        public float Latitude { get; }
        public float Longitude { get; }
        public Vector3 Position { get; }
        public float Depth { get; set; }
        public float Light { get; set; }
        public float IceDepth { get; set; }
        public float Volcanism { get; set; }
        public float Fertility { get; set; }
        public float[] OreAbundance { get; set; }
        public Biome Biome { get; set; }
        public List<WorldDatum> Neighbors { get; }
        public bool InDetailedSim { get; set; }
        public SimulationStation SimulationStation { get; set; }
        public Dictionary<Faction, float> FactionInfluence { get;  }
        public List<SimulationStation> InfluenceSources { get; }
        public Vector3 CurrentFlow { get; set; }
        public bool IsExplored { get; set; }
        public WreckDefinition LocalWreck { get; set; }
        public List<SeafloorObject> SeafloorObjects { get; }

        public float IronOutput =>
            1 * OreAbundance[(int)OreType.Hematite]
            + 0.1f * OreAbundance[(int)OreType.Bornite]
            + 0.3f * OreAbundance[(int)OreType.Lazulite]
            + 0.3f * OreAbundance[(int)OreType.Pyrite];

        public float CopperOutput => 0.5f * OreAbundance[(int)OreType.Bornite];

        public float LeadOutput =>
            0.5f * OreAbundance[(int)OreType.Galena]
            + 0.3f * OreAbundance[(int)OreType.Uranite];

        public float NickelOutput =>
            0.5f * OreAbundance[(int)OreType.Garnierite];

        public float ThoriumOutput =>
            0.6f * OreAbundance[(int)OreType.Uranite];

        public float PhosphorusOutput =>
            0.6f * OreAbundance[(int)OreType.Langbeinite]
            + 0.6f * OreAbundance[(int)OreType.Lazulite];

        public float MagnesiumOutput =>
            0.5f * OreAbundance[(int)OreType.Garnierite]
            + 0.4f * OreAbundance[(int)OreType.Langbeinite];

        public float SulfurOutput =>
            0.5f * OreAbundance[(int)OreType.Galena]
            + 0.4f * OreAbundance[(int)OreType.Bornite]
            + 0.6f * OreAbundance[(int)OreType.Pyrite];

        public WorldDatum(
            float latitude,
            float longitude,
            Vector3 position)
        {
            Id = $"{latitude}|{longitude}".Replace(".", ";").Replace(",", ";");
            Latitude = latitude;
            Longitude = longitude;
            Position = position;
            Neighbors = new List<WorldDatum>();
            FactionInfluence = new Dictionary<Faction, float>();
            InfluenceSources = new List<SimulationStation>();
            SeafloorObjects = new List<SeafloorObject>();
        }
        
        public double DistanceTo(WorldDatum location)
        {
            return (Position - location.Position).Length();
        } 
    }
}
