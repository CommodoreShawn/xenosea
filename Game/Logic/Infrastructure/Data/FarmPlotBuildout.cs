﻿using Godot;
using System;
using System.Linq;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class FarmPlotBuildout
    {
        public Vector3 CenterPosition { get; set; }
        public Vector3[] PlantPositions { get; set; }

        public void SerializeTo(SerializationObject buildoutRoot)
        {
            buildoutRoot.SetAttribute("position_x", CenterPosition.x.ToString());
            buildoutRoot.SetAttribute("position_y", CenterPosition.y.ToString());
            buildoutRoot.SetAttribute("position_z", CenterPosition.z.ToString());

            foreach(var plantPosition in PlantPositions)
            {
                var plantPos = buildoutRoot.AddChild("plant");
                plantPos.SetAttribute("x", plantPosition.x.ToString());
                plantPos.SetAttribute("y", plantPosition.y.ToString());
                plantPos.SetAttribute("z", plantPosition.z.ToString());
            }
        }

        public static FarmPlotBuildout Deserialize(
            SerializationObject buildoutRoot)
        {
            return new FarmPlotBuildout
            {
                CenterPosition = new Vector3(
                    Convert.ToSingle(buildoutRoot.GetAttribute("position_x")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("position_y")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("position_z"))),
                PlantPositions = buildoutRoot.Children
                    .Where(c => c.Name == "plant")
                    .Select(c => new Vector3(
                        Convert.ToSingle(c.GetAttribute("x")),
                        Convert.ToSingle(c.GetAttribute("y")),
                        Convert.ToSingle(c.GetAttribute("z"))))
                    .ToArray()
            };
        }
    }
}
