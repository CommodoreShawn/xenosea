﻿namespace Xenosea.Logic.Infrastructure.Data
{
    public enum Biome
    {
        Ice,
        Plains,
        Dunes,
        Forest,
        Reef,
        Smokers
    }
}