﻿using Godot;
using System;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class BridgeBuildout
    {
        public Vector3 From { get; set; }
        public Vector3 To { get; set; }

        public void SerializeTo(SerializationObject buildoutRoot)
        {
            buildoutRoot.SetAttribute("from_x", From.x.ToString());
            buildoutRoot.SetAttribute("from_y", From.y.ToString());
            buildoutRoot.SetAttribute("from_z", From.z.ToString());

            buildoutRoot.SetAttribute("to_x", To.x.ToString());
            buildoutRoot.SetAttribute("to_y", To.y.ToString());
            buildoutRoot.SetAttribute("to_z", To.z.ToString());
        }

        public static BridgeBuildout Deserialize( 
            SerializationObject buildoutRoot)
        {
            return new BridgeBuildout
            {
                From = new Vector3(
                    Convert.ToSingle(buildoutRoot.GetAttribute("from_x")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("from_y")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("from_z"))),
                To = new Vector3(
                    Convert.ToSingle(buildoutRoot.GetAttribute("to_x")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("to_y")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("to_z"))),
            };
        }
    }
}
