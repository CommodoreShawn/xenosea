﻿using System;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class SaveFileSummary
    {
        public string Name { get; set; }
        public DateTime LastPlayed { get; set; }
        public DateTime InGameDate { get; set; }
        public double Money { get; set; }
        public string SubClass { get; set; }
        public string LocationName { get; set; }
    }
}
