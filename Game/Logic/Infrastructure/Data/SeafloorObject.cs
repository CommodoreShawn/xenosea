﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class SeafloorObject
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public bool IsDead { get; set; }
        public WorldDatum Datum { get; set; }
        public Vector3 Position { get; set; }
        public Dictionary<string, string> Data { get; set; }

        public void SerializeTo(SerializationObject seafloorObjectRoot)
        {
            seafloorObjectRoot.SetAttribute("id", Id);
            seafloorObjectRoot.SetAttribute("type", Type);
            seafloorObjectRoot.SetAttribute("is_dead", IsDead.ToString());
            seafloorObjectRoot.SetAttribute("datum_id", Datum.Id);
            seafloorObjectRoot.SetAttribute("pos_x", Position.x.ToString());
            seafloorObjectRoot.SetAttribute("pos_y", Position.y.ToString());
            seafloorObjectRoot.SetAttribute("pos_z", Position.z.ToString());
            seafloorObjectRoot.AddDictionary("data", Data);
        }

        public static SeafloorObject DeserializeFrom(
            List<WorldDatum> worldDatums,
            SerializationObject seafloorObjectRoot)
        {
            var datumId = seafloorObjectRoot.GetAttribute("datum_id");

            var datum = worldDatums
                .Where(x => x.Id == datumId)
                .FirstOrDefault();

            if (datum != null)
            {
                return new SeafloorObject
                {
                    Id = seafloorObjectRoot.GetAttribute("id"),
                    Type = seafloorObjectRoot.GetAttribute("type"),
                    IsDead = Convert.ToBoolean(seafloorObjectRoot.GetAttribute("is_dead")),
                    Datum = datum,
                    Position = new Vector3(
                        Convert.ToSingle(seafloorObjectRoot.GetAttribute("pos_x")),
                        Convert.ToSingle(seafloorObjectRoot.GetAttribute("pos_y")),
                        Convert.ToSingle(seafloorObjectRoot.GetAttribute("pos_z"))),
                    Data = seafloorObjectRoot.GetDictionary("data")
                };
            }

            return null;
        }
    }
}
