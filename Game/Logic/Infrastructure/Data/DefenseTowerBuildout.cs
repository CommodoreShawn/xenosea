﻿using Godot;
using System;

namespace Xenosea.Logic.Infrastructure.Data
{
    public class DefenseTowerBuildout
    {
        public Vector3 FoundationPosition { get; set; }

        public void SerializeTo(SerializationObject buildoutRoot)
        {
            buildoutRoot.SetAttribute("foundation_x", FoundationPosition.x.ToString());
            buildoutRoot.SetAttribute("foundation_y", FoundationPosition.y.ToString());
            buildoutRoot.SetAttribute("foundation_z", FoundationPosition.z.ToString());
        }

        public static DefenseTowerBuildout Deserialize(
            SerializationObject buildoutRoot)
        {
            return new DefenseTowerBuildout
            {
                FoundationPosition = new Vector3(
                    Convert.ToSingle(buildoutRoot.GetAttribute("foundation_x")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("foundation_y")),
                    Convert.ToSingle(buildoutRoot.GetAttribute("foundation_z")))
            };
        }
    }
}
