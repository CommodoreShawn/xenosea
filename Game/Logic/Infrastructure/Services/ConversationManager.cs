﻿using System.Collections.Generic;
using System.Linq;
using Xenosea.addons.XenoTalk.Data;
using Xenosea.Logic.Dialog;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class ConversationManager : IConversationManager, IStateTrackingService
    {
        private readonly IResourceLocator _resourceLocator;
        private readonly IPlayerData _playerData;
        private readonly IWorldManager _worldManager;
        private readonly IQuestTracker _questTracker;
        private readonly ISimulationEventBus _simulationEventBus;
        private readonly ILogger _logger;
        private readonly ISimulationTradeMonitor _simulationTradeMonitor;
        private bool _initialized;

        private List<ConvoTopic> _topics;
        private Dictionary<Npc, Dictionary<string, string>> _conversationState;

        public ConversationManager(
            IResourceLocator resourceLocator,
            IPlayerData playerData,
            IWorldManager worldManager,
            IQuestTracker questTracker,
            ISimulationEventBus simulationEventBus,
            ILogger logger,
            ISimulationTradeMonitor simulationTradeMonitor)
        {
            _resourceLocator = resourceLocator;
            _playerData = playerData;
            _worldManager = worldManager;
            _questTracker = questTracker;
            _simulationEventBus = simulationEventBus;
            _logger = logger;
            _conversationState = new Dictionary<Npc, Dictionary<string, string>>();
            _simulationTradeMonitor = simulationTradeMonitor;
        }

        public void ClearState()
        {
            _conversationState.Clear();
        }

        public void LoadState(IResourceLocator resourceLocator, SerializationObject root)
        {
            var serviceRoot = root.GetChild("ConversationManager");

            _conversationState.Clear();
            if(serviceRoot != null)
            {
                foreach (var npcRoot in serviceRoot.Children.Where(x => x.Name == "npc"))
                {
                    var npc = _resourceLocator.GetNpc(npcRoot.GetAttribute("name"));
                    var state = npcRoot.GetDictionary("state");
                    _conversationState.Add(npc, state);
                }
            }
        }

        public void SaveState(SerializationObject root)
        {
            var serviceRoot = root.AddChild("ConversationManager");

            foreach(var npc in _conversationState.Keys)
            {
                var npcRoot = serviceRoot.AddChild("npc");
                npcRoot.SetAttribute("name", npc.Name);
                npcRoot.AddDictionary("state", _conversationState[npc]);
            }
        }

        public NpcConversation StartConversationWith(Npc npc)
        {
            if(!_initialized)
            {
                Initialize();
            }

            Dictionary<string, string> convoState;
            if (_conversationState.ContainsKey(npc))
            {
                convoState = _conversationState[npc];
            }
            else
            {
                convoState = new Dictionary<string, string>();
                _conversationState.Add(npc, convoState);
            }

            return new NpcConversation(
                _playerData,
                _resourceLocator,
                _worldManager,
                _questTracker,
                _simulationEventBus,
                _logger,
                convoState,
                npc,
                _topics);
        }

        private void Initialize()
        {
            _initialized = true;

            _topics = _resourceLocator.GetConversationTopics()
                .GroupBy(x => x.Name)
                .Select(g => new ConvoTopic
                {
                    Name = g.Key,
                    Responses = g.SelectMany(x => x.Responses).ToArray()
                })
                .ToList();
        }

        public IEnumerable<ConversationResponse> GetRumorsFor(WorldStation station, int numberOfRumors)
        {
            if (!_initialized)
            {
                Initialize();
            }

            var convoState = new Dictionary<string, string>()
            {
                { "station", station.BackingStation.Name }
            };

            var random = new System.Random();
            var exports = _simulationTradeMonitor.GetExports(station.BackingStation).ToArray();
            var imports = _simulationTradeMonitor.GetImports(station.BackingStation).ToArray();

            if (station.BackingStation.EconomicProcesses.Sum(x => x.IndustrialSupport) >= 1)
            {
                convoState.Add("industrial_shop_rumor", "This station is a good source of industrial equipment.");
            }
            else
            {
                var potentialShops = exports
                    .Select(t => t.Item1)
                    .Distinct()
                    .Where(x => x.EconomicProcesses.Sum(s => s.IndustrialSupport) >= 1)
                    .ToArray();

                if(potentialShops.Any())
                {
                    var shop = potentialShops[random.Next(potentialShops.Length)];
                    
                    convoState.Add("industrial_shop_rumor", $"{shop.Name} a good source of industrial equipment.");
                }
            }
            
            if (station.BackingStation.EconomicProcesses.Sum(x => x.MilitarySupport) >= 1)
            {
                convoState.Add("military_shop_rumor", "This station is a good source of military equipment.");
            }
            else
            {
                var potentialShops = exports
                    .Select(t => t.Item1)
                    .Distinct()
                    .Where(x => x.EconomicProcesses.Sum(s => s.MilitarySupport) >= 1)
                    .ToArray();

                if (potentialShops.Any())
                {
                    var shop = potentialShops[random.Next(potentialShops.Length)];

                    convoState.Add("military_shop_rumor", $"{shop.Name} a good source of military equipment.");
                }
            }

            
            if(exports.Any())
            {
                var export = exports[random.Next(exports.Length)];
                convoState.Add("export_rumor", $"We export {export.Item2.Name} to {export.Item1.Name}.");
            }

            if (imports.Any())
            {
                var import = imports[random.Next(imports.Length)];
                convoState.Add("import_rumor", $"We import {import.Item2.Name} from {import.Item1.Name}.");
            }

            var npc = new Npc
            {
                Traits = new[] { "station_rumors" }
            };

            var conversation = new NpcConversation(
                _playerData,
                _resourceLocator,
                _worldManager,
                _questTracker,
                _simulationEventBus,
                _logger,
                convoState,
                npc,
                _topics);

            var responses = new List<ConversationResponse>();
            while(responses.Count < numberOfRumors)
            {
                var response = conversation.ChooseTopic("StationRumors");

                if (!responses.Any(r => r.Lines.FirstOrDefault() == response.Lines.FirstOrDefault()))
                {
                    responses.Add(response);
                }
            }

            return responses;
        }
    }
}
