﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class SettingsService : ISettingsService
    {
        public float EffectsVolume { get; set; }
        public float MusicVolume { get; set; }
        public Dictionary<string, string> RebindableActions { get; }
        public bool IsFullscreen { get; set; }

        public SettingsService()
        {
            RebindableActions = new Dictionary<string, string>
            {
                {"control_throttle_up", "Increase Throttle" },
                {"control_throttle_down", "Decrease Throttle" },
                {"control_rudder_left", "Turn Left" },
                {"control_rudder_right", "Turn Right" },
                {"control_dive_up", "Dive Up" },
                {"control_dive_down", "Dive Down" },
                {"aim_toggle", "Toggle Aim" },
                {"switch_ammo_1", "Select Weapon 1" },
                {"switch_ammo_2", "Select Weapon 2" },
                {"switch_ammo_3", "Select Weapon 3" },
                {"switch_ammo_4", "Select Weapon 4" },
                {"switch_ammo_5", "Select Weapon 5" },
                {"switch_ammo_6", "Select Weapon 6" },
                {"switch_ammo_7", "Select Weapon 7" },
                {"zoom_togglebinocs", "Toggle Periscope" },
                {"target_nearest", "Target" },
                {"ui_hide", "Toggle UI" },
                {"drop_decoy", "Drop Decoy" },
                {"ui_map", "Show Map" },
                {"ui_freelook", "Freelook" },
                {"lrpanel_map", "Panel Map Mode" },
                {"lrpanel_divers", "Panel Divers Mode" },
                {"lrpanel_torps", "Panel Torpedo Mode" },
                {"lrpanel_target", "Panel Target Mode" }
            };
        }

        public event SettingsChangedHandler OnSettingsChanged;

        public void LoadSettings()
        {
            var configFile = new ConfigFile();

            if(configFile.Load("settings.ini") == Error.Ok)
            {
                EffectsVolume = Convert.ToSingle(configFile.GetValue("volumes", "effects", "1"));
                MusicVolume = Convert.ToSingle(configFile.GetValue("volumes", "music", "1"));
                IsFullscreen = Convert.ToBoolean(configFile.GetValue("display", "fullscreen", "true"));

                foreach (var action in RebindableActions.Keys)
                {
                    var scanCode = Convert.ToUInt32(configFile.GetValue("keybinds", action, "0"));

                    if (scanCode > 0)
                    {
                        InputMap.ActionEraseEvents(action);
                        InputMap.ActionAddEvent(action, new InputEventKey { Scancode = scanCode });
                    }
                }
            }
            else
            {
                EffectsVolume = 1;
                MusicVolume = 1;
            }

            AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Music"), GD.Linear2Db(MusicVolume));
            AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Effects"), GD.Linear2Db(EffectsVolume));
        }

        public void SaveSettings()
        {
            var configFile = new ConfigFile();

            configFile.SetValue("volumes", "effects", EffectsVolume);
            configFile.SetValue("volumes", "music", MusicVolume);
            configFile.SetValue("display", "fullscreen", IsFullscreen);

            foreach(var action in RebindableActions.Keys)
            {
                var currentAction = InputMap.GetActionList(action).OfType<InputEventKey>().FirstOrDefault();

                if (currentAction != null)
                {
                    configFile.SetValue("keybinds", action, currentAction.Scancode);
                }
            }

            configFile.Save("settings.ini");

            OnSettingsChanged?.Invoke();

            AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Music"), GD.Linear2Db(MusicVolume));
            AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Effects"), GD.Linear2Db(EffectsVolume));
        }

        public void ApplyDisplaySettings()
        {
            OS.WindowFullscreen = IsFullscreen;
        }
    }
}
