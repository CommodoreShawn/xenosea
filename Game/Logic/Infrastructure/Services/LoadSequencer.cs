﻿using System;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class LoadSequencer : ILoadSequencer
    {
        private readonly IWorldGenerator _worldGenerator;
        private readonly IStationGenerator _stationGenerator;
        private readonly IWorldManager _worldManager;
        private readonly IPlayerData _playerData;
        private readonly IGameSerializer _gameSerializer;

        public LoadSequencer(
            IWorldGenerator worldGenerator,
            IStationGenerator stationGenerator,
            IGameSerializer gameSerializer,
            IWorldManager worldManager,
            IPlayerData playerData)
        {
            _worldGenerator = worldGenerator;
            _stationGenerator = stationGenerator;
            _gameSerializer = gameSerializer;
            _worldManager = worldManager;
            _playerData = playerData;
        }

        public void StartNewWorld(
            WorldDefinition worldDefinition, 
            ReportProgress reportProgress)
        {
            _worldGenerator.GenerateWorld(
                worldDefinition, 
                (overall, step, prog) => reportProgress(overall * 0.6, step, prog));

            _stationGenerator.PlaceStations(
                worldDefinition, 
                prog => reportProgress(0.7, "Placing stations.", prog));

            _worldGenerator.PlaceAgents(
                prog => reportProgress(0.8, "Placing agents.", prog));

            reportProgress(0.9, "Initializing player.", 0);

            foreach (var poi in worldDefinition.StartingKnownPoi)
            {
                var poiPos = Util.LatLonToVector(poi.Latitude, poi.Longitude, Constants.WORLD_RADIUS);

                var location = _worldManager.WorldDatums
                    .OrderBy(d => d.Position.DistanceTo(poiPos))
                    .Take(1)
                    .FirstOrDefault();

                if (location != null)
                {
                    _playerData.KnownPointsOfInterest
                        .Add(new KnownPointOfInterest
                        {
                            ContactType = poi.ContactType,
                            Location = location,
                            FriendlyName = poi.Name,
                            ReferenceId = poi.Name
                        });
                }
            }

            foreach (var topic in worldDefinition.StartingTopics)
            {
                _playerData.KnownTopics.Add(topic);
            }

            reportProgress(0.9, "Initializing player.", 0.2);

            var position = Util.LatLonToVector(
                worldDefinition.PlayerStartLatitude, 
                worldDefinition.PlayerStartLongitude, 
                Constants.WORLD_RADIUS);

            var spawnDatum = _worldManager.WorldDatums
                .OrderBy(d => d.Position.DistanceTo(position))
                .Take(1)
                .Where(d => d.SimulationStation == null)
                .FirstOrDefault();

            reportProgress(0.9, "Initializing player.", 0.5);

            _playerData.Faction = worldDefinition.PlayerFaction;
            _playerData.Money = worldDefinition.StartingMoney;
            _playerData.DetailedSimDistance = 40000;
            _playerData.SpawnDatum = spawnDatum;
            _playerData.PlayerLoadout = worldDefinition.StarterSub;

            reportProgress(0.9, "Initializing player.", 1.0);
        }

        public void LoadWorld(WorldDefinition worldDefinition, string fileName, ReportProgress reportProgress)
        {
            _worldGenerator.GenerateWorld(
                worldDefinition,
                (overall, step, prog) => reportProgress(overall * 0.5, step, prog));

            _gameSerializer.LoadGameState(
                fileName,
                (overall, step, prog) => reportProgress(overall * 0.4 + 0.5, step, prog));


            _stationGenerator.CalculateStationInfleunce(
                prog => reportProgress(0.9, "Calculating influence.", prog));
        }
    }
}
