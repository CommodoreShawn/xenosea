﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.AI;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.Infrastructure.Missions;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class SimulationPatrolMissionGenerator : ISubSimulator
    {
        private readonly IWorldManager _worldManager;
        private readonly IPerftracker _perftracker;
        private readonly IMissionTracker _missionTracker;
        
        public SimulationPatrolMissionGenerator(
            IWorldManager worldManager,
            IPerftracker perftracker,
            IMissionTracker missionTracker)
        {
            _worldManager = worldManager;
            _perftracker = perftracker;
            _missionTracker = missionTracker;
        }

        public void Clear()
        {
        }

        public void Initialize()
        {
            foreach (var station in _worldManager.SimulationStations)
            {
                station.PerimiterPatrols.Clear();

                var excludedDatums = new List<WorldDatum>();
                excludedDatums.Add(station.Location);
                excludedDatums.AddRange(station.Location.Neighbors);

                var ringDatums = excludedDatums
                    .SelectMany(d => d.Neighbors)
                    .Except(excludedDatums)
                    .Distinct()
                    .ToList();
                var random = new Random(station.Name.GetHashCode());

                var ring = new List<WorldDatum>();

                if (ringDatums.Any())
                {
                    var current = ringDatums[random.Next(ringDatums.Count)];
                    ringDatums.Remove(current);

                    
                    ring.Add(current);

                    while(ringDatums.Any())
                    {
                        var angleToCurrent = station.Location.Position.DirectionTo(current.Position);

                        var next = current.Neighbors
                            .Intersect(ringDatums)
                            .OrderByDescending(n => station.Location.Position.DirectionTo(n.Position).AngleTo(angleToCurrent))
                            .FirstOrDefault();

                        if(next != null)
                        {
                            ring.Add(next);
                            ringDatums.Remove(next);
                            current = next;
                        }
                        else
                        {
                            ringDatums.Clear();
                        }
                    }
                }

                var quarterSize = ring.Count / 4;

                var firstQuarter = ring.Take(quarterSize).ToList();
                var secondQuarter = ring.Skip(quarterSize).Take(quarterSize).ToList();
                var thirdQuarter = ring.Skip(quarterSize * 2).Take(quarterSize).ToList();
                var fourthQuarter = ring.Skip(quarterSize * 3).ToList();

                station.PerimiterPatrols.Add(MakePatrolPath(random, station, firstQuarter));
                station.PerimiterPatrols.Add(MakePatrolPath(random, station, secondQuarter));
                station.PerimiterPatrols.Add(MakePatrolPath(random, station, thirdQuarter));
                station.PerimiterPatrols.Add(MakePatrolPath(random, station, fourthQuarter));

                station.PerimiterPatrols
                    .RemoveAll(p => p.Any(d => d.Depth < d.IceDepth));
            }
        }

        private List<WorldDatum> MakePatrolPath(Random random, SimulationStation station, List<WorldDatum> patrolPoints)
        {
            if(random.NextDouble() < 0.5)
            {
                patrolPoints.Reverse();
            }

            var path = SimulationAgentPather.DeterminePath(station.Location, patrolPoints.First());
            path.AddRange(patrolPoints);
            path.AddRange(SimulationAgentPather.DeterminePath(patrolPoints.Last(), station.Location));

            path.Remove(path.First());
            path.Remove(path.Last());

            return path.Distinct().ToList();
        }

        public void Process(float delta)
        {
            foreach (var station in _worldManager.SimulationStations)
            {
                var existingMissions = _missionTracker.GetPatrolMissionsBySource(station);

                for (var i = 0; i < station.PerimiterPatrols.Count; i++)
                {
                    if (!existingMissions.Any(m => m.PatrolPath == i))
                    {
                        var routeThreat = station.PerimiterPatrols[i]
                            .SelectMany(d => d.FactionInfluence)
                            .Where(kvp => kvp.Key.IsHostileTo(station.Faction))
                            .Sum(kvp => kvp.Value);

                        if (routeThreat > 0)
                        {
                            var newPatrolMission = new SimulationPatrolMission(
                                station.Faction.SecuritySubfaction ?? station.Faction,
                                routeThreat * 5000,
                                0,
                                TimeSpan.FromMinutes(120),
                                station,
                                i);

                            _missionTracker.AddMission(station, newPatrolMission);
                        }
                    }
                }
            }
        }
    }
}
