﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class WorldGenerator : IWorldGenerator, ISubSimulator
    {
        private readonly IWorldManager _worldManager;

        private SimplexNoiseGenerator _heightNoiseGen;
        private SimplexNoiseGenerator _volcanismNoiseGen;
        private SimplexNoiseGenerator _floorNoiseGen;
        private SimplexNoiseGenerator _roofNoiseGen;
        private SimplexNoiseGenerator _fertilityNoiseGen;
        private SimplexNoiseGenerator[] _oreNoiseGen;

        private Queue<Tuple<DatumCell, int>> _cellBuildQueue;
        private Task _buildTask;

        public WorldGenerator(
            IWorldManager worldManager)
        {
            _worldManager = worldManager;

            var random = new Random(Constants.SEED.GetHashCode());
            _heightNoiseGen = new SimplexNoiseGenerator(random.Next());
            _volcanismNoiseGen = new SimplexNoiseGenerator(random.Next());
            _cellBuildQueue = new Queue<Tuple<DatumCell, int>>();
            _floorNoiseGen = new SimplexNoiseGenerator(0);
            _roofNoiseGen = new SimplexNoiseGenerator(314159);
            _fertilityNoiseGen = new SimplexNoiseGenerator(83943);
            _oreNoiseGen = new SimplexNoiseGenerator[8];

            for (var i = 0; i < _oreNoiseGen.Length; i++)
            {
                _oreNoiseGen[i] = new SimplexNoiseGenerator(random.Next());
            }
        }

        public void GenerateWorld(WorldDefinition worldDefinition, ReportProgress reportProgress)
        {
            var random = new Random(Constants.SEED.GetHashCode());

            var datums = GenerateDatums(
                random,
                progress => reportProgress(0.0, "Creating datums.", progress));

            Util.LinkDatums(
                datums,
                progress => reportProgress(0.7, "Linking datums.", progress));

            _worldManager.AddDatums(datums);

            PlaceWrecks(
                worldDefinition,
                progress => reportProgress(0.9, "Placing wrecks.", progress));
        }

        private List<WorldDatum> GenerateDatums(
            Random random,
            ReportStepProgress reportStepProgress)
        {
            var datumAroundEquator = (2 * Constants.WORLD_RADIUS * Mathf.Pi) / Constants.DATUM_DISTANCE;
            var degreesPerDatum = 360 / datumAroundEquator;

            var datums = new List<WorldDatum>();

            // divided by as we do one side of the planet, north and south of the equator
            for (var latDatum = 0; latDatum < datumAroundEquator / 4; latDatum++)
            {
                var degreesLat = latDatum * degreesPerDatum;


                var radiusAtLat = Constants.WORLD_RADIUS * Mathf.Cos(Mathf.Deg2Rad(degreesLat));
                var datumsAroundLat = (2 * radiusAtLat * Mathf.Pi) / Constants.DATUM_DISTANCE;
                var degreesPerDatumAtLat = 360 / datumsAroundLat;

                for (var lonDatum = 0; lonDatum < datumsAroundLat; lonDatum++)
                {
                    var degreesLon = lonDatum * degreesPerDatumAtLat;

                    var position = Util.LatLonToVector(degreesLat, degreesLon, Constants.WORLD_RADIUS);

                    datums.Add(GenerateDatum(
                        degreesLat,
                        degreesLon,
                        position));

                    if (position.y != 0)
                    {
                        datums.Add(GenerateDatum(
                            -degreesLat,
                            degreesLon,
                            new Vector3(position.x, -position.y, position.z)));
                    }
                }

                reportStepProgress(latDatum / (datumAroundEquator / 4.0));
            }

            return datums;
        }

        private WorldDatum GenerateDatum(
            float latitude,
            float longitude,
            Vector3 position)
        {
            var radiusAtLat = Constants.WORLD_RADIUS * Mathf.Cos(Mathf.Deg2Rad(latitude));
            var datumsAroundLat = (2 * radiusAtLat * Mathf.Pi) / 5000;
            var degreesPerDatumAtLat = 360 / datumsAroundLat;

            var datumsAroundEquator = (2 * Constants.WORLD_RADIUS * Mathf.Pi) / 5000;
            var degreesPerDatumAtEquator = 360 / datumsAroundEquator;

            var depth = GetDepthAt(latitude, longitude);
            var light = Mathf.Cos(Mathf.Deg2Rad(latitude));
            var iceDepth = 50 + (1 - light) * Constants.MAX_DEPTH;

            var volcanism = GetVolcanismAt(latitude, longitude);

            var current = new Vector2(
                -Mathf.Cos(Mathf.Deg2Rad(latitude) * 6) * 10 * degreesPerDatumAtLat,
                -Mathf.Sin(Mathf.Deg2Rad(latitude) * 12) * 5 * degreesPerDatumAtLat);

            var clearDatumsUpstream = 1;
            var wasBlocked = false;

            var upstreamCheckDist = 16f;
            for (var i = 1; i < upstreamCheckDist && !wasBlocked; i++)
            {
                if (GetDepthAt(
                    latitude - current.y * i * datumsAroundEquator,
                    longitude - current.x * i * degreesPerDatumAtLat)
                        < iceDepth)
                {
                    wasBlocked = true;
                }
                else
                {
                    clearDatumsUpstream += 1;
                }
            }

            current *= (clearDatumsUpstream / upstreamCheckDist);

            if(iceDepth >= depth)
            {
                current = Vector2.Zero;
            }

            var fertility = volcanism;
            var factor = 0.8f;
            var drift = current;

            for(var i = 0; i < 8; i++)
            {
                fertility += factor * GetVolcanismAt(latitude + drift.y, longitude + drift.x);

                factor *= 0.8f;
                drift += current;
            }

            fertility /= 4.2f;

            var currentTo = Util.LatLonToVector(
                latitude + current.y,
                longitude + current.x,
                Constants.WORLD_RADIUS);

            var datum = new WorldDatum(
                latitude,
                longitude,
                position)
            {
                Depth = depth,
                Light = light,
                IceDepth = iceDepth,
                Volcanism = volcanism,
                Fertility = fertility,
                OreAbundance = new float[_oreNoiseGen.Length],
                CurrentFlow = Vector3.Zero,
                // TODO decide on deleting this or not
                //CurrentFlow = (currentTo - position) / 3000
        };

            for (var i = 0; i < _oreNoiseGen.Length; i++)
            {
                datum.OreAbundance[i] = GetMineralValue(_oreNoiseGen[i], position);
            }

            datum.Biome = MapBiome(datum);

            return datum;
        }

        private float GetMineralValue(SimplexNoiseGenerator noiseGen, Vector3 position)
        {
            var value = noiseGen.CoherentNoise(
                position.x,
                position.y,
                position.z,
                octaves: 2,
                multiplier: Constants.NOISE_SCALE / 2,
                persistence: 0.5f);

            return Mathf.Clamp(value / 0.16f, 0, 1);
        }

        private Biome MapBiome(WorldDatum datum)
        {
            if(datum.Depth <= datum.IceDepth)
            {
                return Biome.Ice;
            }
            else if(datum.Volcanism >= 0.95)
            {
                return Biome.Smokers;
            }
            else if(datum.Fertility > 0.8)
            {
                return Biome.Reef;
            }
            else if (datum.Fertility > 0.3)
            {
                return Biome.Forest;
            }
            else if (datum.Fertility > 0)
            {
                return Biome.Plains;
            }

            return Biome.Dunes;
        }

        private void PlaceWrecks(WorldDefinition worldDefinition, ReportStepProgress reportStepProgress)
        {
            var count = 0.0;
            foreach (var wreckDefinition in worldDefinition.PersistentWrecks)
            {
                var position = Util.LatLonToVector(wreckDefinition.Latitude, wreckDefinition.Longitude, Constants.WORLD_RADIUS);

                var location = _worldManager.WorldDatums
                    .OrderBy(d => d.Position.DistanceTo(position))
                    .Take(1)
                    .Where(d => d.SimulationStation == null)
                    .FirstOrDefault();

                if (location != null)
                {
                    location.LocalWreck = wreckDefinition;
                }
                else
                {
                    GD.Print("Error: Could not find available datum for placing wreck " + wreckDefinition.Name);
                }

                count += 1;
                reportStepProgress(count / worldDefinition.PersistentWrecks.Length);
            }
        }

        public void PlaceAgents(ReportStepProgress reportStepProgress)
        {
            var count = 0.0;
            var stationCount = _worldManager.SimulationStations.Count();

            foreach (var station in _worldManager.SimulationStations)
            {
                for (var i = 0; i < station.SupportedAgentDefinitions.Length; i++)
                {
                    if (station.SupportedAgentDefinitions[i] != null)
                    {
                        var agentOwner = station.Faction;
                        if(station.SupportedAgentDefinitions[i].IsSecurity)
                        {
                            agentOwner = station.Faction.SecuritySubfaction ?? station.Faction;
                        }
                        else if (station.SupportedAgentDefinitions[i].IsTrader)
                        {
                            agentOwner = station.Faction.TradeSubfaction ?? station.Faction;
                        }

                        var newAgent = new SimulationAgent(
                            station.SupportedAgentDefinitions[i],
                            station,
                            agentOwner,
                            station.Location);

                        station.SupportedAgents[i] = newAgent;
                        _worldManager.AddSimulationAgent(newAgent);
                    }
                }

                count += 1;
                reportStepProgress(count / stationCount);
            }
        }

        public float GetDepthAt(
            float latitude,
            float longitude)
        {
            var position = Util.LatLonToVector(latitude, longitude, Constants.WORLD_RADIUS);

            var heightNoise = _heightNoiseGen.CoherentNoise(
                position.x,
                position.y,
                position.z,
                octaves: 4,
                multiplier: Constants.NOISE_SCALE,
                persistence: 0.5f);

            var depth = 1 - Mathf.Sqrt(Mathf.Clamp((heightNoise + 0.2f) / 0.4f, 0, 1)) - 0.1f;

            var volcanism = GetVolcanismAt(latitude, longitude);

            return Mathf.Clamp(depth - volcanism / 10, 0, 1) * Constants.MAX_DEPTH;
        }

        public float GetVolcanismAt(
            float latitude, 
            float longitude)
        {
            var position = Util.LatLonToVector(latitude, longitude, Constants.WORLD_RADIUS);

            var volcanism = _volcanismNoiseGen.CoherentNoise(
                position.x,
                position.y,
                position.z,
                octaves: 2,
                multiplier: Constants.NOISE_SCALE,
                persistence: 0.5f);

            volcanism = Mathf.Clamp(volcanism / 0.2f, -1, 1);
            volcanism = Mathf.Clamp((1 - Mathf.Abs(volcanism) * 4), 0, 1);

            return volcanism;
        }

        public void QueueGeometryRebuild(DatumCell datumCell, int vertexCount)
        {
            _cellBuildQueue.Enqueue(Tuple.Create(datumCell, vertexCount));
        }

        public void Process(float delta)
        {
            if(_buildTask == null
                || _buildTask.Status == TaskStatus.RanToCompletion
                || _buildTask.Status == TaskStatus.Faulted)
            {
                _buildTask = null;

                if(_cellBuildQueue.Count > 0)
                {
                    var item = _cellBuildQueue.Dequeue();

                    _buildTask = Task.Run(() =>
                    {
                        var cell = item.Item1;

                        var buildData = BuildCellGeometry(
                            item.Item1.WorldDatum.Latitude, 
                            item.Item1.WorldDatum.Longitude, 
                            item.Item2, 
                            false,
                            item.Item1.WorldDatum);

                        if (Godot.Object.IsInstanceValid(cell))
                        {
                            cell.CellBuildData = buildData;
                        }
                    });
                }
            }
        }

        private CellBuildData BuildCellGeometry(
            float latitude, 
            float longitude, 
            int vertexCount, 
            bool plainRoof,
            WorldDatum datum)
        {
            var position = Util.LatLonToVector(latitude, longitude, Constants.WORLD_RADIUS);

            var stopwatch = System.Diagnostics.Stopwatch.StartNew();
            var heightGenStopwatch = new System.Diagnostics.Stopwatch();
            var floorNoiseStopwatch = new System.Diagnostics.Stopwatch();
            var roofNoiseStopwatch = new System.Diagnostics.Stopwatch();

            var datumAroundEquator = (2 * Constants.WORLD_RADIUS * Mathf.Pi) / Constants.DATUM_DISTANCE;
            var degreesPerDatum = 360 / datumAroundEquator;
            var radiusAtLat = Constants.WORLD_RADIUS * Mathf.Cos(Mathf.Deg2Rad(latitude));
            var datumsAroundLat = (2 * radiusAtLat * Mathf.Pi) / Constants.DATUM_DISTANCE;
            var degreesPerDatumAtLat = 360 / datumsAroundLat;

            var lonDegPerVertex = degreesPerDatumAtLat / (vertexCount - 2);
            var latDegPerVertex = degreesPerDatum / (vertexCount - 2);

            var floorVerticies = new Vector3[vertexCount, vertexCount];
            var floorDepths = new float[vertexCount, vertexCount];
            var roofVerticies = new Vector3[vertexCount, vertexCount];
            var baseThickness = new float[vertexCount, vertexCount];
            var adjThickness = new float[vertexCount, vertexCount];

            for (var i = 0; i < vertexCount; i++)
            {
                for (var j = 0; j < vertexCount; j++)
                {
                    var deltaLon = (i - (vertexCount / 2f)) * lonDegPerVertex;
                    var deltaLat = (j - (vertexCount / 2f)) * latDegPerVertex;

                    var samplePoint = Util.LatLonToVector(
                        latitude + deltaLat,
                        longitude + deltaLon,
                        Constants.WORLD_RADIUS);

                    var light = Mathf.Cos(Mathf.Deg2Rad(latitude + deltaLat));
                    var iceThickness = 50 + (1 - light) * 20000;

                    var dirToCore = samplePoint.Normalized() * -1;

                    floorNoiseStopwatch.Start();
                    var depthAdj = _floorNoiseGen.CoherentNoise(
                        samplePoint.x,
                        samplePoint.y,
                        samplePoint.z,
                        octaves: 4,
                        multiplier: 1000,
                        persistence: 0.5f);
                    floorNoiseStopwatch.Stop();

                    depthAdj /= 0.2f;
                    depthAdj *= 100;

                    heightGenStopwatch.Start();
                    depthAdj += GetDepthAt(
                        latitude + deltaLat,
                        longitude + deltaLon);
                    heightGenStopwatch.Stop();

                    floorDepths[i, j] = depthAdj;

                    floorVerticies[i, j] = samplePoint
                        + dirToCore * depthAdj
                        - position;

                    roofNoiseStopwatch.Start();
                    depthAdj = _roofNoiseGen.CoherentNoise(
                        samplePoint.x,
                        samplePoint.y,
                        samplePoint.z,
                        octaves: 4,
                        multiplier: 1000,
                        persistence: 0.5f);
                    roofNoiseStopwatch.Stop();

                    depthAdj /= 0.2f;
                    depthAdj *= 100;
                    baseThickness[i, j] = iceThickness;
                    adjThickness[i, j] = depthAdj;
                    depthAdj += iceThickness;

                    if(plainRoof)
                    {
                        depthAdj = iceThickness;
                    }

                    roofVerticies[i, j] = samplePoint
                        + dirToCore * depthAdj
                        - position;
                }
            }

            var floorSurfaceTool = new SurfaceTool();
            floorSurfaceTool.Begin(Mesh.PrimitiveType.Triangles);
            var roofSurfaceTool = new SurfaceTool();
            roofSurfaceTool.Begin(Mesh.PrimitiveType.Triangles);


            var indexToVertex = new int[(vertexCount - 1) * (vertexCount - 1) * 6];


            for (var i = 0; i < vertexCount - 1; i++)
            {
                for (var j = 0; j < vertexCount - 1; j++)
                {
                    var uvScale = 1f / 5f;

                    var index = i * 6 + (j * (vertexCount - 1) * 6);

                    var edgeOverlap = 0;
                    if(i == vertexCount - 1 || j == vertexCount - 1)
                    {
                        edgeOverlap = 1;
                    }

                    floorSurfaceTool.AddColor(SlopeAndBiomeToColor(i, j, floorDepths, floorVerticies, floorVerticies[i, j], datum));
                    floorSurfaceTool.AddUv(MakeTextureUV(0, 0, i, j, uvScale));
                    indexToVertex[index + 0] = (i + 0) + (j + 0) * vertexCount;
                    floorSurfaceTool.AddVertex(ApplyEdgeOverlap(floorVerticies[i, j], edgeOverlap));
                    floorSurfaceTool.AddColor(SlopeAndBiomeToColor(i + 1, j + 1, floorDepths, floorVerticies, floorVerticies[i + 1, j + 1], datum));
                    floorSurfaceTool.AddUv(MakeTextureUV(1, 1, i, j, uvScale));
                    indexToVertex[index + 1] = (i + 1) + (j + 1) * vertexCount;
                    floorSurfaceTool.AddVertex(ApplyEdgeOverlap(floorVerticies[i + 1, j + 1], edgeOverlap));
                    floorSurfaceTool.AddColor(SlopeAndBiomeToColor(i, j + 1, floorDepths, floorVerticies, floorVerticies[i, j + 1], datum));
                    floorSurfaceTool.AddUv(MakeTextureUV(0, 1, i, j, uvScale));
                    indexToVertex[index + 2] = (i + 0) + (j + 1) * vertexCount;
                    floorSurfaceTool.AddVertex(ApplyEdgeOverlap(floorVerticies[i, j + 1], edgeOverlap));

                    floorSurfaceTool.AddColor(SlopeAndBiomeToColor(i, j, floorDepths, floorVerticies, floorVerticies[i, j], datum));
                    floorSurfaceTool.AddUv(MakeTextureUV(0, 0, i, j, uvScale));
                    indexToVertex[index + 3] = (i + 0) + (j + 0) * vertexCount;
                    floorSurfaceTool.AddVertex(ApplyEdgeOverlap(floorVerticies[i, j], edgeOverlap));
                    floorSurfaceTool.AddColor(SlopeAndBiomeToColor(i + 1, j, floorDepths, floorVerticies, floorVerticies[i + 1, j], datum));
                    floorSurfaceTool.AddUv(MakeTextureUV(1, 0, i, j, uvScale));
                    indexToVertex[index + 4] = (i + 1) + (j + 0) * vertexCount;
                    floorSurfaceTool.AddVertex(ApplyEdgeOverlap(floorVerticies[i + 1, j], edgeOverlap));
                    floorSurfaceTool.AddColor(SlopeAndBiomeToColor(i + 1, j + 1, floorDepths, floorVerticies, floorVerticies[i + 1, j + 1], datum));
                    floorSurfaceTool.AddUv(MakeTextureUV(1, 1, i, j, uvScale));
                    indexToVertex[index + 5] = (i + 1) + (j + 1) * vertexCount;
                    floorSurfaceTool.AddVertex(ApplyEdgeOverlap(floorVerticies[i + 1, j + 1], edgeOverlap));

                    roofSurfaceTool.AddColor(DepthToEmissiveColor(adjThickness[i, j], baseThickness[i, j]));
                    roofSurfaceTool.AddUv(MakeTextureUV(0, 0, i, j, uvScale));
                    roofSurfaceTool.AddVertex(ApplyEdgeOverlap(roofVerticies[i, j], -edgeOverlap));
                    roofSurfaceTool.AddColor(DepthToEmissiveColor(adjThickness[i, j + 1], baseThickness[i, j + 1]));
                    roofSurfaceTool.AddUv(MakeTextureUV(0, 1, i, j, uvScale));
                    roofSurfaceTool.AddVertex(ApplyEdgeOverlap(roofVerticies[i, j + 1], -edgeOverlap));
                    roofSurfaceTool.AddColor(DepthToEmissiveColor(adjThickness[i + 1, j + 1], baseThickness[i + 1, j + 1]));
                    roofSurfaceTool.AddUv(MakeTextureUV(1, 1, i, j, uvScale));
                    roofSurfaceTool.AddVertex(ApplyEdgeOverlap(roofVerticies[i + 1, j + 1], -edgeOverlap));


                    roofSurfaceTool.AddColor(DepthToEmissiveColor(adjThickness[i, j], baseThickness[i, j]));
                    roofSurfaceTool.AddUv(MakeTextureUV(0, 0, i, j, uvScale));
                    roofSurfaceTool.AddVertex(ApplyEdgeOverlap(roofVerticies[i, j], -edgeOverlap));
                    roofSurfaceTool.AddColor(DepthToEmissiveColor(adjThickness[i + 1, j + 1], baseThickness[i + 1, j + 1]));
                    roofSurfaceTool.AddUv(MakeTextureUV(1, 1, i, j, uvScale));
                    roofSurfaceTool.AddVertex(ApplyEdgeOverlap(roofVerticies[i + 1, j + 1], -edgeOverlap));
                    roofSurfaceTool.AddColor(DepthToEmissiveColor(adjThickness[i + 1, j], baseThickness[i + 1, j]));
                    roofSurfaceTool.AddUv(MakeTextureUV(1, 0, i, j, uvScale));
                    roofSurfaceTool.AddVertex(ApplyEdgeOverlap(roofVerticies[i + 1, j], -edgeOverlap));
                }
            }
            
            floorSurfaceTool.GenerateNormals();
            floorSurfaceTool.Index();
            roofSurfaceTool.GenerateNormals();
            roofSurfaceTool.Index();

            var floorArrays = floorSurfaceTool.CommitToArrays();

            // get all of the normals for each vertex, average them together, and set that as the normal for all of the copies

            var floorNormalArray = floorArrays[(int)ArrayMesh.ArrayType.Normal] as Array;
            var normalSum = new Vector3[vertexCount * vertexCount];
            var normalCount = new int[vertexCount * vertexCount];

            for (var i = 0; i < floorNormalArray.Length; i++)
            {
                var vertIndex = indexToVertex[i];

                var normal = (Vector3)floorNormalArray.GetValue(i);
                normalSum[vertIndex] += normal;
                normalCount[vertIndex] += 1;
            }

            for (var i = 0; i < floorNormalArray.Length; i++)
            {
                var vertIndex = indexToVertex[i];

                if (normalCount[vertIndex] > 0)
                {
                    var normal = normalSum[vertIndex] / normalCount[vertIndex];

                    floorNormalArray.SetValue(normal, i);
                }
            }

            floorArrays[(int)ArrayMesh.ArrayType.Normal] = floorNormalArray;

            var buildData = new CellBuildData
            {
                FloorSurface = floorArrays,
                RoofSurface = roofSurfaceTool.CommitToArrays()
            };
            stopwatch.Stop();

            return buildData;
        }

        private Vector3 ApplyEdgeOverlap(Vector3 vertex, int edgeOverlap)
        {
            if(edgeOverlap != 0)
            {
                return vertex - vertex.Normalized() * edgeOverlap;
            }

            return vertex;
        }

        private Color SlopeAndBiomeToColor(
            int x, int y, 
            float[,] depths,
            Vector3[,] floorVerticies,
            Vector3 vertexPosition, 
            WorldDatum worldDatum)
        {
            var referenceDepth = depths[x, y];
            var greatestSlope = 0f;

            for (var i = -1; i <= 1; i++)
            {
                for (var j = -1; j <= 1; j++)
                {
                    var newX = x + i;
                    var newY = y + j;

                    if(newX >= 0 && newX < depths.GetLength(0)
                        && newY >= 0 && newY < depths.GetLength(1))
                    {
                        var dist = floorVerticies[x, y].DistanceTo(floorVerticies[newX, newY]);

                        if (dist > 0)
                        {
                            var slope = Mathf.Abs(referenceDepth - depths[newX, newY]) / dist;

                            greatestSlope = Mathf.Max(greatestSlope, slope);
                        }
                    }
                }
            }



            // Rocky slope
            if(greatestSlope > 0.4)
            {
                return Colors.Green;
            }

            if(worldDatum == null)
            {
                return Colors.Red;
            }

            var fertilityNoise = _fertilityNoiseGen.CoherentNoise(
                vertexPosition.x + worldDatum.Position.x,
                vertexPosition.y + worldDatum.Position.y,
                vertexPosition.z + worldDatum.Position.z,
                octaves: 2,
                multiplier: 1000,
                persistence: 0.5f);

            var fertility = worldDatum.Fertility + fertilityNoise;

            if(fertility > 0.1)
            {
                return Colors.Blue;
            }

            return Colors.Red;
        }

        public void Initialize()
        {
        }

        private Color DepthToEmissiveColor(float adjThickness, float baseThickness)
        {
            var baseEmissive = 5f / Mathf.Sqrt(baseThickness);
            var adjEmissive = 1.5f - (adjThickness + 100) / 200;
            var factor = Mathf.Clamp(baseEmissive * adjEmissive, 0, 1);

            return new Color(
                0.00f * factor,
                0.45f * factor,
                0.46f * factor);
        }

        private Vector2 MakeTextureUV(int deltaX, int deltaY, int vertX, int vertY, float uvScale)
        {
            return new Vector2(
                ((deltaX + vertX) * uvScale),
                ((deltaY + vertY) * uvScale));
        }

        public CellBuildData ForceGeometryRebuild(float latitude, float longitude, int vertexCount, bool plainRoof)
        {
            return BuildCellGeometry(latitude, longitude, vertexCount, plainRoof, null);
        }

        public void Clear()
        {
        }

        public float GetDetailedDepthAt(float latitude, float longitude)
        {
            var samplePoint = Util.LatLonToVector(
                latitude,
                longitude,
                Constants.WORLD_RADIUS);

            var depthAdj = _floorNoiseGen.CoherentNoise(
                samplePoint.x,
                samplePoint.y,
                samplePoint.z,
                octaves: 4,
                multiplier: 1000,
                persistence: 0.5f);

            depthAdj /= 0.2f;
            depthAdj *= 100;

            depthAdj += GetDepthAt(
                latitude,
                longitude);

            return depthAdj;
        }
    }
}