﻿using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class SoundEffectPlayer : ISoundEffectPlayer
    {
        public SoundEffectPlayerNode PlayerNode { get; set; }

        public void PlayUiClick()
        {
            if(PlayerNode != null
                && Godot.Object.IsInstanceValid(PlayerNode))
            {
                PlayerNode.PlayUiClick();
            }
        }
    }
}
