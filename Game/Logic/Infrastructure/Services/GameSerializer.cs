﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class GameSerializer : IGameSerializer
    {
        private const string SAVE_FOLDER = "saves";
        private const string EXTENSION = ".xeno";

        public string ActiveSaveName { get; private set; }

        private readonly IStateTrackingService[] _stateTrackingServices;
        private readonly ILogger _logger;
        private readonly IResourceLocator _resourceLocator;
        private readonly IPlayerData _playerData;

        public GameSerializer(
            IEnumerable<IStateTrackingService>  stateTrackingServices,
            ILogger logger,
            IResourceLocator resourceLocator,
            IPlayerData playerData)
        {
            _stateTrackingServices = stateTrackingServices.ToArray();
            _logger = logger;
            _resourceLocator = resourceLocator;
            _playerData = playerData;
        }

        public void ClearGameState()
        {
            ActiveSaveName = null;

            foreach(var service in _stateTrackingServices)
            {
                service.ClearState();
            }
        }

        public IEnumerable<SaveListItem> ListSaves()
        {
            Directory.CreateDirectory(SAVE_FOLDER);

            var results = new List<SaveListItem>();

            foreach (var filename in Directory.EnumerateFiles(SAVE_FOLDER).Where(filename => filename.EndsWith(EXTENSION)))
            {
                try
                {
                    results.Add(new SaveListItem
                    {
                        Name = Path.GetFileNameWithoutExtension(filename),
                        LastPlayed = File.GetLastWriteTime(filename)
                    });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Error when listing save files");
                }
            }

            return results;
        }

        public void SaveGameState(string fileName, ReportProgress reportProgress)
        {
            _playerData.LastSaveTime = DateTime.Now;

            ActiveSaveName = Path.GetFileNameWithoutExtension(fileName);

            var savePath = Path.Combine(SAVE_FOLDER, fileName + EXTENSION);

            var root = new SerializationObject()
            {
                Name = "save_root"
            };

            for (var i = 0; i < _stateTrackingServices.Length; i++)
            {
                _stateTrackingServices[i].SaveState(root);

                reportProgress(0, "Saving data...", i / (double)_stateTrackingServices.Length);
            }

            reportProgress(0.8, "Serializing...", 0);
            var raw = JsonConvert.SerializeObject(root);

            Directory.CreateDirectory(Path.GetDirectoryName(savePath));
            if (File.Exists(savePath))
            {
                File.Delete(savePath);
            }

            reportProgress(0.9, "Writing to Disk...", 0);
            File.WriteAllText(savePath, raw);
            reportProgress(0.9, "Writing to Disk...", 1);
        }

        public SaveFileSummary GetSaveSummary(string fileName)
        {
            var savePath = Path.Combine(SAVE_FOLDER, fileName + EXTENSION);

            var data = File.ReadAllText(savePath);

            var root = JsonConvert.DeserializeObject<SerializationObject>(data);

            var playerRoot = root.GetChild("PlayerData");

            return new SaveFileSummary
            {
                SubClass = playerRoot.GetAttribute("player_sub_name"),
                InGameDate = Convert.ToDateTime(root.GetChild("WorldManager").GetAttribute("date")),
                LastPlayed = File.GetLastWriteTime(savePath),
                LocationName = playerRoot.GetChild("player_loadout").GetAttribute("docked_to"),
                Name = fileName,
                Money = Convert.ToDouble(playerRoot.GetAttribute("money"))
            };
        }

        public void DeleteSave(string fileName)
        {
            var savePath = Path.Combine(SAVE_FOLDER, fileName + EXTENSION);
            File.Delete(savePath);
        }

        public bool IsValidFileName(string fileName)
        {
            return !fileName
                .Intersect(Path.GetInvalidFileNameChars())
                .Any()
                && ! string.IsNullOrEmpty(fileName);
        }

        public void LoadGameState(string fileName, ReportProgress reportProgress)
        {
            var savePath = Path.Combine(SAVE_FOLDER, fileName + EXTENSION);

            reportProgress(0.0, "Reading file...", 0);
            var data = File.ReadAllText(savePath);

            reportProgress(0.1, "Deserializing...", 0);
            var root = JsonConvert.DeserializeObject<SerializationObject>(data);

            ActiveSaveName = Path.GetFileNameWithoutExtension(fileName);

            for (var i = 0; i < _stateTrackingServices.Length; i++)
            {
                _stateTrackingServices[i].LoadState(_resourceLocator, root);

                reportProgress(0.3, "Loading data...", i / (double)_stateTrackingServices.Length);
            }
        }
    }
}
