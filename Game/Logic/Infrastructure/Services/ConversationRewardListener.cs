﻿using Godot;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class ConversationRewardListener : ISubSimulator
    {
        private readonly INotificationSystem _notificationSystem;
        private readonly IPlayerData _playerData;

        public ConversationRewardListener(
            ISimulationEventBus simulationEventBus,
            INotificationSystem notificationSystem,
            IPlayerData playerData)
        {
            simulationEventBus.OnSimulationEvent += OnSimulationEvent;
            _notificationSystem = notificationSystem;
            _playerData = playerData;
        }

        private void OnSimulationEvent(ISimulationEvent evt)
        {
            if (evt is DialogueTriggerSimulationEvent dialogueTrigger
                && dialogueTrigger.TriggerName.StartsWith("givemoney_"))
            {
                var money = float.Parse(dialogueTrigger.TriggerName.Split('_')[1]);

                _playerData.Money += money;

                _notificationSystem.AddNotification(
                    new Notification
                    {
                        IsGood = true,
                        IsMessage = true,
                        IsImportant = true,
                        Body = $"You recieved {money.ToString("C0")}"
                    });
            }
        }

        public void Process(float delta)
        {
        }

        public void Initialize()
        {
        }

        public void Clear()
        {
        }
    }
}
