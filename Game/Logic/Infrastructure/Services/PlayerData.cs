﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class PlayerData : IPlayerData, IStateTrackingService
    {
        public PlayerController PlayerController { get; set; }
        public Submarine PlayerSub { get; set; }
        public Faction Faction { get; set; }
        public PackedScene SubPrefab { get; set; }
        public double Money { get; set; }
        public WorldDatum SpawnDatum { get; set; }
        public float DetailedSimDistance { get; set; }
        public SubLoadout PlayerLoadout { get; set; }
        public SubState PlayerLoadLoadout { get; set; }
        public DateTime LastSaveTime { get; set; }
        public SimpleWaypoint ManualWaypoint { get; set; }
        public bool CruiseStealthMode { get; set; }
        public bool WeaponsSafe { get; set; }
        public bool InBattle { get; set; }
        public bool InTutorial { get; set; }
        public List<KnownPointOfInterest> KnownPointsOfInterest { get; }
        public List<string> KnownTopics { get; }
        public bool SkipIntro { get; set; }

        private readonly IWorldManager _worldManager;

        public PlayerData(IWorldManager worldManager)
        {
            _worldManager = worldManager;
            KnownPointsOfInterest = new List<KnownPointOfInterest>();
            KnownTopics = new List<string>();
        }

        public void ClearState()
        {
            PlayerSub = null;
            Money = 0;
            PlayerLoadLoadout = null;
            LastSaveTime = DateTime.Now;
            KnownPointsOfInterest.Clear();
            ManualWaypoint = null;
            KnownTopics.Clear();
            WeaponsSafe = true;
            CruiseStealthMode = false;
            InTutorial = false;
            PlayerLoadout = null;
        }

        public void LoadState(IResourceLocator resourceLocator, SerializationObject root)
        {
            var serviceRoot = root.GetChild("PlayerData");
            Money = Convert.ToDouble(serviceRoot.GetAttribute("money"));
            DetailedSimDistance = Convert.ToSingle(serviceRoot.GetAttribute("detailed_sim_distance"));
            Faction = serviceRoot.LookupReference(
                "faction_id",
                _worldManager.Factions,
                f => f.Name);

            PlayerLoadLoadout = SubState.LoadState(resourceLocator, _worldManager, serviceRoot.GetChild("player_loadout"));

            foreach(var poiRoot in serviceRoot.Children.Where(x=>x.Name == "poi"))
            {
                KnownPointsOfInterest.Add(KnownPointOfInterest.LoadState(_worldManager, poiRoot));
            }

            foreach (var topicRoot in serviceRoot.Children.Where(x => x.Name == "topic"))
            {
                KnownTopics.Add(topicRoot.GetAttribute("name"));
            }

            if (serviceRoot.HasChild("manual_waypoint"))
            {
                ManualWaypoint = SimpleWaypoint.LoadState(serviceRoot.GetChild("manual_waypoint"));
            }

            foreach (var exploredDatum in serviceRoot.Children.Where(x => x.Name == "explored_datum"))
            {
                var datum = exploredDatum.LookupReferenceOrNull(
                    "id",
                    _worldManager.WorldDatums,
                    d => d.Id);

                if(datum != null)
                {
                    datum.IsExplored = true;
                }
            }
        }

        public void SaveState(SerializationObject root)
        {
            var serviceRoot = root.AddChild("PlayerData");
            serviceRoot.SetAttribute("faction_id", Faction.Name);
            serviceRoot.SetAttribute("money", Money.ToString());
            serviceRoot.SetAttribute("detailed_sim_distance", DetailedSimDistance.ToString());
            serviceRoot.SetAttribute("player_sub_name", PlayerSub.HullType.Name);
            PlayerSub.BuildSubState().SaveState(serviceRoot.AddChild("player_loadout"));

            foreach(var poi in KnownPointsOfInterest)
            {
                poi.SaveState(serviceRoot.AddChild("poi"));
            }

            foreach(var topic in KnownTopics)
            {
                    serviceRoot.AddChild("topic").SetAttribute("name", topic);
            }

            if (ManualWaypoint != null)
            {
                ManualWaypoint.SaveState(serviceRoot.AddChild("manual_waypoint"));
            }

            foreach (var datum in _worldManager.WorldDatums.Where(x => x.IsExplored))
            {
                serviceRoot.AddChild("explored_datum").SetAttribute("id", datum.Id);
            }
        }
    }
}
