﻿using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.Infrastructure.Missions;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class MissionTracker : IMissionTracker, ISubSimulator, IStateTrackingService
    {
        private readonly object LOCK = new object();

        private List<IMission> _missions;
        private Dictionary<IStation, List<IMission>> _missionsByLocation;
        private Dictionary<Submarine, List<IMission>> _missionsByAgent;
        private Dictionary<CommodityType, List<ITradeMission>> _tradeMissionsByCommodity;
        private Dictionary<SimulationStation, List<IMission>> _missionsBySimulationLocation;
        private Dictionary<SimulationAgent, List<IMission>> _missionsBySimulationAgent;
        private Dictionary<SimulationStation, List<SimulationPatrolMission>> _simulationPatrolMissionsBySource;
        private List<IMission> _playerMissions;

        private readonly IWorldManager _worldManager;
        private readonly INotificationSystem _notificationSystem;
        private readonly IStandingTracker _standingTracker;
        private readonly IQuestTracker _questTracker;
        private readonly IEconomicLogger _economicLogger;

        public IMission TrackedMission { get; set; }

        public MissionTracker(
            IWorldManager worldManager,
            INotificationSystem notificationSystem,
            IStandingTracker standingTracker,
            IQuestTracker questTracker,
            IEconomicLogger economicLogger)
        {
            _worldManager = worldManager;
            _notificationSystem = notificationSystem;
            _standingTracker = standingTracker;
            _questTracker = questTracker;
            _missions = new List<IMission>();
            _missionsByLocation = new Dictionary<IStation, List<IMission>>();
            _missionsByAgent = new Dictionary<Submarine, List<IMission>>();
            _tradeMissionsByCommodity = new Dictionary<CommodityType, List<ITradeMission>>();
            _simulationPatrolMissionsBySource = new Dictionary<SimulationStation, List<SimulationPatrolMission>>();
            _missionsBySimulationLocation = new Dictionary<SimulationStation, List<IMission>>();
            _missionsBySimulationAgent = new Dictionary<SimulationAgent, List<IMission>>();
            _playerMissions = new List<IMission>();
            _economicLogger = economicLogger;
        }

        public void Initialize()
        {
        }

        public void AddMission(IStation location, IMission mission)
        {
            lock (LOCK)
            {
                _missions.Add(mission);

                if (location is WorldStation worldStation)
                {
                    if (!_missionsBySimulationLocation.ContainsKey(worldStation.BackingStation))
                    {
                        _missionsBySimulationLocation.Add(worldStation.BackingStation, new List<IMission>());
                    }
                    _missionsBySimulationLocation[worldStation.BackingStation].Add(mission);
                }
                else
                {
                    if (!_missionsByLocation.ContainsKey(location))
                    {
                        _missionsByLocation.Add(location, new List<IMission>());
                    }
                    _missionsByLocation[location].Add(mission);
                }

                if (mission is ITradeMission tradeMission)
                {
                    if (!_tradeMissionsByCommodity.ContainsKey(tradeMission.Commodity))
                    {
                        _tradeMissionsByCommodity.Add(tradeMission.Commodity, new List<ITradeMission>());
                    }
                    _tradeMissionsByCommodity[tradeMission.Commodity].Add(tradeMission);
                }
            }
        }

        public void Clear()
        {
            lock (LOCK)
            {
                _missions.Clear();
                _missionsByAgent.Clear();
                _missionsByLocation.Clear();
                _tradeMissionsByCommodity.Clear();
                _missionsBySimulationLocation.Clear();
                _missionsBySimulationAgent.Clear();
                _playerMissions.Clear();
                _simulationPatrolMissionsBySource.Clear();
            }
        }

        public void DeliverMission(IMission mission, IStation toStation, Submarine agent)
        {
            lock (LOCK)
            {
                if (_missionsByAgent.ContainsKey(agent))
                {
                    _missionsByAgent[agent].Remove(mission);
                }

                _missions.Remove(mission);

                if (mission is ITradeMission tradeMission)
                {
                    agent.CommodityStorage.Remove(tradeMission.Commodity, tradeMission.Quantity);
                    toStation.CommodityStorage.Add(tradeMission.Commodity, tradeMission.Quantity);
                    _tradeMissionsByCommodity[tradeMission.Commodity].Remove(tradeMission);
                }

                if (mission is SimulationPatrolMission simulationPatrolMission)
                {
                    _simulationPatrolMissionsBySource[simulationPatrolMission.FromStation].Remove(simulationPatrolMission);
                }

                if (agent.IsPlayerSub && mission.ForFaction != null)
                {
                    _standingTracker.AddStandingChange(
                        mission.ForFaction,
                        mission.Reward * Constants.CASH_TO_STANDING,
                        "Completed mission.");
                }

                if (agent.IsPlayerSub)
                {
                    _playerMissions.Remove(mission);
                    
                    if(TrackedMission == mission)
                    {
                        TrackedMission = null;
                    }
                }
            }
        }

        public void DestroyMission(IMission mission, Submarine agent)
        {
            lock (LOCK)
            {
                if (_missionsByAgent.ContainsKey(agent))
                {
                    _missionsByAgent[agent].Remove(mission);
                }

                if (agent.IsPlayerSub)
                {
                    _playerMissions.Remove(mission);
                }

                _missions.Remove(mission);

                if (mission is ITradeMission tradeMission)
                {
                    _tradeMissionsByCommodity[tradeMission.Commodity].Remove(tradeMission);
                }

                if (mission is SimulationPatrolMission simulationPatrolMission)
                {
                    _simulationPatrolMissionsBySource[simulationPatrolMission.FromStation].Remove(simulationPatrolMission);
                }

                if (agent.IsPlayerSub)
                {
                    _standingTracker.AddStandingChange(
                        mission.ForFaction,
                        mission.Reward * Constants.CASH_TO_STANDING * -1,
                        "Failed mission.");
                }
            }
        }

        public IEnumerable<IMission> GetAvailableMissions(IStation station)
        {
            lock (LOCK)
            {
                if (station is WorldStation worldStation
                && _missionsBySimulationLocation.ContainsKey(worldStation.BackingStation))
                {
                    return _missionsBySimulationLocation[worldStation.BackingStation];
                }

                if (_missionsByLocation.ContainsKey(station))
                {
                    return _missionsByLocation[station];
                }

                return Enumerable.Empty<IMission>();
            }
        }

        public IEnumerable<IMission> GetCurrentMissions(Submarine agent)
        {
            lock (LOCK)
            {
                IEnumerable<IMission> missions = Enumerable.Empty<IMission>();

                if (agent.IsPlayerSub)
                {
                    missions = missions
                        .Concat(_playerMissions)
                        .Concat(_questTracker.AllQuests
                            .Where(q => q.IsActive)
                            .OfType<IMission>());
                }
                else if (_missionsByAgent.ContainsKey(agent))
                {
                    missions = missions
                        .Concat(_missionsByAgent[agent]);
                }

                return missions;
            }
        }

        public IEnumerable<ITradeMission> GetTradeMissionsByCommodity(CommodityType commodity)
        {
            lock (LOCK)
            {
                if (_tradeMissionsByCommodity.ContainsKey(commodity))
                {
                    return _tradeMissionsByCommodity[commodity];
                }

                return Enumerable.Empty<ITradeMission>();
            }
        }

        public bool IsHeldByAgent(Submarine agent, IMission mission)
        {
            lock (LOCK)
            {
                if (_missionsByAgent.ContainsKey(agent))
                {
                    return _missionsByAgent[agent].Contains(mission);
                }

                if (agent.IsPlayerSub)
                {
                    return _playerMissions.Contains(mission);
                }

                return false;
            }
        }

        public bool IsMissionAvailableAt(IStation station, IMission mission)
        {
            lock (LOCK)
            {
                if (station is WorldStation worldStation
                && _missionsBySimulationLocation.ContainsKey(worldStation.BackingStation))
                {
                    return _missionsBySimulationLocation[worldStation.BackingStation].Contains(mission);
                }

                if (_missionsByLocation.ContainsKey(station))
                {
                    return _missionsByLocation[station].Contains(mission);
                }

                return false;
            }
        }

        public void PickupMission(IMission mission, IStation fromStation, Submarine agent)
        {
            lock (LOCK)
            {
                if (mission.AllowedTime != null)
                {
                    mission.DueDate = _worldManager.CurrentDate + mission.AllowedTime;
                }

                if (_missionsByLocation.ContainsKey(fromStation))
                {
                    _missionsByLocation[fromStation].Remove(mission);
                }

                if (!_missionsByAgent.ContainsKey(agent))
                {
                    _missionsByAgent.Add(agent, new List<IMission>());
                }

                _missionsByAgent[agent].Add(mission);

                if (agent.IsPlayerSub)
                {
                    _playerMissions.Add(mission);

                    if(TrackedMission == null)
                    {
                        TrackedMission = mission;
                    }
                }

                if (mission is ITradeMission tradeMission)
                {
                    agent.CommodityStorage.Add(tradeMission.Commodity, tradeMission.Quantity);
                }

                if (fromStation is WorldStation worldStation)
                {
                    _missionsBySimulationLocation[worldStation.BackingStation].Remove(mission);
                }
            }
        }

        public void Process(float delta)
        {
            lock (LOCK)
            {
                foreach (var kvp in _missionsByAgent)
                {
                    for (var i = 0; i < kvp.Value.Count; i++)
                    {
                        if (kvp.Key.IsWrecked || kvp.Value[i].DueDate <= _worldManager.CurrentDate)
                        {
                            if (kvp.Key.IsPlayerSub
                                && kvp.Value[i].DueDate <= _worldManager.CurrentDate)
                            {
                                _notificationSystem.AddNotification(
                                    new Notification
                                    {
                                        IsBad = true,
                                        IsMessage = true,
                                        IsImportant = true,
                                        Body = $"Mission {kvp.Value[i].Name} failed, time limit exceeed."
                                    });
                            }

                            DestroyMission(kvp.Value[i], kvp.Key);
                            i -= 1;
                        }
                    }
                }
            }
        }

        public void TransferMissions(Submarine oldSub, Submarine newSub)
        {
            lock (LOCK)
            {
                if (_missionsByAgent.ContainsKey(oldSub))
                {
                    if (!_missionsByAgent.ContainsKey(newSub))
                    {
                        _missionsByAgent.Add(newSub, new List<IMission>());
                    }

                    _missionsByAgent[newSub].AddRange(_missionsByAgent[oldSub]);
                    _missionsByAgent[oldSub].Clear();
                }
            }
        }

        public void AddMission(SimulationStation location, IMission mission)
        {
            lock (LOCK)
            {
                _missions.Add(mission);
                if (!_missionsBySimulationLocation.ContainsKey(location))
                {
                    _missionsBySimulationLocation.Add(location, new List<IMission>());
                }
                _missionsBySimulationLocation[location].Add(mission);

                if (mission is ITradeMission tradeMission)
                {
                    if (!_tradeMissionsByCommodity.ContainsKey(tradeMission.Commodity))
                    {
                        _tradeMissionsByCommodity.Add(tradeMission.Commodity, new List<ITradeMission>());
                    }
                    _tradeMissionsByCommodity[tradeMission.Commodity].Add(tradeMission);
                }

                if (mission is SimulationPatrolMission patrolMission)
                {
                    if (!_simulationPatrolMissionsBySource.ContainsKey(patrolMission.FromStation))
                    {
                        _simulationPatrolMissionsBySource.Add(patrolMission.FromStation, new List<SimulationPatrolMission>());
                    }

                    _simulationPatrolMissionsBySource[patrolMission.FromStation].Add(patrolMission);
                }
            }
        }

        public IEnumerable<IMission> GetAvailableMissions(SimulationStation station)
        {
            lock (LOCK)
            {
                if (_missionsBySimulationLocation.ContainsKey(station))
                {
                    return _missionsBySimulationLocation[station];
                }

                return Enumerable.Empty<IMission>();
            }
        }

        public IEnumerable<IMission> GetCurrentMissions(SimulationAgent agent)
        {
            lock (LOCK)
            {
                if (_missionsBySimulationAgent.ContainsKey(agent))
                {
                    return _missionsBySimulationAgent[agent];
                }

                return Enumerable.Empty<IMission>();
            }
        }

        public void PickupMission(IMission mission, SimulationStation fromStation, SimulationAgent agent)
        {
            lock (LOCK)
            {
                if (mission.AllowedTime != null)
                {
                    mission.DueDate = _worldManager.CurrentDate + mission.AllowedTime;
                }

                _missionsBySimulationLocation[fromStation].Remove(mission);

                if (!_missionsBySimulationAgent.ContainsKey(agent))
                {
                    _missionsBySimulationAgent.Add(agent, new List<IMission>());
                }

                _missionsBySimulationAgent[agent].Add(mission);

                if (mission is ITradeMission tradeMission)
                {
                    agent.CommodityStorage.Add(tradeMission.Commodity, tradeMission.Quantity);

                    _economicLogger.LogMissionPickup(agent, tradeMission);
                }
            }
        }

        public void DestroyMission(IMission mission, SimulationAgent agent)
        {
            lock (LOCK)
            {
                if (_missionsBySimulationAgent.ContainsKey(agent))
                {
                    _missionsBySimulationAgent[agent].Remove(mission);
                }

                _missions.Remove(mission);

                if (mission is ITradeMission tradeMission)
                {
                    _tradeMissionsByCommodity[tradeMission.Commodity].Remove(tradeMission);
                }
            }
        }

        public void DestroyMission(IMission mission, SimulationStation station)
        {
            lock (LOCK)
            {
                if (_missionsBySimulationLocation.ContainsKey(station))
                {
                    _missionsBySimulationLocation[station].Remove(mission);
                }

                _missions.Remove(mission);

                if (mission is ITradeMission tradeMission)
                {
                    _tradeMissionsByCommodity[tradeMission.Commodity].Remove(tradeMission);
                }
            }
        }

        public void DeliverMission(IMission mission, SimulationStation toStation, SimulationAgent agent)
        {
            lock (LOCK)
            {
                _missionsBySimulationAgent[agent].Remove(mission);
                _missions.Remove(mission);

                if (mission is ITradeMission tradeMission)
                {
                    agent.CommodityStorage.Remove(tradeMission.Commodity, tradeMission.Quantity);
                    toStation.CommodityStorage.Add(tradeMission.Commodity, tradeMission.Quantity);
                    _tradeMissionsByCommodity[tradeMission.Commodity].Remove(tradeMission);
                    _economicLogger.LogMissionDeliver(agent, tradeMission);
                }

                if (mission is SimulationPatrolMission simulationPatrolMission)
                {
                    _simulationPatrolMissionsBySource[simulationPatrolMission.FromStation].Remove(simulationPatrolMission);
                }
            }
        }

        public void ClearState()
        {
            Clear();
        }

        public void SaveState(SerializationObject root)
        {
            lock (LOCK)
            {
                var serviceRoot = root.AddChild("MissionTracker");
                foreach (var mission in _missions.Where(x => x.CanBeSerialized))
                {
                    mission.SerializeTo(serviceRoot.AddChild("mission"));
                }

                var byAgentRoot = serviceRoot.AddChild("MissionsBySimulationAgent");
                foreach (var kvp in _missionsBySimulationAgent.Where(kvp => kvp.Value.Any()))
                {
                    var agentRoot = byAgentRoot.AddChild("agent");
                    agentRoot.SetAttribute("id", kvp.Key.Id);
                    agentRoot.AddList("missions", kvp.Value.Select(x => x.Id));
                }

                var byLocationRoot = serviceRoot.AddChild("MissionsBySimulationLocation");
                foreach (var kvp in _missionsBySimulationLocation.Where(kvp => kvp.Value.Any()))
                {
                    var locationRoot = byLocationRoot.AddChild("location");
                    locationRoot.SetAttribute("name", kvp.Key.Name);
                    locationRoot.AddList("missions", kvp.Value.Select(x => x.Id));
                }

                serviceRoot.AddList("player_missions", _playerMissions.Select(x => x.Id));

                if (TrackedMission != null)
                {
                    serviceRoot.SetAttribute("tracked_mission", TrackedMission.Id);
                }
            }
        }

        public void LoadState(IResourceLocator resourceLocator, SerializationObject root)
        {
            lock (LOCK)
            {
                var serviceRoot = root.GetChild("MissionTracker");
                foreach (var missionRoot in serviceRoot.Children.Where(x => x.Name == "mission"))
                {
                    IMission mission = null;

                    var type = missionRoot.GetAttribute("type");
                    if (type == "SimulationTradeMission")
                    {
                        mission = SimulationTradeMission.DeserializeFrom(
                            _worldManager,
                            resourceLocator,
                            missionRoot);
                    }
                    else if (type == "SimulationPatrolMission")
                    {
                        mission = SimulationPatrolMission.DeserializeFrom(
                            _worldManager,
                            resourceLocator,
                            missionRoot);
                    }
                    else if (type == "RetrieveLogFromWreckMission")
                    {
                        mission = RetrieveLogFromWreckMission.DeserializeFrom(
                            _worldManager,
                            resourceLocator,
                            missionRoot);
                    }

                    if (mission != null)
                    {
                        _missions.Add(mission);

                        if (mission is ITradeMission tradeMission)
                        {
                            if (!_tradeMissionsByCommodity.ContainsKey(tradeMission.Commodity))
                            {
                                _tradeMissionsByCommodity.Add(tradeMission.Commodity, new List<ITradeMission>());
                            }
                            _tradeMissionsByCommodity[tradeMission.Commodity].Add(tradeMission);
                        }
                        else if (mission is SimulationPatrolMission patrolMission)
                        {
                            if (!_simulationPatrolMissionsBySource.ContainsKey(patrolMission.FromStation))
                            {
                                _simulationPatrolMissionsBySource.Add(patrolMission.FromStation, new List<SimulationPatrolMission>());
                            }

                            _simulationPatrolMissionsBySource[patrolMission.FromStation].Add(patrolMission);
                        }
                    }
                }

                var byAgentRoot = serviceRoot.GetChild("MissionsBySimulationAgent");
                foreach (var agentRoot in byAgentRoot.Children)
                {
                    var agent = byAgentRoot.LookupReferenceOrNull("id", _worldManager.SimulationAgents, x => x.Id);

                    if (agent != null)
                    {
                        foreach (var missionId in agentRoot.GetList("missions"))
                        {
                            var mission = _missions.Where(x => x.Id == missionId).FirstOrDefault();

                            if (mission != null)
                            {
                                if (!_missionsBySimulationAgent.ContainsKey(agent))
                                {
                                    _missionsBySimulationAgent.Add(agent, new List<IMission>());
                                }

                                _missionsBySimulationAgent[agent].Add(mission);
                            }
                        }
                    }
                }

                var byLocationRoot = serviceRoot.GetChild("MissionsBySimulationLocation");
                foreach (var locationRoot in byLocationRoot.Children)
                {
                    var location = locationRoot.LookupReferenceOrNull("name", _worldManager.SimulationStations, x => x.Name);

                    if (location != null)
                    {
                        foreach (var missionId in locationRoot.GetList("missions"))
                        {
                            var mission = _missions.Where(x => x.Id == missionId).FirstOrDefault();

                            if (mission != null)
                            {
                                if (!_missionsBySimulationLocation.ContainsKey(location))
                                {
                                    _missionsBySimulationLocation.Add(location, new List<IMission>());
                                }

                                _missionsBySimulationLocation[location].Add(mission);
                            }
                        }
                    }
                }

                foreach (var playerMissionId in serviceRoot.GetList("player_missions"))
                {
                    var mission = _missions
                        .Where(x => x.Id == playerMissionId)
                        .FirstOrDefault();

                    if (mission != null)
                    {
                        _playerMissions.Add(mission);
                    }
                    else
                    {
                        Godot.GD.Print("Cannot find player mission: " + playerMissionId);
                        Godot.GD.Print("Missions:\n" + string.Join("\n", _missions.Select(x => x.Id)));
                    }
                }


                if (serviceRoot.HasAttribute("tracked_mission"))
                {
                    TrackedMission = serviceRoot.LookupReferenceOrNull(
                        "tracked_mission",
                        _playerMissions.Concat(_questTracker.AllQuests.OfType<IMission>()),
                        x => x.Id);
                }
            }
        }

        public IEnumerable<SimulationPatrolMission> GetPatrolMissionsBySource(SimulationStation fromStation)
        {
            lock (LOCK)
            {
                if (_simulationPatrolMissionsBySource.ContainsKey(fromStation))
                {
                    return _simulationPatrolMissionsBySource[fromStation];
                }

                return Enumerable.Empty<SimulationPatrolMission>();
            }
        }
    }
}
