﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class WeatherSimulator : ISubSimulator
    {
        public static readonly object LOCK = new object();

        private readonly IWorldManager _worldManager;
        private readonly IPerftracker _perftracker;
        private readonly ILogger _logger;

        private Task _weatherSimTask;
        private float _weatherSimCooldown;
        private Random _random;
        private DateTime? _lastUpdateTime;

        private List<WeatherSystem> _weatherToAdd;
        private List<WeatherSystem> _weatherToRemove;

        public WeatherSimulator(
            IWorldManager worldManager,
            IPerftracker perftracker,
            ILogger logger)
        {
            _worldManager = worldManager;
            _perftracker = perftracker;
            _logger = logger;
            _random = new Random();

            _weatherToAdd = new List<WeatherSystem>();
            _weatherToRemove = new List<WeatherSystem>();
        }

        public void Clear()
        {
            _weatherSimTask = null;
            _lastUpdateTime = null;
        }

        public void Initialize()
        {
        }

        public void Process(float delta)
        {
            var stopwatch = System.Diagnostics.Stopwatch.StartNew();

            _weatherSimCooldown -= delta;

            if (_weatherSimTask == null)
            {
                if (_weatherSimCooldown <= 0)
                {
                    _weatherSimCooldown = 10;

                    _weatherSimTask = Task.Run(DoWeatherSimLogic);
                }
            }
            else if (_weatherSimTask.Status != TaskStatus.Running)
            {
                _weatherSimTask = null;
            }

            foreach (var weatherSystem in _worldManager.WeatherSystems)
            {
                if (weatherSystem.NearestDatum != null)
                {
                    var waterDepth = weatherSystem.NearestDatum.Depth - weatherSystem.NearestDatum.IceDepth;

                    weatherSystem.Position =
                        (weatherSystem.Position + weatherSystem.NearestDatum.CurrentFlow * delta).Normalized()
                        * (Constants.WORLD_RADIUS - Math.Max(0, waterDepth / 2 + weatherSystem.NearestDatum.IceDepth));


                    var ageFactor = (float)Math.Max(1, 500 - waterDepth);

                    weatherSystem.Age += delta * ageFactor;
                }
            }

            lock(LOCK)
            {
                foreach(var weather in _weatherToAdd)
                {
                    _worldManager.AddWeatherSystem(weather);
                }
                foreach (var weather in _weatherToRemove)
                {
                    _worldManager.RemoveWeatherSystem(weather);
                }

                _weatherToAdd.Clear();
                _weatherToRemove.Clear();
            }

            stopwatch.Stop();
            _perftracker.LogPerf("WeatherSimulator", stopwatch.ElapsedMilliseconds);
        }

        private void DoWeatherSimLogic()
        {
            try
            {
                var weatherSystems = _worldManager.WeatherSystems.ToList();

                var timeSinceUpdate = TimeSpan.FromSeconds(0);

                if(_lastUpdateTime != null)
                {
                    timeSinceUpdate = _worldManager.CurrentDate - _lastUpdateTime.Value;
                }
                _lastUpdateTime = _worldManager.CurrentDate;


                // TODO decide on keeping this or not
                //foreach(var datum in _worldManager.WorldDatums)
                //{
                //    if(datum.Depth > datum.IceDepth)
                //    {
                //        var energy = datum.Light * datum.Volcanism * 25;

                //        var stormChance = (energy - 5f) / 100000000f * timeSinceUpdate.TotalSeconds;

                //        if(_random.NextDouble() <= stormChance)
                //        {
                //            var nearbyWeatherSystems = _worldManager.WeatherSystems
                //                .Where(x => x.Position.DistanceTo(datum.Position) < 2 * Constants.DATUM_DISTANCE)
                //                .ToArray();

                //            if(!nearbyWeatherSystems.Any())
                //            {
                //                var newWeather = new WeatherSystem(
                //                        datum.Position,
                //                        energy)
                //                {
                //                    NearestDatum = datum
                //                };

                //                lock (LOCK)
                //                {
                //                    _weatherToAdd.Add(newWeather);
                //                }
                //            }
                //        }
                //    }
                //}

                var datumAroundEquator = (2 * Constants.WORLD_RADIUS * (float)Math.PI) / Constants.DATUM_DISTANCE;
                var degreesPerDatum = 360 / datumAroundEquator;

                foreach (var weatherSystem in weatherSystems)
                {
                    var datumLocation = _worldManager.WorldDatums
                        .OrderBy(d => d.Position.DistanceSquaredTo(weatherSystem.Position))
                        .FirstOrDefault();

                    if(datumLocation != null)
                    {
                        weatherSystem.NearestDatum = datumLocation;

                        var datumEnergy = datumLocation.Light * datumLocation.Volcanism * 25;

                        if(datumEnergy > weatherSystem.MaxEnergy)
                        {
                            weatherSystem.MaxEnergy += (datumEnergy - weatherSystem.MaxEnergy) * 0.1f * (float)timeSinceUpdate.TotalSeconds; 
                        }

                        weatherSystem.Energy = weatherSystem.MaxEnergy - weatherSystem.Age * 0.001f;


                        if(weatherSystem.Energy <= 0)
                        {
                            lock (LOCK)
                            {
                                //Godot.GD.Print("removing storm for low energy");
                                //Godot.GD.Print($"Age {weatherSystem.Age} Energy {weatherSystem.Energy} Max Energy {weatherSystem.MaxEnergy} Radius {weatherSystem.Radius}");
                                _weatherToRemove.Add(weatherSystem);
                            }
                        }
                    }
                    else
                    {
                        lock (LOCK)
                        {
                            _weatherToRemove.Add(weatherSystem);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error in WeatherSimulator.DoWeatherSimLogic");
            }
        }
    }
}
