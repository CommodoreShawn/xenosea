﻿using System.Linq;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class AutomaticKnownPoiTracker : ISubSimulator
    {
        private readonly IPlayerData _playerData;
        private readonly INotificationSystem _notificationSystem;

        private float _checkDelay;

        public AutomaticKnownPoiTracker(
            IPlayerData playerData,
            INotificationSystem notificationSystem)
        {
            _playerData = playerData;
            _notificationSystem = notificationSystem;
        }
        
        public void Clear()
        {
        }

        public void Initialize()
        {
        }

        public void Process(float delta)
        {
            _checkDelay -= delta;

            if(_playerData?.PlayerSub != null && _checkDelay <= 0)
            {
                _checkDelay = 25;

                var stations = _playerData.PlayerSub
                    .SonarContactRecords
                    .Select(x => x.Value)
                    .Where(x => x.Identified && x.ContactType == ContactType.Station)
                    .Select(x => x.SonarContact)
                    .OfType<WorldStation>()
                    .ToArray();

                foreach (var station in stations)
                {
                    if(!_playerData.KnownPointsOfInterest.Any(x=>x.ReferenceId == station.ReferenceId))
                    {
                        _notificationSystem.AddNotification(new Notification
                        {
                            IsGood = true,
                            Body = $"You have discovered {station.FriendlyName}."
                        });

                        _playerData.KnownPointsOfInterest.Add(
                            new Data.KnownPointOfInterest
                            {
                                ContactType = ContactType.Station,
                                FriendlyName = station.FriendlyName,
                                Location = station.BackingStation.Location,
                                ReferenceId = station.ReferenceId
                            });
                    }
                }
            }
        }
    }
}
