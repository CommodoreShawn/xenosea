﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Resources;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class StationGenerator : IStationGenerator
    {
        private const int MAX_TOWER_HEIGHT = 6;
        private readonly IWorldGenerator _worldGenerator;
        private readonly IWorldManager _worldManager;

        public StationGenerator(IWorldGenerator worldGenerator, IWorldManager worldManager)
        {
            _worldGenerator = worldGenerator;
            _worldManager = worldManager;
        }

        public void PlaceStations(
            WorldDefinition worldDefinition, 
            ReportStepProgress reportStepProgress)
        {
            var count = 0.0;
            foreach (var stationDefinition in worldDefinition.Stations)
            {
                var position = Util.LatLonToVector(stationDefinition.Latitude, stationDefinition.Longitude, Constants.WORLD_RADIUS);

                var location = _worldManager.WorldDatums
                    .OrderBy(d => d.Position.DistanceTo(position))
                    .Take(1)
                    .Where(d => d.SimulationStation == null)
                    .FirstOrDefault();

                if (location != null)
                {
                    var simulationStation = SimulationStation.BuildStation(
                        _worldManager,
                        this,
                        location,
                        stationDefinition);
                    location.SimulationStation = simulationStation;

                    _worldManager.AddSimulationStation(simulationStation);
                }
                else
                {
                    GD.Print("Error: Could not find available datum for placing station " + stationDefinition.Name);
                }

                count += 1;
                reportStepProgress(0.5 * count / worldDefinition.Stations.Length);
            }

            CalculateStationInfleunce((p) => reportStepProgress(0.5 + 0.5 * p));
        }

        public void CalculateStationInfleunce(ReportStepProgress reportStepProgress)
        {
            var count = 0f;
            var datumCount = _worldManager.WorldDatums.Count();
            foreach (var datum in _worldManager.WorldDatums)
            {
                if (datum.Depth > datum.IceDepth
                    && datum.Depth < Constants.DEPTH_MAX_SUB)
                {
                    foreach (var station in _worldManager.SimulationStations)
                    {
                        var distance = Mathf.Max(1, datum.Position.DistanceTo(station.Location.Position));

                        var influence = station.InfluenceStrength / distance;

                        if (!datum.FactionInfluence.ContainsKey(station.Faction))
                        {
                            datum.FactionInfluence.Add(station.Faction, 0);
                        }
                        datum.FactionInfluence[station.Faction] = Mathf.Max(datum.FactionInfluence[station.Faction], influence);

                        if (influence > 0.1)
                        {
                            datum.InfluenceSources.Add(station);
                        }
                    }
                }

                count += 1;
                reportStepProgress(count / datumCount);
            }
        }

        public StationBuildout GenerateStation(
            WorldDatum location,
            StationDefinition stationDefinition)
        {
            var depth = _worldGenerator.GetDepthAt(stationDefinition.Latitude, stationDefinition.Longitude);
            var height = Constants.WORLD_RADIUS - depth;
            var center = Util.LatLonToVector(stationDefinition.Latitude, stationDefinition.Longitude, height);
            var cellCenter = Util.LatLonToVector(stationDefinition.Latitude, stationDefinition.Longitude, Constants.WORLD_RADIUS);

            var datumAroundEquator = (2 * Constants.WORLD_RADIUS * Mathf.Pi) / Constants.DATUM_DISTANCE;
            var degreesPerDatum = 360 / datumAroundEquator;
            var radiusAtLat = Constants.WORLD_RADIUS * Mathf.Cos(Mathf.Deg2Rad(stationDefinition.Latitude));
            var datumsAroundLat = (2 * radiusAtLat * Mathf.Pi) / Constants.DATUM_DISTANCE;
            var degreesPerDatumAtLat = 360 / datumsAroundLat;

            var lonDegPerUnit = degreesPerDatumAtLat / Constants.DATUM_DISTANCE;
            var latDegPerUnit = degreesPerDatum / Constants.DATUM_DISTANCE;

            var stationCenter = center;

            var random = new Random($"{stationDefinition.Latitude},{stationDefinition.Longitude}".GetHashCode());

            var moduleCount = stationDefinition.EconomicProcesses.Where(x => !x.IsFarm).Count();

            List<TowerSpecification> activeTowers;

            if (stationDefinition.ModuleSet.CaveLoadout == null)
            {
                activeTowers = BuildTowers(
                    random,
                    moduleCount,
                    stationDefinition.StationShape,
                    stationDefinition.ModuleSet.GridSpacing,
                    stationDefinition.Latitude,
                    stationDefinition.Longitude,
                    latDegPerUnit,
                    lonDegPerUnit,
                    stationDefinition.ModuleSet.GroundSlopeTolerance,
                    stationCenter,
                    stationDefinition.ModuleSet.LayoutType);
            }
            else
            {
                activeTowers = BuildTowersWithCave(
                    random,
                    stationDefinition.EconomicProcesses,
                    stationDefinition.StationShape,
                    stationDefinition.ModuleSet.GridSpacing,
                    stationDefinition.Latitude,
                    stationDefinition.Longitude,
                    latDegPerUnit,
                    lonDegPerUnit,
                    stationDefinition.ModuleSet.GroundSlopeTolerance,
                    stationCenter,
                    stationDefinition.ModuleSet.CaveLoadout,
                    stationDefinition.ModuleSet);
            }

            BuildBridges(activeTowers, stationDefinition.ModuleSet.SectionHeight, random);

            var dockingTowers = DetermineDockingTowers(
                random,
                activeTowers,
                stationDefinition.TradeSourceLatitude,
                stationDefinition.TradeSourceLongitude,
                (int)Mathf.Max(1, Mathf.Sqrt(moduleCount)),
                stationDefinition.ModuleSet.GridSpacing);

            var defenseTowers = DetermineDefenseTowers(
                random,
                stationCenter,
                activeTowers,
                stationDefinition.Latitude,
                stationDefinition.Longitude,
                latDegPerUnit,
                lonDegPerUnit,
                stationDefinition.ModuleSet.GroundSlopeTolerance,
                stationDefinition.DefenseTowerCount,
                stationDefinition.AttackSourceLatitude,
                stationDefinition.AttackSourceLongitude,
                stationDefinition.ModuleSet.DefenseTowerDistance,
                stationDefinition.ModuleSet.CaveLoadout != null);

            var farmPlots = DetermineFarmPlots(
                random,
                stationCenter,
                activeTowers,
                dockingTowers,
                defenseTowers,
                stationDefinition.Latitude,
                stationDefinition.Longitude,
                latDegPerUnit,
                lonDegPerUnit,
                stationDefinition.ModuleSet.GroundSlopeTolerance,
                stationDefinition.EconomicProcesses.Where(x => x.IsFarm).Count(),
                stationDefinition.ModuleSet.FarmPlotSize,
                stationDefinition.ModuleSet.PlantSpacing);

            var towers = activeTowers
                .Select(x => new TowerBuildout
                {
                    FoundationPosition = x.FoundationPosition,
                    Sections = new AbstractEconomicProcess[x.Sections],
                    Bridges = x.Bridges.ToArray()
                })
                .ToList();

            var processesToPlace = stationDefinition.EconomicProcesses
                .Where(x => !x.IsFarm)
                .ToArray();
            random.Shuffle(processesToPlace);
            processesToPlace = processesToPlace
                .OrderBy(x => x.IsDefense ? 0 : 1)
                .ToArray();

            Tuple<TowerBuildout, int, bool>[] moduleSlots;
            if (stationDefinition.ModuleSet.CaveLoadout == null)
            {
                moduleSlots = towers
                    .SelectMany(t => Enumerable.Range(0, t.Sections.Length).Select(i => Tuple.Create(t, i, true)))
                    .ToArray();
            }
            else
            {
                var slots = new List<Tuple<TowerBuildout, int, bool>>();
                for(var i = 0; i < towers.Count; i++)
                {
                    if(i == 0)
                    {
                        slots.AddRange(Enumerable.Range(0, towers[i].Sections.Length).Select(s => Tuple.Create(towers[i], s, false)));
                    }
                    else
                    {
                        slots.AddRange(Enumerable.Range(0, towers[i].Sections.Length).Select(s => Tuple.Create(towers[i], s, true)));
                    }
                }

                moduleSlots = slots.ToArray();
            }



            random.Shuffle(moduleSlots);
            moduleSlots = moduleSlots
                .OrderBy(t => t.Item3)
                .ThenByDescending(t => t.Item2)
                .ToArray();

            var stationPosition = stationCenter;

            var modulePositions = new List<Tuple<AbstractEconomicProcess, Vector3>>();

            for(var i = 0; i < moduleSlots.Length && i < processesToPlace.Length; i++)
            {
                moduleSlots[i].Item1.Sections[moduleSlots[i].Item2] = processesToPlace[i];

                var position = moduleSlots[i].Item1.FoundationPosition
                        + moduleSlots[i].Item1.FoundationPosition.Normalized() * (moduleSlots[i].Item2 + 0.5f) * stationDefinition.ModuleSet.SectionHeight;

                if (i == 0)
                {
                    // Move station center to the tallest tower top
                    stationPosition = position;
                }

                // True for all modules in surface stations, and cave modules in cave stations
                if (moduleSlots[i].Item3)
                {
                    modulePositions.Add(Tuple.Create(processesToPlace[i], position));
                }
            }

            var tradeSource = Util.LatLonToVector(
                stationDefinition.TradeSourceLatitude, 
                stationDefinition.TradeSourceLongitude, 
                Constants.WORLD_RADIUS);

            var dockWaitingArea =  Util.GetNavigationCenter(location);

            var stationBuildout = new StationBuildout
            {
                StationModuleSet = stationDefinition.ModuleSet,
                CenterPosition = stationPosition,
                Towers = towers,
                DockingTowers = dockingTowers
                    .Select(x => new DockingTowerBuildout
                    {
                        AttachedTowerPosition = x.FromTower.FoundationPosition,
                        FoundationPosition = x.Tower.FoundationPosition
                    })
                    .ToList(),
                DefenseTowers = defenseTowers,
                FarmPlots = farmPlots,
                DockWaitingArea = dockWaitingArea,
            };

            if (stationDefinition.ModuleSet.CaveLoadout == null)
            {
                stationBuildout.CameraCenter = new Vector3(
                    towers.Average(x => x.FoundationPosition.x),
                    towers.Average(x => x.FoundationPosition.y),
                    towers.Average(x => x.FoundationPosition.z));

                if (dockingTowers.Any())
                {
                    stationBuildout.CameraDistance = dockingTowers
                        .Max(x => x.Tower.FoundationPosition.DistanceTo(stationBuildout.CameraCenter))
                        + 200;
                }
                else
                {
                    stationBuildout.CameraDistance = 600;
                }
            }
            else
            {
                stationBuildout.CameraCenter = new Vector3(
                    towers.Skip(1).Average(x => x.FoundationPosition.x),
                    towers.Skip(1).Average(x => x.FoundationPosition.y),
                    towers.Skip(1).Average(x => x.FoundationPosition.z));

                if (towers.Count > 1)
                {
                    stationBuildout.CameraDistance = towers.Skip(1)
                        .Max(x => x.FoundationPosition.DistanceTo(stationBuildout.CameraCenter))
                        + 200;
                }
                else
                {
                    stationBuildout.CameraDistance = 600;
                }
            }

            PlaceStationRooms(
                stationDefinition,
                stationBuildout,
                modulePositions,
                dockingTowers,
                random,
                stationDefinition.ModuleSet.CaveLoadout != null);

            return stationBuildout;
        }

        private void PlaceStationRooms(
            StationDefinition stationDefinition,
            StationBuildout stationBuildout, 
            List<Tuple<AbstractEconomicProcess, Vector3>> modulePositions, 
            List<DockingTowerSpecification> dockingTowers,
            Random random,
            bool isCaveStation)
        {
            var placedRooms = new List<StationRoomBuildout>();
            stationBuildout.SecurityOffice = new StationRoomBuildout
            {
                Name = stationDefinition.SecurityOfficeName,
                Position = modulePositions
                    .OrderByDescending(x => x.Item2.Length())
                    .Select(x => x.Item2 + x.Item2.Normalized() * 0.5f * stationDefinition.ModuleSet.SectionHeight)
                    .First()
            };
            placedRooms.Add(stationBuildout.SecurityOffice);

            var availableModules = modulePositions
                .Where(mp => placedRooms.Min(r => mp.Item2.DistanceTo(r.Position)) > stationDefinition.ModuleSet.SectionHeight)
                .ToArray();

            if (stationBuildout.DockingTowers.Any() && !isCaveStation)
            {
                stationBuildout.Warehouse = new StationRoomBuildout
                {
                    Name = stationDefinition.WarehouseName,
                    Position = modulePositions
                        .OrderBy(x => dockingTowers.Min(d => d.Tower.FoundationPosition.DistanceSquaredTo(x.Item2)))
                        .Select(x => x.Item2 + x.Item2.Normalized() * 0.5f * stationDefinition.ModuleSet.SectionHeight)
                        .First()
                };
                placedRooms.Add(stationBuildout.Warehouse);
            }
            else if(availableModules.Any())
            {
                stationBuildout.Warehouse = new StationRoomBuildout
                {
                    Name = stationDefinition.WarehouseName,
                    Position = availableModules[random.Next(availableModules.Length)].Item2
                };
                placedRooms.Add(stationBuildout.Warehouse);
            }

            availableModules = modulePositions
                .Where(mp => placedRooms.Min(r => mp.Item2.DistanceTo(r.Position)) > stationDefinition.ModuleSet.SectionHeight)
                .ToArray();

            if(availableModules.Any())
            {
                var options = availableModules.Where(x => x.Item1.IsCivil).ToArray();
                if(!options.Any())
                {
                    options = availableModules;
                }

                stationBuildout.Bar = new StationRoomBuildout
                {
                    Name = stationDefinition.BarName,
                    Position = availableModules[random.Next(availableModules.Length)].Item2
                };
                placedRooms.Add(stationBuildout.Bar);
            }

            availableModules = modulePositions
                .Where(mp => placedRooms.Min(r => mp.Item2.DistanceTo(r.Position)) > stationDefinition.ModuleSet.SectionHeight)
                .ToArray();

            if (availableModules.Any())
            {
                stationBuildout.EquipmentVendor = new StationRoomBuildout
                {
                    Name = stationDefinition.EquipmentShopName,
                    Position = availableModules[random.Next(availableModules.Length)].Item2
                };
                placedRooms.Add(stationBuildout.EquipmentVendor);
            }

            availableModules = modulePositions
                .Where(mp => placedRooms.Min(r => mp.Item2.DistanceTo(r.Position)) > stationDefinition.ModuleSet.SectionHeight)
                .ToArray();

            if (availableModules.Any() && !string.IsNullOrWhiteSpace(stationDefinition.BountyOfficeName))
            {
                stationBuildout.BountyOffice = new StationRoomBuildout
                {
                    Name = stationDefinition.BountyOfficeName,
                    Position = availableModules[random.Next(availableModules.Length)].Item2
                };
                placedRooms.Add(stationBuildout.BountyOffice);
            }
        }

        public StationBuildout GenerateTestStation(
            float latitude,
            float longitude,
            int moduleCount,
            StationShapeProfile stationShape,
            StationLayoutType layoutType,
            float gridSpacing,
            float groundSlopeTolerance,
            float moduleHeight,
            float tradeSourceLatitude,
            float tradeSourceLongitude,
            int dockingTowerCount,
            int defenseTowerCount,
            float attackSourceLatitude,
            float attackSourceLongitude,
            float defenseTowerDistance,
            int farmCount,
            float farmPlotSize,
            float plantSpacing)
        {
            var depth = _worldGenerator.GetDepthAt(latitude, longitude);
            var height = Constants.WORLD_RADIUS - depth;
            var center = Util.LatLonToVector(latitude, longitude, height);

            var datumAroundEquator = (2 * Constants.WORLD_RADIUS * Mathf.Pi) / Constants.DATUM_DISTANCE;
            var degreesPerDatum = 360 / datumAroundEquator;
            var radiusAtLat = Constants.WORLD_RADIUS * Mathf.Cos(Mathf.Deg2Rad(latitude));
            var datumsAroundLat = (2 * radiusAtLat * Mathf.Pi) / Constants.DATUM_DISTANCE;
            var degreesPerDatumAtLat = 360 / datumsAroundLat;

            var lonDegPerUnit = degreesPerDatumAtLat / Constants.DATUM_DISTANCE;
            var latDegPerUnit = degreesPerDatum / Constants.DATUM_DISTANCE;

            var stationCenter = center;

            var random = new Random($"{latitude},{longitude}".GetHashCode());

            var activeTowers = BuildTowers(
                random, 
                moduleCount, 
                stationShape,
                gridSpacing,
                latitude,
                longitude,
                latDegPerUnit,
                lonDegPerUnit,
                groundSlopeTolerance,
                stationCenter,
                layoutType);

            BuildBridges(activeTowers, moduleHeight, random);

            var dockingTowers = DetermineDockingTowers(
                random,
                activeTowers,
                tradeSourceLatitude,
                tradeSourceLongitude,
                dockingTowerCount,
                gridSpacing);

            var defenseTowers = DetermineDefenseTowers(
                random,
                stationCenter,
                activeTowers,
                latitude,
                longitude,
                latDegPerUnit,
                lonDegPerUnit,
                groundSlopeTolerance,
                defenseTowerCount,
                attackSourceLatitude,
                attackSourceLongitude,
                defenseTowerDistance,
                false);

            var farmPlots = DetermineFarmPlots(
                random,
                stationCenter,
                activeTowers,
                dockingTowers,
                defenseTowers,
                latitude,
                longitude,
                latDegPerUnit,
                lonDegPerUnit,
                groundSlopeTolerance,
                farmCount, 
                farmPlotSize,
                plantSpacing);

            return new StationBuildout
            {
                CenterPosition = stationCenter,
                Towers = activeTowers
                    .Select(x => new TowerBuildout
                    {
                        FoundationPosition = x.FoundationPosition,
                        Sections = new AbstractEconomicProcess[x.Sections],
                        Bridges = x.Bridges.ToArray()
                    })
                    .ToList(),
                DockingTowers = dockingTowers
                    .Select(x=> new DockingTowerBuildout
                    {
                        AttachedTowerPosition = x.FromTower.FoundationPosition,
                        FoundationPosition = x.Tower.FoundationPosition
                    })
                    .ToList(),
                DefenseTowers = defenseTowers,
                FarmPlots = farmPlots
            };
        }

        private List<FarmPlotBuildout> DetermineFarmPlots(
            Random random, 
            Vector3 stationCenter, 
            List<TowerSpecification> activeTowers, 
            List<DockingTowerSpecification> dockingTowers, 
            List<DefenseTowerBuildout> defenseTowers, 
            float latitude, 
            float longitude, 
            float latDegPerUnit, 
            float lonDegPerUnit, 
            float groundSlopeTolerance, 
            int farmCount,
            float farmPlotSize,
            float plantSpacing)
        {
            var potentialPlots = new List<FarmPlotSpecification>();

            var groundLevelCheckPoints = new Vector2[]
            {
                new Vector2(-farmPlotSize / 2, -farmPlotSize / 2),
                new Vector2(-farmPlotSize / 2, farmPlotSize / 2),
                new Vector2(farmPlotSize / 2, -farmPlotSize / 2),
                new Vector2(farmPlotSize / 2, farmPlotSize / 2),
            };

            var gridRadius = 15;

            for (var i = -gridRadius; i <= gridRadius; i++)
            {
                var y = Mathf.Sqrt2 * i * farmPlotSize / 2;

                for (var j = -gridRadius; j <= gridRadius; j++)
                {
                    float x;

                    if (i % 2 == 0)
                    {
                        x = j * farmPlotSize;
                    }
                    else
                    {
                        x = (j - 0.5f) * farmPlotSize;
                    }

                    var position = GetFoundationPosition(
                        latitude + y * latDegPerUnit,
                        longitude + x * lonDegPerUnit);
                    var positionHeight = position.Length();

                    var neighborHeights = groundLevelCheckPoints
                        .Select(p => GetFoundationPosition(
                            latitude + (y + p.y) * latDegPerUnit,
                            longitude + (x + p.x) * lonDegPerUnit))
                        .Select(v => v.Length())
                        .ToArray();

                    if (neighborHeights.All(h => Math.Abs(h - positionHeight) < groundSlopeTolerance)
                        && !activeTowers.Any(t=>t.FoundationPosition.DistanceTo(position) < farmPlotSize * 2)
                        && !dockingTowers.Any(t => t.Tower.FoundationPosition.DistanceTo(position) < farmPlotSize * 2)
                        && !defenseTowers.Any(t => t.FoundationPosition.DistanceTo(position) < farmPlotSize * 2))
                    {
                        potentialPlots.Add(new FarmPlotSpecification
                        {
                            CenterPosition = position,
                            X = x,
                            Y = y
                        });
                    }
                }
            }

            var farms = new List<FarmPlotBuildout>();
            while(potentialPlots.Any() && farms.Count < farmCount)
            {
                var newPlot = potentialPlots
                    .OrderBy(x => ScoreFarmPlotPosition(x, farms, stationCenter))
                    .First();

                potentialPlots.Remove(newPlot);

                var plantDeltas = new List<Vector2>();

                for (var u = 0f; u < farmPlotSize / 2; u += plantSpacing)
                {
                    for (var v = 0f; v < farmPlotSize / 2; v += plantSpacing)
                    {
                        if ((u != 0 || v != 0)
                            && Math.Sqrt(u * u + v * v) < farmPlotSize)
                        {
                            plantDeltas.Add(new Vector2(u, v));
                            plantDeltas.Add(new Vector2(u, -v));
                            plantDeltas.Add(new Vector2(-u, v));
                            plantDeltas.Add(new Vector2(-u, -v));
                        }
                    }
                }

                farms.Add(new FarmPlotBuildout
                {
                    CenterPosition = newPlot.CenterPosition,
                    PlantPositions = plantDeltas
                        .Distinct()
                        .Where(d => random.NextDouble() < 0.3)
                        .Select(d => GetFoundationPosition(
                            latitude + (newPlot.Y + d.y) * latDegPerUnit,
                            longitude + (newPlot.X + d.x) * lonDegPerUnit))
                        .ToArray()
                });
            }

            return farms;
        }

        private float ScoreFarmPlotPosition(
            FarmPlotSpecification potentialPlot, 
            List<FarmPlotBuildout> farmPlots, 
            Vector3 stationCenter)
        {
            var plotCost = 0f;
            if(farmPlots.Any())
            {
                plotCost = farmPlots.Min(x => x.CenterPosition.DistanceTo(potentialPlot.CenterPosition));
            }

            return plotCost
                + stationCenter.DistanceTo(potentialPlot.CenterPosition);
        }

        private List<DefenseTowerBuildout> DetermineDefenseTowers(
            Random random, 
            Vector3 stationCenter,
            List<TowerSpecification> activeTowers, 
            float latitude, 
            float longitude, 
            float latDegPerUnit, 
            float lonDegPerUnit, 
            float groundSlopeTolerance, 
            int defenseTowerCount, 
            float attackSourceLatitude, 
            float attackSourceLongitude,
            float defenseTowerDistance,
            bool isCaveStation)
        {
            var minTowerSpacing = 500;

            var stationRadius = activeTowers
                .Select(t => t.FoundationPosition.DistanceTo(stationCenter))
                .Max();

            if(isCaveStation)
            {
                stationRadius = 0;
            }

            var towerRadius = stationRadius + defenseTowerDistance;

            var radianStep = 2 * Mathf.Pi / (2 * Mathf.Pi * towerRadius / minTowerSpacing);
            var potentialTowerLocations = new List<Vector3>();

            var groundLevelCheckPoints = new Vector2[]
            {
                new Vector2(-15, -15),
                new Vector2(-15, 15),
                new Vector2(15, -15),
                new Vector2(15, 15),
            };

            for (var r = 0f; r < Mathf.Pi * 2; r += radianStep)
            {
                var towerLat = latitude + Mathf.Cos(r) * latDegPerUnit * towerRadius;
                var towerLon = longitude + Mathf.Sin(r) * lonDegPerUnit * towerRadius;

                var position = GetFoundationPosition(
                    towerLat,
                    towerLon);
                var positionHeight = position.Length();

                var neighborHeights = groundLevelCheckPoints
                    .Select(p => GetFoundationPosition(
                        towerLat + p.y * latDegPerUnit,
                        towerLon + p.x * lonDegPerUnit))
                    .Select(v => v.Length())
                    .ToArray();

                if (neighborHeights.All(h => Math.Abs(h - positionHeight) < groundSlopeTolerance))
                {
                    potentialTowerLocations.Add(position);
                }
            }

            Vector3 attackSourcePoint = Vector3.Zero;

            if(attackSourceLatitude != 0 || attackSourceLongitude != 0)
            {
                attackSourcePoint = Util.LatLonToVector(
                    attackSourceLatitude, 
                    attackSourceLongitude, 
                    Constants.WORLD_RADIUS);
            }

            var pickedTowerLocations = new List<Vector3>();

            while(potentialTowerLocations.Any() && pickedTowerLocations.Count < defenseTowerCount)
            {
                var pickedLocation = potentialTowerLocations
                    .OrderByDescending(t => ScoreDefenseTowerLocation(
                        t,
                        stationCenter,
                        pickedTowerLocations,
                        attackSourcePoint))
                    .FirstOrDefault();

                if(pickedLocation != null)
                {
                    potentialTowerLocations.Remove(pickedLocation);
                    pickedTowerLocations.Add(pickedLocation);

                }
            }

            return pickedTowerLocations
                .Select(x => new DefenseTowerBuildout
                {
                    FoundationPosition = x
                })
                .ToList();
        }

        private float ScoreDefenseTowerLocation(
            Vector3 potentialTowerLocation, 
            Vector3 stationCenter,
            List<Vector3> pickedTowerLocations, 
            Vector3 attackSourcePoint)
        {
            var heightScore = potentialTowerLocation.Length() - stationCenter.Length();

            var proximityScore = 0f;
            if(pickedTowerLocations.Any())
            {
                proximityScore = pickedTowerLocations
                    .Select(x => x.DistanceTo(potentialTowerLocation))
                    .Min();
            }

            var attackSourceScore = 0f;
            if(attackSourcePoint != Vector3.Zero)
            {
                attackSourceScore = stationCenter.DistanceTo(attackSourcePoint)
                    - potentialTowerLocation.DistanceTo(attackSourcePoint);
            }

            return heightScore * 1
                + proximityScore * 1
                + attackSourceScore * 1;
        }

        private List<DockingTowerSpecification> DetermineDockingTowers(
            Random random, 
            List<TowerSpecification> activeTowers, 
            float tradeSourceLatitude, 
            float tradeSourceLongitude,
            int dockingTowerCount,
            float gridSpacing)
        {
            var tradeSource = Util.LatLonToVector(tradeSourceLatitude, tradeSourceLongitude, Constants.WORLD_RADIUS);

            var potentialPlacements = activeTowers
                .Where(t => t.Sections >= 2)
                .SelectMany(t => t.Neighbors
                    .Where(n => n.Sections < 1 && n.Neighbors.Count(x => x.Sections > 0) == 1)
                    .Select(n => new DockingTowerSpecification
                    {
                        Tower = n,
                        FromTower = t,
                        ApproachSlot = n.Neighbors.OrderByDescending(x => x.FoundationPosition.DistanceTo(t.FoundationPosition)).FirstOrDefault(),
                    }))
                .Where(x => x.ApproachSlot.FoundationPosition.Length() < x.Tower.FoundationPosition.Length() + 100)
                .Where(x => !activeTowers.Any(t => t.FoundationPosition.DistanceTo(x.ApproachSlot.FoundationPosition) < gridSpacing))
                .OrderBy(x => x.ApproachSlot.FoundationPosition.DistanceSquaredTo(tradeSource))
                .ToArray();

            if(potentialPlacements.Length < 1)
            {
                potentialPlacements = activeTowers
                    .SelectMany(t => t.Neighbors
                        .Where(n => n.Sections < 1 && n.Neighbors.Count(x => x.Sections > 0) == 1)
                        .Select(n => new DockingTowerSpecification
                        {
                            Tower = n,
                            FromTower = t,
                            ApproachSlot = n.Neighbors.OrderByDescending(x => x.FoundationPosition.DistanceTo(t.FoundationPosition)).FirstOrDefault(),
                        }))
                    .OrderBy(x => x.ApproachSlot.FoundationPosition.DistanceSquaredTo(tradeSource))
                    .ToArray();

                if (potentialPlacements.Length < 1)
                {
                    potentialPlacements = activeTowers
                        .SelectMany(t => t.Neighbors)
                        .SelectMany(t => t.Neighbors
                            .Where(n => n.Sections < 1)
                            .Select(n => new DockingTowerSpecification
                            {
                                Tower = n,
                                FromTower = t,
                                ApproachSlot = n.Neighbors.OrderByDescending(x => x.FoundationPosition.DistanceTo(t.FoundationPosition)).FirstOrDefault(),
                            }))
                        .OrderBy(x => x.ApproachSlot.FoundationPosition.DistanceSquaredTo(tradeSource))
                        .ToArray();
                }
            }

            var placements = new List<DockingTowerSpecification>();

            while(potentialPlacements.Any() && placements.Count < dockingTowerCount)
            {
                placements.Add(potentialPlacements.First());

                potentialPlacements = potentialPlacements
                    .Where(pp => !placements.Any(p => p.Tower == pp.Tower)
                        && !placements.Any(p => p.ApproachSlot == pp.Tower))
                    .ToArray();
            }

            return placements;
        }

        private List<TowerSpecification> BuildTowers(
            Random random,
            int moduleCount,
            StationShapeProfile stationShape,
            float gridSpacing,
            float latitude,
            float longitude,
            float latDegPerUnit,
            float lonDegPerUnit,
            float groundSlopeTolerance,
            Vector3 stationCenter,
            StationLayoutType layoutType)
        {
            var foundationPositions = new List<Vector3>();

            var groundLevelCheckPoints = new Vector2[]
            {
                new Vector2(-gridSpacing / 2, -gridSpacing / 2),
                new Vector2(-gridSpacing / 2, gridSpacing / 2),
                new Vector2(gridSpacing / 2, -gridSpacing / 2),
                new Vector2(gridSpacing / 2, gridSpacing / 2),
            };

            var gridRadius = 10;

            if (layoutType == StationLayoutType.HexGrid)
            {
                for (var i = -gridRadius; i <= gridRadius; i++)
                {
                    var y = Mathf.Sqrt2 * i * gridSpacing / 2;
                    for (var j = -gridRadius; j <= gridRadius; j++)
                    {
                        float x;

                        if (i % 2 == 0)
                        {
                            x = j * gridSpacing;
                        }
                        else
                        {
                            x = (j - 0.5f) * gridSpacing;
                        }

                        var position = GetFoundationPosition(
                            latitude + y * latDegPerUnit,
                            longitude + x * lonDegPerUnit);
                        var positionHeight = position.Length();

                        var neighborHeights = groundLevelCheckPoints
                            .Select(p => GetFoundationPosition(
                                latitude + (y + p.y) * latDegPerUnit,
                                longitude + (x + p.x) * lonDegPerUnit))
                            .Select(v => v.Length())
                            .ToArray();

                        if (neighborHeights.All(h => Math.Abs(h - positionHeight) < groundSlopeTolerance))
                        {
                            foundationPositions.Add(position);
                        }
                    }
                }
            }
            else
            {
                for (var i = -gridRadius; i <= gridRadius; i++)
                {
                    for (var j = -gridRadius; j <= gridRadius; j++)
                    {
                        var x = j * gridSpacing;
                        var y = i * gridSpacing;
                        
                        var position = GetFoundationPosition(
                            latitude + y * latDegPerUnit,
                            longitude + x * lonDegPerUnit);
                        var positionHeight = position.Length();

                        var neighborHeights = groundLevelCheckPoints
                            .Select(p => GetFoundationPosition(
                                latitude + (y + p.y) * latDegPerUnit,
                                longitude + (x + p.x) * lonDegPerUnit))
                            .Select(v => v.Length())
                            .ToArray();

                        if (neighborHeights.All(h => Math.Abs(h - positionHeight) < groundSlopeTolerance))
                        {
                            foundationPositions.Add(position);
                        }
                    }
                }
            }

            var allTowers = foundationPositions
                .Select(x => new TowerSpecification
                {
                    FoundationPosition = x
                })
                .OrderBy(x => x.FoundationPosition.DistanceTo(stationCenter))
                .ToList();

            foreach (var tower in allTowers)
            {
                tower.Neighbors = allTowers
                    .Where(t => t != tower)
                    .Where(t => t.FoundationPosition.DistanceTo(tower.FoundationPosition) <= gridSpacing * 1)
                    .ToList();
            }

            var activeTowers = allTowers.Take(1).ToList();

            var maxHeight = MAX_TOWER_HEIGHT;

            for (var i = 0; i < moduleCount; i++)
            {
                int newTowerCost, addToTowerCost;
                switch (stationShape)
                {
                    case StationShapeProfile.Peak:
                        newTowerCost = activeTowers.Count * 2;
                        addToTowerCost = activeTowers.Max(t => t.Sections) * 1;
                        break;
                    case StationShapeProfile.Flat:
                    default:
                        newTowerCost = activeTowers.Count * 1;
                        addToTowerCost = activeTowers.Max(t => t.Sections) * 2;
                        break;
                }

                newTowerCost += random.Next(4);
                addToTowerCost += random.Next(4);

                if (!activeTowers.Any(t => t.Sections < maxHeight))
                {
                    addToTowerCost += 10000;
                }

                if (!activeTowers.SelectMany(t => t.Neighbors).Except(activeTowers).Any())
                {
                    newTowerCost += 10000;
                }

                if (activeTowers.Any(t => t.Sections < 2))
                {
                    newTowerCost += 10000;
                }

                if (addToTowerCost < newTowerCost && activeTowers.Any(t => t.Sections < maxHeight))
                {
                    var addTowerOptions = activeTowers
                        .Where(t => t.Sections < maxHeight)
                        .ToArray();

                    if (addTowerOptions.Any())
                    {
                        var addTower = addTowerOptions[random.Next(addTowerOptions.Length)];
                        addTower.Sections += 1;
                    }
                }
                else
                {
                    var newTowerOptions = activeTowers
                        .SelectMany(t => t.Neighbors)
                        .Except(activeTowers)
                        .ToArray();

                    if(newTowerOptions.Any(t=>t.Sections < 2))
                    {
                        newTowerOptions = newTowerOptions
                            .Where(t => t.Sections < 2)
                            .ToArray();
                    }

                    if (newTowerOptions.Any())
                    {
                        var newTower = newTowerOptions[random.Next(newTowerOptions.Length)];
                        newTower.Sections += 1;
                        activeTowers.Add(newTower);
                    }
                }
            }

            return activeTowers;
        }

        private List<TowerSpecification> BuildTowersWithCave(
            Random random,
            AbstractEconomicProcess[] economicProcesses,
            StationShapeProfile stationShape,
            float gridSpacing,
            float latitude,
            float longitude,
            float latDegPerUnit,
            float lonDegPerUnit,
            float groundSlopeTolerance,
            Vector3 stationCenter,
            CaveSpecification caveSpecification,
            StationModuleSet stationModuleSet)
        {
            var towerPositions = new List<Vector3>();

            var cave = caveSpecification.CavePrefab.Instance() as Spatial;
            foreach(var child in cave.GetChildren().OfType<Spatial>().Where(x=>x.Name.StartsWith("Tower")))
            {
                towerPositions.Add(child.Transform.origin);
            }
            cave.Free();

            var foundationPositions = new List<Vector3>();

            var groundLevelCheckPoints = new Vector2[]
            {
                new Vector2(-gridSpacing / 2, -gridSpacing / 2),
                new Vector2(-gridSpacing / 2, gridSpacing / 2),
                new Vector2(gridSpacing / 2, -gridSpacing / 2),
                new Vector2(gridSpacing / 2, gridSpacing / 2),
            };

            var gridRadius = 10;

            for (var i = -gridRadius; i <= gridRadius; i++)
            {
                var y = Mathf.Sqrt2 * i * gridSpacing / 2;
                for (var j = -gridRadius; j <= gridRadius; j++)
                {
                    float x;

                    if (i % 2 == 0)
                    {
                        x = j * gridSpacing;
                    }
                    else
                    {
                        x = (j - 0.5f) * gridSpacing;
                    }

                    var position = GetFoundationPosition(
                        latitude + y * latDegPerUnit,
                        longitude + x * lonDegPerUnit);
                    var positionHeight = position.Length();

                    var neighborHeights = groundLevelCheckPoints
                        .Select(p => GetFoundationPosition(
                            latitude + (y + p.y) * latDegPerUnit,
                            longitude + (x + p.x) * lonDegPerUnit))
                        .Select(v => v.Length())
                        .ToArray();

                    if (neighborHeights.All(h => Math.Abs(h - positionHeight) < groundSlopeTolerance))
                    {
                        foundationPositions.Add(position);
                    }
                }
            }

            var allTowers = foundationPositions
                .Select(x => new TowerSpecification
                {
                    FoundationPosition = x
                })
                .OrderBy(x => x.FoundationPosition.DistanceTo(stationCenter))
                .ToList();

            foreach (var tower in allTowers)
            {
                tower.Neighbors = allTowers
                    .Where(t => t != tower)
                    .Where(t => t.FoundationPosition.DistanceTo(tower.FoundationPosition) <= gridSpacing * 1)
                    .ToList();
            }

            var surfaceTower = allTowers.First();

            var surfaceModuleCount = economicProcesses
                .Where(x => x.IsDefense)
                .Count() + 2;

            surfaceTower.Sections = surfaceModuleCount;

            var caveModuleCount = economicProcesses
                .Where(x => !x.IsFarm)
                .Count()
                - surfaceModuleCount;

            var caveCenter = stationCenter.Normalized() * (stationCenter.Length() - 10000);
            var caveTowers = new List<TowerSpecification>();
            foreach(var towerSpot in towerPositions)
            {
                var position = Transform.Identity
                    .Translated(caveCenter)
                    .LookingAt(stationCenter, Vector3.Up)
                    .Translated(towerSpot)
                    .origin;

                // +50 as a sort of a hack for alignment
                caveTowers.Add(new TowerSpecification
                {
                    FoundationPosition = position + (position.Normalized() * 50),
                    Neighbors = new List<TowerSpecification>(),
                    Sections = 0
                });
            }

            foreach(var caveTower in caveTowers)
            {
                caveTower.Neighbors = caveTowers
                    .Where(t => t != caveTower)
                    .Where(t => t.FoundationPosition.DistanceTo(caveTower.FoundationPosition) <= stationModuleSet.GridSpacing * 1.2)
                    .OrderBy(t => t.FoundationPosition.DistanceSquaredTo(caveTower.FoundationPosition))
                    .Take(6)
                    .ToList();
            }

            var maxHeight = MAX_TOWER_HEIGHT;

            for (var i = 0; i < caveModuleCount; i++)
            {
                var availableTowers = caveTowers
                    .Where(t => t.Sections < maxHeight)
                    .ToArray();

                if(availableTowers.Any())
                {
                    availableTowers[random.Next(availableTowers.Length)].Sections += 1;
                }
            }

            return surfaceTower.Yield()
                .Concat(caveTowers.Where(x => x.Sections > 0))
                .ToList();
        }

        private Vector3 GetFoundationPosition(float latitude, float longitude)
        {
            var depth = _worldGenerator.GetDetailedDepthAt(latitude, longitude);
            var height = Constants.WORLD_RADIUS - depth;
            return Util.LatLonToVector(latitude, longitude, height);
        }

        private void BuildBridges(
            List<TowerSpecification> activeTowers, 
            float moduleHeight,
            Random random)
        {
            var existingBridges = new List<Tuple<TowerSpecification, TowerSpecification>>();

            foreach(var tower in activeTowers)
            {
                var activeNeighbors = tower.Neighbors
                    .Where(x => x.Sections > 0)
                    .ToArray();

                var towerFloor = tower.FoundationPosition.Length();

                foreach(var neighbor in activeNeighbors)
                {
                    var bridgeCount = existingBridges
                        .Where(t => (t.Item1 == tower && t.Item2 == neighbor)
                            || (t.Item1 == neighbor && t.Item2 == tower))
                        .Count();

                    if(bridgeCount < 1)
                    {
                        var neighborFloor = neighbor.FoundationPosition.Length();

                        var heightMin = Mathf.Max(
                            towerFloor,
                            neighborFloor);

                        var heightMax = Mathf.Min(
                            towerFloor + tower.Sections * moduleHeight,
                            neighborFloor + neighbor.Sections * moduleHeight);

                        if(heightMin < heightMax)
                        {
                            var bridgeHeights = new List<float>();
                            var heightRange = heightMax - heightMin;

                            if (heightRange < 40)
                            {
                                bridgeHeights.Add((heightMin + heightMax) / 2);
                            }
                            else
                            {
                                var startHeight = heightMin + random.Next(5, 15);
                                var heightStep = random.Next(15, 25);

                                for(var i = startHeight + 10; i < heightMax - 5; i += heightStep)
                                {
                                    bridgeHeights.Add(i);
                                }
                            }

                            if(bridgeHeights.Count > 1 && random.NextDouble() > 0.5)
                            {
                                bridgeHeights.RemoveAt(random.Next(bridgeHeights.Count));
                            }

                            foreach (var height in bridgeHeights)
                            {
                                existingBridges.Add(Tuple.Create(tower, neighbor));

                                tower.Bridges.Add(new BridgeBuildout
                                {
                                    From = tower.FoundationPosition.Normalized() * height,
                                    To = neighbor.FoundationPosition.Normalized() * height,
                                });
                            }
                        }
                    }
                }
            }
        }

        private class TowerSpecification
        {
            public List<BridgeBuildout> Bridges { get; set; }
            public List<TowerSpecification> Neighbors { get; set; }
            public Vector3 FoundationPosition { get; set; }
            public int Sections { get; set; }

            public TowerSpecification()
            {
                Bridges = new List<BridgeBuildout>();
                Neighbors = new List<TowerSpecification>();
            }
        }

        private class DockingTowerSpecification
        {
            public TowerSpecification Tower { get; set; }
            public TowerSpecification FromTower { get; set; }
            public TowerSpecification ApproachSlot { get; set; }
        }

        private class FarmPlotSpecification
        {
            public Vector3 CenterPosition { get; set; }
            public float X { get; set; }
            public float Y { get; set; }
        }
    }
}
