﻿using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class WorldSimulator : IWorldSimulator
    {
        private readonly IWorldManager _worldManager;
        private readonly ISubSimulator[] _subSimulators;
        private bool _initialized;

        public WorldSimulator(
            IWorldManager worldManager,
            IEnumerable<ISubSimulator> subSimulators)
        {
            _worldManager = worldManager;
            _subSimulators = subSimulators.ToArray();
        }

        public void Clear()
        {
            _initialized = false;

            foreach (var subSimulator in _subSimulators)
            {
                subSimulator.Clear();
            }
        }

        public void Process(float delta)
        {
            _worldManager.CurrentDate = _worldManager.CurrentDate.AddSeconds(delta);

            if (!_initialized)
            {
                _initialized = true;
                foreach (var subSimulator in _subSimulators)
                {
                    subSimulator.Initialize();
                }
            }

            foreach(var subSimulator in _subSimulators)
            {
                subSimulator.Process(delta);
            }

            foreach (var simulationStation in _worldManager.SimulationStations)
            {
                simulationStation.SimulationUpdate(delta);
            }
        }
    }
}
