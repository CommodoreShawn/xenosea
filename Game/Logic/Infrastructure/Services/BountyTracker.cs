﻿using System.Collections.Generic;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class BountyTracker : IBountyTracker
    {
        public IEnumerable<RegisteredBounty> AvailableBounties => _registeredBounties;
        private List<RegisteredBounty> _registeredBounties;
        private readonly INotificationSystem _notificationSystem;
        private readonly IWorldManager _worldManager;
        private readonly IPlayerData _playerData;
        private readonly IStandingTracker _standingTracker;

        public BountyTracker(
            INotificationSystem notificationSystem,
            IWorldManager worldManager,
            IPlayerData playerData,
            IStandingTracker standingTracker)
        {
            _notificationSystem = notificationSystem;
            _worldManager = worldManager;
            _playerData = playerData;
            _standingTracker = standingTracker;
            _registeredBounties = new List<RegisteredBounty>();
        }

        public void AddBounty(
            Faction targetOwner, 
            string targetName, 
            double targetValue)
        {
            foreach(var faction in _worldManager.Factions)
            {
                if (faction.OffersBountyAgainst(targetOwner))
                {
                    var bountyValue = targetValue * Constants.BOUNTY_FACTOR;

                    _registeredBounties.Add(new RegisteredBounty
                    {
                        OfferingFaction = faction,
                        TargetFaction = targetOwner,
                        TargetName = targetName,
                        BountyValue = bountyValue
                    });

                    _notificationSystem.AddNotification(new Notification
                    {
                        IsGood = true,
                        Body = $"{faction.Name} Bounty registered for: {bountyValue.ToString("C0")}"
                    });
                }
            }
        }

        public void RedeemBounty(RegisteredBounty bounty)
        {
            _playerData.Money += bounty.BountyValue;
            _registeredBounties.Remove(bounty);
            _standingTracker.AddStandingChange(
                bounty.OfferingFaction,
                bounty.BountyValue * Constants.CASH_TO_STANDING,
                "Redeemed bounty.");
        }
    }
}
