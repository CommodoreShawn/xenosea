﻿using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class SonarContactTracker : ISonarContactTracker
    {
        private List<ISonarContact> _sonarContacts;

        public SonarContactTracker()
        {
            _sonarContacts = new List<ISonarContact>();
        }

        public void AddSonarContact(ISonarContact sonarContact)
        {
            _sonarContacts.Add(sonarContact);
        }

        public IEnumerable<ISonarContact> GetSonarContacts()
        {
            return _sonarContacts
                .Where(i => Object.IsInstanceValid(i as Node));
        }

        public void RemoveSonarContact(ISonarContact sonarContact)
        {
            _sonarContacts.Remove(sonarContact);
        }

        public void SetVisibleContacts(Submarine playerSub, IEnumerable<ISonarContact> visibleContacts)
        {
            foreach(var contact in _sonarContacts)
            {
                contact.IsPlayerVisible = contact == playerSub
                    || contact == playerSub.IsDockedTo;
            }

            foreach(var contact in visibleContacts)
            {
                contact.IsPlayerVisible = true;
            }
        }

        public SonarDetectionInfo RunSonarDetectionLogic(
            World world,
            Transform detectorGlobalTransform,
            float detectorSensitivity, 
            bool towedArrayActive, 
            ISonarContact contact)
        {
            var collisionInfo = world.DirectSpaceState
                .IntersectRay(
                    detectorGlobalTransform.origin,
                    contact.RealPosition,
                    null,
                    0b00000000000000000010,
                    collideWithBodies: true,
                    collideWithAreas: true);

            var isTerrainBlocked = collisionInfo.Count > 0;

            var detectorGain = 0f;

            var angleTo = detectorGlobalTransform.basis.z.AngleTo(detectorGlobalTransform.origin.DirectionTo(contact.RealPosition));


            if (angleTo < 0.78)
            {
                detectorGain = 0.7f;
            }
            else if (angleTo < 2.35)
            {
                detectorGain = towedArrayActive ? 2.0f : 1.0f;
            }
            else
            {
                detectorGain = towedArrayActive ? 0.6f : 0.1f;
            }

            detectorGain *= detectorSensitivity;

            var distance = (detectorGlobalTransform.origin - contact.RealPosition).Length();

            var detectedNoise = contact.OverallNoise / (4 * Mathf.Pi * Mathf.Pow(Constants.SONAR_ENV_PROPAGATION * distance, 2)) * detectorGain;

            return new SonarDetectionInfo
            {
                SensorGain = detectorGain,
                Distance = distance,
                IsTerrainBlocked = isTerrainBlocked,
                Noise = detectedNoise
            };
        }
    }
}
