﻿using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.addons.XenoTalk;
using Xenosea.addons.XenoTalk.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class ResourceLocator : IResourceLocator
    {
        private List<AgentDefinition> _agentDefinitions;
        private List<AmmunitionType> _ammunitionTypes;
        private List<CommodityType> _commodityTypes;
        private List<DiveTeamType> _diveTeamTypes;
        private List<MinisubType> _minisubTypes;
        private List<TorpedoType> _torpedoTypes;
        private List<ConvoTopic> _conversationTopics;
        private List<FixedPointOfInterest> _fixedPointOfInterests;
        private List<Npc> _npcs;
        private List<BasicModuleType> _moduleTypes;
        private List<SubHullType> _subHullTypes;
        private List<ArmorType> _armorTypes;
        private List<AbstractEconomicProcess> _economicProcesses;
        private List<Faction> _factions;
        private List<StationModuleSet> _moduleSets;

        public IEnumerable<SubHullType> AllSubHullTypes => _subHullTypes;
        public IEnumerable<ArmorType> AllArmorTypes => _armorTypes;
        public IEnumerable<TorpedoType> AllTorpedoTypes => _torpedoTypes;
        public IEnumerable<MinisubType> AllMinisubTypes => _minisubTypes;
        public IEnumerable<DiveTeamType> AllDiveTeamTypes => _diveTeamTypes;
        public IEnumerable<BasicModuleType> AllModuleTypes => _moduleTypes;

        public ResourceLocator()
        {
            _agentDefinitions = ListResources<AgentDefinition>("res://Resources/Worldgen/AgentDefinitions/").ToList();
            _ammunitionTypes = ListResources<AmmunitionType>("res://Resources/Equipment/Ammunition/").ToList();
            _commodityTypes = ListResources<CommodityType>("res://Resources/Simulation/Commodities/").ToList();
            _diveTeamTypes = ListResources<DiveTeamType>("res://Resources/Equipment/DiveTeams/").ToList();
            _minisubTypes = ListResources<MinisubType>("res://Resources/Equipment/Minisubs/").ToList();
            _torpedoTypes = ListResources<TorpedoType>("res://Resources/Equipment/Torpedoes/").ToList();
            _fixedPointOfInterests = ListResources<FixedPointOfInterest>("res://Resources/Worldgen/FixedPois/").ToList();
            _moduleTypes = ListResources<BasicModuleType>("res://Resources/Equipment/Modules/")
                .ToList();
            _subHullTypes = ListResources<SubHullType>("res://Resources/Equipment/SubHulls/").ToList();
            _armorTypes = ListResources<ArmorType>("res://Resources/Equipment/Armor/").ToList();

            _conversationTopics = ListFiles("res://Conversations/", ".xml")
                .Select(x => DialogueParser.Parse(x))
                .SelectMany(x => x)
                .ToList();

            _npcs = ListResources<Npc>("res://Resources/Npcs/").ToList();

            _economicProcesses = Enumerable.Empty<AbstractEconomicProcess>()
                .Concat(ListResources<EconomicProcess>("res://Resources/Simulation/Economy/EconomicProcess/"))
                .Concat(ListResources<FarmingEconomicProcess>("res://Resources/Simulation/Economy/Farming/"))
                .Concat(ListResources<MiningEconomicProcess>("res://Resources/Simulation/Economy/Mining/"))
                .ToList();

            _factions = ListResources<Faction>("res://Resources/Simulation/Factions/")
                .Concat(ListResources<Faction>("res://Resources/Simulation/Factions/Hudson/"))
                .ToList();

            _moduleSets = ListResources<StationModuleSet>("res://Resources/Worldgen/StationModuleSets/")
                .ToList();
        }

        public AgentDefinition GetAgentDefinition(string name)
        {
            var result = _agentDefinitions
                .Where(x => x.Name == name)
                .FirstOrDefault();

            if(result == null)
            {
                throw new System.Exception($"Could not find AgentDefinition with name {name}");
            }

            return result;
        }

        public AmmunitionType GetAmmoType(string name)
        {
            var result = _ammunitionTypes
                .Where(x => x.Name == name)
                .FirstOrDefault();

            if (result == null)
            {
                throw new System.Exception($"Could not find AmmunitionType with name {name}");
            }

            return result;
        }

        public CommodityType GetCommodityType(string name)
        {
            var result = _commodityTypes
                .Where(x => x.Name == name)
                .FirstOrDefault();

            if (result == null)
            {
                throw new System.Exception($"Could not find CommodityType with name {name}");
            }

            return result;
        }

        public DiveTeamType GetDiveTeamType(string name)
        {
            var result = _diveTeamTypes
                .Where(x => x.Name == name)
                .FirstOrDefault();

            if (result == null)
            {
                throw new System.Exception($"Could not find DiveTeamType with name {name}");
            }

            return result;
        }

        public MinisubType GetMinisubType(string name)
        {
            var result = _minisubTypes
                 .Where(x => x.Name == name)
                 .FirstOrDefault();

            if (result == null)
            {
                throw new System.Exception($"Could not find MinisubType with name {name}");
            }

            return result;
        }

        public TorpedoType GetTorpedoType(string name)
        {
            var result = _torpedoTypes
                .Where(x => x.Name == name)
                .FirstOrDefault();

            if (result == null)
            {
                throw new System.Exception($"Could not find TorpedoType with name {name}");
            }

            return result;
        }

        public List<ConvoTopic> GetConversationTopics()
        {
            return _conversationTopics;
        }

        private IEnumerable<TResource> ListResources<TResource>(string path) where TResource : Resource
        {
            var results = new List<TResource>();
            Directory directory = new Directory();
            var err = directory.Open(path);

            if (err != Error.Ok)
            {
                GD.PrintErr($"Unable to load directory {path}: {err}");
            }

            directory.ListDirBegin(true);

            var files = new List<string>();

            AddDirectoryContents(directory, files);

            foreach (var file in files.Where(f => f.EndsWith(".tres")))
            {
                try
                {
                    var resource = ResourceLoader.Load<TResource>(file);

                    if (resource != null)
                    {
                        results.Add(resource);
                    }
                }
                catch (System.Exception ex)
                {
                    GD.PrintErr(ex.Message);
                }
            }

            return results;
        }

        private IEnumerable<string> ListFiles(string path, string extension)
        {
            var results = new List<string>();
            Directory directory = new Directory();
            var err = directory.Open(path);

            if (err != Error.Ok)
            {
                GD.PrintErr($"Unable to load directory {path}: {err}");
            }

            directory.ListDirBegin(true);

            var files = new List<string>();

            AddDirectoryContents(directory, files);

            foreach (var file in files.Where(f => f.EndsWith(extension)))
            {
                try
                {
                    results.Add(file);
                }
                catch (System.Exception ex)
                {
                    GD.PrintErr(ex.Message);
                }
            }

            return results;
        }

        private void AddDirectoryContents(Directory dir, List<string> files)
        {
            var fileName = dir.GetNext();

            while (fileName != string.Empty)
            {
                var path = dir.GetCurrentDir() + "/" + fileName;

                if (dir.CurrentIsDir())
                {
                    var subDir = new Directory();
                    subDir.Open(path);
                    subDir.ListDirBegin(true, false);
                    AddDirectoryContents(subDir, files);
                }
                else
                {
                    files.Add(path);
                }

                fileName = dir.GetNext();
            }
        }

        public FixedPointOfInterest GetFixedPoi(string name)
        {
            var result = _fixedPointOfInterests
                .Where(x => x.Name == name)
                .FirstOrDefault();

            if (result == null)
            {
                throw new System.Exception($"Could not find FixedPointOfInterest with name {name}");
            }

            return result;
        }

        public Npc GetNpc(string name)
        {
            var result = _npcs
                 .Where(x => x.Name == name)
                 .FirstOrDefault();

            if (result == null)
            {
                throw new System.Exception($"Could not find Npc with name {name}");
            }

            return result;
        }

        public TModule GetModuleType<TModule>(string name) where TModule : BasicModuleType
        {
            var result = _moduleTypes
                .OfType<TModule>()
                .Where(x => x.ModuleTypeId == name)
                .FirstOrDefault();

            if (result == null)
            {
                throw new System.Exception($"Could not find ModuleType {nameof(TModule)} with Id {name}");
            }

            return result;
        }

        public SubHullType GetSubHull(string name)
        {
            var result = _subHullTypes
                 .Where(x => x.SubHullTypeId == name)
                 .FirstOrDefault();

            if (result == null)
            {
                throw new System.Exception($"Could not find SubHullType with Id {name}");
            }

            return result;
        }

        public ArmorType GetArmorType(string name)
        {
            var result = _armorTypes
                 .Where(x => x.ArmorTypeId == name)
                 .FirstOrDefault();

            if (result == null)
            {
                throw new System.Exception($"Could not find ArmorType with Id {name}");
            }

            return result;
        }

        public AbstractEconomicProcess GetEconomicProcess(string name)
        {
            var result = _economicProcesses
                 .Where(x => x.Name == name)
                 .FirstOrDefault();

            if (result == null)
            {
                throw new System.Exception($"Could not find EconomicProcess with Name {name}");
            }

            return result;
        }

        public Faction GetFaction(string name)
        {
            var result = _factions
                 .Where(x => x.Name == name)
                 .FirstOrDefault();

            if (result == null)
            {
                throw new System.Exception($"Could not find Faction with Name {name}");
            }

            return result;
        }

        public StationModuleSet GetStationModuleSet(string name)
        {
            var result = _moduleSets
                 .Where(x => x.Name == name)
                 .FirstOrDefault();

            if (result == null)
            {
                throw new System.Exception($"Could not find StationModuleSet with Name {name}");
            }

            return result;
        }
    }
}
