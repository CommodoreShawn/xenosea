﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.AI;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.Infrastructure.Missions;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class WreckMissionsGenerator : ISubSimulator, ISeafloorObjectSpawner
    {
        private static readonly string TYPE_WRECK = "mission_wreck";

        private readonly IWorldManager _worldManager;
        private readonly IPerftracker _perftracker;
        private readonly IMissionTracker _missionTracker;
        private readonly ISimulationTradeMonitor _tradeMonitor;
        private readonly ISimulationEventBus _simulationEventBus;
        private readonly IWorldGenerator _worldGenerator;
        private readonly IPlayerData _playerData;
        private readonly IResourceLocator _resourceLocator;
        private readonly INotificationSystem _notificationSystem;
        private readonly Random _random;

        private float _missionExpiryCheckTimer;

        public WreckMissionsGenerator(
            IWorldManager worldManager,
            IPerftracker perftracker,
            IMissionTracker missionTracker,
            ISimulationTradeMonitor tradeMonitor,
            ISimulationEventBus simulationEventBus,
            IWorldGenerator worldGenerator,
            IResourceLocator resourceLocator,
            INotificationSystem notificationSystem,
            IPlayerData playerData)
        {
            _worldManager = worldManager;
            _perftracker = perftracker;
            _missionTracker = missionTracker;
            _tradeMonitor = tradeMonitor;
            _simulationEventBus = simulationEventBus;
            _worldGenerator = worldGenerator;
            _resourceLocator = resourceLocator;
            _notificationSystem = notificationSystem;
            _playerData = playerData;

            _random = new Random();

            _simulationEventBus.OnSimulationEvent += OnSimulationEvent;
        }

        public void Clear()
        {
        }

        public void Initialize()
        {
        }

        private void OnSimulationEvent(ISimulationEvent evt)
        {
            if (evt is SimulationAgentDestroyedSimulationEvent deadAgentEvent)
            {
                MakeSalvageMission(deadAgentEvent);
            }
            else if (evt is PlayerSalvagedSimulationEvent salvageEvent
                && salvageEvent.SalvagedSub is Submarine sub)
            {
                var mission = _missionTracker.GetCurrentMissions(_playerData.PlayerSub)
                    .OfType<AbstractWreckMission>()
                    .Where(x => x.Wreck.Id == sub.Name)
                    .FirstOrDefault();

                if(mission != null)
                {
                    if(mission is RetrieveLogFromWreckMission logMission)
                    {
                        logMission.LogRetrieved = true;

                        _notificationSystem.AddNotification(new Notification
                        {
                            IsGood = true,
                            IsMessage = true,
                            IsImportant = true,
                            Body = $"Your salvage team has retrieved the log book from {sub.FriendlyName}."
                        });
                    }
                }
            }
        }

        private void MakeSalvageMission(SimulationAgentDestroyedSimulationEvent deadAgentEvent)
        {
            var deadAgent = deadAgentEvent.SimulationAgent;

            if (deadAgent.HomeStation != null)
            {
                var isMerchant = false;
                AgentDefinition definition = null;
                for (var i = 0; i < deadAgent.HomeStation.SupportedAgents.Length; i++)
                {
                    if (deadAgent.HomeStation.SupportedAgents[i] == deadAgent)
                    {
                        definition = deadAgent.HomeStation.SupportedAgentDefinitions[i];
                        isMerchant = deadAgent.HomeStation.SupportedAgentDefinitions[i].IsTrader;
                    }
                }

                if (definition != null && !deadAgent.InDetailedSim)
                {
                    var existingWreckMissions = _missionTracker.GetAvailableMissions(deadAgent.HomeStation)
                        .OfType<AbstractWreckMission>()
                        .ToList();

                    var totalAgents = deadAgent.HomeStation.SupportedAgentDefinitions
                        .Count();

                    if (existingWreckMissions.Count < totalAgents / 2)
                    {
                        var locationThreat = deadAgent.Location.FactionInfluence
                            .Where(kvp => kvp.Key.IsHostileTo(deadAgent.Faction))
                            .Sum(kvp => kvp.Value) * 4;

                        var wreckDatum = deadAgent.Location;
                        var wreckValue = deadAgent.SubmarineLoadout.HullType.Value / 15f;
                        var threatValue = locationThreat * 5000;

                        var distanceToAgent = Util.EstimateTravelDistance(deadAgent.HomeStation.Location, wreckDatum);

                        var waypointPositions = wreckDatum.Neighbors
                            .Concat(wreckDatum.Yield())
                            .Where(x => x.IceDepth < x.Depth)
                            .ToArray();

                        var waypointDatum = waypointPositions[_random.Next(waypointPositions.Length)];

                        var wreck = new SeafloorObject
                        {
                            Id = Guid.NewGuid().ToString(),
                            Type = TYPE_WRECK,
                            Datum = wreckDatum,
                            IsDead = false,
                            Position = Util.LatLonToVector(
                                wreckDatum.Latitude + (float)(_random.NextDouble() - 0.5f),
                                wreckDatum.Longitude + (float)(_random.NextDouble() - 0.5f),
                                Constants.WORLD_RADIUS - _worldGenerator.GetDetailedDepthAt(wreckDatum.Latitude, wreckDatum.Longitude) + 4),
                            Data = new Dictionary<string, string>
                                {
                                    { "agent_definition", definition.Name },
                                    { "name", $"{deadAgent.Faction.Name} {definition.AgentNameRole}" },
                                    { "faction", deadAgent.Faction.Name }
                                }
                        };

                        foreach (var commodity in deadAgent.CommodityStorage.CommoditiesPresent)
                        {
                            var qty = (int)deadAgent.CommodityStorage.GetQty(commodity);

                            if (qty > 0)
                            {
                                var toKeep = _random.Next(qty);

                                if (toKeep > 0)
                                {
                                    wreck.Data.Add(commodity.Name, toKeep.ToString());
                                }
                            }
                        }

                        var expireTime = _worldManager.CurrentDate + TimeSpan.FromDays(3);

                        var r = _random.Next(2);
                        r = 0;
                        AbstractWreckMission mission = null;
                        if (r == 0 || !isMerchant)
                        {
                            _worldManager.AddSeafloorObject(wreck);
                            _simulationEventBus.SendEvent(new NewSeafloorObjectSimulationEvent(wreck));

                            mission = new RetrieveLogFromWreckMission(
                                $"Retrieve the log of {deadAgent.Faction.Name} {definition.AgentNameRole}",
                                deadAgent.Faction,
                                threatValue,
                                0,
                                TimeSpan.FromSeconds(distanceToAgent / 5),
                                Util.GetNavigationCenter(waypointDatum),
                                wreck,
                                expireTime,
                                deadAgent.HomeStation);
                        }
                        else
                        {
                            // TODO deliver cargo from wreck mission
                        }

                        if (mission != null)
                        {
                            //Godot.GD.Print("Wreck mission at: " + deadAgent.HomeStation.Name);
                            _missionTracker.AddMission(deadAgent.HomeStation, mission);
                        }


                        // TODO generate one of the mission types
                        // Retreieve logs from wreck
                        // retrieve cargo and deliver it
                        // stop salvage operation? (it;s like log book, but with a time limit)

                        //var newMission = new SalvageWreckMission(
                        //    deadAgent.Faction,
                        //    wreckValue + threatValue,
                        //    )

                        //_missionTracker.AddMission(deadAgent.HomeStation, newMission);
                    }
                }
            }
        }

        public void Process(float delta)
        {
            _missionExpiryCheckTimer -= delta;

            if(_missionExpiryCheckTimer <= 0)
            {
                _missionExpiryCheckTimer = _random.Next(30, 60);

                foreach (var station in _worldManager.SimulationStations)
                {
                    if (_playerData.PlayerSub?.IsDockedTo != station)
                    {

                        var expiredMissions = _missionTracker.GetAvailableMissions(station)
                            .OfType<AbstractWreckMission>()
                            .Where(x => x.ExpireTime <= _worldManager.CurrentDate)
                            .ToArray();

                        foreach(var mission in expiredMissions)
                        {
                            Godot.GD.Print("Expired wreck mission at: " + station.Name);
                            _missionTracker.DestroyMission(mission, station);
                        }
                    }
                }
            }
        }

        public bool CanHandle(SeafloorObject seafloorObject)
        {
            return seafloorObject.Type == TYPE_WRECK;
        }

        public void Spawn(DatumCell datumCell, SeafloorObject seafloorObject)
        {
            var definition = _resourceLocator.GetAgentDefinition(seafloorObject.Data["agent_definition"]);
            var faction = _worldManager.Factions.Where(x => x.Name == seafloorObject.Data["faction"]).FirstOrDefault();

            var wreckSub = definition.SubLoadout.HullType.SubPrefab.Instance() as Submarine;
            wreckSub.Name = seafloorObject.Id;
            wreckSub.FriendlyName = seafloorObject.Data["name"];
            wreckSub.Faction = faction;

            var random = new Random(datumCell.WorldDatum.Id.GetHashCode());

            var lookPos = seafloorObject.Position
                + new Godot.Vector3(
                    random.Next(-100, 100),
                    random.Next(-100, 100),
                    random.Next(-100, 100));

            wreckSub.LookAtFromPosition(
                seafloorObject.Position,
                lookPos,
                seafloorObject.Position * 2);

            datumCell.GetParent().AddChild(wreckSub);

            wreckSub.BuildModules(definition.SubLoadout);
            wreckSub.Health = 0;
            wreckSub.Mode = Godot.RigidBody.ModeEnum.Static;
            wreckSub.IsMissionWreck = true;
            wreckSub.IsWrecked = true;
        }
    }
}
