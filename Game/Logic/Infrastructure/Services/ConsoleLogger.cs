﻿using System;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class ConsoleLogger : ILogger
    {
        public void Error(string message)
        {
            Godot.GD.PrintErr(message);
        }

        public void Error(Exception ex, string message)
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                Godot.GD.PrintErr(message);
            }
            Godot.GD.PrintErr($"{ex.GetType()}: {ex.Message}");
            Godot.GD.PrintErr(ex.StackTrace);

            if(ex.InnerException != null)
            {
                Error(ex.InnerException, string.Empty);
            }
        }

        public void Info(string message)
        {
            Godot.GD.Print(message);
        }
    }
}
