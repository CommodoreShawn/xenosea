﻿using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.Infrastructure.Missions;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class EconomicLogger : IEconomicLogger
    {
        private readonly ILogger _logger;
        private bool _loggingActive;

        public EconomicLogger(ILogger logger)
        {
            _logger = logger;
            _loggingActive = false;
        }

        public void LogMissionDeliver(SimulationAgent agent, ITradeMission tradeMission)
        {
            if(_loggingActive)
            {
                _logger.Info($"{agent.Faction.Name} {agent.Name} delivered mission: {tradeMission.Commodity.Name} to {tradeMission.DestinationName}");
            }
        }

        public void LogMissionPickup(SimulationAgent agent, ITradeMission tradeMission)
        {
            if (_loggingActive)
            {
                _logger.Info($"{agent.Faction.Name} {agent.Name} picked up mission: {tradeMission.Commodity.Name} to {tradeMission.DestinationName}");
            }
        }
    }
}
