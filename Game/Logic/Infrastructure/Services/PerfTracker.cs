﻿using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class PerfTracker : IPerftracker
    {
        private Dictionary<string, long> _totals;
        private Dictionary<string, int> _counts;

        public PerfTracker()
        {
            _totals = new Dictionary<string, long>();
            _counts = new Dictionary<string, int>();
        }

        public void LogPerf(string name, long elapsedMilliseconds)
        {
            if(!_totals.ContainsKey(name))
            {
                _totals.Add(name, 0);
                _counts.Add(name, 0);
            }

            _totals[name] += elapsedMilliseconds;
            _counts[name] += 1;
        }

        public void PrintPerfInfo(int framesSinceLast)
        {
            Godot.GD.Print($"========================");
            foreach (var key in _totals.Keys.ToArray())
            {
                if (_counts[key] > 0)
                {
                    var avg = _totals[key] / (double)_counts[key];

                    Godot.GD.Print($"{key}: {avg.ToString("F2")} ms per call; {(_totals[key] / framesSinceLast).ToString("F2")} ms per frame; {(_counts[key] / framesSinceLast).ToString("F1")} calls per frame");

                    _totals[key] = 0;
                    _counts[key] = 0;
                }
            }
        }
    }
}
