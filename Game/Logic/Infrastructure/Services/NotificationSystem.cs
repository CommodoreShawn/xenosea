﻿using System.Linq;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class NotificationSystem : INotificationSystem
    {
        public event NotificationHandler OnNotification;

        private IPlayerData _playerData;

        public NotificationSystem(IPlayerData playerData)
        {
            _playerData = playerData;
        }

        public void AddNotification(Notification notification)
        {
            OnNotification?.Invoke(notification);
        }

        public void Broadcast(ISonarContact sender, string message)
        {
            if(string.IsNullOrWhiteSpace(message))
            {
                return;
            }

            if (sender == _playerData.PlayerSub)
            {
                sender.SpeakingTimer = 2;
                AddNotification(new Notification
                {
                    IsMessage = true,
                    Body = $"{sender.FriendlyName}: {message}"
                });
            }
            else if (_playerData.PlayerSub != null)
            { 
                var contact = _playerData.PlayerSub.SonarContactRecords.Values
                    .Where(r => r.SonarContact == sender && r.IsDetected)
                    .FirstOrDefault();

                if (contact != null && contact.SignalLevel > 0)
                {
                    if (contact.Identified)
                    {
                        sender.SpeakingTimer = 2;
                        AddNotification(new Notification
                        {
                            IsMessage = true,
                            Body = $"{sender.FriendlyName}: {message}"
                        });
                    }
                    else
                    {
                        sender.SpeakingTimer = 2;
                        AddNotification(new Notification
                        {
                            IsMessage = true,
                            Body = $"Unknown: {message}"
                        });
                    }
                }
            }
        }
    }
}