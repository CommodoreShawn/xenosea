﻿using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class SimToEngineLink : ISimToEngineLink
    {
        public Gameplay GameplayInstance { get; set; }

        public void Despawn(SimulationAgent agent)
        {
            if(agent.DetailedRepresentation != null)
            {
                agent.DetailedRepresentation.QueueFree();
                agent.DetailedRepresentation = null;
            }
        }

        public void Spawn(SimulationAgent agent)
        {
            if (GameplayInstance != null)
            {
                agent.DetailedRepresentation = GameplayInstance.SpawnAgent(agent);
            }
        }
    }
}
