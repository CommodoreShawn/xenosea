﻿using System;
using System.Linq;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class StandingTracker : IStandingTracker
    {
        private readonly INotificationSystem _notificationSystem;
        private readonly IWorldManager _worldManager;
        private Faction _playerFaction;

        public StandingTracker(
            INotificationSystem notificationSystem,
            IWorldManager worldManager)
        {
            _notificationSystem = notificationSystem;
            _worldManager = worldManager;
        }

        public void Initialize(Faction playerFaction)
        {
            _playerFaction = playerFaction;
        }

        public void AddStandingChange(
            Faction toFaction, 
            double standingChange, 
            string description)
        {
            if (standingChange != 0)
            {
                _notificationSystem.AddNotification(
                    new Notification
                    {
                        IsGood = standingChange > 0,
                        IsBad = standingChange < 0,
                        Body = $"Standing with {toFaction.Name} changed by {standingChange.ToString("F1")}."
                    });

                AdjustStanding(_playerFaction, toFaction, standingChange);
                AdjustStanding(toFaction, _playerFaction, standingChange);

                foreach(var factionName in toFaction.FactionRelations.Keys)
                {
                    if(factionName != _playerFaction.Name)
                    {

                        var factor = toFaction.FactionRelations[factionName] / 100.0;

                        var otherFaction = _worldManager.Factions
                            .Where(f => f.Name == factionName)
                            .FirstOrDefault();

                        if(otherFaction != null)
                        {
                            AdjustStanding(_playerFaction, otherFaction, standingChange * 0.5 * factor);
                            AdjustStanding(otherFaction, _playerFaction, standingChange * 0.5 * factor);
                        }
                    }
                }
            }
        }

        private void AdjustStanding(Faction from, Faction to, double change)
        {
            if (!from.FactionRelations.ContainsKey(to.Name))
            {
                from.FactionRelations.Add(to.Name, 0);
            }

            from.FactionRelations[to.Name] = Math.Max(Math.Min(from.FactionRelations[to.Name] + change, 100), -100);
        }

        public void Clear()
        {
        }
    }
}
