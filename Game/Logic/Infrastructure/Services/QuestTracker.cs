﻿using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class QuestTracker : IQuestTracker, ISubSimulator, IStateTrackingService
    {
        private List<IQuest> _quests;
        public IEnumerable<IQuest> AllQuests => _quests;
        private readonly ISimulationEventBus _simulationEventBus;

        public QuestTracker(ISimulationEventBus simulationEventBus)
        {
            _quests = new List<IQuest>();
            _simulationEventBus = simulationEventBus;
            _simulationEventBus.OnSimulationEvent += OnSimulationEvent;
        }

        private void OnSimulationEvent(ISimulationEvent evt)
        {
            foreach(var quest in _quests)
            {
                quest.OnSimulationEvent(evt);
            }
        }

        public Dictionary<string, string> GetAllQuestStates()
        {
            return _quests
                .ToDictionary(q => q.Id, q => q.CurrentStateId);
        }

        public IQuest GetQuest(string questId)
        {
            return _quests
                .Where(q => q.Id == questId)
                .FirstOrDefault();
        }

        public string GetQuestState(string questId)
        {
            return _quests
                .Where(q => q.Id == questId)
                .FirstOrDefault()?.CurrentStateId;
        }

        public void InitializeQuests(IEnumerable<IQuest> quests)
        {
            _quests.Clear();
            _quests.AddRange(quests);
            foreach(var quest in quests)
            {
                quest.Reset();
                quest.Initialize();
            }
        }

        public void Initialize()
        {
        }

        public void Process(float delta)
        {
            foreach(var quest in _quests)
            {
                quest.Process(delta);
            }
        }

        public void Clear()
        {
        }

        public void ClearState()
        {
            foreach (var quest in _quests)
            {
                quest.Reset();
                quest.Initialize();
            }
        }

        public void SaveState(SerializationObject root)
        {
            var serviceRoot = root.AddChild("QuestTracker");
            foreach (var quest in _quests)
            {
                var questRoot = serviceRoot.AddChild("quest")
                    .SetAttribute("id", quest.Id);

                quest.SerializeTo(questRoot);
            }
        }

        public void LoadState(IResourceLocator resourceLocator, SerializationObject root)
        {
            var serviceRoot = root.GetChild("QuestTracker");
            foreach (var questRoot in serviceRoot.Children)
            {
                var quest = questRoot.LookupReferenceOrNull("id", _quests, q => q.Id);
                if (quest != null)
                {
                    quest.DeserializeFrom(questRoot);
                }
            }
        }
    }
}
