﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class CrimeTracker : ICrimeTracker, ISubSimulator
    {
        private readonly ISonarContactTracker _sonarContactTracker;
        private readonly IStandingTracker _standingTracker;
        private readonly INotificationSystem _notificationSystem;
        private List<Crime> _crimeRecords;

        public CrimeTracker(
            ISonarContactTracker sonarContactTracker,
            IStandingTracker standingTracker,
            INotificationSystem notificationSystem)
        {
            _sonarContactTracker = sonarContactTracker;
            _standingTracker = standingTracker;
            _notificationSystem = notificationSystem;
            _crimeRecords = new List<Crime>();
        }

        public void Clear()
        {
            _crimeRecords.Clear();
        }

        public void ReportCrime(ICombatant offender, ICombatant victim, float actualDamage)
        {
            var stationWitnesses = _sonarContactTracker.GetSonarContacts()
                .OfType<IStation>()
                .Where(x => x.SonarContactRecords.Any(scr => scr.SonarContact == victim && scr.IsDetected && scr.Identified))
                .Where(x => x.SonarContactRecords.Any(scr => scr.SonarContact == offender && scr.IsDetected && scr.Identified))
                .Where(x => !x.Faction.IsHostileTo(victim.Faction))
                .ToArray();

            if(stationWitnesses.Any())
            {
                ApplyCrimeToStanding(victim.FriendlyName, victim.Faction, actualDamage);
            }
            else
            {
                var submarineWitnesses = _sonarContactTracker.GetSonarContacts()
                    .OfType<Submarine>()
                    .Where(x => !x.IsPlayerSub)
                    .Where(x => x.SonarContactRecords.Values.Any(scr => scr.SonarContact == victim && scr.IsDetected && scr.Identified))
                    .Where(x => x.SonarContactRecords.Values.Any(scr => scr.SonarContact == offender && scr.IsDetected && scr.Identified))
                    .Where(x => !x.Faction.IsHostileTo(victim.Faction))
                    .ToList();

                if (victim is Submarine submarine)
                {
                    submarineWitnesses.Add(submarine);
                }

                if(submarineWitnesses.Any())
                {
                    _crimeRecords.Add(new Crime
                    {
                        VictimName = victim.FriendlyName,
                        VictimFaction = victim.Faction,
                        DamageDealt = actualDamage,
                        Witnesses = submarineWitnesses
                    });
                }
            }
        }

        private void ApplyCrimeToStanding(
            string victimName,
            Faction victimFaction, 
            float damageDealt)
        {
            _standingTracker.AddStandingChange(
                victimFaction,
                damageDealt * -0.002,
                "Assault");
        }

        public void Process(float delta)
        {
            foreach(var crime in _crimeRecords)
            {
                crime.Witnesses.RemoveAll(x => !Godot.Object.IsInstanceValid(x) || x.IsWrecked);

                if (crime.Witnesses.Any(x => x.IsDockedTo != null))
                {
                    ApplyCrimeToStanding(crime.VictimName, crime.VictimFaction, crime.DamageDealt);
                    crime.Witnesses.Clear();
                }
            }

            _crimeRecords.RemoveAll(x => !x.Witnesses.Any());
        }

        public void Initialize()
        {
        }

        public void ReportPiracy(ICombatant offender, ICombatant victim)
        {
            var submarineWitnesses = _sonarContactTracker.GetSonarContacts()
                .OfType<Submarine>()
                .Where(x => !x.IsPlayerSub)
                .Where(x => x.SonarContactRecords.Values.Any(scr => scr.SonarContact == victim && scr.IsDetected && scr.Identified))
                .Where(x => x.SonarContactRecords.Values.Any(scr => scr.SonarContact == offender && scr.IsDetected && scr.Identified))
                .Where(x => !x.Faction.IsHostileTo(victim.Faction))
                .ToList();

            foreach(var submarine in submarineWitnesses)
            {
                var controller = submarine.GetChildren()
                    .OfType<GoalBotController>()
                    .FirstOrDefault();

                if(controller != null && controller.IsSecurityResponder)
                {
                    var contactRecord = submarine.SonarContactRecords.Values
                        .Where(scr => scr.SonarContact == offender && scr.IsDetected && scr.Identified)
                        .FirstOrDefault();

                    if(contactRecord != null)
                    {
                        contactRecord.AggrivationScore += 100;
                    }
                }
            }
        }

        private class Crime
        {
            public string VictimName { get; set; }
            public Faction VictimFaction { get; set; }
            public float DamageDealt { get; set; }
            public List<Submarine> Witnesses { get; set; }
        }
    }
}
