﻿using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class SimulationEventBus : ISimulationEventBus
    {
        public event SimulationEventHandler OnSimulationEvent;

        public void SendEvent(ISimulationEvent evt)
        {
            OnSimulationEvent?.Invoke(evt);
        }
    }
}
