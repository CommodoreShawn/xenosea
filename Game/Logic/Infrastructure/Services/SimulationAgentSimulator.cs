﻿using System;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class SimulationAgentSimulator : ISubSimulator
    {
        private readonly IWorldManager _worldManager;
        private readonly IPlayerData _playerData;
        private readonly ISimToEngineLink _simToEngineLink;
        private readonly Random _random;
        private readonly ISimulationEventBus _simulationEventBus;

        public SimulationAgentSimulator(
            IWorldManager worldManager,
            IPlayerData playerData,
            ISimToEngineLink simToEngineLink,
            ISimulationEventBus simulationEventBus)
        {
            _worldManager = worldManager;
            _playerData = playerData;
            _simToEngineLink = simToEngineLink;
            _random = new Random();
            _simulationEventBus = simulationEventBus;
        }

        public void Clear()
        {
        }

        public void Initialize()
        {
        }

        public void Process(float delta)
        {
            var deadAgents = _worldManager.SimulationAgents.Where(x => x.Health <= 0).ToArray();
            foreach(var deadAgent in deadAgents)
            {
                _simulationEventBus.SendEvent(new SimulationAgentDestroyedSimulationEvent(deadAgent));
                _worldManager.RemoveSimulationAgent(deadAgent);
            }

            foreach (var agent in _worldManager.SimulationAgents)
            {
                if(agent.InDetailedSim)
                {
                    if(!agent.Location.InDetailedSim
                        && _playerData.PlayerSub != null
                        && (agent.DetailedRepresentation.RealPosition - _playerData.PlayerSub.RealPosition).Length() > _playerData.DetailedSimDistance)
                    {
                        _simToEngineLink.Despawn(agent);
                    }
                }
                else if(agent.Location.InDetailedSim)
                {
                    _simToEngineLink.Spawn(agent);
                }

                agent.SimulationUpdate(delta, _random);
            }
        }
    }
}
