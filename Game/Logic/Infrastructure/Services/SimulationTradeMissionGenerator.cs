﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.Infrastructure.Missions;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class SimulationTradeMissionGenerator : ISubSimulator, ISimulationTradeMonitor
    {
        private readonly IWorldManager _worldManager;
        private readonly IPerftracker _perftracker;
        private readonly IMissionTracker _missionTracker;
        private List<TradeRoute> _tradeRoutes;
        private Dictionary<SimulationStation, List<TradeRoute>> _tradeRoutesBySource;
        private Dictionary<SimulationStation, List<TradeRoute>> _tradeRoutesByDestination;
        
        public SimulationTradeMissionGenerator(
            IWorldManager worldManager,
            IPerftracker perftracker,
            IMissionTracker missionTracker)
        {
            _worldManager = worldManager;
            _perftracker = perftracker;
            _missionTracker = missionTracker;
            _tradeRoutes = new List<TradeRoute>();
            _tradeRoutesBySource = new Dictionary<SimulationStation, List<TradeRoute>>();
            _tradeRoutesByDestination = new Dictionary<SimulationStation, List<TradeRoute>>();
        }

        public void Clear()
        {
            _tradeRoutes.Clear();
            _tradeRoutesBySource.Clear();
            _tradeRoutesByDestination.Clear();
        }

        public IEnumerable<SimulationStation> GetExports(SimulationStation source, CommodityType commodity)
        {
            if(_tradeRoutesBySource.ContainsKey(source))
            {
                return _tradeRoutesBySource[source]
                    .Where(x => x.Commodity == commodity)
                    .Select(x => x.To);
            }

            return Enumerable.Empty<SimulationStation>();
        }

        public IEnumerable<SimulationStation> GetImports(SimulationStation destination, CommodityType commodity)
        {
            if (_tradeRoutesByDestination.ContainsKey(destination))
            {
                return _tradeRoutesByDestination[destination]
                    .Where(x => x.Commodity == commodity)
                    .Select(x => x.From);
            }

            return Enumerable.Empty<SimulationStation>();
        }

        public void Initialize()
        {
            _tradeRoutes.Clear();
            _tradeRoutesBySource.Clear();
            _tradeRoutesByDestination.Clear();

            foreach(var station in _worldManager.SimulationStations)
            {
                _tradeRoutesBySource.Add(station, new List<TradeRoute>());
                _tradeRoutesByDestination.Add(station, new List<TradeRoute>());
            }

            var tradeLinks = BuildTradeLinks();

            _tradeRoutes = BuildTradeRoutes(tradeLinks);

            foreach(var tradeRoute in _tradeRoutes)
            {
                _tradeRoutesBySource[tradeRoute.From].Add(tradeRoute);
                _tradeRoutesByDestination[tradeRoute.To].Add(tradeRoute);
            }
        }

        private Dictionary<SimulationStation, List<SimulationStation>> BuildTradeLinks()
        {
            var links = new Dictionary<SimulationStation, List<SimulationStation>>();
            foreach(var station in _worldManager.SimulationStations)
            {
                links.Add(station, new List<SimulationStation>());

                foreach (var otherStation in _worldManager.SimulationStations.Where(s => s != station))
                {
                    var distance = Util.EstimateTravelDistance(station.Location, otherStation.Location);

                    if (distance <= station.TradeDistance
                        && distance <= otherStation.TradeDistance)
                    {
                        links[station].Add(otherStation);
                    }
                }

                //Godot.GD.Print($"Trade Links from {station.Name}: {string.Join(", ", links[station].Select(x => x.Name))}");
            }

            return links;
        }

        private List<TradeRoute> BuildTradeRoutes(Dictionary<SimulationStation, List<SimulationStation>> tradeLinks)
        {
            var tradeRoutes = new List<TradeRoute>();

            foreach(var toStation in _worldManager.SimulationStations)
            {
                var potentialSourceStations = _worldManager.SimulationStations
                    .Where(s => s != toStation)
                    .ToArray();

                foreach (var fromStation in potentialSourceStations)
                {
                    var commoditiesToTrade = fromStation.Exports
                        .Intersect(toStation.Imports)
                        .Except(toStation.Faction.Contraband)
                        .ToArray();

                    foreach (var commodity in commoditiesToTrade)
                    {
                        var path = FindTradeLinkPath(fromStation, toStation, tradeLinks, false);

                        if (path != null)
                        {
                            //Godot.GD.Print($"Trade Route: {commodity.Name} from {fromStation.Name} to {toStation.Name}");
                            //Godot.GD.Print(string.Join(" => ", path.Select(x => x.Name)));

                            while (path.Count > 1)
                            {
                                tradeRoutes.Add(new TradeRoute
                                {
                                    IsSmuggling = path[1].Faction.Contraband.Contains(commodity),
                                    Commodity = commodity,
                                    EstimatedDistance = Util.EstimateTravelDistance(path[0].Location, path[1].Location),
                                    ForFaction = path[1].Faction.TradeSubfaction ?? path[1].Faction,
                                    From = path[0],
                                    To = path[1]
                                });

                                path.RemoveAt(0);
                            }
                        }
                    }
                }

                foreach (var smuggler in toStation.FactionsPresent.Where(fp => fp.SmugglingMissionCommodities.Any()))
                {
                    potentialSourceStations = _worldManager.SimulationStations
                        .Where(s => s != toStation)
                        .ToArray();

                    foreach (var fromStation in potentialSourceStations)
                    {
                        var commoditiesToSmuggle = fromStation.Exports
                            .Intersect(toStation.Imports)
                            .Where(c => toStation.Faction.Contraband.Contains(c) && smuggler.SmugglingMissionCommodities.Contains(c))
                            .ToArray();

                        foreach (var commodity in commoditiesToSmuggle)
                        {
                            var path = FindTradeLinkPath(fromStation, toStation, tradeLinks, true);

                            if (path != null)
                            {
                                //Godot.GD.Print($"Smuggling Route: {commodity.Name} from {fromStation.Name} to {toStation.Name}");
                                //Godot.GD.Print(string.Join(" => ", path.Select(x => x.Name)));

                                while (path.Count > 1)
                                {
                                    tradeRoutes.Add(new TradeRoute
                                    {
                                        IsSmuggling = path[1].Faction.Contraband.Contains(commodity),
                                        Commodity = commodity,
                                        EstimatedDistance = Util.EstimateTravelDistance(path[0].Location, path[1].Location),
                                        ForFaction = path[1].Faction.TradeSubfaction ?? path[1].Faction,
                                        From = path[0],
                                        To = path[1]
                                    });

                                    path.RemoveAt(0);
                                }
                            }
                        }
                    }
                }
            }

            return tradeRoutes;
        }

        private List<SimulationStation> FindTradeLinkPath(
            SimulationStation fromStation, 
            SimulationStation toStation, 
            Dictionary<SimulationStation, List<SimulationStation>> tradeLinks,
            bool isSmuggling)
        {
            var open = new List<SimulationStation>();
            var closed = new List<SimulationStation>();
            var cameFrom = new Dictionary<SimulationStation, SimulationStation>();
            var distTo = new Dictionary<SimulationStation, float>();

            open.Add(fromStation);
            cameFrom.Add(fromStation, null);
            distTo.Add(fromStation, 0);

            while(open.Any())
            {
                var current = open.First();
                closed.Add(current);
                open.Remove(current);

                if(current == toStation)
                {
                    return ConstructTradeLinkPath(current, cameFrom);
                }

                IEnumerable<SimulationStation> potentialNeighbors;
                if(!isSmuggling)
                {
                    potentialNeighbors = tradeLinks[current]
                        .Where(n => n.Faction.GetStandingTo(current.Faction) >= n.Faction.MinimumFactionStandingForTrade);
                }
                else
                {
                    potentialNeighbors = tradeLinks[current]
                        .Where(n => !n.Faction.IsHostileTo(current.Faction));
                }

                foreach(var neighbor in potentialNeighbors.Except(closed))
                {
                    var dist = Util.EstimateTravelDistance(current.Location, neighbor.Location) + distTo[current];

                    if(open.Contains(neighbor))
                    {
                        if(dist < distTo[neighbor])
                        {
                            distTo[neighbor] = dist;
                            cameFrom[neighbor] = current;
                        }
                    }
                    else
                    {
                        open.Add(neighbor);
                        distTo.Add(neighbor, dist);
                        cameFrom.Add(neighbor, current);
                    }
                }
            }

            return null;
        }

        private List<SimulationStation> ConstructTradeLinkPath(
            SimulationStation current, 
            Dictionary<SimulationStation, SimulationStation> cameFrom)
        {
            var path = new List<SimulationStation>();

            while(current != null)
            {
                path.Add(current);
                current = cameFrom[current];
            }

            path.Reverse();

            return path;
        }

        public void Process(float delta)
        {
            var stopwatch = System.Diagnostics.Stopwatch.StartNew();

            foreach (var group in _tradeRoutes.GroupBy(t => t.Commodity))
            {
                var commodity = group.Key;

                var expectedQuantities = _worldManager.SimulationStations
                    .OfType<SimulationStation>()
                    .ToDictionary(s => s, s => s.CommodityStorage.GetQty(commodity));

                foreach(var tradeMission in _missionTracker.GetTradeMissionsByCommodity(commodity).OfType<SimulationTradeMission>())
                {
                    expectedQuantities[tradeMission.Destination] += tradeMission.Quantity;
                }
                
                foreach(var tradeRoute in group)
                {
                    var missionAmount = 100;

                    if(expectedQuantities[tradeRoute.From] - missionAmount > expectedQuantities[tradeRoute.To])
                    {
                        expectedQuantities[tradeRoute.From] -= missionAmount;
                        tradeRoute.From.CommodityStorage.Remove(commodity, missionAmount);
                        _missionTracker.AddMission(
                            tradeRoute.From,
                            new SimulationTradeMission(
                                tradeRoute.ForFaction,
                                tradeRoute.To.PriceOf(commodity) * missionAmount - tradeRoute.From.PriceOf(commodity) * missionAmount * 0.7,
                                tradeRoute.From.PriceOf(commodity) * missionAmount * 0.7,
                                TimeSpan.FromSeconds(tradeRoute.EstimatedDistance / 5),
                                tradeRoute.From,
                                tradeRoute.To,
                                commodity,
                                missionAmount,
                                tradeRoute.IsSmuggling));
                    }
                }
            }

            stopwatch.Stop();
            _perftracker.LogPerf("EconomicMissionGenerator", stopwatch.ElapsedMilliseconds);
        }

        public IEnumerable<Tuple<SimulationStation, CommodityType>> GetExports(SimulationStation source)
        {
            if (_tradeRoutesBySource.ContainsKey(source))
            {
                return _tradeRoutesBySource[source]
                    .Select(x => Tuple.Create(x.To, x.Commodity));
            }

            return Enumerable.Empty<Tuple<SimulationStation, CommodityType>>();
        }

        public IEnumerable<Tuple<SimulationStation, CommodityType>> GetImports(SimulationStation destination)
        {
            if (_tradeRoutesBySource.ContainsKey(destination))
            {
                return _tradeRoutesByDestination[destination]
                    .Select(x => Tuple.Create(x.From, x.Commodity));
            }

            return Enumerable.Empty<Tuple<SimulationStation, CommodityType>>();
        }

        private class TradeRoute
        {
            public SimulationStation From { get; set; }
            public SimulationStation To { get; set; }
            public Faction ForFaction { get; set; }
            public CommodityType Commodity { get; set; }
            public bool IsSmuggling { get; set; }
            public float EstimatedDistance { get; set; }
        }
    }
}
