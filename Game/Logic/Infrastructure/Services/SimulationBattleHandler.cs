﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class SimulationBattleHandler : ISubSimulator, ISimulationBattleHandler, IStateTrackingService
    {
        private static readonly object LOCK = new object();

        private List<SimulationBattle> _simulationBattles;
        private float _attackCooldown;
        private Task _battleSimTask;
        private Random _random;

        private bool _debugOn = false;

        private readonly ILogger _logger;
        private readonly IWorldManager _worldManager;

        public SimulationBattleHandler(
            ILogger logger,
            IWorldManager worldManager)
        {
            _logger = logger;
            _worldManager = worldManager;
            _simulationBattles = new List<SimulationBattle>();
            _random = new Random();
        }

        public void Clear()
        {
            _simulationBattles.Clear();
            _attackCooldown = 0;
            _battleSimTask = null;
        }

        public SimulationBattle GetOrStartBattle(WorldDatum location)
        {
            lock (LOCK)
            {
                var battleAtLocation = _simulationBattles
                    .Where(b => b.Location == location)
                    .FirstOrDefault();

                if (battleAtLocation == null)
                {
                    BattleDebug($"Starting battle at {location.Id}");
                    battleAtLocation = new SimulationBattle(location);
                    _simulationBattles.Add(battleAtLocation);
                }

                return battleAtLocation;
            }
        }

        public void Initialize()
        {
        }

        public void Process(float delta)
        {
            var battlesToDissolve = _simulationBattles
                .Where(x => x.Location.InDetailedSim)
                .ToArray();
            foreach(var battle in battlesToDissolve)
            {
                BattleDebug($"Ending battle at {battle.Location.Id} due to player proximity.");
                foreach (var participant in battle.Participants)
                {
                    participant.CurrentBattle = null;
                }
                battle.Participants.Clear();
                _simulationBattles.Remove(battle);
            }

            _attackCooldown -= delta;

            if(_battleSimTask == null)
            {
                if(_attackCooldown <= 0)
                {
                    _attackCooldown = 10;

                    foreach(var battle in _simulationBattles)
                    {
                        if(battle.Participants.Count < 2)
                        {
                            BattleDebug($"Ending battle at {battle.Location.Id}");

                            foreach(var participant in battle.Participants)
                            {
                                participant.CurrentBattle = null;
                            }
                            battle.Participants.Clear();
                        }
                    }

                    _simulationBattles.RemoveAll(x => x.Participants.Count < 2);

                    _battleSimTask = Task.Run(DoBattleSimLogic);
                }
            }
            else if(_battleSimTask.Status != TaskStatus.Running)
            {
                _battleSimTask = null;
            }
        }

        private void DoBattleSimLogic()
        {
            try
            {
                SimulationBattle[] battles;

                lock (LOCK)
                {
                    battles = _simulationBattles.ToArray();
                }

                foreach (var battle in battles)
                {
                    var participants = battle.Participants.ToArray();

                    BattleDebug($"Simulating battle at {battle.Location.Id}");
                    BattleDebug($"Participants:");

                    foreach (var participant in participants)
                    {
                        BattleDebug($"    {participant.Faction.Name} {participant.Name} {participant.Id}");
                    }

                    foreach (var participant in participants)
                    {
                        var anyHostilePresent = participants
                            .Any(o => participant.Faction.IsHostileTo(o.Faction) || o.Faction.IsHostileTo(participant.Faction));

                        var leftBattle = false;

                        if(!anyHostilePresent)
                        {
                            BattleDebug($"{participant.Faction.Name} {participant.Name} {participant.Id}: no hostiles, leaving");
                            leftBattle = true;
                            participant.CurrentBattle = null;
                            participant.BattleEscapeTimer = 60;
                        }
                        else if(participant.Goal != null && participant.Goal.AvoidBattle)
                        {
                            var caught = false;

                            BattleDebug($"{participant.Faction.Name} {participant.Name} {participant.Id}: trying to escape");
                            var catchers = participants
                                .Where(p => p.Faction.IsHostileTo(participant.Faction))
                                .Where(p => !(p.Goal?.AvoidBattle ?? false))
                                .ToArray();

                            foreach(var catcher in catchers)
                            {
                                if(!caught && _random.NextDouble() > participant.Stealth)
                                {
                                    BattleDebug($"{participant.Faction.Name} {participant.Name} {participant.Id}: caught by {catcher.Faction.Name} {catcher.Name} {catcher.Id}");
                                    caught = true;
                                }
                                else
                                {
                                    BattleDebug($"{participant.Faction.Name} {participant.Name} {participant.Id}: escaped {catcher.Faction.Name} {catcher.Name} {catcher.Id}");
                                }
                            }

                            if(!caught)
                            {
                                BattleDebug($"{participant.Faction.Name} {participant.Name} {participant.Id}: escaped!");
                                leftBattle = true;
                                participant.CurrentBattle = null;
                                participant.BattleEscapeTimer = 60;
                            }
                        }

                        if(!leftBattle)
                        {
                            var targets = participants
                                .Where(p => participant.Faction.IsHostileTo(p.Faction))
                                .ToArray();

                            if(targets.Any())
                            {
                                var target = targets[_random.Next(targets.Length)];

                                var attackerScore = participant.Agility + _random.NextDouble();
                                var defenderScore = target.Agility + _random.NextDouble();

                                BattleDebug($"{participant.Faction.Name} {participant.Name} {participant.Id}: attacks {target.Faction.Name} {target.Name} {target.Id}");

                                if (_random.NextDouble() < participant.Evasion)
                                {
                                    BattleDebug($"{target.Faction.Name} {target.Name} {target.Id} evades the attack");
                                }
                                else
                                {
                                    if (attackerScore > defenderScore)
                                    {
                                        var damage = participant.FocusedDamage * (1 - target.Armor);
                                        target.Health -= damage;
                                        BattleDebug($"Attacks with advantage and deals {damage} ({target.Health} health remaining).");
                                        
                                    }
                                    else
                                    {
                                        var damage = participant.WideDamage * (1 - target.Armor);
                                        target.Health -= damage;
                                        BattleDebug($"Attacks without advantage and deals {damage} ({target.Health} health remaining).");
                                    }
                                }
                            }
                        }
                        else
                        {
                            battle.Participants.Remove(participant);
                        }
                    }

                    foreach (var participant in battle.Participants.Where(p => p.Health <= 0))
                    {
                        BattleDebug($"{participant.Faction.Name} {participant.Name} {participant.Id} is destroyed");
                    }

                    battle.Participants.RemoveAll(p => p.Health <= 0);
                }
            }
            catch(Exception e)
            {
                _logger.Error(e, "Error in SimulationBattleHandler.DoBattleSimLogic");
            }
        }

        private void BattleDebug(string message)
        {
            if(_debugOn)
            {
                Godot.GD.Print(message);
            }
        }

        public void ClearState()
        {
            Clear();
        }

        public void SaveState(SerializationObject root)
        {
            var serviceRoot = root.AddChild("SimulationBattleHandler");

            foreach(var battle in _simulationBattles)
            {
                var battleRoot = serviceRoot.AddChild("battle");
                battleRoot.SetAttribute("location_id", battle.Location.Id);
                battleRoot.AddList("participants", battle.Participants.Select(x => x.Id));
            }
        }

        public void LoadState(IResourceLocator resourceLocator, SerializationObject root)
        {
            var serviceRoot = root.GetChild("SimulationBattleHandler");

            foreach (var battleRoot in serviceRoot.Children.Where(x => x.Name == "battle"))
            {
                var location = battleRoot.LookupReference("location_id", _worldManager.WorldDatums, x => x.Id);
                var participants = battleRoot.GetList("participants")
                    .Select(x => _worldManager.SimulationAgents.Where(a => a.Id == x).FirstOrDefault())
                    .Where(x => x != null)
                    .ToArray();

                var battle = new SimulationBattle(location);
                battle.Participants.AddRange(participants);
                _simulationBattles.Add(battle);
            }
        }
    }
}
