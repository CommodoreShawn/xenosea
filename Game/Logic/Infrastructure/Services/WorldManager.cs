﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Services
{
    public class WorldManager : IWorldManager, IStateTrackingService
    {
        private List<HubPoint> _hubs;
        private List<Faction> _factions;
        private List<WorldDatum> _worldDatums;
        private List<SimulationStation> _simulationStations;
        private List<SimulationAgent> _simulationAgents;
        private List<WeatherSystem> _weatherSystems;
        private List<SeafloorObject> _seafloorObjects;

        public DateTime CurrentDate { get; set; }
        public IEnumerable<HubPoint> Hubs => _hubs;
        public IEnumerable<Faction> Factions => _factions;
        public IEnumerable<WorldDatum> WorldDatums => _worldDatums;
        public IEnumerable<SimulationStation> SimulationStations => _simulationStations;
        public IEnumerable<SimulationAgent> SimulationAgents => _simulationAgents;
        public IEnumerable<WeatherSystem> WeatherSystems => _weatherSystems;
        public IEnumerable<SeafloorObject> SeafloorObjects => _seafloorObjects;

        public DateTime SessionStartTime { get; set; }
        

        public WorldManager()
        {
            _hubs = new List<HubPoint>();
            _factions = new List<Faction>();
            _worldDatums = new List<WorldDatum>();
            _simulationStations = new List<SimulationStation>();
            _simulationAgents = new List<SimulationAgent>();
            _weatherSystems = new List<WeatherSystem>();
            _seafloorObjects = new List<SeafloorObject>();
        }

        public void Clear()
        {
            _hubs.Clear();
            CurrentDate = new DateTime(685, 12, 10);
            _factions.Clear();
            _worldDatums.Clear();
            _simulationStations.Clear();
            _simulationAgents.Clear();
            _weatherSystems.Clear();
        }

        public void AddHubPoint(HubPoint hubPoint)
        {
            _hubs.Add(hubPoint);
        }

        public void AddFaction(Faction faction)
        {
            if (!_factions.Contains(faction))
            {
                _factions.Add(faction);
            }
        }

        public void AddDatums(IEnumerable<WorldDatum> worldDatums)
        {
            _worldDatums.AddRange(worldDatums);
        }

        public void AddSimulationStation(SimulationStation station)
        {
            //Godot.GD.Print($"Adding {station.Name}");
            //Godot.GD.Print($"\tAgent Support     : {station.EconomicProcesses.Sum(x => x.AgentSupport)}");
            //Godot.GD.Print($"\tMilitary Support  : {station.EconomicProcesses.Sum(x => x.MilitarySupport)}");
            //Godot.GD.Print($"\tIndustrial Support: {station.EconomicProcesses.Sum(x => x.IndustrialSupport)}");

            station.Location.SimulationStation = station;
            _simulationStations.Add(station);
        }

        public void AddSimulationAgent(SimulationAgent agent)
        {
            if (agent != null)
            {
                _simulationAgents.Add(agent);
            }
        }

        public void RemoveSimulationAgent(SimulationAgent deadAgent)
        {
            _simulationAgents.Remove(deadAgent);
        }

        public void ClearState()
        {
            Clear();
        }

        public void SaveState(SerializationObject root)
        {
            var serviceRoot = root.AddChild("WorldManager");
            serviceRoot.SetAttribute("date", CurrentDate.ToString());
            var factionRoot = serviceRoot.AddChild("factions");
            foreach(var faction in Factions)
            {
                faction.SaveState(factionRoot.AddChild(faction.Name));
            }

            var stationRoot = serviceRoot.AddChild("stations");
            foreach (var station in SimulationStations)
            {
                station.SaveState(stationRoot.AddChild(station.Name));
            }

            var agentRoot = serviceRoot.AddChild("agents");
            foreach (var agent in SimulationAgents)
            {
                agent.SaveState(agentRoot.AddChild("agent"));
            }

            foreach(var weatherSystem in WeatherSystems)
            {
                weatherSystem.SaveState(serviceRoot.AddChild("weather_system"));
            }

            foreach(var seafloorObject in SeafloorObjects)
            {
                seafloorObject.SerializeTo(serviceRoot.AddChild("seafloor_object"));
            }
        }

        public void LoadState(IResourceLocator resourceLocator, SerializationObject root)
        {
            var serviceRoot = root.GetChild("WorldManager");
            CurrentDate = Convert.ToDateTime(serviceRoot.GetAttribute("date"));
            
            foreach(var factionRoot in serviceRoot.GetChild("factions").Children)
            {
                var faction = Factions
                    .Where(f => f.Name == factionRoot.Name)
                    .FirstOrDefault();

                if(faction != null)
                {
                    faction.LoadState(factionRoot);
                }
            }

            foreach (var stationRoot in serviceRoot.GetChild("stations").Children)
            {
                AddSimulationStation(
                    SimulationStation.LoadState(
                        resourceLocator,
                        this,
                        stationRoot));
            }

            foreach(var agentRoot in serviceRoot.GetChild("agents").Children)
            {
                AddSimulationAgent(SimulationAgent.LoadState(resourceLocator, this, agentRoot));
            }

            foreach (var stationChild in serviceRoot.GetChild("stations").Children)
            {
                var station = SimulationStations
                    .Where(f => f.Name == stationChild.Name)
                    .FirstOrDefault();

                if (station != null)
                {
                    station.LoadSupportedAgentState(this, stationChild);
                }
            }

            foreach(var weatherChild in serviceRoot.Children.Where(x=>x.Name == "weather_system"))
            {
                AddWeatherSystem(WeatherSystem.LoadState(weatherChild));
            }

            foreach(var seafloorObjectChild in serviceRoot.Children.Where(x => x.Name == "seafloor_object"))
            {
                var seafloorObject = SeafloorObject.DeserializeFrom(_worldDatums, seafloorObjectChild);

                if (seafloorObject != null)
                {
                    AddSeafloorObject(seafloorObject);
                }
            }
        }

        public void AddWeatherSystem(WeatherSystem weatherSystem)
        {
            _weatherSystems.Add(weatherSystem);
        }

        public void RemoveWeatherSystem(WeatherSystem weatherSystem)
        {
            _weatherSystems.Remove(weatherSystem);
        }

        public void AddSeafloorObject(SeafloorObject obj)
        {
            obj.Datum.SeafloorObjects.Add(obj);
            _seafloorObjects.Add(obj);
        }

        public void RemoveSeafloorObject(SeafloorObject obj)
        {
            obj.Datum.SeafloorObjects.Remove(obj);
            _seafloorObjects.Remove(obj);
        }
    }
}
