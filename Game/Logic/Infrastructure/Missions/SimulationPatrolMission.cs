﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Missions
{
    public class SimulationPatrolMission : AbstractMission, IPatrolMission
    {
        public SimulationStation FromStation { get; }
        public int PatrolPath { get; }
        public override string Name => $"Patrol near {FromStation.Name}";

        public override string Description => $"Follow the patrol route, destroy any enemies encoutered, and return to {FromStation.Name}.";

        public override string Detail0 => $"Complete By: {DueDate.Value.ToShortTimeString()} {DueDate.Value.ToShortDateString()}";
        public override string Detail1 => $"Waypoints: {MissionPath?.Waypoints?.Count}";
        public override string Detail2 => $"Reward: {Reward.ToString("C0")}";

        public override bool CanBeSerialized => true;

        private bool _patrolComplete;
        private bool _sentCompleteMessage;

        private List<List<Tuple<Faction, AgentDefinition>>> _spawnsByWaypoint;
        
        public SimulationPatrolMission(
            Faction forFaction, 
            double reward, 
            double deposit,
            TimeSpan allowedTime,
            SimulationStation fromStation,
            int patrolPath) 
            : base(forFaction, reward, deposit, allowedTime)
        {
            FromStation = fromStation;
            PatrolPath = patrolPath;
        }

        public override bool CanBeDelivered(Submarine agent)
        {
            return agent.IsDockedTo is WorldStation ws 
                && ws.BackingStation == FromStation
                && _patrolComplete;
        }

        public override void ProcessPlayerLogic(
            INotificationSystem notificationSystem,
            Submarine playerSub, 
            bool isTrackedMission,
            float delta)
        {
            if (MissionPath == null)
            {
                var routeThreat = FromStation.PerimiterPatrols[PatrolPath]
                    .SelectMany(d => d.FactionInfluence)
                    .Where(kvp => kvp.Key.IsHostileTo(ForFaction))
                    .Sum(kvp => kvp.Value);

                MissionPath = new MissionPath(
                    FromStation.PerimiterPatrols[PatrolPath]
                    .Select(x => Util.GetNavigationCenter(x)));

                _spawnsByWaypoint = new List<List<Tuple<Faction, AgentDefinition>>>();

                foreach (var waypoint in MissionPath.Waypoints)
                {
                    _spawnsByWaypoint.Add(new List<Tuple<Faction, AgentDefinition>>());
                }

                var random = new Random();
                
                var spawnValue = (float)random.Next(
                    (int)(routeThreat / 2),
                    (int)(routeThreat));

                spawnValue = Math.Max(spawnValue, 1);

                GD.Print($"Threat: {routeThreat} Spawns: {spawnValue}");

                var enemyOptions = FromStation.PerimiterPatrols[PatrolPath]
                    .SelectMany(d => d.InfluenceSources)
                    .Where(s => s.Faction.IsHostileTo(ForFaction))
                    .SelectMany(s => s.SupportedAgentDefinitions.Where(x => x.CanBePatrolEnemy).Select(x => Tuple.Create(s.Faction, x)))
                    .ToArray();

                //GD.Print($"Enemy factions: " + string.Join("\n ", enemyOptions.Select(x => x.Item1).Distinct().Select(x => $"{x.Name}: {x.FactionRelations[ForFaction.Name]}")));

                while (spawnValue > 0 && enemyOptions.Any())
                {
                    enemyOptions = enemyOptions
                        .Where(x => x.Item2.SupportCost <= spawnValue)
                        .ToArray();

                    if(enemyOptions.Any())
                    {
                        var spawnSpec = enemyOptions[random.Next(enemyOptions.Length)];
                        spawnValue -= spawnSpec.Item2.SupportCost;
                        var waypointIndex = random.Next(MissionPath.Waypoints.Count);

                        _spawnsByWaypoint[waypointIndex].Add(spawnSpec);
                    }
                }
            }

            if (isTrackedMission && MissionPath != null && MissionPath.Waypoints.Any())
            {
                if (playerSub.RealPosition.DistanceTo(MissionPath.EstimatedPosition) < 500)
                {
                    MissionPath.Waypoints.RemoveAt(0);
                    _spawnsByWaypoint.RemoveAt(0);

                    if (_spawnsByWaypoint.Any())
                    {
                        var random = new Random();

                        foreach (var spawnSpec in _spawnsByWaypoint[0])
                        {
                            var spawnPoint = MissionPath.Waypoints[0] +
                                new Vector3(
                                    random.Next(-50, 50),
                                    random.Next(-50, 50),
                                    random.Next(-50, 50));

                            var submarine = spawnSpec.Item2.SubLoadout.HullType.SubPrefab.Instance() as Submarine;
                            submarine.LookAtFromPosition(spawnPoint, playerSub.GlobalTransform.origin, playerSub.UpVector);
                            submarine.Faction = spawnSpec.Item1;
                            submarine.IsMissionEnemy = true;
                            submarine.FriendlyName = $"{spawnSpec.Item1.Name} {spawnSpec.Item2.AgentNameRole}";
                            playerSub.GetParent().AddChild(submarine);
                            submarine.AddChild(spawnSpec.Item2.MissionEnemyControllerPrefab.Instance());
                            submarine.BuildModules(spawnSpec.Item2.SubLoadout);
                        }
                    }
                }
            }

            if (MissionPath != null && !MissionPath.Waypoints.Any())
            {
                _patrolComplete = true;

                if (!_sentCompleteMessage)
                {
                    _sentCompleteMessage = true;

                    notificationSystem.AddNotification(
                        new Notification
                        {
                            IsMessage = true,
                            IsGood = true,
                            IsImportant = true,
                            Body = $"Patrol complete, good job. Return to {FromStation.Name} for your reward."
                        });
                }
            }
        }

        public override void SerializeTo(SerializationObject missionRoot)
        {
            missionRoot.SetAttribute("type", "SimulationPatrolMission");
            missionRoot.SetAttribute("id", Id);
            missionRoot.SetAttribute("for_faction", ForFaction.Name);
            missionRoot.SetAttribute("reward", Reward.ToString());
            missionRoot.SetAttribute("deposit", Deposit.ToString());
            missionRoot.SetAttribute("allowed_time", AllowedTime?.ToString());
            missionRoot.SetAttribute("due_date", DueDate?.ToString());
            missionRoot.SetAttribute("from_station", FromStation.Name);
            missionRoot.SetAttribute("patrol_path", PatrolPath.ToString());
            missionRoot.SetAttribute("patrol_complete", _patrolComplete.ToString());
            missionRoot.SetAttribute("patrol_sent_message", _sentCompleteMessage.ToString());

            if (MissionPath != null)
            {
                var missionPathRoot = missionRoot.AddChild("mission_path");
                for (var i = 0; i < MissionPath.Waypoints.Count; i++)
                {
                    var waypointRoot = missionPathRoot.AddChild("point");
                    waypointRoot.SetAttribute("x", MissionPath.Waypoints[i].x.ToString());
                    waypointRoot.SetAttribute("y", MissionPath.Waypoints[i].y.ToString());
                    waypointRoot.SetAttribute("z", MissionPath.Waypoints[i].z.ToString());

                    var spawnsRoot = waypointRoot.AddChild("spawns");
                    foreach (var spawn in _spawnsByWaypoint[i])
                    {
                        var spawnRoot = spawnsRoot.AddChild("spawn");
                        spawnRoot.SetAttribute("faction", spawn.Item1.Name);
                        spawnRoot.SetAttribute("agent_definition", spawn.Item2.Name);
                    }
                }
            }
        }

        public static IMission DeserializeFrom(
            IWorldManager worldManager,
            IResourceLocator resourceLocator,
            SerializationObject missionRoot)
        {
            var mission = new SimulationPatrolMission(
                missionRoot.LookupReference("for_faction", worldManager.Factions, f => f.Name),
                Convert.ToDouble(missionRoot.GetAttribute("reward")),
                Convert.ToDouble(missionRoot.GetAttribute("deposit")),
                TimeSpan.Parse(missionRoot.GetAttribute("allowed_time")),
                missionRoot.LookupReference("from_station", worldManager.SimulationStations, s => s.Name),
                Convert.ToInt32(missionRoot.GetAttribute("patrol_path")))
            {
                Id = missionRoot.GetAttribute("id"),
                _patrolComplete = Convert.ToBoolean(missionRoot.GetAttribute("patrol_complete")),
                _sentCompleteMessage = Convert.ToBoolean(missionRoot.GetAttribute("patrol_sent_message"))
            };

            var currentWaypoint = Convert.ToInt32(missionRoot.GetAttribute("current_waypoint"));

            if (missionRoot.HasAttribute("due_date"))
            {
                mission.DueDate = Convert.ToDateTime(missionRoot.GetAttribute("due_date"));
            }

            if (missionRoot.HasChild("mission_path"))
            {
                var missionPathRoot = missionRoot.GetChild("mission_path");

                var missionPathPoints = new List<Vector3>();

                mission._spawnsByWaypoint = new List<List<Tuple<Faction, AgentDefinition>>>();
                for (var i = 0; i < missionPathRoot.Children.Count; i++)
                {
                    var waypointRoot = missionPathRoot.Children[i];

                    missionPathPoints.Add(new Vector3(
                        Convert.ToSingle(waypointRoot.GetAttribute("x")),
                        Convert.ToSingle(waypointRoot.GetAttribute("y")),
                        Convert.ToSingle(waypointRoot.GetAttribute("z"))));

                    var spawnsRoot = waypointRoot.GetChild("spawns");

                    var spawns = new List<Tuple<Faction, AgentDefinition>>();
                    foreach (var spawnRoot in spawnsRoot.Children)
                    {
                        spawns.Add(Tuple.Create(
                            spawnRoot.LookupReference("faction", worldManager.Factions, f => f.Name),
                            resourceLocator.GetAgentDefinition(spawnRoot.GetAttribute("agent_definition"))));
                    }
                    mission._spawnsByWaypoint.Add(spawns);
                }
                mission.MissionPath = new MissionPath(missionPathPoints);
            }

            return mission;
        }
    }
}
