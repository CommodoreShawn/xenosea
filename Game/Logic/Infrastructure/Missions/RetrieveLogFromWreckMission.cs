﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Missions
{
    public class RetrieveLogFromWreckMission : AbstractWreckMission
    {
        public bool LogRetrieved { get; set; }

        public override string Description => $"Find the wreck of the {Wreck.Data["name"]} and return its log book.";

        public override string Detail0 => $"Complete By: {DueDate.Value.ToShortTimeString()} {DueDate.Value.ToShortDateString()}";
        public override string Detail1 => LogRetrieved ? $"Return to {DeliverStation.Name}" : $"Find the wreck near the waypoint.";
        public override string Detail2 => $"Reward: {Reward.ToString("C0")}";

        private readonly Random _random;
        private List<Tuple<Faction, AgentDefinition>> _overWreckSpawns;
        private List<Tuple<float, Faction, AgentDefinition, Vector3>> _reinforcementSpawns;
        private float _overWreckTimer;
        private bool _spawnsCreated;
        private bool _visitedWreck;

        public RetrieveLogFromWreckMission(
            string name,
            Faction forFaction,
            double reward,
            double deposit,
            TimeSpan allowedTime,
            Vector3 waypointPosition,
            SeafloorObject wreck,
            DateTime expireTime,
            SimulationStation destinationStation)
            : base(name, forFaction, reward, deposit, allowedTime, waypointPosition, wreck, expireTime, destinationStation)
        {
            LogRetrieved = false;

            _overWreckSpawns = new List<Tuple<Faction, AgentDefinition>>();
            _reinforcementSpawns = new List<Tuple<float, Faction, AgentDefinition, Vector3>>();
            _spawnsCreated = false;
            _visitedWreck = false;
            _random = new Random();
        }

        public override void SerializeTo(SerializationObject missionRoot)
        {
            missionRoot.SetAttribute("type", "RetrieveLogFromWreckMission");
            missionRoot.SetAttribute("id", Id);
            missionRoot.SetAttribute("for_faction", ForFaction.Name);
            missionRoot.SetAttribute("reward", Reward.ToString());
            missionRoot.SetAttribute("deposit", Deposit.ToString());
            missionRoot.SetAttribute("allowed_time", AllowedTime?.ToString());
            missionRoot.SetAttribute("due_date", DueDate?.ToString());
            missionRoot.SetAttribute("reward", Reward.ToString());
            missionRoot.SetAttribute("name", Name);
            missionRoot.SetAttribute("log_retrieved", LogRetrieved.ToString());
            missionRoot.SetAttribute("pos_x", WaypointPosition.x.ToString());
            missionRoot.SetAttribute("pos_y", WaypointPosition.y.ToString());
            missionRoot.SetAttribute("pos_z", WaypointPosition.z.ToString());
            missionRoot.SetAttribute("wreck_id", Wreck.Id);
            missionRoot.SetAttribute("expire_time", ExpireTime.ToString());
            missionRoot.SetAttribute("deliver_station", DeliverStation.Name);
        }

        public static IMission DeserializeFrom(
            IWorldManager worldManager,
            IResourceLocator resourceLocator,
            SerializationObject missionRoot)
        {
            var mission = new RetrieveLogFromWreckMission(
                missionRoot.GetAttribute("name"),
                missionRoot.LookupReference("for_faction", worldManager.Factions, f => f.Name),
                Convert.ToDouble(missionRoot.GetAttribute("reward")),
                Convert.ToDouble(missionRoot.GetAttribute("deposit")),
                TimeSpan.Parse(missionRoot.GetAttribute("allowed_time")),
                new Vector3(
                    Convert.ToSingle(missionRoot.GetAttribute("pos_x")),
                    Convert.ToSingle(missionRoot.GetAttribute("pos_y")),
                    Convert.ToSingle(missionRoot.GetAttribute("pos_z"))),
                missionRoot.LookupReferenceOrNull("wreck_id", worldManager.SeafloorObjects, a => a.Id),
                Convert.ToDateTime(missionRoot.GetAttribute("expire_time")),
                missionRoot.LookupReference("deliver_station", worldManager.SimulationStations, s => s.Name))
            {
                Id = missionRoot.GetAttribute("id"),
                LogRetrieved = Convert.ToBoolean(missionRoot.GetAttribute("log_retrieved"))
            };

            if (missionRoot.HasAttribute("due_date"))
            {
                mission.DueDate = Convert.ToDateTime(missionRoot.GetAttribute("due_date"));
            }

            return mission;
        }

        public override bool CanBeDelivered(Submarine agent)
        {
            return agent.IsDockedTo is WorldStation ws
                && ws.BackingStation == DeliverStation
                && LogRetrieved;
        }

        public override void ProcessPlayerLogic(
            INotificationSystem notificationSystem, 
            Submarine playerSub, 
            bool isTrackedMission, 
            float delta)
        {
            if (isTrackedMission)
            {
                if (MissionPath != null 
                    && LogRetrieved 
                    && MissionPath.EstimatedPosition == WaypointPosition)
                {
                    MissionPath = null;
                }

                if(MissionPath == null)
                {
                    if(LogRetrieved)
                    {
                        MissionPath = new MissionPath(Util.GetNavigationCenter(DeliverStation.Location).Yield());
                    }
                    else
                    {
                        MissionPath = new MissionPath(WaypointPosition.Yield());
                    }
                }
                
            }
            else if (!isTrackedMission)
            {
                MissionPath = null;
            }


            if (!_spawnsCreated)
            {
                _spawnsCreated = true;

                var locationThreat = Wreck.Datum.FactionInfluence
                    .Where(kvp => kvp.Key.IsHostileTo(ForFaction))
                    .Sum(kvp => kvp.Value)
                    * 2;

                var spawnValue = (float)_random.Next(
                    (int)(locationThreat / 2),
                    (int)(locationThreat));

                spawnValue = Math.Max(spawnValue, 1);

                GD.Print($"RetrieveLogFromWreckMission: Threat: {locationThreat} Spawns: {spawnValue}");

                var enemyOptions = Wreck.Datum.InfluenceSources
                    .Where(s => s.Faction.IsHostileTo(ForFaction))
                    .SelectMany(s => s.SupportedAgentDefinitions.Where(x => x.CanBePatrolEnemy).Select(x => Tuple.Create(s, x)))
                    .ToArray();

                var reinforcementSpawnDatums = Wreck.Datum.Neighbors
                    .Where(d => d.IceDepth < d.Depth)
                    .SelectMany(d => d.Neighbors)
                    .Where(d => d.IceDepth < d.Depth)
                    .Distinct()
                    .ToArray();

                while (spawnValue > 0 && enemyOptions.Any())
                {
                    enemyOptions = enemyOptions
                        .Where(x => x.Item2.SupportCost <= spawnValue)
                        .ToArray();

                    if (enemyOptions.Any())
                    {
                        var spawnSpec = enemyOptions[_random.Next(enemyOptions.Length)];
                        spawnValue -= spawnSpec.Item2.SupportCost;
                        
                        if(_random.NextDouble() < 0.5 || !reinforcementSpawnDatums.Any())
                        {
                            _overWreckSpawns.Add(Tuple.Create(
                                spawnSpec.Item1.Faction.SecuritySubfaction ?? spawnSpec.Item1.Faction, 
                                spawnSpec.Item2));
                        }
                        else
                        {
                            var spawnDatum = reinforcementSpawnDatums
                                .OrderBy(x => x.Position.DistanceSquaredTo(spawnSpec.Item1.Location.Position))
                                .FirstOrDefault();

                            if(spawnDatum != null)
                            {
                                _reinforcementSpawns.Add(Tuple.Create(
                                    (float)_random.Next(0, 100),
                                    spawnSpec.Item1.Faction.SecuritySubfaction ?? spawnSpec.Item1.Faction,
                                    spawnSpec.Item2,
                                    Util.GetNavigationCenter(spawnDatum)));
                            }
                        }
                    }
                }
            }

            if(!_visitedWreck && playerSub.RealPosition.DistanceTo(Wreck.Position) < 1000)
            {
                _visitedWreck = true;
            }

            if(_overWreckSpawns.Any() && playerSub.RealPosition.DistanceTo(Wreck.Position) < 8000)
            {
                foreach (var tuple in _overWreckSpawns)
                {
                    GD.Print($"RetrieveLogFromWreckMission over wreck spawn");
                    DoSpawn(
                        playerSub,
                        Wreck.Position + Wreck.Position.Normalized() * 100,
                        tuple.Item2,
                        tuple.Item1);
                }
                _overWreckSpawns.Clear();
            }

            if(_visitedWreck)
            {
                _overWreckTimer += delta;

                foreach(var spawn in _reinforcementSpawns.Where(x=>x.Item1 <= _overWreckTimer))
                {
                    GD.Print($"RetrieveLogFromWreckMission reinfocement spawn");
                    DoSpawn(
                        playerSub,
                        spawn.Item4,
                        spawn.Item3,
                        spawn.Item2);
                }

                _reinforcementSpawns.RemoveAll(x => x.Item1 <= _overWreckTimer);
            }
        }

        private void DoSpawn(
            Submarine playerSub,
            Vector3 spawnPos,
            AgentDefinition agentDefinition,
            Faction spawnFaction)
        {
            spawnPos = spawnPos + 
                new Vector3(
                    _random.Next(-50, 50),
                    _random.Next(-50, 50),
                    _random.Next(-50, 50));

            var submarine = agentDefinition.SubLoadout.HullType.SubPrefab.Instance() as Submarine;
            submarine.LookAtFromPosition(spawnPos, playerSub.GlobalTransform.origin, playerSub.UpVector);
            submarine.Faction = spawnFaction;
            submarine.IsMissionEnemy = true;
            submarine.FriendlyName = $"{spawnFaction.Name} {agentDefinition.AgentNameRole}";
            playerSub.GetParent().AddChild(submarine);
            submarine.AddChild(agentDefinition.MissionEnemyControllerPrefab.Instance());
            submarine.BuildModules(agentDefinition.SubLoadout);
        }
    }
}
