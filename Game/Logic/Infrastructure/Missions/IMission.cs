﻿using System;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Missions
{
    public interface IMission
    {
        string Id { get; }
        TimeSpan? AllowedTime { get; }
        DateTime? DueDate { get; set; }
        string Name { get; }
        double Deposit { get; }
        double Reward { get; }
        string Description { get; }
        string Detail0 { get; }
        string Detail1 { get; }
        string Detail2 { get; }
        bool IsExpired { get; }
        MissionPath MissionPath { get; set; }
        Faction ForFaction { get; }

        bool CanBeSerialized { get; }

        bool CanBeDelivered(Submarine playerSub);

        void ProcessPlayerLogic(
            INotificationSystem notificationSystem,
            Submarine playerSub,
            bool isTrackedMission,
            float delta);

        void SerializeTo(SerializationObject serializationObject);
    }
}