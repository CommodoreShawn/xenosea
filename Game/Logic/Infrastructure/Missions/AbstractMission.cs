﻿using System;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Missions
{
    public abstract class AbstractMission: IMission
    {
        public string Id { get; protected set; }
        public Faction ForFaction { get; }
        public double Reward { get; set; }
        public double Deposit { get; set; }
        public TimeSpan? AllowedTime { get; set; }
        public DateTime? DueDate { get; set; }
        public bool IsExpired { get; set; }
        public abstract string Name { get; }
        public abstract string Description { get; }
        public abstract string Detail0 { get; }
        public abstract string Detail1 { get; }
        public abstract string Detail2 { get; }

        public MissionPath MissionPath { get; set; }
        public abstract bool CanBeSerialized { get; }

        protected AbstractMission(
            Faction forFaction,
            double reward,
            double deposit,
            TimeSpan allowedTime)
        {
            Id = Guid.NewGuid().ToString();
            ForFaction = forFaction;
            Reward = reward;
            Deposit = deposit;
            AllowedTime = allowedTime;
            MissionPath = null;
        }

        public abstract bool CanBeDelivered(Submarine agent);

        public abstract void ProcessPlayerLogic(
            INotificationSystem notificationSystem,
            Submarine playerSub, 
            bool isTrackedMission,
            float delta);

        public abstract void SerializeTo(SerializationObject serializationObject);
    }
}
