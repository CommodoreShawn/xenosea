﻿namespace Xenosea.Logic.Infrastructure.Missions
{
    public interface ITradeMission : IMission
    {
        CommodityType Commodity { get; }
        float Quantity { get; set; }
        bool IsSmuggling { get; }
        Faction DestinationFaction { get; }
        string DestinationName { get; }
    }
}