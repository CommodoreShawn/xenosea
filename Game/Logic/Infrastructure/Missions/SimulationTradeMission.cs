﻿using System;
using System.Linq;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure.Missions
{
    public class SimulationTradeMission : AbstractMission, ITradeMission
    {
        public SimulationStation Destination { get; }
        public SimulationStation Source { get; }
        public CommodityType Commodity { get; }
        public float Quantity { get; set; }
        public bool IsSmuggling { get; }
        public override string Name => $"Deliver {Commodity.Name} to {Destination.Name}";

        public override string Description => $"Deliver the specified goods to the specified station by the specified time to recieve your reward, and to refund the deposit.";

        public override string Detail0 => $"Deliver By: {DueDate?.ToShortTimeString()} {DueDate?.ToShortDateString()}";
        public override string Detail1 => $"{Quantity.ToString("N0")} {Commodity.Name}";
        public override string Detail2 => $"Reward: {Reward.ToString("C0")} plus {Deposit.ToString("C0")} Deposit";

        
        public Faction DestinationFaction => Destination.Faction;
        public string DestinationName => Destination.Name;
        
        public override bool CanBeSerialized => true;

        public SimulationTradeMission(
            Faction forFaction, 
            double reward, 
            double deposit,
            TimeSpan allowedTime,
            SimulationStation source,
            SimulationStation destination,
            CommodityType commodity,
            float quantity,
            bool isSmuggling) 
            : base(forFaction, reward, deposit, allowedTime)
        {
            Source = source;
            Destination = destination;
            Commodity = commodity;
            Quantity = quantity;
            IsSmuggling = isSmuggling;
        }

        public override bool CanBeDelivered(Submarine agent)
        {
            return agent.IsDockedTo is WorldStation
                && (agent.IsDockedTo  as WorldStation).BackingStation == Destination
                && agent.CommodityStorage.GetQty(Commodity) >= Quantity;
        }

        public override void ProcessPlayerLogic(
            INotificationSystem notificationSystem,
            Submarine playerSub, 
            bool isTrackedMission,
            float delta)
        {
            if (MissionPath == null && isTrackedMission)
            {
                MissionPath = new MissionPath(
                    Util.GetNavigationCenter(Destination.Location).Yield());
            }
            else if (!isTrackedMission)
            {
                MissionPath = null;
            }

            if(isTrackedMission && MissionPath != null && MissionPath.Waypoints.Any())
            {
                if(playerSub.RealPosition.DistanceTo(MissionPath.EstimatedPosition) < 100)
                {
                    MissionPath.Waypoints.RemoveAt(0);
                }
            }
        }

        public override void SerializeTo(SerializationObject missionRoot)
        {
            missionRoot.SetAttribute("type", "SimulationTradeMission");
            missionRoot.SetAttribute("id", Id);
            missionRoot.SetAttribute("for_faction", ForFaction.Name);
            missionRoot.SetAttribute("reward", Reward.ToString());
            missionRoot.SetAttribute("deposit", Deposit.ToString());
            missionRoot.SetAttribute("allowed_time", AllowedTime?.ToString());
            missionRoot.SetAttribute("due_date", DueDate?.ToString());
            missionRoot.SetAttribute("source", Source.Name);
            missionRoot.SetAttribute("destination", Destination.Name);
            missionRoot.SetAttribute("commodity", Commodity.Name);
            missionRoot.SetAttribute("quantity", Quantity.ToString());
            missionRoot.SetAttribute("is_smuggling", IsSmuggling.ToString());
        }

        public static IMission DeserializeFrom(
            IWorldManager worldManager, 
            IResourceLocator resourceLocator, 
            SerializationObject missionRoot)
        {
            var mission = new SimulationTradeMission(
                missionRoot.LookupReference("for_faction", worldManager.Factions, f => f.Name),
                Convert.ToDouble(missionRoot.GetAttribute("reward")),
                Convert.ToDouble(missionRoot.GetAttribute("deposit")),
                TimeSpan.Parse(missionRoot.GetAttribute("allowed_time")),
                missionRoot.LookupReference("source", worldManager.SimulationStations, s => s.Name),
                missionRoot.LookupReference("destination", worldManager.SimulationStations, s => s.Name),
                resourceLocator.GetCommodityType(missionRoot.GetAttribute("commodity")),
                Convert.ToSingle(missionRoot.GetAttribute("quantity")),
                Convert.ToBoolean(missionRoot.GetAttribute("is_smuggling")))
            {
                Id = missionRoot.GetAttribute("id")
            };

            if(missionRoot.HasAttribute("due_date"))
            {
                mission.DueDate = Convert.ToDateTime(missionRoot.GetAttribute("due_date"));
            }

            return mission;
        }
    }
}
