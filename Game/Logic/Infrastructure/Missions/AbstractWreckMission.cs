﻿using Godot;
using System;
using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic.Infrastructure.Missions
{
    public abstract class AbstractWreckMission : AbstractMission
    {
        public override string Name { get; } 
        public Vector3 WaypointPosition { get; }
        public AgentDefinition WreckedAgent { get; }
        public SeafloorObject Wreck { get; }
        public DateTime ExpireTime { get; }
        public SimulationStation DeliverStation { get; }

        public override bool CanBeSerialized => true;

        public AbstractWreckMission(
            string name,
            Faction forFaction,
            double reward,
            double deposit,
            TimeSpan allowedTime,
            Vector3 waypointPosition,
            SeafloorObject wreck,
            DateTime expireTime,
            SimulationStation deliverStation)
            : base(forFaction, reward, deposit, allowedTime)
        {
            Name = name;
            Wreck = wreck;
            WaypointPosition = waypointPosition;
            ExpireTime = expireTime;
            DeliverStation = deliverStation;
        }
    }
}
