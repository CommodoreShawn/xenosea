﻿using Autofac;
using Xenosea.Logic.Infrastructure.Services;

namespace Xenosea.Logic.Infrastructure
{
    /// <summary>
    /// Core dependency injection module.
    /// </summary>
    public class CoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Services
            builder.RegisterType<WorldManager>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SonarContactTracker>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<NotificationSystem>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<PlayerData>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<PerfTracker>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<WorldSimulator>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<MissionTracker>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<BountyTracker>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<QuestTracker>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SimulationEventBus>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ConversationRewardListener>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<StandingTracker>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<CrimeTracker>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<WorldGenerator>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SimulationTradeMissionGenerator>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SimulationAgentSimulator>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SimulationPatrolMissionGenerator>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<GameSerializer>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ConsoleLogger>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ResourceLocator>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SimToEngineLink>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SimulationBattleHandler>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<AutomaticKnownPoiTracker>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ConversationManager>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<WeatherSimulator>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SettingsService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<EconomicLogger>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<StationGenerator>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<LoadSequencer>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SoundEffectPlayer>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<WreckMissionsGenerator>().AsImplementedInterfaces().SingleInstance();
        }
    }
}
