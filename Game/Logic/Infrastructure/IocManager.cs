﻿using Autofac;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.Infrastructure
{
    public static class IocManager
    {
        public static IContainer IocContainer { get; set; }

        public static ISettingsService SettingsService { get; private set; }
        public static IPerftracker Perftracker { get; private set; }

        public static void Initialize()
        {
            var builder = new ContainerBuilder();

            var iocModules = new Module[]
            {
                new CoreModule()
            };

            foreach (var module in iocModules)
            {
                builder.RegisterModule(module);
            }

            IocContainer = builder.Build();

            SettingsService = IocContainer.Resolve<ISettingsService>();
            Perftracker = IocContainer.Resolve<IPerftracker>();

            //foreach (var initService in IocContainer.Resolve<IEnumerable<IInitializableService>>())
            //{
            //    initService.Initialize();
            //}
        }
    }
}
