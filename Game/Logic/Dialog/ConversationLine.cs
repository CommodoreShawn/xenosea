﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xenosea.Logic.Dialog
{
    public class ConversationLine
    {
        public string Speaker { get; set; }
        public string Text { get; set; }
    }
}
