﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.addons.XenoTalk.Data;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.Dialog
{
    public class NpcConversation
    {
        private readonly IPlayerData _playerData;
        private readonly IResourceLocator _resourceLocator;
        private readonly IWorldManager _worldManager;
        private readonly IQuestTracker _questTracker;
        private readonly ISimulationEventBus _simulationEventBus;
        private readonly ILogger _logger;
        private readonly Dictionary<string, string> _conversationState;
        private readonly Npc _npc;
        private readonly List<ConvoTopic> _convoTopics;
        private readonly Random _random;

        private ConvoResponse _lastResponse;

        public NpcConversation(
            IPlayerData playerData,
            IResourceLocator resourceLocator,
            IWorldManager worldManager,
            IQuestTracker questTracker,
            ISimulationEventBus simulationEventBus,
            ILogger logger,
            Dictionary<string, string> conversationState,
            Npc npc,
            List<ConvoTopic> convoTopics)
        {
            _playerData = playerData;
            _resourceLocator = resourceLocator;
            _conversationState = conversationState;
            _worldManager = worldManager;
            _simulationEventBus = simulationEventBus;
            _npc = npc;
            _convoTopics = convoTopics;
            _questTracker = questTracker;
            _logger = logger;

            _random = new Random();
        }

        public ConversationResponse Start()
        {
            return ChooseTopic("Hello");
        }

        public IEnumerable<string> GetAvailableTopics()
        {
            return _convoTopics
                .Where(x => x.Responses.Any(r => r.Conditions.All(c => IsMet(c))))
                .Select(x => x.Name)
                .Where(x => _playerData.KnownTopics.Contains(x))
                .Where(x => x != "Hello")
                .ToArray();
        }

        private bool IsMet(ConvoResponseCondition condition)
        {
            SimulationStation station = null;

            if(_conversationState.ContainsKey("station"))
            {
                station = _worldManager.SimulationStations
                    .Where(x => x.Name == _conversationState["station"])
                    .First();
            }

            switch (condition.Type)
            {
                case "location":
                    ConvoDebug($"location {condition.Data["value"]} {(_playerData.PlayerSub.IsDockedTo as WorldStation)?.BackingStation?.Name == condition.Data["value"]}");
                    return (_playerData.PlayerSub.IsDockedTo as WorldStation)?.BackingStation?.Name == condition.Data["value"];
                case "has_trait":
                    ConvoDebug($"has_trait {condition.Data["value"]} {condition.Data["value"].Split(',').Select(x => x.Trim()).Intersect(_npc.Traits).Any()}");
                    return condition.Data["value"].Split(',').Select(x => x.Trim()).Intersect(_npc.Traits).Any();
                case "not_has_trait":
                    ConvoDebug($"not_has_trait {condition.Data["value"]} {!condition.Data["value"].Split(',').Select(x => x.Trim()).Intersect(_npc.Traits).Any()}");
                    return !condition.Data["value"].Split(',').Select(x => x.Trim()).Intersect(_npc.Traits).Any();
                case "has_name":
                    ConvoDebug($"has_name {condition.Data["value"]} {_npc.Name == condition.Data["value"]}");
                    return _npc.Name == condition.Data["value"];
                case "variable_not_equal":
                    ConvoDebug($"variable_not_equal {condition.Data["variable"]} {condition.Data["value"]} {!_conversationState.ContainsKey(condition.Data["variable"]) || _conversationState[condition.Data["variable"]] != condition.Data["value"]}");
                    return !_conversationState.ContainsKey(condition.Data["variable"])
                        || _conversationState[condition.Data["variable"]] != condition.Data["value"];
                case "variable_equal":
                    ConvoDebug($"variable_equal {condition.Data["variable"]} {condition.Data["value"]} {_conversationState.ContainsKey(condition.Data["variable"]) && _conversationState[condition.Data["variable"]] == condition.Data["value"]}");
                    return _conversationState.ContainsKey(condition.Data["variable"])
                        && _conversationState[condition.Data["variable"]] == condition.Data["value"];
                case "quest_state":
                    ConvoDebug($"quest_state {condition.Data["quest"]} {condition.Data["state"]} {_questTracker.GetQuestState(condition.Data["quest"]) == condition.Data["state"]}");
                    return condition.Data["state"].Split(',').Select(x => x.Trim()).Contains(_questTracker.GetQuestState(condition.Data["quest"]));
                case "proximity":
                    ConvoDebug($"proximity {condition.Data["lat"]} {condition.Data["lon"]}: {condition.Data["dist"]}");
                    var targetLocation = Util.LatLonToVector(
                        Convert.ToSingle(condition.Data["lat"]),
                        Convert.ToSingle(condition.Data["lon"]),
                        Constants.WORLD_RADIUS);
                    return station.Location.Position.DistanceTo(targetLocation) <= Convert.ToSingle(condition.Data["dist"]);
                case "faction_proximity":
                    ConvoDebug($"faction_proximity {condition.Data["faction"]} {condition.Data["dist"]}");
                    var distance = Convert.ToSingle(condition.Data["dist"]);

                    return _worldManager.SimulationStations
                        .Where(x => x.Faction.Name == condition.Data["faction"])
                        .Any(x => x.Location.Position.DistanceTo(station.Location.Position) <= distance);
                case "variable_exists":
                    ConvoDebug($"variable_exists {condition.Data["value"]}");
                    return _conversationState.ContainsKey(condition.Data["value"]);
                default:
                    Godot.GD.PrintErr($"Unknown conversation condition: '{condition.Type}'");
                    return false;
            }
        }

        private void ConvoDebug(string text)
        {
            //Godot.GD.Print(text);
        }

        private bool IsHighPriority(ConvoResponseCondition condition)
        {
            switch (condition.Type)
            {
                case "has_name":
                    return true;
                case "quest_state":
                    return true;
                default:
                    return false;
            }
        }

        public ConversationResponse ChooseOption(string option)
        {
            if(_lastResponse != null
                && _lastResponse.Options.Any(x => x.OptionText == option))
            {
                var response = _lastResponse.Options
                    .Where(x => x.OptionText == option)
                    .FirstOrDefault();

                _lastResponse = response;

                return BuildResponse(option, response);
            }

            _lastResponse = null;
            return BuildIDKResponse(option);
        }

        public ConversationResponse ChooseTopic(string topic)
        {
            var responses = _convoTopics
                .Where(x => x.Name == topic)
                .SelectMany(x => x.Responses)
                .Where(x => x.Conditions.All(c => IsMet(c)))
                .ToArray();

            foreach(var response in _convoTopics.Where(x => x.Name == topic).SelectMany(x => x.Responses))
            {
                ConvoDebug("========== For: " + response.Text);

                foreach(var condition in response.Conditions)
                {
                    ConvoDebug("" + IsMet(condition));
                }
            }

            if (responses.Any())
            {
                var highPriorityResponses = responses
                    .Where(r => r.Conditions.All(c => IsMet(c)))
                    .Where(r => r.Conditions.Any(c => IsHighPriority(c)))
                    .ToArray();

                if(highPriorityResponses.Any())
                {
                    responses = highPriorityResponses;
                }

                var pickedResponse = responses[_random.Next(responses.Length)];

                _lastResponse = pickedResponse;

                return BuildResponse(topic, pickedResponse);
            }
            else
            {
                _lastResponse = null;

                return BuildIDKResponse(topic);
            }
        }

        private ConversationResponse BuildIDKResponse(string inputLine)
        {
            return new ConversationResponse
            {
                Lines = new ConversationLine[]
                {
                    new ConversationLine
                    {
                        Speaker = "Player",
                        Text = inputLine,
                    },
                    new ConversationLine
                    {
                        Speaker = _npc.Name,
                        Text = "I don't know"
                    }
                }
            };
        }

        private ConversationResponse BuildResponse(string inputLine, ConvoResponse pickedResponse)
        {
            return new ConversationResponse
            {
                Lines = new ConversationLine[]
                    {
                        new ConversationLine
                        {
                            Speaker = "Player",
                            Text = inputLine,
                        },
                        new ConversationLine
                        {
                            Speaker = _npc.Name,
                            Text = DoVariableReplacement(pickedResponse.Text)
                        }
                    },
                Options = pickedResponse.Options
                        .Select(x => x.OptionText)
                        .ToArray(),
                Actions = pickedResponse.Actions
                        .Select(x => (Func<string>)(() => ExecuteAction(x)))
                        .ToArray()
            };
        }

        private string ExecuteAction(ConvoResponseAction action)
        {
            try
            {
                switch (action.Type)
                {
                    case "discover_topic":
                        if (!_playerData.KnownTopics.Contains(action.Data["value"]))
                        {
                            _playerData.KnownTopics.Add(action.Data["value"]);
                            return $"Discovered topic {action.Data["value"]}";
                        }
                        return null;
                    case "discover_poi":
                        if (!_playerData.KnownPointsOfInterest.Any(x => x.FriendlyName == action.Data["value"]))
                        {
                            var poi = _resourceLocator.GetFixedPoi(action.Data["value"]);

                            var poiPos = Util.LatLonToVector(poi.Latitude, poi.Longitude, Constants.WORLD_RADIUS);

                            var location = _worldManager.WorldDatums
                                .OrderBy(d => d.Position.DistanceTo(poiPos))
                                .Take(1)
                                .Where(d => d.SimulationStation == null)
                                .FirstOrDefault();

                            if (location != null)
                            {
                                _playerData.KnownPointsOfInterest
                                    .Add(new KnownPointOfInterest
                                    {
                                        ContactType = poi.ContactType,
                                        Location = location,
                                        FriendlyName = poi.Name,
                                        ReferenceId = poi.Name
                                    });

                                return $"Discovered location {poi.Name}";
                            }
                        }
                        return null;
                    case "set_variable":
                        var variable = action.Data["variable"];
                        var value = action.Data["value"];
                        if (!_conversationState.ContainsKey(variable))
                        {
                            _conversationState.Add(variable, value);
                        }
                        else
                        {
                            _conversationState[variable] = value;
                        }
                        return null;
                    case "set_quest_state":
                        Godot.GD.Print($"Set quest state {action.Data["quest"]} {action.Data["state"]}");
                        _simulationEventBus.SendEvent(new SetQuestStateSimulationEvent(action.Data["quest"], action.Data["state"]));
                        return null;
                    default:
                        Godot.GD.PrintErr($"Unknown conversation action: '{action.Type}'");
                        return null;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error executing conversation action.");
            }

            return null;
        }


        private string DoVariableReplacement(string text)
        {
            text = text
                .Replace("{{location}}", (_playerData.PlayerSub.IsDockedTo as WorldStation)?.BackingStation?.Name)
                .Replace("{{npc_name}}", _npc.Name)
                .Replace("{{playersub_class}}", _playerData.PlayerSub.FriendlyName)
                ;

            if(text.Contains("{{var:"))
            {
                var startIndex = text.IndexOf("{{var:");
                var before = text.Substring(0, startIndex);
                var rem = text.Substring(startIndex + 6);
                var endIndex = rem.IndexOf("}}");

                if(endIndex > 0)
                {
                    var variable = rem.Substring(0, endIndex);
                    var tail = rem.Substring(endIndex + 2);

                    tail = DoVariableReplacement(tail);

                    if (_conversationState.ContainsKey(variable))
                    {
                        return $"{before} {_conversationState[variable]} {tail}";
                    }
                }
            }

            return text;
        }
    }
}
