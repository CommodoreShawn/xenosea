﻿using System.Collections.Generic;

namespace Xenosea.Logic.Dialog
{
    public class DialogResponse
    {
        public List<DialogLine> Lines { get; set; }
        public DialogOptions Options { get; set; }

        public DialogResponse()
        {
            Lines = new List<DialogLine>();
        }
    }
}
