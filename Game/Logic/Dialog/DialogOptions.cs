﻿using Godot.Collections;
using System.Collections.Generic;

namespace Xenosea.Logic.Dialog
{
    public class DialogOptions
    {
        public string Speaker => _line["speaker"] as string;
        public string Text => _line["name"] as string;

        public List<DialogOption> Options { get; }
        private Dictionary _line;

        public DialogOptions(Dictionary line)
        {
            _line = line;
            Options = new List<DialogOption>();
            var nextIndex = 0;
            foreach(var option in _line["options"] as Array)
            {
                Options.Add(new DialogOption(option as Dictionary, nextIndex));
                nextIndex += 1;
            }
        }
    }
}