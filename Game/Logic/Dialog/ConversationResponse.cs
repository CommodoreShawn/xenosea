﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Xenosea.Logic.Dialog
{
    public class ConversationResponse
    {
        public IEnumerable<ConversationLine> Lines { get; set; }
        public IEnumerable<Func<string>> Actions { get; set; }
        public IEnumerable<string> Options { get; set; }
        
        public ConversationResponse()
        {
            Lines = Enumerable.Empty<ConversationLine>();
            Actions = Enumerable.Empty<Func<string>>();
            Options = Enumerable.Empty<string>();
        }
    }
}
