﻿using Godot.Collections;

namespace Xenosea.Logic.Dialog
{
    public class DialogLine
    {
        public string Speaker => _line["speaker"] as string;
        public string Text => _line["text"] as string;
        private Dictionary _line;

        public DialogLine(Dictionary line)
        {
            _line = line;
        }
    }
}