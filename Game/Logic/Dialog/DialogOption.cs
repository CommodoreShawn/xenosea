﻿using Godot.Collections;

namespace Xenosea.Logic.Dialog
{
    public class DialogOption
    {
        public int Index { get; }
        private Dictionary _option;
        public string Label => _option["label"] as string;

        public DialogOption(Dictionary option, int index)
        {
            _option = option;
            Index = index;
        }
    }
}