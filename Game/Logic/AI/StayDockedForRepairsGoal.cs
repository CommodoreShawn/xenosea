﻿using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class StayDockedForRepairsGoal : IBotGoal
    {
        public int Priority => StayDockedForRepairsGoalType.PRIORITY;

        public StayDockedForRepairsGoal()
        {
        }

        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return controller.Submarine.IsDockedTo != null 
                && controller.Submarine.Health < controller.Submarine.MaximumHealth;
        }

        public void Update(GoalBotController controller, float delta)
        {
            if (controller.DockingSequence != null
                    && Godot.Object.IsInstanceValid(controller.DockingSequence.Station as Godot.Object))
            {
                controller.DockingSequence.Update(controller, delta);
            }
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return "Getting repairs.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return null;
        }
    }
}
