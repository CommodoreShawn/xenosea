﻿using Godot;
using System.Linq;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class SellLootGoal : IBotGoal
    {
        public int Priority => SellLootGoalType.PRIORITY;
        
        private float _unloadTimer;
        
        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return controller.Submarine.IsDockedTo != null
                && controller.Submarine.CommodityStorage.TotalVolume > 0;
        }

        public void Update(GoalBotController controller, float delta)
        {
            _unloadTimer -= delta;

            if(_unloadTimer <= 0)
            {
                var unloadAmt = 0.0;
                while(unloadAmt < 50 && controller.Submarine.CommodityStorage.TotalVolume > 0)
                {
                    var commodityToUnload = controller.Submarine.CommodityStorage.CommoditiesPresent.First();

                    var toTake = Mathf.Min(50, controller.Submarine.CommodityStorage.GetQty(commodityToUnload));
                    controller.Submarine.CommodityStorage.Remove(commodityToUnload, toTake);
                    controller.Submarine.IsDockedTo.CommodityStorage.Add(commodityToUnload, toTake);
                    unloadAmt += toTake;
                }

                _unloadTimer = 10;
            }
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return "Unloading plunder.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return null;
        }
    }
}
