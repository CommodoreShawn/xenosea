﻿using System.Linq;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class GatherMinisubsGoal : IBotGoal
    {
        public int Priority => GatherMinisubsGoalType.PRIORITY;
        
        public bool IsValid(IStation station)
        {
            return station.MinisubHangars
                .Where(h => h.ActiveMinisub != null)
                .Any();
        }

        public bool IsValid(GoalBotController controller)
        {
            return controller.Submarine.MinisubHangars
                .Where(h => h.ActiveMinisub != null)
                .Any();
        }

        public void Update(GoalBotController controller, float delta)
        {
            foreach(var hangar in controller.Submarine.MinisubHangars)
            {
                hangar.ShouldRecall = true;
            }

            controller.Submarine.Dive = 0;
            controller.Submarine.Throttle = 0;
            controller.Submarine.Rudder = 0;
        }

        public void Update(IStation station, float delta)
        {
            foreach (var hangar in station.MinisubHangars)
            {
                hangar.ShouldRecall = true;
            }
        }

        public string MakeBroadcast()
        {
            return "Recalling minisubs.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return null;
        }
    }
}
