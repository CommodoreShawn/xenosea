﻿using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class TravelAndDockGoal : IBotGoal
    {
        public int Priority => TravelAndDockGoalType.PRIORITY;

        private IStation _station;
        private HubPoint _targetHub;
        private IAiPath _path;
        private Queue<IPatrolWaypoint> _pointQueue;
        private bool _failed;

        public TravelAndDockGoal(IStation station)
        {
            _station = station;
        }

        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return _failed || controller.Submarine.IsDockedTo != _station;
        }

        public void Update(GoalBotController controller, float delta)
        {
            if(_path == null)
            {
                _path = controller.Submarine.LocalHub.AiPaths
                    .Where(p => p.DestinationHub.LocalStations.Contains(_station))
                    .Where(p => p.TradeRoute)
                    .FirstOrDefault();

                if(_path != null)
                {
                    _pointQueue = _path.GetWaypoints();
                    _targetHub = _path.DestinationHub;
                }
                else
                {
                    _failed = true;
                }
            }

            if (controller.DockingSequence != null && controller.DockingSequence.Station != _station)
            {
                if (!controller.DockingSequence.IsUndocking)
                {
                    controller.DockingSequence.Undock();
                }

                controller.DockingSequence.Update(controller, delta);

                if(controller.DockingSequence.IsFinished)
                {
                    controller.DockingSequence = null;
                }
            }
            else if (_targetHub == controller.Submarine.LocalHub)
            {
                if (controller.Submarine.RealPosition.DistanceTo(_station.DockWaitingArea.GlobalTransform.origin) > 1000)
                {
                    controller.AutopilotTo(_station.DockWaitingArea.GlobalTransform.origin, delta,1);
                }
                else
                {
                    if (controller.DockingSequence != null)
                    {
                        controller.DockingSequence.Update(controller, delta);
                    }
                    else
                    {
                        controller.DockingSequence = _station.RequestDocking(controller.Submarine);

                        controller.Submarine.Throttle = 0;
                        controller.Submarine.Rudder = 0;
                        controller.Submarine.Dive = 0;

                        if (controller.DockingSequence == null)
                        {
                            _failed = true;
                        }
                    }
                }
            }
            else if(_pointQueue != null && _pointQueue.Count > 0)
            {
               if(_pointQueue.Peek().IsSatisfied(controller))
                {
                    _pointQueue.Dequeue();

                    if (_pointQueue.Count < 1)
                    {
                        controller.Submarine.LocalHub = _path.DestinationHub;
                    }
                }
               else
                {
                    _pointQueue.Peek().UpdateOrders(controller, delta);
                }
            }
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return "Setting course for " + _station.FriendlyName;
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return $"We are travelling to {_station.FriendlyName}";
        }
    }
}
