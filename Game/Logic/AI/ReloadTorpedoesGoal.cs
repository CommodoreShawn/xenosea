﻿using System.Linq;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class ReloadTorpedoesGoal : IBotGoal
    {
        public int Priority => ReloadTorpedoesGoalType.PRIORITY;
        
        private float _loadTimer;
        private TorpedoType _typeToLoad;

        public ReloadTorpedoesGoal(TorpedoType typeToLoad)
        {
            _typeToLoad = typeToLoad;
        }

        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return controller.Submarine.IsDockedTo != null
                && controller.Submarine.TorpedoTubes.Any(t => t.TorpedoesAvailable.Count < t.MagazineCapacity);
        }

        public void Update(GoalBotController controller, float delta)
        {
            _loadTimer -= delta;

            if(_loadTimer <= 0)
            {
                var tubeToFill = controller.Submarine.TorpedoTubes
                    .Where(t => t.TorpedoesAvailable.Count < t.MagazineCapacity)
                    .FirstOrDefault();

                if(tubeToFill != null)
                {
                    tubeToFill.TorpedoesAvailable.Add(_typeToLoad);
                    _loadTimer = 5;
                }
            }
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return "Reloading torpedoes.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return null;
        }
    }
}
