﻿using Autofac;
using System;
using System.Linq;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;
using Xenosea.Resources;

namespace Xenosea.Logic.AI
{
    public class ScanForContrabandGoal : IBotGoal
    {
        public int Priority => ScanForContrabandGoalType.PRIORITY;

        private SonarContactRecord _sonarContactRecord;
        private ISimulationEventBus _simulationEventBus;
        private IWorldManager _worldManager;
        private INotificationSystem _notificationSystem;
        private bool _waitingOnUltimatum;
        private DateTime _ultimatumTime;
        
        public ScanForContrabandGoal(
            SonarContactRecord sonarContactRecord)
        {
            _sonarContactRecord = sonarContactRecord;
            _simulationEventBus = IocManager.IocContainer.Resolve<ISimulationEventBus>();
            _worldManager = IocManager.IocContainer.Resolve<IWorldManager>();
            _notificationSystem = IocManager.IocContainer.Resolve<INotificationSystem>();
        }

        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return _sonarContactRecord.IsValidInstance
                && _sonarContactRecord.IsDetected
                && _sonarContactRecord.ContactType != ContactType.Wreck
                && _sonarContactRecord.ContactType != ContactType.Torpedo
                && !(_sonarContactRecord.AiData.ContainsKey("cargo_scanned") && !_waitingOnUltimatum);
        }

        public void Update(GoalBotController controller, float delta)
        {
            if(_waitingOnUltimatum)
            {
                if(controller.Submarine.RealPosition.DistanceTo(_sonarContactRecord.EstimatedPosition) < 300)
                {
                    controller.AutopilotTo(_sonarContactRecord.EstimatedPosition, delta,1);
                    controller.Submarine.AimPoint = _sonarContactRecord.EstimatedPosition;
                }

                if((_worldManager.CurrentDate - _ultimatumTime).TotalSeconds > 60)
                {
                    _waitingOnUltimatum = false;

                    if (_sonarContactRecord.SonarContact is Submarine targetSubmarine
                        && controller.Submarine.Faction.Contraband.Any(c => targetSubmarine.CommodityStorage.GetQty(c) > 0))
                    {
                        _sonarContactRecord.AggrivationScore = 100;

                        controller.Submarine.BroadcastAggrivation(_sonarContactRecord);

                        _notificationSystem.Broadcast(controller.Submarine, "Time's up, opening fire!");
                    }
                }
            }
            else if(_sonarContactRecord.CargoScan && !_sonarContactRecord.AiData.ContainsKey("cargo_scanned"))
            {
                _sonarContactRecord.AiData.Add("cargo_scanned", string.Empty);

                if (_sonarContactRecord.SonarContact is Submarine targetSubmarine
                    && controller.Submarine.Faction.Contraband.Any(c => targetSubmarine.CommodityStorage.GetQty(c) > 0))
                {
                    var contrabandFound = controller.Submarine.Faction.Contraband
                        .Where(c => targetSubmarine.CommodityStorage.GetQty(c) > 0)
                        .ToArray();

                    _waitingOnUltimatum = true;
                    _ultimatumTime = _worldManager.CurrentDate;

                    _simulationEventBus.SendEvent(new CargoScanUltimatumSimulationEvent(controller.Submarine, targetSubmarine, contrabandFound, true));
                }
                else
                {
                    _notificationSystem.Broadcast(controller.Submarine, "You're clean.");
                }
            }
            else
            {
                controller.AutopilotTo(_sonarContactRecord.EstimatedPosition, delta,1);
                controller.Submarine.AimPoint = _sonarContactRecord.EstimatedPosition;
            }
        }

        public void Update(IStation station, float delta)
        {
            station.AimPoint = _sonarContactRecord.EstimatedPosition;
        }

        public string MakeBroadcast()
        {
            return $"{_sonarContactRecord.FriendlyName}, I'm scanning your cargo hold.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return evt is CargoScanUltimatumResponseSimulationEvent response
                && response.DemandingSubmarine == controller.Submarine;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
            if(evt is CargoScanUltimatumResponseSimulationEvent response
                && response.DemandingSubmarine == controller.Submarine)
            {
                _waitingOnUltimatum = false;

                if (!response.WasAgreedTo)
                {
                    var sonarContact = controller.Submarine.SonarContactRecords.Values
                        .Where(scr => scr.SonarContact == response.TargetSubmarine)
                        .FirstOrDefault();

                    if(sonarContact != null)
                    {
                        sonarContact.AggrivationScore = 100;

                        controller.Submarine.BroadcastAggrivation(sonarContact);

                        _notificationSystem.Broadcast(controller.Submarine, "So be it, opening fire!");
                    }
                }
            }
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return null;
        }
    }
}
