﻿using Godot;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class UndockMinisubGoal : IBotGoal
    {
        public int Priority => UndockMinisubGoalType.PRIORITY;

        private float _undockPercent = 0;

        public UndockMinisubGoal()
        {
        }

        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return controller is MinisubController minisubController 
                && minisubController.IsUndocking
                && Object.IsInstanceValid(minisubController.Home);
        }

        public void Update(GoalBotController controller, float delta)
        {
            _undockPercent += delta * 0.2f;

            if (controller is MinisubController minisubController)
            {
                var from = minisubController.Home.GlobalTransform.origin;
                var to = minisubController.Home.ApproachPoint.GlobalTransform.origin;

                var pos = minisubController.Home.GlobalTransform.origin + ((to - from) * _undockPercent);

                controller.Submarine.LookAtFromPosition(
                    pos,
                    pos - minisubController.Home.GlobalTransform.basis.z,
                    controller.Submarine.UpVector);

                if(_undockPercent >= 1)
                {
                    if(minisubController.Home.GetParent() is Submarine)
                    {
                        controller.Submarine.RemoveCollisionExceptionWith(minisubController.Home.GetParent());
                    }

                    controller.Submarine.CollisionLayer = 0b00000000000000000001;
                    minisubController.IsUndocking = false;
                    controller.Submarine.Mode = RigidBody.ModeEnum.Rigid;
                }
            }
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return "Launching.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return null;
        }
    }
}
