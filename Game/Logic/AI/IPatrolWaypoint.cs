﻿using Godot;

namespace Xenosea.Logic.AI
{
    public interface IPatrolWaypoint
    {
        Vector3 Position { get; }
        bool IsSatisfied(GoalBotController controller);
        void UpdateOrders(GoalBotController controller, float delta);
    }
}
