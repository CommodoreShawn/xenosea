﻿using Godot;
using System.Linq;
using Xenosea.Logic.Detection;
using Xenosea.Logic.SimulationEvents;
using Xenosea.Resources;

namespace Xenosea.Logic.AI
{
    public class SalvageWreckGoal : IBotGoal
    {
        public int Priority => 20; // high priority to prevent interruption

        private SonarContactRecord _salvageTarget;

        private bool _isApproaching;
        private bool _isGathering;

        public SalvageWreckGoal(
            SonarContactRecord salvageTarget)
        {
            _salvageTarget = salvageTarget;
            _isApproaching = true;
        }

        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return _isApproaching
                || _isGathering;
        }

        public void Update(GoalBotController controller, float delta)
        {
            if(_salvageTarget == null || !_salvageTarget.IsValidInstance)
            {
                if (_isApproaching)
                {
                    _isApproaching = false;
                    _isGathering = true;
                }
            }

            if(_isApproaching)
            {
                var goalPoint = _salvageTarget.SonarContact.RealPosition + controller.Submarine.UpVector * 20;

                var dist = controller.Submarine.RealPosition.DistanceTo(goalPoint);

                if(dist > 300)
                {
                    controller.AutopilotTo(goalPoint, delta,1);
                }
                else if (dist > 2)
                {
                    controller.CarefulAutopilotTo(goalPoint, delta, 0.5f);
                }
                else
                {
                    controller.Submarine.Dive = 0;
                    controller.Submarine.Throttle = 0;
                    controller.Submarine.Rudder = 0;

                    _isApproaching = false;
                    _isGathering = true;

                    foreach (var diveTeam in controller.Submarine.DiveTeams)
                    {
                        if(diveTeam.IsValidTarget(_salvageTarget))
                        {
                            diveTeam.Deploy(_salvageTarget);
                        }
                    }
                }
            }

            if(_isGathering)
            {
                if (controller.Submarine.DiveTeams.All(dt => dt.DeployedDiver == null && !dt.DiverIsRecovering))
                {
                    _isGathering = false;
                }
            }
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return $"Salvaging {_salvageTarget.SonarContact.FriendlyName}.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return null;
        }
    }
}
