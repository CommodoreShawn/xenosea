﻿using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class DockToStationGoal : IBotGoal
    {
        public int Priority => DockToStationGoalType.PRIORITY;

        private IStation _station;
        private bool _failed;

        public DockToStationGoal(IStation station)
        {
            _station = station;
        }

        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return _failed || controller.Submarine.IsDockedTo != _station;
        }

        public void Update(GoalBotController controller, float delta)
        {
            controller.Submarine.AimPoint = controller.Submarine.GlobalTransform.origin + controller.Submarine.GlobalTransform.basis.z;

            if (controller.Submarine.RealPosition.DistanceTo(_station.DockWaitingArea.GlobalTransform.origin) > 1000)
            {
                controller.AutopilotTo(_station.DockWaitingArea.GlobalTransform.origin, delta, 1);
            }
            else
            {
                if (controller.DockingSequence != null)
                {
                    controller.DockingSequence.Update(controller, delta);
                }
                else
                {
                    controller.DockingSequence = _station.RequestDocking(controller.Submarine);

                    controller.Submarine.Throttle = 0;
                    controller.Submarine.Rudder = 0;
                    controller.Submarine.Dive = 0;

                    if (controller.DockingSequence == null)
                    {
                        _failed = true;
                    }
                }
            }
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return "Docking at " + _station.FriendlyName;
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return null;
        }
    }
}
