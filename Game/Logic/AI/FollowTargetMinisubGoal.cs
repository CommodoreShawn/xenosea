﻿using Xenosea.Logic.Detection;
using Xenosea.Logic.SimulationEvents;
using Xenosea.Resources;

namespace Xenosea.Logic.AI
{
    public class FollowTargetMinisubGoal : IBotGoal
    {
        public int Priority { get; private set; }
        private SonarContactRecord _target;

        public FollowTargetMinisubGoal(
            SonarContactRecord sonarContactRecord,
            int priority)
        {
            Priority = priority;
            _target = sonarContactRecord;
        }

        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return controller is MinisubController minisubController
                && minisubController.Home.Target is SonarContactRecord targetRecord
                && targetRecord.SonarContact == _target.SonarContact
                && _target.IsDetected
                && _target.SonarContact.ContactType != ContactType.Wreck
                && _target.SonarContact.ContactType != ContactType.Torpedo
                && !_target.IsHostileTo(controller.Submarine)
                && !_target.IsHostileTo(minisubController.Home.Combatant);
        }

        public void Update(GoalBotController controller, float delta)
        {
            controller.AutopilotFollow(_target.EstimatedPosition, delta);
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return "Escorting target.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return $"We are escorting {_target.FriendlyName}";
        }
    }
}
