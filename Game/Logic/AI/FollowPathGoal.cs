﻿using System.Collections.Generic;
using Xenosea.Logic.Detection;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class FollowPathGoal : IBotGoal
    {
        public int Priority { get; private set; }

        private IAiPath _path;
        private Queue<IPatrolWaypoint> _pointQueue;

        public FollowPathGoal(IAiPath path, int priority)
        {
            Priority = priority;
            _path = path;
            _pointQueue = path.GetWaypoints();
        }

        public bool IsValid(IStation station)
        {
            return _pointQueue.Count > 0;
        }

        public bool IsValid(GoalBotController controller)
        {
            return _pointQueue.Count > 0;
        }

        public void Update(GoalBotController controller, float delta)
        {
            controller.Submarine.AimPoint = controller.Submarine.GlobalTransform.origin + controller.Submarine.GlobalTransform.basis.z;

            if (controller.DockingSequence != null)
            {
                if (!controller.DockingSequence.IsUndocking)
                {
                    controller.DockingSequence.Undock();
                }

                controller.DockingSequence.Update(controller, delta);

                if (controller.DockingSequence.IsFinished)
                {
                    controller.DockingSequence = null;
                }
            }
            else if (_pointQueue.Count > 0)
            {
               if(_pointQueue.Peek().IsSatisfied(controller))
                {
                    _pointQueue.Dequeue();

                    if (_pointQueue.Count < 1)
                    {
                        controller.Submarine.LocalHub = _path.DestinationHub;
                    }
                }
               else
                {
                    _pointQueue.Peek().UpdateOrders(controller, delta);
                }
            }
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return "Setting course for " + _path.DestinationHub.Name;
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return $"We are on patrol to {_path.DestinationHub.Name}.";
        }
    }
}
