﻿using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.AI.SimulationGoals
{
    public interface ISimulationBotGoal
    {
        int Priority { get; }
        bool IsValid { get; }
        string Description { get; }
        bool AvoidBattle { get; }

        void OnRemoved();
        void SimulationUpdate(float delta);
        void SaveState(SerializationObject goalRoot);
        string MakeFollowGoalBroadcast();
        void DetailedSimUpdate(GoalBotController controller, float delta);
        string DescribeGoalForHail(GoalBotController controller);
    }
}
