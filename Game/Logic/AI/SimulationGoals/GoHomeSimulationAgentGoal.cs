﻿using System;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.AI.SimulationGoals
{
    public class GoHomeSimulationAgentGoal : ISimulationBotGoal
    {
        private readonly SimulationAgent _agent;

        public int Priority { get; }

        public bool IsValid { get; private set; }

        public string Description => "Returning home.";

        public bool AvoidBattle => true;

        private SimulationStation _destination;

        private SimulationAgentPather _pather;

        public GoHomeSimulationAgentGoal(
            int priority,
            SimulationAgent agent)
        {
            Priority = priority;
            _agent = agent;
            _pather = new SimulationAgentPather(_agent, true);
            IsValid = true;
            _destination = _agent.HomeStation;
        }

        public void OnRemoved()
        {
        }

        public void SimulationUpdate(float delta)
        {
            if (_agent.Location == _destination.Location)
            {
                IsValid = false;
            }
            else
            {
                _pather.UpdatePath(_destination.Location);

                if (_pather.PathSuccessful)
                {
                    if (!_agent.InDetailedSim && _agent.MovingTo == null)
                    {
                        _agent.MovingTo = _pather.NextPoint;
                    }
                }
            }
        }

        public void SaveState(SerializationObject goalRoot)
        {
            goalRoot.SetAttribute("goal_type", "GoHomeSimulationAgentGoal");
            goalRoot.SetAttribute("priority", Priority.ToString());
            goalRoot.SetAttribute("destination", _destination.Name);
        }


        public static ISimulationBotGoal LoadState(
            IWorldManager worldManager,
            SerializationObject goalRoot,
            SimulationAgent agent)
        {
            var goal = new GoHomeSimulationAgentGoal(
                Convert.ToInt32(goalRoot.GetAttribute("priority")),
                agent);

            goal._destination = goalRoot.LookupReferenceOrNull("destination", worldManager.SimulationStations, s => s.Name);

            return goal;
        }

        public string MakeFollowGoalBroadcast()
        {
            return $"Returning to {_agent.HomeStation.Name}";
        }

        public void DetailedSimUpdate(GoalBotController controller, float delta)
        {
            if (controller.Submarine.IsDockedTo != null
                && controller.Submarine.IsDockedTo is WorldStation worldStation
                && worldStation.BackingStation == _destination)
            {
                IsValid = false;
            }
            else if (controller.DockingSequence != null
                && controller.DockingSequence.Station is WorldStation worldStation1
                && worldStation1.BackingStation == _destination)
            {
                controller.DockingSequence.Update(controller, delta);
            }
            else if (controller.DockingSequence != null)
            {
                if (!controller.DockingSequence.IsUndocking)
                {
                    controller.DockingSequence.Undock();
                }

                controller.DockingSequence.Update(controller, delta);
            }
            else if (_pather.PathSuccessful && _pather.NextPoint != null)
            {
                var autopilotPoint = Util.GetNavigationCenter(_pather.NextPoint);

                controller.AutopilotTo(autopilotPoint, delta, 1);

                controller.Submarine.DebugText = $"Go Home: next point: " + controller.Submarine.RealPosition.DistanceTo(autopilotPoint).ToString("N0");

                if (controller.Submarine.RealPosition.DistanceTo(autopilotPoint) <= 500)
                {
                    _agent.Location = _pather.NextPoint;
                }
            }
            else
            {
                if (_destination.DetailedRepresentation != null)
                {
                    var dockWaitingArea = _destination.DetailedRepresentation.DockWaitingArea;

                    if (controller.Submarine.RealPosition.DistanceTo(dockWaitingArea.GlobalTransform.origin) > 1000)
                    {
                        controller.AutopilotTo(dockWaitingArea.GlobalTransform.origin, delta,1);
                    }
                    else
                    {
                        controller.DockingSequence = _destination.DetailedRepresentation.RequestDocking(controller.Submarine);

                        controller.Submarine.Throttle = 0;
                        controller.Submarine.Rudder = 0;
                        controller.Submarine.Dive = 0;
                    }
                }
            }
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return $"We are returning to {_agent.HomeStation.Name}";
        }
    }
}
