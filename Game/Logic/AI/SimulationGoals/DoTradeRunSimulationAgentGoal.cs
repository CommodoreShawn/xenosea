﻿using System;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.Infrastructure.Missions;

namespace Xenosea.Logic.AI.SimulationGoals
{
    public class DoTradeRunSimulationAgentGoal : ISimulationBotGoal
    {
        private readonly int STATE_LOADING = 0;
        private readonly int STATE_UNLOADING = 1;
        private readonly int STATE_TRAVELLING = 2;

        private readonly IMissionTracker _missionTracker;
        private readonly IWorldManager _worldManager;
        private readonly SimulationAgent _agent;
        private readonly bool _onlyToHome;

        public int Priority { get; }

        public bool IsValid { get; private set; }
        
        public string Description => "On a trade run.";

        public bool AvoidBattle => true;

        private SimulationStation _destination;

        private SimulationAgentPather _pather;

        private float _transferWait;
        private int _state;

        public DoTradeRunSimulationAgentGoal(
            int priority,
            IMissionTracker missionTracker,
            IWorldManager worldManager,
            SimulationAgent agent,
            bool onlyToHome)
        {
            Priority = priority;
            _missionTracker = missionTracker;
            _worldManager = worldManager;
            _agent = agent;
            _pather = new SimulationAgentPather(_agent, true);
            IsValid = true;
            _state = STATE_LOADING;
            _onlyToHome = onlyToHome;
        }

        public void OnRemoved()
        {
        }

        public void SimulationUpdate(float delta)
        {
            _transferWait -= delta;

            if(_transferWait > 0)
            {
                return;
            }

            var currentTradeMissions = _missionTracker.GetCurrentMissions(_agent)
                .OfType<SimulationTradeMission>()
                .ToArray();

            if (_state == STATE_LOADING)
            {
                if (_agent?.Location?.SimulationStation == null)
                {
                    IsValid = false;
                    return;
                }

                var missionOptions = _missionTracker.GetAvailableMissions(_agent.Location.SimulationStation)
                    .OfType<SimulationTradeMission>()
                    .Where(m => m.Quantity <= _agent.RemainingCargoCapacity)
                    .Where(m => _agent.Faction.GetStandingTo(m.ForFaction) >= _agent.Faction.MinimumFactionStandingForTrade)
                    .Where(m => _agent.Faction.GetStandingTo(m.Destination.Faction) >= _agent.Faction.MinimumFactionStandingForTrade)
                    .ToArray();

                if(_destination == null)
                {
                    if(_onlyToHome)
                    {
                        _destination = _agent.HomeStation;
                    }
                    else if (currentTradeMissions.Any())
                    {
                        _destination = currentTradeMissions
                            .First()
                            .Destination;
                    }
                    else
                    {
                        _destination = missionOptions.FirstOrDefault()?.Destination;
                    }
                }

                if(_destination != null)
                {
                    var newMission = missionOptions
                        .Where(x => x.Destination == _destination)
                        .Where(m => m.Quantity <= _agent.RemainingCargoCapacity)
                        .Where(m => _agent.Faction.GetStandingTo(m.ForFaction) >= _agent.Faction.MinimumFactionStandingForTrade)
                        .FirstOrDefault();

                    if(newMission != null)
                    {
                        _missionTracker.PickupMission(newMission, _agent.Location.SimulationStation, _agent);
                        _transferWait = 3.0f;
                    }
                    else if(currentTradeMissions.Any())
                    {
                        _state = STATE_TRAVELLING;
                    }
                }
            }
            else if(_state == STATE_UNLOADING)
            {
                if(currentTradeMissions.Any())
                {
                    _missionTracker.DeliverMission(currentTradeMissions.First(), _destination, _agent);
                    _transferWait = 1.0f;
                }
                else
                {
                    IsValid = false;
                }
            }
            else if(_state == STATE_TRAVELLING)
            {
                if (!_agent.InDetailedSim && _agent.Location == _destination.Location)
                {
                    _state = STATE_UNLOADING;
                }
                else
                {
                    _pather.UpdatePath(_destination.Location);

                    if (_pather.PathSuccessful)
                    {
                        if (!_agent.InDetailedSim && _agent.MovingTo == null)
                        {
                            _agent.MovingTo = _pather.NextPoint;
                        }
                    }
                }
            }
        }

        public void SaveState(SerializationObject goalRoot)
        {
            goalRoot.SetAttribute("goal_type", "DoTradeRunSimulationAgentGoal");
            goalRoot.SetAttribute("priority", Priority.ToString());
            goalRoot.SetAttribute("destination", _destination?.Name);
            goalRoot.SetAttribute("only_to_home", _onlyToHome.ToString());
            goalRoot.SetAttribute("state", _state.ToString());
        }

        public static ISimulationBotGoal LoadState(
            IWorldManager worldManager, 
            IMissionTracker missionTracker,
            SerializationObject goalRoot, 
            SimulationAgent agent)
        {
            var goal = new DoTradeRunSimulationAgentGoal(
                Convert.ToInt32(goalRoot.GetAttribute("priority")),
                missionTracker,
                worldManager,
                agent,
                Convert.ToBoolean(goalRoot.GetAttribute("only_to_home")));

            goal._destination = goalRoot.LookupReferenceOrNull("destination", worldManager.SimulationStations, s => s.Name);
            goal._state = Convert.ToInt32(goalRoot.GetAttribute("state"));

            return goal;
        }

        public string MakeFollowGoalBroadcast()
        {
            return "Departing on trade run.";
        }

        public void DetailedSimUpdate(GoalBotController controller, float delta)
        {
            controller.Submarine.DebugText = $"State: {_state}";

            if (_state == STATE_TRAVELLING)
            {
                if(controller.DockingSequence != null
                    && !Godot.Object.IsInstanceValid(controller.DockingSequence.Station as Godot.Object))
                {
                    controller.DockingSequence = null;
                }

                if (controller.Submarine.IsDockedTo != null
                    && controller.Submarine.IsDockedTo is WorldStation worldStation
                    && worldStation.BackingStation == _destination)
                {
                    controller.Submarine.DebugText = $"Docked and unloading";
                    _state = STATE_UNLOADING;
                }
                else if (controller.DockingSequence != null
                    && controller.DockingSequence.Station is WorldStation worldStation1
                    && worldStation1.BackingStation == _destination)
                {
                    controller.Submarine.DebugText = $"Docking sequence";
                    controller.DockingSequence.Update(controller, delta);
                }
                else if (controller.DockingSequence != null)
                {
                    controller.Submarine.DebugText = $"Undocking sequence";

                    if (!controller.DockingSequence.IsUndocking)
                    {
                        controller.DockingSequence.Undock();
                    }

                    controller.DockingSequence.Update(controller, delta);

                    if(controller.DockingSequence.IsFinished)
                    {
                        controller.DockingSequence = null;
                    }
                }
                else if (controller.Submarine.RealPosition.DistanceTo(Util.GetNavigationCenter(_destination.Location)) < Constants.DATUM_DISTANCE * 1.5f)
                {
                    if (_destination.DetailedRepresentation != null
                        && Godot.Object.IsInstanceValid(_destination.DetailedRepresentation))
                    {
                        var dockWaitingArea = _destination.DetailedRepresentation.DockWaitingArea;

                        if (controller.Submarine.RealPosition.DistanceTo(dockWaitingArea.GlobalTransform.origin) > 100)
                        {
                            controller.AutopilotTo(dockWaitingArea.GlobalTransform.origin, delta, 1);
                        }
                        else
                        {
                            controller.DockingSequence = _destination.DetailedRepresentation.RequestDocking(controller.Submarine);

                            controller.Submarine.Throttle = 0;
                            controller.Submarine.Rudder = 0;
                            controller.Submarine.Dive = 0;
                        }
                    }
                }
                else
                {
                    _pather.UpdatePath(_destination.Location);

                    if (_pather.PathSuccessful && _pather.NextPoint != null)
                    {
                        var autopilotPoint = Util.GetNavigationCenter(_pather.NextPoint);
                        controller.AutopilotTo(autopilotPoint, delta,1);

                        controller.Submarine.DebugText = $"Autopilot to next point: " + controller.Submarine.RealPosition.DistanceTo(autopilotPoint).ToString("N0");

                        if (controller.Submarine.RealPosition.DistanceTo(autopilotPoint) <= 500)
                        {
                            _agent.Location = _pather.NextPoint;
                        }
                    }
                }
            }
            else if (controller.DockingSequence != null
                    && Godot.Object.IsInstanceValid(controller.DockingSequence.Station as Godot.Object))
            {
                controller.DockingSequence.Update(controller, delta);
            }
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            var commodities = controller.Submarine.CommodityStorage.CommoditiesPresent
                .Select(x => x.Name)
                .ToArray();

            if (!commodities.Any() || _destination == null)
            {
                return "We're gathering cargo for a trade run.";
            }

            return $"We're on a trade run, carrying {TextUtil.JoinWithAnd(commodities)} to {_destination.Name}.";
        }
    }
}
