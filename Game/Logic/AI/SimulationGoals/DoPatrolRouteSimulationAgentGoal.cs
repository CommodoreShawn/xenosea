﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;
using Xenosea.Logic.Infrastructure.Interfaces;

namespace Xenosea.Logic.AI.SimulationGoals
{
    public class DoPatrolRouteSimulationAgentGoal : ISimulationBotGoal
    {
        private readonly SimulationAgent _agent;
        private readonly ISimulationBattleHandler _simulationBattleHandler;
        private readonly IWorldManager _worldManager;

        public int Priority { get; }

        public bool IsValid { get; private set; }
        public string Description => "Patrolling.";

        public bool AvoidBattle => false;

        private List<WorldDatum> _route;
        private SimulationStation _source;
        private float _combatCheckTimer;
        private Random _random;

        public DoPatrolRouteSimulationAgentGoal(
            int priority,
            SimulationAgent agent,
            SimulationStation source,
            ISimulationBattleHandler simulationBattleHandler,
            IWorldManager worldManager)
        {
            Priority = priority;
            _agent = agent;
            IsValid = true;
            _source = source;
            _random = new Random();
            _worldManager = worldManager;
            _simulationBattleHandler = simulationBattleHandler;

            if (_agent.Location?.SimulationStation?.PerimiterPatrols?.Any() ?? false)
            {
                _route = _agent.Location.SimulationStation
                    .PerimiterPatrols[_random.Next(_agent.Location.SimulationStation.PerimiterPatrols.Count)]
                    .ToList();

                _route.RemoveAll(x => x == null);
            }
            else
            {
                _route = new List<WorldDatum>();
            }
        }

        public void OnRemoved()
        {
        }

        public void SimulationUpdate(float delta)
        {
            _combatCheckTimer -= delta;

            if(_combatCheckTimer < 0 && _agent.CurrentBattle == null && !_agent.InDetailedSim)
            {
                _combatCheckTimer = _random.Next(30, 90);

                var ownPos = _agent.RoughPosition;

                var catchValue = _random.NextDouble();

                var caughtTargets = _worldManager.SimulationAgents
                    .Where(x => catchValue > x.Stealth 
                        && x.BattleEscapeTimer <= 0
                        && !x.InDetailedSim
                        && _agent.Faction.IsHostileTo(x.Faction))
                    .Where(x => (x.RoughPosition - ownPos).Length() <= 5000)
                    .ToArray();

                if(caughtTargets.Any())
                {
                    var target = caughtTargets[_random.Next(caughtTargets.Length)];

                    if (target.CurrentBattle != null)
                    {
                        _agent.CurrentBattle = target.CurrentBattle;
                        target.CurrentBattle.Participants.Add(_agent);
                    }
                    else
                    {
                        if (_random.NextDouble() >= target.Stealth)
                        {
                            var battle = _simulationBattleHandler.GetOrStartBattle(_agent.Location);

                            battle.Participants.Add(_agent);
                            _agent.CurrentBattle = battle;
                            battle.Participants.Add(target);
                            target.CurrentBattle = battle;
                        }
                    }
                }
            }

            if (!_route.Any())
            {
                IsValid = false;
            }
            else
            {
                if (_agent.Location == _route.First())
                {
                    _route.RemoveAt(0);
                }

                if (_route.Count > 0)
                {
                    _agent.MovingTo = _route.First();
                }
            }
        }

        public void SaveState(SerializationObject goalRoot)
        {
            goalRoot.SetAttribute("goal_type", "DoPatrolRouteSimulationAgentGoal");
            goalRoot.SetAttribute("priority", Priority.ToString());
            if (_route != null)
            {
                goalRoot.SetAttribute("route", string.Join(";", _route.Where(x => x != null).Select(x => x.Id)));
            }
            goalRoot.SetAttribute("source", _source.Name);
        }

        public static ISimulationBotGoal LoadState(
            IWorldManager worldManager,
            ISimulationBattleHandler simulationBattleHandler,
            SerializationObject goalRoot,
            SimulationAgent agent)
        {
            var goal = new DoPatrolRouteSimulationAgentGoal(
                Convert.ToInt32(goalRoot.GetAttribute("priority")),
                agent,
                goalRoot.LookupReference("source", worldManager.SimulationStations, s => s.Name),
                simulationBattleHandler,
                worldManager);

            if (goalRoot.HasAttribute("route"))
            {
                goal._route = goalRoot.GetAttribute("route")
                    .Split(';')
                    .Select(x => worldManager.WorldDatums.FirstOrDefault(d => d.Id == x))
                    .Where(x => x != null)
                    .ToList();
            }
            return goal;
        }

        public string MakeFollowGoalBroadcast()
        {
            return "Departing on patrol.";
        }

        public void DetailedSimUpdate(GoalBotController controller, float delta)
        {
            if (_route.Any())
            {
                var nextWaypoint = Util.GetNavigationCenter(_route.First());

                if(controller.Submarine.RealPosition.DistanceTo(nextWaypoint) < 500)
                {
                    _agent.Location = _route[0];
                    _route.RemoveAt(0);
                }
                else
                {
                    controller.AutopilotTo(nextWaypoint, delta, 1);
                }
            }
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return $"We are on patrol from {_source.Name}.";
        }
    }
}
