﻿using Xenosea.Logic.Detection;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class InvestigateContactGoal : IBotGoal
    {
        public int Priority { get; private set; }

        private SonarContactRecord _sonarContactRecord;
        private bool _minisubTargetingFromParent;

        public InvestigateContactGoal(
            SonarContactRecord sonarContactRecord,
            int priority,
            bool minisubTargetingFromParent)
        {
            _sonarContactRecord = sonarContactRecord;
            _minisubTargetingFromParent = minisubTargetingFromParent;
            Priority = priority;
        }

        public bool IsValid(IStation station)
        {
            return _sonarContactRecord.IsDetected
                && !_sonarContactRecord.Identified;
        }

        public bool IsValid(GoalBotController controller)
        {
            if (_minisubTargetingFromParent
                && controller is MinisubController minisubController)
            {
                if (minisubController.Home.Target is SonarContactRecord targetRecord)
                {
                    if(targetRecord.SonarContact != _sonarContactRecord.SonarContact)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            return _sonarContactRecord.IsDetected
                && !_sonarContactRecord.Identified;
        }

        public void Update(GoalBotController controller, float delta)
        {
            controller.AutopilotTo(_sonarContactRecord.EstimatedPosition, delta,1);
            controller.Submarine.AimPoint = _sonarContactRecord.EstimatedPosition;
        }

        public void Update(IStation station, float delta)
        {
            station.AimPoint = _sonarContactRecord.EstimatedPosition;
        }

        public string MakeBroadcast()
        {
            return "Investigating unknown contact.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return null;
        }
    }
}
