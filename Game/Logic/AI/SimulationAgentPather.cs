﻿using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic.AI
{
    public class SimulationAgentPather
    {
        private SimulationAgent _agent;
        private List<WorldDatum> _path;
        private WorldDatum _destination;
        public WorldDatum NextPoint => _path.FirstOrDefault();
        public bool PathSuccessful { get; private set; }

        private List<WorldDatum> _open;
        private List<WorldDatum> _closed;
        private Dictionary<WorldDatum, WorldDatum> _cameFrom;
        private Dictionary<WorldDatum, float> _distTo;
        private Dictionary<WorldDatum, float> _estToGoal;
        private bool _avoidDepth;

        public SimulationAgentPather(SimulationAgent agent, bool avoidDepth)
        {
            _agent = agent;
            _avoidDepth = avoidDepth;
            _path = new List<WorldDatum>();

            _open = new List<WorldDatum>();
            _closed = new List<WorldDatum>();
            _cameFrom = new Dictionary<WorldDatum, WorldDatum>();
            _distTo = new Dictionary<WorldDatum, float>();
            _path = new List<WorldDatum>();
            _estToGoal = new Dictionary<WorldDatum, float>();
        }

        public void UpdatePath(WorldDatum destination)
        {
            if (destination != _destination)
            {
                _destination = destination;
                ResetPathing();
            }

            if (PathSuccessful)
            {
                if (_agent.Location == NextPoint)
                {
                    _path.RemoveAt(0);
                }
            }
            else
            {
                var iterationLimit = 30;

                for (var i = 0; i < iterationLimit && !PathSuccessful&& _open.Any(); i++)
                {
                    var current = _open
                        .OrderBy(x => _distTo[x] + _estToGoal[x])
                        .First();

                    _open.Remove(current);
                    _closed.Add(current);

                    if (current == _destination)
                    {
                        _path = FinalizePath(current, _cameFrom);
                        PathSuccessful = true;
                    }
                    else
                    {
                        var neighbors = current.Neighbors
                            .Where(x => x.Depth > x.IceDepth)
                            .Where(x => !_closed.Contains(x));

                        foreach (var neighbor in neighbors)
                        {
                            var dist = _distTo[current] + Util.EstimateTravelDistance(current, neighbor) * DetermineAvoidanceWeight(neighbor, _avoidDepth);

                            if (_open.Contains(neighbor))
                            {
                                if (dist < _distTo[neighbor])
                                {
                                    _distTo[neighbor] = dist;
                                    _cameFrom[neighbor] = current;
                                }
                            }
                            else
                            {
                                _estToGoal.Add(neighbor, Util.EstimateTravelDistance(neighbor, _destination) * DetermineAvoidanceWeight(neighbor, _avoidDepth));
                                _open.Add(neighbor);
                                _distTo.Add(neighbor, dist);
                                _cameFrom.Add(neighbor, current);
                            }
                        }
                    }
                }
            }
        }

        private static float DetermineAvoidanceWeight(WorldDatum datum, bool avoidDepth)
        {
            if (!avoidDepth)
            {
                return 1;
            }
            else if (datum.Depth > Constants.DEPTH_MAX_SUB)
            {
                return 8;
            }
            else if (datum.Depth > Constants.DEPTH_MAX_SUIT)
            {
                return 4;
            }
            else
            {
                return 1;
            }
        }

        private void ResetPathing()
        {
            _open.Clear();
            _closed.Clear();
            _cameFrom.Clear();
            _distTo.Clear();
            _path.Clear();
            _estToGoal.Clear();
            PathSuccessful = false;

            _open.Add(_agent.Location);
            _cameFrom.Add(_agent.Location, null);
            _distTo.Add(_agent.Location, 0);
            _estToGoal.Add(_agent.Location, Util.EstimateTravelDistance(_agent.Location, _destination));
        }

        private static List<WorldDatum> FinalizePath(WorldDatum current, Dictionary<WorldDatum, WorldDatum> cameFrom)
        {
            var path = new List<WorldDatum>();
            path.Add(current);

            while (cameFrom[current] != null)
            {
                path.Add(cameFrom[current]);
                current = cameFrom[current];
            }

            path.Reverse();
            return path;
        }

        public static List<WorldDatum> DeterminePath(WorldDatum from, WorldDatum to)
        {
            var open = new List<WorldDatum>();
            var closed = new List<WorldDatum>();
            var cameFrom = new Dictionary<WorldDatum, WorldDatum>();
            var distTo = new Dictionary<WorldDatum, float>();
            var estToGoal = new Dictionary<WorldDatum, float>();

            open.Add(from);
            cameFrom.Add(from, null);
            distTo.Add(from, 0);
            estToGoal.Add(from, Util.EstimateTravelDistance(from, to));

            while(open.Any())
            {
                var current = open
                    .OrderBy(x => distTo[x] + estToGoal[x])
                    .First();

                open.Remove(current);
                closed.Add(current);

                if (current == to)
                {
                    return FinalizePath(current, cameFrom);
                }
                else
                {
                    var neighbors = current.Neighbors
                        .Where(x => !closed.Contains(x));

                    foreach (var neighbor in neighbors)
                    {
                        var dist = distTo[current] + Util.EstimateTravelDistance(current, neighbor) * DetermineAvoidanceWeight(neighbor, false);

                        if (open.Contains(neighbor))
                        {
                            if (dist < distTo[neighbor])
                            {
                                distTo[neighbor] = dist;
                                cameFrom[neighbor] = current;
                            }
                        }
                        else
                        {
                            estToGoal.Add(neighbor, Util.EstimateTravelDistance(neighbor, to) * DetermineAvoidanceWeight(neighbor, false));
                            open.Add(neighbor);
                            distTo.Add(neighbor, dist);
                            cameFrom.Add(neighbor, current);
                        }
                    }
                }
            }

            return new List<WorldDatum>();
        }
    }
}
