﻿using Autofac;
using Godot;
using System;
using System.Linq;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;
using Xenosea.Resources;

namespace Xenosea.Logic.AI
{
    public class ShootHostileContactGoal : IBotGoal
    {
        public int Priority => _priority;

        private SonarContactRecord _targetContactRecord;

        private bool _isStrafing;
        private float _strafeModeCounter;
        private float _torpLaunchDelay;
        private int _priority;
        private bool _minisubTargetingFromParent;
        private bool _didFighterWithdraw;
        private float _fighterWithdrawTimer;
        private float _fighterEvadeTimer;
        private bool _fighterEvadeUp;

        private INotificationSystem _notificationSystem;
        private Random _random;

        public ShootHostileContactGoal(
            SonarContactRecord sonarContactRecord,
            int priority,
            bool minisubTargetingFromParent)
        {
            _targetContactRecord = sonarContactRecord;
            _priority = priority;
            _minisubTargetingFromParent = minisubTargetingFromParent;

            _isStrafing = true;

            _notificationSystem = IocManager.IocContainer.Resolve<INotificationSystem>();
            _random = new Random();
        }

        public bool IsValid(IStation station)
        {
            return _targetContactRecord.IsDetected
                && _targetContactRecord.SonarContact.ContactType != ContactType.Wreck
                && _targetContactRecord.SonarContact.ContactType != ContactType.Torpedo;
        }

        public bool IsValid(GoalBotController controller)
        {
            if (_minisubTargetingFromParent
                && controller is MinisubController minisubController)
            {
                if (minisubController.Home.Target is SonarContactRecord targetRecord)
                {
                    if (targetRecord.SonarContact != _targetContactRecord.SonarContact)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            return _targetContactRecord.IsDetected
                && _targetContactRecord.SonarContact.ContactType != ContactType.Wreck
                && _targetContactRecord.SonarContact.ContactType != ContactType.Torpedo;
        }

        public void Update(GoalBotController controller, float delta)
        {
            var aimVector = _targetContactRecord.EstimatedPosition - controller.Submarine.RealPosition;
            var distance = aimVector.Length();

            var leadTime = 0f;
            if(controller.Submarine.TurretSystems.Any())
            {
                leadTime = distance / controller.Submarine.TurretSystems.Select(x => x.ActiveAmmo.LeadVelocity).Min();
            }

            var leadPoint = _targetContactRecord.EstimatedPosition + _targetContactRecord.LastKnownVelocity * leadTime;

            if(controller.Submarine.HintStrafingShooter || controller.Submarine.HintFighterShooter)
            {
                aimVector = leadPoint - controller.Submarine.RealPosition;
            }

            var yawToTarget = controller.Submarine.Transform.basis.Xform(Vector3.Back)
                .SignedAngleTo(aimVector, controller.Submarine.Transform.basis.y);

            if (controller.Submarine.HintBroadsideShooter)
            {
                ManuverForBroadsideShooting(delta, distance, yawToTarget, aimVector, controller);
            }
            else if (controller.Submarine.HintStrafingShooter)
            {
                ManuverForStrafeShooting(delta, distance, yawToTarget, aimVector, controller);
            }
            else if(controller.Submarine.HintFighterShooter)
            {
                ManuverForFighterShooting(delta, distance, yawToTarget, aimVector, controller);
            }
            else
            {
                ManuverForBasicShooting(delta, distance, aimVector, controller);
                
            }

            controller.Submarine.AimPoint = leadPoint;

            foreach (var turret in controller.Submarine.TurretSystems)
            {
                var newAmmo = turret.TurretModuleType.AmmunitionOptions
                    .Where(a => distance <= a.MaximumRange)
                    .FirstOrDefault();

                if (newAmmo != turret.ActiveAmmo && newAmmo != null)
                {
                    turret.ChangeAmmo(newAmmo);
                }

                if (distance <= turret.ActiveAmmo.MaximumRange)
                {
                    turret.Shoot(_targetContactRecord);
                }
            }

            _torpLaunchDelay -= delta;

            foreach (var torpedo in controller.Submarine.TorpedoTubes)
            {
                if (_torpLaunchDelay <= 0 && torpedo.LoadProgress >= torpedo.LoadTime && torpedo.ActiveAmmo != null)
                {
                    var directionToTarget = controller.Submarine.GlobalTransform.origin.DirectionTo(_targetContactRecord.EstimatedPosition);
                    var distToTarget = controller.Submarine.GlobalTransform.origin.DistanceTo(_targetContactRecord.EstimatedPosition);

                    var isAimed = !controller.Submarine.HintStrafingShooter || _isStrafing;
                    isAimed = isAimed && controller.Submarine.GlobalTransform.basis.z.AngleTo(directionToTarget) < Mathf.Pi / 2;

                    var inRange = distToTarget <= torpedo.ActiveAmmo.Speed * torpedo.ActiveAmmo.FuelTime;

                    if (isAimed && inRange)
                    {

                        torpedo.Target = _targetContactRecord;
                        torpedo.Shoot(_targetContactRecord);
                        _torpLaunchDelay = 2;
                    }
                }
            }

            foreach (var minisubHangar in controller.Submarine.MinisubHangars)
            {
                minisubHangar.Target = _targetContactRecord;

                if(minisubHangar.ActiveMinisub == null && minisubHangar.MinisubType != null
                    && minisubHangar.CurrentCharge >= 0.5 * minisubHangar.MinisubType.MaxCharge)
                {
                    minisubHangar.Shoot(_targetContactRecord);
                }
            }
        }

        private void ManuverForBroadsideShooting(
            float delta, 
            float distance, 
            float yawToTarget, 
            Vector3 aimVector, 
            GoalBotController controller)
        {
            if (controller.Submarine.UpVector.IsNormalized())
            {
                var steerOffset = Mathf.Clamp(2 - distance / 500, 0.5f, 1.5f) * Mathf.Pi / 2;
                var throttle = Mathf.Clamp(distance / 1000, 0.2f, 1f);
                if (yawToTarget < 0)
                {
                    controller.AutopilotHeading(aimVector.Rotated(controller.Submarine.UpVector, steerOffset), delta, throttle);
                }
                else
                {
                    controller.AutopilotHeading(aimVector.Rotated(controller.Submarine.UpVector, -steerOffset), delta, throttle);
                }

                controller.AutopilotAltitude(_targetContactRecord.EstimatedPosition, 0, delta);
            }
        }

        private void ManuverForStrafeShooting(
            float delta,
            float distance,
            float yawToTarget,
            Vector3 aimVector,
            GoalBotController controller)
        {
            _strafeModeCounter += delta;

            if (_isStrafing)
            {
                controller.AutopilotHeading(aimVector, delta, 1f);

                if (distance < 500 && _strafeModeCounter > 5)
                {
                    _isStrafing = false;
                    _strafeModeCounter = 0;
                }
            }
            else
            {
                if (Mathf.Abs(yawToTarget) > 3)
                {
                    controller.Submarine.DropDecoy();
                }

                controller.AutopilotHeading(-aimVector, delta, 1f);

                if (distance > 2000 || _strafeModeCounter > 30)
                {
                    _isStrafing = true;
                    _strafeModeCounter = 0;
                }
            }

            controller.AutopilotAltitude(_targetContactRecord.EstimatedPosition, 0, delta);
        }

        private void ManuverForFighterShooting(
            float delta,
            float distance,
            float yawToTarget,
            Vector3 aimVector,
            GoalBotController controller)
        {
            controller.Submarine.DebugText = string.Empty;

            var turretsReloading = controller.Submarine.TurretSystems
                        .All(ts => ts.Mounts.Where(m => m.Turret != null).Any(m => m.Turret.ReloadCounter < m.Turret.ReloadTime));

            var targetDistance = controller.Submarine.TurretSystems.Min(x => x.ActiveAmmo.MaximumRange) * 0.8f;

            if(!_didFighterWithdraw && turretsReloading)
            {
                _didFighterWithdraw = true;
                _fighterWithdrawTimer = 5;
            }

            var charging = false;

            if(_didFighterWithdraw && _fighterWithdrawTimer > 0)
            {
                controller.Submarine.DebugText = "Withdraw";

                _fighterWithdrawTimer -= delta;

                if(Mathf.Abs(yawToTarget) < 3)
                {
                    controller.AutopilotHeading(-aimVector, delta, 0.5f);
                }
                else
                {
                    controller.AutopilotHeading(-aimVector, delta, 1);
                }

                if (Mathf.Abs(yawToTarget) > 3)
                {
                    controller.Submarine.DropDecoy();
                }
            }
            else if (distance < targetDistance / 2 || turretsReloading || distance > targetDistance * 1.5)
            {
                controller.Submarine.DebugText = "Circle";

                var steerOffset = Mathf.Clamp(2 - distance / targetDistance, 0.5f, 1.5f) * Mathf.Pi / 2;
                if (yawToTarget < 0)
                {
                    controller.AutopilotHeading(aimVector.Rotated(controller.Submarine.UpVector.Normalized(), steerOffset), delta, 1);
                }
                else
                {
                    controller.AutopilotHeading(aimVector.Rotated(controller.Submarine.UpVector.Normalized(), -steerOffset), delta, 1);
                }
            }
            else
            {
                controller.Submarine.DebugText = "Charge";
                charging = true;
                _didFighterWithdraw = false;
                controller.AutopilotHeading(aimVector, delta, 1f);
            }

            if(charging)
            {
                controller.AutopilotAltitude(_targetContactRecord.EstimatedPosition, 0, delta);
            }
            else
            {
                if(_fighterEvadeTimer <= 0)
                {
                    _fighterEvadeTimer = (float)(_random.NextDouble() * 2) + 0.5f;
                    _fighterEvadeUp = _random.NextDouble() < 0.5;
                }

                _fighterEvadeTimer -= delta;

                if(_fighterEvadeUp)
                {
                    controller.Submarine.DebugText += " Up";
                    controller.AutopilotAltitude(_targetContactRecord.EstimatedPosition + controller.Submarine.UpVector * 50, 0, delta);
                }
                else
                {
                    controller.Submarine.DebugText += " Down";
                    controller.AutopilotAltitude(_targetContactRecord.EstimatedPosition + controller.Submarine.UpVector * -50, 0, delta);
                }
            }
        }

        private void ManuverForBasicShooting(
            float delta,
            float distance,
            Vector3 aimVector,
            GoalBotController controller)
        {
            if (distance < 500)
            {
                controller.AutopilotHeading(aimVector, delta, -0.25f);
            }
            else if (distance < 1000)
            {
                controller.AutopilotHeading(aimVector, delta, 0.25f);
            }
            else
            {
                controller.AutopilotHeading(aimVector, delta, 1f);
            }

            controller.AutopilotAltitude(_targetContactRecord.EstimatedPosition, 0, delta);
        }

        public void Update(IStation station, float delta)
        {
            var aimVector = _targetContactRecord.EstimatedPosition - station.RealPosition;
            var distance = aimVector.Length();

            var leadTime = 0f;
            if (station.TurretSystems.Any())
            {
                leadTime = distance / station.TurretSystems.Select(x => x.ActiveAmmo.LeadVelocity).Min();
            }

            station.AimPoint = _targetContactRecord.EstimatedPosition + _targetContactRecord.LastKnownVelocity * leadTime;

            foreach (var turret in station.TurretSystems)
            {
                var newAmmo = turret.TurretModuleType.AmmunitionOptions
                    .Where(a => distance < a.MaximumRange)
                    .FirstOrDefault();

                if (newAmmo != turret.ActiveAmmo && newAmmo != null)
                {
                    turret.ChangeAmmo(newAmmo);
                }

                if (distance <= turret.ActiveAmmo.MaximumRange)
                {
                    turret.Shoot(_targetContactRecord);
                }
            }

            foreach (var torpedo in station.TorpedoTubes)
            {
                if (torpedo.ActiveAmmo != null 
                    && distance <= torpedo.ActiveAmmo.Speed * torpedo.ActiveAmmo.FuelTime)
                {

                    torpedo.Target = _targetContactRecord;
                    torpedo.Shoot(_targetContactRecord);
                }
            }
        }

        public string MakeBroadcast()
        {
            return $"Engaging {_targetContactRecord.SonarContact.FriendlyName}.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return evt is PlayerHailSimulationEvent playerHailSimulationEvent 
                && playerHailSimulationEvent.TargetSubmarine == controller.Submarine;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
            if(evt is PlayerHailSimulationEvent playerHailSimulationEvent 
                && playerHailSimulationEvent.TargetSubmarine == controller.Submarine)
            {
                _notificationSystem.Broadcast(
                    controller.Submarine,
                    "I'm a little busy right now!");
            }
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return null;
        }
    }
}
