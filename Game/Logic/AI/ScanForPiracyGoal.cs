﻿using Autofac;
using System;
using System.Linq;
using Xenosea.Logic.Detection;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;
using Xenosea.Resources;

namespace Xenosea.Logic.AI
{
    public class ScanForPiracyGoal : IBotGoal
    {
        public int Priority => ScanForPiracyGoalType.PRIORITY;

        private SonarContactRecord _sonarContactRecord;
        private ISimulationEventBus _simulationEventBus;
        private IWorldManager _worldManager;
        private INotificationSystem _notificationSystem;
        private bool _waitingOnUltimatum;
        private DateTime _ultimatumTime;
        private CommodityType _demandedCommodity;
        
        public ScanForPiracyGoal(
            SonarContactRecord sonarContactRecord)
        {
            _sonarContactRecord = sonarContactRecord;
            _simulationEventBus = IocManager.IocContainer.Resolve<ISimulationEventBus>();
            _worldManager = IocManager.IocContainer.Resolve<IWorldManager>();
            _notificationSystem = IocManager.IocContainer.Resolve<INotificationSystem>();
        }

        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return _sonarContactRecord.IsValidInstance
                && _sonarContactRecord.IsDetected
                && _sonarContactRecord.ContactType != ContactType.Wreck
                && _sonarContactRecord.ContactType != ContactType.Torpedo
                && !(_sonarContactRecord.AiData.ContainsKey("piracy_checked") && !_waitingOnUltimatum);
        }

        public void Update(GoalBotController controller, float delta)
        {
            if(_waitingOnUltimatum)
            {
                if(controller.Submarine.RealPosition.DistanceTo(_sonarContactRecord.EstimatedPosition) < 300)
                {
                    controller.AutopilotTo(_sonarContactRecord.EstimatedPosition, delta,1);
                    controller.Submarine.AimPoint = _sonarContactRecord.EstimatedPosition;
                }

                if((_worldManager.CurrentDate - _ultimatumTime).TotalSeconds > 60)
                {
                    _waitingOnUltimatum = false;

                    if (_sonarContactRecord.SonarContact is Submarine targetSubmarine
                        && targetSubmarine.CommodityStorage.GetQty(_demandedCommodity) > 0)
                    {
                        _sonarContactRecord.AggrivationScore = 100;

                        controller.Submarine.BroadcastAggrivation(_sonarContactRecord);

                        _notificationSystem.Broadcast(controller.Submarine, "Time's up!");
                    }
                }
            }
            else if(_sonarContactRecord.CargoScan && !_sonarContactRecord.AiData.ContainsKey("piracy_checked"))
            {
                _sonarContactRecord.AiData.Add("piracy_checked", string.Empty);

                if (_sonarContactRecord.SonarContact is Submarine targetSubmarine)
                {
                    var valuableGoods = targetSubmarine.CommodityStorage.CommoditiesPresent
                        .Where(x => x.BaseValue >= 300)
                        .OrderBy(x => x.BaseValue * targetSubmarine.CommodityStorage.GetQty(x))
                        .Take(1)
                        .ToArray();

                    if (valuableGoods.Any())
                    {
                        _waitingOnUltimatum = true;
                        _ultimatumTime = _worldManager.CurrentDate;
                        _demandedCommodity = valuableGoods[0];
                        _simulationEventBus.SendEvent(new CargoScanUltimatumSimulationEvent(controller.Submarine, targetSubmarine, valuableGoods, false));
                    }
                    else
                    {
                        _notificationSystem.Broadcast(controller.Submarine, "You don't have anything I want.");
                    }
                }
            }
            else
            {
                controller.AutopilotTo(_sonarContactRecord.EstimatedPosition, delta,1);
                controller.Submarine.AimPoint = _sonarContactRecord.EstimatedPosition;
            }
        }

        public void Update(IStation station, float delta)
        {
            station.AimPoint = _sonarContactRecord.EstimatedPosition;
        }

        public string MakeBroadcast()
        {
            return $"Hey {_sonarContactRecord.FriendlyName}, carrying anything good?";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return evt is CargoScanUltimatumResponseSimulationEvent response
                && response.DemandingSubmarine == controller.Submarine;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
            if(evt is CargoScanUltimatumResponseSimulationEvent response
                && response.DemandingSubmarine == controller.Submarine)
            {
                _waitingOnUltimatum = false;

                if (!response.WasAgreedTo)
                {
                    var sonarContact = controller.Submarine.SonarContactRecords.Values
                        .Where(scr => scr.SonarContact == response.TargetSubmarine)
                        .FirstOrDefault();

                    if(sonarContact != null)
                    {
                        sonarContact.AggrivationScore = 100;

                        controller.Submarine.BroadcastAggrivation(sonarContact);

                        _notificationSystem.Broadcast(controller.Submarine, "Then I'll take it from your wreck!");
                    }
                }
            }
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return null;
        }
    }
}
