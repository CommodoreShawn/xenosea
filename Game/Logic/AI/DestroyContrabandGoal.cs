﻿using Godot;
using System.Linq;
using Xenosea.Logic.Detection;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class DestroyContrabandGoal : IBotGoal
    {
        public int Priority => DestroyContrabandGoalType.PRIORITY;

        private SonarContactRecord _targetContactRecord;

        public DestroyContrabandGoal(
            SonarContactRecord sonarContactRecord)
        {
            _targetContactRecord = sonarContactRecord;
        }

        public bool IsValid(IStation station)
        {
            return _targetContactRecord.IsValidInstance;
        }

        public bool IsValid(GoalBotController controller)
        {
            return _targetContactRecord.IsValidInstance;
        }

        public void Update(GoalBotController controller, float delta)
        {
            var aimVector = _targetContactRecord.EstimatedPosition - controller.Submarine.RealPosition;
            var distance = aimVector.Length();

            if (distance > 300)
            {
                controller.AutopilotTo(_targetContactRecord.EstimatedPosition, delta, 1);
            }
            else
            {
                var yawToTarget = controller.Submarine.Transform.basis.Xform(Vector3.Back)
                    .SignedAngleTo(aimVector, controller.Submarine.Transform.basis.y);

                if (controller.Submarine.HintBroadsideShooter)
                {
                    var steerOffset = Mathf.Clamp(2 - distance / 500, 0.5f, 1.5f) * Mathf.Pi / 2;
                    
                    if (yawToTarget < 0)
                    {
                        controller.AutopilotHeading(aimVector.Rotated(controller.Submarine.UpVector, steerOffset), delta, 0);
                    }
                    else
                    {
                        controller.AutopilotHeading(aimVector.Rotated(controller.Submarine.UpVector, -steerOffset), delta, 0);
                    }
                }
                else
                {
                    controller.AutopilotHeading(aimVector, delta, 0);
                }

                controller.AutopilotAltitude(_targetContactRecord.EstimatedPosition, 0, delta);

                controller.Submarine.AimPoint = _targetContactRecord.EstimatedPosition;

                foreach (var turret in controller.Submarine.TurretSystems)
                {
                    var newAmmo = turret.TurretModuleType.AmmunitionOptions
                        .Where(a => distance < a.MaximumRange)
                        .FirstOrDefault();

                    if (newAmmo != turret.ActiveAmmo && newAmmo != null)
                    {
                        turret.ChangeAmmo(newAmmo);
                    }

                    turret.Shoot(_targetContactRecord);
                }
            }
        }

        public void Update(IStation station, float delta)
        {
            station.AimPoint = _targetContactRecord.EstimatedPosition;

            var distance = _targetContactRecord.EstimatedPosition.DistanceTo(station.RealPosition);

            foreach (var turret in station.TurretSystems)
            {
                var newAmmo = turret.TurretModuleType.AmmunitionOptions
                    .Where(a => distance < a.MaximumRange)
                    .FirstOrDefault();

                if (newAmmo != turret.ActiveAmmo && newAmmo != null)
                {
                    turret.ChangeAmmo(newAmmo);
                }

                turret.Shoot(_targetContactRecord);
            }
        }

        public string MakeBroadcast()
        {
            return "Destroying contraband.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return null;
        }
    }
}
