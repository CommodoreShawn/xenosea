﻿using Godot;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class FollowHomeMinisubGoal : IBotGoal
    {
        public int Priority => FollowHomeMinisubGoalType.PRIORITY;

        public FollowHomeMinisubGoal()
        {
        }

        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return controller is MinisubController minisubController && Object.IsInstanceValid(minisubController.Home);
        }

        public void Update(GoalBotController controller, float delta)
        {
            if (controller is MinisubController minisubController && Object.IsInstanceValid(minisubController.Home))
            {
                controller.AutopilotFollow(minisubController.Home.GlobalTransform.origin, delta);
            }
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return "Forming up for escort.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            if (controller is MinisubController minisubController 
                && Object.IsInstanceValid(minisubController.Home))
            {
                var parentSub = minisubController.Home.FindParent<Submarine>();

                if(parentSub != null)
                {
                    return $"We are escorting {parentSub.FriendlyName}";
                }
            }

            return $"We are escorting.";
        }
    }
}
