﻿using Godot;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class GotoPointMinisubGoal : IBotGoal
    {
        public int Priority { get; private set; }
        private Vector3 _point;

        public GotoPointMinisubGoal(Vector3 point, int priority)
        {
            Priority = priority;
            _point = point;
        }

        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return controller is MinisubController minisubController
                && minisubController.Home.GotoPoint == _point;
        }

        public void Update(GoalBotController controller, float delta)
        {
            controller.AutopilotTo(_point, delta, 1.0f);
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return "Proceeding to destination.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return $"We are travelling.";
        }
    }
}
