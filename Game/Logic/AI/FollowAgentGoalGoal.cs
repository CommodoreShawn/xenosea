﻿using Xenosea.Logic.AI.SimulationGoals;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class FollowAgentGoalGoal : IBotGoal
    {
        public int Priority { get; }

        private ISimulationBotGoal _currentGoal;

        public FollowAgentGoalGoal(int priority, ISimulationBotGoal currentGoal)
        {
            Priority = priority;
            _currentGoal = currentGoal;
        }

        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return true;
        }

        public void Update(GoalBotController controller, float delta)
        {
            var goal = controller.Submarine?.AgentRepresentation?.Goal;

            if(goal != null)
            {
                _currentGoal = goal;

                controller.Submarine.DebugText = "Do Goal: " + goal.Description;

                goal.DetailedSimUpdate(controller, delta);
            }
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return _currentGoal.MakeFollowGoalBroadcast();
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return _currentGoal.DescribeGoalForHail(controller);
        }
    }
}
