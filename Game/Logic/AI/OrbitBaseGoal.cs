﻿using Godot;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class OrbitBaseGoal : IBotGoal
    {
        public int Priority => OrbitBaseGoalType.PRIORITY;

        public OrbitBaseGoal()
        {
        }

        public bool IsValid(IStation station)
        {
            return true;
        }

        public bool IsValid(GoalBotController controller)
        {
            return true;
        }

        public void Update(GoalBotController controller, float delta)
        {
            var aimVector = controller.Submarine.LocalHub.GlobalTransform.origin - controller.Submarine.RealPosition;
            var distance = aimVector.Length();

            var yawToTarget = controller.Submarine.Transform.basis.Xform(Vector3.Back)
                .SignedAngleTo(aimVector, controller.Submarine.Transform.basis.y);
            
            var steerOffset = Mathf.Clamp(2 - distance / 1000, 0.5f, 1.5f) * Mathf.Pi / 2;
            var throttle = Mathf.Clamp(distance / 2000, 0.2f, 1f);
            
            if (yawToTarget < 0)
            {
                controller.AutopilotHeading(aimVector.Rotated(controller.Submarine.UpVector, steerOffset), delta, throttle);
            }
            else
            {
                controller.AutopilotHeading(aimVector.Rotated(controller.Submarine.UpVector, -steerOffset), delta, throttle);
            }

            controller.AutopilotAltitude(controller.Submarine.LocalHub.GlobalTransform.origin, 0, delta);

            controller.Submarine.AimPoint = controller.Submarine.GlobalTransform.origin + controller.Submarine.GlobalTransform.basis.z;
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return $"Patrolling.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return "We are patrolling this area.";
        }
    }
}
