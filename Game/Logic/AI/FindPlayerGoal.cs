﻿using Autofac;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class FindPlayerGoal : IBotGoal
    {
        public int Priority => FindPlayerGoalType.PRIORITY;

        private readonly IPlayerData _playerData;

        public FindPlayerGoal()
        {
            _playerData = IocManager.IocContainer.Resolve<IPlayerData>();
        }

        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return true;
        }

        public void Update(GoalBotController controller, float delta)
        {
            controller.Submarine.AimPoint = controller.Submarine.GlobalTransform.origin + controller.Submarine.GlobalTransform.basis.z;

            if (_playerData.PlayerSub == null || !Godot.Object.IsInstanceValid(_playerData.PlayerSub))
            {
                controller.Submarine.QueueFree();
            }
            else
            {
                controller.AutopilotTo(_playerData.PlayerSub.RealPosition, delta,1);
                controller.Submarine.AimPoint = _playerData.PlayerSub.RealPosition;
            }
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return "Investigating unknown contact.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return null;
        }
    }
}
