﻿using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public interface IBotGoal
    {
        int Priority { get; }

        bool IsValid(IStation station);

        bool IsValid(GoalBotController controller);

        void Update(GoalBotController controller, float delta);

        void Update(IStation station, float delta);
        
        string MakeBroadcast();

        bool CanHandle(GoalBotController controller, ISimulationEvent evt);

        void Handle(GoalBotController controller, ISimulationEvent evt);

        string DescribeGoalForHail(GoalBotController controller);
    }
}
