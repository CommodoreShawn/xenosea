﻿using Autofac;
using Godot;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;
using Xenosea.Logic.SimulationEvents;

namespace Xenosea.Logic.AI
{
    public class ReturnHomeMinisubGoal : IBotGoal
    {
        public int Priority => ReturnHomeMinisubGoalType.PRIORITY;

        public bool InDockSlide { get; private set; }
        private float _dockPerc;
        private Vector3 _dockSlideFromTo;

        private IPlayerData _playerData;

        public ReturnHomeMinisubGoal()
        {
            _playerData = IocManager.IocContainer.Resolve<IPlayerData>();
        }

        public bool IsValid(IStation station)
        {
            return false;
        }

        public bool IsValid(GoalBotController controller)
        {
            return controller is MinisubController minisubController
                && (minisubController.Home.ShouldRecall || (controller.Submarine.UsesCharge && controller.Submarine.Charge <= 0.25 * controller.Submarine.MaximumCharge))
                && Object.IsInstanceValid(minisubController.Home);
        }

        public void Update(GoalBotController controller, float delta)
        {
            if (controller is MinisubController minisubController)
            {
                if (!InDockSlide)
                {
                    if(minisubController.Home.Combatant is Submarine parentSub 
                        && parentSub.IsPlayerSub
                        && _playerData.InBattle)
                    {
                        controller.Submarine.DropDecoy();
                    }

                    var distanceToApproach = minisubController.Submarine.GlobalTransform.origin
                        .DistanceTo(minisubController.Home.ApproachPoint.GlobalTransform.origin);

                    if(distanceToApproach < 5)
                    {
                        _dockSlideFromTo = minisubController.Submarine.GlobalTransform.origin - minisubController.Home.GlobalTransform.origin;
                        InDockSlide = true;
                        _dockPerc = 0;
                        minisubController.Submarine.CollisionLayer = 0b00000000000000000000;
                        minisubController.Submarine.Mode = RigidBody.ModeEnum.Static;
                        minisubController.Submarine.ActualThrottle = 0;
                        minisubController.Submarine.Throttle = 0;
                        minisubController.Submarine.Dive = 0;
                        if (minisubController.Home.GetParent() is Submarine)
                        {
                            controller.Submarine.AddCollisionExceptionWith(minisubController.Home.GetParent());
                        }
                    }
                    else if(distanceToApproach < 400)
                    {
                        controller.CarefulAutopilotTo(minisubController.Home.ApproachPoint.GlobalTransform.origin, delta, 1.0f);
                    }
                    else
                    {
                        if (!Object.IsInstanceValid(minisubController.Home.ApproachPoint))
                        {
                            controller.Submarine.QueueFree();
                        }
                        else
                        {
                            controller.AutopilotTo(minisubController.Home.ApproachPoint.GlobalTransform.origin, delta, 1);
                        }
                    }
                }
                else
                {
                    _dockPerc += delta * 0.2f;

                    var pos = minisubController.Home.GlobalTransform.origin + (_dockSlideFromTo * (1 - _dockPerc));

                    controller.Submarine.LookAtFromPosition(
                        pos,
                        pos - minisubController.Home.GlobalTransform.basis.z,
                        controller.Submarine.UpVector);

                    if(_dockPerc >= 1)
                    {
                        minisubController.Home.MinisubDocked();
                    }
                }
            }
        }

        public void Update(IStation station, float delta)
        {
        }

        public string MakeBroadcast()
        {
            return "Returning to hangar.";
        }

        public bool CanHandle(GoalBotController controller, ISimulationEvent evt)
        {
            return false;
        }

        public void Handle(GoalBotController controller, ISimulationEvent evt)
        {
        }

        public string DescribeGoalForHail(GoalBotController controller)
        {
            return "We are returning to hangar.";
        }
    }
}
