﻿namespace Xenosea.Logic
{
    public enum OreType
    {
        Hematite = 0,
        Garnierite = 1,
        Galena = 2,
        Bornite = 3,
        Langbeinite = 4,
        Lazulite = 5,
        Uranite = 6,
        Pyrite = 7
    }
}