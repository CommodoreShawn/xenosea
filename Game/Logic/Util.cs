﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.Infrastructure.Data;

namespace Xenosea.Logic
{
    public class Util
    {
        public static void LinkDatums(
            List<WorldDatum> datums,
            ReportStepProgress reportStepProgress)
        {
            var pathableDatums = datums.Where(d => d.Depth > d.IceDepth).ToArray();

            var datumBuckets = datums
                .GroupBy(d => d.Latitude)
                .OrderBy(g => g.Key)
                .ToArray();

            for(var i = 0; i < datumBuckets.Length; i++)
            {
                var linkOptions = new List<WorldDatum>(datumBuckets[i]);
                if(i > 0)
                {
                    linkOptions.AddRange(datumBuckets[i - 1]);
                }
                if(i < datumBuckets.Length - 1)
                {
                    linkOptions.AddRange(datumBuckets[i + 1]);
                }

                foreach(var datum in datumBuckets[i])
                {
                    var nearbyDatums = linkOptions
                        .Where(d => d != datum)
                        .Where(d => (d.Position - datum.Position).Length() < Constants.DATUM_DISTANCE * 2)
                        .ToArray();

                    datum.Neighbors.AddRange(nearbyDatums);
                }

                reportStepProgress(i / (float)datumBuckets.Length);
            }
        }

        public static Vector3 LatLonToVector(float latitude, float longitude, float worldRadius)
        {

            var unitRadiusAtLat = Mathf.Cos(Mathf.Deg2Rad(latitude));
            var y = 1 * Mathf.Sin(Mathf.Deg2Rad(latitude)) * worldRadius;
            var x = unitRadiusAtLat * Math.Cos(Mathf.Deg2Rad(longitude)) * worldRadius;
            var z = unitRadiusAtLat * Math.Sin(Mathf.Deg2Rad(longitude)) * worldRadius;

            return new Vector3((float)x, (float)y, (float)z);
        }

        public static float BiLerp(float ul, float ur, float ll, float lr, float a, float b)
        {
            var u = Mathf.Lerp(ul, ur, a);
            var l = Mathf.Lerp(ll, lr, a);
            return Mathf.Lerp(u, l, b);
        }

        public static float EstimateTravelDistance(WorldDatum from, WorldDatum to)
        {
            var vectorToFrom = from.Position.Normalized();
            var vectoToTo = to.Position.Normalized();
            var angle = vectorToFrom.AngleTo(vectoToTo);

            var circumference = 2 * Mathf.Pi * Constants.WORLD_RADIUS;

            return circumference * (angle / (2 * Mathf.Pi));
        }

        public static Vector3 GetNavigationCenter(WorldDatum location)
        {
            var depth = (location.Depth - location.IceDepth) * 0.75f + location.IceDepth;

            depth = Mathf.Clamp(depth, 0, Constants.DEPTH_MAX_SUIT);

            return location.Position - depth * location.Position.Normalized();
        }

        public static string DistanceToDisplay(float distance, bool prependPlusForPositive)
        {
            var prepend = string.Empty;
            if(prependPlusForPositive && distance > 0)
            {
                prepend = "+";
            }

            if(Mathf.Abs(distance) < 1000)
            {
                return prepend + distance.ToString("N0") + " m";
            }
            else
            {
                return prepend + (distance / 1000f).ToString("N1") + " km";
            }
        }

        public static int Bucketize(float value)
        {
            return (int)(value * 4);
        }

        public static float Debucket(int bucket)
        {
            return bucket / 4f;
        }

        public static void TreeTraverse(Node current, Action<Node> visit)
        {
            if(current == null)
            {
                return;
            }

            visit(current);

            foreach(var child in current.GetChildren().OfType<Node>())
            {
                TreeTraverse(child, visit);
            }
        }

        //public static double GetHightAtPosition(
        //    string seed,
        //    int noiseScale,
        //    double worldRadius,
        //    double maxDepth,
        //    Vector3 datumPosition)
        //{
        //    var random = new Random(seed.GetHashCode());
        //    random.Next();
        //    var heightNoiseGen = new SimplexNoiseGenerator(random.Next());

        //    var heightNoise = heightNoiseGen.CoherentNoise(
        //        datumPosition.X,
        //        datumPosition.Y,
        //        datumPosition.Z,
        //        octaves: 8,
        //        multiplier: noiseScale,
        //        persistence: 0.5f);

        //    var height = Clamp(heightNoise / 0.4f + 0.5f, 0, 1);

        //    return worldRadius - (0.3f * Math.Sqrt(height) + 0.7f * height) * maxDepth;
        //}
    }
}
