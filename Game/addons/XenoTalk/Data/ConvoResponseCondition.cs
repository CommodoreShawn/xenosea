﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Xenosea.addons.XenoTalk.Data
{
    public class ConvoResponseCondition
    {
        public string Type { get; set; }

        public Dictionary<string, string> Data { get; set; }

        public ConvoResponseCondition()
        {
            Data = new Dictionary<string, string>();
        }

        public ConvoResponseCondition(XElement conditionElement)
        {
            Type = XmlUtil.GetAttribute(conditionElement, "type");

            Data = conditionElement
                .Attributes()
                .Where(x => x.Name != "type")
                .ToDictionary(x => x.Name.ToString(), x => x.Value);
        }
    }
}