﻿using System.Linq;
using System.Xml.Linq;

namespace Xenosea.addons.XenoTalk.Data
{
    public class ConvoResponse
    {
        public string Text { get; set; }

        public ConvoResponseOption[] Options { get; set; }

        public ConvoResponseCondition[] Conditions { get; set; }

        public ConvoResponseAction[] Actions { get; set; }

        public ConvoResponse()
        {
        }

        public ConvoResponse(XElement responseElement)
        {
            Text = XmlUtil.GetChildValue(responseElement, "text");

            Options = responseElement
                .Elements("option")
                .Select(x => new ConvoResponseOption(x))
                .ToArray();

            Conditions = responseElement
                .Elements("condition")
                .Select(x => new ConvoResponseCondition(x))
                .ToArray();

            Actions = responseElement
                .Elements("action")
                .Select(x => new ConvoResponseAction(x))
                .ToArray();
        }
    }
}