﻿using System.Xml.Linq;

namespace Xenosea.addons.XenoTalk.Data
{
    public class ConvoResponseOption : ConvoResponse
    {
        public string OptionText { get; set; }

        public ConvoResponseOption()
            :base()
        {
        }

        public ConvoResponseOption(XElement optionElement)
            :base(optionElement)
        {
            OptionText = XmlUtil.GetAttribute(optionElement, "text");
        }
    }
}