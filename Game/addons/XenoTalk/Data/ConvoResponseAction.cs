﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Xenosea.addons.XenoTalk.Data
{
    public class ConvoResponseAction
    {
        public string Type { get; set; }

        public Dictionary<string, string> Data { get; set; }

        public ConvoResponseAction()
        {
            Data = new Dictionary<string, string>();
        }

        public ConvoResponseAction(XElement actionElement)
        {
            Type = XmlUtil.GetAttribute(actionElement, "type");

            Data = actionElement
                .Attributes()
                .Where(x => x.Name != "type")
                .ToDictionary(x => x.Name.ToString(), x => x.Value);
        }
    }
}