﻿using System.Linq;
using System.Xml.Linq;

namespace Xenosea.addons.XenoTalk.Data
{
    public class ConvoTopic
    {
        public string Name { get; set; }

        public ConvoResponse[] Responses { get; set; }

        public ConvoTopic()
        {
        }

        public ConvoTopic(XElement topicRoot)
        {
            Name = XmlUtil.GetAttribute(topicRoot, "name");

            Responses = topicRoot.Elements("response")
                .Select(x => new ConvoResponse(x))
                .ToArray();
        }
    }
}
