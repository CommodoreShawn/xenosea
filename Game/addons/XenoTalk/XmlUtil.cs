﻿using System;
using System.Xml.Linq;

namespace Xenosea.addons.XenoTalk
{
    public static class XmlUtil
    {
        public static string GetAttribute(XElement element, string name)
        {
            var attribute = element.Attribute(name);

            if(attribute == null)
            {
                throw new Exception($"Expected element {element} to have attribute {attribute}, but it did not: {element.BaseUri}");
            }

            return attribute.Value;
        }

        public static string GetChildValue(XElement element, string name)
        {
            var child = element.Element(name);

            if (child == null)
            {
                throw new Exception($"Expected element {element} to have child {child}, but it did not: {element.BaseUri}");
            }

            return child.Value;
        }
    }
}
