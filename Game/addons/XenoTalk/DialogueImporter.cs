﻿using Godot;
using Godot.Collections;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Xenosea.addons.XenoTalk.Data;
using Xenosea.Logic;

namespace Xenosea.addons.XenoTalk
{
#if TOOLS
    [Tool]
    public class DialogueImporter : EditorImportPlugin
    {
        public override string GetImporterName()
        {
            return "XenoTalk.DialogueImporter";
        }

        public override string GetVisibleName()
        {
            return "XenoTalk Dialogue Importer";
        }

        public override Array GetRecognizedExtensions()
        {
            return new Array("xml".Yield());
        }

        public override string GetSaveExtension()
        {
            return "res";
        }

        public override string GetResourceType()
        {
            return "PackedDataContainer";
        }

        public override int GetPresetCount()
        {
            return 1;
        }

        public override string GetPresetName(int preset)
        {
            return "Default";
        }

        public override Array GetImportOptions(int preset)
        {
            return new Array();
        }

        public override bool GetOptionVisibility(string option, Dictionary options)
        {
            return true;
        }

        public override int Import(
            string sourceFile, 
            string savePath, 
            Dictionary options, 
            Array platformVariants, 
            Array genFiles)
        {
            try
            {
                var topics = DialogueParser.Parse(sourceFile);

                var container = new PackedDataContainer();
                container.__Data___ = JsonConvert.SerializeObject(topics).ToUTF8();

                //GD.Print(JsonConvert.SerializeObject(topics));
                GD.Print($"{savePath}.{GetSaveExtension()}");

                var res = ResourceSaver.Save($"{savePath}.{GetSaveExtension()}", container);

                //GD.Print(res);
                return (int)res;
            }
            catch (System.Exception ex)
            {
                GD.PushError($"{ex.GetType().Name}: {ex.Message}\n{ex.StackTrace}");
            }

            return (int)Error.ParseError;
        }
    }

#endif
}
