#if TOOLS
using Godot;
using Xenosea.addons.XenoTalk;

[Tool]
public class XenoTalk : EditorPlugin
{
    private DialogueImporter _dialogueImporter;

    public XenoTalk()
    {
        _dialogueImporter = new DialogueImporter();
    }

    public override void _EnterTree()
    {
        AddImportPlugin(_dialogueImporter);
    }

    public override void _ExitTree()
    {
        RemoveImportPlugin(_dialogueImporter);
        _dialogueImporter = null;
    }
}
#endif