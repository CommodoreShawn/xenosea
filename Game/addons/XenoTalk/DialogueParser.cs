﻿using Godot;
using System.Collections.Generic;
using System.Xml.Linq;
using Xenosea.addons.XenoTalk.Data;

namespace Xenosea.addons.XenoTalk
{
    public static class DialogueParser
    {
        public static List<ConvoTopic> Parse(string filePath)
        {
            var topics = new List<ConvoTopic>();

            var file = new Godot.File();
            file.Open(filePath, File.ModeFlags.Read);

            //GD.Print(sourceFile);
            var document = XDocument.Parse(file.GetAsText());
            file.Close();

            foreach (var topicRoot in document.Root.Elements("topic"))
            {
                topics.Add(new ConvoTopic(topicRoot));
            }

            return topics;
        }
    }
}
