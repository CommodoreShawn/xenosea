﻿//using Xenosea.Logic.AI;

//public class DockPatrolWaypoint : IPatrolWaypoint
//{
//    private PatrolSpawningStation _dockTarget;
//    private bool _wasInSequence;

//    public DockPatrolWaypoint(
//        PatrolSpawningStation dockTarget)
//    {
//        _dockTarget = dockTarget;
//    }

//    public bool IsSatisfied(AiPatrol aiPatrol)
//    {
//        if(_wasInSequence && aiPatrol.Ships.Count < 1)
//        {
//            return true;
//        }

//        return false;
//    }

//    public void UpdateOrders(AiPatrol aiPatrol)
//    {
//        if(aiPatrol.Ships.Count < 1)
//        {
//            return;
//        }

//        if (aiPatrol.Ships[0].DockingSequence == null)
//        {
//            aiPatrol.FlightMode = FlightMode.Dock;
//            aiPatrol.FlightModeTarget = _dockTarget;
//        }

//        var isInSequence = aiPatrol.Ships[0].DockingSequence != null
//            && aiPatrol.Ships[0].DockingSequence is StationDockingSequence sds
//            && sds.Station == _dockTarget;

//        if(isInSequence && !_wasInSequence)
//        {
//            aiPatrol.DockingAtStation(_dockTarget);
//        }

//        _wasInSequence = isInSequence;
//    }
//}