using Godot;
using System.Collections.Generic;

[Tool]
public class PatrolPath : PatrolPoint
{
    [Export]
    public bool Border { get; set; }
    [Export]
    public bool Travel { get; set; }
    [Export]
    public bool Perimiter { get; set; }
    [Export]
    public bool Lawful { get; set; }
    [Export]
    public bool TradeRoute { get; set; }
    [Export]
    public float Difficulty { get; set; }

    //[Export]
    //public SpawnSpecification[] PatrolMissionSpawnOptions;
    [Export]
    public int PatrolMissionSpawnMin;
    [Export]
    public int PatrolMissionSpawnMax;

    public IEnumerable<Spatial> Waypoints { get; private set; }

    public override void _Ready()
    {
        base._Ready();

        var waypoints = new List<Spatial>();
        var children = GetChildren();

        for (var i = 0; i < children.Count; i++)
        {
            if (children[i] is Spatial spatial)
            {
                waypoints.Add(spatial);
            }
        }

        Waypoints = waypoints;
    }
}
