﻿using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Services;
using Xenosea.Resources;

[Tool]
public class StationBuilderPreview : Spatial
{
    [Export]
    public float Latitude;
    [Export]
    public float Longitude;
    [Export]
    public int ModuleCount;
    [Export]
    public float GridSpacing;
    [Export]
    public StationShapeProfile Shape;
    [Export]
    public StationLayoutType LayoutType;
    [Export]
    public PackedScene SectionPrefab;
    [Export]
    public PackedScene CapPrefab;
    [Export]
    public float SectionHeight;
    [Export]
    public float GroundSlopeTolerance;
    [Export]
    public PackedScene BridgePrefab;
    [Export]
    public float BridgeLength;
    [Export]
    public float TradeSourceLatitude;
    [Export]
    public float TradeSourceLongitude;
    [Export]
    public PackedScene DockingTowerPrefab;
    [Export]
    public int DockingTowers;
    [Export]
    public PackedScene DefenseTowerPrefab;
    [Export]
    public int DefenseTowerCount;
    [Export]
    public float AttackSourceLatitude;
    [Export]
    public float AttackSourceLongitude;
    [Export]
    public float DefenseTowerDistance;
    [Export]
    public int FarmCount;
    [Export]
    public float FarmPlotSize;
    [Export]
    public float PlantSpacing;
    [Export]
    public PackedScene FarmPlotPrefab;

    public override void _Ready()
    {
        base._Ready();
        Visible = Engine.EditorHint;
    }

    public void RebuildStation()
    {
        GD.Print("Rebuilding station....");
        if (!Engine.EditorHint)
        {
            GD.PrintErr("Should not be run from outside the editor!");
        }
        else
        {
            this.ClearChildren();

            var floor = GetParent().GetNodeOrNull<StationTerrainPreview>("StationTerrainPreview");

            if (floor != null)
            {
                floor.Latitude = Latitude;
                floor.Longitude = Longitude;
                floor.RebuildTerrain();
            }

            var worldGenerator = new WorldGenerator(null);
            var stationBuilder = new StationGenerator(
                worldGenerator,
                null);

            var buildout = stationBuilder.GenerateTestStation(
                Latitude, 
                Longitude, 
                ModuleCount, 
                Shape,
                LayoutType,
                GridSpacing,
                GroundSlopeTolerance,
                SectionHeight,
                TradeSourceLatitude,
                TradeSourceLongitude,
                DockingTowers,
                DefenseTowerCount,
                AttackSourceLatitude,
                AttackSourceLongitude,
                DefenseTowerDistance,
                FarmCount,
                FarmPlotSize,
                PlantSpacing);

            var worldPos = Util.LatLonToVector(Latitude, Longitude, Constants.WORLD_RADIUS);


            var stationCenterPosition = new Spatial
            {
                Name = "StationCenter",
                Transform = Transform.Identity.Translated(buildout.CenterPosition - worldPos)
            };
            AddChild(stationCenterPosition);
            stationCenterPosition.Owner = GetTree().EditedSceneRoot;

            foreach (var tower in buildout.Towers)
            {
                var foundation = new Spatial()
                {
                    Name = "Tower"
                };
                foundation.Transform = Transform.Identity
                    .Translated(tower.FoundationPosition - worldPos)
                    .LookingAt(worldPos, Vector3.Up);
                AddChild(foundation);
                foundation.Owner = GetTree().EditedSceneRoot;

                GD.Print("Tower: " + tower.Sections.Length);
                for (var i = 0; i < tower.Sections.Length + 1; i++)
                {
                    Spatial section;
                    if(i < tower.Sections.Length)
                    {
                        section = SectionPrefab.Instance() as Spatial;
                    }
                    else
                    {
                        section = CapPrefab.Instance() as Spatial;
                    }

                    foundation.AddChild(section);
                    section.Transform = Transform.Identity
                        .LookingAt(Vector3.Up, Vector3.Forward)
                        .Translated(Vector3.Up * i * SectionHeight);
                    section.Owner = GetTree().EditedSceneRoot;
                }

                foreach(var bridge in tower.Bridges)
                {
                    var bridgeSegmentCount = Mathf.Max(bridge.From.DistanceTo(bridge.To) / BridgeLength, 1);

                    var bridgeNode = new Spatial
                    {
                        Name = "Bridge"
                    };
                    foundation.AddChild(bridgeNode);
                    bridgeNode.LookAtFromPosition(
                        bridge.From - worldPos,
                        bridge.To - worldPos,
                        worldPos.Normalized());

                    
                    bridgeNode.Owner = GetTree().EditedSceneRoot;

                    for (var i = 0; i < bridgeSegmentCount; i++)
                    {
                        var bridgeSegment = BridgePrefab.Instance() as Spatial;

                        bridgeNode.AddChild(bridgeSegment);
                        bridgeSegment.Owner = GetTree().EditedSceneRoot;
                        bridgeSegment.Transform = Transform.Identity
                            .Translated(Vector3.Forward * i * BridgeLength);
                    }
                }
            }

            foreach(var dockingTower in buildout.DockingTowers)
            {
                var tower = DockingTowerPrefab.Instance() as Spatial;

                var lookingAt = dockingTower.AttachedTowerPosition.Normalized() * dockingTower.FoundationPosition.Length();

                tower.Transform = Transform.Identity
                    .Translated(dockingTower.FoundationPosition - worldPos)
                    .LookingAt(lookingAt - worldPos, worldPos);
                AddChild(tower);
                tower.Owner = GetTree().EditedSceneRoot;
            }

            foreach (var defenseTowerBuildout in buildout.DefenseTowers)
            {
                var tower = DefenseTowerPrefab.Instance() as Spatial;

                var lookingAt = buildout.CenterPosition.Normalized() * defenseTowerBuildout.FoundationPosition.Length();

                tower.Transform = Transform.Identity
                    .Translated(defenseTowerBuildout.FoundationPosition - worldPos)
                    .LookingAt(lookingAt - worldPos, worldPos);
                AddChild(tower);
                tower.Owner = GetTree().EditedSceneRoot;
            }

            foreach (var farmBuildout in buildout.FarmPlots)
            {
                var farm = FarmPlotPrefab.Instance() as Spatial;
                farm.Transform = Transform.Identity
                    .Translated(farmBuildout.CenterPosition - worldPos)
                    .LookingAt(worldPos, Vector3.Up);
                AddChild(farm);
                farm.Owner = GetTree().EditedSceneRoot;
            }

            GD.Print("Done");
        }
    }
}
