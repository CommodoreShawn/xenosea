﻿#if TOOLS
using Godot;

[Tool]
public class FieldGeneratorInspector : EditorInspectorPlugin
{
    public override bool CanHandle(Object @object)
    {
        return @object is FieldGenerator;
    }

    public override void ParseBegin(Object @object)
    {
        base.ParseBegin(@object);

        var newButton = new Button
        {
            Text = "Rebuild Crops"
        };

        newButton.Connect("pressed", @object, nameof(FieldGenerator.RebuildCrops));

        AddCustomControl(newButton);
    }
}
#endif