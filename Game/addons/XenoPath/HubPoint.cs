using Autofac;
using Godot;
using Godot.Collections;
using System.Collections.Generic;
using Xenosea.Logic.Infrastructure;
using Xenosea.Logic.Infrastructure.Interfaces;

[Tool]
public class HubPoint : PatrolPoint
{
    [Export]
    public float Radius { get; set; } = 3000;

    [Export]
    public Faction Faction;

    public List<IStation> LocalStations { get; private set; }

    public IEnumerable<IAiPath> AiPaths { get; private set; } = new IAiPath[0];
    private bool _aiPathsFound;

    public override void _Ready()
    {
        base._Ready();
        LocalStations = new List<IStation>();

        if (!Engine.EditorHint)
        {
            IocManager.IocContainer.Resolve<IWorldManager>().AddHubPoint(this);
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(!_aiPathsFound && !Engine.EditorHint)
        {
            _aiPathsFound = true;
            FindAiPaths();
            
        }
    }

    public void FindAiPaths()
    {
        _aiPathsFound = true;
        var siblings = GetParent().GetChildren();
        var aiPaths = new List<IAiPath>();
        AiPaths = aiPaths;

        for (var i = 0; i < siblings.Count; i++)
        {
            if (siblings[i] is PatrolPath patrolPath && patrolPath.GetChildCount() > 0)
            {
                var start = patrolPath.GetChild(0) as Spatial;
                var end = patrolPath.GetChild(patrolPath.GetChildCount() - 1) as Spatial;

                if (start.GlobalTransform.origin.DistanceTo(GlobalTransform.origin) <= Radius)
                {
                    var dstHub = FindLocalStationHub(siblings, end);

                    if (dstHub != null)
                    {
                        aiPaths.Add(new PatrolAiPath(patrolPath, dstHub, false));
                    }
                }
                else if (end.GlobalTransform.origin.DistanceTo(GlobalTransform.origin) <= Radius)
                {
                    var srcHub = FindLocalStationHub(siblings, start);

                    if (srcHub != null)
                    {
                        aiPaths.Add(new PatrolAiPath(patrolPath, srcHub, true));
                    }
                }
            }
            else if (siblings[i] is IStation station)
            {
                if (station.RealPosition.DistanceTo(GlobalTransform.origin) <= Radius)
                {
                    LocalStations.Add(station);
                }
            }
        }
    }

    public static HubPoint FindLocalStationHub(Array siblings, Spatial location)
    {
        for (var i = 0; i < siblings.Count; i++)
        {
            if (siblings[i] is HubPoint stationHub)
            {
                var dst = stationHub.GlobalTransform.origin.DistanceTo(location.GlobalTransform.origin);

                if (dst <= stationHub.Radius)
                {
                    return stationHub;
                }
            }
        }

        return null;
    }
}
