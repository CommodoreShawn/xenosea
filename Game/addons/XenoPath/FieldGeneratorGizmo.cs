#if TOOLS
using Godot;

[Tool]
public class FieldGeneratorGizmo : EditorSpatialGizmoPlugin
{
    public FieldGeneratorGizmo()
    {
        CreateMaterial("line", new Color(1, 0, 0));
    }

    public override string GetName()
    {
        return "FieldGeneratorGizmo";
    }

    public override bool HasGizmo(Spatial spatial)
    {
        return spatial is FieldGenerator;
    }

    public override void Redraw(EditorSpatialGizmo gizmo)
    {
        base.Redraw(gizmo);

        gizmo.Clear();

        var fieldGenerator = gizmo.GetSpatialNode() as FieldGenerator;

        if (fieldGenerator != null)
        {
            gizmo.AddLines(
                    new Vector3[]
                    {
                        new Vector3(-fieldGenerator.Width / 2, 0, -fieldGenerator.Length / 2),
                        new Vector3(fieldGenerator.Width / 2, 0, -fieldGenerator.Length / 2),

                        new Vector3(fieldGenerator.Width / 2, 0, -fieldGenerator.Length / 2),
                        new Vector3(fieldGenerator.Width / 2, 0, fieldGenerator.Length / 2),

                        new Vector3(fieldGenerator.Width / 2, 0, fieldGenerator.Length / 2),
                        new Vector3(-fieldGenerator.Width / 2, 0, fieldGenerator.Length / 2),

                        new Vector3(-fieldGenerator.Width / 2, 0, fieldGenerator.Length / 2),
                        new Vector3(-fieldGenerator.Width / 2, 0, -fieldGenerator.Length / 2),
                    },
                    GetMaterial("line", gizmo));
        }
    }
}
#endif

