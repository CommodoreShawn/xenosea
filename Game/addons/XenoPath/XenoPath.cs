#if TOOLS
using Godot;

[Tool]
public class XenoPath : EditorPlugin
{
    private PatrolPointGizmo _patrolPointGizmo;
    private DockingPointGizmo _dockingPointGizmo;
    private FieldGeneratorGizmo _fieldGeneratorGizmo;
    private StationTerrainPreviewInspector _stationTerrainPreviewInspector;
    private StationBuilderPreviewInspector _stationBuilderPreviewInspector;
    private FieldGeneratorInspector _fieldGeneratorInspector;

    public XenoPath()
    {
        _patrolPointGizmo = new PatrolPointGizmo();
        _dockingPointGizmo = new DockingPointGizmo();
        _fieldGeneratorGizmo = new FieldGeneratorGizmo();
        _stationTerrainPreviewInspector = new StationTerrainPreviewInspector();
        _stationBuilderPreviewInspector = new StationBuilderPreviewInspector();
        _fieldGeneratorInspector = new FieldGeneratorInspector();
    }

    public override void _EnterTree()
    {
        AddSpatialGizmoPlugin(_patrolPointGizmo);
        AddSpatialGizmoPlugin(_dockingPointGizmo);
        AddSpatialGizmoPlugin(_fieldGeneratorGizmo);

        AddCustomType("PatrolPoint", "Spatial",
            GD.Load<Script>("res://addons/XenoPath/PatrolPoint.cs"),
            GD.Load<Texture>("res://addons/XenoPath/PatrolPointIcon.png"));

        AddCustomType("HubPoint", "Spatial",
            GD.Load<Script>("res://addons/XenoPath/HubPoint.cs"),
            GD.Load<Texture>("res://addons/XenoPath/PatrolPointIcon.png"));

        AddCustomType("PatrolPath", "Spatial",
            GD.Load<Script>("res://addons/XenoPath/PatrolPath.cs"),
            GD.Load<Texture>("res://addons/XenoPath/PatrolPointIcon.png"));

        AddCustomType("DockingPoint", "Spatial",
            GD.Load<Script>("res://addons/XenoPath/DockingPoint.cs"),
            GD.Load<Texture>("res://addons/XenoPath/DockingPointIcon.png"));

        AddCustomType("StationTerrainPreview", "Spatial",
            GD.Load<Script>("res://addons/XenoPath/StationTerrainPreview.cs"),
            GD.Load<Texture>("res://addons/XenoPath/StationTerrainPreviewIcon.png"));

        AddCustomType("StationBuilderPreview", "Spatial",
            GD.Load<Script>("res://addons/XenoPath/StationBuilderPreview.cs"),
            GD.Load<Texture>("res://addons/XenoPath/StationBuilderPreviewIcon.png"));

        AddCustomType("FieldGenerator", "Spatial",
            GD.Load<Script>("res://addons/XenoPath/FieldGenerator.cs"),
            GD.Load<Texture>("res://addons/XenoPath/FieldGeneratorIcon.png"));

        AddInspectorPlugin(_stationTerrainPreviewInspector);
        AddInspectorPlugin(_stationBuilderPreviewInspector);
        AddInspectorPlugin(_fieldGeneratorInspector);
    }

    public override void _ExitTree()
    {
        RemoveSpatialGizmoPlugin(_patrolPointGizmo);
        RemoveSpatialGizmoPlugin(_dockingPointGizmo);
        RemoveSpatialGizmoPlugin(_fieldGeneratorGizmo);
        RemoveCustomType("PatrolPoint");
        RemoveCustomType("HubPoint");
        RemoveCustomType("PatrolPath");
        RemoveCustomType("DockingPoint");
        RemoveCustomType("StationTerrainPreview");
        RemoveCustomType("FieldGenerator");
        RemoveInspectorPlugin(_stationTerrainPreviewInspector);
        RemoveInspectorPlugin(_fieldGeneratorInspector);
    }
}
#endif