﻿#if TOOLS
using Godot;

[Tool]
public class StationTerrainPreviewInspector : EditorInspectorPlugin
{
    public override bool CanHandle(Object @object)
    {
        return @object is StationTerrainPreview;
    }

    public override void ParseBegin(Object @object)
    {
        base.ParseBegin(@object);

        var newButton = new Button
        {
            Text = "Rebuild Terrain"
        };

        newButton.Connect("pressed", @object, nameof(StationTerrainPreview.RebuildTerrain));

        AddCustomControl(newButton);
    }
}
#endif