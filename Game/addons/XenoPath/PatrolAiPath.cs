﻿using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic.AI;

public class PatrolAiPath : IAiPath
{
    public string Name => _internalPath.Name;

    public HubPoint DestinationHub { get; private set; }
    public float DistanceWeight { get; }
    public bool Border => _internalPath.Border;
    public bool Travel => _internalPath.Travel;
    public bool Perimiter => _internalPath.Perimiter;
    public bool Lawful => _internalPath.Lawful;
    public bool TradeRoute => _internalPath.TradeRoute;
    public float Difficulty => _internalPath.Difficulty;
    //public SpawnSpecification[] PatrolMissionSpawnOptions => _internalPath.PatrolMissionSpawnOptions;
    public int PatrolMissionSpawnMin => _internalPath.PatrolMissionSpawnMin;
    public int PatrolMissionSpawnMax => _internalPath.PatrolMissionSpawnMax;

    private PatrolPath _internalPath;
    public bool IsReversePath { get; private set; }

    public PatrolAiPath(
        PatrolPath patrolPath,
        HubPoint destinationHub,
        bool reversePath)
    {
        _internalPath = patrolPath;
        DestinationHub = destinationHub;
        IsReversePath = reversePath;
    }

    public Queue<IPatrolWaypoint> GetWaypoints()
    {
        if (IsReversePath)
        {
            return new Queue<IPatrolWaypoint>(
                _internalPath.Waypoints
                .Reverse()
                .Select(s => new GotoPatrolWaypoint(s)));
        }
        else
        {
            return new Queue<IPatrolWaypoint>(
                _internalPath.Waypoints
                .Select(s => new GotoPatrolWaypoint(s)));
        }
    }
}