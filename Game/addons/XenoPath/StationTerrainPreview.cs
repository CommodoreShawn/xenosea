﻿
using Godot;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Services;

[Tool]
public class StationTerrainPreview : Spatial
{
    [Export]
    public float Latitude;
    [Export]
    public float Longitude;
    [Export]
    public Material FloorMaterial;
    [Export]
    public Material RoofMaterial;
    
    private MeshInstance _floorMesh;
    private MeshInstance _roofMesh;
    private CollisionShape _floorCollider;

    public override void _Ready()
    {
        base._Ready();
        Visible = Engine.EditorHint;
    }

    public void RebuildTerrain()
    {
        _floorMesh = GetNode<MeshInstance>("FloorMesh");
        _roofMesh = GetNode<MeshInstance>("RoofMesh");
        _floorCollider = GetNode<CollisionShape>("Area/FloorCollider");

        GD.Print("Rebuilding terrain....");
        if (!Engine.EditorHint)
        {
            GD.PrintErr("Should not be run from outside the editor!");
        }
        else
        {
            var worldGenerator = new WorldGenerator(null);

            var data = worldGenerator.ForceGeometryRebuild(
                Latitude, 
                Longitude, 
                Constants.TERRAIN_VERTEX_COUNT,
                true);

            var floorMesh = new ArrayMesh();
            floorMesh.AddSurfaceFromArrays(Mesh.PrimitiveType.Triangles, data.FloorSurface);
            var roofMesh = new ArrayMesh();
            roofMesh.AddSurfaceFromArrays(Mesh.PrimitiveType.Triangles, data.RoofSurface);

            _floorMesh.Mesh = floorMesh;
            _floorMesh.MaterialOverride = FloorMaterial;
            _floorCollider.Shape = floorMesh.CreateTrimeshShape();

            _roofMesh.Mesh = roofMesh;
            _roofMesh.MaterialOverride = RoofMaterial;


            //var worldPos = Util.LatLonToVector(Latitude, Longitude, Constants.WORLD_RADIUS);
            //var invert = Transform.Identity.LookingAt(worldPos, Vector3.Up).Inverse();
            //Transform = invert;
            
            GD.Print("Done");
        }
    }
}
