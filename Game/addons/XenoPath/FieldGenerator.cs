﻿
using Godot;
using System.Collections.Generic;
using System.Linq;
using Xenosea.Logic;
using Xenosea.Logic.Infrastructure.Services;

[Tool]
public class FieldGenerator : Spatial
{
    [Export]
    public PackedScene[] CropPrefabs;

    [Export]
    public PackedScene LightPrefab;

    [Export]
    public float Width = 100;

    [Export]
    public float Length = 100;

    [Export]
    public float CropSpacing = 5;

    [Export]
    public float LightSpacing = 50;

    [Export]
    public float CropChance = 0.5f;

    [Export]
    public PackedScene FarmerDiverPrefab;

    private bool _spawnedFarmers;

    public Spatial[] CropPoints { get; private set; }

    public override void _Ready()
    {
        base._Ready();

        if(!Engine.EditorHint)
        {
            CropPoints = GetChildren()
                .OfType<Spatial>()
                .Where(s => s.Name.StartsWith("Crop"))
                .ToArray();
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (!Engine.EditorHint
            && !_spawnedFarmers
            && CropPoints.Length > 0)
        {
            
            _spawnedFarmers = true;

            var farmerDiverTargetCount = Mathf.Max(1, CropPoints.Length / 25);

            var random = new System.Random();

            for (var i = 0; i  < farmerDiverTargetCount; i++)
            {
                //var farmer = FarmerDiverPrefab.Instance() as FarmerDiver;
                //farmer.Field = this;
                //this.FindParent<GameplayRoot>().AddChild(farmer);
                //farmer.GlobalTransform = GlobalTransform;
            }
        }
    }

    public void RebuildCrops()
    {
        this.ClearChildren();
        var random = new System.Random();

        PhysicsServer.SetActive(true);

        if(CropPrefabs.Length < 1)
        {
            return;
        }

        for(var i = 0f; i <= Width; i += 1)
        {
            for(var j = 0f; j <= Length; j += 1)
            {
                var placeLight = i % LightSpacing == 0 && j % LightSpacing == 0;
                var placeCrop = i % CropSpacing == 0 && j % CropSpacing == 0;

                if(placeLight)
                {
                    var lightPos = GlobalTransform.Xform(
                        new Vector3(
                            i - Width / 2,
                            0,
                            j - Length / 2));

                    var rayPoint = GlobalTransform.Xform(
                        new Vector3(
                            i - Width / 2,
                            -2000,
                            j - Length / 2));

                    var collisionInfo = GetWorld().DirectSpaceState
                        .IntersectRay(
                            lightPos,
                            rayPoint,
                            new Godot.Collections.Array(),
                            collideWithAreas: true);

                    if (collisionInfo.Count > 0)
                    {
                        var light = LightPrefab.Instance() as Spatial;

                        AddChild(light);
                        light.GlobalTransform = Transform.Identity.Translated((Vector3)collisionInfo["position"]);
                        light.Owner = GetTree().EditedSceneRoot;
                    }
                }
                else if (placeCrop && random.NextDouble() <= CropChance)
                {
                    var cropPos = GlobalTransform.Xform(
                        new Vector3(
                            i - Width / 2,
                            0,
                            j - Length / 2));

                    var rayPoint = GlobalTransform.Xform(
                        new Vector3(
                            i - Width / 2,
                            -2000,
                            j - Length / 2));

                    var collisionInfo = GetWorld().DirectSpaceState
                        .IntersectRay(
                            cropPos,
                            rayPoint,
                            new Godot.Collections.Array(),
                            collideWithAreas: true);

                    if (collisionInfo.Count > 0)
                    {
                        var crop = CropPrefabs[random.Next(CropPrefabs.Length)].Instance() as Spatial;

                        AddChild(crop);
                        crop.GlobalTransform = Transform.Identity.Translated((Vector3)collisionInfo["position"]);
                        crop.Owner = GetTree().EditedSceneRoot;
                    }
                }
            }
        }
    }
}
