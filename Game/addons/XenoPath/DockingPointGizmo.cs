#if TOOLS
using Godot;

[Tool]
public class DockingPointGizmo : EditorSpatialGizmoPlugin
{
    public DockingPointGizmo()
    {
        CreateMaterial("main", new Color(1, 0, 0));
    }

    public override string GetName()
    {
        return "DockingPointGizmo";
    }

    public override bool HasGizmo(Spatial spatial)
    {
        return spatial is DockingPoint;
    }

    public override void Redraw(EditorSpatialGizmo gizmo)
    {
        base.Redraw(gizmo);

        gizmo.Clear();

        var dockingPoint = gizmo.GetSpatialNode() as DockingPoint;
        var approachPoint = dockingPoint?.GetNodeOrNull<Spatial>("ApproachPoint");
        var highApproachPoint = dockingPoint?.GetNodeOrNull<Spatial>("HighApproachPoint");

        if (approachPoint != null)
        {
            gizmo.AddLines(
                new Vector3[] { new Vector3(0, 0, 0), approachPoint.Transform.origin },
                GetMaterial("main", gizmo));

            if (highApproachPoint != null)
            {
                gizmo.AddLines(
                    new Vector3[] { approachPoint.Transform.origin, highApproachPoint.Transform.origin },
                    GetMaterial("main", gizmo));
            }
        }
    }
}
#endif

