#if TOOLS
using Godot;

[Tool]
public class PatrolPointGizmo : EditorSpatialGizmoPlugin
{
    public PatrolPointGizmo()
    {
        CreateMaterial("hub", new Color(0, 0, 1));
        CreateMaterial("path", new Color(0, 1, 0));
    }

    public override string GetName()
    {
        return "PatrolPointGizmo";
    }

    public override bool HasGizmo(Spatial spatial)
    {
        return spatial is PatrolPoint;
    }

    public override void Redraw(EditorSpatialGizmo gizmo)
    {
        base.Redraw(gizmo);

        gizmo.Clear();

        var patrolPoint = gizmo.GetSpatialNode() as PatrolPoint;

        if (patrolPoint is HubPoint stationHub)
        {
            var radPer = Mathf.Pi / 8;
            for (var i = 0; i < 17; i++)
            {
                gizmo.AddLines(
                    new Vector3[]
                    {
                        new Vector3(
                            stationHub.Radius * Mathf.Cos(radPer * i),
                            0,
                            stationHub.Radius * Mathf.Sin(radPer * i)),
                        new Vector3(
                            stationHub.Radius * Mathf.Cos(radPer * (i + 1)),
                            0,
                            stationHub.Radius * Mathf.Sin(radPer * (i + 1)))
                    },
                    GetMaterial("hub", gizmo));
            }
        }

        if(patrolPoint is PatrolPath patrolPath)
        {
            var children = patrolPath.GetChildren();

            for(var i = 1; i < children.Count; i++)
            {
                gizmo.AddLines(
                    new Vector3[]
                    {
                        (children[i - 1] as Spatial).Transform.origin,
                        (children[i] as Spatial).Transform.origin,
                    },
                    GetMaterial("path", gizmo));
            }
        }
    }
}
#endif

