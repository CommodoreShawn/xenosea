﻿#if TOOLS
using Godot;

[Tool]
public class StationBuilderPreviewInspector : EditorInspectorPlugin
{
    public override bool CanHandle(Object @object)
    {
        return @object is StationBuilderPreview;
    }

    public override void ParseBegin(Object @object)
    {
        base.ParseBegin(@object);

        var newButton = new Button
        {
            Text = "Rebuild Station"
        };

        newButton.Connect("pressed", @object, nameof(StationBuilderPreview.RebuildStation));

        AddCustomControl(newButton);
    }
}
#endif