using Godot;
using System.Collections.Generic;
using Xenosea.Logic.Docking;

[Tool]
public class DockingPoint : Spatial
{
    [Export]
    public bool LeftSideDocking;

    [Export]
    public int MinimumSize { get; set; } = 0;

    [Export]
    public int MaximumSize { get; set; } = 5;

    public Spatial ApproachPoint { get; private set; }
    public Spatial HighApproachPoint { get; private set; }

    public DockingSequence DockingSequence { get; set; }

    public float DiverLaunchCooldown { get; set; }
    public List<StationRepairDiver> RepairDivers { get; set; }

    public override void _Ready()
    {
        base._Ready();

        ApproachPoint = GetNode<Spatial>("ApproachPoint");
        HighApproachPoint = GetNode<Spatial>("HighApproachPoint");
        RepairDivers = new List<StationRepairDiver>();
    }
}
