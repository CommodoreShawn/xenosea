﻿using System.Collections.Generic;
using Xenosea.Logic.AI;

public interface IAiPath
{
    bool Border { get; }
    bool Travel { get; }
    bool Perimiter { get; }
    bool Lawful { get; }
    bool TradeRoute { get; }
    string Name { get; }

    Queue<IPatrolWaypoint> GetWaypoints();

    HubPoint DestinationHub { get; }

    float DistanceWeight { get; }
}
