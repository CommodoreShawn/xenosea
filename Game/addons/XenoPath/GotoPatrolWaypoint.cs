﻿using Godot;
using Xenosea.Logic.AI;

public class GotoPatrolWaypoint : IPatrolWaypoint
{
    private Spatial _spatial;
    public Vector3 Position => _spatial.GlobalTransform.origin;

    public GotoPatrolWaypoint(Spatial spatial)
    {
        _spatial = spatial;
    }

    public bool IsSatisfied(GoalBotController controller)
    {
        if(!controller.Submarine.IsWrecked)
        {
            return controller.Submarine.RealPosition.DistanceTo(_spatial.GlobalTransform.origin) < 200;
        }

        return false;
    }

    public void UpdateOrders(GoalBotController controller, float delta)
    {
        controller.AutopilotTo(_spatial.GlobalTransform.origin, delta, 1);
    }
}